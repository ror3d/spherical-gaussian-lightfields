
# Compilation

## Dependencies

- Cuda 10.1 or greater. It works with 11.4 (latest at time of writing): https://developer.nvidia.com/cuda-downloads
- CuDNN 7.X or 8.X for the corresponding CUDA version: https://developer.nvidia.com/rdp/cudnn-download
- Optix 6.0 (might work with later versions maybe, not tested, and I think the API changed so probably not): https://developer.nvidia.com/designworks/optix/downloads/legacy
- Ensure that Nvidia driviers are up to date, Optix or CUDA might break for no apparent reason otherwise.
- External dependencies in `external/` directory are provided for windows

## Compile

- Generate the project files with CMake >= 3.18.
- Tested on Visual Studio 2019, it probably will not work with other systems.
- Make sure to set the configuration to Release (or RelWithDebInfo) to run the programs with good performance.
- For rendering: set the `lightfield-bake-app` project as the Startup Project.
- For baking the gaussians: set the `trainer` project as the Startup Project.
- Compiling should hopefully just work and the required dlls should be already taken care of.


# Workflow Overview

## Pre-training Render

Using the `lightfield-bake-app` project:

- In the Lightfield Bake tab, generate the input for the trainer
	- This generates a json file, a couple of LMM files (lightmap metadata), and bunch of LFB files.
- In the Lightmap Bake tab, bake the diffuse light into a texture to be used later.

We usually leave the lightfield bake to run overnight to get a more converged output.

## Bake Gaussians

Using the `trainer` project:

- Preprocess the baked lightfield files (improve convergence and results)
- Train the network to generate the gaussians with the preprocessed lightfield files. This produces the files for evaluation (and restart of the network) every few epochs.

In our experience, it usually takes around 4 hours for the network to converge to good results, regardless of the amount of lightfields.


# Usage

## Rendering app

### File input

- When run, it will ask the user to find a model file to open (or give the option to open previously opened files)
- Requires GLTF files as input, as text, vertex information in .bin file or embedded as base64.
	- Supports PBR material model, for the most part
- Textures as jpeg, png or hdr
- In the `sample/` directory there's a cornell box sample model.

### Interface

- Settings can be stored so they persist between runs with the button at the top of the Settings panel.
- Window layout, including starting selected tab, can be stored between runs through the menu bar `Window > Save Window Layout`
- I toggles the GUI visibility in the frame. `File > Take Screenshot` does what it says. The animations window is to create video animations, by exporting each frame as a single image.


#### Camera control
- WASD moves the camera in flying mode.
- E and Q move up and down respectively.
- Left mouse button drag to control direction of the camera.
- Right mouse button drag to roll the camera.


#### Rendering Tabs

- On the Settings panel you will find several different properties to be modified that relate to different renderers.
- `Deferred` tab in the application shows a view of the loaded model with diffuse lighting (no specular from the environment, that's added with the gaussians).
- `SG Result` tab is to evaluate the gaussians when they have been baked.
- `Pathtracer` is the same scene run through the pathtracer used to generate the gaussians. Make sure the scene looks as expected here.
- `Lightmap Bake` is used to bake the diffuse contributions of the light on the scene as a lightmap. Once done, the lightmap can be saved to a file.
- `Lightfield Bake` tab is to generate the lighfield images that are input to the training. For it to work, it requires that a lightmap can be baked, because it uses the same code to decide the positions of each lightfield to sample.

- On the Scene panel, different properties for each object and material on the scene can be modified.
	- The `Scene` root object in the tree has several properties that modify global things of the scene
		- Unit Multiplier determines how fast the camera moves
		- **Environment Map** lets you set an environment map to the scene. You can save the scene file with that modified to keep it stored.
		- Environment Map Intensity is a multiplier to the light the environment map contributes to the scene.
		- Environment Color tints the light from the environment map.
		- **Lightmap Texture** allows loading a texture to be used as the diffuse contribution lighting for the scene.


### Pathtracer settings
- `max_samples` determines the maximum number of samples to take for the image. 0 means render indefinitely.
- `max_bounces` is the limit of bounces a light ray takes in the scene. More means slower iterations but less biased result.
- `paths_per_pixel` This is the amount of paths that will be run before updating the visible rendered image. Higher values will make the application feel very slow, too low values will reduce the throughput because the rendered texture is copied out of the CUDA surface and that takes time. A 
- `Minimum material roughness` is used to avoid too low roughness values, as the gaussians can't handle those very well.
- `Max envmap value` clips the value of the light contribution from the environment map. 0 means no clipping.


### Baking Lightmap
- `Width` and `Height` in the settings panel for the `Texture Baker` determine the resolution of the lightmap texture that will be generated.
- The algorithm uses the second UV channel (`TEXCOORD_1`) of the objects in the scene (requires all objects to have it, and they should not overlap).
- In the rendered view window:
	- `Bake Lightmap` starts baking
	- `Restart` forgets the baked map.
	- `Save...` let's you save to file (required to include it in the scene)


### Baking Lightfields

Takes samples of lightfields in the scene placed on the surface of objects.

Similar to the lightmap, it uses the second UV channel of the objects, dividing the [0,1] square into a grid of NxN points.

Produces `.lfb` files, each containing a batch of lightfield images. It iterates through all the points in the scene and produces the lightfield for those, repeating from the beginning after having run past all of them.

The settings here:

- `Lightfield Size` determines the resolution of each lighfield. We always used 128.
- `Base Lightmap Side Size` determines how many lightfields will be generated. We used 128, 256 and 512 in our tests.
- `Batch Grid Size` to avoid having a single file of 100GB, the output of the algorithm is divided in batch files. Each contains NxN tiles, N being the value of this setting. All the tiles in the batch will be pathtraced at the same time, so it's best to keep this to not big values.
- `Default Max Samples` determines the maximum number of samples to take for a single batch before moving to the next batch. This does not limit how many samples each lightfield will end up having, since the program will go back to the beginning and accumulate new samples there.
- `Max Jitter Angle Roughness` and `Max Jitter Position` control how much the position of the lightfield points is jittered as well as the direction at which the starting rays are shot.
- `Environment as Alpha` makes the environment map be not included in the lightfield and also output the points that see the environment, to be able to use the environment visibility gaussians.
- `Only pathtrace once` stops the algorithm after one iteration of the batches has been pathtraced.

In the Pathtrace Lightfield window:
- Start pathtracing asks for a file location to save the output, and then it starts pathtracing the lightfields.
- Continue pathtracing asks for a json file that was generated by a previous run of the lightfield pathtracer to be able to continue tracing from that point.
- Trace single lightfield produces only one lightfield image instead of batches. For debug, mostly.
- Display Channels determines which channels are displayed while pathtracing. This does not change what is actually saved to file.


### Visualising trained gaussians

The results of the baking/training can be visualised in the `SG Result` tab. There:

- The window `SG Options` lets you load SGT file produced in the training step. There's one for the reflections, and one for the environment visibility.
- The `Convolved Environment Map` window is there to preconvolve the environment map into a 3D texture for a range of roughnesses. It's slower the more levels that are created and the more samples that are taken, so keep that in mind. The default values finish work in a few seconds when running in Release.

Once these steps are done, you should be seeing the result of our algorithm.

The `SG Debug` window is used to visualise the spherical gaussians at a specific point. The point can be selected in the scene with `Control+Left Click`.
The selected point will be marked in red in the scene. In the images, the reflected direction from the eye to the point will be marked with a small white/green cross.

The images shown represent, in order:
	- Evaluation of the gaussians for the whole sphere around the point. Half of the area will be darkened, that represents the back side of the surface, which we don't use.
	- Same view, but showing the boundaries of the gaussians instead of the actual value. In red is the plane of the surface.
	- The absolute value of the difference between the evaluated gaussians and the LFB input to the training. This requires that the input that was used in the training is in the parent directory of the SGT files.
	- The LFB input to the training that was used to generate this set of gaussians.


## Training app

### Preprocessing

The data needs to be preprocessed a bit for the trainer to work better, as well as to have the input to the visibility gaussians. To do that, you can run the trainer app with the following command line arguments\*:

- `trainer --convolve path_to_the_lfb_directory` -- this smoothes the lightfields a bit with a gaussian blur, to compensate for possible lack of convergence in the pathtracing, as well as because the gaussians can't really represent too high frequencies like sharp edges from the geometry.
- `trainer --scramble path_to_the_lfb_directory` -- this scrambles the lightfield images throughout the batches, since training neural networks tends to work better when the input is randomized in order. Otherwise it would go in in the order of the points on the lightmap. **More importantly**, it also does the division of the channels 3,4,5 over 6,7,8 that's required to train for the visibility gaussians.

Both functions create a backup of the data as it was before the changes. In most cases you can probably just delete the `convolved` and `original` sub-directories once these two steps are done.

(\*) In Visual Studio we use the Smart Command Line Arguments extension to simplify the process of changing the commands to run. In the project there's already a configuration file for it.


### Training

There's a bunch of different options to train the gaussians. We recommend using the following:

- `trainer -j -h -l path_to_the_lfb_directory` -- Use this command to train the reflections gaussians.
- `trainer -j -h -l -C3 path_to_the_lfb_directory` -- Use this to train the visibility gaussians. They train over the channels 3,4,5, which are the result of the division done in the scramble step of the preprocessing.

This step will output the files in a directory named something like `output_jitter_upper_ramp_g16`, which describes the settings of the training. The second command will look like `output_jitter_upper_ramp_channels_3_4_5_g16`.
The files output by the training step are, every few epochs:
	- an NPP file (network parameters) and SGT file (spherical gaussian texture)
	- mse_image_N.png is the mse for each point in the gaussian map
	- output_epoch_N.png is the input_colors_epoch_0.png minus the evaluated value of the gaussians. (Optimally these would become black)
It will also update the file mseplot.py with the latest MSE, you can use the plot it produces to review the progress of the MSE in the training.


