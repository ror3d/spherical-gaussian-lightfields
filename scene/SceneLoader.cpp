#include "SceneLoader.h"
#include "Scene.h"

#include <glm/gtc/constants.hpp>
#include <stb_image.h>
#include <utils/Log.h>
#include <utils/ProgressListener.h>
#include <utils/glm_extensions.h>
#include <sstream>
#include <utils/json.hpp>

using namespace std;

namespace scene
{
bool loadScene_gltf( Scene* scene, const std::string& filename );
bool saveScene_gltf( Scene* scene, const std::string& filename );

bool loadScene( Scene* scene, const std::string& filename )
{
	size_t ed = filename.find_last_of( '.' );
	std::string extension = filename.substr( ed );
	if ( extension == ".gltf" )
	{
		return loadScene_gltf( scene, filename );
	}
	else if ( extension == ".scene" )
	{
		LOG_ERROR( "Old scene format is no longer supported." );
	}

	return false;
}

bool saveScene( Scene* scene, const std::string& filename )
{
	size_t ed = filename.find_last_of( '.' );
	std::string extension = filename.substr( ed );
	if ( extension == ".gltf" )
	{
		return saveScene_gltf( scene, filename );
	}
	else if ( extension == ".scene" )
	{
		LOG_ERROR( "Old scene format is no longer supported." );
	}

	return false;
}


}   // namespace scene
