#pragma once
#include "Material.h"

namespace scene
{
	class Intersection
	{
	public:
		using vec3 = glm::vec3;
		using vec2 = glm::vec2;

		vec3 m_position;
		vec3 m_normal;
		vec3 m_geometryNormal;
		vec3 facingNormal(const vec3 &wi) const {
			// TODO: use glm::faceforward();
			return (dot(wi, m_geometryNormal) > 0.0f) ? m_normal : -m_normal;
		};
		vec2 m_uv;
		Material *m_material;
	};
}
