#pragma once

#include <string>
#include "glm/vec3.hpp"
#include "utils/glm_extensions.h"
#include "Texture.h"

namespace scene {
	class Intersection;
	class Material {
	public:
		using vec3 = glm::vec3;
		using vec4 = glm::vec4;
		std::string m_name;

	    [[deprecated]] Texture* m_occlusion_texture = nullptr;

	    vec4     m_base_color_factor = vec4(1);
		std::string m_base_color_texture_path;
	    Texture* m_base_color_texture = nullptr;
		std::string m_normal_texture_path;
	    Texture* m_normal_texture = nullptr;
	    float    m_normal_scale = 1;
	    float    m_alpha_cutoff = 0.5;
	    vec3     m_emissive_factor = vec3(0);
		std::string m_emissive_texture_path;
	    Texture* m_emissive_texture = nullptr;
	    float    m_metallic_factor = 1;
	    float    m_roughness_factor = 1;
		std::string m_metallic_roughness_texture_path;
	    Texture* m_metallic_roughness_texture = nullptr;

		Material() { };
	};

	///////////////////////////////////////////////////////////////////////////
	// Serialization
	///////////////////////////////////////////////////////////////////////////
	inline std::ostream& operator << (std::ostream &os, Material &m)
	{
		os << m.m_name << " "
			<< glm::vec3(m.m_base_color_factor) << " "
			<< ((m.m_base_color_texture != NULL) ? m.m_base_color_texture->m_filename : std::string("notexture")) << " "
			<< ((m.m_normal_texture != NULL) ? m.m_normal_texture->m_filename : std::string("notexture")) << " "
			<< ((m.m_metallic_roughness_texture != NULL) ? m.m_metallic_roughness_texture->m_filename : std::string("notexture")) << " "
			<< 0.f << " "
			<< m.m_roughness_factor << " "
			<< m.m_metallic_factor; 
		return os;
	}

	inline std::istream& operator >> (std::istream &is, Material &m)
	{
		glm::vec3 color;
		is >> color;
		m.m_base_color_factor = glm::vec4(color, 1);
		std::string texture_filename; 
		is >> texture_filename; 
		if(texture_filename != "notexture") {
			m.m_base_color_texture = new Texture2D_RGBA8;
			m.m_base_color_texture->loadFromFile(texture_filename);
			m.m_base_color_texture_path = texture_filename;
		}
		is >> texture_filename;
		if (texture_filename != "notexture") {
			m.m_normal_texture = new Texture2D_RGBA8;
			m.m_normal_texture->loadFromFile(texture_filename);
			m.m_normal_texture_path = texture_filename;
		}
		is >> texture_filename;
		if (texture_filename != "notexture") {
			m.m_metallic_roughness_texture = new Texture2D_RGBA8;
			m.m_metallic_roughness_texture->loadFromFile(texture_filename);
			m.m_metallic_roughness_texture_path = texture_filename;
		}
		float dummy_old_fresnel;
		is >> dummy_old_fresnel;
		is >> m.m_roughness_factor;
		is >> m.m_metallic_factor;
		return is;
	}
}
