#include "ShadowGenerators.h"
#include <sstream>
#ifndef NO_OPENGL_OR_CUDA
#include <utils/opengl_helpers.h>
#endif

using namespace std;
using namespace chag;

namespace scene
{
	void ShadowMapShadowGenerator::resize(int width, int height)
	{
#ifndef NO_OPENGL_OR_CUDA		
		m_resolution = ivec2(width, height);
		if (m_framebuffer_ID == 0) {
			m_depth_texture = opengl_helpers::texture::createTexture(width, height, GL_DEPTH_COMPONENT32);
			m_framebuffer_ID = opengl_helpers::framebuffer::createFramebuffer(width, height, m_depth_texture, {});
		}
		int temp;
		glGetIntegerv(GL_TEXTURE_BINDING_2D, &temp);
		glBindTexture(GL_TEXTURE_2D, m_depth_texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		glBindTexture(GL_TEXTURE_2D, temp);
#endif
	}

#ifndef NO_OPENGL_OR_CUDA		
	void ShadowMapShadowGenerator::update(const vector<Model *> &models, GLSLProgramObject *program)
	{
		view &myView = *(getView());
		glPushAttrib(GL_ALL_ATTRIB_BITS);
		opengl_helpers::framebuffer::pushFramebuffer(m_framebuffer_ID);
		glClear(GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, m_resolution.x, m_resolution.y);
		glPolygonOffset(m_polygon_offset_factor, m_polygon_offset_units);
		glEnable(GL_POLYGON_OFFSET_FILL);
		program->enable();
		myView.push();
		{
			for(auto model : models) {
				// TODO: Check if shadow caster
				model->render(program);
			}
		}
		myView.pop();
		program->disable();
		opengl_helpers::framebuffer::popFramebuffer();
		glPopAttrib();
	}

	void ShadowMapShadowGenerator::submit(GLSLProgramObject *programObject, int lightID)
	{
		{	stringstream ss;
		ss << "light_has_shadowmap[" << lightID << "]";
		programObject->setUniform(ss.str().c_str(), true);
		}
		{   stringstream ss;
		ss << "light_shadowmap_sampler[" << lightID << "]";
		programObject->bindTexture(ss.str().c_str(), m_depth_texture, GL_TEXTURE_2D, 6 + lightID);
		}
		{   stringstream ss;
		ss << "light_shadowmap_mvp[" << lightID << "]";
		programObject->setUniform(ss.str().c_str(), getView()->get_MVP());
		}
		{   stringstream ss;
		ss << "light_fov[" << lightID << "]";
		programObject->setUniform(ss.str().c_str(), getView()->m_fov);
		}
	}
#endif
}
