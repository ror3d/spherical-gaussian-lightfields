#pragma once
#include "Item.h"
#include "Sampling.h"
#include "glm/gtc/constants.hpp"
#include "utils/Log.h"
#include "utils/glm_extensions.h"

namespace scene {
///////////////////////////////////////////////////////////////////////////
// Lights
///////////////////////////////////////////////////////////////////////////
class Light final : public Item {
 public:
	using vec3 = glm::vec3;
	enum class Shape { Point, Disc, Rectangle };
	enum class Distribution { Diffuse, Spot };

	Light() {};
	Light(const Light &other) : m_shape(other.m_shape), m_distribution(other.m_distribution), m_radiance(other.m_radiance), m_color(other.m_color) {}
	~Light() final {};
	Item *clone() final {
		return new Light(*this);
	}
	
	// Per light data
	struct {
		Shape type;
		union {
			// Disc
			struct {
				float m_radius;
			} Disc;

			// Rectangle
			struct {
				float m_width, m_height;
			} Rectangle;
		};
	} m_shape;
	struct {
		Distribution type;
		union {
			// Spot
			struct {
				float m_attenuation_start;
				float m_attenuation_end;
				float m_beam_fov;
				float m_field_fov;
			} Spot;
		};
	} m_distribution;
	// Common for all lights
	float m_radiance;
	vec3 m_color;
};

///////////////////////////////////////////////////////////////////////////
// Serialization
///////////////////////////////////////////////////////////////////////////
inline std::ostream& operator<<(std::ostream& os, const scene::Light& c) {
	// Prefix
	os << "light";
	switch (c.m_shape.type) {
		case Light::Shape::Point:     os << " " << "point";     break;
		case Light::Shape::Disc:      os << " " << "disc" ;     break;
		case Light::Shape::Rectangle: os << " " << "rectangle"; break; 
		default: 
			LOG_ERROR("Shape not serialized!");
			break;
	}
	switch (c.m_distribution.type) {
		case Light::Distribution::Diffuse: os << " " << "diffuse"; break;
		case Light::Distribution::Spot:    os << " " << "spot";    break;
		default:
			LOG_ERROR("Distribution not serialized!");
			break;
	}

	// Common
	os << " " << c.m_name << " " << c.m_color << " " << c.m_radiance << " " << c.m_transform;

	// Shape
	switch (c.m_shape.type) {
		case Light::Shape::Disc: {
			os << " " << c.m_shape.Disc.m_radius;
			break;
		}
		case Light::Shape::Rectangle: {
			os << " " << c.m_shape.Rectangle.m_width << " " << c.m_shape.Rectangle.m_height;
			break;
		}
		case Light::Shape::Point: break;
		default: LOG_ERROR("Trying to save unimplemented light " << c.m_name << "!"); break;
	}

	// Distribution
	switch (c.m_distribution.type) {
		case Light::Distribution::Spot: {
			os << " " << c.m_distribution.Spot.m_attenuation_start << " "
			   << c.m_distribution.Spot.m_attenuation_end << " " << c.m_distribution.Spot.m_beam_fov << " "
			   << c.m_distribution.Spot.m_field_fov;
		}
		case Light::Distribution::Diffuse:
		default: break;
	}
	return os;
}

inline std::istream& operator>>(std::istream& is, scene::Light& l) {
	// Types
	std::string shape;
	std::string dist;

	is >> shape;
	if      (shape == "point")     { l.m_shape.type = Light::Shape::Point; }
	else if (shape == "disc")      { l.m_shape.type = Light::Shape::Disc; }
	else if (shape == "rectangle") { l.m_shape.type = Light::Shape::Rectangle; }
	else { LOG_ERROR("Could not read shape: " << shape); }

	is >> dist;
	if      (dist == "diffuse") { l.m_distribution.type = Light::Distribution::Diffuse; }
	else if (dist == "spot")    { l.m_distribution.type = Light::Distribution::Spot; }
	else { LOG_ERROR("Could not read distribution: " << dist); }

	// Common
	is >> l.m_name >> l.m_color >> l.m_radiance >> l.m_transform;

	// Shape
	switch (l.m_shape.type) {
		case Light::Shape::Disc: {
			is >> l.m_shape.Disc.m_radius;
			break;
		}
		case Light::Shape::Rectangle: {
			is >> l.m_shape.Rectangle.m_width >> l.m_shape.Rectangle.m_height;
			break;
		}
		case Light::Shape::Point:
		default: break;
	}

	// Distribution
	switch (l.m_distribution.type) {
		case Light::Distribution::Spot: {
			is >> l.m_distribution.Spot.m_attenuation_start >> l.m_distribution.Spot.m_attenuation_end >>
			    l.m_distribution.Spot.m_beam_fov >> l.m_distribution.Spot.m_field_fov;
			break;
		}
		case Light::Distribution::Diffuse:
		default: break;
	}
	return is;
}
}  // namespace scene
