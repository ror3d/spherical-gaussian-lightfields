#pragma once

#include <map>
#include <vector>
#include <string>
#include <memory>
#include <stdint.h>
#include "glm/vec2.hpp"
#include "glm/vec3.hpp"
#ifndef NO_OPENGL_OR_CUDA
#include <GL/glew.h>
#endif

namespace scene {
	///////////////////////////////////////////////////////////////////////////
	// Textures
	// Instead of going all generic here, I will subclass texture for a couple
	// of different types. May or may not be a good idea. Time will tell. 
	///////////////////////////////////////////////////////////////////////////
	class Texture
	{
	public:
		static const size_t INVALID_GLTF_TEX_ID = ~size_t( 0 );
		using vec2 = glm::vec2;
		using uvec2 = glm::uvec2;
		using colorf = glm::vec3;

		enum class Format
		{
			F_Y,
			F_YA,
			F_RGB,
			F_RGBA
		};
		enum class Type
		{
			T_FLOAT32,
			T_UINT8
		};

		Texture() : m_width( 0 ), m_height( 0 ), m_GLID( 0 ) {}
		virtual ~Texture() {}

		virtual colorf sample(vec2 uv) const = 0;
		virtual colorf getRawValue(uvec2 uv) const = 0;
		virtual bool loadFromFile(const std::string &filename) = 0;
	    virtual bool loadFromMemory( const std::string& name, const void* memory, size_t length ) = 0;
		virtual bool createFromMemory( const std::string& name, unsigned int width, unsigned int height, Format fmt, Type type, const void* data ) = 0;

		virtual void copyFrom( Texture* t );

		virtual GLenum getBindTarget() const { return GL_TEXTURE_2D; }

	public:
		static Texture* loadFromAnyFileType( const std::string &filename );
	public:
		std::string m_filename;
		size_t m_gltfTexId = INVALID_GLTF_TEX_ID;
		unsigned int m_width, m_height;
		uint32_t m_GLID;
		Format m_format;
		Type m_type;
		std::vector<uint8_t> m_data;

	private:
		static std::map<std::string, std::unique_ptr<Texture>> s_cache;
	protected:
		static Texture* getCached( const std::string& name );
		static void addToCache( const std::string& name, Texture* tex );
	};

	class Texture2D_RGBA8 : public Texture
	{
	public:
		Texture2D_RGBA8() { };
		Texture2D_RGBA8( const Texture2D_RGBA8& ) = default;

		colorf sample(vec2 uv) const override;
		colorf getRawValue(uvec2 uv) const override;
		bool loadFromFile(const std::string &filename) override;
	    bool loadFromMemory( const std::string& name, const void* memory, size_t length );
		bool createFromMemory( const std::string& name, unsigned int width, unsigned int height, Format fmt, Type type, const void* data ) override;

	public:
	};


	class Texture2D_RGBA32F : public Texture
	{
	public:
		Texture2D_RGBA32F() { };
		Texture2D_RGBA32F( const Texture2D_RGBA32F& ) = default;

		colorf sample(vec2 uv) const override;
		colorf getRawValue(uvec2 uv) const override;
		bool loadFromFile(const std::string &filename) override;
	    bool loadFromMemory( const std::string& name, const void* memory, size_t length );
		bool createFromMemory( const std::string& name, unsigned int width, unsigned int height, Format fmt, Type type, const void* data ) override;
	public:
	};
}
