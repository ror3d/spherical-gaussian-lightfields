#pragma once
#include "Item.h"
#include "utils/Log.h"
#include "utils/glm_extensions.h"

namespace scene
{
	///////////////////////////////////////////////////////////////////////////
	// Cameras
	///////////////////////////////////////////////////////////////////////////
	class Camera : public Item {
	public:
		Camera() : Item() {}
		Camera(const Camera &other) : Item(other), m_near(other.m_near), m_far(other.m_far) {}
		virtual ~Camera() {}
		float m_near;
		float m_far;
	};

	class OrthoCamera : public Camera {
		OrthoCamera() : Camera() {};
		OrthoCamera(const OrthoCamera &/*other*/) : Camera() {}
		virtual Item *clone() { LOG_ERROR("OrthoCamera::clone not implemented"); return NULL; }
	};

	class PerspectiveCamera : public Camera {
	public:
		PerspectiveCamera() : Camera() {}
		PerspectiveCamera(const PerspectiveCamera &other) : Camera(other), m_fov(other.m_fov), m_aspect_ratio(other.m_aspect_ratio) {}
		float m_fov;
		float m_aspect_ratio;
		virtual view * getView() {
			if(m_view == NULL) m_view = new view();
			m_view->pos = m_transform.pos;
			m_view->R = m_transform.R;
			m_view->m_near = m_near;
			m_view->m_far = m_far;
			m_view->m_fov = m_fov;
			m_view->m_aspect_ratio = m_aspect_ratio;
			return m_view;
		}
		virtual Item *clone() {
			return new PerspectiveCamera(*this);
		}
	};

	///////////////////////////////////////////////////////////////////////////
	// Serialization
	///////////////////////////////////////////////////////////////////////////
	inline std::ostream& operator << (std::ostream &os, scene::Camera &c)
	{
		scene::PerspectiveCamera *pcamera = dynamic_cast<scene::PerspectiveCamera *>(&c);
		scene::OrthoCamera *ocamera = dynamic_cast<scene::OrthoCamera *>(&c);
		if(pcamera) {
			os << "perspective_camera " << pcamera->m_name << " " << pcamera->m_near << " " << pcamera->m_far << " "
				<< pcamera->m_fov << " " << pcamera->m_aspect_ratio << " " << pcamera->m_transform;
		}
		else if(ocamera) {
			os << "ortho_camera " << ocamera->m_name << " " << ocamera->m_near << " " << ocamera->m_far << " "
				<< ocamera->m_transform;
		}
		else {
			LOG_ERROR("Trying to save unimplemented camera " << c.m_name << "!");
		}
		return os;
	}
	inline std::istream& operator >> (std::istream& is, scene::PerspectiveCamera& c)
	{
		is >> c.m_name >> c.m_near >> c.m_far >> c.m_fov >> c.m_aspect_ratio >> c.m_transform;
		return is;
	}
}
