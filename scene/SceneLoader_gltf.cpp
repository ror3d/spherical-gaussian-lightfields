#include "Scene.h"
#include "SceneLoader.h"

#include <glm/gtc/constants.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include <sstream>
#include <algorithm>
#include <utils/filesystem.h>
#include <stb_image.h>
#include <utils/Log.h>
#include <utils/ProgressListener.h>
#include <utils/base64.h>
#include <utils/glm_extensions.h>
#include <utils/json.hpp>
#include <utils/json_extras.hpp>

#ifdef OPAQUE   // Windows bullshit
#undef OPAQUE
#endif

using namespace std;
using json = nlohmann::json;

namespace scene
{
namespace gltf
{
	template<typename _T, typename _C = std::vector<_T>>
	class offset_pointer
	{
	public:
		offset_pointer() {}
		offset_pointer( _C* _container ) : container( _container ) {}
		offset_pointer( _C* _container, size_t _offset ) : container( _container ), offset(_offset) {}
		offset_pointer( const offset_pointer& ) = default;
		offset_pointer( offset_pointer&& ) = default;
		offset_pointer& operator=( const offset_pointer& ) = default;
		offset_pointer& operator=( offset_pointer&& ) = default;

		_T& operator* () { return (*container)[offset]; }
		_T* operator-> () { return &((*container)[offset]); }
		const _T& operator* () const { return (*container)[offset]; }
		const _T* operator-> () const { return &((*container)[offset]); }

		operator bool() const { return container != nullptr; }

		void set( _C* _container, size_t _offset ) { container = _container; offset = _offset; }
		void set( size_t _offset ) { offset = _offset; }
		void set( _C* _container ) { container = _container; }
		void unset() { container = nullptr; offset = 0; }

		size_t get_offset() const { return offset; }
		_C* get_container() const { return container; }
		_T* get() { return &((*container)[offset]); }
		const _T* get() const { return &((*container)[offset]); }
	private:
		_C* container = nullptr;
		size_t offset = 0;
	};

	struct GLTF;

	struct Buffer
	{
		size_t				 index;
		size_t               byteLength = 0;
		std::string          uri;
		std::vector<uint8_t> data;
	};

	struct BufferView
	{
		size_t  index;
		Buffer* buffer = nullptr;
		size_t  byteLength = 0;
		size_t  byteOffset = 0;
		size_t  byteStride = 0;
		enum Target { NONE = 0, ARRAY_BUFFER = 34962, ELEMENT_ARRAY_BUFFER = 34963 };
		Target target = NONE;   // The target that the GPU buffer should be bound to.
	};

	struct Accessor
	{
		enum ComponentType { BYTE = 5120, UNSIGNED_BYTE, SHORT, UNSIGNED_SHORT, INT, UNSIGNED_INT, FLOAT };
		enum DataType { SCALAR, VEC2, VEC3, VEC4, MAT2, MAT3, MAT4 };

	public:
		size_t        index;
		std::string   name;
		BufferView*   bufferView = nullptr;
		ComponentType componentType;
		DataType      type;
		size_t        count = 0;
		size_t        byteOffset = 0;
		std::vector<float> min;
		std::vector<float> max;
		// I don't think we really care about min & max, or sparse.

	public:
		static size_t getSize( ComponentType t )
		{
			switch ( t )
			{
				case BYTE:
				case UNSIGNED_BYTE:
					return 1;
				case SHORT:
				case UNSIGNED_SHORT:
					return 2;
				case INT:
				case UNSIGNED_INT:
					return 4;
				case FLOAT:
					return 4;
			}
			DebugError( "Unrecognized component type" );
			return 1;
		}
		static size_t getSize( DataType t )
		{
			switch ( t )
			{
				case SCALAR:
					return 1;
				case VEC2:
					return 2;
				case VEC3:
					return 3;
				case VEC4:
				case MAT2:
					return 4;
				case MAT3:
					return 9;
				case MAT4:
					return 16;
			}
			DebugError( "Unrecognized data type" );
			return 1;
		}
	};

	struct Camera
	{
		size_t      index;
		std::string name;
		enum Type { PERSPECTIVE, ORTHOGRAPHIC };
		Type        type = PERSPECTIVE;
		union
		{
			struct
			{
				float aspectRatio;
				float yfov;
				float znear, zfar;
			} perspective;

			struct
			{
				float xmag, ymag;
				float znear, zfar;
			} orthographic;
		};
	};

	struct Image
	{
		std::string          name;
		size_t               index;
		std::string          uri;
		BufferView*          bufferView = nullptr;
		std::string          mimeType;
		std::vector<uint8_t> decodedUriData;
	};

	struct Sampler
	{
		std::string name;
		size_t      index;
		enum Filter {
			NEAREST = 9728,
			LINEAR = 9729,
			NEAREST_MIPMAP_NEAREST = 9984,
			LINEAR_MIPMAP_NEAREST = 9985,
			NEAREST_MIPMAP_LINEAR = 9986,
			LINEAR_MIPMAP_LINEA = 9987
		};
		enum WrapMode { CLAMP_TO_EDGE = 33071, MIRRORED_REPEAT = 33648, REPEAT = 10497 };
		Filter   magFilter = LINEAR;   // Only valid for magFilter are NEAREST and LINEAR
		Filter   minFilter = LINEAR;
		WrapMode wrapS = REPEAT;
		WrapMode wrapT = REPEAT;
	};

	struct Texture
	{
		std::string name;
		size_t   index;
		offset_pointer<Image>   image = nullptr;
		offset_pointer<Sampler> sampler = nullptr;
	};

	struct TextureRef
	{
		size_t   texCoordIndex = 0;   // index for TEXCOORD_0, TEXCOORD_1, etc.
		offset_pointer<Texture> texture = nullptr;
	};

	struct NormalTextureRef : public TextureRef
	{
		float scale = 1;   // Scale multiplier for normal vectors from a normal texture
	};

	struct OcclusionTextureRef : public TextureRef
	{
		float strength = 1;
	};

	struct Material
	{
		enum AlphaMode { OPAQUE, MASK, BLEND };
	public:
		size_t      index;
		std::string name;
		struct
		{
			glm::vec4  baseColorFactor = { 1, 1, 1, 1 };
			TextureRef baseColorTexture;
			float      metallicFactor = 1;
			float      roughnessFactor = 1;
			TextureRef metallicRoughnessTexture;
		} pbrMetallicRoughness;
		NormalTextureRef    normalTexture;
		TextureRef          emissiveTexture;
		glm::vec3           emissiveFactor;
		AlphaMode           alphaMode = OPAQUE;
		float               alphaCutoff = 0;
		bool                doubleSided = false;
	};

	struct Primitive
	{
		enum Mode { POINTS, LINES, LINE_LOOP, LINE_STRIP, TRIANGLES, TRIANGLE_STRIP, TRIANGLE_FAN };
	public:
		Accessor*                        indices = nullptr;
		Material*                        material = nullptr;
		Mode                             mode = TRIANGLES;
		std::map<std::string, Accessor*> attributes;
	};

	struct Mesh
	{
		size_t                 index;
		std::string            name;
		std::vector<Primitive> primitives;
	};

	struct KhrLight
	{
		size_t      index;
		std::string name;
		glm::vec3   color = glm::vec3( 1, 1, 1 );
		float       intensity = 1;
		float       range = std::numeric_limits<float>::infinity();
		enum { Point, Spot, Directional } type;
		struct   // Spotlight specific
		{
			float innerConeAngle = 0;
			float outerConeAngle = glm::pi<float>() / 4;
		} spotlight;
	};

	struct Node
	{
		Node() : matrix( 1 ) {}
		size_t index;
		std::string name;
		enum { TRS, MATRIX } transform_type = MATRIX;
		union
		{
			struct
			{
				glm::quat rotation;
				glm::vec3 scale;
				glm::vec3 translation;
			} m;
			glm::mat4 matrix;
		};
		std::vector<offset_pointer<Node>> children;
		Mesh*              mesh = nullptr;
		Camera*            camera = nullptr;
		KhrLight*          khr_light = nullptr;
	};

	struct Scene
	{
		size_t			   index;
		std::string        name;
		std::vector<offset_pointer<Node>> nodes;
		struct
		{
			float unitMultiplier = 1;
			float debugGeomMultiplier = 1;
			glm::vec3 environmentColor = { -1, -1, -1 };
			float environmentMultiplier = 1;
			TextureRef environmentMap;
			TextureRef lightMap;
		} extra;
	};

	struct GLTF
	{
		std::string             filePath;
		std::vector<Buffer>     buffers;
		std::vector<BufferView> bufferViews;
		std::vector<Accessor>   accessors;
		std::vector<Image>      images;
		std::vector<Sampler>    samplers;
		std::vector<Texture>    textures;
		std::vector<Material>   materials;
		std::vector<Mesh>       meshes;
		std::vector<Camera>     cameras;
		std::vector<Node>       nodes;
		std::vector<Scene>      scenes;
		std::vector<KhrLight>   khrLights;
		Scene*                  defaultScene = nullptr;
		json                    extras;
	};

	static thread_local GLTF* s_currentGltf = nullptr;

	std::string get_relative_path( std::string current_path, std::string target_path )
	{
		std::replace( current_path.begin(), current_path.end(), '\\', '/' );
		std::replace( target_path.begin(), target_path.end(), '\\', '/' );
		size_t pp = 0;
		size_t last_slash = 0;
		while ( pp < current_path.length() && pp < target_path.length() && current_path[pp] == target_path[pp] )
		{
			if ( current_path[pp] == '/' )
			{
				last_slash = pp;
			}
			++pp;
		}

		target_path = target_path.substr( last_slash + 1 );
		while ( pp < current_path.length() )
		{
			if ( current_path[pp] == '/' )
			{
				target_path = "../" + target_path;
			}
			++pp;
		}

		return target_path;
	}

	std::string get_local_uri( const std::string& referrer_uri, const std::string& local_uri )
	{
		size_t      last_fslash = referrer_uri.find_last_of( '/' );
		size_t      last_bslash = referrer_uri.find_last_of( '\\' );
		std::string path = "";
		if ( last_fslash != std::string::npos || last_bslash != std::string::npos )
		{
			size_t last_slash = last_fslash != std::string::npos && last_bslash != std::string::npos
			                            ? std::max( last_fslash, last_bslash )
			                            : std::min( last_fslash, last_bslash );
			path = referrer_uri.substr( 0, last_slash + 1 );
		}

		return path + local_uri;
	}

	void load_binary_data( const std::string& uri, std::vector<uint8_t>& data )
	{
		if ( uri.find( "data:" ) == 0 )
		{
			// Decode the data
			size_t start = uri.find( "," );
			if ( start != std::string::npos )
			{
				start += 1;
				data = b64decode( uri.data() + start, uri.size() - start );
			}
			else
			{
				DebugError( "Unrecognized data URI" );
			}
		}
		else
		{
			std::string   local_uri = get_local_uri( s_currentGltf->filePath, uri );
			std::ifstream f( local_uri, std::ios::binary );   // Try to open as a local filename
			if ( !f )                                         // Try opening as-is
			{
				f = std::ifstream( uri );
			}

			if ( !f )
			{
				DebugError( "Could not open binary file" );
			}
			else if ( uri.substr( uri.find_last_of( '.' ) ) == ".bin" )
			{
				f.seekg( 0, f.end );
				size_t len = f.tellg();
				f.seekg( 0, f.beg );
				data.resize( len );
				f.read( (char*)data.data(), len );
			}
			else if ( uri.substr( uri.find_last_of( '.' ) ) == ".glb" )
			{
				DebugError( "GLB not implemented" );
			}
			else
			{
				DebugError( "File type not recognized" );
			}
		}
	}

	void save_binary_data( const std::vector<uint8_t>& data, std::string& uri )
	{
		// Depending on if uri points to a file or contains the data base64, update uri or save to file
		if ( uri.find( "data:" ) == 0 )
		{
			uri = "data:," + b64encode( data.data(), data.size() );
		}
		else
		{
			LOG_WARNING( "We won't be wrinting bin data to the file " << uri << ". We might want to if it doesn't exist, maybe." );
			// TODO
			return;

			std::string   local_uri = get_local_uri( s_currentGltf->filePath, uri );
			std::ofstream f( local_uri, std::ios::binary );   // Try to open as a local filename
			if ( !f )                                         // Try opening as-is
			{
				f = std::ofstream( uri );
			}

			if ( !f )
			{
				DebugError( "Could not open binary file for writing" );
			}
			else if ( uri.substr( uri.find_last_of( '.' ) ) == ".bin" )
			{
				f.write( (const char*)data.data(), data.size() );
			}
			else if ( uri.substr( uri.find_last_of( '.' ) ) == ".glb" )
			{
				DebugError( "GLB export not implemented" );
			}
			else
			{
				DebugError( "File type not recognized" );
			}
		}
	}

	void from_json( const json& j, Buffer& b )
	{
		j.at( "byteLength" ).get_to( b.byteLength );
		j.at( "uri" ).get_to( b.uri );

		load_binary_data( b.uri, b.data );
	}

	void to_json( json& j, const Buffer& b )
	{
		j[ "byteLength" ] = b.byteLength;

		std::string uri = b.uri;
		save_binary_data( b.data, uri );

		j[ "uri" ] = uri;
	}

	void from_json( const json& j, BufferView& b )
	{
		size_t buff;
		j.at( "buffer" ).get_to( buff );
		b.buffer = &s_currentGltf->buffers[buff];

		j.at( "byteLength" ).get_to( b.byteLength );
		if ( j.find( "byteOffset" ) != j.end() )
		{
			j.at( "byteOffset" ).get_to( b.byteOffset );
		}
		if ( j.find( "target" ) != j.end() )
		{
			int target;
			j.at( "target" ).get_to( target );
			b.target = BufferView::Target( target );
		}
		if ( j.find( "byteStride" ) != j.end() )
		{
			size_t stride;
			j.at( "byteStride" ).get_to( stride );
			b.byteStride = stride;
		}
	}

	void to_json( json& j, const BufferView& b )
	{
		j[ "buffer" ] = b.buffer->index;
		j[ "byteLength" ] = b.byteLength;
		if ( b.byteOffset > 0 )
		{
			j[ "byteOffset" ] = b.byteOffset;
		}
		if ( b.target != BufferView::Target::NONE )
		{
			j[ "target" ] = (int)b.target;
		}
		if ( b.byteStride > 0 )
		{
			j[ "byteStride" ] = b.byteStride;
		}
	}

	void from_json( const json& j, Accessor& a )
	{
		DebugAssert( j.find( "bufferView" ) != j.end() || !"We don't support sparse buffer views right now" );

		size_t bv;
		j.at( "bufferView" ).get_to( bv );
		a.bufferView = &s_currentGltf->bufferViews[bv];
		int val;
		j.at( "componentType" ).get_to( val );
		a.componentType = Accessor::ComponentType( val );
		j.at( "count" ).get_to( val );
		a.count = val;
		std::string type;
		j.at( "type" ).get_to( type );

		if ( j.find( "normalized" ) != j.end() )
		{
			DebugError( "Normalized integers are not supported" );
		}

		if ( j.find( "sparse" ) != j.end() )
		{
			DebugError( "Accessors with sparse information are not supported" );
		}

		if ( type == "SCALAR" )
			a.type = Accessor::SCALAR;
		else if ( type == "VEC2" )
			a.type = Accessor::VEC2;
		else if ( type == "VEC3" )
			a.type = Accessor::VEC3;
		else if ( type == "VEC4" )
			a.type = Accessor::VEC4;
		else if ( type == "MAT2" )
			a.type = Accessor::MAT2;
		else if ( type == "MAT3" )
			a.type = Accessor::MAT3;
		else if ( type == "MAT4" )
			a.type = Accessor::MAT4;

		if ( j.find( "min" ) != j.end() )
		{
			j.at( "min" ).get_to( a.min );
		}
		if ( j.find( "max" ) != j.end() )
		{
			j.at( "max" ).get_to( a.max );
		}

		if ( j.find( "byteOffset" ) != j.end() )
		{
			j.at( "byteOffset" ).get_to( a.byteOffset );
		}
	}

	void to_json( json& j, const Accessor& a )
	{
		if ( !a.name.empty() )
		{
			j[ "name" ] = a.name;
		}
		if ( a.bufferView )
		{
			j[ "bufferView" ] = a.bufferView->index;
		}
		switch ( a.type )
		{
			case Accessor::SCALAR:
				j[ "type" ] = "SCALAR";
				break;
			case Accessor::VEC2:
				j[ "type" ] = "VEC2";
				break;
			case Accessor::VEC3:
				j[ "type" ] = "VEC3";
				break;
			case Accessor::VEC4:
				j[ "type" ] = "VEC3";
				break;
			case Accessor::MAT2:
				j[ "type" ] = "MAT2";
				break;
			case Accessor::MAT3:
				j[ "type" ] = "MAT3";
				break;
			case Accessor::MAT4:
				j[ "type" ] = "MAT4";
				break;
		}
		j[ "componentType" ] = (int)a.componentType;
		j[ "count" ] = a.count;

		if ( a.byteOffset > 0 )
		{
			j[ "byteOffset" ] = a.byteOffset;
		}

		if ( !a.min.empty() )
		{
			j[ "min" ] = a.min;
		}
		if ( !a.max.empty() )
		{
			j[ "max" ] = a.max;
		}


	}

	void from_json( const json& j, Sampler& s )
	{
		if ( j.find( "name" ) != j.end() )
		{
			j.at( "name" ).get_to( s.name );
		}
		int val;
		if ( j.find( "magFilter" ) != j.end() )
		{
			j.at( "magFilter" ).get_to( val );
			s.magFilter = Sampler::Filter( val );
		}
		if ( j.find( "minFilter" ) != j.end() )
		{
			j.at( "minFilter" ).get_to( val );
			s.minFilter = Sampler::Filter( val );
		}
		if ( j.find( "wrapS" ) != j.end() )
		{
			j.at( "wrapS" ).get_to( val );
			s.wrapS = Sampler::WrapMode( val );
		}
		if ( j.find( "wrapT" ) != j.end() )
		{
			j.at( "wrapT" ).get_to( val );
			s.wrapT = Sampler::WrapMode( val );
		}
	}

	void to_json( json& j, const Sampler& s )
	{
		if ( !s.name.empty() )
		{
			j[ "name" ] = s.name;
		}
		j[ "magFilter" ] = (int)s.magFilter;
		j[ "minFilter" ] = (int)s.minFilter;
		if ( s.wrapS != Sampler::WrapMode::REPEAT )
		{
			j[ "wrapS" ] = (int)s.wrapS;
		}
		if ( s.wrapT != Sampler::WrapMode::REPEAT )
		{
			j[ "wrapT" ] = (int)s.wrapT;
		}
	}

	void from_json( const json& j, Image& i )
	{
		if ( j.find( "name" ) != j.end() )
		{
			j.at( "name" ).get_to( i.name );
		}
		if ( j.find( "uri" ) != j.end() )
		{
			j.at( "uri" ).get_to( i.uri );
		}
		else
		{
			size_t bv;
			j.at( "bufferView" ).get_to( bv );
			i.bufferView = &s_currentGltf->bufferViews[bv];
			j.at( "mimeType" ).get_to( i.mimeType );
		}
	}

	void to_json( json& j, const Image& i )
	{
		if ( !i.name.empty() )
		{
			j[ "name" ] = i.name;
		}
		if ( i.bufferView )
		{
			j[ "bufferView" ] = i.bufferView->index;
		}
		else
		{
			j[ "uri" ] = i.uri;
		}
	}

	void from_json( const json& j, Texture& t )
	{
		if ( j.find( "sampler" ) != j.end() )
		{
			size_t sampl;
			j.at( "sampler" ).get_to( sampl );
			t.sampler.set(&s_currentGltf->samplers, sampl);
		}
		if ( j.find( "source" ) != j.end() )
		{
			size_t img;
			j.at( "source" ).get_to( img );
			t.image.set(&s_currentGltf->images, img);
		}
	}

	void to_json( json& j, const Texture& t )
	{
		if ( !t.name.empty() )
		{
			j[ "name" ] = t.name;
		}
		if ( t.sampler )
		{
			j[ "sampler" ] = t.sampler->index;
		}
		if ( t.image )
		{
			j[ "source" ] = t.image->index;
		}
	}

	void from_json( const json& j, TextureRef& r )
	{
		size_t idx;
		j.at( "index" ).get_to( idx );
		r.texture.set( &s_currentGltf->textures, idx );
		if ( j.find( "texCoord" ) != j.end() )
		{
			j.at( "texCoord" ).get_to( r.texCoordIndex );
		}
	}

	void to_json( json& j, const TextureRef& r )
	{
		j[ "index" ] = r.texture->index;
		if ( r.texCoordIndex != 0 )
		{
			j[ "texCoord" ] = r.texCoordIndex;
		}
	}

	void from_json( const json& j, NormalTextureRef& r )
	{
		size_t idx;
		j.at( "index" ).get_to( idx );
		r.texture.set( &s_currentGltf->textures, idx );
		if ( j.find( "texCoord" ) != j.end() )
		{
			j.at( "texCoord" ).get_to( r.texCoordIndex );
		}
		if ( j.find( "scale" ) != j.end() )
		{
			j.at( "scale" ).get_to( r.scale );
		}
	}

	void to_json( json& j, const NormalTextureRef& r )
	{
		j[ "index" ] = r.texture->index;
		if ( r.texCoordIndex != 0 )
		{
			j[ "texCoord" ] = r.texCoordIndex;
		}
		if ( r.scale != 1 )
		{
			j[ "scale" ] = r.scale;
		}
	}

	void from_json( const json& j, OcclusionTextureRef& r )
	{
		size_t idx;
		j.at( "index" ).get_to( idx );
		r.texture.set( &s_currentGltf->textures, idx );
		if ( j.find( "texCoord" ) != j.end() )
		{
			j.at( "texCoord" ).get_to( r.texCoordIndex );
		}
		if ( j.find( "strength" ) != j.end() )
		{
			j.at( "strength" ).get_to( r.strength );
		}
	}

	void to_json( json& j, const OcclusionTextureRef& r )
	{
		j[ "index" ] = r.texture->index;
		if ( r.texCoordIndex != 0 )
		{
			j[ "texCoord" ] = r.texCoordIndex;
		}
		if ( r.strength != 1 )
		{
			j[ "strength" ] = r.strength;
		}
	}

	void from_json( const json& j, Material& m )
	{
		if ( j.find( "name" ) != j.end() )
		{
			j.at( "name" ).get_to( m.name );
		}

		if ( j.find( "normalTexture" ) != j.end() )
		{
			j.at( "normalTexture" ).get_to( m.normalTexture );
		}
		if ( j.find( "emissiveTexture" ) != j.end() )
		{
			j.at( "emissiveTexture" ).get_to( m.emissiveTexture );
		}
		if ( j.find( "emissiveFactor" ) != j.end() )
		{
			j.at( "emissiveFactor" ).get_to( m.emissiveFactor );
		}
		if ( j.find( "alphaMode" ) != j.end() )
		{
			std::string am;
			j.at( "alphaMode" ).get_to( am );
			if ( am == "OPAQUE" )
				m.alphaMode = Material::OPAQUE;
			else if ( am == "MASK" )
				m.alphaMode = Material::MASK;
			else if ( am == "BLEND" )
				m.alphaMode = Material::BLEND;
		}
		if ( j.find( "alphaCutoff" ) != j.end() )
		{
			j.at( "alphaCutoff" ).get_to( m.alphaCutoff );
		}
		if ( j.find( "doubleSided" ) != j.end() )
		{
			j.at( "doubleSided" ).get_to( m.doubleSided );
		}
		if ( j.find( "pbrMetallicRoughness" ) != j.end() )
		{
			const json& k = j.at( "pbrMetallicRoughness" );
			if ( k.find( "baseColorFactor" ) != k.end() )
			{
				k.at( "baseColorFactor" ).get_to( m.pbrMetallicRoughness.baseColorFactor );
			}
			if ( k.find( "baseColorTexture" ) != k.end() )
			{
				k.at( "baseColorTexture" ).get_to( m.pbrMetallicRoughness.baseColorTexture );
			}
			if ( k.find( "metallicFactor" ) != k.end() )
			{
				k.at( "metallicFactor" ).get_to( m.pbrMetallicRoughness.metallicFactor );
			}
			if ( k.find( "roughnessFactor" ) != k.end() )
			{
				k.at( "roughnessFactor" ).get_to( m.pbrMetallicRoughness.roughnessFactor );
			}
			if ( k.find( "metallicRoughnessTexture" ) != k.end() )
			{
				k.at( "metallicRoughnessTexture" ).get_to( m.pbrMetallicRoughness.metallicRoughnessTexture );
			}
		}
	}

	void to_json( json& j, const Material& m )
	{
		if ( !m.name.empty() )
		{
			j[ "name" ] = m.name;
		}
		if ( m.normalTexture.texture )
		{
			j[ "normalTexture" ] = m.normalTexture;
		}
		if ( m.emissiveTexture.texture )
		{
			j[ "emissiveTexture" ] = m.emissiveTexture;
		}
		if ( m.emissiveFactor.length() > 0 )
		{
			j[ "emissiveFactor" ] = m.emissiveFactor;
		}
		if ( m.alphaCutoff != 0.5 )
		{
			j[ "alphaCutoff" ] = m.alphaCutoff;
		}
		switch ( m.alphaMode )
		{
			case Material::BLEND:
				j[ "alphaMode" ] = "BLEND";
				break;
			case Material::MASK:
				j[ "alphaMode" ] = "MASK";
				break;
			default:
				break;
		}
		if ( m.doubleSided )
		{
			j[ "doubleSided" ] = m.doubleSided;
		}

		json pbr;
		if ( m.pbrMetallicRoughness.baseColorFactor != glm::vec4( 1, 1, 1, 1 ) )
		{
			pbr[ "baseColorFactor" ] = m.pbrMetallicRoughness.baseColorFactor;
		}
		if ( m.pbrMetallicRoughness.baseColorTexture.texture )
		{
			pbr[ "baseColorTexture" ] = m.pbrMetallicRoughness.baseColorTexture;
		}
		if ( m.pbrMetallicRoughness.metallicFactor != 1 )
		{
			pbr[ "metallicFactor" ] = m.pbrMetallicRoughness.metallicFactor;
		}
		if ( m.pbrMetallicRoughness.roughnessFactor != 1 )
		{
			pbr[ "roughnessFactor" ] = m.pbrMetallicRoughness.roughnessFactor;
		}
		if ( m.pbrMetallicRoughness.metallicRoughnessTexture.texture )
		{
			pbr[ "metallicRoughnessTexture" ] = m.pbrMetallicRoughness.metallicRoughnessTexture;
		}
		if ( !pbr.empty() )
		{
			j[ "pbrMetallicRoughness" ] = pbr;
		}
	}

	void from_json( const json& j, Primitive& p )
	{
		std::map<std::string, size_t> attribs;
		j.at( "attributes" ).get_to( attribs );
		for ( const auto& att : attribs )
		{
			p.attributes[att.first] = &s_currentGltf->accessors[att.second];
		}

		size_t idx;
		if ( j.find( "indices" ) != j.end() )
		{
			j.at( "indices" ).get_to( idx );
			p.indices = &s_currentGltf->accessors[idx];
		}
		if ( j.find( "material" ) != j.end() )
		{
			j.at( "material" ).get_to( idx );
			p.material = &s_currentGltf->materials[idx];
		}
		if ( j.find( "mode" ) != j.end() )
		{
			int mode;
			j.at( "mode" ).get_to( mode );
			p.mode = Primitive::Mode( mode );
		}
		if ( j.find( "targets" ) != j.end() )
		{
			DebugError( "Morph targets are not supported" );
		}
	}

	void to_json( json& j, const Primitive& p )
	{
		std::map<std::string, size_t> attribs;
		for ( const auto& a : p.attributes )
		{
			attribs[a.first] = a.second->index;
		}
		j[ "attributes" ] = attribs;

		if ( p.indices )
		{
			j[ "indices" ] = p.indices->index;
		}
		if ( p.material )
		{
			j[ "material" ] = p.material->index;
		}
		if ( p.mode != Primitive::Mode::TRIANGLES )
		{
			j[ "primitive" ] = (int)p.mode;
		}
	}

	void from_json( const json& j, Mesh& m )
	{
		if ( j.find( "name" ) != j.end() )
		{
			j.at( "name" ).get_to( m.name );
		}
		j.at( "primitives" ).get_to( m.primitives );
	}

	void to_json( json& j, const Mesh& m )
	{
		if ( !m.name.empty() )
		{
			j[ "name" ] = m.name;
		}
		j[ "primitives" ] = m.primitives;
	}

	void from_json( const json& j, Camera& c )
	{
		std::string type;
		j.at( "type" ).get_to( type );
		if ( type == "orthographic" )
		{
			c.type = Camera::ORTHOGRAPHIC;
			const json& p = j.at( "orthographic" );
			p.at( "xmag" ).get_to( c.orthographic.xmag );
			p.at( "ymag" ).get_to( c.orthographic.ymag );
			p.at( "znear" ).get_to( c.orthographic.znear );
			p.at( "zfar" ).get_to( c.orthographic.zfar );
		}
		else if ( type == "perspective" )
		{
			c.type = Camera::PERSPECTIVE;
			const json& p = j.at( "perspective" );
			p.at( "yfov" ).get_to( c.perspective.yfov );
			p.at( "znear" ).get_to( c.perspective.znear );
			if ( p.find( "aspectRatio" ) != p.end() )
			{
				p.at( "aspectRatio" ).get_to( c.perspective.aspectRatio );
			}
			else
			{
				c.perspective.aspectRatio = 1;
			}

			if ( p.find( "zfar" ) != p.end() )
			{
				p.at( "zfar" ).get_to( c.perspective.zfar );
			}
		}
		else
		{
			DebugError( "Unsupported camera type" );
		}

		if ( j.find( "name" ) != j.end() )
		{
			j.at( "name" ).get_to( c.name );
		}
	}

	void to_json( json& j, const Camera& c )
	{
		if ( !c.name.empty() )
		{
			j[ "name" ] = c.name;
		}
		if ( c.type == Camera::PERSPECTIVE )
		{
			json p;
			p[ "yfov" ] = c.perspective.yfov;
			if ( c.perspective.zfar < std::numeric_limits<float>::infinity() )
			{
				p[ "zfar" ] = c.perspective.zfar;
			}
			p[ "znear" ] = c.perspective.znear;
			j[ "type" ] = "perspective";
			j[ "perspective" ] = p;
		}
		else if ( c.type == Camera::ORTHOGRAPHIC )
		{
			json ortho;
			ortho[ "xmag" ] = c.orthographic.xmag;
			ortho[ "ymag" ] = c.orthographic.ymag;
			ortho[ "zfar" ] = c.orthographic.zfar;
			ortho[ "znear" ] = c.orthographic.znear;
			j[ "type" ] = "orthographic";
			j[ "orthographic" ] = ortho;
		}
	}

	void from_json( const json& j, KhrLight& l )
	{
		std::string type;
		j.at( "type" ).get_to( type );
		if ( type == "directional" )
		{
			l.type = KhrLight::Directional;
		}
		else if ( type == "point" )
		{
			l.type = KhrLight::Point;
		}
		else if ( type == "spot" )
		{
			l.type = KhrLight::Spot;
		}
		else
		{
			DebugError( "Unsupported light type" );
		}

		if ( j.find( "name" ) != j.end() )
		{
			j.at( "name" ).get_to( l.name );
		}
		if ( j.find( "color" ) != j.end() )
		{
			j.at( "color" ).get_to( l.color );
		}
		if ( j.find( "intensity" ) != j.end() )
		{
			j.at( "intensity" ).get_to( l.intensity );
		}
		if ( j.find( "range" ) != j.end() )
		{
			j.at( "range" ).get_to( l.range );
		}

		if ( l.type == KhrLight::Spot )
		{
			if ( j.find( "innerConeAngle" ) != j.end() )
			{
				j.at( "innerConeAngle" ).get_to( l.spotlight.innerConeAngle );
			}
			if ( j.find( "outerConeAngle" ) != j.end() )
			{
				j.at( "outerConeAngle" ).get_to( l.spotlight.outerConeAngle );
			}
		}
	}

	void to_json( json& j, const KhrLight& l )
	{
		if ( !l.name.empty() )
		{
			j[ "name" ] = l.name;
		}
		j[ "color" ] = l.color;
		j[ "intensity" ] = l.intensity;

		switch ( l.type )
		{
			case KhrLight::Point:
				j[ "type" ] = "point";
				break;
			case KhrLight::Spot:
				{
					j[ "type" ] = "spot";
					json spot;
					if ( l.spotlight.innerConeAngle > 0 )
					{
						spot[ "innerConeAngle" ] = l.spotlight.innerConeAngle;
					}
					if ( l.spotlight.outerConeAngle != glm::pi<float>() / 4 )
					{
						spot[ "outerConeAngle" ] = l.spotlight.outerConeAngle;
					}
					if ( !spot.empty() ) j[ "spot" ] = spot;
				}
				break;
			case KhrLight::Directional:
				j[ "type" ] = "directional";
				break;
		}

		if ( l.range != std::numeric_limits<float>::infinity() )
		{
			j[ "range" ] = l.range;
		}
	}

	void from_json( const json& j, Node& n )
	{
		if ( j.find( "children" ) != j.end() )
		{
			// WARNING: We have pre-initialized the `nodes` list to make sure they are not moved
			n.children.reserve( j.at( "children" ).size() );
			for ( size_t i : j.at( "children" ) )
			{
				n.children.push_back( { &s_currentGltf->nodes, i } );
			}
		}

		if ( j.find( "name" ) != j.end() )
		{
			j.at( "name" ).get_to( n.name );
		}

		if ( j.find( "matrix" ) != j.end() )
		{
			j.at( "matrix" ).get_to( n.matrix );
			n.transform_type = Node::MATRIX;
		}
		else
		{
			n.transform_type = Node::TRS;
			n.m.rotation = glm::quat();
			n.m.scale = glm::vec3( 1 );
			n.m.translation = glm::vec3( 0 );

			if ( j.find( "rotation" ) != j.end() )
			{
				j.at( "rotation" ).get_to( n.m.rotation );
			}
			if ( j.find( "scale" ) != j.end() )
			{
				j.at( "scale" ).get_to( n.m.scale );
			}
			if ( j.find( "translation" ) != j.end() )
			{
				j.at( "translation" ).get_to( n.m.translation );
			}
		}

		size_t idx;
		if ( j.find( "mesh" ) != j.end() )
		{
			j.at( "mesh" ).get_to( idx );
			n.mesh = &s_currentGltf->meshes[idx];
		}
		if ( j.find( "camera" ) != j.end() )
		{
			j.at( "camera" ).get_to( idx );
			n.camera = &s_currentGltf->cameras[idx];
		}

		if ( j.find( "extensions" ) != j.end() && j.at( "extensions" ).find( "KHR_lights_punctual" ) != j.at( "extensions" ).end()
		     && j.at( "extensions" ).at( "KHR_lights_punctual" ).find( "light" )
		                != j.at( "extensions" ).at( "KHR_lights_punctual" ).end() )
		{
			j.at( "extensions" ).at( "KHR_lights_punctual" ).at( "light" ).get_to( idx );
			n.khr_light = &s_currentGltf->khrLights[idx];
		}
	}

	void to_json( json& j, const Node& n )
	{
		if ( !n.name.empty() )
		{
			j[ "name" ] = n.name;
		}
		if ( n.transform_type == Node::MATRIX )
		{
			j[ "matrix" ] = n.matrix;
		}
		else if ( n.transform_type == Node::TRS )
		{
			j[ "rotation" ] = n.m.rotation;
			j[ "scale" ] = n.m.scale;
			j[ "translation" ] = n.m.translation;
		}
		else
		{
			DebugError( "Transform type not implemented" );
		}

		if ( !n.children.empty() )
		{
			std::vector<size_t> children;
			for ( size_t i = 0; i < n.children.size(); ++i )
			{
				children.push_back( n.children[i]->index );
			}
			j[ "children" ] = children;
		}

		if ( n.camera )
		{
			j[ "camera" ] = n.camera->index;
		}
		if ( n.mesh )
		{
			j[ "mesh" ] = n.mesh->index;
		}
		if ( n.khr_light )
		{
			j["extensions"] = json{ {"KHR_lights_punctual", json{ {"light", n.khr_light->index} } } };
		}
	}

	void from_json( const json& j, Scene& s )
	{
		if ( j.find( "name" ) != j.end() )
		{
			j.at( "name" ).get_to( s.name );
		}
		if ( j.find( "nodes" ) != j.end() )
		{
			s.nodes.reserve( j.at( "nodes" ).size() );
			for ( size_t i : j.at( "nodes" ) )
			{
				s.nodes.push_back( { &s_currentGltf->nodes, i } );
			}
		}
		if ( j.find( "extras" ) != j.end() )
		{
			const json& e = j.at( "extras" );
			if ( e.find( "unitMultiplier" ) != e.end() )
			{
				e.at( "unitMultiplier" ).get_to( s.extra.unitMultiplier );
			}
			if ( e.find( "debugGeometryMultiplier" ) != e.end() )
			{
				e.at( "debugGeometryMultiplier" ).get_to( s.extra.debugGeomMultiplier );
			}
			if ( e.find( "environmentColor" ) != e.end() )
			{
				e.at( "environmentColor" ).get_to( s.extra.environmentColor );
			}
			if ( e.find( "environmentMultiplier" ) != e.end() )
			{
				e.at( "environmentMultiplier" ).get_to( s.extra.environmentMultiplier );
			}
			if ( e.find( "environmentMap" ) != e.end() )
			{
				e.at( "environmentMap" ).get_to( s.extra.environmentMap );
			}
			if ( e.find( "lightMap" ) != e.end() )
			{
				e.at( "lightMap" ).get_to( s.extra.lightMap );
			}
		}
	}

	void to_json( json& j, const Scene& s )
	{
		if ( !s.name.empty() )
		{
			j[ "name" ] = s.name;
		}
		std::vector<size_t> nodes;
		for ( const auto& n : s.nodes )
		{
			nodes.push_back( n->index );
		}
		j[ "nodes" ] = nodes;
		
		json e = {};

		if ( s.extra.unitMultiplier != 1 )
		{
			e["unitMultiplier"] = s.extra.unitMultiplier;
		}

		if ( s.extra.debugGeomMultiplier != 1 )
		{
			e["debugGeometryMultiplier"] = s.extra.debugGeomMultiplier;
		}

		if ( s.extra.environmentColor != glm::vec3(-1, -1, -1) )
		{
			e["environmentColor"] = s.extra.environmentColor;
		}

		if ( s.extra.environmentMultiplier != 1 )
		{
			e["environmentMultiplier"] = s.extra.environmentMultiplier;
		}

		if ( s.extra.lightMap.texture )
		{
			e["lightMap"] = s.extra.lightMap;
		}

		if ( s.extra.environmentMap.texture )
		{
			e["environmentMap"] = s.extra.environmentMap;
		}

		if ( !e.empty() )
		{
			j["extras"] = e;
		}
	}

	void from_json( const json& j, GLTF& g )
	{
		s_currentGltf = &g;

		if ( j.find( "buffers" ) != j.end() )
		{
			j.at( "buffers" ).get_to( g.buffers );
			for ( size_t i = 0; i < g.buffers.size(); ++i ) g.buffers[i].index = i;
		}
		if ( j.find( "bufferViews" ) != j.end() )
		{
			j.at( "bufferViews" ).get_to( g.bufferViews );
			for ( size_t i = 0; i < g.bufferViews.size(); ++i ) g.bufferViews[i].index = i;
		}
		if ( j.find( "accessors" ) != j.end() )
		{
			j.at( "accessors" ).get_to( g.accessors );
			for ( size_t i = 0; i < g.accessors.size(); ++i ) g.accessors[i].index = i;
		}
		if ( j.find( "samplers" ) != j.end() )
		{
			j.at( "samplers" ).get_to( g.samplers );
			for ( size_t i = 0; i < g.samplers.size(); ++i ) g.samplers[i].index = i;
		}
		if ( j.find( "images" ) != j.end() )
		{
			j.at( "images" ).get_to( g.images );
			for ( size_t i = 0; i < g.images.size(); ++i ) g.images[i].index = i;
		}
		if ( j.find( "textures" ) != j.end() )
		{
			j.at( "textures" ).get_to( g.textures );
			for ( size_t i = 0; i < g.textures.size(); ++i ) g.textures[i].index = i;
		}
		if ( j.find( "materials" ) != j.end() )
		{
			j.at( "materials" ).get_to( g.materials );
			for ( size_t i = 0; i < g.materials.size(); ++i ) g.materials[i].index = i;
		}
		if ( j.find( "meshes" ) != j.end() )
		{
			j.at( "meshes" ).get_to( g.meshes );
			for ( size_t i = 0; i < g.meshes.size(); ++i ) g.meshes[i].index = i;
		}
		if ( j.find( "cameras" ) != j.end() )
		{
			j.at( "cameras" ).get_to( g.cameras );
			for ( size_t i = 0; i < g.cameras.size(); ++i ) g.cameras[i].index = i;
		}

		if ( j.find( "extensions" ) != j.end() )
		{
			if ( j.at( "extensions" ).find( "KHR_lights_punctual" ) != j.at( "extensions" ).end()
			     && j.at( "extensions" ).at( "KHR_lights_punctual" ).find( "lights" )
			                != j.at( "extensions" ).at( "KHR_lights_punctual" ).end() )
			{
				j.at( "extensions" ).at( "KHR_lights_punctual" ).at( "lights" ).get_to( g.khrLights );
				for ( size_t i = 0; i < g.khrLights.size(); ++i ) g.khrLights[i].index = i;
			}
		}

		if ( j.find( "nodes" ) != j.end() )
		{
			g.nodes.reserve( j.at( "nodes" ).size() );
			j.at( "nodes" ).get_to( g.nodes );
			for ( size_t i = 0; i < g.nodes.size(); ++i ) g.nodes[i].index = i;
		}

		if ( j.find( "scenes" ) != j.end() )
		{
			j.at( "scenes" ).get_to( g.scenes );
			for ( size_t i = 0; i < g.scenes.size(); ++i ) g.scenes[i].index = i;
		}

		if ( j.find( "scene" ) != j.end() )
		{
			size_t idx;
			j.at( "scene" ).get_to( idx );
			g.defaultScene = &g.scenes[idx];
		}

		if ( j.find( "extras" ) != j.end() )
		{
			g.extras = j["extras"];
		}

		s_currentGltf = nullptr;
	}

	void to_json( json& j, const GLTF& g )
	{
		json asset{
			{"generator", "ChagApp glTF 2.0 I/O"},
			{"version", "2.0"}
		};
		std::vector<std::string> extensionsUsed;
		json extensions;

		if ( g.khrLights.size() > 0 )
		{
			extensionsUsed.push_back( "KHR_lights_punctual" );
			extensions[ "KHR_lights_punctual" ] = json{ {"lights", g.khrLights} };
		}

		j = json{
				{"accessors", g.accessors},
				{"asset", asset},
				{"extensions", extensions},
				{"extensionsRequired", extensionsUsed},
				{"extensionsUsed", extensionsUsed},
				{"bufferViews", g.bufferViews},
				{"buffers", g.buffers},
				{"cameras", g.cameras},
				{"images", g.images},
				{"textures", g.textures},
				{"samplers", g.samplers},
				{"meshes", g.meshes},
				{"materials", g.materials},
				{"nodes", g.nodes},
				{"scenes", g.scenes},
				{"scene", g.defaultScene->index},
		};

		if ( !g.extras.empty() )
		{
			j["extras"] = g.extras;
		}
	}

	static std::string getTexturePath( TextureRef& tex, const std::string& referrerFile )
	{
		if ( !tex.texture )
		{
			return "";
		}

		if ( tex.texture->image->bufferView == nullptr )
		{
			const std::string& uri = tex.texture->image->uri;
			if ( uri.find( "data:" ) == std::string::npos )
			{
				// Load from file
				std::string   local_uri = get_local_uri( referrerFile, uri );
				std::ifstream f( local_uri );
				if ( f )
				{
					return local_uri;
				}
				else
				{
					f = std::ifstream( uri );
					if ( f )
					{
						return uri;
					}
				}
			}
		}
		return "";
	}

	static scene::Texture* loadTexture( TextureRef& tex, const std::string& referrerFile )
	{
		const unsigned char* imgData = nullptr;
		size_t               imgDataLength = 0;
		if ( !tex.texture )
		{
			return nullptr;
		}

		scene::Texture* rtex = nullptr;
		if ( tex.texture->image->bufferView == nullptr )
		{
			const std::string& uri = tex.texture->image->uri;
			if ( uri.find( "data:" ) == 0 )
			{
				// Decode the data
				if ( tex.texture->image->decodedUriData.empty() )
				{
					size_t start = uri.find( "," );
					if ( start != std::string::npos )
					{
						start += 1;
						tex.texture->image->decodedUriData = b64decode( uri.data() + start, uri.size() - start );
					}
				}

				if ( !tex.texture->image->decodedUriData.empty() )
				{
					imgData = tex.texture->image->decodedUriData.data();
					imgDataLength = tex.texture->image->decodedUriData.size();
				}
			}
			else
			{
				// Load from file
				std::string local_uri = get_local_uri( referrerFile, uri );
				rtex = scene::Texture::loadFromAnyFileType( local_uri );
				if ( !rtex )
				{
					rtex = scene::Texture::loadFromAnyFileType( uri );
				}
			}
		}
		else
		{
			imgData = tex.texture->image->bufferView->buffer->data.data();
			imgData += tex.texture->image->bufferView->byteOffset;
			imgDataLength = tex.texture->image->bufferView->byteLength;
		}

		if ( !rtex && imgData )
		{
			rtex = new Texture2D_RGBA8;
			if ( !rtex->loadFromMemory( "", imgData, imgDataLength ) )
			{
				delete rtex;
				rtex = nullptr;
			}
		}


		if ( rtex )
		{
			rtex->m_gltfTexId = tex.texture->index;
		}

		// TODO: update sampler with texture information
		return rtex;
	}

}   // namespace gltf

bool loadScene_gltf( Scene* scene, const std::string& filename )
{
	g_progress->push_task( "Loading scene" );
	gltf::Node   node;
	gltf::Camera camera;
	json         file;
	try
	{
		std::ifstream fstream( filename );
		if ( fstream )
		{
			file = json::parse( fstream );
		}
		else
		{
			LOG_ERROR( "Failed to load " << filename << ". File not found!" );
			return false;
		}
	}
	catch ( std::exception& e )
	{
		LOG_ERROR( "Failed to load " << filename );
		DebugError( e.what() );
		g_progress->pop_task();
		return false;
	}

	gltf::GLTF *gp = new gltf::GLTF;
	gltf::GLTF &g = *gp;
	g.filePath = filename;
	try
	{
		file.get_to( g );
	}
	catch ( std::exception& e )
	{
		LOG_ERROR( "Failed to load " << filename );
		DebugError( e.what() );
		delete gp;
		g_progress->pop_task();
		return false;
	}

	if ( scene->m_gltfSceneLoaderData != nullptr )
	{
		gltf::GLTF* g = reinterpret_cast<gltf::GLTF*>(scene->m_gltfSceneLoaderData);
		DebugAssert( g != nullptr );
		delete g;
	}

	scene->m_gltfSceneLoaderData = gp;

	scene->m_filename = filename;

	scene->m_scene_name = std::filesystem::path(filename).stem().string();

	scene->clear();

	if ( g.defaultScene->extra.environmentColor != glm::vec3( -1, -1, -1 ) )
	{
		scene->m_environment_color = g.defaultScene->extra.environmentColor;
	}
	scene->m_envmap_intensity_multiplier = g.defaultScene->extra.environmentMultiplier;
	scene->m_sceneUnitMultiplier = g.defaultScene->extra.unitMultiplier;
	scene->m_sceneDebugGeomSizeMultiplier = g.defaultScene->extra.debugGeomMultiplier;
	if ( g.defaultScene->extra.environmentMap.texture )
	{
		scene->m_environmentMap_path = gltf::getTexturePath( g.defaultScene->extra.environmentMap, filename );
		scene->m_environmentMap = gltf::loadTexture( g.defaultScene->extra.environmentMap, filename );
	}
	if ( g.defaultScene->extra.lightMap.texture )
	{
		scene->m_lightmap = gltf::loadTexture( g.defaultScene->extra.lightMap, filename );
	}

	std::function<void( const gltf::Node*, const glm::mat4& )> rec = [&]( const gltf::Node* n, const glm::mat4& parent ) {
		glm::mat4 curT;
		if ( n->transform_type == gltf::Node::MATRIX )
		{
			curT = n->matrix;
		}
		else
		{
			curT = glm::translate( n->m.translation ) * glm::toMat4( n->m.rotation ) * glm::scale( n->m.scale );
		}
		curT = parent * curT;
		for ( const auto& c : n->children )
		{
			rec( c.get(), curT );
		}

		glm::mat3 rot;
		glm::vec3 tr;
		glm::vec3 sc;
		if ( n->camera || n->mesh || n->khr_light )
		{
			glm::vec3 scale;
			glm::quat orientation;
			glm::vec3 translation;
			glm::vec3 skew;
			glm::vec4 perspective;
			glm::decompose( curT, scale, orientation, translation, skew, perspective );
			if ( fabsf( skew.x ) > 1e-6 || fabsf( skew.y ) > 1e-6 || fabsf( skew.z ) > 1e-6 )
			{
				LOG_ERROR( "Skewed meshes are not supported. This probably is happening because the model has a hierarchy with "
				           "scalings and rotations." );
				LOG_WARNING( "Scaled meshes in a hierarchy might not be scaled as expected. Apply the scaling to your mesh if "
				             "that happens." );
			}
			if ( perspective.x != 0 || perspective.y != 0 || perspective.z != 0 || perspective.w != 1 )
			{
				LOG_ERROR( "Perspective in the MV transform????" );
			}
			rot = glm::transpose( glm::toMat3( orientation ) );
			tr = translation;
			sc = scale;
		}

		if ( n->camera )
		{
			if ( n->camera->type == gltf::Camera::PERSPECTIVE )
			{
				scene::PerspectiveCamera* camera = new scene::PerspectiveCamera;
				camera->m_name = n->camera->name.empty() ? n->name : n->camera->name;
				camera->m_transform.pos = tr;
				camera->m_transform.R = rot;
				camera->m_transform.scale = sc;
				camera->m_far = n->camera->perspective.zfar;
				camera->m_near = n->camera->perspective.znear;
				camera->m_fov = n->camera->perspective.yfov * 180 / glm::pi<float>();
				camera->m_aspect_ratio = n->camera->perspective.aspectRatio;
				scene->m_cameras.push_back( camera );
			}
			else
			{
				LOG_ERROR( "Orthographic camera not supported" );
			}
		}
		else if ( n->khr_light )
		{
			scene::Light* light = new scene::Light;
			light->m_name = n->khr_light->name.empty() ? n->name : n->khr_light->name;
			light->m_transform.pos = tr;
			light->m_transform.R = rot;
			light->m_transform.scale = sc;
			light->m_color = n->khr_light->color;
			light->m_radiance = n->khr_light->intensity;
			light->m_distribution.type = n->khr_light->type == gltf::KhrLight::Spot ? scene::Light::Distribution::Spot
			                                                                        : scene::Light::Distribution::Diffuse;
			light->m_shape.type = scene::Light::Shape::Point;
			light->m_shape.Disc.m_radius = 1;
			scene->m_lights.push_back( light );
		}
		else if ( n->mesh )
		{
			scene::Model* model = new scene::Model;
			model->m_name = n->name;
			model->m_transform.pos = tr;
			model->m_transform.R = rot;
			model->m_transform.scale = sc;
			for ( const auto& p : n->mesh->primitives )
			{
				DebugAssert( p.mode == gltf::Primitive::TRIANGLES );
				DebugAssert( p.material != nullptr );
				Material* mat = new Material;
				mat->m_name = p.material->name;

				mat->m_base_color_factor = p.material->pbrMetallicRoughness.baseColorFactor;
				mat->m_base_color_texture_path = gltf::getTexturePath( p.material->pbrMetallicRoughness.baseColorTexture, filename );
				mat->m_base_color_texture = gltf::loadTexture( p.material->pbrMetallicRoughness.baseColorTexture, filename );
				mat->m_normal_scale = p.material->normalTexture.scale;
				mat->m_normal_texture_path = gltf::getTexturePath( p.material->normalTexture, filename );
				mat->m_normal_texture = gltf::loadTexture( p.material->normalTexture, filename );
				mat->m_alpha_cutoff = p.material->alphaCutoff;
				mat->m_emissive_factor = p.material->emissiveFactor;
				mat->m_emissive_texture_path = gltf::getTexturePath( p.material->emissiveTexture, filename );
				mat->m_emissive_texture = gltf::loadTexture( p.material->emissiveTexture, filename );
				mat->m_metallic_factor = p.material->pbrMetallicRoughness.metallicFactor;
				mat->m_roughness_factor = p.material->pbrMetallicRoughness.roughnessFactor;
				mat->m_metallic_roughness_texture_path =
				        gltf::getTexturePath( p.material->pbrMetallicRoughness.metallicRoughnessTexture, filename );
				mat->m_metallic_roughness_texture = gltf::loadTexture( p.material->pbrMetallicRoughness.metallicRoughnessTexture, filename );
				model->m_materials[p.material->name] = mat;

				bool hasPosition = false;
				bool hasNormal = false;
				bool hasTangent = false;
				bool hasTex = false;
				bool isIndexed = p.indices != nullptr;

				size_t idxCount = 0;
				size_t idxOffset = model->m_indices.m_host_vector.size();
				if ( isIndexed )
				{
					uint8_t* indexBufferBytes =
							p.indices->bufferView->buffer->data.data() + p.indices->bufferView->byteOffset + p.indices->byteOffset;

					DebugAssert( p.indices->type == gltf::Accessor::DataType::SCALAR );
					DebugAssert( p.indices->componentType == gltf::Accessor::ComponentType::UNSIGNED_SHORT
								 || p.indices->componentType == gltf::Accessor::ComponentType::UNSIGNED_BYTE
								 || p.indices->componentType == gltf::Accessor::ComponentType::UNSIGNED_INT );
					model->m_indices.m_host_vector.reserve( p.indices->count + model->m_indices.m_host_vector.size() );

					idxCount = p.indices->count;

					size_t idx_start = model->m_indices.m_host_vector.size();
					uint32_t idx_offset = (uint32_t)model->m_positions.m_host_vector.size();

					if ( p.indices->componentType == gltf::Accessor::ComponentType::UNSIGNED_SHORT )
					{
						for ( size_t i = 0; i < p.indices->count; ++i )
						{
							model->m_indices.m_host_vector.push_back( reinterpret_cast<uint16_t*>(indexBufferBytes)[i] );
						}
					}
					else if ( p.indices->componentType == gltf::Accessor::ComponentType::UNSIGNED_INT )
					{
						for ( size_t i = 0; i < p.indices->count; ++i )
						{
							model->m_indices.m_host_vector.push_back( reinterpret_cast<uint32_t*>(indexBufferBytes)[i] );
						}
					}
					else if ( p.indices->componentType == gltf::Accessor::ComponentType::UNSIGNED_BYTE )
					{
						for ( size_t i = 0; i < p.indices->count; ++i )
						{
							model->m_indices.m_host_vector.push_back( indexBufferBytes[i] );
						}
					}
					for ( size_t i = idx_start; i < model->m_indices.m_host_vector.size(); ++i )
					{
						model->m_indices.m_host_vector[i] += idx_offset;
					}
				}

#define EXTRACT_VERTEX_INFORMATION( _container, _type )                                                                     \
	do                                                                                                                      \
	{                                                                                                                       \
		const uint8_t* data = a.second->bufferView->buffer->data.data();                                                    \
		data += a.second->bufferView->byteOffset + a.second->byteOffset;                                                    \
		size_t stride = a.second->bufferView->byteStride;                                                                   \
		size_t attribSize = gltf::Accessor::getSize( a.second->componentType ) * gltf::Accessor::getSize( a.second->type ); \
		if ( attribSize > stride )                                                                                          \
		{                                                                                                                   \
			stride = attribSize;                                                                                            \
		}                                                                                                                   \
		_container.reserve( a.second->count + _container.size() );                                                          \
		for ( size_t i = 0; i < a.second->count; ++i )                                                                      \
		{                                                                                                                   \
			const _type* v = (const _type*)( data + i * stride );                                                           \
			_container.push_back( *v );                                                                                     \
		}                                                                                                                   \
	} while ( 0 )
				//////////////////////////////

				size_t vtxCount = 0;
				size_t vtxOffset = model->m_positions.m_host_vector.size();
				for ( const auto& a : p.attributes )
				{
					if ( a.first == "POSITION" )
					{
						DebugAssert( a.second->componentType == gltf::Accessor::ComponentType::FLOAT );
						DebugAssert( a.second->type == gltf::Accessor::DataType::VEC3 );
						EXTRACT_VERTEX_INFORMATION( model->m_positions.m_host_vector, glm::vec3 );
						vtxCount += a.second->count;
						hasPosition = true;
					}
					else if ( a.first == "NORMAL" )
					{
						DebugAssert( a.second->componentType == gltf::Accessor::ComponentType::FLOAT );
						DebugAssert( a.second->type == gltf::Accessor::DataType::VEC3 );
						EXTRACT_VERTEX_INFORMATION( model->m_normals.m_host_vector, glm::vec3 );
						hasNormal = true;
					}
					else if ( a.first == "TANGENT" )
					{
						DebugAssert( a.second->componentType == gltf::Accessor::ComponentType::FLOAT );
						DebugAssert( a.second->type == gltf::Accessor::DataType::VEC3 || a.second->type == gltf::Accessor::DataType::VEC4 );
						EXTRACT_VERTEX_INFORMATION( model->m_tangents.m_host_vector, glm::vec3 );
						hasTangent = true;
					}
					else if ( a.first == "TEXCOORD_0" )
					{
						DebugAssert( a.second->componentType == gltf::Accessor::ComponentType::FLOAT );
						DebugAssert( a.second->type == gltf::Accessor::DataType::VEC2 );
						EXTRACT_VERTEX_INFORMATION( model->m_uvs[0].m_host_vector, glm::vec2 );
						hasTex = true;
					}
					else if ( a.first == "TEXCOORD_1" )
					{
						DebugAssert( a.second->componentType == gltf::Accessor::ComponentType::FLOAT );
						DebugAssert( a.second->type == gltf::Accessor::DataType::VEC2 );
						EXTRACT_VERTEX_INFORMATION( model->m_uvs[1].m_host_vector, glm::vec2 );
					}
				}

				DebugAssert( hasPosition );

				// Create material pointers array
				{
					size_t count = isIndexed ? idxCount : vtxCount;

					model->m_triangleMaterial.reserve( model->m_triangleMaterial.size() + count / 3 );
					for ( size_t i = 0; i < count / 3; ++i )
					{
						model->m_triangleMaterial.push_back( mat );
					}
				}

				// Add primitive info
				{
					scene::Model::Primitive prim;
					prim.material = mat;
					prim.vbo_offset = GLsizei( isIndexed ? idxOffset : vtxOffset );
					prim.vbo_count = GLsizei( isIndexed ? idxCount : vtxCount );
					prim.gltf_prim_id = model->m_primitives.size();
					model->m_primitives.push_back(prim);
				}

				if ( !hasNormal )
				{
					model->m_normals.m_host_vector.reserve( model->m_positions.m_host_vector.size() );
					for ( size_t i = model->m_normals.m_host_vector.size(); i < model->m_positions.m_host_vector.size(); i += 3 )
					{
						glm::vec3* vs = &( model->m_positions.m_host_vector[i] );
						glm::vec3  v0 = glm::normalize( vs[0] - vs[1] );
						glm::vec3  v1 = glm::normalize( vs[2] - vs[1] );
						glm::vec3  normal = glm::normalize( glm::cross( v1, v0 ) );
						for ( size_t j = 0; j < 3; ++j )
						{
							model->m_normals.m_host_vector.push_back( normal );
						}
					}
				}
				if ( !hasTangent )
				{
					///////////////////////////////////////////////////////////
					// If the model is not exported with tangents, we will try
					// to create proper tangents with respect to the UV0 
					// mapping. If there are no UV coords, just create a
					// tangent vector orthogonal to the normal. 
					// 
					// WARNING: It is not uncommon that an incoming model _has_ 
					//			UV0's but they are crap. 
					//
					// Proper tangent calculation code stolen from: 
					// https://gamedev.stackexchange.com/questions/68612/how-to-compute-tangent-and-bitangent-vectors
					///////////////////////////////////////////////////////////
					int num_vertices = model->m_normals.m_host_vector.size();
					model->m_tangents.m_host_vector.reserve(num_vertices );
					if (!hasTex) {
						for (size_t i = model->m_tangents.m_host_vector.size(); i < model->m_normals.m_host_vector.size(); ++i)
						{
							model->m_tangents.m_host_vector.push_back(glm::perp(model->m_normals.m_host_vector[i]));
						}
					}
					else {
						model->m_tangents.m_host_vector.resize(num_vertices);
						vector<glm::vec3> tan1( model->m_positions.m_host_vector.size(), { 0.0f, 0.0f, 0.0f } );
						vector<glm::vec3> tan2( model->m_positions.m_host_vector.size(), { 0.0f, 0.0f, 0.0f } );
						// Loop over triangles
						const bool has_indices = model->m_indices.m_host_vector.size() > 0;
						const int num_triangles = has_indices ? model->m_indices.m_host_vector.size() / 3
						                                      : model->m_positions.m_host_vector.size() / 3; 
						for (int i = 0; i < num_triangles; i++)
						{
							glm::vec3 v1, v2, v3;
							glm::vec2 w1, w2, w3; 
							int i1 = i * 3 + 0; 
							int i2 = i * 3 + 1;
							int i3 = i * 3 + 2; 
							if (has_indices) {
								i1 = model->m_indices.m_host_vector[i * 3 + 0];
								i2 = model->m_indices.m_host_vector[i * 3 + 1];
								i3 = model->m_indices.m_host_vector[i * 3 + 2];
							}
							v1 = model->m_positions.m_host_vector[i1];
							v2 = model->m_positions.m_host_vector[i2];
							v3 = model->m_positions.m_host_vector[i3];
							w1 = model->m_uvs[0].m_host_vector[i1];
							w2 = model->m_uvs[0].m_host_vector[i2];
							w3 = model->m_uvs[0].m_host_vector[i3];

							float x1 = v2.x - v1.x;
							float x2 = v3.x - v1.x;
							float y1 = v2.y - v1.y;
							float y2 = v3.y - v1.y;
							float z1 = v2.z - v1.z;
							float z2 = v3.z - v1.z;

							float s1 = w2.x - w1.x;
							float s2 = w3.x - w1.x;
							float t1 = w2.y - w1.y;
							float t2 = w3.y - w1.y;

							float r = 1.0F / ( s1 * t2 - s2 * t1 );
							glm::vec3 sdir( ( t2 * x1 - t1 * x2 ) * r, ( t2 * y1 - t1 * y2 ) * r, ( t2 * z1 - t1 * z2 ) * r );
							glm::vec3 tdir( ( s1 * x2 - s2 * x1 ) * r, ( s1 * y2 - s2 * y1 ) * r, ( s1 * z2 - s2 * z1 ) * r );

							tan1[i1] += sdir;
							tan1[i2] += sdir;
							tan1[i3] += sdir;

							tan2[i1] += tdir;
							tan2[i2] += tdir;
							tan2[i3] += tdir;
						}

						for (int a = 0; a < num_vertices; a++)
						{
							const glm::vec3 & n = model->m_normals.m_host_vector[a];
							const glm::vec3 & t = tan1[a];
							// Gram-Schmidt orthogonalize
							glm::vec3 tangent = normalize(t - n * glm::dot(n, t));
							// Calculate handedness
							// TODO: Don't know what this should do!
							//tangent[a].w = (Dot(Cross(n, t), tan2[a]) < 0.0F) ? -1.0F : 1.0F;
							//if (dot(cross(n, t), tan2[a]) < 0.0f) model->m_tangents.m_host_vector[a] = glm::vec3(0.0f); 

							// Sanity check that we did not get crazy (probably nan) tangents
							if (isnan(tangent.x) || isnan(tangent.y) || isnan(tangent.z) || abs(dot(tangent, n)) > 0.1f)
							{
								static int num_warnings = 0; 
								if (num_warnings++ < 10) {
									LOG_WARNING("Created broken tangent from UV0s!" << ((num_warnings == 10) ? " (supressing further warnings)":""));
								}
							}
							
							model->m_tangents.m_host_vector[a] = tangent; 
						}

					}
				}
				if ( !hasTex )
				{
					model->m_uvs[0].m_host_vector.reserve( model->m_positions.m_host_vector.size() );
					for ( size_t i = model->m_uvs[0].m_host_vector.size(); i < model->m_positions.m_host_vector.size(); ++i )
					{
						model->m_uvs[0].m_host_vector.push_back( glm::vec2( 0, 0 ) );
					}
				}
			}
			model->m_gltfMeshId = n->mesh->index;
			model->uploadRenderDataToGPU();
			scene->m_models.push_back( model );
		}
	};

	glm::mat4 root( 1 );
	for ( const auto& s : g.scenes )
	{
		for ( const auto& n : s.nodes )
		{
			rec( n.get(), root );
		}
	}

	// Create default camera if none is present
	if ( scene->m_cameras.size() == 0 )
	{
		scene::PerspectiveCamera* camera = new scene::PerspectiveCamera;
		camera->m_name = "default";
		camera->m_transform.pos = glm::vec3();
		camera->m_transform.R = glm::mat3();
		camera->m_transform.scale = glm::vec3(1);
		camera->m_far = 1000;
		camera->m_near = 0.1;
		camera->m_fov = 45;
		camera->m_aspect_ratio = 1;
		scene->m_cameras.push_back( camera );
	}

	scene->m_extra = g.extras;

	g_progress->pop_task();
	return true;
}


bool saveScene_gltf( Scene* scene, const std::string& filename )
{
	gltf::GLTF *gorig = reinterpret_cast<gltf::GLTF*>(scene->m_gltfSceneLoaderData);
	DebugAssert( gorig != nullptr );

	gltf::GLTF g;

	g.filePath = filename;

	// Let's copy all the low level data from the original gltf file
	// TODO: optimize this to only copy the data that's actually used, in case we have deleted things?
	// TODO: add new buffers and accessors for new geometry???
	g.buffers = gorig->buffers;
	g.bufferViews.reserve( gorig->bufferViews.size() );
	for ( auto b : gorig->bufferViews )
	{
		b.buffer = &g.buffers[b.buffer->index];
		g.bufferViews.push_back( b );
	}
	g.accessors.reserve( gorig->accessors.size() );
	for ( auto a : gorig->accessors )
	{
		a.bufferView = &g.bufferViews[a.bufferView->index];
		g.accessors.push_back( a );
	}

	g.samplers = gorig->samplers;
	
	auto addImage = [&]( scene::Texture* tex )
	{
		gltf::Image i;
		i.uri = gltf::get_relative_path( g.filePath, tex->m_filename );
		i.index = g.images.size();
		g.images.push_back( i );

		gltf::Texture t;
		t.image.set( &g.images, i.index );
		t.sampler.set( &g.samplers, 0 );
		t.index = g.textures.size();
		g.textures.push_back( t );

		tex->m_gltfTexId = t.index;
	};

	for ( const auto& m : scene->m_models )
	{
		gltf::Mesh* meshorig = &gorig->meshes[m->m_gltfMeshId];
		for ( const auto& p : m->m_primitives )
		{
			gltf::Primitive& porig = meshorig->primitives[p.gltf_prim_id];
			gltf::Material& mat = *porig.material;

			if ( p.material->m_emissive_texture )
			{
				addImage( p.material->m_emissive_texture );
			}
			if ( p.material->m_normal_texture )
			{
				addImage( p.material->m_normal_texture );
			}
			if ( p.material->m_base_color_texture )
			{
				addImage( p.material->m_base_color_texture );
			}
			if ( p.material->m_metallic_roughness_texture )
			{
				addImage( p.material->m_metallic_roughness_texture );
			}
		}
	}

	// ATTENTION! make sure to reserve enough space for all the nodes so they won't be reallocated
	g.nodes.reserve( scene->m_cameras.size() + scene->m_lights.size() + scene->m_models.size() );

	g.scenes.push_back( {} );
	g.defaultScene = &g.scenes.front();
	g.defaultScene->index = 0;
	g.defaultScene->nodes.reserve( g.nodes.size() );
	if ( scene->m_environmentMap )
	{
		addImage( scene->m_environmentMap );
		g.defaultScene->extra.environmentMap.texture.set( &g.textures, scene->m_environmentMap->m_gltfTexId );
	}
	else
	{
		g.defaultScene->extra.environmentMap.texture.unset();
	}

	if ( scene->m_lightmap && !scene->m_lightmap->m_filename.empty() )
	{
		addImage( scene->m_lightmap );
		g.defaultScene->extra.lightMap.texture.set( &g.textures, scene->m_lightmap->m_gltfTexId );
	}
	else
	{
		g.defaultScene->extra.lightMap.texture.unset();
	}

	g.defaultScene->extra.environmentColor = scene->m_environment_color;
	g.defaultScene->extra.environmentMultiplier = scene->m_envmap_intensity_multiplier;
	g.defaultScene->extra.unitMultiplier = scene->m_sceneUnitMultiplier;
	g.defaultScene->extra.debugGeomMultiplier = scene->m_sceneDebugGeomSizeMultiplier;

	// Export Cameras
	g.cameras.reserve( scene->m_cameras.size() );
	for ( const auto& c : scene->m_cameras )
	{
		if ( const scene::PerspectiveCamera* cam = dynamic_cast<const scene::PerspectiveCamera*>(c) )
		{
			gltf::Camera gc;
			gc.name = cam->m_name;
			gc.type = gltf::Camera::PERSPECTIVE;
			gc.perspective.yfov = cam->m_fov * glm::pi<float>() / 180.f;
			gc.perspective.znear = cam->m_near;
			gc.perspective.zfar = cam->m_far;
			gc.perspective.aspectRatio = cam->m_aspect_ratio;
			g.cameras.push_back(gc);

			gltf::Node n;
			n.camera = &g.cameras.back();
			n.name = cam->m_name;
			n.transform_type = gltf::Node::TRS;
			n.m.translation = cam->m_transform.pos;
			n.m.rotation = cam->m_transform.R;
			n.m.scale = cam->m_transform.scale;
			g.nodes.push_back( n );

			g.defaultScene->nodes.push_back( { &g.nodes, g.nodes.size()-1 } );
		}
	}
	for ( size_t i = 0; i < g.cameras.size(); ++i )
	{
		g.cameras[i].index = i;
	}

	// Export KHR lights
	g.khrLights.reserve( scene->m_lights.size() );
	for ( const auto& l : scene->m_lights )
	{
		if ( l->m_shape.type == scene::Light::Shape::Point )
		{
			gltf::KhrLight light;
			light.name = l->m_name;
			light.type = (l->m_distribution.type == scene::Light::Distribution::Spot) ? gltf::KhrLight::Spot : gltf::KhrLight::Point;
			light.intensity = l->m_radiance;
			light.color = l->m_color;
			if ( light.type == gltf::KhrLight::Spot )
			{
				light.spotlight.innerConeAngle = l->m_distribution.Spot.m_attenuation_start;
				light.spotlight.outerConeAngle = l->m_distribution.Spot.m_attenuation_end;
			}
			g.khrLights.push_back( light );

			gltf::Node n;
			n.khr_light = &g.khrLights.back();
			n.name = l->m_name;
			n.transform_type = gltf::Node::TRS;
			n.m.translation = l->m_transform.pos;
			n.m.rotation = l->m_transform.R;
			n.m.scale = l->m_transform.scale;
			g.nodes.push_back( n );

			g.defaultScene->nodes.push_back( { &g.nodes, g.nodes.size()-1 } );
		}
		else
		{
			DebugError("GLTF doesn't support non-point lights (yet)");
		}
	}
	for ( size_t i = 0; i < g.khrLights.size(); ++i )
	{
		g.khrLights[i].index = i;
	}

	// Reserve space for materials
	size_t nmats = 0;
	for ( const auto& m : scene->m_models )
	{
		nmats += m->m_materials.size();
	}
	g.materials.reserve( nmats );

	// Export models
	g.meshes.reserve( scene->m_models.size() );
	for ( const auto& m : scene->m_models )
	{
		gltf::Mesh mesh;
		gltf::Mesh* meshorig = &gorig->meshes[m->m_gltfMeshId];
		mesh.name = m->m_name;
		mesh.primitives.reserve( m->m_primitives.size() );

		for ( const auto& p : m->m_primitives )
		{
			gltf::Primitive prim;
			gltf::Primitive& porig = meshorig->primitives[p.gltf_prim_id];

			prim.mode = porig.mode;

			prim.indices = &g.accessors[porig.indices->index];
			for ( const auto& attr : porig.attributes )
			{
				prim.attributes[attr.first] = &g.accessors[attr.second->index];
			}

			gltf::Material mat;
			mat = *porig.material;
			if ( p.material->m_emissive_texture )
			{
				mat.emissiveTexture.texture.set( &g.textures, p.material->m_emissive_texture->m_gltfTexId );
			}
			else
			{
				mat.emissiveTexture.texture.unset();
			}

			if ( p.material->m_normal_texture )
			{
				mat.normalTexture.texture.set( &g.textures, p.material->m_normal_texture->m_gltfTexId );
			}
			else
			{
				mat.normalTexture.texture.unset();
			}

			if ( p.material->m_base_color_texture )
			{
				mat.pbrMetallicRoughness.baseColorTexture.texture.set( &g.textures, p.material->m_base_color_texture->m_gltfTexId );
			}
			else
			{
				mat.pbrMetallicRoughness.baseColorTexture.texture.unset();
			}

			if ( p.material->m_metallic_roughness_texture )
			{
				mat.pbrMetallicRoughness.metallicRoughnessTexture.texture.set(
				        &g.textures, p.material->m_metallic_roughness_texture->m_gltfTexId );
			}
			else
			{
				mat.pbrMetallicRoughness.metallicRoughnessTexture.texture.unset();
			}
			mat.alphaCutoff = p.material->m_alpha_cutoff;
			mat.emissiveFactor = p.material->m_emissive_factor;
			mat.pbrMetallicRoughness.baseColorFactor = p.material->m_base_color_factor;
			mat.pbrMetallicRoughness.roughnessFactor = p.material->m_roughness_factor;
			mat.pbrMetallicRoughness.metallicFactor = p.material->m_metallic_factor;

			g.materials.push_back(mat);
			prim.material = &g.materials.back();

			mesh.primitives.push_back( prim );
		}
		g.meshes.push_back( mesh );

		gltf::Node n;
		n.mesh = &g.meshes.back();
		n.name = m->m_name;
		n.transform_type = gltf::Node::TRS;
		n.m.translation = m->m_transform.pos;
		n.m.rotation = m->m_transform.R;
		n.m.scale = m->m_transform.scale;
		g.nodes.push_back( n );

		g.defaultScene->nodes.push_back( { &g.nodes, g.nodes.size()-1 } );
	}
	for ( size_t i = 0; i < g.meshes.size(); ++i )
	{
		g.meshes[i].index = i;
	}
	for ( size_t i = 0; i < g.materials.size(); ++i )
	{
		g.materials[i].index = i;
	}

	for ( size_t i = 0; i < g.nodes.size(); ++i )
	{
		g.nodes[i].index = i;
	}

	if ( g.textures.size() > 0 && g.samplers.size() == 0 )
	{
		gltf::Sampler s;
		s.index = 0;
		g.samplers.push_back(s);
	}

	g.extras = scene->m_extra;

	json file;
	file = g;
	{
		std::ofstream fstream( filename );
		fstream << file.dump(4);
	}

	return true;
}

void scene_clearGltfData( Scene * scene )
{
	gltf::GLTF *gorig = reinterpret_cast<gltf::GLTF*>(scene->m_gltfSceneLoaderData);
	if ( gorig != nullptr )
	{
		delete gorig;
	}
}

}   // namespace scene
