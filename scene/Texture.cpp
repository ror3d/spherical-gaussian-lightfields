#include "Texture.h"
#ifndef NO_OPENGL_OR_CUDA
#include <GL/glew.h>
#endif
#include <utils/Log.h>
// Should create the implementation elsewhere. Really. 
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "glm/common.hpp"

namespace scene {
	std::map<std::string, std::unique_ptr<Texture>> Texture::s_cache;

	void Texture::copyFrom( Texture * t )
	{
		m_filename = t->m_filename;
		m_width = t->m_width;
		m_height = t->m_height;
		m_GLID = t->m_GLID;
		m_data = t->m_data;
		LOG_VERBOSE("Loaded " << t->m_filename << "(cached)");
	}

	Texture * Texture::loadFromAnyFileType( const std::string & filename )
	{
		if ( stbi_is_hdr( filename.c_str() ) )
		{
			Texture2D_RGBA32F *hdr = new Texture2D_RGBA32F();
			if ( !hdr->loadFromFile( filename ) )
			{
				delete hdr;
				return nullptr;
			}
			else
			{
				return hdr;
			}
		}
		else
		{
			Texture2D_RGBA8 *tex = new Texture2D_RGBA8();
			if ( !tex->loadFromFile( filename ) )
			{
				delete tex;
				return nullptr;
			}
			else
			{
				return tex;
			}
		}
		return nullptr;
	}

	Texture * Texture::getCached( const std::string & name )
	{
		auto it = s_cache.find( name );
		if ( it != s_cache.end() )
		{
			return it->second.get();
		}
		return nullptr;
	}

	void Texture::addToCache( const std::string & name, Texture * tex )
	{
		if ( s_cache.find( name ) != s_cache.end() )
		{
			LOG_WARNING( "Overwriting already loaded texture with name " << name );
			s_cache[name] = nullptr;
		}
		s_cache[name] = std::unique_ptr<Texture>(tex);
	}

	bool Texture2D_RGBA8::loadFromFile(const std::string &filename)
	{
		if(Texture* cached = getCached(filename))
		{
			copyFrom( cached );
		}
		else
		{
			int width, height, components; 

			stbi_set_flip_vertically_on_load( false );
			uint8_t * data = stbi_load(filename.c_str(), &width, &height, &components, 4);
			stbi_set_flip_vertically_on_load( false );
			if (data == NULL) {
				LOG_ERROR("Failed to load " << filename);
				return false;
			}
			bool created = createFromMemory( filename, width, height, Format::F_RGBA, Type::T_UINT8, data );
			stbi_image_free( data );
			if (!created)
			{
				return false;
			}
		    LOG_VERBOSE( "Loaded " << filename );
			addToCache( filename, new Texture2D_RGBA8( *this ) );
		}
		return true;
    }

    bool Texture2D_RGBA8::loadFromMemory( const std::string& name, const void* memory, size_t length )
    {
		if(Texture* cached = getCached(name))
		{
			copyFrom( cached );
		}
	    else
	    {
		    int width, height, components;

		    stbi_set_flip_vertically_on_load( false );
		    uint8_t* data = stbi_load_from_memory( (const stbi_uc*)memory, (int)length, &width, &height, &components, 4 );
		    stbi_set_flip_vertically_on_load( false );
		    if ( data == NULL )
		    {
			    LOG_ERROR( "Failed to load " << name );
			    return false;
		    }
			bool created = createFromMemory( name, width, height, Format::F_RGBA, Type::T_UINT8, data );
			stbi_image_free( data );
			if (!created)
			{
				return false;
			}
		    LOG_VERBOSE( "Loaded " << name );
			addToCache( name, new Texture2D_RGBA8( *this ) );
	    }
	    return true;
    }

	bool Texture2D_RGBA8::createFromMemory( const std::string & name, unsigned int width, unsigned int height, Format fmt, Type type, const void * data )
	{
		DebugAssert( fmt == Format::F_RGBA && type == Type::T_UINT8 );
		m_filename = name;
		m_width = width;
		m_height = height;
		m_data.assign((uint8_t*)data, (uint8_t*)data + width * height * sizeof(uint32_t));
		m_format = fmt;
		m_type = type;
#ifndef NO_OPENGL_OR_CUDA
		glGenTextures( 1, &m_GLID );
		glBindTexture( GL_TEXTURE_2D, m_GLID );
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 32.0f);
		glGenerateMipmap( GL_TEXTURE_2D );
#endif
		return true;
	}

	Texture::colorf Texture2D_RGBA8::sample(vec2 uv) const
	{
		int x = (int(uv.x * m_width) % m_width);
		int y = (int(uv.y * m_height) % m_height);
		uint32_t color = m_data[y * m_width + x];
		return colorf((color        & 0xFF) / 255.0f,
				   ((color >> 8)  & 0xFF) / 255.0f,
				   ((color >> 16) & 0xFF) / 255.0f);
	}

	Texture2D_RGBA8::colorf Texture2D_RGBA8::getRawValue( uvec2 uv ) const
	{
		return colorf();
	}

	Texture::colorf Texture2D_RGBA32F::sample( vec2 uv ) const
	{
		using namespace glm;
		uv *= vec2( m_width, m_height );
		ivec2 iuv = glm::round( uv );
		
		iuv = glm::min( iuv - ivec2(1), glm::max( ivec2(0), iuv ) );

		// TODO:
		return colorf();
	}

	Texture2D_RGBA32F::colorf Texture2D_RGBA32F::getRawValue( uvec2 uv ) const
	{
		return glm::vec3(((glm::vec4*)m_data.data())[uv.y * m_width + uv.x]);
		//return reinterpret_cast<const Texture2D_RGBA32F::colorf*>(m_data.data())[uv.x + uv.y * m_width];
	}

	bool Texture2D_RGBA32F::loadFromFile( const std::string & filename )
	{
		if ( Texture* cached = getCached( filename ) )
		{
			copyFrom( cached );
		}
		else
		{
			int width, height, components;

			stbi_set_flip_vertically_on_load( false );
			float * data = stbi_loadf( filename.c_str(), &width, &height, &components, 3 );
			DebugAssert( components == 3 );
			stbi_set_flip_vertically_on_load( false );
			if ( data == NULL )
			{
				LOG_ERROR( "Failed to load " << filename );
				return false;
			}
			bool created = createFromMemory( filename, width, height, Format::F_RGB, Type::T_FLOAT32, data );
			stbi_image_free( data );
			if ( !created )
			{
				return false;
			}
			LOG_VERBOSE( "Loaded " << filename );
			addToCache( filename, new Texture2D_RGBA32F( *this ) );
		}
		return true;
	}

	bool Texture2D_RGBA32F::loadFromMemory( const std::string & name, const void * memory, size_t length )
	{
		if ( Texture* cached = getCached( name ) )
		{
			copyFrom( cached );
		}
		else
		{
			int width, height, components;

			stbi_set_flip_vertically_on_load( false );
			float * data = stbi_loadf_from_memory( (stbi_uc*)memory, length, &width, &height, &components, 3 );
			DebugAssert( components == 3 );
			stbi_set_flip_vertically_on_load( false );
			if ( data == NULL )
			{
				LOG_ERROR( "Failed to load " << name );
				return false;
			}
			bool created = createFromMemory( name, width, height, Format::F_RGB, Type::T_FLOAT32, data );
			stbi_image_free( data );
			if ( !created )
			{
				return false;
			}
			LOG_VERBOSE( "Loaded " << name );
			addToCache( name, new Texture2D_RGBA32F( *this ) );
		}
		return true;
	}

	bool Texture2D_RGBA32F::createFromMemory( const std::string & name, unsigned int width, unsigned int height, Format fmt, Type type, const void * data )
	{
		DebugAssert( type == Type::T_FLOAT32 );
		m_filename = name;
		m_width = width;
		m_height = height;
		m_format = Format::F_RGBA;
		m_type = type;

		m_data.resize( width * height * 4 * sizeof( float ) );
		uint8_t channels = 1;
		if ( fmt == Format::F_RGB )
		{
			channels = 3;
		}
		else if ( fmt == Format::F_YA )
		{
			channels = 2;
		}
		
		for ( size_t j = 0; j < m_height; ++j )
		{
			for ( size_t i = 0; i < m_width; ++i )
			{
				size_t idxr = ((j * m_width) + i) * channels;
				size_t idxw = ((j * m_width) + i) * 4;
				for ( uint8_t c = 0; c < channels; ++c )
				{
					((float*)m_data.data())[idxw + c] = ((float*)data)[idxr+c];
				}
				for ( uint8_t c = channels; c < 4; ++c )
				{
					((float*)m_data.data())[idxw + c] = 1;
				}
			}
		}

#ifndef NO_OPENGL_OR_CUDA

		glGenTextures( 1, &m_GLID );
		glBindTexture( GL_TEXTURE_2D, m_GLID );
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA32F, m_width, m_height, 0, GL_RGBA, GL_FLOAT, m_data.data() );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glGenerateMipmap( GL_TEXTURE_2D );
#endif
		return true;
	}
}
