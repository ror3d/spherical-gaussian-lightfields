#include "Model.h"
#ifndef NO_OPENGL_OR_CUDA
#include <utils/DebugRenderer.h>
#endif
using namespace chag;

namespace scene
{
#ifndef NO_OPENGL_OR_CUDA
void Model::render( GLSLProgramObject* programObject )
{
	programObject->setUniform( "modelMatrix", m_transform.get_MV_inv() );
	glBindVertexArray( m_vaob );
	if ( !m_indices.m_host_vector.empty() )
	{
		glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_indices.m_buffer_object );
	}
	for ( auto &prim : m_primitives )
	{
		const auto material = prim.material;
		programObject->setUniform( "base_color_factor", material->m_base_color_factor );
		programObject->setUniform( "metal_factor", material->m_metallic_factor );
		programObject->setUniform( "roughness_factor", material->m_roughness_factor );
		programObject->setUniform( "alpha_cutoff", material->m_alpha_cutoff );
		programObject->setUniform( "normal_scale", material->m_normal_scale );
		programObject->setUniform("emissive_factor", material->m_emissive_factor);

		if ( material->m_base_color_texture )
		{
			programObject->setUniform( "has_base_color_texture", true );
			programObject->bindTexture(
			        "base_color_texture", material->m_base_color_texture->m_GLID, GL_TEXTURE_2D, 0 );
		}
		else
		{
			programObject->setUniform( "has_base_color_texture", false );
		}
		if ( material->m_normal_texture )
		{
			programObject->setUniform( "has_normal_texture", true );
			programObject->bindTexture( "normal_texture", material->m_normal_texture->m_GLID, GL_TEXTURE_2D, 1 );
		}
		else
		{
			programObject->setUniform( "has_normal_texture", false );
		}
		if ( material->m_metallic_roughness_texture )
		{
			programObject->setUniform( "has_metal_roughness_texture", true );
			programObject->bindTexture( "metal_roughness_texture", material->m_metallic_roughness_texture->m_GLID, GL_TEXTURE_2D, 2 );
		}
		else
		{
			programObject->setUniform( "has_metal_roughness_texture", false );
		}

		if ( !m_uvs[1].m_host_vector.empty() )
		{
			programObject->setUniform( "has_lm_uv", true );
		}

		if ( m_indices.m_host_vector.empty() )
		{
			glDrawArrays( GL_TRIANGLES,
						  prim.vbo_offset,
						  prim.vbo_count );
		}
		else
		{
			glDrawElements( GL_TRIANGLES,
							prim.vbo_count,
							GL_UNSIGNED_INT,
							(void*)(prim.vbo_offset * sizeof(GLuint)) );
		}
	}
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
}
#endif
Model::~Model()
{
	for ( auto mat : m_materials )
	{
		delete mat.second;
	}
	m_materials.clear();
	LOG_ERROR("Scene::Model needs to clear opengl memory");
}

Aabb Model::getTransformedAabb()
{
	Aabb aabb = make_inverse_extreme_aabb();
	aabb = combine( aabb, vec3( m_transform.get_MV_inv() * vec4( m_aabb.min.x, m_aabb.min.y, m_aabb.min.z, 1.0f ) ) );
	aabb = combine( aabb, vec3( m_transform.get_MV_inv() * vec4( m_aabb.max.x, m_aabb.min.y, m_aabb.min.z, 1.0f ) ) );
	aabb = combine( aabb, vec3( m_transform.get_MV_inv() * vec4( m_aabb.min.x, m_aabb.max.y, m_aabb.min.z, 1.0f ) ) );
	aabb = combine( aabb, vec3( m_transform.get_MV_inv() * vec4( m_aabb.max.x, m_aabb.max.y, m_aabb.min.z, 1.0f ) ) );
	aabb = combine( aabb, vec3( m_transform.get_MV_inv() * vec4( m_aabb.min.x, m_aabb.min.y, m_aabb.max.z, 1.0f ) ) );
	aabb = combine( aabb, vec3( m_transform.get_MV_inv() * vec4( m_aabb.max.x, m_aabb.min.y, m_aabb.max.z, 1.0f ) ) );
	aabb = combine( aabb, vec3( m_transform.get_MV_inv() * vec4( m_aabb.min.x, m_aabb.max.y, m_aabb.max.z, 1.0f ) ) );
	aabb = combine( aabb, vec3( m_transform.get_MV_inv() * vec4( m_aabb.max.x, m_aabb.max.y, m_aabb.max.z, 1.0f ) ) );
	return aabb;
}

Item* Model::clone()
{
	///////////////////////////////////////////////////////////////////
	// WARNING: With the other objects, a clone is a complete copy,
	//			but here geometry is not copied, only transform and
	//          material.
	///////////////////////////////////////////////////////////////////
	Model* new_item = new Model;
	*new_item = *this;
	for ( auto m : m_materials )
	{
		Material* material_copy = new Material;
		*material_copy = *m.second;
		for ( auto &p : new_item->m_primitives )
		{
			if ( p.material == m.second )
			{
				p.material = material_copy;
			}
		}
		new_item->m_materials[m.first] = material_copy;
	}
	return new_item;
}

#ifndef NO_OPENGL_OR_CUDA
void Model::uploadRenderDataToGPU()
{
	glGenVertexArrays( 1, &m_vaob );
	glBindVertexArray( m_vaob );
	m_positions.upload();
	glVertexAttribPointer( 0, 3, GL_FLOAT, false, 0, 0 );
	glEnableVertexAttribArray( 0 );
	m_normals.upload();
	glVertexAttribPointer( 1, 3, GL_FLOAT, false, 0, 0 );
	glEnableVertexAttribArray( 1 );
	m_uvs[0].upload();
	glVertexAttribPointer( 2, 2, GL_FLOAT, false, 0, 0 );
	glEnableVertexAttribArray( 2 );
	m_tangents.upload();
	glVertexAttribPointer( 3, 3, GL_FLOAT, false, 0, 0 );
	glEnableVertexAttribArray( 3 );

	for ( GLuint i = 1; i < m_uvs.size(); ++i )
	{
		if ( !m_uvs[i].m_host_vector.empty() )
		{
			m_uvs[i].upload();
			glVertexAttribPointer( 3 + i, 2, GL_FLOAT, false, 0, 0 );
			glEnableVertexAttribArray( 3 + i );
		}
	}

	glBindVertexArray( 0 );

	if ( !m_indices.m_host_vector.empty() )
	{
		m_indices.upload();
	}
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
}

void Model::buildOctTreeNode( const chag::Aabb& nodeAABB, std::vector<uint32_t> triangles, const uint32_t minTriangles, const float minNodeSize )
{
	const float eps = 0.01f;
	float nodeAabbSize =
	        glm::min( nodeAABB.max.x - nodeAABB.min.x, glm::min( nodeAABB.max.y - nodeAABB.min.y, nodeAABB.max.z - nodeAABB.min.z ) );
	if ( ( triangles.size() <= minTriangles ) || nodeAabbSize < minNodeSize )
	{
		Batch b;
		b.offset = uint32_t( m_batch_positions.m_host_vector.size() );
		b.count = uint32_t( triangles.size() ) * 3;
		b.aabb = make_inverse_extreme_aabb();
		for ( auto t : triangles )
		{
			const vec3& v0 = m_positions.m_host_vector[t * 3 + 0];
			const vec3& v1 = m_positions.m_host_vector[t * 3 + 1];
			const vec3& v2 = m_positions.m_host_vector[t * 3 + 2];
			b.aabb = combine( b.aabb, v0 );
			b.aabb = combine( b.aabb, v1 );
			b.aabb = combine( b.aabb, v2 );
			m_batch_positions.m_host_vector.push_back( v0 );
			m_batch_positions.m_host_vector.push_back( v1 );
			m_batch_positions.m_host_vector.push_back( v2 );
			// DebugRenderer::instance().addAabb(b.aabb, DebugRenderer::blue, "batch_hierarchy");
		}
		m_batches.push_back( b );
		return;
	}
	const vec3 corners[] = { { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 1.0f, 1.0f, 0.0f },
		                     { 0.0f, 0.0f, 1.0f }, { 1.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, 1.0f } };
	for ( int n = 0; n < 8; n++ )
	{
		Aabb subNodeAABB;
		subNodeAABB.min = nodeAABB.min + corners[n] * nodeAABB.getHalfSize() - eps;
		subNodeAABB.max = subNodeAABB.min + nodeAABB.getHalfSize() + 2.0f * eps;
		std::vector<unsigned int> subNodeTriangles;
		for ( unsigned int i = 0; i < triangles.size(); )
		{
			const vec3& v0 = m_positions.m_host_vector[triangles[i] * 3 + 0];
			const vec3& v1 = m_positions.m_host_vector[triangles[i] * 3 + 1];
			const vec3& v2 = m_positions.m_host_vector[triangles[i] * 3 + 2];
			vec3 midPoint = ( v0 + v1 + v2 ) / 3.0f;
			auto inside = []( const chag::Aabb& aabb, const vec3& point ) {
				return ( ( point.x > aabb.min.x && point.x < aabb.max.x ) && ( point.y > aabb.min.y && point.y < aabb.max.y )
				         && ( point.z > aabb.min.z && point.z < aabb.max.z ) );
			};
			if ( inside( subNodeAABB, midPoint ) )
			{
				subNodeTriangles.push_back( triangles[i] );
				// swap in the last one...
				triangles[i] = *triangles.rbegin();
				triangles.resize( triangles.size() - 1 );
			}
			else
			{
				i++;
			}
		}
		if ( subNodeTriangles.size() > 0 )
		{
			buildOctTreeNode( subNodeAABB, subNodeTriangles, minTriangles, minNodeSize );
		}
	}
	if ( triangles.size() > 0 )
	{
		LOG_ERROR( "WHOA! Some (" << triangles.size() << ")triangles were not in any subchild!" );
		for ( auto t : triangles )
		{
			const vec3& v0 = m_positions.m_host_vector[t * 3 + 0];
			const vec3& v1 = m_positions.m_host_vector[t * 3 + 1];
			const vec3& v2 = m_positions.m_host_vector[t * 3 + 2];
			vec3 midPoint = ( v0 + v1 + v2 ) / 3.0f;
			LOG_WARNING( midPoint << " is not in " << m_aabb.min << "->" << m_aabb.max );
		}
	}
}

void Model::buildBatches()
{
	///////////////////////////////////////////////////////////////////////
	// Separate geometry into batches of nearby geometry
	//
	// NOTE: Assuming that m_positions contain all vertices of all
	// triangles in expanded form (three vertices in a row per triangle).
	///////////////////////////////////////////////////////////////////////
	LOG_INFO( "Building batches for shadow geometry" );
	std::vector<unsigned int> triangles( m_positions.m_host_vector.size() / 3 );
	for ( uint32_t i = 0; i < m_positions.m_host_vector.size() / 3; ++i )
	{
		triangles[i] = i;
	}
	// DebugRenderer::instance().clear("batch_hierarchy");
	// TODO: It really matters that some triangles are much larger than others!
	// TODO: Do a binary subdivision scheme instead, always going / 8 means average batch sizes vary a lot and average is very
	// small.
	buildOctTreeNode( m_aabb, triangles, 2048, m_aabb.getHalfSize().x / 100.0f );
	m_batch_positions.upload();
	LOG_VERBOSE( "Triangles: " << triangles.size() );
	LOG_VERBOSE( "Batches: " << m_batches.size() );
	size_t sum = 0;
	for ( size_t b = 0; b < m_batches.size(); b++ )
	{
		sum += m_batches[b].count;
	}
	LOG_VERBOSE( "Total triangles: " << sum / 3 );
	LOG_VERBOSE( "Avg triangles/Batch: " << sum / m_batches.size() );
}
void Model::renderBatches( mat4* VP )
{
	///////////////////////////////////////////////////////////////////////
	//
	///////////////////////////////////////////////////////////////////////

	static std::vector<int> offsets;
	static std::vector<int> count;
	offsets.clear();
	count.clear();

	static bool firsttime = true;
	if(firsttime) {
		for ( auto& b : m_batches )
		{
			bool use_batch = true;
			if ( VP != NULL )
			{
				// Extract frustum planes from matrix
				vec4 planes[6];
				float* vp = &( *VP )[0][0];
				planes[0] = vec4( vp[3] - vp[0], vp[7] - vp[4], vp[11] - vp[8], vp[15] - vp[12] );    // R
				planes[1] = vec4( vp[3] + vp[0], vp[7] + vp[4], vp[11] + vp[8], vp[15] + vp[12] );    // L
				planes[2] = vec4( vp[3] + vp[1], vp[7] + vp[5], vp[11] + vp[9], vp[15] + vp[13] );    // B
				planes[3] = vec4( vp[3] - vp[1], vp[7] - vp[5], vp[11] - vp[9], vp[15] - vp[13] );    // T
				planes[4] = vec4( vp[3] - vp[2], vp[7] - vp[6], vp[11] - vp[10], vp[15] - vp[14] );   // F
				planes[5] = vec4( vp[3] + vp[2], vp[7] + vp[6], vp[11] + vp[10], vp[15] + vp[14] );   // N
				for ( int i = 0; i < 6; i++ )
				{
					float dist = sqrtf( planes[i].x * planes[i].x + planes[i].y * planes[i].y + planes[i].z * planes[i].z );
					planes[i] = ( 1.0f / dist ) * planes[i];
				}
				// Test aabb against frustum
				const vec3 c = b.aabb.getCentre();
				const vec3 h = b.aabb.getHalfSize();
				enum { INSIDE, OUTSIDE, INTERSECT };
				unsigned int result = OUTSIDE;   // Assume that the aabb will be inside the frustum
				for ( unsigned int i = 0; i < 6; i++ )
				{
					vec3 n = vec3( planes[i] );
					float d = planes[i].w;
					float e = h.x * abs( n.x ) + h.y * abs( n.y ) + h.z * abs( n.z );
					float s = dot( c, n ) + d;
					// if(s - e > 0) {	result = OUTSIDE; break; }
					if ( s + e < 0 )
					{
						result = INSIDE;
						break;
					}
					// else result = INTERSECT;
				}
				if ( result == INSIDE )
					use_batch = false;
			}

			if ( use_batch )
			{
				offsets.push_back( b.offset );
				count.push_back( b.count );
			}
			/*
			if(firsttime) {
			if(use_batch) DebugRenderer::instance().addAabb(b.aabb, DebugRenderer::red, "batch_hierarchy");
			else DebugRenderer::instance().addAabb(b.aabb, DebugRenderer::green, "batch_hierarchy");
			}
			*/
		}
		firsttime = false;
	}

	glBindVertexArray( 0 );
	glBindBuffer( GL_ARRAY_BUFFER, m_batch_positions.m_buffer_object );
	glVertexAttribPointer( 0, 3, GL_FLOAT, false, 0, 0 );
	glEnableVertexAttribArray( 0 );
	glMultiDrawArrays( GL_TRIANGLES, &offsets[0], &count[0], uint32_t( offsets.size() ) );
}
#endif
}   // namespace scene
