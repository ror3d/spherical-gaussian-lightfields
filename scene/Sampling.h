#pragma once
#include "glm/fwd.hpp"

// TODO: Move all this stuff somewhere else
namespace sample {
inline float sign(float v) { return v < 0.0f ? -1.0f : 1.0f; };
float randf();

bool sameHemisphere(const glm::vec3 &i, const glm::vec3 &o, const glm::vec3 &n);
glm::vec3 uniformSampleHemisphere();
glm::vec3 uniformSampleSphere();
void concentricSampleDisk(float *dx, float *dy, float u1 = -1.0f, float u2 = -1.0f);
glm::vec3 cosineSampleHemisphere(float u1 = -1.0f, float u2 = -1.0f);
glm::vec3 restrictedCosineSampleHemisphere(const float fov);
}  // namespace sample
