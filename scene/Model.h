#pragma once
#include "Item.h"
#include <vector>
#include <array>
#include <map>
#include "utils/Log.h"
#ifndef NO_OPENGL_OR_CUDA
#include "utils/GLSLProgramObject.h"
#endif
#include "Material.h"
#include "utils/glm_extensions.h"
#include "utils/Aabb.h"

namespace scene
{
	///////////////////////////////////////////////////////////////////////////
	// Helper for buffer objects
	///////////////////////////////////////////////////////////////////////////
	template<class T>
	class buffer {
	public:
		std::vector<T>	m_host_vector;
		#ifndef NO_OPENGL_OR_CUDA
		uint32_t			m_buffer_object;
		buffer() : m_buffer_object(0) {};
		virtual void upload() {
			if(m_buffer_object == 0) glGenBuffers(1, &m_buffer_object);
			glBindBuffer(GL_ARRAY_BUFFER, m_buffer_object);
			glBufferData(GL_ARRAY_BUFFER, m_host_vector.size() * sizeof(T), &m_host_vector[0], GL_STATIC_DRAW);
		};
		#endif
	};

	template<class _T>
	class index_buffer : public buffer<_T>
	{
	public:
		virtual void upload() override
		{
			if(buffer<_T>::m_buffer_object == 0) glGenBuffers(1, &(buffer<_T>::m_buffer_object));
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer<_T>::m_buffer_object);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer<_T>::m_host_vector.size() * sizeof(_T), &buffer<_T>::m_host_vector[0], GL_STATIC_DRAW);
		};
	};

	///////////////////////////////////////////////////////////////////////////
	// Model
	///////////////////////////////////////////////////////////////////////////
	class Model : public Item {
	public:
		using vec2 = glm::vec2;
		using vec3 = glm::vec3;
		using vec4 = glm::vec4;
		using mat4 = glm::mat4;
		Model() : Item() { m_hidden = false; }
		~Model();
		bool m_hidden;
		std::string m_filename;
		std::map<std::string, Material *> m_materials;
		unsigned int m_embreeGeometryID;
		buffer<vec3> m_positions;
		buffer<vec3> m_normals;
		buffer<vec3> m_tangents;
		std::array<buffer<vec2>, 8> m_uvs;
		index_buffer<GLuint> m_indices;
		std::vector<Material *> m_triangleMaterial;

		struct Primitive
		{
			Material* material;
			GLsizei vbo_offset;
			GLsizei vbo_count;
			size_t gltf_prim_id;
		};
		std::vector<Primitive> m_primitives;

		size_t m_gltfMeshId;

		uint32_t m_vaob;
		chag::Aabb m_aabb;
		virtual chag::Aabb getLocalAabb() { return m_aabb; };
		virtual chag::Aabb getTransformedAabb();
	    virtual Item* clone();

#ifndef NO_OPENGL_OR_CUDA
		virtual void render(GLSLProgramObject *programObject);
		///////////////////////////////////////////////////////////////////////////
		// Geometry batches (cullable) for shadows
		///////////////////////////////////////////////////////////////////////////
		struct Batch {
			uint32_t offset, count;
			chag::Aabb aabb;
		};
		buffer<vec3> m_batch_positions;
		std::vector<Batch> m_batches;
	    void uploadRenderDataToGPU();
		void buildOctTreeNode(const chag::Aabb &nodeAABB, std::vector<uint32_t> triangles,
							  const uint32_t minTriangles, const float minNodeSize);
		void buildBatches(); 
		void renderBatches(mat4 *VP = nullptr); 
#endif
	};
	///////////////////////////////////////////////////////////////////////////
	// Serialization
	///////////////////////////////////////////////////////////////////////////
	inline std::ostream& operator << (std::ostream &os, scene::Model &m)
	{
		os << "model " << m.m_name << " " << m.m_filename << " " << m.m_transform;
		return os;
	}
	inline std::istream& operator >> (std::istream& is, scene::Model &m)
	{
		is >> m.m_name >> m.m_filename >> m.m_transform;
		return is;
	}
}

