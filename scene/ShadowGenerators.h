#pragma once
#include <utils/Log.h>
#include <utils/view.h>
#ifndef NO_OPENGL_OR_CUDA
#include <GL/glew.h>
#include <utils/GLSLProgramObject.h>
#endif
#include <vector>
#include "Item.h"
#include "Lights.h"
#include "Model.h"
#include "glm/vec2.hpp"

namespace scene
{
	class ShadowGenerator : public Item {
	public:
		using ivec2 = glm::ivec2;
		ShadowGenerator() : Item() {};
		Light *m_light;
#ifndef NO_OPENGL_OR_CUDA
		virtual void update(const std::vector<scene::Model *> &/*models*/, GLSLProgramObject * /*program*/) {};
		virtual void submit(GLSLProgramObject * /*programObject*/, int /*lightID*/) {};
#endif
		virtual void save(std::ofstream &os) = 0;
	};

	class ShadowMapShadowGenerator : public ShadowGenerator {
	public:
		float m_polygon_offset_factor, m_polygon_offset_units;
		ivec2 m_resolution;
		uint32_t m_depth_texture;
		uint32_t m_framebuffer_ID;
		ShadowMapShadowGenerator() : ShadowGenerator() { m_framebuffer_ID = 0; };
		void resize(int width, int height);
		virtual Item *clone() {
			LOG_WARNING("Clone not yet implemented for ShadowMapShadowGenerator!");
			return NULL;
		}
		virtual view *getView() = 0;
	#ifndef NO_OPENGL_OR_CUDA	
		virtual void update(const std::vector<scene::Model *> &models, GLSLProgramObject *program);
		virtual void submit(GLSLProgramObject *programObject, int lightID);
	#endif
		virtual void save(std::ofstream &os) = 0;
	};

	class PerspectiveShadowMapShadowGenerator : public ShadowMapShadowGenerator {
	public:
		PerspectiveShadowMapShadowGenerator() : ShadowMapShadowGenerator() {};
		float m_fov;
		float m_aspect_ratio;
		float m_near;
		float m_far;
		virtual view *getView() {
			if(m_view == NULL) m_view = new view();
			m_view->pos = m_light->m_transform.pos;
			m_view->R = m_light->m_transform.R;
			m_view->m_near = m_near;
			m_view->m_far = m_far;
			m_view->m_fov = m_fov;
			m_view->m_aspect_ratio = m_aspect_ratio;
			return m_view;
		}
		virtual void save(std::ofstream &os) {
			os << "shadowmapshadowgenerator " << m_name << " " << m_light->m_name << " " << m_fov << " " << m_aspect_ratio << " "
				<< m_near << " " << m_far << " " << m_polygon_offset_factor << " " << m_polygon_offset_units << " "
				<< m_resolution.x << " " << m_resolution.y << std::endl;
		}
	};

	class OrthoShadowMapShadowGenerator : public ShadowMapShadowGenerator {
	public:
		OrthoShadowMapShadowGenerator() : ShadowMapShadowGenerator() {};
		float m_right, m_left, m_top, m_bottom, m_near, m_far;
		virtual view *getView() {
			if(m_view == NULL) m_view = new orthoview();
			orthoview *view = dynamic_cast<orthoview *>(m_view);
			view->pos = m_light->m_transform.pos;
			view->R = m_light->m_transform.R;
			view->m_right = m_right;
			view->m_left = m_left;
			view->m_top = m_top;
			view->m_bottom = m_bottom;
			view->m_near = m_near;
			view->m_far = m_far;
			return m_view;
		}
		virtual void save(std::ofstream &os) {
			os << "orthoshadowmapshadowgenerator " << m_name << " " << m_light->m_name << " " << m_right << " " << m_left << " "
				<< m_top << " " << m_bottom << " " << m_near << " " << m_far << " " << m_polygon_offset_factor << " " << m_polygon_offset_units << " "
				<< m_resolution.x << " " << m_resolution.y << std::endl;
		}
	};
}