///////////////////////////////////////////////////////////////////////////////
// Simple constant color shader 
///////////////////////////////////////////////////////////////////////////////
shader shadowMapVertex
{
	#version 400 compatibility
	out vec3 color; 
	uniform mat4 modelMatrix;
	void main()
	{
		color = gl_Color.xyz;
		gl_Position = gl_ModelViewProjectionMatrix * modelMatrix * gl_Vertex;
	}
}

shader shadowMapFragment
{
	#version 400 compatibility
	in vec3 color; 
	void main()
	{
		gl_FragColor.xyz = color; 
	}
}