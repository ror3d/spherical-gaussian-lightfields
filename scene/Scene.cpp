#include "Scene.h"
#include <sstream>
#include <GL/freeglut.h>
#include "stb_image.h"
#include "utils/Log.h"
#include "utils/ProgressListener.h"
#include "utils/glm_extensions.h"
#include "glm/gtc/constants.hpp"

using namespace chag;
using namespace std;

namespace scene
{

void scene_clearGltfData( Scene* scene );

	Scene::Scene(void)
	{
		m_controllableViewCounter = 0;
		m_renderableViewCounter = 0;
		m_sceneUnitMultiplier = 1.0f;
		m_environment_color = vec3(0.184f, 0.228f, 0.275f);
		m_environmentMap = nullptr;
		m_environmentMap_path = "";
		m_envmap_intensity_multiplier = 1.0f;
		{
			PerspectiveCamera *camera = new PerspectiveCamera;
			camera->m_name = "DefaultCamera";
			camera->m_transform.lookAt(vec3(0.0f, 0.0f, -10.0f), vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));
			camera->m_near = 0.1f;
			camera->m_far = 1000.0f;
			camera->m_fov = 60.0f;
			camera->m_aspect_ratio = 1.0f;
			m_cameras.push_back(camera);
		}
		{
			Light *light = new Light();
		    light->m_shape.type        = Light::Shape::Disc;
		    light->m_distribution.type = Light::Distribution::Diffuse;
		    light->m_name = "DefaultLight";
			light->m_transform.lookAt(vec3(0.0f, 10.0f, 0.0f), vec3(0.0f, -1.0f, 0.0f));
			light->m_color = vec3(1.0f, 1.0f, 1.0f);
			light->m_radiance = 1000.0f;
			light->m_shape.Disc.m_radius = 1.0f;
			m_lights.push_back(light);
		}
#ifndef NO_OPENGL_OR_CUDA		
		m_programManager = new GLSLProgramManager("../scene/Scene.glsl");
		m_shadowMapProgram = m_programManager->getProgramObject("shadowMapVertex", "shadowMapFragment");
#endif
	}

	Scene::~Scene(void)
	{
#ifndef NO_OPENGL_OR_CUDA				
		delete m_programManager;
		m_programManager = nullptr;
#endif
		for (auto camera : m_cameras) delete camera;
		//for (auto light : m_lights) delete light;
		for (size_t i = 0; i < m_lights.size(); i++) {
			delete m_lights[i];
		}

		for (auto model : m_models) delete model;

		m_cameras.clear();
		m_lights.clear();
		m_models.clear();

		scene_clearGltfData( this );
	}

    void Scene::clear()
    {
		for ( auto camera : m_cameras )
			delete camera;
		for ( auto light : m_lights )
			delete light;
		for ( auto model : m_models )
			delete model;
		m_cameras.clear();
		m_lights.clear();
		m_models.clear();
    }

#ifndef NO_OPENGL_OR_CUDA	
	void Scene::submitModels(GLSLProgramObject *programObject)
	{
		for (const auto &model : m_models) {
			if(!model->m_hidden) model->render(programObject);
		}
	}

	void Scene::submitLights(GLSLProgramObject *programObject, orientation &transform)
	{
		int number_of_lights = int(m_lights.size());
		vector<vec3> light_position;
		vector<vec3> light_direction;
		vector<float>  light_radiance;
		vector<vec3> light_color;
		vector<float>  light_radius;
		programObject->setUniform("lights_number_of_lights", number_of_lights);
		mat4 MV = transform.get_MV();
		mat3 MVInvTransp = transpose(mat3(transform.get_MV_inv()));
		for (const auto &light : m_lights) {
			if(light->m_shape.type == Light::Shape::Disc && light->m_distribution.type == Light::Distribution::Diffuse)
			{
				light_position.push_back(vec3(MV * vec4(light->m_transform.pos, 1.0f)));
				light_direction.push_back(-(MVInvTransp * light->m_transform.R[2]));
				light_radiance.push_back(light->m_radiance);
				light_color.push_back(light->m_color);
				light_radius.push_back(light->m_shape.Disc.m_radius);
			}
			else if(light->m_shape.type == Light::Shape::Rectangle && light->m_distribution.type == Light::Distribution::Diffuse)
			{
				// TODO: HACK! Treating rectangle light as disc for shading!
				light_position.push_back(vec3(MV * vec4(light->m_transform.pos, 1.0f)));
				light_direction.push_back(-(MVInvTransp * light->m_transform.R[2]));
				light_radiance.push_back(light->m_radiance);
				light_color.push_back(light->m_color);
				light_radius.push_back(0.5f * (light->m_shape.Rectangle.m_width + light->m_shape.Rectangle.m_height));
			}
			else if(light->m_shape.type == Light::Shape::Point && light->m_distribution.type == Light::Distribution::Diffuse)
			{
				light_position.push_back(vec3(MV * vec4(light->m_transform.pos, 1.0f)));
				light_direction.push_back(-(MVInvTransp * light->m_transform.R[2]));
				light_radiance.push_back(light->m_radiance);
				light_color.push_back(light->m_color);
				light_radius.push_back(0);
			}
			else if(light->m_distribution.type == Light::Distribution::Spot) {
				LOG_WARNING("SpotLight not implemented for forward shading");
			}
			else {
				LOG_ERROR("Non DiscDiffuseLight not implemented");
			}
		}
		programObject->setUniform("light_position", (vec3 *)&light_position[0].x, number_of_lights);
		programObject->setUniform("light_direction", (vec3 *)&light_direction[0].x, number_of_lights);
		programObject->setUniform("light_radiance", &light_radiance[0], number_of_lights);
		programObject->setUniform("light_color", (vec3 *)&light_color[0].x, number_of_lights);
		programObject->setUniform("light_radius", &light_radius[0], number_of_lights);

		mat3 MVtranspose = mat3(transpose(transform.get_MV()));
		programObject->setUniform("modelViewTranspose", MVtranspose);

		if(m_environmentMap) {
			programObject->setUniform("environment_map_active", true);
			programObject->bindTexture("environment_map_texture", m_environmentMap->m_GLID, m_environmentMap->getBindTarget(), 0);
		}
	}
#endif

	Aabb Scene::getAabb()
	{
		Aabb r = make_inverse_extreme_aabb();
		for(const auto &model : m_models) {
			r = combine(r, model->getTransformedAabb());
		}
		return r;
	}

	scene::Item * Scene::getCurrentControllableItem() {
		if(m_controllableViewCounter < static_cast<int>(m_cameras.size())) return m_cameras[m_controllableViewCounter];
		else if(m_controllableViewCounter < static_cast<int>(m_cameras.size() + m_lights.size())) return m_lights[m_controllableViewCounter - m_cameras.size()];
		else return m_models[m_controllableViewCounter - (m_cameras.size() + m_lights.size())];
	}

	void Scene::nextControllableItem() {
		m_controllableViewCounter = (m_controllableViewCounter + 1) % (m_cameras.size() + m_lights.size() + m_models.size());
	};

	void Scene::prevControllableItem() {
		m_controllableViewCounter = (m_controllableViewCounter - 1) % (m_cameras.size() + m_lights.size() + m_models.size());
	}
	scene::Item * Scene::getCurrentRenderableItem() {
		if(m_renderableViewCounter < static_cast<int>(m_cameras.size())) return m_cameras[m_renderableViewCounter];
		else if(m_renderableViewCounter < static_cast<int>(m_cameras.size() + m_lights.size())) return m_lights[m_renderableViewCounter - m_cameras.size()];
		else if(m_renderableViewCounter < static_cast<int>(m_cameras.size() + m_lights.size() + m_models.size())) return m_models[m_renderableViewCounter - (m_cameras.size() + m_lights.size())];
		else return m_shadowGenerators[m_renderableViewCounter - (m_cameras.size() + m_lights.size() + m_models.size())];
	}
	void Scene::nextRenderableItem() {
		m_renderableViewCounter = (m_renderableViewCounter + 1) % (m_cameras.size() + m_lights.size() + m_models.size() + m_shadowGenerators.size());
	}
	void Scene::prevRenderableItem() {
		m_renderableViewCounter = (m_renderableViewCounter - 1) % (m_cameras.size() + m_lights.size() + m_models.size() + m_shadowGenerators.size());
	}

	void Scene::setCurrentControllableItem(const std::string &name)
	{
		for(unsigned int i = 0; i < m_cameras.size(); i++) if(m_cameras[i]->m_name == name) m_controllableViewCounter = i;
		for(unsigned int i = 0; i < m_lights.size(); i++) if(m_lights[i]->m_name == name) m_controllableViewCounter = int(m_cameras.size()) + i;
		for(unsigned int i = 0; i < m_models.size(); i++) if(m_models[i]->m_name == name) m_controllableViewCounter = int(m_cameras.size()) + int(m_lights.size()) + i;
	}

	void Scene::setCurrentRenderableItem(const std::string &name)
	{
		for(int i = 0; i < static_cast<int>(m_cameras.size()); i++) if(m_cameras[i]->m_name == name) m_renderableViewCounter = i;
		for(int i = 0; i < static_cast<int>(m_lights.size()); i++) if(m_lights[i]->m_name == name) m_renderableViewCounter = int(m_cameras.size()) + i;
		for(int i = 0; i < static_cast<int>(m_models.size()); i++) if(m_models[i]->m_name == name) m_renderableViewCounter = int(m_cameras.size()) + int(m_lights.size()) + i;
		for(int i = 0; i < static_cast<int>(m_shadowGenerators.size()); i++) if(m_shadowGenerators[i]->m_name == name) m_renderableViewCounter = int(m_cameras.size()) + int(m_lights.size() + int(m_models.size())) + i;
	}

#ifndef NO_OPENGL_OR_CUDA		
	void Scene::submitShadowGenerators(GLSLProgramObject *programObject)
	{
		// This is not pretty. 
		for(size_t i = 0u; i < m_lights.size(); ++i) {
			stringstream ss;
			ss << "light_has_shadowmap[" << i << "]";
			programObject->setUniform(ss.str().c_str(), false);
		}

		for(auto shadowGenerator : m_shadowGenerators) {
			for(size_t i = 0u; i < m_lights.size(); ++i) {
				if(shadowGenerator->m_light == m_lights[i]) {
					shadowGenerator->submit(programObject, static_cast<int>(i));
				}
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// Set up a number of (cullable) batches containing only the position 
	// information from all models
	///////////////////////////////////////////////////////////////////////////
	void Scene::buildShadowGeometry()
	{
		for(auto &m : m_models) {
			m->buildBatches();
		}
	}
	void Scene::submitShadowGeometry(mat4 *VP)
	{
		for(auto &m : m_models) {
			m->renderBatches(VP); 
		}
	}
#endif
}
