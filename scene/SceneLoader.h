#pragma once

#include <string>
#include <iostream>

namespace scene
{
class Scene;

bool loadScene( Scene* scene, const std::string& filename );

bool saveScene( Scene* scene, const std::string& filename );

}
