#pragma once
#include <utils/view.h>
#include <map>
#include <string>
#include <iostream>
#include <variant>
#ifndef NO_OPENGL_OR_CUDA
#include <fstream> 
#include "utils/GLSLProgramManager.h"
#include "utils/GLSLProgramObject.h"
#endif
#include "utils/Log.h"
#include "utils/glm_extensions.h"
#include "Sampling.h"
#include "Material.h"
#include "Intersection.h"
#include "Texture.h"
#include "Item.h"
#include "Cameras.h"
#include "Lights.h"
#include "Model.h"
#include "ShadowGenerators.h"
#include "utils/json_extras.hpp"
#include "utils/json.hpp"

namespace scene
{

	class Scene
	{
	public:
		using vec2 = glm::vec2;
		using vec3 = glm::vec3;
		using vec4 = glm::vec4;
		using ivec3 = glm::ivec3;
		using mat3 = glm::mat3;
		using mat4 = glm::mat4;

		std::string m_scene_name;

		///////////////////////////////////////////////////////////////////////////
		// Shadow Generators
		///////////////////////////////////////////////////////////////////////////
#ifndef NO_OPENGL_OR_CUDA			
		GLSLProgramManager *m_programManager;
		GLSLProgramObject  *m_shadowMapProgram;
		void updateShadowGenerators() {
			for(auto sg : m_shadowGenerators) {
				sg->update(m_models, m_shadowMapProgram);
			}
		}
		void submitShadowGenerators(GLSLProgramObject *programObject);

		///////////////////////////////////////////////////////////////////////////
		// Geometry batches (cullable) for shadows
		///////////////////////////////////////////////////////////////////////////
		void buildShadowGeometry(); 
		void submitShadowGeometry(mat4 *VP = nullptr); 
#endif
		///////////////////////////////////////////////////////////////////////////
		// Containers
		///////////////////////////////////////////////////////////////////////////
		std::vector<Model *>	m_models;
		std::vector<Camera *>	m_cameras;
		std::vector<Light *>	m_lights;
		std::vector<ShadowGenerator *> m_shadowGenerators;

		///////////////////////////////////////////////////////////////////////////
		// Environment 
		///////////////////////////////////////////////////////////////////////////
		Texture* m_environmentMap = nullptr;
		std::string m_environmentMap_path;
		float m_envmap_intensity_multiplier;
		vec3 m_environment_color;

		///////////////////////////////////////////////////////////////////////////
		// Lightmaps
		///////////////////////////////////////////////////////////////////////////
	    Texture* m_lightmap = nullptr;

		///////////////////////////////////////////////////////////////////////////
		// Getters/setters
		///////////////////////////////////////////////////////////////////////////
		Light * getLight(std::string name) {
			for(size_t i = 0ull; i < m_lights.size(); i++) {
				if(m_lights[i]->m_name == name) return m_lights[i];
			}
			return NULL;
		}
		Camera * getCamera(std::string name) {
			for(size_t i = 0ull; i < m_cameras.size(); i++) {
				if(m_cameras[i]->m_name == name) return m_cameras[i];
			}
			return NULL;
		}
		Model * getModel(std::string name) {
			for(size_t i = 0ull; i < m_models.size(); i++) {
				if(m_models[i]->m_name == name) return m_models[i];
			}
			return NULL;
		}

		///////////////////////////////////////////////////////////////////////////
		// Load/Save Scene
		///////////////////////////////////////////////////////////////////////////
		std::string m_filename;
	    void clear();
		void* m_gltfSceneLoaderData = nullptr;

		///////////////////////////////////////////////////////////////////////////
		// Extra
		///////////////////////////////////////////////////////////////////////////
		nlohmann::json m_extra;

		///////////////////////////////////////////////////////////////////////////
		// Geometry
		///////////////////////////////////////////////////////////////////////////
		float m_sceneUnitMultiplier;
		float m_sceneDebugGeomSizeMultiplier;

		chag::Aabb getAabb();

		///////////////////////////////////////////////////////////////////////////
		// Render
		///////////////////////////////////////////////////////////////////////////
#ifndef NO_OPENGL_OR_CUDA				
		void submitModels(GLSLProgramObject *programObject);
		void submitLights(GLSLProgramObject *programObject, chag::orientation &transform);
#endif
		///////////////////////////////////////////////////////////////////////////
		// Camera/Light control
		// The "current controllable" view is the one that gets transformed by 
		// mouse input and what not. The "current renderable" is the one we look
		// from. 
		///////////////////////////////////////////////////////////////////////////
		int m_controllableViewCounter;
		int m_renderableViewCounter;
		Item *getCurrentControllableItem();
		void setCurrentControllableItem(const std::string &name);
		void nextControllableItem();
		void prevControllableItem();
		Item * getCurrentRenderableItem();
		void setCurrentRenderableItem(const std::string &name);
		void nextRenderableItem();
		void prevRenderableItem();

		Scene(void);
		~Scene(void);
	};
}
