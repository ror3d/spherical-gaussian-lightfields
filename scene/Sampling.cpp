#include "Sampling.h"
#include <limits>
#include <mutex>
#include <random>
#include <thread>
#include "glm/glm.hpp"
#include "glm/gtc/constants.hpp"
#include "utils/Log.h"

std::mutex _mutex;

static thread_local std::mt19937 generator;
static thread_local std::uniform_real_distribution<float> unit_dist(0.0f, 1.0f);
static thread_local bool is_seedeed = false;

namespace sample {

float randf() {
	if (!is_seedeed) {
		size_t id_size_t = std::hash<std::thread::id>()(std::this_thread::get_id());
		unsigned id = id_size_t % std::numeric_limits<unsigned>::max();
		generator.seed(id);
		is_seedeed = true;
		std::lock_guard<std::mutex> guard(_mutex);
		LOG_INFO("New random generator: " << id);
	}
	return unit_dist(generator);
}

bool sameHemisphere(const glm::vec3 &i, const glm::vec3 &o, const glm::vec3 &n) {
	return sign(dot(o, n)) == sign(dot(i, n));
}

glm::vec3 uniformSampleHemisphere() {
	const float r1 = randf();
	const float r2 = randf();
	return glm::vec3(
		2.0f * cosf(glm::two_pi<float>() * r1) * sqrtf(r2 * (1.0f - r2)),
		2.0f * sinf(glm::two_pi<float>() * r1) * sqrtf(r2 * (1.0f - r2)),
		std::abs(1.0f - 2.0f * r2));
}

glm::vec3 uniformSampleSphere() {
	const float r1 = randf();
	const float r2 = randf();
	return glm::vec3(
		2.0f * cosf(glm::two_pi<float>() * r1) * sqrtf(r2 * (1.0f - r2)),
		2.0f * sinf(glm::two_pi<float>() * r1) * sqrtf(r2 * (1.0f - r2)),
		1.0f - 2.0f * r2);
}

void concentricSampleDisk(float *dx, float *dy, float u1, float u2) {
	float r, theta;
	if (u1 == -1.0f && u2 == -1.0f) {
		u1 = randf();
		u2 = randf();
	}
	// Map uniform random numbers to $[-1,1]^2$
	const float sx = 2 * u1 - 1;
	const float sy = 2 * u2 - 1;
	// Map square to $(r,\theta)$
	// Handle degeneracy at the origin
	if (sx == 0.0 && sy == 0.0) {
		*dx = 0.0;
		*dy = 0.0;
		return;
	}
	if (sx >= -sy) {
		if (sx > sy) {
			// Handle first region of disk
			r = sx;
			if (sy > 0.0) theta = sy / r;
			else          theta = 8.0f + sy / r;
		} else {
			// Handle second region of disk
			r     = sy;
			theta = 2.0f - sx / r;
		}
	} else {
		if (sx <= sy) {
			// Handle third region of disk
			r     = -sx;
			theta = 4.0f - sy / r;
		} else {
			// Handle fourth region of disk
			r     = -sy;
			theta = 6.0f + sx / r;
		}
	}
	theta *= glm::pi<float>() / 4.0f;
	*dx = r * cosf(theta);
	*dy = r * sinf(theta);
}

glm::vec3 cosineSampleHemisphere(float u1, float u2) {
	glm::vec3 ret;
	concentricSampleDisk(&ret.x, &ret.y, u1, u2);
	ret.z = sqrt(glm::max(0.0f, 1.0f - ret.x * ret.x - ret.y * ret.y));
	return ret;
}

glm::vec3 restrictedCosineSampleHemisphere(const float fov) {
	glm::vec3 ret;
	const float u1 = randf();
	const float u2 = randf();
	// Uniformly sample disk.
	const float r   = sqrtf(u1) * sinf(fov);
	const float phi = glm::two_pi<float>() * u2;
	ret.x = r * cosf(phi);
	ret.y = r * sinf(phi);

	// Project up to hemisphere.
	ret.z = sqrtf(fmaxf(0.0f, 1.0f - r * r));
	return ret;
}
}  // namespace sample
