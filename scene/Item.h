#pragma once

#include <string>
#include <utils/orientation.h>
#include <utils/view.h>
namespace scene
{
	///////////////////////////////////////////////////////////////////////////
	// The base class for all objects in the scene (models, lights, cameras), 
	// that have an orientation in the world.
	///////////////////////////////////////////////////////////////////////////
	class Item {
	public:
		Item() { m_view = NULL; }
		Item(const Item &other) : m_name(other.m_name), m_transform(other.m_transform), m_view(nullptr) {
			if (other.m_view != nullptr)
				m_view = new view(*other.m_view);
		}
	    virtual ~Item()
	    {
			if ( m_view )
			{
				delete m_view;
				m_view = nullptr;
			}
	    };
		std::string m_name;
		chag::Orientation m_transform;
		///////////////////////////////////////////////////////////////////////
		// Any Item must be able to provide a view, so it can be used to view
		// the scene. Items that have a "real" view (e.g. perspective camera 
		// or spot light) override this method. 
		///////////////////////////////////////////////////////////////////////
		view *m_view = nullptr;
		virtual view * getView() {
			if(m_view == NULL) m_view = new view();
			m_view->pos = m_transform.pos;
			m_view->R = m_transform.R;
			m_view->m_near = 0.01f;
			m_view->m_far = 10000.0f;
			m_view->m_fov = 60.0f;
			m_view->m_aspect_ratio = 1.0f;
			return m_view;
		}
		void rename(const std::string &new_name) {
			m_name = new_name;
		};
		virtual Item *clone() = 0;
	};
}
