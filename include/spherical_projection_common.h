#pragma once
#if __CUDACC__
#define FUNCDECL __host__ __device__ __inline__
#else 
#define FUNCDECL inline
#endif

#define ENVMAP_SIZE 128
#ifndef M_PI
#define M_PI 3.14159265359
#endif

#ifdef USE_OPTIX
#define CHAG_VEC3 float3
#else
#include <glm/glm.hpp>
#define CHAG_VEC3 glm::vec3
#endif

FUNCDECL float SG(float theta, float phi, float Theta, float Phi, float s) {
	float cosangle = sin(theta) * cos(phi) * sin(Theta) * cos(Phi)
		+ sin(theta) * sin(phi) * sin(Theta) * sin(Phi) + cos(theta) * cos(Theta);
	return exp(s * (cosangle - 1.0f));
};

FUNCDECL float SG( const CHAG_VEC3 & dir, const CHAG_VEC3 Dir, float s )
{
	float cosangle = dot(dir, Dir);
	return exp( s * ( cosangle - 1.0f ) );
};

#if 0
FUNCDECL void GetTexCoordsFromThetaPhi(float & pixel_x, float & pixel_y, const float theta, const float phi)
{
	float phi0 = phi - M_PI; 
	float theta0 = theta; 
	//
	float X0 = sin(theta0) * cos(phi0);
	float Y0 = sin(theta0) * sin(phi0);
	float Z0 = cos(theta0);
	//
	bool upper_hemisphere = Z0 > 0.0f; 
	Z0 = abs(Z0);
	//
	float u0 = X0 / sqrt(1.0f + Z0);
	float v0 = Y0 / sqrt(1.0f + Z0);
	//
	float r0 = sqrt(u0 * u0 + v0 * v0);
	float phi1 = atan2(v0, u0);
	phi1 /= 2.0f; 
	float u1 = r0 * cos(phi1);
	float v1 = r0 * sin(phi1);
	//
	if (upper_hemisphere) u1 *= -1.0f; 
	//
	float r1 = sqrt(u1 * u1 + v1 * v1);
	float phi2 = atan2(v1, u1);
	if (phi2 < -M_PI / 4.0f) phi2 += 2.0f * M_PI; // in range [-pi/4,7pi/4]
	float a, b, x, y;
	if (phi2 < M_PI / 4.0f) { // region 1
		a = r1;
		b = phi2 * a / (M_PI / 4.0f);
	}
	else if (phi2 < 3 * M_PI / 4.0f) { // region 2
		b = r1;
		a = -(phi2 - M_PI / 2.0f) * b / (M_PI / 4.0f);
	}
	else if (phi2 < 5 * M_PI / 4) { // region 3
		a = -r1;
		b = (phi2 - M_PI)*a / (M_PI / 4.0f);
	}
	else {// region 4
		b = -r1;
		a = -(phi2 - 3 * M_PI / 2.0f) * b / (M_PI / 4.0f);
	}
	x = a;// (a + 1) / 2.0f;
	y = b;// (b + 1) / 2.0f;
	//
	x += 1.0f;
	if (x > 1.0f) x -= 2.0f; 
	pixel_x = (x + 1.0f) * 0.5f;
	pixel_y = (y + 1.0f) * 0.5f;
}

FUNCDECL void GetThetaPhiFromTexCoords(const float pixel_x, const float pixel_y, float & theta, float & phi)
{
	// Rotate to get poles back in middle
	float x = pixel_x * 2.0f - 1.0f;
	x -= 1.0f;
	if (x < -1.0f) x += 2;
	float y = pixel_y * 2.0f - 1.0f;
	// From square to unit disk
	float u, v;
	{
		float _phi, r;
		if (x > -y) { // region 1 or 2
			if (x > y) { // region 1, also |a| > |b|
				r = x;
				_phi = (M_PI / 4) * (y / x);
			}
			else { // region 2, also |b| > |a|
				r = y;
				_phi = (M_PI / 4) * (2 - (x / y));
			}
		}
		else { // region 3 or 4
			if (x < y) { // region 3, also |a| >= |b|, a != 0
				r = -x;
				_phi = (M_PI / 4) * (4 + (y / x));
			}
			else { // region 4, |b| >= |a|, but a==0 and b==0 could occur.
				r = -y;
				if (y != 0) _phi = (M_PI / 4) * (6 - (x / y));
				else _phi = 0;
			}
		}
		u = r * cos(_phi);
		v = r * sin(_phi);
	}
	// From half disk to disk
	bool upper_hemisphere = u < 0.0f;
	if (upper_hemisphere) u *= -1.0f;
	{
		float r = sqrt(u * u + v * v);
		float _phi = atan2(v, u);
		_phi *= 2;
		u = r * cos(_phi);
		v = r * sin(_phi);
	}
	// To sphere
	float X, Y, Z;
	{
		X = u * sqrt(2 - u * u - v * v);
		Y = v * sqrt(2 - u * u - v * v);
		Z = 1 - u * u - v * v;
		if (!upper_hemisphere) Z *= -1;
	}
	phi = atan2(Y, X);
	theta = acos(Z);
	phi += M_PI;
};


/*
A Survey of Efficient Representations for Independent Unit Vectors
http://jcgt.org/published/0003/02/01/paper.pdf
*/

#else 

FUNCDECL float signNotZero(float v)
{
	return (v >= 0.0f) ? +1.0f : -1.0f;
}

FUNCDECL void GetTexCoordsFromThetaPhi(float & pixel_x, float & pixel_y, const float theta, const float phi)
{
	float x = sin(theta) * cos(phi);
	float y = sin(theta) * sin(phi);
	float z = cos(theta);
	// Project the sphere onto the octahedron, and then onto the xy plane
	float px = x * (1.0f / (abs(x) + abs(y) + abs(z)));
	float py = y * (1.0f / (abs(x) + abs(y) + abs(z)));
	// Reflect the folds of the lower hemisphere over the diagonals
	if (z <= 0.0f)
	{
		float _px = (1.0f - abs(py)) * signNotZero(px);
		float _py = (1.0f - abs(px)) * signNotZero(py);
		px = _px; 
		py = _py;
	}
	pixel_x = 0.5f * (px + 1.0f);
	pixel_y = 0.5f * (py + 1.0f);
}

FUNCDECL void GetThetaPhiFromTexCoords(const float pixel_x, const float pixel_y, float & theta, float & phi)
{
	float x = pixel_x * 2.0f - 1.0f;
	float y = pixel_y * 2.0f - 1.0f; 
	float z = 1.0 - abs(x) - abs(y);
	if (z < 0.0f) {
		float _x = (1.0f - abs(y)) * signNotZero(x);
		float _y = (1.0f - abs(x)) * signNotZero(y);
		x = _x; 
		y = _y; 
	}
	// Normalize
	float l = sqrt(x * x + y * y + z * z);
	x /= l; 
	y /= l;
	z /= l;
	// Get theta and phi
	phi = atan2(y, x);
	theta = acos(z);
}

#endif



FUNCDECL CHAG_VEC3 GetXYZFromThetaPhi(const float theta, const float phi)
{
	CHAG_VEC3 r;
	r.x = sin( theta ) * cos( phi );
	r.y = sin( theta ) * sin( phi );
	r.z = cos( theta );
	return r;
}

FUNCDECL void GetThetaPhiFromXYZ(CHAG_VEC3 xyz, float& theta, float& phi)
{
	theta = acosf( xyz.z );
	phi = atan2f( xyz.y, xyz.x );
}
