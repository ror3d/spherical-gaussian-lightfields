if (CUDA_FOUND)
	if(DEFINED CUDA_MAX_SUPPORTED_ARCH)
		set(CUDA_SUCCESS "TRUE")
	else()
		#Get CUDA compute capability
		set(CHECK_CUDA_ARCH_OUTDIR ${CMAKE_BINARY_DIR}/CMakeFiles)
		set(CHECK_CUDA_ARCH_OUTFILE ${CHECK_CUDA_ARCH_OUTDIR}/check_cuda_arch) # No suffix required
		set(CHECK_CUDA_ARCH_CU ${CMAKE_CURRENT_SOURCE_DIR}/CMake/check_cuda_arch.cu)
		file(MAKE_DIRECTORY ${CHECK_CUDA_ARCH_OUTDIR})
		
		if(WIN32)
			get_filename_component(VC_PATH ${CMAKE_CXX_COMPILER} DIRECTORY)
			set(OLD_PATH $ENV{PATH})
			set(NEW_PATH "${VC_PATH};${OLD_PATH}")
			set(ENV{PATH} "${NEW_PATH}")
		endif()
		execute_process(COMMAND nvcc -lcuda ${CHECK_CUDA_ARCH_CU} -o ${CHECK_CUDA_ARCH_OUTFILE})
		execute_process(COMMAND ${CHECK_CUDA_ARCH_OUTFILE}
						RESULT_VARIABLE CUDA_RETURN_CODE
						OUTPUT_VARIABLE CUDA_RETURN_ARCH)
		
		if(WIN32)
			set(ENV{PATH} "${OLD_PATH}")
		endif()

		if(${CUDA_RETURN_CODE} EQUAL 0)
			set(CUDA_SUCCESS "TRUE")
			set(CUDA_MAX_SUPPORTED_ARCH "${CUDA_RETURN_ARCH}" CACHE STRING "This is the highest architecture number supported by the GPU.")
		else()
			set(CUDA_SUCCESS "FALSE")
		endif()
	endif()

	if (${CUDA_SUCCESS})
		message(STATUS "CUDA Architecture: ${CUDA_RETURN_ARCH}")
		
		if(NOT DEFINED CMAKE_CUDA_ARCHITECTURES)
			set(CMAKE_CUDA_ARCHITECTURES ${CUDA_MAX_SUPPORTED_ARCH})
		endif()

	else()
		message(WARNING ${CUDA_RETURN_ARCH})
	endif()
endif()
