﻿#include "sg_renderer.h"
#include <CHAGApp/PerformanceProfiler.h>
#include <utils/half.hpp>
#include <utils/json.hpp>
#include <utils/json_extras.hpp>
#include <utils/opengl_helpers.h>
#include <vector>
#include "../include/spherical_projection_common.h"
#include <utils/filesystem.h>

#include <wx/filepicker.h>
#include <wx/msgdlg.h>
#include <CHAGApp/imgui/imgui.h>
#include <CHAGApp/Settings.h>

using namespace std;

#define GAUSSIAN_TEXTURE_SIZE 32

#define NUM_ENVMAP_SAMPLES_PER_PASS 256

struct quart
{
	quart() {}
	quart( float f ) { v = (f * 255); }
	quart& operator=( float f ) { v = (f * 255); return *this; }
	uint8_t v = 0;
};

namespace chag
{
SGRenderer::SGRenderer() : IRenderer( new GLSLProgramManager( "sg_renderer.glsl" ) )
{
}

void SGRenderer::init()
{
	m_gbuffer_program = m_program_manager->getProgramObject( "gbuffer_vert", "gbuffer_frag" );
	m_deferred_program = m_program_manager->getProgramObject( "deferred_vert", "deferred_frag" );
	m_convolve_envmap_program =
	        m_program_manager->getProgramObject( "convolve_env_vert",
	                                             "convolve_env_frag",
	                                             "",
	                                             "#define NUM_SAMPLES " + std::to_string( NUM_ENVMAP_SAMPLES_PER_PASS ) + "\n" );
}

void SGRenderer::reloadShaders()
{
	m_program_manager->reload();
}

bool SGRenderer::loadSGFile( const std::string & file, const std::string& json_file, GLuint& sg_tex0, GLuint& sg_tex1 )
{
	///////////////////////////////////////////////////////////////////////////
	// Loading SG texture.
	// TODO: Filename hardcoded for now, but should be a scene property or at
	//       least a Setting.
	// TODO: Size, num gaussians and num parameters hardcoded but should be
	//       part of file header.
	///////////////////////////////////////////////////////////////////////////
	// const string filename = "../../lightfield-bake/lightfields/oneball/output/gaussian_parameter_texture_8.bin";

	const string filename = file;
	LOG_INFO( "Loading SG Texture ..." << filename );
	ifstream is( filename, ios::binary );
	if ( !is.is_open() )
	{
		LOG_ERROR( "Failed to open lightfield texture." );
		return false;
	}

	vector<float> data;

	m_texture_size = 128;
	m_num_gaussians = 16;
	m_num_parameters = 7;

	std::string ext = std::filesystem::path( file ).extension().string();
	for ( size_t i = 0; i < ext.length(); ++i ) if ( ext[i] >= 'A' && ext[i] <= 'Z' ) ext[i] = ext[i] - 'A' + 'a'; // Get lowercase

	if ( ext == ".sgt" )
	{
		float magic = 0;
		uint16_t version = 0;
		uint16_t lm_width = 0;
		uint16_t lm_height = 0;
		uint16_t gp_num_gaussians = 0;
		uint8_t gp_format = 0;
		uint8_t gp_num_parameters = 0;
		std::string gp_params_semantic;

#define IS_READ(x) is.read( reinterpret_cast<char*>(&x), sizeof( x ) )
#define IS_READ_CHECK(x, y) IS_READ(x); DebugAssert(x y);
		IS_READ_CHECK( magic, == 54168231936.f );
		IS_READ( version );
		DebugAssert( version > 0 && version <= 2 );
		IS_READ_CHECK( lm_width, > 0 );
		IS_READ_CHECK( lm_height, == lm_width );
		IS_READ( gp_num_gaussians );
		IS_READ_CHECK( gp_format, == 0 );
		IS_READ_CHECK( gp_num_parameters, == 7 );
#undef IS_READ
#undef IS_READ_CHECK
		if ( version >= 2 )
		{
			char c;
			do
			{
				is.read( &c, 1 );
				if ( c != 0 )
				{
					gp_params_semantic += c;
				}
			} while ( c != 0 );
			// TODO: Check semantics
		}

		m_texture_size = lm_width;
		m_num_gaussians = gp_num_gaussians;
		m_num_parameters = gp_num_parameters;
	}
	data.resize( m_texture_size * m_texture_size * m_num_gaussians * m_num_parameters );
	is.read( (char*)data.data(), m_texture_size * m_texture_size * m_num_gaussians * m_num_parameters * sizeof( float ) );

	///////////////////////////////////////////////////////////////////////////
	// Read metadata and create a mapping from texel to file
	// TODO: Filename hardcoded. Lazy.
	///////////////////////////////////////////////////////////////////////////
	try
	{
		using json = nlohmann::json;
		// TODO: Include some of this data in the bin file, for showing the debug information we could have a setting pointing to the base directory
		const string filename = json_file;

		base_data_path = getFilePath( json_file );

		json meta;
		try
		{
			std::ifstream fstream( filename );
			if ( fstream )
			{
				meta = json::parse( fstream );
			}
			else
			{
				LOG_ERROR( "Failed to load " << filename << ". File not found!" );
				return false;
			}
		}
		catch ( std::exception& e )
		{
			LOG_ERROR( "Failed to load " << filename );
			DebugError( e.what() );
		}

		DebugAssert( m_texture_size == meta["lightmapGridSize"][0] );

		size_t batch_files_count = meta["batches"].size();
		data_filenames.resize( batch_files_count );
		uv_to_file.resize( m_texture_size * m_texture_size );

		for ( auto& batch : meta["batches"] )
		{
			size_t data_file_index = batch["pageIndex"];
			data_filenames[data_file_index] = batch["uri"].get<std::string>();
			int idx = 0;
			for ( auto& texel : batch["texels"] )
			{
				glm::u16vec2 coord = texel;
				uv_to_file[coord.y * m_texture_size + coord.x] = make_pair( data_file_index, idx++ );
			}
		}
	}
	catch ( ... )
	{
	}

#if GAUSSIAN_TEXTURE_SIZE == 32
#define GAUSSIAN_C_TYPE float
#define GAUSSIAN_GL_TYPE GL_FLOAT
#define GAUSSIAN_GL_RGBA_INTERNAL_FORMAT GL_RGBA32F
#define GAUSSIAN_GL_RGB_INTERNAL_FORMAT GL_RGB32F
#elif GAUSSIAN_TEXTURE_SIZE == 16
#define GAUSSIAN_C_TYPE half_float::half
#define GAUSSIAN_GL_TYPE GL_HALF_FLOAT
#define GAUSSIAN_GL_RGBA_INTERNAL_FORMAT GL_RGBA16F
#define GAUSSIAN_GL_RGB_INTERNAL_FORMAT GL_RGB16F
#elif GAUSSIAN_TEXTURE_SIZE == 8
#define GAUSSIAN_C_TYPE quart
#define GAUSSIAN_GL_TYPE GL_UNSIGNED_BYTE
#define GAUSSIAN_GL_RGBA_INTERNAL_FORMAT GL_RGBA8
#define GAUSSIAN_GL_RGB_INTERNAL_FORMAT GL_RGB8
#else
#error Undefined texture size
#endif

	// Move first three parameters to array texture
	vector<GAUSSIAN_C_TYPE> texture0, texture1;
	texture0.resize( m_texture_size * m_texture_size * m_num_gaussians * 4 );
	texture1.resize( m_texture_size * m_texture_size * m_num_gaussians * 3 );
	int ctr = 0;
	for ( int y = 0; y < m_texture_size; y++ )
	{
		for ( int x = 0; x < m_texture_size; x++ )
		{
			for ( int g = 0; g < m_num_gaussians; g++ )
			{
				for ( int p = 0; p < 4; p++ )
				{
					texture0[g * ( m_texture_size * m_texture_size * 4 ) + y * m_texture_size * 4 + x * 4 + p] = data[ctr++];
				}
				for ( int p = 0; p < 3; p++ )
				{
					texture1[g * ( m_texture_size * m_texture_size * 3 ) + y * m_texture_size * 3 + x * 3 + p] = data[ctr++];
				}
			}
		}
	}
	if ( sg_tex0 )
	{
		glDeleteTextures( 1, &sg_tex0 );
	}

	glGenTextures( 1, &sg_tex0 );
	glBindTexture( GL_TEXTURE_2D_ARRAY, sg_tex0 );
	glTexStorage3D( GL_TEXTURE_2D_ARRAY, 1, GAUSSIAN_GL_RGBA_INTERNAL_FORMAT, m_texture_size, m_texture_size, m_num_gaussians );
	glTexSubImage3D( GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, m_texture_size, m_texture_size, m_num_gaussians, GL_RGBA, GAUSSIAN_GL_TYPE, texture0.data() );
	glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	 //glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	 //glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT );

	if ( sg_tex1 )
	{
		glDeleteTextures( 1, &sg_tex1 );
	}
	glGenTextures( 1, &sg_tex1 );
	glBindTexture( GL_TEXTURE_2D_ARRAY, sg_tex1 );
	glTexStorage3D( GL_TEXTURE_2D_ARRAY, 1, GAUSSIAN_GL_RGB_INTERNAL_FORMAT, m_texture_size, m_texture_size, m_num_gaussians );
	glTexSubImage3D( GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, m_texture_size, m_texture_size, m_num_gaussians, GL_RGB, GAUSSIAN_GL_TYPE, texture1.data() );
	glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	 //glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	 //glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT );
	cout << "done.\n";

	///////////////////////////////////////////////////////////////////////////
	// Allocate debug memory
	///////////////////////////////////////////////////////////////////////////
	if ( debug_ssbo  != -1)
	{
		glDeleteBuffers( 1, &debug_ssbo );
	}
	glGenBuffers( 1, &debug_ssbo );
	glBindBuffer( GL_SHADER_STORAGE_BUFFER, debug_ssbo );
	glBufferData( GL_SHADER_STORAGE_BUFFER, ( m_num_gaussians * m_num_parameters + 3 + 2 + 3 ) * sizeof( float ), nullptr, GL_DYNAMIC_DRAW );

	return true;
}


void SGRenderer::resize( uint16_t width, uint16_t height )
{
	if ( m_gbuffer == 0 )
	{
		// Create the GBuffer
		m_gbuffer = opengl_helpers::framebuffer::createFramebuffer(
		        width,
		        height,
		        opengl_helpers::framebuffer::createTexture( width, height, GL_DEPTH_COMPONENT32 ),   // Depth buffer (position)
		        {
		                opengl_helpers::framebuffer::createTexture( width, height, GL_RGBA16F ),   // (diffuse) reflectance,
		                                                                                       // roughness
		                opengl_helpers::framebuffer::createTexture( width, height, GL_RGBA16F ),   // (spherical coords) normal,
		                                                                                       // _unused, metal_factor
		                opengl_helpers::framebuffer::createTexture( width, height, GL_RGBA32F ),   // Ambient Light value
		        } );
	}
	else
	{
		opengl_helpers::framebuffer::resizeFramebuffer( m_gbuffer, width, height );
	}
}


std::string SGRenderer::loadSGFileDialog( GLuint& sg_tex0, GLuint& sg_tex1 )
{
	std::string bin_file;

	if ( bin_file.empty() ) // ?? (Erik) How could it not be? 
	{
		wxFileDialog openFileDialog(
			m_parentWindow, _( "Open Spherical Gaussian train file" ), "", "", "Spherical Gaussian Texture (*.sgt)|*.sgt|bin files (*.bin)|*.bin", wxFD_OPEN );
		if ( openFileDialog.ShowModal() == wxID_OK )
		{
			bin_file = std::string( openFileDialog.GetPath().mb_str( wxConvUTF8 ) );
			if ( filesystem::path(bin_file).extension().string() == ".bin" )
			{
				LOG_WARNING( "bin files are deprecated. Opening with default properties which might be very wrong." );
			}
		}
		else
		{
			return "";
		}
	}

	if ( tryLoadSGFile( bin_file, sg_tex0, sg_tex1 ) )
	{
		return bin_file;
	}
	else
	{
		return "";
	}
}

bool SGRenderer::convolveEnvMap()
{
	if ( m_convolved_envmap == 0 && std::filesystem::exists( "sg_file.json" ) )
	{
		try
		{
			std::ifstream settingsfs( "sg_file.json" );
			if ( settingsfs )
			{
				nlohmann::json settings = nlohmann::json::parse( settingsfs );

				std::string envmap_fname = settings.value( "environment", "" );

				if ( !envmap_fname.empty() )
				{
					// Load file
					if ( std::filesystem::exists( envmap_fname ) )
					{
					}
				}
			}
		}
		catch ( std::exception& e )
		{
		}
	}

	ImGui::Begin("Convolved Environment Map");

	int v = m_total_envmap_levels;
	ImGui::InputInt( "Levels", &v );
	m_total_envmap_levels = std::max( 0, v );
	v = m_total_envmap_samples;
	ImGui::InputInt( "Samples", &v );
	m_total_envmap_samples = std::max( 0, v );

	if ( !m_current_envmap )
	{
		if ( ImGui::Button( "Convolve" ) )
		{
			scene::Texture2D_RGBA32F * envmap = dynamic_cast<scene::Texture2D_RGBA32F *>(m_scene->m_environmentMap);
			m_current_envmap = envmap;
			m_convolved_envmap_samples = 0;
			m_convolved_envmap_levels = 0;

			if ( m_convolved_envmap != 0 )
			{
				glDeleteTextures( 1, &m_convolved_envmap );
				m_convolved_envmap = 0;
			}
		}
	}
	else
	{
		ImGui::Button("...");
	}

	ImGui::End();


	if ( m_convolved_envmap_levels < m_total_envmap_levels && m_current_envmap )
	{
		int width = m_current_envmap->m_width;
		int height = m_current_envmap->m_height;

		if ( m_convolved_envmap == 0 )
		{
			glCreateTextures( GL_TEXTURE_3D, 1, &m_convolved_envmap );
			glTextureStorage3D( m_convolved_envmap, 1, GL_RGB16F, width, height, m_total_envmap_levels );
			glTextureParameteri( m_convolved_envmap, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
			glTextureParameteri( m_convolved_envmap, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
			glTextureParameteri( m_convolved_envmap, GL_TEXTURE_WRAP_S, GL_REPEAT );
			glTextureParameteri( m_convolved_envmap, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT );
			glTextureParameteri( m_convolved_envmap, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );
		}

		if ( m_convolving_envmap_fbf == 0 )
		{
			glCreateFramebuffers( 1, &m_convolving_envmap_fbf );
			glNamedFramebufferParameteri( m_convolving_envmap_fbf, GL_FRAMEBUFFER_DEFAULT_WIDTH, width );
			glNamedFramebufferParameteri( m_convolving_envmap_fbf, GL_FRAMEBUFFER_DEFAULT_HEIGHT, height );
		}

		glPushAttrib(GL_ALL_ATTRIB_BITS);
		glViewport(0, 0, width, height);
		m_convolve_envmap_program->enable();
		m_convolve_envmap_program->bindTexture( "envmap", m_scene->m_environmentMap->m_GLID, GL_TEXTURE_2D, 0 );
		glTextureParameteri(m_scene->m_environmentMap->m_GLID, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTextureParameteri(m_scene->m_environmentMap->m_GLID, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTextureParameteri(m_scene->m_environmentMap->m_GLID, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTextureParameteri(m_scene->m_environmentMap->m_GLID, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

		glEnable( GL_BLEND );
		glBlendFunc( GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA );
		glBlendEquation( GL_FUNC_ADD );
		glBlendColor( 0, 0, 0, NUM_ENVMAP_SAMPLES_PER_PASS / float( NUM_ENVMAP_SAMPLES_PER_PASS + m_convolved_envmap_samples ) );

		if ( m_convolved_envmap_samples == 0 )
		{
			LOG_INFO( "Convolving environment map level " << m_convolved_envmap_levels );
			glNamedFramebufferTextureLayer( m_convolving_envmap_fbf, GL_COLOR_ATTACHMENT0, m_convolved_envmap, 0, m_convolved_envmap_levels );
		}

		float roughness = (m_convolved_envmap_levels + 0.5f) / float( m_total_envmap_levels ); // Between 0 and 1
		// Transform to get more precision at lower roughnesses (inversed in shader code)
		roughness = pow(roughness, 2.2f);
		opengl_helpers::framebuffer::pushFramebuffer( m_convolving_envmap_fbf );
		m_convolve_envmap_program->setUniform( "roughness", roughness );
		m_convolve_envmap_program->setUniform( "frame_number", m_convolved_envmap_samples );
		m_convolve_envmap_program->setUniform(
				"max_envmap_value", Settings::instance().get<float>( "pathtracer", "pathtrace_max_envmap_value" ) );
		m_convolve_envmap_program->setUniform( "env_map_multiplier",
											   m_scene->m_environment_color * m_scene->m_envmap_intensity_multiplier );

		orthoview::drawFullScreenQuad();

		opengl_helpers::framebuffer::popFramebuffer();

		m_convolve_envmap_program->disable();
		glPopAttrib(); 

		m_convolved_envmap_samples += NUM_ENVMAP_SAMPLES_PER_PASS;
		if ( m_convolved_envmap_samples >= m_total_envmap_samples )
		{
			m_convolved_envmap_levels += 1;

			if ( m_convolved_envmap_levels >= m_total_envmap_levels )
			{
				// Save in a cache file
#if 0 // TODO: This crashes, apparently I'm not allocating enough memory? idk
				{
					std::string env_fname = "env_cache.bin";
					std::ofstream env_f( env_fname, std::ofstream::binary | std::ofstream::trunc );
					std::vector<uint8_t> data( width * height * 3 /*RGB*/ * 2 /*half float*/ );

					env_f.write( reinterpret_cast<char*>(&width), sizeof( int ) );
					env_f.write( reinterpret_cast<char*>(&height), sizeof( int ) );
					env_f.write( reinterpret_cast<char*>(&m_total_envmap_levels), sizeof( unsigned int ) );
					for ( int level = 0; level < m_total_envmap_levels; ++level )
					{
						glGetTextureImage( m_convolved_envmap, level, GL_RGB, GL_HALF_FLOAT, data.size(), data.data() );
						env_f.write( (char*)data.data(), data.size() );
					}
				}
#endif

				m_current_envmap = nullptr;
				glDeleteFramebuffers( 1, &m_convolving_envmap_fbf );
				m_convolving_envmap_fbf = 0;
			}
			else
			{
				m_convolved_envmap_samples = 0;
			}
		}

		return true;
	}

	return false;
}

bool SGRenderer::tryLoadSGFile( const std::string & file, GLuint & sg_tex0, GLuint & sg_tex1 )
{
	std::string json_file;
	if ( !file.empty() )
	{
		std::filesystem::path json_path = file;
		json_path = json_path.parent_path().parent_path();
		for ( auto& p : std::filesystem::directory_iterator( json_path ) )
		{
			if ( p.is_regular_file() && p.path().extension().string() == ".json" )
			{
				json_file = p.path().string();
				break;
			}
		}
	}

	if ( !json_file.empty() && !file.empty() )
	{
		return loadSGFile( file, json_file, sg_tex0, sg_tex1 );
	}
	return false;
}

void SGRenderer::render()
{
	ImGui::Begin( "SG Options" );
	if ( ImGui::Button( "Load Reflection SGs" ) )
	{
		m_lightfield_file = loadSGFileDialog( m_SGLightfieldTexture0, m_SGLightfieldTexture1 );
		m_sg_files_changed = true;
	}
	ImGui::SameLine();
	if ( ImGui::Button( "Clear Refl." ) )
	{
		if ( m_SGLightfieldTexture0 != 0 )
		{
			glDeleteTextures( 1, &m_SGLightfieldTexture0 );
		}
		if ( m_SGLightfieldTexture1 != 0 )
		{
			glDeleteTextures( 1, &m_SGLightfieldTexture1 );
		}
		m_lightfield_file = "";
		m_sg_files_changed = true;
	}
	if ( ImGui::Button( "Load Envi. Vis. SGs" ) )
	{
		m_visibility_file = loadSGFileDialog( m_SGEnvVisTexture0, m_SGEnvVisTexture1 );
		m_sg_files_changed = true;
	}
	ImGui::SameLine();
	if ( ImGui::Button( "Clear Env." ) )
	{
		if ( m_SGEnvVisTexture0 != 0 )
		{
			glDeleteTextures( 1, &m_SGEnvVisTexture0 );
		}
		if ( m_SGEnvVisTexture1 != 0 )
		{
			glDeleteTextures( 1, &m_SGEnvVisTexture1 );
		}
		m_visibility_file = "";
		m_sg_files_changed = true;
	}
	ImGui::End();

	if ( m_SGLightfieldTexture0 == 0 && m_SGEnvVisTexture0 == 0 && std::filesystem::exists( "sg_file.json" ) )
	{
		try
		{
			std::ifstream settingsfs( "sg_file.json" );
			if ( settingsfs )
			{
				nlohmann::json settings = nlohmann::json::parse( settingsfs );
				// TODO: Check model

				std::string model = settings["model"];
				std::string lf_file = settings["lightfield"];
				std::string vis_file = settings["visibility"];

				if ( tryLoadSGFile( lf_file, m_SGLightfieldTexture0, m_SGLightfieldTexture1 ) )
				{
					m_lightfield_file = lf_file;
					m_sg_files_changed = true;
				}
				if ( tryLoadSGFile( vis_file, m_SGEnvVisTexture0, m_SGEnvVisTexture1 ) )
				{
					m_visibility_file = vis_file;
					m_sg_files_changed = true;
				}
			}
		}
		catch ( std::exception& e )
		{
			nlohmann::json settings;
			settings["model"] = "something";
			settings["lightfield"] = "";
			settings["visibility"] = "";
			{
				std::ofstream settingsfs( "sg_file.json" );
				settingsfs << settings.dump(4);
			}
			m_sg_files_changed = true;
			DebugError( "Failed to load sg_file.json: %s", e.what() );
		}
	}
	if ( m_sg_files_changed )
	{
		nlohmann::json settings;
		{
			std::ifstream settingsfs( "sg_file.json" );
			if ( settingsfs )
			{
				settings = nlohmann::json::parse( settingsfs );
			}
		}
		settings["model"] = "something";
		settings["lightfield"] = m_lightfield_file;
		settings["visibility"] = m_visibility_file;
		{
			std::ofstream settingsfs( "sg_file.json" );
			settingsfs << settings.dump(4);
		}

		m_sg_files_changed = false;
	}


	if ( convolveEnvMap() )
	{
		return;
	}

	auto currentCamera = m_scene->getCurrentRenderableItem()->getView();

	///////////////////////////////////////////////////////////////////////
	// Generate GBuffer
	///////////////////////////////////////////////////////////////////////
	{
		glPushAttrib(GL_ALL_ATTRIB_BITS);
		glEnable(GL_CULL_FACE);
		PROFILE_GL( "Generate GBuffer" );
		opengl_helpers::framebuffer::pushFramebuffer( m_gbuffer );
		glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		currentCamera->push();
		m_gbuffer_program->enable();
		m_gbuffer_program->setUniform( "environment_color", m_scene->m_environment_color );
		m_scene->submitModels( m_gbuffer_program );
		m_gbuffer_program->disable();
		currentCamera->pop();
		opengl_helpers::framebuffer::popFramebuffer();
		glPopAttrib(); 
	}

	///////////////////////////////////////////////////////////////////////
	// Deferred shading
	///////////////////////////////////////////////////////////////////////
	{
		PROFILE_GL( "Deferred shading" );
		glPushAttrib( GL_ALL_ATTRIB_BITS );
		glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glDisable( GL_DEPTH_TEST );
		m_deferred_program->enable();
		m_deferred_program->setUniform( "camera_MVP_inv", currentCamera->get_MVP_inv() );
		m_deferred_program->setUniform( "camera_position", currentCamera->pos );
		opengl_helpers::framebuffer::bindFramebufferDepthBuffer( m_gbuffer, 0 );
		size_t cbuffers_bound = opengl_helpers::framebuffer::bindFramebufferColorBuffers( m_gbuffer, 1 );
		// Initialize with an "ambient" pass (or whatever GI approximation
		// is suitable)
		m_deferred_program->setUniform( "ambient_pass", true );

		glBindBufferBase( GL_SHADER_STORAGE_BUFFER, 0, debug_ssbo );

		m_deferred_program->setUniform("NUM_GAUSSIANS", m_num_gaussians);
		m_deferred_program->bindTexture( "lightfield_gp_texture0", m_SGLightfieldTexture0, GL_TEXTURE_2D_ARRAY, cbuffers_bound + 1 );
		m_deferred_program->bindTexture( "lightfield_gp_texture1", m_SGLightfieldTexture1, GL_TEXTURE_2D_ARRAY, cbuffers_bound + 2 );

		m_deferred_program->bindTexture( "env_vis_gp_texture0", m_SGEnvVisTexture0, GL_TEXTURE_2D_ARRAY, cbuffers_bound + 3 );
		m_deferred_program->bindTexture( "env_vis_gp_texture1", m_SGEnvVisTexture1, GL_TEXTURE_2D_ARRAY, cbuffers_bound + 4 );


		//if ( m_scene->m_environmentMap != nullptr )z
		m_deferred_program->setUniform("has_envmap", m_convolved_envmap != 0);
		if(m_convolved_envmap != 0)
		{
			m_deferred_program->bindTexture("env_texture", m_convolved_envmap, GL_TEXTURE_3D, cbuffers_bound + 5);
		}
		m_deferred_program->setUniform( "environment_color", m_scene->m_environment_color );


        if ( m_scene->m_lightmap )
		{
			m_deferred_program->setUniform( "has_scene_lightmap", true );
			m_deferred_program->bindTexture( "scene_lightmap_texture", m_scene->m_lightmap->m_GLID, GL_TEXTURE_2D, cbuffers_bound + 6 );
			glTextureParameteri(m_scene->m_lightmap->m_GLID, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTextureParameteri(m_scene->m_lightmap->m_GLID, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}
		else
		{
			m_deferred_program->setUniform( "has_scene_lightmap", false );
		}


		m_deferred_program->setUniform( "debug_pixel", debug_pixel );
		m_deferred_program->setUniform("debug_show_grid", m_debug.show_grid);
		m_deferred_program->setUniform("debug_show_direction_grid", m_debug.show_direction_grid);
		m_deferred_program->setUniform("debug_gaussian_from_env_visibility", m_debug.show_env_visibility);
		m_deferred_program->setUniform("debug_grid_size", glm::vec2(m_texture_size, m_texture_size));

		view::drawFullScreenQuad();

		m_deferred_program->disable();
		glPopAttrib();
	}

	{
		// Copy depth buffer from gbuffer
		int w = opengl_helpers::framebuffer::getFramebufferDefaultWidth( m_gbuffer );
		int h = opengl_helpers::framebuffer::getFramebufferDefaultHeight( m_gbuffer );
		glBlitNamedFramebuffer(
		        m_gbuffer, opengl_helpers::framebuffer::currentFramebuffer(), 0, 0, w, h, 0, 0, w, h, GL_DEPTH_BUFFER_BIT, GL_NEAREST );
	}

	drawDebugThings();
}

void SGRenderer::drawDebugThings()
{
	// Draw currently selected envmap
	static uint32_t viz_texture = 0;
	static uint32_t sil_texture = 0;
	if ( viz_texture == 0 )
	{
		viz_texture = opengl_helpers::texture::createTexture( ENVMAP_SIZE, ENVMAP_SIZE, GL_RGB32F );
		sil_texture = opengl_helpers::texture::createTexture( ENVMAP_SIZE, ENVMAP_SIZE, GL_RGB32F );
	}
	static uint32_t original_texture = 0;
	if ( original_texture == 0 )
		original_texture = opengl_helpers::texture::createTexture( ENVMAP_SIZE, ENVMAP_SIZE, GL_RGB32F );

	static uint32_t diff_texture = 0;
	if (diff_texture == 0)
		diff_texture = opengl_helpers::texture::createTexture(ENVMAP_SIZE, ENVMAP_SIZE, GL_RGB32F);

	if ( debug_pixel_changed )
	{
		glBindBufferBase( GL_SHADER_STORAGE_BUFFER, 0, debug_ssbo );
		float* debug_parameters = (float*)glMapBuffer( GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY );

		glm::vec3 normal = { debug_parameters[m_num_gaussians * m_num_parameters + 5],
			                 debug_parameters[m_num_gaussians * m_num_parameters + 6],
			                 debug_parameters[m_num_gaussians * m_num_parameters + 7] };

		vector<glm::vec3> predicted_envmap( ENVMAP_SIZE * ENVMAP_SIZE, glm::vec3( 0.0f ) );
		vector<glm::vec3> silhouettes(ENVMAP_SIZE * ENVMAP_SIZE, glm::vec3(0.0f));
		vector<glm::vec3> difference(ENVMAP_SIZE * ENVMAP_SIZE, glm::vec3(0.0f));

		for ( int i = 0; i < m_num_gaussians; i++ )
		{
			glm::vec3 Dir = { debug_parameters[i * m_num_parameters + 0],
				              debug_parameters[i * m_num_parameters + 1],
				              debug_parameters[i * m_num_parameters + 2] };
			float s = debug_parameters[i * m_num_parameters + 3];
			glm::vec3 A = { debug_parameters[i * m_num_parameters + 4],
				            debug_parameters[i * m_num_parameters + 5],
				            debug_parameters[i * m_num_parameters + 6] };

			// Evaluate the gaussian for a 128x128 image
			for ( int y = 0; y < ENVMAP_SIZE; y++ )
			{
				for ( int x = 0; x < ENVMAP_SIZE; x++ )
				{
					float phi, theta;
					GetThetaPhiFromTexCoords( (x + 0.5f) / float(ENVMAP_SIZE), (y + 0.5f) / float(ENVMAP_SIZE), theta, phi );
					glm::vec3 dir = { sin( theta ) * cos( phi ), sin( theta ) * sin( phi ), cos( theta ) };

					// Silhouettes
					float v = SG( dir, Dir, s );
					if ( v > 0.4f && v < 0.6f )
					{
						//glm::vec3 col = { 0.4f, 0.4f, 0.4f };   // A;
						glm::vec3 col = 0.1f + 0.9f * A;
						if ( A.x < 0.0f || A.y < 0.0f || A.z < 0.0f )
						{
							col = abs(col);
							col.r = 1.0; 
						}
						//if ( A.x < 0.1f || A.y < 0.1f || A.z < 0.1f )
						//{
						//	col = glm::vec3( 1.0f, 0.0f, 0.0f );
						//}
						silhouettes[y * ENVMAP_SIZE + x] += col;
					}
					if ( dot( dir, normal ) < 0.1f && dot( dir, normal ) > -0.1 )
					{
						silhouettes[y * ENVMAP_SIZE + x].r = 0.5f;
					}

					predicted_envmap[y * ENVMAP_SIZE + x] +=
					        ( ( dot( dir, normal ) < 0.0f ) ? 0.5f : 1.0f ) * A * SG( dir, Dir, s );
				}
			}
		}

		// Draw sample point in predicted image
		glm::vec3 dir = { debug_parameters[m_num_gaussians * m_num_parameters + 0],
			              debug_parameters[m_num_gaussians * m_num_parameters + 1],
			              debug_parameters[m_num_gaussians * m_num_parameters + 2] };
		glm::vec2 phitheta = { atan2( dir.y, dir.x ), acos( dir.z ) };
		if ( phitheta.x < 0 )
			phitheta.x = 2.0f * M_PI + phitheta.x;
		if ( phitheta.y < 0 )
			phitheta.y = M_PI + phitheta.y;
		glm::vec2 envmapcoords;
		GetTexCoordsFromThetaPhi( envmapcoords.x, envmapcoords.y, phitheta.y, phitheta.x );
		glm::ivec2 ienvmapcoord = envmapcoords * float(ENVMAP_SIZE);
		auto DrawPixel = []( int x, int y, glm::vec3 color, std::vector<glm::vec3>& buffer ) {
			if ( x < 0 || x >= ENVMAP_SIZE || y < 0 || y >= ENVMAP_SIZE )
			{
				return;
			}
			buffer[y * ENVMAP_SIZE + x] = color;
		};
		DrawPixel( ienvmapcoord.x - 1, ienvmapcoord.y, { 0.9f, 0.9f, 0.0f }, predicted_envmap );
		DrawPixel( ienvmapcoord.x + 1, ienvmapcoord.y, { 0.9f, 0.9f, 0.0f }, predicted_envmap );
		DrawPixel( ienvmapcoord.x, ienvmapcoord.y - 1, { 0.0f, 0.9f, 0.9f }, predicted_envmap );
		DrawPixel( ienvmapcoord.x, ienvmapcoord.y + 1, { 0.0f, 0.9f, 0.9f }, predicted_envmap );

		glTextureSubImage2D( viz_texture, 0, 0, 0, ENVMAP_SIZE, ENVMAP_SIZE, GL_RGB, GL_FLOAT, predicted_envmap.data() );
		glTextureSubImage2D( sil_texture, 0, 0, 0, ENVMAP_SIZE, ENVMAP_SIZE, GL_RGB, GL_FLOAT, silhouettes.data() );

		//cout << ienvmapcoord.x << ", " << ienvmapcoord.y << endl; 

#if 1
		// Read closest original environment map
		int u = int( debug_parameters[m_num_gaussians * m_num_parameters + 3] * m_texture_size );
		int v = int( debug_parameters[m_num_gaussians * m_num_parameters + 4] * m_texture_size );
		if ( u < 0 )
			u = m_texture_size + u;
		if ( v < 0 )
			v = m_texture_size + v;
		cout << u << ", " << v << endl; 
		auto& file_and_idx = uv_to_file[v * m_texture_size + u];
		std::vector<glm::vec3> original_envmap( ENVMAP_SIZE * ENVMAP_SIZE, glm::vec3( 0.0f ) );
		using namespace half_float;
		std::string file = base_data_path + data_filenames[file_and_idx.first];
		std::ifstream f( file, std::ifstream::binary );
		// 2 bytes: magic number = half_float 1./3. (this might be used to check correct endianness)
		// 2 bytes: version
		// 2 bytes: lightfield tile side length. Lightfield images are always a square.
		// 2 bytes: number of tiles in this texture
		half magic;
		uint16_t version;
		uint16_t lf_size;
		uint16_t lf_count;
		uint16_t lf_channels;
		uint32_t lf_num_samples;
		f.read( reinterpret_cast<char*>( &magic ), sizeof( half ) );
		DebugAssert( magic == half( 1.f / 3.f ) );
		f.read( reinterpret_cast<char*>( &version ), sizeof( uint16_t ) );
		// DebugAssert( version == 3 );
		f.read( reinterpret_cast<char*>( &lf_size ), sizeof( uint16_t ) );
		f.read( reinterpret_cast<char*>( &lf_count ), sizeof( uint16_t ) );
		f.read( reinterpret_cast<char*>( &lf_channels ), sizeof( uint16_t ) );
		if ( version > 3 )
			f.read( reinterpret_cast<char*>( &lf_num_samples ), sizeof( uint32_t ) );
		size_t chunk_size = lf_size * lf_size * lf_channels;
		size_t chunk_size_bytes = chunk_size * sizeof( half );
		f.seekg( chunk_size_bytes * file_and_idx.second, ios_base::cur );
		std::vector<half> buffer( chunk_size );
		f.read( reinterpret_cast<char*>( buffer.data() ), chunk_size_bytes );


		int start_channel = 0; 
		if ( m_debug.show_env_visibility )
		{
			start_channel = 3;
		}

		for ( int i = 0; i < ENVMAP_SIZE * ENVMAP_SIZE; i++ )
		{
			original_envmap[i].x = buffer[(start_channel + 0) * ENVMAP_SIZE * ENVMAP_SIZE + i];
			original_envmap[i].y = buffer[(start_channel + 1) * ENVMAP_SIZE * ENVMAP_SIZE + i];
			original_envmap[i].z = buffer[(start_channel + 2) * ENVMAP_SIZE * ENVMAP_SIZE + i];
			if ( isnan( original_envmap[i].x ) || isnan( original_envmap[i].y ) || isnan( original_envmap[i].z ) )
			{
				original_envmap[i] = glm::vec3{ 0.f, 0.f, 0.f };
			}
		}

		DrawPixel(ienvmapcoord.x - 1, ienvmapcoord.y, { 0.9f, 0.9f, 0.0f }, original_envmap);
		DrawPixel(ienvmapcoord.x + 1, ienvmapcoord.y, { 0.9f, 0.9f, 0.0f }, original_envmap);
		DrawPixel(ienvmapcoord.x, ienvmapcoord.y - 1, { 0.0f, 0.9f, 0.9f }, original_envmap);
		DrawPixel(ienvmapcoord.x, ienvmapcoord.y + 1, { 0.0f, 0.9f, 0.9f }, original_envmap);

		glTextureSubImage2D( original_texture, 0, 0, 0, ENVMAP_SIZE, ENVMAP_SIZE, GL_RGB, GL_FLOAT, original_envmap.data() );


		for (int i = 0; i < ENVMAP_SIZE * ENVMAP_SIZE; i++)
		{
			difference[i].x = abs(original_envmap[i].x - predicted_envmap[i].x);
			difference[i].y = abs(original_envmap[i].y - predicted_envmap[i].y);
			difference[i].z = abs(original_envmap[i].z - predicted_envmap[i].z);
		}
		glTextureSubImage2D(diff_texture, 0, 0, 0, ENVMAP_SIZE, ENVMAP_SIZE, GL_RGB, GL_FLOAT, difference.data());

#endif
		debug_pixel_changed = false;
		glUnmapBuffer( GL_SHADER_STORAGE_BUFFER );
		glBindBuffer( GL_SHADER_STORAGE_BUFFER, 0 );   // unbind
	}

	ImGui::Begin( "SG Debug" );
	ImGui::Checkbox("Show grid", &m_debug.show_grid);
	ImGui::SameLine();
	ImGui::Checkbox("Show direction grid", &m_debug.show_direction_grid);
	ImGui::Checkbox("Debug Env Visibility", &m_debug.show_env_visibility);

	// Draw the images
	std::array<uint32_t, 4> debug_images = { viz_texture, sil_texture, diff_texture, original_texture };

	const int num_images = debug_images.size();
	const int spacing = 10;
	const ImVec2 wsze = ImGui::GetWindowSize();
	for ( uint32_t t = 0; t < num_images; ++t )
	{
		ImGui::Image( ImTextureID(debug_images[t]), ImVec2( wsze.x / num_images - spacing, wsze.x / num_images - spacing ) );
		if ( t < num_images - 1 )
		{
			ImGui::SameLine();
		}
	}
	ImGui::End();
}

void SGRenderer::destroy()
{
}

}   // namespace chag
