#include <array>
#include <memory>

#include "CHAGApp/CHAGApp.h"
#include "CHAGApp/PerformanceProfiler.h"
#include "utils/CheckGLError.h"
#include "utils/GLSLProgramManager.h"
#include "wx/menu.h"
#include <utils/opengl_helpers.h>
#include <wx/aboutdlg.h>
#include <wx/filepicker.h>

#include <CHAGApp/ProgressListenerDialog.h>

#include "renderer/deferred_renderer.h"
#include "renderer/overlay_renderer.h"
#include "sg_renderer.h"
#ifdef USE_PATHTRACER
#include "pathtracer/PathTracer.h"
#include "pathtracer/lightfield_pt_renderer.h"
#include "pathtracer/lightmap_pt_renderer.h"
#include "pathtracer/pinhole_pt_renderer.h"
#endif

#include "utils/filesystem.h"

#include "CHAGApp/imgui/imgui.h"

#include "utils/animator.h"

#include <utils/json.hpp>
#include <utils/json_extras.hpp>

using namespace std;
using namespace chag;

class NewApplication : public CHAGApp
{
public:
	virtual std::string applicationWindowText() override { return std::string( "CHAG Renderer" ); }

	virtual void onGlContextCreated() override;

	///////////////////////////////////////////////////////////////////////////
	// This is called from wxGLPane before destroying the opengl context so
	// we can clear things that use it
	///////////////////////////////////////////////////////////////////////////
	virtual void onGlContextDestroy() override;

	///////////////////////////////////////////////////////////////////////////
	// This is called before CHAGApp has even set up the window. GLEW is not
	// initialized, and there is not even a GL context.
	///////////////////////////////////////////////////////////////////////////
	virtual bool OnInit() override;

	///////////////////////////////////////////////////////////////////////////
	// This is called whenever the default framebuffer changes size.
	// This can happen before the scene has been loaded.
	///////////////////////////////////////////////////////////////////////////
	virtual void onReshape( int width, int height ) override;

	///////////////////////////////////////////////////////////////////////////
	// This is called just before the first frame is rendered. GLEW is
	// initialized and the context is active. The scene is loaded.
	///////////////////////////////////////////////////////////////////////////
	virtual void onFirstFrame() override;
	virtual void onReloadShaders() override;
	virtual void onSceneChanged() override;
	virtual void onViewChanged() override;

	virtual void onSettingChanged( const std::string& category, const std::string& setting ) override;

	virtual void onDisplay() override;
	virtual void onMouse(int x, int y, bool leftIsDown, bool middleIsDown, bool rightIsDown) override;

	//////////////////////////////////////////////////////////////////////////////
	// Implement application specific menus
	//////////////////////////////////////////////////////////////////////////////
	void onApplicationCreateMenuBar( wxMenuBar* menuBar ) override;
	void onUnhandledMenuCommand( wxCommandEvent& event ) override;

#ifdef USE_PATHTRACER
	void updateTextureBakerSettings();

	void updatePathtracerSettings();
	void updateLightfieldSettings();
#endif


	//////////////////////////////////////////////////////////////////////////////
	// This is called by the parent to build the renderer modes tabs
	//////////////////////////////////////////////////////////////////////////////
	virtual std::vector<std::string> getRendererTabNames() const override
	{
#ifdef USE_PATHTRACER
		return { "Deferred", "SG Result", "Pathtracer", "Lightmap Bake", "Lightfield Bake" };
#else
		return {};
#endif
	}
	virtual void onRenderTabChanged( const std::string& tab ) override;

public:
	GLSLProgramManager* m_program_manager;
	std::map<std::string, IRenderer*> m_renderers;
#ifdef USE_PATHTRACER
	wxMenu* m_pathtracer_menu;
#endif

	wxMenu* m_application_menu;
	bool m_orbit_light = false; 

	chag::Animator<glm::vec3> m_cam_pos_animator;
	bool m_record_animation = false;
};

void NewApplication::onGlContextCreated()
{
	m_program_manager = new GLSLProgramManager( "../renderer/main.glsl" );

	m_renderers["deferred"] = new DeferredRenderer( m_program_manager );
	m_renderers["overlay"] = new OverlayRenderer( m_program_manager );
	m_renderers["sg_renderer"] = new SGRenderer();
#ifdef USE_PATHTRACER
	m_renderers["pinhole"] = new pathtracer::PinholePathtracerRenderer();
	m_renderers["lightmap"] = new pathtracer::LightmapPathtracerRenderer();
	m_renderers["lightfield"] = new pathtracer::LightfieldPathtracerRenderer();
#endif

	for ( auto& rend : m_renderers )
	{
		rend.second->setParentWindow( m_frame );
		rend.second->init();
	}
}

void NewApplication::onMouse(int x, int y, bool leftIsDown, bool middleIsDown, bool rightIsDown) 
{
	if (leftIsDown && wxGetKeyState(WXK_CONTROL)) {
		SGRenderer * sg_renderer = dynamic_cast<SGRenderer *>(m_renderers["sg_renderer"]);
		if (sg_renderer != nullptr) {
			sg_renderer->setDebugPixel(x, y);
		}
	}
}


void NewApplication::onGlContextDestroy()
{
	delete m_program_manager;

	for ( auto& rend : m_renderers )
	{
		rend.second->destroy();
		delete rend.second;
	}
#ifdef USE_PATHTRACER
	// Release pathtracer resources
	pathtracer::finalize();
#endif
}

const int ID_About = 100;

const int ID_OrbitLight = 101; 

void NewApplication::onApplicationCreateMenuBar( wxMenuBar* menuBar )
{
#ifdef USE_PATHTRACER
	m_application_menu = new wxMenu; 
	m_application_menu->Append(ID_OrbitLight, "&Orbit Light\tF1", "Orbit Light", wxITEM_CHECK);
	menuBar->Append(m_application_menu, "Application");
#endif
}

void NewApplication::onUnhandledMenuCommand( wxCommandEvent& event )
{
	if ( event.GetId() == ID_About )
	{
		wxAboutDialogInfo info;
		info.SetName( _( "My Program" ) );
		info.SetVersion( _( "1.2.3 Beta" ) );
		info.SetDescription( _( "This program does something great." ) );
		info.SetCopyright( wxT( "(C) 2007 Me <my@email.addre.ss>" ) );
		wxAboutBox( info );
	}
	if (event.GetId() == ID_OrbitLight)
	{
		m_orbit_light = !m_orbit_light;
	}
	else
	{
		LOG_INFO( "No Handler for menu command." );
	}
}

bool NewApplication::OnInit()
{
	return CHAGApp::OnInit();
}


void NewApplication::onReshape( int width, int height )
{
	for ( auto& rend : m_renderers )
	{
		if ( rend.second->resizeOnContextResized() )
		{
			rend.second->resize( width, height );
		}
	}

#ifndef NO_SCENE
	if ( m_scene != NULL )
	{
		scene::PerspectiveCamera* camera = dynamic_cast<scene::PerspectiveCamera*>(m_scene->getCurrentRenderableItem());
		if ( camera != NULL )
		{
			camera->m_aspect_ratio = width / float( height );
		}
	}
#endif
}

void NewApplication::onFirstFrame()
{
#ifdef USE_PATHTRACER
	updateTextureBakerSettings();
	updatePathtracerSettings();
	updateLightfieldSettings();
#endif
}

void NewApplication::onReloadShaders()
{
	m_program_manager->reload();
	SGRenderer* sg_renderer = dynamic_cast<SGRenderer*>( m_renderers["sg_renderer"] );
	if ( sg_renderer != nullptr )
	{
		sg_renderer->reloadShaders();
	}
}

void NewApplication::onSceneChanged()
{
#ifndef NO_SCENE
	if ( m_scene != NULL )
	{
		scene::PerspectiveCamera* camera = dynamic_cast<scene::PerspectiveCamera*>( m_scene->getCurrentRenderableItem() );
		if ( camera != NULL )
		{
			glm::ivec2 sz = getGlSize();
			camera->m_aspect_ratio = sz.x / float( sz.y );

			m_cam_pos_animator.setValue( &camera->m_transform.pos );
		}
	}

#ifdef USE_PATHTRACER
	g_progress->push_task( "Building scene for raytracing" );
	pathtracer::setScene( m_scene );
	g_progress->pop_task();
#endif

	for ( auto& rend : m_renderers )
	{
		rend.second->updateScene( m_scene );
	}
#endif
}

void NewApplication::onViewChanged()
{
#ifdef USE_PATHTRACER
	pathtracer::updateLights();
#endif
	for ( auto& rend : m_renderers )
	{
		rend.second->updateView();
	}
}

void NewApplication::onSettingChanged( const std::string& category, const std::string& setting )
{
	if ( category == "framebuffer" && setting == "resolutions" )
	{
		std::string resol = Settings::instance().get<std::string>( category, setting );
		if ( resol.find( 'x' ) != std::string::npos )
		{
			int w = std::stoul( resol.substr( 0, resol.find( 'x' ) ) );
			int h = std::stoul( resol.substr( resol.find( 'x' ) + 1 ) );
			Settings::instance().set_setting( "framebuffer", "locked_resolution_width", w );
			Settings::instance().set_setting( "framebuffer", "locked_resolution_height", h );
		}
	}

#ifdef USE_PATHTRACER
	if ( category == "uvbaker" )
	{
		updateTextureBakerSettings();
	}
	if ( category == "pathtracer" )
	{
		updatePathtracerSettings();
	}
	if ( category == "pathtracer" || category == "lightfield" )
	{
		updateLightfieldSettings();
	}
#endif
}

#ifdef USE_PATHTRACER
void NewApplication::updateTextureBakerSettings()
{
	int w = Settings::instance().get<int>( "uvbaker", "uv_texture_width" );
	int h = Settings::instance().get<int>( "uvbaker", "uv_texture_height" );
	m_renderers["lightmap"]->resize( w, h );
}

void NewApplication::updatePathtracerSettings()
{
	int max_samples = Settings::instance().get<int>( "pathtracer", "max_samples" );
	int max_bounces = Settings::instance().get<int>( "pathtracer", "max_bounces" );
	int num_paths = Settings::instance().get<int>( "pathtracer", "paths_per_pixel" );
	float min_roughness = Settings::instance().get<float>( "pathtracer", "pathtrace_min_roughness" );
	float max_envmap_value = Settings::instance().get<float>( "pathtracer", "pathtrace_max_envmap_value" );

	for ( auto& rend : m_renderers )
	{
		pathtracer::IPathtracingRenderer* pt = dynamic_cast<pathtracer::IPathtracingRenderer*>( rend.second );
		if ( pt )
		{
			pt->setMaxSamplesPerPixel( max_samples );
			pt->setMaxBouncesPerPath( max_bounces );
			pt->setNumPathsPerPixelPerPass( num_paths );
			pt->setMinRoughness( min_roughness );
			pt->setMaxEnvmapValue( max_envmap_value );
		}
	}
}

void NewApplication::updateLightfieldSettings()
{
	pathtracer::LightfieldPathtracerRenderer* rend = dynamic_cast<pathtracer::LightfieldPathtracerRenderer*>( m_renderers["lightfield"] );
	if ( rend )
	{
		rend->setGridSize( Settings::instance().get<int>( "lightfield", "lightfield_tex_grid_size" ) );

		rend->setLightfieldSize( Settings::instance().get<int>( "lightfield", "lightfield_size" ) );

		int lm_size = Settings::instance().get<int>( "lightfield", "lightfield_lm_texels" );
		rend->setBaseLightmapSize( lm_size, lm_size );

		uint16_t w = Settings::instance().get<int>( "lightfield", "lightfield_lm_texels" );
		rend->setBaseLightmapSize( w, w );

		float jitter = Settings::instance().get<float>( "lightfield", "lightfield_jitter_max_roughness" );
		rend->setMaxJitterRoughness( jitter );

		jitter = Settings::instance().get<float>( "lightfield", "lightfield_jitter_max_position" );
		rend->setMaxJitterPosition( jitter );

		bool envAsA = Settings::instance().get<bool>( "lightfield", "lightfield_environment_as_alpha" );
		rend->setTreatEnvironmentAsAlpha( envAsA );

		bool pathtrace_once = Settings::instance().get<bool>( "lightfield", "lightfield_stop_at_first_pass" );
		rend->setContinueAfterFirstPass( !pathtrace_once );

		size_t max_samples = std::max<size_t>( Settings::instance().get<int>( "lightfield", "lightfield_default_max_samples" ), size_t( 1 ) );
		rend->setMaxSamplesPerPixel( max_samples );
	}
}

#endif

void NewApplication::onDisplay()
{

	SGRenderer * sg_renderer = dynamic_cast<SGRenderer *>(m_renderers["sg_renderer"]);
	if (sg_renderer != nullptr) {
		int old_x = sg_renderer->debug_pixel.x;
		int old_y = sg_renderer->debug_pixel.y;
		if (false && old_x > 1 && old_y > 1) {
			static int ctr = 0; 
			ctr += 1; 
			cout << old_x << ", " << old_y << endl;
			if(ctr % 1 == 0)
				sg_renderer->setDebugPixel(old_x, old_y - 8);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// Little hack allowing us to orbit an object (the lightsource)
	///////////////////////////////////////////////////////////////////////////
	if (m_orbit_light) {
		auto currentCamera = m_scene->getCurrentRenderableItem()->getView();
		chag::Orientation & o = m_scene->getCurrentRenderableItem()->m_transform;
		chag::Orientation & l = m_scene->m_lights[0]->m_transform; 
		o.lookAt(o.pos, l.pos, glm::vec3(0.0f, 1.0f, 0.0f)); // o.R[1]);
	}


	std::string renderer = getCurrentRenderTab();
	PUSH_TRACE( "FRAME" );
#ifdef USE_PATHTRACER
	if ( renderer == "Pathtracer" )
	{
		{
			PROFILE_CPU( "Pathtrace" );
			dynamic_cast<pathtracer::IPathtracingRenderer*>(m_renderers["pinhole"])->pathTrace();
		}
		{
			PROFILE_GL( "Pathtrace Render" );
			m_renderers["pinhole"]->render();
		}
	}
	else if ( renderer == "Lightmap Bake" )
	{
		PROFILE_GL( "Lightmap Bake Render" );
		{
			PROFILE_CPU( "Pathtrace" );
			dynamic_cast<pathtracer::IPathtracingRenderer*>(m_renderers["lightmap"])->pathTrace();
		}
		{
			PROFILE_GL( "Pathtrace Render" );
			m_renderers["lightmap"]->render();
		}
	}
	else if ( renderer == "Lightfield Bake" )
	{
		PROFILE_CPU( "Lightfield Bake Render" );
		{
			PROFILE_CPU( "Pathtrace" );
			dynamic_cast<pathtracer::IPathtracingRenderer*>( m_renderers["lightfield"] )->pathTrace();
		}
		{
			PROFILE_GL( "Pathtrace Render" );
			m_renderers["lightfield"]->render();
		}
	}
	else if ( renderer == "Deferred" )
	{
		ImGui::ShowDemoWindow();

		m_renderers["deferred"]->render();

		///////////////////////////////////////////////////////////////////////
		// Overlay graphics
		///////////////////////////////////////////////////////////////////////
		m_renderers["overlay"]->render();
	}
	else if ( renderer == "SG Result" )
	{
		SGRenderer* rend = dynamic_cast<SGRenderer*>(m_renderers["sg_renderer"]);

		m_renderers["sg_renderer"]->render();

		///////////////////////////////////////////////////////////////////////
		// Overlay graphics
		///////////////////////////////////////////////////////////////////////
		m_renderers["overlay"]->render();
	}
	else
	{
		DebugError( "Render type not implemented" );
	}
#else
	{
		m_renderers["deferred"]->render();

		///////////////////////////////////////////////////////////////////////
		// Overlay graphics
		///////////////////////////////////////////////////////////////////////
		m_renderers["overlay"]->render();
	}
#endif
	POP_TRACE();


#if 0
	static vector<double> current_timings;
	static uint32_t current_frame_count = 10;

	if ( current_frame_count == 0 )
	{
		m_cam_pos_animator.update( m_record_animation ? 1.f / 60.f : getFrameDt() );
	}
#else
	m_cam_pos_animator.update( m_record_animation ? 1.f / 60.f : getFrameDt() );
#endif

	if ( m_record_animation )
	{

		PerformanceProfiler::instance().updateTraces();
		auto node = PerformanceProfiler::instance().m_root.m_children[0]->m_children[1];
		cout << node->m_name << ": " << node->m_opengl_time << endl;

		static uint32_t frame_num = 0;
		static vector<double> timings;

		if ( m_cam_pos_animator.isPlaying() && !m_cam_pos_animator.isFinished() )
		{
			if ( !std::filesystem::exists( "video_output/" ) )
			{
				std::filesystem::create_directory( "video_output" );
			}
#if 0
			if ( current_frame_count == 0 )
			{
				std::sort( current_timings.begin(), current_timings.end() );
				timings.push_back( current_timings[(current_timings.size() - 1) / 2] );
				current_timings.clear();
				frame_num++;
				current_frame_count = 10;
			}
			else
			{
				current_timings.push_back( node->m_opengl_time );
				current_frame_count--;
			}
#else
			takeScreenshot( "video_output/" + std::to_string( frame_num ) + ".png" );
			timings.push_back( node->m_opengl_time );
			frame_num++;
#endif
		}
		else
		{
			ofstream os("video_output/timings.py");
			os << "import matplotlib.pyplot as plt\n";
			os << "import numpy as np\n";
			os << "x = np.linspace(0, " << timings.size() << ", " << timings.size() << ")\n";
			os << "y = ["; 
			for (int i = 0; i < timings.size() - 1; i++) {
				os << timings[i] << ", "; 
			}
			os << timings.back() << "]\n";
			os << "plt.plot(x, y)\n";
			os << "plt.show()\n";
			timings.clear();

			m_record_animation = false;
			m_cam_pos_animator.pause();
			frame_num = 0;
			ImGui::EnableRender();
		}
	}

	ImGui::Begin( "Animations Window" );
	{
		static float time = 0;
		ImGui::InputFloat( "Time", &time, 0.1, 0.5, 1 );
		if ( ImGui::Button( "Add Keyframe" ) )
		{
			m_cam_pos_animator.addKeyFrame( time, *m_cam_pos_animator.getValue() );
		}
		ImGui::BeginChild( "Frames", { 0, 200 }, true );
		{
			size_t to_remove = 0;
			for ( size_t i = 0; i < m_cam_pos_animator.getNumKeyFrames(); ++i )
			{
				ImGui::PushID( i );
				float t = m_cam_pos_animator.getFrameTime( i );
				ImGui::Text( "%d. %f", i, t );
				ImGui::SameLine();
				if ( ImGui::Button( "View" ) )
				{
					m_cam_pos_animator.setCurrentTime( t );
				}
				ImGui::SameLine();
				if ( ImGui::Button( "X" ) )
				{
					to_remove = i+1;
				}
				ImGui::PopID();
			}
			if ( to_remove > 0 )
			{
				m_cam_pos_animator.removeFrame( to_remove - 1 );
			}
			ImGui::SetWindowSize("Frames", ImGui::GetWindowSize());
		}
		ImGui::EndChild();

		ImGui::Separator();

		if ( ImGui::Button( "Clear keyframes" ) )
		{
			m_cam_pos_animator.clearKeyFrames();
		}

		ImGui::Separator();

		if ( ImGui::Button( "Play" ) )
		{
			m_cam_pos_animator.play();
		}
		ImGui::SameLine();
		if ( ImGui::Button( "Pause" ) )
		{
			m_cam_pos_animator.pause();
		}
		ImGui::SameLine();
		if ( ImGui::Button( "Restart" ) )
		{
			m_cam_pos_animator.reset();
		}

		if ( !m_record_animation && ImGui::Button( "Record" ) )
		{
			ImGui::DisableRender();
			m_record_animation = true;
			m_cam_pos_animator.reset();
			m_cam_pos_animator.setCurrentTime( 0 );
			m_cam_pos_animator.play();
		}
		else if ( ImGui::Button( "Stop Recording" ) )
		{
			ImGui::EnableRender();
			m_record_animation = false;
			m_cam_pos_animator.pause();
		}

		if ( ImGui::Button( "Save Keyframes" ) )
		{
			nlohmann::json kf = nlohmann::json::array_t();
			for ( size_t i = 0; i < m_cam_pos_animator.getNumKeyFrames(); ++i )
			{
				nlohmann::json k;
				k["t"] = m_cam_pos_animator.getFrameTime( i );
				k["v"] = m_cam_pos_animator.getFrameValue( i );
				kf.push_back( k );
			}
			std::ofstream kff( "keyframes.json" );
			kff << kf.dump( 1, '\t' );
		}
		ImGui::SameLine();
		if ( ImGui::Button( "Load Keyframes" ) )
		{
			std::ifstream kff( "keyframes.json" );
			if ( kff )
			{
				nlohmann::json kf = nlohmann::json::parse( kff );
				m_cam_pos_animator.clearKeyFrames();
				for ( size_t i = 0; i < kf.size(); ++i )
				{
					auto k = kf[i];
					float t = k.value( "t", 0.f );
					glm::vec3 pos = k.value<glm::vec3>( "v", { 0, 0, 0 } );
					m_cam_pos_animator.addKeyFrame( t, pos );
				}
			}
			else
			{
				LOG_ERROR( "Failed to read keyframes.json" );
			}
		}
	}
	ImGui::End();

	if ( !ImGui::GetIO().WantCaptureKeyboard && ImGui::IsKeyPressed( 'I', false ) )
	{
		if ( ImGui::IsRenderEnabled() )
		{
			ImGui::DisableRender();
		}
		else
		{
			ImGui::EnableRender();
		}
	}
}


void NewApplication::onRenderTabChanged( const std::string& tab )
{
}

#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#include <stdlib.h>

wxIMPLEMENT_WX_THEME_SUPPORT
#if defined( WIN32 )
        int CALLBACK
        WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR cmdline, int ncmdShow )
#else
        int
        main( int argc, char* argv[] )
#endif   // WIN32
{
	// Uncomment following line to debug memory leaks.
	// The number should be set to the first ID in the memory leak dump
	//_CrtSetBreakAlloc( 86019 );

	///////////////////////////////////////////////////////////////////////////
	// Set the locale to the user-defined one, rather than the default C
	///////////////////////////////////////////////////////////////////////////
	Log::SetLocale( std::locale( "" ) );

	///////////////////////////////////////////////////////////////////////////
	// Rerout stdout to a file (which will be read for output to textbox later)
	///////////////////////////////////////////////////////////////////////////
	freopen( "stdout.txt", "w", stdout );

#if defined( WIN32 )
	///////////////////////////////////////////////////////////////////////////
	// Just doing a freopen is not enough since some (CUDA kernel prints in
	// particular) might use the windows APIs for stdout.
	///////////////////////////////////////////////////////////////////////////
	int file_descriptor = _fileno( stdout );
	HANDLE h = (HANDLE)_get_osfhandle( file_descriptor );
	SetStdHandle( STD_OUTPUT_HANDLE, h );
#endif   // WIN32

#if defined( WIN32 )
	auto const code = wxEntry(::GetModuleHandle( NULL ), NULL, ( LPSTR )::GetCommandLine(), SW_SHOWNORMAL );
#else
	auto const code = wxEntry( argc, argv );
#endif   // WIN32

	return code;
}
wxIMPLEMENT_APP_NO_MAIN( NewApplication );
