///////////////////////////////////////////////////////////////////////////////
// Preconvolution of the environment map for surfaces with different BRDFs
///////////////////////////////////////////////////////////////////////////////
shader convolve_env_vert
{
#version 450 compatibility
	out vec2 texcoord;
	void main()
	{
		texcoord = gl_Vertex.xy;
		gl_Position = ftransform();
	}
}
shader convolve_env_frag
{
#line 0
#version 450 compatibility
	const float PI = 3.14159265359;

	in vec2 texcoord;

	layout( binding = 0 ) uniform sampler2D env_map;
	uniform vec3 env_map_multiplier;

	uniform float roughness;
	uniform float max_envmap_value; 
	uniform uint frame_number; 

	const vec3 perp( const vec3 a )
	{
		vec3 r = cross( a, vec3( 0, 0, 1 ) );
		if ( dot( r, r ) < 0.0001 )
		{
			r = cross( a, vec3( 0, 1, 0 ) );
		}
		return r;
	}

	unsigned int tea16( unsigned int val0, unsigned int val1 )
	{
		unsigned int v0 = val0;
		unsigned int v1 = val1;
		unsigned int s0 = 0;

		for ( unsigned int n = 0; n < 16; n++ )
		{
			s0 += 0x9e3779b9;
			v0 += ( ( v1 << 4 ) + 0xa341316c ) ^ ( v1 + s0 ) ^ ( ( v1 >> 5 ) + 0xc8013ea4 );
			v1 += ( ( v0 << 4 ) + 0xad90777d ) ^ ( v0 + s0 ) ^ ( ( v0 >> 5 ) + 0x7e95761e );
		}

		return v0;
	}

	unsigned int lcg( inout unsigned int prev )
	{
		const unsigned int LCG_A = 1664525u;
		const unsigned int LCG_C = 1013904223u;
		prev = ( LCG_A * prev + LCG_C );
		return prev & 0x00FFFFFF;
	}

	float randf_lcg( inout unsigned int prev )
	{
		return ( float( lcg( prev ) ) / float( 0x01000000 ) );
	}


	void main()
	{
		unsigned int seed = tea16( ( uint( 65537 * texcoord.x ) + frame_number + 0x9E3779B9 + uint( 11579 * roughness ) ) * 97,
		                           ( uint( 65537 * texcoord.y ) + frame_number + 0x9E3779B9 + uint( 13417 * ( 1 - roughness ) ) ) * 83 );

		float theta = texcoord.y * PI;
		float phi = texcoord.x * 2.f * PI;
		vec3 n = { sin( theta ) * cos( phi ), sin( theta ) * sin( phi ), cos( theta ) };
		vec3 t = normalize( perp( n ) );
		vec3 b = cross( n, t );

		// Sample GGX lobe around normal
		// NOTE: Code is from GGX importance sampling, for special case of normal incidence
		vec3 sum = vec3( 0.f );
		for ( int s = 0; s < NUM_SAMPLES; s++ )
		{
			float alpha = roughness * roughness;
			float u1 = randf_lcg( seed );
			float u2 = randf_lcg( seed );
			float r = sqrt( u1 / ( 1 - u1 ) );
			float _phi = 2.f * PI * u2;
			vec2 slope = r * vec2( cos( _phi ), sin( _phi ) );
			slope *= alpha;
			vec3 wh = normalize( vec3( -slope.x, -slope.y, 1.0f ) );

			vec3 sdir = -reflect(vec3(0.0, 0.0, 1.0), wh);

			// Out of tangent space
			sdir = sdir.x * t + sdir.y * b + sdir.z * n;
			float Theta = acos( sdir.z );
			float Phi = atan( sdir.y, sdir.x );
			if ( Phi < 0.0f )
				Phi += 2.f * PI;

			// Sample image
			vec2 sUV = vec2( Phi / ( 2.f * PI ), Theta / PI );

			vec4 sv = texture2D( env_map, sUV );
			sum += clamp( env_map_multiplier * sv.xyz, 0.0, max_envmap_value > 0 ? max_envmap_value : 10000000.0);
		}
		gl_FragColor.rgb = sum / float( NUM_SAMPLES );
	}
}

///////////////////////////////////////////////////////////////////////////////
// Render to GBuffer
///////////////////////////////////////////////////////////////////////////////
shader gbuffer_vert
{
#version 400 compatibility
	layout( location = 0 ) in vec3 position;
	layout( location = 1 ) in vec3 normalIn;
	layout( location = 2 ) in vec2 uvIn;
	layout( location = 3 ) in vec3 tangentIn;
	layout( location = 4 ) in vec2 uvLmIn;
	out vec3 world_space_position;
	out vec3 world_space_normal;
	out vec3 world_space_tangent;
	out vec2 uv;
	out vec2 uv_lm;
	uniform mat4 modelMatrix;
	void main()
	{
		world_space_position = ( modelMatrix * vec4( position, 1.0 ) ).xyz;
		world_space_normal = ( modelMatrix * vec4( normalIn, 0.0 ) ).xyz;
		world_space_tangent = ( modelMatrix * vec4( tangentIn, 0.0 ) ).xyz;
		uv = vec2( uvIn.x, uvIn.y );
		uv_lm = mod(uvLmIn, 1);
		gl_Position = gl_ModelViewProjectionMatrix * vec4( world_space_position, 1.0 );
	}
}
shader gbuffer_frag
{
#version 400 compatibility
	///////////////////////////////////////////////////////////////////////////
	// Vertex shader inputs
	///////////////////////////////////////////////////////////////////////////
	in vec3 world_space_position;
	in vec3 world_space_normal;
	in vec3 world_space_tangent;
	in vec2 uv;
	in vec2 uv_lm;
	///////////////////////////////////////////////////////////////////////////
	// Constants
	///////////////////////////////////////////////////////////////////////////
	const float PI = 3.14159265359;
	///////////////////////////////////////////////////////////////////////////
	// Material uniforms
	///////////////////////////////////////////////////////////////////////////
	uniform vec4 base_color_factor;
	uniform bool has_base_color_texture = false;
	uniform sampler2D base_color_texture;
	uniform float alpha_cutoff = 0.5;
	uniform float normal_scale;
	uniform bool has_normal_texture = false;
	uniform sampler2D normal_texture;
	uniform float metal_factor;
	uniform float roughness_factor;
	uniform bool has_metal_roughness_texture = false;
	uniform sampler2D metal_roughness_texture;
	uniform vec3 emissive_factor = vec3( 0.0f, 0.0f, 0.0f );

	uniform bool has_lm_uv;
	uniform vec3 environment_color;

	void main()
	{
		///////////////////////////////////////////////////////////////////////
		// Store diffuse reflectance, either from texture or solid
		///////////////////////////////////////////////////////////////////////
		vec3 color_out;
		if ( has_base_color_texture )
		{
			// Immediately kill fragment if below treshold alpha
			vec4 base_color = texture2D( base_color_texture, uv );
			if ( base_color.w < 0.5 )
				discard;
			color_out = base_color.xyz * base_color_factor.xyz;
		}
		else
		{
			color_out = base_color_factor.xyz;
		}
		gl_FragData[0].xyz = color_out;


		///////////////////////////////////////////////////////////////////////
		// Tangent and UV for envmap reflections
		///////////////////////////////////////////////////////////////////////
		vec3 tangent_out = normalize( world_space_tangent );
		if ( tangent_out.x == 0.0 )
			tangent_out.x = 1e-5;
		gl_FragData[2].xy = vec2( atan( tangent_out.y, tangent_out.x ), tangent_out.z );
		gl_FragData[2].zw = uv_lm;

		// Hacking this in
		if ( emissive_factor != vec3( 0.0 ) )
		{
			gl_FragData[0].xyz = emissive_factor;
			gl_FragData[2] = vec4( 0.0 );
		}


		///////////////////////////////////////////////////////////////////////
		// Store roughness and metallicity.
		///////////////////////////////////////////////////////////////////////
		float roughness_out;
		float metallicity_out;
		if ( has_metal_roughness_texture )
		{
			vec4 metal_rough = texture2D( metal_roughness_texture, uv );
			gl_FragData[0].w = roughness_factor * metal_rough.g;
			gl_FragData[1].w = metal_factor * metal_rough.b;
		}
		else
		{
			gl_FragData[0].w = roughness_factor;
			gl_FragData[1].w = metal_factor;
		}

		//gl_FragData[0].w = max(gl_FragData[0].w, 0.2);

		///////////////////////////////////////////////////////////////////////
		// Generate (possibly normal-mapped) normal and store in two
		// components.
		///////////////////////////////////////////////////////////////////////
		vec3 normal_out = normalize( world_space_normal );
		if ( has_normal_texture )
		{
			vec3 normal_map_value = texture2D( normal_texture, uv ).xyz;
			normal_map_value = normal_map_value * 2.0 - vec3( 1.0 );
			vec3 bitangent = normalize( cross( world_space_normal, world_space_tangent ) );
			// The negation on the x axis is scientifically established by looking at
			// the walls of crytek sponza after using the gimp plugin to generate
			// normal maps
			normal_out = normalize( -normal_map_value.x * normalize( world_space_tangent ) + normal_map_value.y * bitangent
			                        + normal_map_value.z * normalize( world_space_normal ) );
		}
		gl_FragData[1].xyz = normal_out;
	}
}


///////////////////////////////////////////////////////////////////////////////
// Deferred shading
///////////////////////////////////////////////////////////////////////////////
shader deferred_vert
{
#version 450 compatibility
	out vec2 texcoord;
	void main()
	{
		texcoord = gl_Vertex.xy;
		gl_Position = ftransform();
	}
}

shader deferred_frag
{
#version 450 compatibility
	in vec2 texcoord;
	///////////////////////////////////////////////////////////////////////////
	// GBuffer
	///////////////////////////////////////////////////////////////////////////
	layout( binding = 0 ) uniform sampler2D depth_buffer;
	layout( binding = 1 ) uniform sampler2D base_color_roughness_buffer;
	layout( binding = 2 ) uniform sampler2D normal_metal_buffer;
	layout( binding = 3 ) uniform sampler2D tangent_uv_buffer;
	const float M_PI = 3.14159265359;
	///////////////////////////////////////////////////////////////////////////
	// Light uniforms
	///////////////////////////////////////////////////////////////////////////
	uniform vec3 light_position;
	uniform vec3 light_direction;
	uniform vec3 light_radiance;
	uniform float light_radius;
	uniform float light_attenuation_start;
	uniform float light_attenuation_end;
	uniform float light_beam_fov;
	uniform float light_field_fov;
	///////////////////////////////////////////////////////////////////////////
	// Ambient pass uniforms
	///////////////////////////////////////////////////////////////////////////
	uniform bool ambient_pass;
	uniform vec3 environment_color;
	///////////////////////////////////////////////////////////////////////////
	// Camera uniforms
	///////////////////////////////////////////////////////////////////////////
	uniform vec3 camera_position;
	uniform mat4 camera_MVP_inv;

	///////////////////////////////////////////////////////////////////////////
	// SG Textures
	///////////////////////////////////////////////////////////////////////////
	uniform int NUM_GAUSSIANS = 16;
	uniform int NUM_PARAMETERS = 7;
	layout( binding = 4 ) uniform sampler2DArray lightfield_gp_texture0;
	layout( binding = 5 ) uniform sampler2DArray lightfield_gp_texture1;
	layout( binding = 6 ) uniform sampler2DArray env_vis_gp_texture0;
	layout( binding = 7 ) uniform sampler2DArray env_vis_gp_texture1;
	layout( binding = 8 ) uniform sampler3D env_texture;
	uniform bool has_envmap;

	uniform bool has_scene_lightmap = false;
	layout( binding = 9 ) uniform sampler2D scene_lightmap_texture;

	///////////////////////////////////////////////////////////////////////////
	// Debug
	///////////////////////////////////////////////////////////////////////////

	uniform bool debug_show_grid;
	uniform bool debug_show_direction_grid;
	uniform vec2 debug_grid_size;

	///////////////////////////////////////////////////////////////////////////
	// Debug output
	///////////////////////////////////////////////////////////////////////////
	uniform ivec2 debug_pixel;
	layout( std430, binding = 0 ) buffer debug
	{
		float debug_parameters[];
	};
	uniform bool debug_gaussian_from_env_visibility;


	///////////////////////////////////////////////////////////////////////////
	// PBR
	///////////////////////////////////////////////////////////////////////////

	vec3 fresnelSchlick( float cosTheta, vec3 F0 )
	{
		return F0 + ( 1.0 - F0 ) * pow( 1.0 - cosTheta, 5.0 );
	}

	float DistributionGGX( vec3 N, vec3 H, float roughness )
	{
		float a = roughness * roughness;
		float a2 = a * a;
		float NdotH = max( dot( N, H ), 0.0 );
		float NdotH2 = NdotH * NdotH;

		float num = a2;
		float denom = ( NdotH2 * ( a2 - 1.0 ) + 1.0 );
		denom = M_PI * denom * denom;

		return num / denom;
	}

	float GeometrySchlickGGX( float NdotV, float roughness )
	{
		float r = ( roughness + 1.0 );
		float k = ( r * r ) / 8.0;

		float num = NdotV;
		float denom = NdotV * ( 1.0 - k ) + k;

		return num / denom;
	}
	float GeometrySmith( vec3 N, vec3 V, vec3 L, float roughness )
	{
		float NdotV = max( dot( N, V ), 0.0 );
		float NdotL = max( dot( N, L ), 0.0 );
		float ggx2 = GeometrySchlickGGX( NdotV, roughness );
		float ggx1 = GeometrySchlickGGX( NdotL, roughness );

		return ggx1 * ggx2;
	}

	const float PI = 3.1415926535;


#define float3 vec3
#define saturate( N ) clamp( N, 0.0, 1.0 )
	struct SG
	{
		float3 Amplitude;
		float3 Axis;
		float Sharpness;
	};

	float3 EvaluateSG( in SG sg, in float3 dir )
	{
		float cosAngle = dot( dir, sg.Axis );
		return sg.Amplitude * exp( sg.Sharpness * ( cosAngle - 1.0f ) );
	}

	SG SGProduct( in SG x, in SG y )
	{
		float3 um = ( x.Sharpness * x.Axis + y.Sharpness * y.Axis ) / ( x.Sharpness + y.Sharpness );
		float umLength = length( um );
		float lm = x.Sharpness + y.Sharpness;

		SG res;
		res.Axis = um * ( 1.0f / umLength );
		res.Sharpness = lm * umLength;
		res.Amplitude = x.Amplitude * y.Amplitude * exp( lm * ( umLength - 1.0f ) );

		return res;
	}

	float3 SGIntegral( in SG sg )
	{
		float expTerm = 1.0f - exp( -2.0f * sg.Sharpness );
		return 2 * PI * ( sg.Amplitude / sg.Sharpness ) * expTerm;
	}

	float3 ApproximateSGIntegral( in SG sg )
	{
		return 2 * PI * ( sg.Amplitude / sg.Sharpness );
	}

	float3 SGInnerProduct( in SG x, in SG y )
	{
		float umLength = length( x.Sharpness * x.Axis + y.Sharpness * y.Axis );
		float3 expo = exp( umLength - x.Sharpness - y.Sharpness ) * x.Amplitude * y.Amplitude;
		float other = 1.0f - exp( -2.0f * umLength );
		return ( 2.0f * PI * expo * other ) / umLength;
	}

	float SGSharpnessFromThreshold( in float amplitude, in float epsilon, in float cosTheta )
	{
		return ( log( epsilon ) - log( amplitude ) ) / ( cosTheta - 1.0f );
	}

	SG CosineLobeSG( in float3 direction )
	{
		SG cosineLobe;
		cosineLobe.Axis = direction;
		cosineLobe.Sharpness = 2.133f;
		cosineLobe.Amplitude = vec3( 1.17 );

		return cosineLobe;
	}

	float3 SGIrradianceInnerProduct( in SG lightingLobe, in float3 normal )
	{
		SG cosineLobe = CosineLobeSG( normal );
		return max( SGInnerProduct( lightingLobe, cosineLobe ), 0.0f );
	}

	float3 SGDiffuseInnerProduct( in SG lightingLobe, in float3 normal, in float3 albedo )
	{
		float3 brdf = albedo / PI;
		return SGIrradianceInnerProduct( lightingLobe, normal ) * brdf;
	}

	float3 SGIrradianceFitted( in SG lightingLobe, in float3 normal )
	{
		const float muDotN = dot( lightingLobe.Axis, normal );
		const float lambda = lightingLobe.Sharpness;

		const float c0 = 0.36f;
		const float c1 = 1.0f / ( 4.0f * c0 );

		float eml = exp( -lambda );
		float em2l = eml * eml;
		float rl = 1.0 / lambda;

		float scale = 1.0f + 2.0f * em2l - rl;
		float bias = ( eml - em2l ) * rl - em2l;

		float x = sqrt( 1.0f - scale );
		float x0 = c0 * muDotN;
		float x1 = c1 * x;

		float n = x0 + x1;

		float y = saturate( muDotN );
		if ( abs( x0 ) <= x1 )
			y = n * n / x;

		float result = scale * y + bias;

		return result * ApproximateSGIntegral( lightingLobe );
	}

	SG DistributionTermSG( in float3 direction, in float roughness )
	{
		SG distribution;
		distribution.Axis = direction;
		float m2 = roughness * roughness;
		distribution.Sharpness = 2 / m2;
		distribution.Amplitude = vec3(1.0f / ( PI * m2 ));

		return distribution;
	}

	SG WarpDistributionSG( in SG ndf, in float3 view )
	{
		SG warp;

		warp.Axis = reflect( -view, ndf.Axis );
		warp.Amplitude = ndf.Amplitude;
		warp.Sharpness = ndf.Sharpness;
		warp.Sharpness /= ( 4.0f * max( dot( ndf.Axis, view ), 0.0001f ) );

		return warp;
	}

    struct ASG
	{
		float3 Amplitude;
		float3 BasisZ;
		float3 BasisX;
		float3 BasisY;
		float SharpnessX;
		float SharpnessY;
	};

	float3 EvaluateASG( in ASG asg, in float3 dir )
	{
		float sTerm = saturate( dot( asg.BasisZ, dir ) );
		float lambdaTerm = asg.SharpnessX * dot( dir, asg.BasisX ) * dot( dir, asg.BasisX );
		float muTerm = asg.SharpnessY * dot( dir, asg.BasisY ) * dot( dir, asg.BasisY );
		return asg.Amplitude * sTerm * exp( -lambdaTerm - muTerm );
	}

    float3 ConvolveASG_SG( in ASG asg, in SG sg )
	{
		// The ASG paper specifes an isotropic SG as
		// exp(2 * nu * (dot(v, axis) - 1)),
		// so we must divide our SG sharpness by 2 in order
		// to get the nup parameter expected by the ASG formula
		float nu = sg.Sharpness * 0.5f;

		ASG convolveASG;
		convolveASG.BasisX = asg.BasisX;
		convolveASG.BasisY = asg.BasisY;
		convolveASG.BasisZ = asg.BasisZ;

		convolveASG.SharpnessX = ( nu * asg.SharpnessX ) / ( nu + asg.SharpnessX );
		convolveASG.SharpnessY = ( nu * asg.SharpnessY ) / ( nu + asg.SharpnessY );

		convolveASG.Amplitude = vec3(PI / sqrt( ( nu + asg.SharpnessX ) * ( nu + asg.SharpnessY ) ));

		float3 asgResult = EvaluateASG( convolveASG, sg.Axis );
		return asgResult * sg.Amplitude * asg.Amplitude;
	}

	ASG WarpDistributionASG( in SG ndf, in float3 view )
	{
		ASG warp;

		// Generate any orthonormal basis with Z pointing in the
		// direction of the reflected view vector
		warp.BasisZ = reflect( -view, ndf.Axis );
		warp.BasisX = normalize( cross( ndf.Axis, warp.BasisZ ) );
		warp.BasisY = normalize( cross( warp.BasisZ, warp.BasisX ) );

		float dotDirO = max( dot( view, ndf.Axis ), 0.0001f );

		// Second derivative of the sharpness with respect to how
		// far we are from basis Axis direction
		warp.SharpnessX = ndf.Sharpness / ( 8.0f * dotDirO * dotDirO );
		warp.SharpnessY = ndf.Sharpness / 8.0f;

		warp.Amplitude = ndf.Amplitude;

		return warp;
	}










	float GGX_Lambda( in float alpha, in float absTanTh )
	{
		if ( isinf( absTanTh ) )
			return 0.f;
		float a2t2th = alpha * absTanTh * alpha * absTanTh;
		return ( -1 + sqrt( 1.f + a2t2th ) ) / 2.f;
	}

	void GetPixelFromThetaPhi( out float pixel_x, out float pixel_y, const float theta, const float phi )
	{
		const int ENVMAP_SIZE = 128;
		float phi0 = phi - M_PI;
		float theta0 = theta;
		//
		float X0 = sin( theta0 ) * cos( phi0 );
		float Y0 = sin( theta0 ) * sin( phi0 );
		float Z0 = cos( theta0 );
		//
		bool upper_hemisphere = Z0 > 0.0f;
		Z0 = abs( Z0 );
		//
		float u0 = X0 / sqrt( 1.0f + Z0 );
		float v0 = Y0 / sqrt( 1.0f + Z0 );
		//
		float r0 = sqrt( u0 * u0 + v0 * v0 );
		float phi1 = atan( v0, u0 );
		phi1 /= 2.0f;
		float u1 = r0 * cos( phi1 );
		float v1 = r0 * sin( phi1 );
		//
		if ( upper_hemisphere )
			u1 *= -1.0f;
		//
		float r1 = sqrt( u1 * u1 + v1 * v1 );
		float phi2 = atan( v1, u1 );
		if ( phi2 < -M_PI / 4.0f )
			phi2 += 2.0f * M_PI;   // in range [-pi/4,7pi/4]
		float a, b, x, y;
		if ( phi2 < M_PI / 4.0f )
		{   // region 1
			a = r1;
			b = phi2 * a / ( M_PI / 4.0f );
		}
		else if ( phi2 < 3 * M_PI / 4.0f )
		{   // region 2
			b = r1;
			a = -( phi2 - M_PI / 2.0f ) * b / ( M_PI / 4.0f );
		}
		else if ( phi2 < 5 * M_PI / 4 )
		{   // region 3
			a = -r1;
			b = ( phi2 - M_PI ) * a / ( M_PI / 4.0f );
		}
		else
		{   // region 4
			b = -r1;
			a = -( phi2 - 3 * M_PI / 2.0f ) * b / ( M_PI / 4.0f );
		}
		x = a;   // (a + 1) / 2.0f;
		y = b;   // (b + 1) / 2.0f;
		//
		x += 1.0f;
		if ( x > 1.0f )
			x -= 2.0f;
		pixel_x = ( x + 1.0f ) * 0.5f * ENVMAP_SIZE;
		pixel_y = ( y + 1.0f ) * 0.5f * ENVMAP_SIZE;
	}


	void main()
	{
		///////////////////////////////////////////////////////////////////////
		// Draw environment for empty pixels
		///////////////////////////////////////////////////////////////////////
		if ( texture( normal_metal_buffer, texcoord ).xyz == vec3( 0.0 ) )
		{
			vec4 ndc_coord = vec4( texcoord.x * 2.0 - 1, texcoord.y * 2.0 - 1, 1.0, 1.0 );
			vec4 world_coord = camera_MVP_inv * ndc_coord;
			vec3 dir = normalize( ( world_coord * ( 1.0 / world_coord.w ) ).xyz - camera_position );

			float theta = acos( dir.y );
			float phi = atan( dir.z, dir.x );

			if ( !has_envmap )
			{
				gl_FragColor.xyz = environment_color;
			}
			else
			{
				// environment color and intensity is in texture
				gl_FragColor.xyz = texture( env_texture, vec3( phi / ( 2.0 * PI ), theta / PI, 0.2 ) ).xyz;
			}
			return;
		}

		///////////////////////////////////////////////////////////////////////
		// Fetch base color and roughness, encoded normal and metallicity
		///////////////////////////////////////////////////////////////////////
		vec4 color_roughness = texture( base_color_roughness_buffer, texcoord );
		vec3 base_color = color_roughness.xyz;
		float roughness_factor = color_roughness.w;

		vec4 normal_metal = texture( normal_metal_buffer, texcoord );
		vec3 normal = normal_metal.xyz;
		float metal_factor = normal_metal.w;

		///////////////////////////////////////////////////////////////////////
		// Decode position
		///////////////////////////////////////////////////////////////////////
		vec4 mvp_pos =
		        vec4( texcoord.x * 2.0 - 1.0, texcoord.y * 2.0 - 1.0, texture( depth_buffer, texcoord ).x * 2.0 - 1.0, 1.0 );
		vec4 hposition = camera_MVP_inv * mvp_pos;
		vec3 position = ( 1.0 / hposition.w ) * hposition.xyz;
		///////////////////////////////////////////////////////////////////////
		// Decode tangent
		///////////////////////////////////////////////////////////////////////
		vec3 tangent;
		{
			vec2 enc = texture( tangent_uv_buffer, texcoord ).xy;
			vec2 scth = vec2( sin( enc.x ), cos( enc.x ) );
			vec2 scphi = vec2( sqrt( 1.0 - enc.y * enc.y ), enc.y );
			tangent = normalize( vec3( scth.y * scphi.x, scth.x * scphi.x, scphi.y ) );
		}

		///////////////////////////////////////////////////////////////////////
		// Clamp normal to be front facing
		///////////////////////////////////////////////////////////////////////
		{
			vec3 wo = normalize(camera_position - position);
			const float dist = dot(wo, normal);
			const float grazing = 0.15f;
			if (dist < grazing)
			{
				normal = normalize(normal - dist * wo);
				normal = normalize(normal + grazing * wo);
			}
		}

		///////////////////////////////////////////////////////////////////////////
		// Adjust shading tangent
		///////////////////////////////////////////////////////////////////////////
		{
			float d = dot(tangent, normal);
			tangent = normalize(tangent - d * normal);
		}

		///////////////////////////////////////////////////////////////////////
		// Calculate fresnel value
		///////////////////////////////////////////////////////////////////////
		vec3 F0 = vec3( 0.04 );
		F0 = mix( F0, base_color, metal_factor );

		///////////////////////////////////////////////////////////////////////
		// This same shader is currently used for the ambient pass, where
		// we do some indirect illumination approximation (currently just
		// ambient)
		///////////////////////////////////////////////////////////////////////
		vec2 uv = texture(tangent_uv_buffer, texcoord).zw;

		if ( uv == vec2( 0.0f ) )
		{
			gl_FragColor.xyz = base_color;
			return;
		}

		///////////////////////////////////////////////////////////////////////
		// Load irradiance from texture
		///////////////////////////////////////////////////////////////////////
		vec3 irradiance = texture( scene_lightmap_texture, uv ).xyz;

		if ( ambient_pass )
		{
			vec3 cosine_lobe_axis = normal;
			float cosine_lobe_sharpness = 2.133;
			float cosine_lobe_amplitude = 1.17;

			vec3 V = normalize( camera_position - position );

			vec3 R = normalize( reflect( -V, normal ) );

			// View, reflection direction and halfvector
			vec3 wo = V;
			vec3 wi = R;

			vec3 wh = normal;


			if ( ivec2( gl_FragCoord.xy ) == debug_pixel )
			{
				debug_parameters[NUM_GAUSSIANS * NUM_PARAMETERS + 0] = R.x;
				debug_parameters[NUM_GAUSSIANS * NUM_PARAMETERS + 1] = R.y;
				debug_parameters[NUM_GAUSSIANS * NUM_PARAMETERS + 2] = R.z;
				debug_parameters[NUM_GAUSSIANS * NUM_PARAMETERS + 3] = uv.x;
				debug_parameters[NUM_GAUSSIANS * NUM_PARAMETERS + 4] = uv.y;
				debug_parameters[NUM_GAUSSIANS * NUM_PARAMETERS + 5] = normal.x;
				debug_parameters[NUM_GAUSSIANS * NUM_PARAMETERS + 6] = normal.y;
				debug_parameters[NUM_GAUSSIANS * NUM_PARAMETERS + 7] = normal.z;
			}

			vec3 diffuse_sum = vec3( 0.0 );
			vec3 D_sum = vec3( 0.0 );


			vec3 direct_sum = vec3( 0.0 );

			// Assuming that alpha is roughness squared and m2 is alpha squared
			float alpha = roughness_factor * roughness_factor;

			for ( int i = 0; i < NUM_GAUSSIANS; i++ )
			{
				// "Light" SG
				SG light;
				light.Axis = normalize( texture( lightfield_gp_texture0, vec3( uv.x, uv.y, i ) ).xyz );
				light.Sharpness = texture( lightfield_gp_texture0, vec3( uv.x, uv.y, i ) ).w;
			    light.Amplitude = texture( lightfield_gp_texture1, vec3( uv.x, uv.y, i ) ).xyz;

                // Parameters for debug
				if ( !debug_gaussian_from_env_visibility && ivec2( gl_FragCoord.xy ) == debug_pixel )
				{
					debug_parameters[i * NUM_PARAMETERS + 0] = light.Axis.x;
					debug_parameters[i * NUM_PARAMETERS + 1] = light.Axis.y;
					debug_parameters[i * NUM_PARAMETERS + 2] = light.Axis.z;
					debug_parameters[i * NUM_PARAMETERS + 3] = light.Sharpness;
					debug_parameters[i * NUM_PARAMETERS + 4] = light.Amplitude.x;
					debug_parameters[i * NUM_PARAMETERS + 5] = light.Amplitude.y;
					debug_parameters[i * NUM_PARAMETERS + 6] = light.Amplitude.z;
				}

                // Create an SG that approximates the NDF.
				SG ndf = DistributionTermSG( normal, alpha );

#if 0
                // Warp the distribution so that our lobe is in the same   
				// domain as the lighting lobe
				SG warpedNDF = WarpDistributionSG( ndf, wo );
                vec3 D = SGInnerProduct( warpedNDF, light );
				float nDotL = clamp(dot(wh, warpedNDF.Axis), 0.0, 1.0);
#else
				// Apply a warpring operation that will bring the SG from
				// the half-angle domain the the the lighting domain.
				ASG warpedNDF = WarpDistributionASG( ndf, wo );

				// Convolve the NDF with the light
				vec3 D = ConvolveASG_SG( warpedNDF, light );
				float nDotL = clamp(dot(wh, warpedNDF.BasisZ), 0.0, 1.0);
#endif
				float nDotV = clamp(dot(wh, wo), 0.0, 1.0);
				float absTanThetaV = abs(tan(acos(nDotV)));
				float absTanThetaL = abs(tan(acos(nDotL)));
				float G = 1 / (1 + GGX_Lambda(alpha, absTanThetaV) + GGX_Lambda(alpha, absTanThetaL));
				vec3 F = fresnelSchlick(nDotL, F0);
				float d = (4.f * nDotL *nDotV);

				D_sum += F * G * D / d * nDotL; // dot(wh, wo);
				direct_sum += EvaluateSG(light, wi);
			}

			//gl_FragColor.xyz = direct_sum; 
			//return; 
			//float nDotL = clamp( dot( wh, wi ), 0.0, 1.0 );
			//float nDotV = clamp( dot( wh, wo ), 0.0, 1.0 );
			//// Calculate F for reflection direction only
			//vec3 F = fresnelSchlick( nDotV, F0 );
			//float d = ( 4.f * nDotL * nDotV );
			//// Calculate G for reflection direction only
			//float absTanThetaV = abs( tan( acos( nDotV ) ) );
			//float absTanThetaL = abs( tan( acos( nDotL ) ) );
			//float G = 1 / ( 1 + GGX_Lambda( alpha, absTanThetaV ) + GGX_Lambda( alpha, absTanThetaL ) );
			//D_sum = min( D_sum, vec3( 1 ) );
			vec3 refl_glossy_term = D_sum; // (F * G * D_sum / d);
			//vec3 refl_glossy_term = D_sum;


			vec3 visibility_factor = vec3( 0.0 );

			for ( int i = 0; i < NUM_GAUSSIANS; i++ )
			{
				SG sg; 
				sg.Axis = normalize( texture( env_vis_gp_texture0, vec3( uv.x, uv.y, i ) ).xyz );
				sg.Sharpness = texture( env_vis_gp_texture0, vec3( uv.x, uv.y, i ) ).w;
				sg.Amplitude = texture( env_vis_gp_texture1, vec3( uv.x, uv.y, i ) ).xyz;
				visibility_factor += EvaluateSG( sg, wi );


				// Parameters for debug
				if ( debug_gaussian_from_env_visibility && ivec2(gl_FragCoord.xy) == debug_pixel)
				{
					debug_parameters[i * NUM_PARAMETERS + 0] = sg.Axis.x;
					debug_parameters[i * NUM_PARAMETERS + 1] = sg.Axis.y;
					debug_parameters[i * NUM_PARAMETERS + 2] = sg.Axis.z;
					debug_parameters[i * NUM_PARAMETERS + 3] = sg.Sharpness;
					debug_parameters[i * NUM_PARAMETERS + 4] = sg.Amplitude.x;
					debug_parameters[i * NUM_PARAMETERS + 5] = sg.Amplitude.y;
					debug_parameters[i * NUM_PARAMETERS + 6] = sg.Amplitude.z;
				}
				// visibility_factor += A * SG( wi, axis, s );
			}

			// This is a sharpening of the visibility HACK. But we must avoid light leaks.
			visibility_factor = clamp( ( ( ( visibility_factor - 0.5 ) * 1.2 ) + 0.5 ), 0, 1 );

			float nDotL = clamp( dot( wh, wi ), 0.0, 1.0 );
			float nDotV = clamp( dot( wh, wo ), 0.0, 1.0 );
			// Calculate F for reflection direction only
			vec3 F = fresnelSchlick( nDotV, F0 );
			// Calculate G for reflection direction only
			float absTanThetaV = abs( tan( acos( nDotV ) ) );
			float absTanThetaL = abs( tan( acos( nDotL ) ) );
			float G = 1 / ( 1 + GGX_Lambda( alpha, absTanThetaV ) + GGX_Lambda( alpha, absTanThetaL ) );


			vec3 env_glossy_term;
			{
				float theta = acos( wi.y );
				float phi = atan( wi.z, wi.x );
				// Transform to get more precision at lower roughnesses (inversed in convolution code)
				float roughness = pow(roughness_factor, 1.0 / 2.2);
				vec3 envmap = texture( env_texture, vec3( phi / ( 2.0 * PI ), theta / PI, roughness ) ).xyz;
				env_glossy_term = F * G * visibility_factor * envmap;
				//gl_FragColor.xyz = env_glossy_term;
				////gl_FragColor.xyz = vec3(F) * G * visibility_factor * envmap; // G * visibility_factor * envmap;
				//return; 
			}

			vec3 diffuse_term = (1 - metal_factor) * (1 - F) * base_color * irradiance;


			gl_FragColor.xyz = refl_glossy_term + env_glossy_term + diffuse_term;
			//gl_FragColor.xyz = diffuse_term + env_glossy_term;
			//gl_FragColor.xyz = env_glossy_term + diffuse_term;
			//gl_FragColor.xyz = refl_glossy_term + env_glossy_term;
			//gl_FragColor.xyz = tangent; 


			if (ivec2(gl_FragCoord.xy) == debug_pixel) {
				gl_FragColor.xyz = vec3(1.0, 0.0, 0.0);
			}


			if ( debug_show_grid )
			{
				if ( fract( uv.x * debug_grid_size.x ) < 0.05 || fract( uv.y * debug_grid_size.y ) < 0.05 )
				{
					gl_FragColor.xyz *= 1.1;
					return;
				}
			}

			if ( debug_show_direction_grid )
			{
				float theta = acos( wi.y );
				float phi = atan( wi.z, wi.x );
				float X, Y;
				GetPixelFromThetaPhi( X, Y, theta, phi );
				if ( abs( X - round( X ) ) < 0.01 || abs( Y - round( Y ) ) < 0.01 )
				{
					gl_FragColor.xyz *= 0.9;
				}
			}
		}
	}
}
