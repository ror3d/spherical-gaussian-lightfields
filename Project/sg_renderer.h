#pragma once

#include "../renderer/irenderer.h"

namespace chag
{
class SGRenderer : public IRenderer
{
public:
	SGRenderer();

	virtual void init() override;

	virtual void resize( uint16_t width, uint16_t height ) override;

	virtual void render() override;

	virtual void destroy() override;

	void reloadShaders();

	glm::ivec2 debug_pixel; 
	bool debug_pixel_changed = false; 
	void setDebugPixel(int x, int y) { debug_pixel = { x, y }; debug_pixel_changed = true; };

	int m_texture_size;
	int m_num_gaussians;
	int m_num_parameters;
	uint32_t debug_ssbo = -1; 

	uint32_t m_SGLightfieldTexture0 = 0, m_SGLightfieldTexture1 = 0; 
	uint32_t m_SGEnvVisTexture0 = 0, m_SGEnvVisTexture1 = 0; 

	std::string base_data_path; 
	std::vector<std::string> data_filenames; 
	std::vector<std::pair<int /*filename index*/, int /*index in file*/>> uv_to_file; 

private:
	bool tryLoadSGFile( const std::string& file, GLuint& sg_tex0, GLuint& sg_tex1 );
	std::string loadSGFileDialog( GLuint& sg_tex0, GLuint& sg_tex1 );

	bool convolveEnvMap();

	bool loadSGFile( const std::string& file, const std::string& json_file, GLuint& sg_tex0, GLuint& sg_tex1 );

	void drawDebugThings();

private:
	GLSLProgramObject* m_deferred_program = nullptr;
	GLSLProgramObject* m_gbuffer_program = nullptr;
	GLuint m_gbuffer = 0;

	GLSLProgramObject* m_convolve_envmap_program = nullptr;
	scene::Texture* m_current_envmap = nullptr;
	GLuint m_convolved_envmap = 0;
	unsigned int m_total_envmap_samples = 4096;
	unsigned int m_total_envmap_levels = 6;
	unsigned int m_convolved_envmap_samples = 0;
	unsigned int m_convolved_envmap_levels = 0;
	GLuint m_convolving_envmap_fbf = 0;

	std::string m_lightfield_file;
	std::string m_visibility_file;
	bool m_sg_files_changed = false;

	struct
	{
		bool show_grid = false;
		bool show_direction_grid = false;
		bool show_env_visibility = false;
	} m_debug;
};
}
