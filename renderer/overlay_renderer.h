#pragma once

#include "irenderer.h"

namespace chag
{

class OverlayRenderer : public IRenderer
{
public:
	OverlayRenderer( GLSLProgramManager* program_manager ) : IRenderer( program_manager ) {}

	virtual void init() override;

	virtual void resize( uint16_t width, uint16_t height ) override;

	virtual void render() override;

	virtual void destroy() override;

private:
	void renderLights();
	void renderAxes();
};

}
