#pragma once

#include "utils/GLSLProgramManager.h"
#ifndef NO_SCENE
#include <scene/Scene.h>
#endif

class wxWindow;

namespace chag
{

class IRenderer
{
public:
	IRenderer( GLSLProgramManager* program_manager ) : m_program_manager( program_manager ) {}
	virtual ~IRenderer() {}

	void setParentWindow( wxWindow* window ) { m_parentWindow = window; }

	virtual void init() = 0;

	virtual void resize( uint16_t width, uint16_t height ) = 0;

	virtual bool resizeOnContextResized() const { return true; }

#ifndef NO_SCENE
	virtual void updateScene( scene::Scene* scene ) { m_scene = scene; }
#endif

	virtual void updateView() {}

	virtual void render() = 0;

	virtual void destroy() = 0;

protected:
	GLSLProgramManager* m_program_manager = nullptr;

	wxWindow* m_parentWindow = nullptr;

#ifndef NO_SCENE
	scene::Scene* m_scene = nullptr;
#endif
};

}
