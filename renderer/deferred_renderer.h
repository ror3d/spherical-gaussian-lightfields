#pragma once

#include "irenderer.h"

namespace chag
{

class DeferredRenderer : public IRenderer
{
public:
	DeferredRenderer( GLSLProgramManager* program_manager ) : IRenderer( program_manager ) {}

	virtual void init() override;

	virtual void resize( uint16_t width, uint16_t height ) override;

	virtual void updateScene( scene::Scene *scene ) override;

	virtual void render() override;

	virtual void destroy() override;

private:
	GLSLProgramObject* m_deferred_program = nullptr;
	GLSLProgramObject* m_gbuffer_program = nullptr;
	GLuint m_gbuffer = 0;

	glm::vec3 m_envAvgColor;
	scene::Texture* m_oldEnvmap = nullptr;
};

}
