#include "overlay_renderer.h"

#include <utils/opengl_helpers.h>

void chag::OverlayRenderer::init()
{
}

void chag::OverlayRenderer::resize( uint16_t width, uint16_t height )
{
}

void chag::OverlayRenderer::render()
{
#ifndef NO_SCENE
	auto currentCamera = m_scene->getCurrentRenderableItem()->getView();

	currentCamera->push();
	{
		// Draw the light geometry
		renderLights();
		//renderAxes();
	}
	currentCamera->pop();
#endif
}

void chag::OverlayRenderer::destroy()
{
}

void chag::OverlayRenderer::renderLights()
{
	using namespace scene;
	using namespace glm;
	using namespace opengl_helpers;

	for(auto light : m_scene->m_lights)
	{
		if ( m_scene->getCurrentControllableItem() == (scene::Item*)light )
		{
			continue;
		}
		if(light->m_shape.type == Light::Shape::Disc && light->m_distribution.type == Light::Distribution::Diffuse)
		{
			// TODO: Create better geometry here
			geometry::sphere( light->m_transform.pos, m_scene->m_sceneDebugGeomSizeMultiplier, vec3{ 1, 0, 1 } );
		}
		else if(light->m_shape.type == Light::Shape::Rectangle && light->m_distribution.type == Light::Distribution::Diffuse)
		{
			// TODO: Create better geometry here
			geometry::sphere( light->m_transform.pos, m_scene->m_sceneDebugGeomSizeMultiplier, vec3{ 1, 0, 1 } );
		}
		else if (light->m_shape.type == Light::Shape::Point && light->m_distribution.type == Light::Distribution::Diffuse)
		{
			geometry::sphere( light->m_transform.pos, m_scene->m_sceneDebugGeomSizeMultiplier, light->m_color );
			geometry::disk( light->m_transform.pos, m_scene->m_sceneDebugGeomSizeMultiplier * 16, vec3{ 1, 0, 0 }, vec3{ 1, 1, 0 }, false );
			geometry::disk( light->m_transform.pos, m_scene->m_sceneDebugGeomSizeMultiplier * 16, vec3{ 0, 1, 0 }, vec3{ 1, 1, 0 }, false );
			geometry::disk( light->m_transform.pos, m_scene->m_sceneDebugGeomSizeMultiplier * 16, vec3{ 0, 0, 1 }, vec3{ 1, 1, 0 }, false );
		}
		else if (light->m_shape.type == Light::Shape::Point && light->m_distribution.type == Light::Distribution::Spot)
		{
			// TODO: Create better geometry here
			geometry::sphere( light->m_transform.pos, m_scene->m_sceneDebugGeomSizeMultiplier, vec3{ 1, 0, 1 } );
		}

	}

}

void chag::OverlayRenderer::renderAxes()
{
	using namespace glm;
	using namespace opengl_helpers;
	geometry::line( vec3( 0 ), m_scene->m_sceneDebugGeomSizeMultiplier * vec3( 1, 0, 0 ), vec3( 1, 0, 0 ) );
	geometry::line( vec3( 0 ), m_scene->m_sceneDebugGeomSizeMultiplier * vec3( 0, 1, 0 ), vec3( 0, 1, 0 ) );
	geometry::line( vec3( 0 ), m_scene->m_sceneDebugGeomSizeMultiplier * vec3( 0, 0, 1 ), vec3( 0, 0, 1 ) );
}
