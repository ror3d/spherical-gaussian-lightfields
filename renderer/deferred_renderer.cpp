#include "deferred_renderer.h"
#include <utils/opengl_helpers.h>
#include "CHAGApp/PerformanceProfiler.h"


namespace chag
{
void DeferredRenderer::init()
{
	m_gbuffer_program = m_program_manager->getProgramObject( "gbuffer_vert", "gbuffer_frag" );
	m_deferred_program = m_program_manager->getProgramObject( "deferred_vert", "deferred_frag" );
}

void DeferredRenderer::resize( uint16_t width, uint16_t height )
{
	if ( m_gbuffer == 0 )
	{
		// Create the GBuffer
		m_gbuffer = opengl_helpers::framebuffer::createFramebuffer(
		        width,
		        height,
		        opengl_helpers::texture::createTexture( width, height, GL_DEPTH_COMPONENT32 ),   // Depth buffer (position)
		        {
		                opengl_helpers::texture::createTexture( width, height, GL_RGBA16F ),   // (diffuse) reflectance,
		                                                                                       // roughness
		                opengl_helpers::texture::createTexture( width, height, GL_RGBA16F ),   // (spherical coords) normal,
		                                                                                       // _unused, metal_factor
		                opengl_helpers::texture::createTexture( width, height, GL_RGBA16F ),   // Ambient Light value
		        } );
	}
	else {
		opengl_helpers::framebuffer::resizeFramebuffer(m_gbuffer, width, height);
	}
}

void DeferredRenderer::updateScene( scene::Scene *scene )
{
	IRenderer::updateScene( scene );

	if ( scene->m_environmentMap == nullptr )
	{
		m_envAvgColor = glm::vec3( 1 );
	}
	else if ( scene->m_environmentMap != m_oldEnvmap )
	{
		// TODO: Should we do the same we do in the SG Renderer where we preconvolve the environment map according to different BRDFs so we
		// can get nicer results? might be nice.

		m_oldEnvmap = scene->m_environmentMap;

		m_envAvgColor = glm::vec3( 0 );
		for ( size_t j = 0; j < scene->m_environmentMap->m_height; ++j )
		{
			for ( size_t i = 0; i < scene->m_environmentMap->m_width; ++i )
			{
				m_envAvgColor += scene->m_environmentMap->getRawValue( { i, j } );
			}
		}
		m_envAvgColor /= scene->m_environmentMap->m_width * scene->m_environmentMap->m_height;
	}
}

void DeferredRenderer::render()
{
#ifndef NO_SCENE
	auto currentCamera = m_scene->getCurrentRenderableItem()->getView();
	///////////////////////////////////////////////////////////////////////
	// Update Shadow Maps
	///////////////////////////////////////////////////////////////////////
	{	PROFILE_GL("Update Shadow Maps");
		m_scene->updateShadowGenerators();
	}
	///////////////////////////////////////////////////////////////////////
	// Generate GBuffer
	///////////////////////////////////////////////////////////////////////
	{	PROFILE_GL("Generate GBuffer");
		opengl_helpers::framebuffer::pushFramebuffer(m_gbuffer);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		currentCamera->push();
		m_gbuffer_program->enable();
		if ( m_scene->m_lightmap )
		{
			m_gbuffer_program->setUniform( "has_scene_lightmap", true );
			m_gbuffer_program->bindTexture( "scene_lightmap_texture", m_scene->m_lightmap->m_GLID, GL_TEXTURE_2D, 3 );
		}
		else
		{
			m_gbuffer_program->setUniform( "has_scene_lightmap", false );
		}
		// We multiply by 0.5 because each surface will get light at most from half the hemisphere
		m_gbuffer_program->setUniform("environment_color", m_scene->m_environment_color * m_envAvgColor * m_scene->m_envmap_intensity_multiplier * 0.5f);

		m_scene->submitModels(m_gbuffer_program);
		m_gbuffer_program->disable();
		currentCamera->pop();
		opengl_helpers::framebuffer::popFramebuffer();
	}

	///////////////////////////////////////////////////////////////////////
	// Deferred shading
	///////////////////////////////////////////////////////////////////////
	{	PROFILE_GL("Deferred shading");
		glPushAttrib(GL_ALL_ATTRIB_BITS);
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDisable(GL_DEPTH_TEST);
		m_deferred_program->enable();
		m_deferred_program->setUniform("camera_MVP_inv", currentCamera->get_MVP_inv());
		m_deferred_program->setUniform("camera_position", currentCamera->pos);
		size_t next_buffer_binding = 0;
		opengl_helpers::framebuffer::bindFramebufferDepthBuffer(m_gbuffer, next_buffer_binding++);
		next_buffer_binding += opengl_helpers::framebuffer::bindFramebufferColorBuffers(m_gbuffer, next_buffer_binding);
		// Initialize with an "ambient" pass (or whatever GI approximation is suitable)
		m_deferred_program->setUniform("ambient_pass", true);
		m_deferred_program->setUniform("environment_color", m_scene->m_environment_color * m_scene->m_envmap_intensity_multiplier);
		m_deferred_program->setUniform( "has_envmap", m_scene->m_environmentMap != nullptr );
		if ( m_scene->m_environmentMap )
		{
			m_deferred_program->bindTexture( "env_texture", m_scene->m_environmentMap->m_GLID, GL_TEXTURE_2D, next_buffer_binding );
		}
		++next_buffer_binding;

		m_deferred_program->bindTexture("light_shadowmap_sampler", opengl_helpers::texture::getDummyShadowmap(), GL_TEXTURE_2D, next_buffer_binding );

		view::drawFullScreenQuad();
		m_deferred_program->setUniform("ambient_pass", false);
		// One renderpass per light, with additive blending.
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_ONE, GL_ONE);
		for (auto light : m_scene->m_lights) {
			scene::ShadowGenerator *shadow_generator = NULL;
			for (auto sg : m_scene->m_shadowGenerators) {
				if (sg->m_light == light) shadow_generator = sg;
			}
			/*
			scene::ShadowMapShadowGenerator *shadow_map_generator = dynamic_cast<scene::ShadowMapShadowGenerator *>(shadow_generator);
			if (light->m_shape.type != scene::Light::Shape::Disc ||
				light->m_distribution.type != scene::Light::Distribution::Diffuse) {
				//LOG_WARNING("Not a Disc Diffuse Light!");
			}
			if (shadow_map_generator) {
				m_deferred_program->setUniform("light_has_shadowmap", true);
				m_deferred_program->bindTexture("light_shadowmap_sampler", shadow_map_generator->m_depth_texture, GL_TEXTURE_2D, next_buffer_binding );
				m_deferred_program->setUniform("light_shadowmap_mvp", shadow_map_generator->getView()->get_MVP());
			}
			else
			*/
			{
				m_deferred_program->setUniform("light_has_shadowmap", false);
				m_deferred_program->bindTexture("light_shadowmap_sampler", opengl_helpers::texture::getDummyShadowmap(), GL_TEXTURE_2D, next_buffer_binding );
			}
			m_deferred_program->setUniform("light_position",   light->m_transform.pos);
			m_deferred_program->setUniform("light_direction", -light->m_transform.R[2]);
			m_deferred_program->setUniform("light_radiance",   light->m_radiance * light->m_color);
			m_deferred_program->setUniform("light_radius",     light->m_shape.Disc.m_radius);
			view::drawFullScreenQuad();
		}

		m_deferred_program->disable();
		glPopAttrib();
	}

	{
	// Copy depth buffer from gbuffer
		int w = opengl_helpers::framebuffer::getFramebufferDefaultWidth( m_gbuffer );
		int h = opengl_helpers::framebuffer::getFramebufferDefaultHeight( m_gbuffer );
		glBlitNamedFramebuffer( m_gbuffer, opengl_helpers::framebuffer::currentFramebuffer(),
								0, 0, w, h, 0, 0, w, h, GL_DEPTH_BUFFER_BIT, GL_NEAREST );
	}
#endif
}

void DeferredRenderer::destroy()
{
}

}   // namespace chag
