///////////////////////////////////////////////////////////////////////////////
// Copy texture to screen
///////////////////////////////////////////////////////////////////////////////
shader copy_vert
{
	#version 420 compatibility
	out vec2 texcoord; 
	void main()
	{
		texcoord = gl_Vertex.xy; 
		gl_Position = ftransform();
	}
}

shader copy_frag
{
	#version 420 compatibility
	in vec2 texcoord;
	layout(binding = 0)uniform sampler2D renderbuffer; 
	void main()
	{
		gl_FragColor.xyz = texture(renderbuffer, texcoord).xyz;
	}
}
///////////////////////////////////////////////////////////////////////////////
// Render to GBuffer 
///////////////////////////////////////////////////////////////////////////////
shader gbuffer_vert
{
#version 400 compatibility
	layout(location = 0) in vec3 position;
	layout(location = 1) in vec3 normalIn;
	layout(location = 2) in vec2 uvIn;
	layout(location = 3) in vec3 tangentIn;
	layout(location = 4) in vec2 uvLmIn;
	out vec3 world_space_position;
	out vec3 world_space_normal;
	out vec3 world_space_tangent;
	out vec2 uv;
	out vec2 uv_lm;
	uniform mat4 modelMatrix;
	void main()
	{
		world_space_position = (modelMatrix * vec4(position, 1.0)).xyz;
		world_space_normal = (modelMatrix * vec4(normalIn, 0.0)).xyz;
		world_space_tangent = (modelMatrix * vec4(tangentIn, 0.0)).xyz;
		uv = vec2( uvIn.x, uvIn.y );
		uv_lm = uvLmIn;
		gl_Position = gl_ModelViewProjectionMatrix * vec4(world_space_position, 1.0);
	}
}
shader gbuffer_frag
{
#version 420 compatibility
	///////////////////////////////////////////////////////////////////////////
	// Vertex shader inputs
	///////////////////////////////////////////////////////////////////////////
	in vec3 world_space_position;
	in vec3 world_space_normal;
	in vec3 world_space_tangent;
	in vec2 uv;
	in vec2 uv_lm;
	///////////////////////////////////////////////////////////////////////////
	// Constants
	///////////////////////////////////////////////////////////////////////////
	const float PI = 3.14159265359;
	///////////////////////////////////////////////////////////////////////////
	// Material uniforms
	///////////////////////////////////////////////////////////////////////////
	uniform vec4      base_color_factor;
	uniform bool      has_base_color_texture = false;
	layout( binding = 0 ) uniform sampler2D base_color_texture;
	uniform float     alpha_cutoff = 0.5;
	uniform float     normal_scale;
	uniform bool      has_normal_texture = false;
	layout( binding = 1 ) uniform sampler2D normal_texture;
	uniform float     metal_factor;
	uniform float     roughness_factor;
	uniform bool      has_metal_roughness_texture = false;
	layout( binding = 2 ) uniform sampler2D metal_roughness_texture;

	uniform bool has_lm_uv;
	uniform bool has_scene_lightmap = false;
	layout( binding = 3 ) uniform sampler2D scene_lightmap_texture;
	uniform vec3 environment_color; 

	void main()
	{
		///////////////////////////////////////////////////////////////////////
		// Store diffuse reflectance, either from texture or solid
		///////////////////////////////////////////////////////////////////////
		vec3 color_out;
		if ( has_base_color_texture )
		{ 
			// Immediately kill fragment if below treshold alpha
			vec4 base_color = texture2D( base_color_texture, uv );
			if (base_color.w < 0.5) discard;
			color_out = base_color.xyz * base_color_factor.xyz;
		}
		else
		{
			color_out = base_color_factor.xyz;
		}
		gl_FragData[0].xyz = color_out; 


		///////////////////////////////////////////////////////////////////////
		// Store lightmap information
		///////////////////////////////////////////////////////////////////////

		vec3 lightmap_color = vec3( 0 );
		if ( has_scene_lightmap && has_lm_uv )
		{
			lightmap_color = texture2D(scene_lightmap_texture, uv_lm).xyz;
		}
		else
		{
			lightmap_color = environment_color;
		}
		gl_FragData[2].xyz = lightmap_color; 


		///////////////////////////////////////////////////////////////////////
		// Store roughness and metallicity. 
		///////////////////////////////////////////////////////////////////////
		float roughness_out;
		float metallicity_out;
		if (has_metal_roughness_texture)
		{ 
			vec4 metal_rough = texture2D( metal_roughness_texture, uv );
			gl_FragData[0].w = roughness_factor * metal_rough.g; 
			gl_FragData[1].w = metal_factor * metal_rough.b;
		}
		else
		{
			gl_FragData[0].w = roughness_factor;
			gl_FragData[1].w = metal_factor;
		}
		///////////////////////////////////////////////////////////////////////
		// Generate (possibly normal-mapped) normal and store in two 
		// components. 
		///////////////////////////////////////////////////////////////////////
		vec3 normal_out = normalize(world_space_normal);
		if (has_normal_texture)
		{
			vec3 normal_map_value = texture2D(normal_texture, uv).xyz;
			normal_map_value = normal_map_value * 2.0 - vec3(1.0);
			vec3 bitangent = normalize(cross(world_space_normal, world_space_tangent));
			// The negation on the x axis is scientifically established by looking at 
			// the walls of crytek sponza after using the gimp plugin to generate 
			// normal maps
			normal_out = normalize(
				-normal_map_value.x * normalize(world_space_tangent) +
				normal_map_value.y * bitangent +
				normal_map_value.z * normalize(world_space_normal));
		}
		// Have to avoid x = exactly n.x = 0.0 or atan2 freaks out. 
		if (normal_out.x == 0.0)
		{
			normal_out.x = 1e-5;
		}
		gl_FragData[1].xy = vec2( atan(normal_out.y, normal_out.x),	normal_out.z);

		// Set to 1 so we know there's something here. If there's not, render envmap
		gl_FragData[1].z = 1;
	}
}


///////////////////////////////////////////////////////////////////////////////
// Deferred shading 
///////////////////////////////////////////////////////////////////////////////
shader deferred_vert
{
#version 450 compatibility
	out vec2 texcoord;
	void main()
	{
		texcoord = gl_Vertex.xy;
		gl_Position = ftransform();
	}
}

shader deferred_frag
{
#version 450 compatibility
	in vec2 texcoord;

	const float M_PI = 3.14159265359;

	///////////////////////////////////////////////////////////////////////////
	// GBuffer
	///////////////////////////////////////////////////////////////////////////
	layout(binding = 0) uniform sampler2D depth_buffer;
	layout(binding = 1) uniform sampler2D base_color_roughness_buffer;
	layout(binding = 2) uniform sampler2D normal_metal_buffer;
	layout(binding = 3) uniform sampler2D lightmap_buffer;

	///////////////////////////////////////////////////////////////////////////
	// Ambient pass uniforms
	///////////////////////////////////////////////////////////////////////////
	uniform bool ambient_pass;
	uniform vec3 environment_color; 

	layout( binding = 4 ) uniform sampler2D env_texture;
	uniform bool has_envmap;

	///////////////////////////////////////////////////////////////////////////
	// Light uniforms
	///////////////////////////////////////////////////////////////////////////
	uniform vec3 light_position;
	uniform vec3 light_direction;
	uniform vec3 light_radiance;
	uniform float light_radius;
	uniform float light_attenuation_start;
	uniform float light_attenuation_end;
	uniform float light_beam_fov;
	uniform float light_field_fov;

	layout(binding = 5) uniform sampler2DShadow light_shadowmap_sampler;
	uniform bool light_has_shadowmap;
	uniform mat4 light_shadowmap_mvp;

	///////////////////////////////////////////////////////////////////////////
	// Camera uniforms
	///////////////////////////////////////////////////////////////////////////
	uniform vec3 camera_position; 
	uniform mat4 camera_MVP_inv; 

	vec3 fresnelSchlick( float cosTheta, vec3 F0 )
	{
		return F0 + ( 1.0 - F0 ) * pow( 1.0 - cosTheta, 5.0 );
	}

	float DistributionGGX( vec3 N, vec3 H, float roughness )
	{
		float a = roughness * roughness;
		float a2 = a * a;
		float NdotH = max( dot( N, H ), 0.0 );
		float NdotH2 = NdotH * NdotH;

		float num = a2;
		float denom = ( NdotH2 * ( a2 - 1.0 ) + 1.0 );
		denom = M_PI * denom * denom;

		return num / denom;
	}

	float GeometrySchlickGGX( float NdotV, float roughness )
	{
		float r = ( roughness + 1.0 );
		float k = ( r * r ) / 8.0;

		float num = NdotV;
		float denom = NdotV * ( 1.0 - k ) + k;

		return num / denom;
	}
	float GeometrySmith( vec3 N, vec3 V, vec3 L, float roughness )
	{
		float NdotV = max( dot( N, V ), 0.0 );
		float NdotL = max( dot( N, L ), 0.0 );
		float ggx2 = GeometrySchlickGGX( NdotV, roughness );
		float ggx1 = GeometrySchlickGGX( NdotL, roughness );

		return ggx1 * ggx2;
	}

	float GGX_Lambda( in float alpha, in float absTanTh )
	{
		if ( isinf( absTanTh ) )
			return 0.f;
		float a2t2th = alpha * absTanTh * alpha * absTanTh;
		return (-1 + sqrt( 1.f + a2t2th )) / 2.f;
	}

	struct Material
	{
		float roughness;
		float metallicity;
		vec3 F0;
		vec3 base_color;
	};

	///////////////////////////////////////////////////////////////////////////
	// Light Radiance Calculations
	///////////////////////////////////////////////////////////////////////////

	vec3 getOutRadianceFromInRay( vec3 wi, vec3 wo, vec3 normal, const Material mat, vec3 lightSample )
	{
		///////////////////////////////////////////////////////////////////////
		// GGX implementation from: 
		// http://www.filmicworlds.com/images/ggx-opt/optimized-ggx.hlsl
		// NOTE: Same page has a bunch of optimizations that I didn't bother
		//       with. 
		///////////////////////////////////////////////////////////////////////

		vec3 Lo = vec3( 0 );
		vec3 N = normal;
		vec3 V = wo;
		vec3 L = wi;
		vec3 H = normalize( V + L );

		vec3  radiance = lightSample;

		// cook-torrance brdf
		float NDF = DistributionGGX( N, H, mat.roughness );
		float G = GeometrySmith( N, V, L, mat.roughness );
		vec3  F = fresnelSchlick( max( dot( H, V ), 0.0 ), mat.F0 );

		vec3 kS = F;
		vec3 kD = vec3( 1.0 ) - kS;
		kD *= 1.0 - mat.metallicity;

		vec3  numerator = NDF * G * F;
		float denominator = 4.0 * max( dot( N, V ), 0.0 ) * max( dot( N, L ), 0.0 );
		vec3  specular = numerator / max( denominator, 0.001 );

		// add to outgoing radiance Lo
		float NdotL = max( dot( N, L ), 0.0 );
		Lo += (kD * mat.base_color / M_PI + specular) * radiance * NdotL;

		return Lo;
	}

	vec3 getOutRadianceFromDiskAreaLight(vec3 wPos, vec3 wo, vec3 normal, const Material mat)
	{
		///////////////////////////////////////////////////////////////////////
		// Calculate incoming radiance from light
		// Note: The light intensity calculated here is as if the light were
		//       an area light, but we sample the middle. This is so we match
		//       the path-tracer. Could be prettier.
		///////////////////////////////////////////////////////////////////////

		vec3 lightSamplePos = light_position;
		vec3 wi_raw = lightSamplePos - wPos;
		float distSquared = dot(wi_raw, wi_raw);
		vec3 wi = wi_raw / sqrt( distSquared );

		float lightArea = light_radius * light_radius * M_PI;
		vec3 lightSampleLe = vec3(dot(-wi, light_direction) < 0.0 ? vec3(0.0) : light_radiance);
		float lightPdf = distSquared / (abs(dot(light_direction, -wi)) * lightArea);
		lightSampleLe = lightSampleLe * (1.0 / lightPdf);

		// TODO: Will want to improve this to use a different model
		return getOutRadianceFromInRay( wi, wo, normal, mat, lightSampleLe );
	}

	vec3 getOutRadianceFromPointLight(vec3 wPos, vec3 wo, vec3 normal, const Material mat)
	{
		///////////////////////////////////////////////////////////////////////
		// Calculate incoming radiance from light
		///////////////////////////////////////////////////////////////////////

		vec3 lightSamplePos = light_position;
		vec3 wi_raw = lightSamplePos - wPos;
		float distSquared = dot(wi_raw, wi_raw);
		vec3 wi = wi_raw / sqrt( distSquared );

		vec3 lightSampleLe = vec3(dot(-wi, light_direction) < 0.0 ? vec3(0.0) : light_radiance);
		float lightPdf = distSquared / abs(dot(light_direction, -wi));
		lightSampleLe = lightSampleLe * (1.0 / lightPdf);

		// TODO: Will want to improve this to use a different model
		return getOutRadianceFromInRay( wi, wo, normal, mat, lightSampleLe );
	}

	void main()
	{
		Material mat;
		///////////////////////////////////////////////////////////////////////
		// Fetch base color and roughness, encoded normal and metallicity
		///////////////////////////////////////////////////////////////////////
		vec4  color_roughness = texture( base_color_roughness_buffer, texcoord );
		mat.base_color = color_roughness.xyz;
		mat.roughness = color_roughness.w;

		vec4 normal_metal = texture( normal_metal_buffer, texcoord );
		vec2 coded_normal = normal_metal.xy;
		mat.metallicity = normal_metal.w;

		///////////////////////////////////////////////////////////////////////
		// This same shader is currently used for the ambient pass, where
		// we do some indirect illumination approximation (currently just
		// ambient)
		///////////////////////////////////////////////////////////////////////
		if (ambient_pass) {

			///////////////////////////////////////////////////////////////////////
			// Draw environment for empty pixels
			///////////////////////////////////////////////////////////////////////
			if ( normal_metal.z == 0.0 )
			{
				vec4 ndc_coord = vec4( texcoord.x * 2.0 - 1, texcoord.y * 2.0 - 1, 1.0, 1.0 );
				vec4 world_coord = camera_MVP_inv * ndc_coord;
				vec3 dir = normalize( ( world_coord * ( 1.0 / world_coord.w ) ).xyz - camera_position );

				float theta = acos( dir.y );
				float phi = atan( dir.z, dir.x );

				if ( !has_envmap )
				{
					gl_FragColor.xyz = environment_color;
				}
				else
				{
					// environment color and intensity is in texture
					gl_FragColor.xyz = environment_color * texture(env_texture, vec2( phi / (2.0 * M_PI), theta / M_PI )).xyz;
				}
				return;
			}

			gl_FragColor.xyz = texture(lightmap_buffer, texcoord).xyz * mat.base_color;
			return; 
		}

		///////////////////////////////////////////////////////////////////////
		// Decode position
		///////////////////////////////////////////////////////////////////////
		vec4 mvp_pos = vec4(texcoord.x * 2.0 - 1.0,	texcoord.y * 2.0 - 1.0,
							texture(depth_buffer, texcoord).x * 2.0 - 1.0, 1.0);
		vec4 hposition = camera_MVP_inv * mvp_pos; 
		vec3 position = (1.0 / hposition.w) * hposition.xyz; 

		vec3 wo = normalize( camera_position - position );

		///////////////////////////////////////////////////////////////////////
		// Decode normal
		///////////////////////////////////////////////////////////////////////
		vec3 normal;
		{
			vec2 enc = coded_normal;
			vec2 scth = vec2(sin(enc.x), cos(enc.x));
			vec2 scphi = vec2(sqrt(1.0 - enc.y*enc.y), enc.y);
			normal = normalize(vec3(scth.y*scphi.x, scth.x*scphi.x, scphi.y));
		}

		///////////////////////////////////////////////////////////////////////
		// Calculate fresnel value
		///////////////////////////////////////////////////////////////////////
		mat.F0 = vec3( 0.04 );
		mat.F0 = mix( mat.F0, mat.base_color, mat.metallicity );

		///////////////////////////////////////////////////////////////////////
		// Find out if shadowed
		///////////////////////////////////////////////////////////////////////
		/*
		if (light_has_shadowmap) {
			vec4 lightMvpPosition = light_shadowmap_mvp * vec4(position, 1.0);
			lightMvpPosition *= 1.0 / lightMvpPosition.w;
			vec3 smCoord = (lightMvpPosition.xyz + vec3(1.0)) * 0.5;
			if (smCoord.x > 0.0 && smCoord.x < 1.0 && 
				smCoord.y > 0.0 && smCoord.y < 1.0 && 
				smCoord.z > 0.0 && smCoord.z < 1.0) {
				float shadow = texture(light_shadowmap_sampler, smCoord.xyz);
				lightSampleLe *= shadow;
			}
			else { lightSampleLe = vec3(0.0); }
		}
		*/

		vec3 Lo = getOutRadianceFromPointLight( position, wo, normal, mat );

		///////////////////////////////////////////////////////////////////////
		// Combine to final radiance
		///////////////////////////////////////////////////////////////////////

		// TODO: need to somehow pass to sRGB?

		gl_FragColor.xyz = Lo;
	}
}
