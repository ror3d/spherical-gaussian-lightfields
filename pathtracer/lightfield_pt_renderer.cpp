#include "lightfield_pt_renderer.h"
#include "PathTracer.h"

#include <CHAGApp/PerformanceProfiler.h>
#include <CHAGApp/ProgressListenerDialog.h>
#include <utils/opengl_helpers.h>

#include <utils/half.hpp>
#include <utils/json_extras.hpp>
#include <glm/glm.hpp>

#include <CHAGApp/imgui/imgui.h>
#include <wx/filepicker.h>
#include <wx/dirdlg.h>

#include <fstream>
#include <queue>
#include <utils/filesystem.h>
#include <utils/colors.h>


//#define EXPORT_ONLY_VISIBILITY

namespace pathtracer
{

void LightfieldPathtracerRenderer::destroy()
{
	LightmapPathtracerRenderer::destroy();
}

void LightfieldPathtracerRenderer::renderGUI()
{
	ImGui::Begin( "Pathtrace Lightfield" );
	{
		if ( isPathTracing() )
		{
			if ( ImGui::Button( "Stop pathtracing" ) )
			{
				stopPathTracing();
			}
			if ( m_lightfield_settings_current.render_single_lightfield )
			{
				if ( ImGui::Button( "Change LF coords" ) )
				{
					ImGui::OpenPopup( "Single lightfield pathtrace" );
				}
			}
		}
		else
		{
			if ( ImGui::Button( "Start pathtracing lightfields" ) )
			{
				ImGui::OpenPopup( "New lightfield pathtrace" );
			}
			if ( ImGui::Button( "Continue pathtracing previous lightfields" ) )
			{
				wxFileDialog openFileDialog( m_parentWindow, _( "Open Lightfield Metadata" ), "", "", "json files (*.json)|*.json", wxFD_OPEN );
				if ( openFileDialog.ShowModal() == wxID_OK )
				{
					std::string filename = std::string( openFileDialog.GetPath().mb_str( wxConvUTF8 ) );
					LOG_VERBOSE( "Opening..." << filename );

					continueFromPartialFile( filename );

					startPathTracing();
				}
			}
			if ( ImGui::Button( "Trace single lightfield" ) )
			{
				ImGui::OpenPopup( "Single lightfield pathtrace" );
			}
		}

		{
			const char* items[] = { "Reflection", "Env w/ vis", "Env w/o vis" };
			static int item_current = 0;
			ImGui::Combo( "Display Channels", &item_current, items, IM_ARRAYSIZE( items ) );
			switch ( item_current )
			{
				case 0:
					m_pImage->setRenderChannels( glm::uvec3{ 0, 1, 2 } );
					break;
				case 1:
					m_pImage->setRenderChannels( glm::uvec3{ 3, 4, 5 } );
					break;
				case 2:
					m_pImage->setRenderChannels( glm::uvec3{ 6, 7, 8 } );
					break;
			}
		}

		if ( ImGui::BeginPopupModal( "New lightfield pathtrace", nullptr, ImGuiWindowFlags_AlwaysAutoResize ) )
		{
			static char filename_prefix[256] = "";
			if ( m_need_update_scene_name )
			{
				strcpy_s( filename_prefix, m_scene->m_scene_name.c_str() );
				m_need_update_scene_name = false;
			}
			ImGui::InputText( "Filename Prefix", filename_prefix, 256 );

			static char outDirPath[512] = "";
			{
				if ( ImGui::Button( "..." ) )
				{
					wxDirDialog dirPickDlg( m_parentWindow, _( "Select output dir" ), outDirPath );
					if ( dirPickDlg.ShowModal() == wxID_OK )
					{
						strcpy_s( outDirPath, dirPickDlg.GetPath().ToStdString().c_str() );
					}
				}
				ImGui::SameLine();
				ImGui::InputText( "File Path", outDirPath, 512, ImGuiInputTextFlags_ReadOnly );
			}

			if ( ImGui::Button( "Start" ) )
			{
				std::string out_dir = outDirPath;
				out_dir += (out_dir.empty() || out_dir.back() == '\\') ? "" : "\\";
				std::string prefix = filename_prefix;
				out_dir += prefix + "\\";

				setFilePath( out_dir, prefix );
				updateImageSettings();
				startPathTracing();
				ImGui::CloseCurrentPopup();
			}
			ImGui::SameLine();
			if ( ImGui::Button( "Cancel" ) )
			{
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}

		if ( ImGui::BeginPopupModal( "Single lightfield pathtrace", nullptr, ImGuiWindowFlags_AlwaysAutoResize ) )
		{
			static int uv[2] = { 322, 158 };
			ImGui::InputInt2( "TexCoord", uv );
			if ( uv[0] < 0 )
			{
				uv[0] = 0;
			}
			else if ( uv[0] >= m_lightfield_settings_next.base_lightmap_size.x )
			{
				uv[0] = m_lightfield_settings_next.base_lightmap_size.x - 1;
			}
			if ( uv[1] < 0 )
			{
				uv[1] = 0;
			}
			else if ( uv[1] >= m_lightfield_settings_next.base_lightmap_size.y )
			{
				uv[1] = m_lightfield_settings_next.base_lightmap_size.y - 1;
			}

			if ( isPathTracing() )
			{
				if ( ImGui::Button( "Change" ) )
				{
					m_lightfield_settings_current.single_texel_coords = { uv[0], uv[1] };
					for ( size_t i = 0; i < m_valid_coords.size(); ++i )
					{
						if ( m_valid_coords[i] == m_lightfield_settings_current.single_texel_coords )
						{
							m_batches_to_process.front().first_texel_coords_idx = i;
						}
					}
					startPage();
					m_pImage->restart();
					ImGui::CloseCurrentPopup();
				}
			}
			else
			{
				if ( ImGui::Button( "Start" ) )
				{
					updateImageSettings();
					m_lightfield_settings_current.render_single_lightfield = true;
					m_lightfield_settings_current.save_to_file = false;
					m_lightfield_settings_current.single_texel_coords = { uv[0], uv[1] };
					m_lightfield_settings_current.grid_size = 1;
					startPathTracing();
					m_pImage->restart();
					ImGui::CloseCurrentPopup();
				}
			}
			ImGui::SameLine();
			if ( ImGui::Button( "Cancel" ) )
			{
				ImGui::CloseCurrentPopup();
			}
			ImGui::EndPopup();
		}
	}
	ImGui::End();

}

void LightfieldPathtracerRenderer::updateImageSettings()
{
	IPathtracingRenderer::updateImageSettings();
	m_lightfield_settings_current = m_lightfield_settings_next;
	m_positionsDirty = true;
}

void LightfieldPathtracerRenderer::render()
{
#if 0 // To debug the helper textures
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable( GL_DEPTH_TEST );

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	m_copy_program->enable();
	glBindTexture( GL_TEXTURE_2D, m_corrected_position_texture );

	orthoview::drawFullScreenQuad();
	m_copy_program->disable();

	glPopAttrib();
#elif 0
	if ( !m_island_ids.empty() )
	{
		static uint32_t idtex = 0;
		if ( idtex == 0 )
		{
			glGenTextures( 1, &idtex );

			glBindTexture( GL_TEXTURE_2D, idtex );

			std::vector<glm::vec3> islands( m_island_ids.size(), glm::vec3{ 0 } );
			std::vector<chag::color3_t> island_colors;
			size_t valid = 0;
			for ( size_t i = 0; i < m_island_ids.size(); ++i )
			{
				uint32_t id = m_island_ids[i];
				if ( id != 0 )
				{
					if ( island_colors.size() <= id )
					{
						for ( size_t j = island_colors.size(); j < id; ++j )
						{
							chag::color3_t c = chag::hsv_to_rgb( fmodf(j * 0.618034f, 1.f), 0.5, 0.95 );
							island_colors.push_back(c);
						}
					}
					islands[i] = island_colors[id - 1];
					++valid;
				}
			}
			DebugAssert( valid == m_valid_coords.size() );

			glTexImage2D( GL_TEXTURE_2D,
						  0,
						  GL_RGB32F,
						  m_lightfield_settings_current.base_lightmap_size.x,
						  m_lightfield_settings_current.base_lightmap_size.y,
						  0,
						  GL_RGB,
						  GL_FLOAT,
						  islands.data() );

			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		}
		glPushAttrib( GL_ALL_ATTRIB_BITS );
		glDisable( GL_DEPTH_TEST );

		glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
		glClear( GL_COLOR_BUFFER_BIT );

		m_copy_program->enable();

		glBindTexture( GL_TEXTURE_2D, idtex );
		orthoview::drawFullScreenQuad();

		m_copy_program->disable();
		glPopAttrib();
	}
#else
	IPathtracingRenderer::render();
#endif
}

void LightfieldPathtracerRenderer::initializePathtracer_internal()
{
#ifdef USE_PATHTRACER
	std::vector<std::string> compile_flags;
#ifndef EXPORT_ONLY_VISIBILITY
	if ( m_lightfield_settings_current.environment_as_alpha )
	{
		compile_flags.push_back( "RENDER_OUTGOING_ENVIRONMENT_WITH_VISIBILITY" );
		m_pImage->setNumChannels( 9 );
	}
	else
	{
		m_pImage->setNumChannels( 3 );
	}
#else
	compile_flags.push_back( "RENDER_ONLY_ENVIRONMENT_VISIBILITY" );
	m_pImage->setNumChannels( 6 );
#endif

	m_pImage->initialize( "../pathtracer/lightfield_pathtracer.cu",
	                      "pathtrace_lightfield",
	                      "lightfield_radiance_closest_hit",
	                      "lightfield_shadow_any_hit",
	                      "lightfield_miss",
	                      "lightfield_exception",
						  compile_flags );

	// Declare variables
	m_pImage->setVariable( "valid_coords_buffer", m_valid_coords );

	m_pImage->setTexture2DVariable( "input_position_buffer",
									m_corrected_position_texture,
									pathtracer::PathtracedImage::TextureFilter::Nearest,
									false );
	m_pImage->setTexture2DVariable( "input_normal_buffer",
									m_geom_normal_texture,
									pathtracer::PathtracedImage::TextureFilter::Nearest,
									false );
	m_pImage->setTexture2DVariable( "input_tangent_buffer",
									m_geom_tangent_texture,
									pathtracer::PathtracedImage::TextureFilter::Nearest,
									false );

	m_pImage->setVariable( "lightfield_grid_size", uint32_t( m_lightfield_settings_current.grid_size ) );
	m_pImage->setVariable( "lightfield_tex_size", uint32_t( m_lightfield_settings_current.lightfield_size ) );
	m_pImage->setVariable( "lightfield_max_jitter_roughness", m_lightfield_settings_current.max_jitter_roughness );
	m_pImage->setVariable( "lightfield_max_jitter_position", m_lightfield_settings_current.max_jitter_position );

	startPage();

	size_t sz = size_t( m_lightfield_settings_current.lightfield_size ) * m_lightfield_settings_current.grid_size;
	m_pImage->resize( sz, sz );
#endif
}

void LightfieldPathtracerRenderer::setBaseLightmapSize( uint16_t width, uint16_t height )
{
	m_asserts.baseLightmapSizeSet = true;
	m_lightfield_settings_next.base_lightmap_size.x = width;
	m_lightfield_settings_next.base_lightmap_size.y = height;
	m_image_settings_changed = true;

	// This resizes the render buffers and marks the positions as dirty
	LightmapPathtracerRenderer::resize( width, height );
}

void LightfieldPathtracerRenderer::setLightfieldSize( uint16_t size )
{
	m_asserts.lightfieldfieldSizeSet = true;
	m_lightfield_settings_next.lightfield_size = size;
	m_image_settings_changed = true;
}

void LightfieldPathtracerRenderer::setGridSize( uint16_t size )
{
	m_lightfield_settings_next.grid_size = size;
	m_image_settings_changed = true;
}

void LightfieldPathtracerRenderer::setFilePath( const std::string & path, const std::string& prefix )
{
	m_path = path;
	for ( size_t i = 0; i < m_path.length(); ++i )
	{
		if ( m_path[i] == '\\' ) m_path[i] = '/';
	}
	m_path += (m_path.empty() || m_path.back() == '/') ? "" : "/";
	std::filesystem::create_directories( m_path );
	m_file_prefix = prefix;
}

void LightfieldPathtracerRenderer::setMaxJitterRoughness( float roughness )
{
	m_lightfield_settings_next.max_jitter_roughness = roughness;
	m_image_settings_changed = true;
}

void LightfieldPathtracerRenderer::setMaxJitterPosition( float mult )
{
	m_lightfield_settings_next.max_jitter_position = mult;
	m_image_settings_changed = true;
}

void LightfieldPathtracerRenderer::setTreatEnvironmentAsAlpha( bool easa )
{
	m_lightfield_settings_next.environment_as_alpha = easa;
	m_image_settings_changed = true;
}

void LightfieldPathtracerRenderer::setContinueAfterFirstPass( bool cont )
{
	m_lightfield_settings_next.continue_after_first_pass = cont;
	m_image_settings_changed = true;
}

void LightfieldPathtracerRenderer::updateScene( scene::Scene * scene )
{
	LightmapPathtracerRenderer::updateScene( scene );
	m_need_update_scene_name = true;
}

void LightfieldPathtracerRenderer::pathTrace_impl()
{
#ifdef USE_PATHTRACER
	if ( m_positionsDirty )
	{
		DebugAssert( m_asserts.baseLightmapSizeSet );
		DebugAssert( m_asserts.lightfieldfieldSizeSet );

		g_progress->push_task( "Generating base textures for Lightmap" );
		{
			{
				g_progress->push_task( "Preparing base lightmap" );
				renderLightmapBaseTextures();
				g_progress->pop_task();
			}
			if ( m_lightfield_settings_current.save_to_file )
			{
				g_progress->push_task( "Exporting lightfield metadata" );
				outputRawMetadata();
				g_progress->pop_task();
			}
			{
				g_progress->push_task( "Correcting positions texture" );
				updatePositionsTexture();
				g_progress->pop_task();
			}
			initializeBatchesToProcess();
			if ( m_lightfield_settings_current.save_to_file )
			{
				g_progress->push_task( "Exporting lightfield metadata" );
				outputCorrectedMetadata();
				g_progress->pop_task();
			}
			initializePathtracer_internal();
		}
		g_progress->pop_task();

		m_positionsDirty = false;
	}

	m_pImage->tracePaths();

	if ( hasFinishedMaxSamplesPerPixel() )
	{
		auto lfb = m_batches_to_process.front();
		if ( m_lightfield_settings_current.save_to_file )
		{
			uint16_t n_channels = ( m_lightfield_settings_current.environment_as_alpha ? m_pImage->getNumChannels() : 3 );
			outputCurrentLightfield(
			        lfb.filename, lfb.texel_count, n_channels, m_pImage->getCurrentNumberOfSamples(), lfb.continuing );
		}

		if ( !m_lightfield_settings_current.render_single_lightfield )
		{
			m_batches_to_process.pop_front();

			// Move the current batch at the back so we get to it later
			// only if option enabled
			if ( m_lightfield_settings_current.continue_after_first_pass )
			{
				lfb.continuing = true;
				m_batches_to_process.push_back( lfb );
			}

			if ( !m_batches_to_process.empty() )
			{
				m_pImage->restart();
				startPage();
			}
			else
			{
				stopPathTracing();
			}
		}
	}
#endif
}

void LightfieldPathtracerRenderer::startPage()
{
	m_pImage->setVariable( "lightfield_grid_page_start", uint32_t( m_batches_to_process.front().first_texel_coords_idx ) );
}

uint32_t LightfieldPathtracerRenderer::outputCurrentLightfield( const std::string& filename,
															size_t texel_count,
															uint16_t n_channels,
															uint32_t samples_added,
															bool continuing )
{
	typedef glm::tvec3<half_float::half, glm::lowp> hvec3;

	const uint16_t lightfield_size = m_lightfield_settings_current.lightfield_size;
	const uint16_t grid_size = m_lightfield_settings_current.grid_size;

	glm::ivec2 sz = m_pImage->getSize();
	const float* fdata = (const float*)m_pImage->getData();
	std::vector<half_float::half> bdata ( lightfield_size * lightfield_size * n_channels * texel_count );
	
	// Put color channels one after the other instead of interleaved
#if _WIN32
#pragma omp parallel for
#endif
	for ( int j = 0; j < grid_size; ++j )
	{
		if ( j * grid_size >= texel_count )
		{
			break;
		}
		for ( size_t i = 0; i < grid_size; ++i )
		{
			size_t w_index = (j * grid_size + i);

			if ( w_index >= texel_count )
			{
				break;
			}

			for ( size_t c = 0; c < n_channels; ++c )
			{
				for ( size_t y = 0; y < lightfield_size; ++y )
				{
					for ( size_t x = 0; x < lightfield_size; ++x )
					{
						size_t r_channel = (j * lightfield_size + y) * sz.x + i * lightfield_size + x;
						size_t r = r_channel * m_pImage->getNumChannels();
						size_t w_channel = (w_index * n_channels + c) * lightfield_size * lightfield_size;
						size_t w = w_channel + y * lightfield_size + x;
						bdata[w] = *(fdata + r + c);
					}
				}
			}
		}
	}

	if ( continuing )
	{
		uint16_t version = 0;
		half_float::half magic(0.f);
		uint16_t lf_side_length = 0;
		uint16_t lf_count = 0;
		uint16_t lf_channels = 0;
		uint32_t lf_samples = 0;

		std::ifstream f( m_path + filename, std::ifstream::binary );
		if ( f )
		{
			f.read( reinterpret_cast<char*>(&magic), sizeof( half_float::half ) );
			DebugAssert( magic == half_float::half( 1.f / 3.f ) );
			f.read( reinterpret_cast<char*>(&version), sizeof( uint16_t ) );
			DebugAssert( version == 4 );
			f.read( reinterpret_cast<char*>(&lf_side_length), sizeof( uint16_t ) );
			DebugAssert( lf_side_length == lightfield_size );
			f.read( reinterpret_cast<char*>(&lf_count), sizeof( uint16_t ) );
			DebugAssert( lf_count == texel_count );
			f.read( reinterpret_cast<char*>(&lf_channels), sizeof( uint16_t ) );
			DebugAssert( lf_channels == n_channels );
			f.read( reinterpret_cast<char*>(&lf_samples), sizeof( uint32_t ) );

			std::vector<half_float::half> rdata ( lf_count * lf_side_length * lf_side_length * lf_channels );
			f.read( reinterpret_cast<char*>(rdata.data()), sizeof( half_float::half ) * rdata.size() );

			for ( size_t i = 0; i < rdata.size(); ++i )
			{
				bdata[i] = (float( bdata[i] ) * samples_added + float( rdata[i] ) * lf_samples) / (samples_added + lf_samples);
			}
			samples_added += lf_samples;
		}
	}

	// 2 bytes: magic number = half_float 1./3. (this might be used to check correct endianness)
	// 2 bytes: version
	// 2 bytes: lightfield tile side length. Lightfield images are always a square.
	// 2 bytes: number of tiles in this texture
	// 2 bytes: number channels per tile image
	// 4 bytes: number of samples

	// Version 2:
	// Version 3: Added number of channels per tile in the header. Added possibility of having different number of channels than 3.
	// Version 4: Add number of samples
	uint16_t version = 4;
	half_float::half magic(1.f / 3.f);
	uint16_t lf_side_length = lightfield_size;
	uint16_t lf_count = texel_count;
	uint16_t lf_channels = n_channels;
	uint32_t lf_samples = samples_added;
	{
		std::ofstream f( m_path + filename, std::ofstream::binary );
		f.write( reinterpret_cast<const char*>(&magic), sizeof( half_float::half ) );
		f.write( reinterpret_cast<const char*>(&version), sizeof( uint16_t ) );
		f.write( reinterpret_cast<const char*>(&lf_side_length), sizeof( uint16_t ) );
		f.write( reinterpret_cast<const char*>(&lf_count), sizeof( uint16_t ) );
		f.write( reinterpret_cast<const char*>(&lf_channels), sizeof( uint16_t ) );
		f.write( reinterpret_cast<const char*>(&lf_samples), sizeof( uint32_t ) );

		f.write( reinterpret_cast<const char*>(bdata.data()), sizeof( half_float::half ) * bdata.size() );
	}

	return samples_added;
}

void LightfieldPathtracerRenderer::outputRawMetadata()
{
	auto sz = m_lightfield_settings_current.base_lightmap_size;
	std::vector<glm::vec4> pos_tex( sz.x * sz.y );
	std::vector<glm::vec4> sh_norm_tex( sz.x * sz.y );
	std::vector<glm::vec4> geo_norm_tex( sz.x * sz.y );
	std::vector<glm::vec4> geo_tan_tex( sz.x * sz.y );

	glGetTextureImage( m_position_texture, 0, GL_RGBA, GL_FLOAT, pos_tex.size() * sizeof(float) * 4, (float*)pos_tex.data() );
	glGetTextureImage( m_shading_normal_texture, 0, GL_RGBA, GL_FLOAT, sh_norm_tex.size() * sizeof(float) * 4, (float*)sh_norm_tex.data() );
	glGetTextureImage( m_geom_normal_texture, 0, GL_RGBA, GL_FLOAT, geo_norm_tex.size() * sizeof(float) * 4, (float*)geo_norm_tex.data() );
	glGetTextureImage( m_geom_tangent_texture, 0, GL_RGBA, GL_FLOAT, geo_tan_tex.size() * sizeof(float) * 4, (float*)geo_tan_tex.data() );

	{
		// 4 bytes: magic number = float 1./5. (this might be used to check correct endianness)
		// 2 bytes: version
		// 2 bytes: lightmap width
		// 2 bytes: lightmap height
		//  - The following arrays have width*height size
		// char[] - Validity
		// float3[] - Positions
		// float3[] - Shading normals
		// float3[] - Geometric normals
		// float3[] - Geometric tangents
		// float2[] - Texel sizes 

		float magic(1.f / 5.f);
		uint16_t version = 1;
		uint16_t lm_width = (uint16_t)sz.x;
		uint16_t lm_height = (uint16_t)sz.y;

		std::ofstream f( m_path + m_file_prefix + "_meta_uncorrected.lmm", std::ofstream::binary );
		f.write( reinterpret_cast<const char*>(&magic), sizeof( float ) );
		f.write( reinterpret_cast<const char*>(&version), sizeof( uint16_t ) );
		f.write( reinterpret_cast<const char*>(&lm_width), sizeof( uint16_t ) );
		f.write( reinterpret_cast<const char*>(&lm_height), sizeof( uint16_t ) );

		// Output valid positions, as 1 byte each
		for ( size_t i = 0; i < pos_tex.size(); ++i )
		{
			if ( pos_tex[i].w == 0 )
			{
				char v = 0;
				f.write( &v, 1 );
			}
			else
			{
				char v = 1;
				f.write( &v, 1 );
			}
		}

		// Output positions
		for ( size_t i = 0; i < pos_tex.size(); ++i )
		{
			f.write( reinterpret_cast<const char*>(&pos_tex[i]), sizeof( float ) * 3 );
		}
		// Output shading normals
		for ( size_t i = 0; i < sh_norm_tex.size(); ++i )
		{
			f.write( reinterpret_cast<const char*>(&sh_norm_tex[i]), sizeof( float ) * 3 );
		}
		// Output geometric normals
		for ( size_t i = 0; i < geo_norm_tex.size(); ++i )
		{
			f.write( reinterpret_cast<const char*>(&geo_norm_tex[i]), sizeof( float ) * 3 );
		}
		// Output geometric tangents
		for ( size_t i = 0; i < geo_tan_tex.size(); ++i )
		{
			glm::vec3 tg = geo_tan_tex[i];
			tg = glm::normalize( tg );
			f.write( reinterpret_cast<const char*>(&tg), sizeof( float ) * 3 );
		}
		// Output texel size, along the <tangent, bitangent>
		for ( size_t i = 0; i < geo_tan_tex.size(); ++i )
		{
			glm::vec4 tg = geo_tan_tex[i];
			float tgl = glm::length( glm::vec3(tg) );
			f.write( reinterpret_cast<const char*>(&tgl), sizeof( float ) );
			f.write( reinterpret_cast<const char*>(&tg.w), sizeof( float ) );
		}
	}
}

void LightfieldPathtracerRenderer::outputCorrectedMetadata()
{
	auto sz = m_lightfield_settings_current.base_lightmap_size;
	std::vector<glm::vec4> pos_tex( sz.x * sz.y );
	std::vector<glm::vec4> sh_norm_tex( sz.x * sz.y );
	std::vector<glm::vec4> geo_norm_tex( sz.x * sz.y );
	std::vector<glm::vec4> geo_tan_tex( sz.x * sz.y );

	glGetTextureImage( m_corrected_position_texture, 0, GL_RGBA, GL_FLOAT, pos_tex.size() * sizeof(float) * 4, (float*)pos_tex.data() );
	glGetTextureImage( m_shading_normal_texture, 0, GL_RGBA, GL_FLOAT, sh_norm_tex.size() * sizeof(float) * 4, (float*)sh_norm_tex.data() );
	glGetTextureImage( m_geom_normal_texture, 0, GL_RGBA, GL_FLOAT, geo_norm_tex.size() * sizeof(float) * 4, (float*)geo_norm_tex.data() );
	glGetTextureImage( m_geom_tangent_texture, 0, GL_RGBA, GL_FLOAT, geo_tan_tex.size() * sizeof(float) * 4, (float*)geo_tan_tex.data() );

	{
		// 4 bytes: magic number = float 1./5. (this might be used to check correct endianness)
		// 2 bytes: version
		// 2 bytes: lightmap width
		// 2 bytes: lightmap height
		//  - The following arrays have width*height size
		// char[] - Validity
		// float3[] - Positions
		// float3[] - Shading normals
		// float3[] - Geometric normals
		// float3[] - Geometric tangents
		// float2[] - Texel sizes 

		float magic( 1.f / 5.f );
		uint16_t version = 1;
		uint16_t lm_width = (uint16_t)sz.x;
		uint16_t lm_height = (uint16_t)sz.y;

		std::ofstream f( m_path + m_file_prefix + "_meta.lmm", std::ofstream::binary );
		f.write( reinterpret_cast<const char*>(&magic), sizeof( float ) );
		f.write( reinterpret_cast<const char*>(&version), sizeof( uint16_t ) );
		f.write( reinterpret_cast<const char*>(&lm_width), sizeof( uint16_t ) );
		f.write( reinterpret_cast<const char*>(&lm_height), sizeof( uint16_t ) );

		// Output valid positions, as 1 byte each
		for ( size_t i = 0; i < pos_tex.size(); ++i )
		{
			if ( pos_tex[i].w == 0 )
			{
				char v = 0;
				f.write( &v, 1 );
			}
			else
			{
				char v = 1;
				f.write( &v, 1 );
			}
		}

		// Output positions
		for ( size_t i = 0; i < pos_tex.size(); ++i )
		{
			f.write( reinterpret_cast<const char*>(&pos_tex[i]), sizeof( float ) * 3 );
		}
		// Output shading normals
		for ( size_t i = 0; i < sh_norm_tex.size(); ++i )
		{
			f.write( reinterpret_cast<const char*>(&sh_norm_tex[i]), sizeof( float ) * 3 );
		}
		// Output geometric normals
		for ( size_t i = 0; i < geo_norm_tex.size(); ++i )
		{
			f.write( reinterpret_cast<const char*>(&geo_norm_tex[i]), sizeof( float ) * 3 );
		}
		// Output geometric tangents
		for ( size_t i = 0; i < geo_tan_tex.size(); ++i )
		{
			glm::vec3 tg = geo_tan_tex[i];
			tg = glm::normalize( tg );
			f.write( reinterpret_cast<const char*>(&tg), sizeof( float ) * 3 );
		}
		// Output texel size, along the <tangent, bitangent>
		for ( size_t i = 0; i < geo_tan_tex.size(); ++i )
		{
			glm::vec4 tg = geo_tan_tex[i];
			float tgl = glm::length( glm::vec3( tg ) );
			f.write( reinterpret_cast<const char*>(&tgl), sizeof( float ) );
			f.write( reinterpret_cast<const char*>(&tg.w), sizeof( float ) );
		}
	}

	// Output json with the information
	// - What texels are in which file?
	{
		const size_t cells_per_file = m_lightfield_settings_current.grid_size * m_lightfield_settings_current.grid_size;

		using json = nlohmann::json;
		json meta;

		meta["version"] = 4;
		meta["texelInformation"] = m_file_prefix + "_meta.lmm";
		meta["texelInformation_uncorrected"] = m_file_prefix + "_meta_uncorrected.lmm";
		meta["lightfieldSideSize"] = m_lightfield_settings_current.lightfield_size;
		meta["lighfieldsPerBatchFile"] = cells_per_file;
		meta["batchFileCount"] = m_batches_to_process.size();
		meta["lightmapGridSize"] = m_lightfield_settings_current.base_lightmap_size;
		meta["alphaIsEnvmapVisibility"] = m_lightfield_settings_current.environment_as_alpha;
		meta["lightfieldBatchSize"] = glm::u16vec2( m_lightfield_settings_current.grid_size, m_lightfield_settings_current.grid_size );
		meta["maxBouncesPerPath"] = m_pImage->getMaxBounces();
		meta["maxJitterRoughness"] = m_lightfield_settings_current.max_jitter_roughness;
		meta["maxJitterPosition"] = m_lightfield_settings_current.max_jitter_position;
		meta["texelCount"] = m_valid_coords.size();
		meta["minRoughness"] = m_pImage->getMinRoughness();

		{
			json batches;
			batches.array();
			for ( auto& b : m_batches_to_process )
			{
				json batch;
				batch["pageIndex"] = b.page_idx;
				batch["uri"] = b.filename;
				batch["texelStartIdx"] = b.first_texel_coords_idx;
				batch["texelCount"] = b.texel_count;
				batch["islandId"] = b.island_id;
				std::vector<glm::u16vec2> texels;
				for ( size_t i = 0; i < b.texel_count; ++i )
				{
					texels.push_back( m_valid_coords[b.first_texel_coords_idx + i] );
				}
				batch["texels"] = texels;
				batches.push_back( batch );
			}
			meta["batches"] = batches;
		}

		std::ofstream f( m_path + m_file_prefix + ".json" );
		f << meta.dump(4);
	}
}

void LightfieldPathtracerRenderer::initializeBatchesToProcess()
{
		std::vector<glm::vec4> pos_tex( m_pImage->getSize().x * m_pImage->getSize().y );
		glBindTexture( GL_TEXTURE_2D, m_corrected_position_texture );

		glGetTexImage( GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, (float*)pos_tex.data() );

		DebugAssert( m_pImage->getSize().x < (1 << 14) && m_pImage->getSize().y < (1 << 14) );

	// Separate islands
	m_island_ids.clear();
	m_island_ids.resize(m_pImage->getSize().x * m_pImage->getSize().y, 1);
	if(false)
	{


		std::queue<glm::i16vec2> to_visit;

		uint32_t currentId = 1;

		auto prepare_visit = [&to_visit, &island_ids = m_island_ids, &pos_tex, &currentId, sz = m_pImage->getSize()]( int16_t x, int16_t y )
		{
			size_t n = x + y * sz.x;
			if ( x >= 0 && y >= 0 && x < sz.x && y < sz.y && island_ids[n] == 0 && pos_tex[n].w == 1 )
			{
				island_ids[n] = currentId;
				to_visit.push( { x, y } );
			}
		};

		size_t width = m_pImage->getSize().x;

		for ( uint16_t j = 0; j < m_pImage->getSize().y; ++j )
		{
			for ( uint16_t i = 0; i < m_pImage->getSize().x; ++i )
			{
				size_t n = i + j * width;
				if ( m_island_ids[n] == 0 && pos_tex[n].w == 1 )
				{
					m_island_ids[n] = currentId;
					to_visit.push( { i, j } );
					// Fill the island with the current id
					do
					{
						glm::i16vec2 p = to_visit.front();
						to_visit.pop();
						prepare_visit( p.x - 1, p.y );
						prepare_visit( p.x + 1, p.y );
						prepare_visit( p.x, p.y - 1 );
						prepare_visit( p.x, p.y + 1 );
					} while ( !to_visit.empty() );
					++currentId;
				}
			}
		}
	}

	// Get valid coords
	std::vector<uint32_t> num_island_coords;
	{
		std::vector<std::pair<uint32_t, uint32_t>> coord_idx_x_island_ids;
		uint32_t k = 0;
		m_valid_coords.clear();
		for ( uint16_t j = 0; j < m_pImage->getSize().y; ++j )
		{
			for ( uint16_t i = 0; i < m_pImage->getSize().x; ++i )
			{
				size_t n = i + j * m_pImage->getSize().x;
				if ( pos_tex[n].w == 1 )
				{
					m_valid_coords.push_back( { i, j } );
					coord_idx_x_island_ids.push_back( { k, m_island_ids[n] } );
					++k;
				}
			}
		}
		std::sort( coord_idx_x_island_ids.begin(), coord_idx_x_island_ids.end(), []( auto& a, auto& b ) { return a.second < b.second; } );
		auto tmp_valid = m_valid_coords;
		uint32_t prevId = 0;
		for ( size_t i = 0; i < m_valid_coords.size(); ++i )
		{
			m_valid_coords[i] = tmp_valid[coord_idx_x_island_ids[i].first];
			if ( prevId != coord_idx_x_island_ids[i].second )
			{
				prevId = coord_idx_x_island_ids[i].second;
				num_island_coords.push_back( 0 );
	}
			++ num_island_coords.back();
		}
	}

	// Create batches
	if ( m_lightfield_settings_current.render_single_lightfield )
	{
		LightfieldBatch batch;
		for ( size_t i = 0; i < m_valid_coords.size(); ++i )
		{
			if ( m_valid_coords[i] == m_lightfield_settings_current.single_texel_coords )
			{
		batch.first_texel_coords_idx = i;
			}
		}
		batch.texel_count = 1;
		batch.continuing = false;
		batch.island_id = 0;
		m_batches_to_process.push_back( batch );
	}
	else
	{
		const size_t cells_per_file = m_lightfield_settings_current.grid_size * m_lightfield_settings_current.grid_size;
		size_t current_island_start = 0;
		for ( size_t island = 0; island < num_island_coords.size(); ++island )
		{
			for ( size_t c = 0; c < (num_island_coords[island] + cells_per_file - 1) / cells_per_file; ++c )
			{
				LightfieldBatch batch;
				batch.first_texel_coords_idx = current_island_start + c * cells_per_file;
				batch.texel_count = std::min( cells_per_file, num_island_coords[island] - c * cells_per_file );
				batch.continuing = false;
				batch.island_id = island + 1;
				m_batches_to_process.push_back( batch );
			}
			current_island_start += num_island_coords[island];
		}
		const uint32_t num_pages = m_batches_to_process.size();
		const uint32_t l10 = uint32_t(std::floor(std::log10f( num_pages ))) + 1;
		size_t idx = 0;
		for ( auto& b : m_batches_to_process )
		{
			b.page_idx = idx;
			std::snprintf( b.filename, sizeof(b.filename), "%s%0*u.lfb", m_file_prefix.c_str(), l10, idx );
			++idx;
		}
	}
}

void LightfieldPathtracerRenderer::continueFromPartialFile( const std::string & filepath )
{
	std::string basePath = filepath;
	{
		for ( size_t i = 0; i < basePath.length(); ++i )
		{
			if ( basePath[i] == '\\' )
			{
				basePath[i] = '/';
			}
		}
		size_t slash = basePath.find_last_of( '/' );
		if ( slash != std::string::npos )
		{
			basePath = basePath.substr( 0, slash + 1 );
		}
		else
		{
			basePath = "";
		}
	}
	m_path = basePath;

	std::string filename = filepath.substr( basePath.length() );

	m_file_prefix = filename.substr( 0, filename.find_last_of( '.' ) );

	loadBatchesToProcess( filepath );

	initializePathtracer_internal();
}

void LightfieldPathtracerRenderer::loadBatchesToProcess( const std::string & filename )
{
	using json = nlohmann::json;
	json meta;
	try
	{
		std::ifstream fstream( filename );
		if ( fstream )
		{
			meta = json::parse( fstream );
		}
		else
		{
			LOG_ERROR( "Failed to load " << filename << ". File not found!" );
			return;
		}
	}
	catch ( std::exception& e )
	{
		LOG_ERROR( "Failed to load " << filename );
		DebugError( e.what() );
		return;
	}
	size_t version = meta["version"];
	DebugAssert( version == 4 );

	m_lightfield_settings_next.base_lightmap_size = meta["lightmapGridSize"];
	m_lightfield_settings_next.lightfield_size = meta["lightfieldSideSize"];
	glm::u16vec2 batch_size = meta["lightfieldBatchSize"];
	uint32_t max_bounces = meta["maxBouncesPerPath"];
	m_pImage->setMaxBounces( max_bounces );
	DebugAssert( batch_size.x == batch_size.y );
	m_lightfield_settings_next.grid_size = batch_size.x;
	m_lightfield_settings_next.max_jitter_roughness = meta["maxJitterRoughness"];
	m_lightfield_settings_current.max_jitter_position = meta["maxJitterPosition"];
	m_lightfield_settings_next.environment_as_alpha = meta["alphaIsEnvmapVisibility"];
	m_pImage->setMinRoughness( meta.value( "minRoughness", 0.f ) );

	updateImageSettings();

	LOG_INFO( "Continuing pathtrace of file " << filename << "\n"
			  << "Base lightmap size was " << m_lightfield_settings_current.base_lightmap_size.x << "x" << m_lightfield_settings_current.base_lightmap_size.y << " texels\n"
			  << "Lightfield size is " << m_lightfield_settings_current.lightfield_size << " pixels\n"
			  << "Batch grid size is " << m_lightfield_settings_current.grid_size << "\n"
			  << "Max bounces per path " << max_bounces << "\n"
			  << "Max jitter roughness is " << m_lightfield_settings_current.max_jitter_roughness << "rad\n"
			  << "Max jitter position multiplier is " << m_lightfield_settings_current.max_jitter_position << "\n"
			  << (m_lightfield_settings_current.environment_as_alpha ? "Treating" : "Not treating") << " environment map as alpha\n"
			  << "\n"
	);

	size_t batch_files_count = meta["batchFileCount"];

	// Load corrected lightmap info
	m_batches_to_process.clear();

	size_t texel_count = meta["texelCount"];
	m_valid_coords.resize( texel_count );

	for ( auto b : meta["batches"] )
	{
		LightfieldBatch batch;
		batch.first_texel_coords_idx = b["texelStartIdx"];
		batch.texel_count = b["texelCount"];
		batch.island_id = b.value( "islandId", 0 );
		batch.page_idx = b["pageIndex"];
		batch.continuing = true;
		std::strcpy( batch.filename, b["uri"].get<std::string>().c_str() );
		m_batches_to_process.push_back( batch );

		size_t i = 0;
		for ( auto t : b["texels"] )
		{
			m_valid_coords[batch.first_texel_coords_idx + i] = t;
			++i;
		}
	}


	{
		std::vector<uint32_t> baking_progress( m_batches_to_process.size(), 0 );

		for ( auto& batch : m_batches_to_process )
		{
			std::string batchFileName = m_path + batch.filename;
			std::ifstream bf( batchFileName, std::ifstream::binary );
			if ( bf )
			{
				uint16_t version = 0;
				half_float::half magic( 0.f );
				uint16_t lf_side_length = 0;
				uint16_t lf_count = 0;
				uint16_t lf_channels = 0;
				uint32_t lf_samples = 0;

				bf.read( reinterpret_cast<char*>(&magic), sizeof( half_float::half ) );
				bf.read( reinterpret_cast<char*>(&version), sizeof( uint16_t ) );
				if ( magic == half_float::half( 1.f / 3.f ) && version >= 4 )
				{
					bf.read( reinterpret_cast<char*>(&lf_side_length), sizeof( uint16_t ) );
					bf.read( reinterpret_cast<char*>(&lf_count), sizeof( uint16_t ) );
					bf.read( reinterpret_cast<char*>(&lf_channels), sizeof( uint16_t ) );
					bf.read( reinterpret_cast<char*>(&lf_samples), sizeof( uint32_t ) );
					baking_progress[batch.page_idx] = lf_samples;
				}
			}
		}

		m_batches_to_process.sort( [&]( const LightfieldBatch& a, const LightfieldBatch& b )
		{
			uint32_t a_samples = baking_progress[a.page_idx];
			uint32_t b_samples = baking_progress[b.page_idx];
			return a_samples < b_samples || ( a_samples == b_samples && a.page_idx < b.page_idx );
		} );
	}

	{
		std::ifstream cif( m_path + meta["texelInformation"].get<std::string>(), std::ifstream::binary );

		float magic;
		uint16_t version;
		uint16_t lm_width;
		uint16_t lm_height;
		cif.read( reinterpret_cast<char*>(&magic), sizeof( magic ) );
		DebugAssert( magic == 1.f / 5.f );
		cif.read( reinterpret_cast<char*>(&version), sizeof( version ) );
		DebugAssert( version == 1 );
		cif.read( reinterpret_cast<char*>(&lm_width), sizeof( lm_width ) );
		DebugAssert( lm_width == m_lightfield_settings_current.base_lightmap_size.x );
		cif.read( reinterpret_cast<char*>(&lm_height), sizeof( lm_height ) );
		DebugAssert( lm_height == m_lightfield_settings_current.base_lightmap_size.y );

		size_t lm_length = lm_width * lm_height;

		std::vector<char> validity( lm_length );
		cif.read( validity.data(), lm_length );
		std::vector<glm::vec3> positions( lm_length );
		cif.read( reinterpret_cast<char*>(positions.data()), lm_length * sizeof( glm::vec3 ) );

		// Skip shading normals array
		cif.seekg( lm_length * sizeof( glm::vec3 ), std::ios_base::cur );

		// Get geometry normals
		std::vector<glm::vec3> normals( lm_length );
		cif.read( reinterpret_cast<char*>(normals.data()), lm_length * sizeof( glm::vec3 ) );

		// Get geometry tangents
		std::vector<glm::vec3> tangents( lm_length );
		cif.read( reinterpret_cast<char*>(tangents.data()), lm_length * sizeof( glm::vec3 ) );

		// Get texel size
		std::vector<glm::vec2> texel_szs( lm_length );
		cif.read( reinterpret_cast<char*>(texel_szs.data()), lm_length * sizeof( glm::vec2 ) );


		std::vector<glm::vec4> pos_tex( lm_length );
		std::vector<glm::vec4> geo_norm_tex( lm_length );
		std::vector<glm::vec4> geo_tan_tex( lm_length );

		for ( size_t i = 0; i < lm_length; ++i )
		{
			pos_tex[i] = glm::vec4( positions[i], validity[i] == 0 ? 0.f : 1.f );
			geo_norm_tex[i] = glm::vec4( normals[i], 0.f );

			geo_tan_tex[i] = glm::vec4( tangents[i] * texel_szs[i].x, texel_szs[i].y );
		}

		if ( m_corrected_position_texture != 0 )
		{
			opengl_helpers::texture::deleteTexture( m_corrected_position_texture );
		}
		if ( m_geom_normal_texture != 0 )
		{
			opengl_helpers::texture::deleteTexture( m_geom_normal_texture );
		}
		if ( m_geom_tangent_texture != 0 )
		{
			opengl_helpers::texture::deleteTexture( m_geom_tangent_texture );
		}
		m_corrected_position_texture = opengl_helpers::texture::createTexture( lm_width, lm_height, GL_RGBA32F );   // position ; is_valid
		m_geom_normal_texture = opengl_helpers::texture::createTexture( lm_width, lm_height, GL_RGBA32F );   // position ; is_valid
		m_geom_tangent_texture = opengl_helpers::texture::createTexture( lm_width, lm_height, GL_RGBA32F );   // position ; is_valid

		glBindTexture( GL_TEXTURE_2D, m_corrected_position_texture );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, lm_width, lm_height, 0, GL_RGBA, GL_FLOAT, (float*)pos_tex.data());

		glBindTexture( GL_TEXTURE_2D, m_geom_normal_texture );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, lm_width, lm_height, 0, GL_RGBA, GL_FLOAT, (float*)geo_norm_tex.data());

		glBindTexture( GL_TEXTURE_2D, m_geom_tangent_texture );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, lm_width, lm_height, 0, GL_RGBA, GL_FLOAT, (float*)geo_tan_tex.data());


		// TODO: Load number of channels??????

		glBindTexture( GL_TEXTURE_2D, 0 );
	}


	m_positionsDirty = false;
}


}
