#include "ipathtracer_renderer.h"

#ifdef USE_PATHTRACER
#include "pathtracer/PathTracer.h"
#endif

#include <CHAGApp/imgui/imgui.h>
#include <stb_image_write.h>
#include <stb_image.h>
#include <filesystem>

namespace pathtracer
{

IPathtracingRenderer* IPathtracingRenderer::s_activePathtracer = nullptr;

GLSLProgramManager* IPathtracingRenderer::s_glslProgram = nullptr;
size_t IPathtracingRenderer::s_glslRefs = 0;

GLSLProgramManager * IPathtracingRenderer::s_addGlslRef()
{
	if ( s_glslProgram == nullptr )
	{
		s_glslProgram = new GLSLProgramManager( "../pathtracer/pathtracer.glsl" );
	}
	++s_glslRefs;
	return s_glslProgram;
}

void IPathtracingRenderer::s_decGlslRef()
{
	DebugAssert( s_glslRefs > 0 );
	if ( s_glslRefs == 1 )
	{
		delete s_glslProgram;
	}
	--s_glslRefs;
}



IPathtracingRenderer::IPathtracingRenderer()
	: IRenderer( s_addGlslRef() )
{
	m_pImage = new PathtracedImage;

	m_image_settings.max_bounces_per_path = m_pImage->getMaxBounces();
	m_image_settings.paths_per_pixel_per_pass = m_pImage->getNumPathsPerPixelPerPass();
	m_image_settings.min_roughness = m_pImage->getMinRoughness();
	m_image_settings.max_envmap_value = m_pImage->getMaxEnvmapValue();
}

IPathtracingRenderer::~IPathtracingRenderer()
{
	DebugAssert( m_pImage == nullptr );
}

void IPathtracingRenderer::init()
{
	pathtracer::initialize();
	m_copy_program = m_program_manager->getProgramObject("copy_vert", "copy_frag");
}


void IPathtracingRenderer::resize( uint16_t width, uint16_t height )
{
#ifdef USE_PATHTRACER
	m_pImage->resize( width, height );
#endif
}

void IPathtracingRenderer::destroy()
{
	if ( m_pImage )
	{
		delete m_pImage;
		m_pImage = nullptr;
	}
}

void IPathtracingRenderer::render()
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable( GL_DEPTH_TEST );

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_copy_program->enable();
	m_pImage->renderQuad();
	m_copy_program->disable();

	glPopAttrib();

	renderGUI();
}

void IPathtracingRenderer::pathTrace()
{
	if ( !isPathTracing() )
	{
		return;
	}

	setAsActivePathtracer();

	pathTrace_impl();
}

void IPathtracingRenderer::renderGUI()
{
	ImGui::Begin( "Pathtrace Scene" );
	{
		if ( m_is_pathtracing )
		{
			if ( ImGui::Button( "Stop pathtracing" ) )
			{
				stopPathTracing();
			}
		}
		else
		{
			if ( ImGui::Button( "Start pathtracing" ) )
			{
				startPathTracing();
			}
		}
		if ( ImGui::Button( "Restart pathtracing" ) )
		{
			restart();
		}
	}
	ImGui::End();
}

void IPathtracingRenderer::save( const std::string & filename )
{
	glm::ivec2 sz = m_pImage->getSize();
	const glm::vec4* fdata = (const glm::vec4*)m_pImage->getData();

	std::string ext = std::filesystem::path( filename ).extension().string();
	if ( ext == "png" )
	{
		std::vector<glm::u8vec4> bdata ( sz.x * sz.y );
		for ( size_t i = 0; i < sz.x * sz.y; ++i )
		{
			bdata[i].x = glm::u8( glm::clamp( fdata[i].x * 255, 0.f, 255.f ) );
			bdata[i].y = glm::u8( glm::clamp( fdata[i].y * 255, 0.f, 255.f ) );
			bdata[i].z = glm::u8( glm::clamp( fdata[i].z * 255, 0.f, 255.f ) );
			bdata[i].w = 255;
		}
		stbi_write_png( filename.c_str(), sz.x, sz.y, 4, bdata.data(), sz.x * sizeof( glm::u8vec4 ) );
	}
	else
	{
		stbi_write_hdr( filename.c_str(), sz.x, sz.y, 4, reinterpret_cast<const float*>(fdata) );
	}
}

void IPathtracingRenderer::restart()
{
	m_pImage->restart();
}

void IPathtracingRenderer::setNumPathsPerPixelPerPass( size_t paths )
{
	m_image_settings.paths_per_pixel_per_pass = paths;
	m_image_settings_changed = true;
}

void IPathtracingRenderer::setMaxBouncesPerPath( uint32_t bounces )
{
	m_image_settings.max_bounces_per_path = bounces;
	m_image_settings_changed = true;
}

void IPathtracingRenderer::setMaxSamplesPerPixel( size_t samples )
{
	m_max_samples_per_pixel = samples;
}

bool IPathtracingRenderer::hasFinishedMaxSamplesPerPixel() const
{
	return m_max_samples_per_pixel != 0 && m_pImage->getCurrentNumberOfSamples() > m_max_samples_per_pixel;
}

void IPathtracingRenderer::setMinRoughness( float roughness )
{
	m_image_settings.min_roughness = roughness;
	m_image_settings_changed = true;
}

void IPathtracingRenderer::setMaxEnvmapValue( float envmap )
{
	m_image_settings.max_envmap_value = envmap;
	m_image_settings_changed = true;
}

void IPathtracingRenderer::updateImageSettings()
{
	m_pImage->setNumPathsPerPixelPerPass( m_image_settings.paths_per_pixel_per_pass );
	m_pImage->setMaxBounces( m_image_settings.max_bounces_per_path );
	m_pImage->setMinRoughness( m_image_settings.min_roughness );
	m_pImage->setMaxEnvmapValue( m_image_settings.max_envmap_value );
	m_image_settings_changed = false;
}

void IPathtracingRenderer::setAsActivePathtracer()
{
	if ( s_activePathtracer != this )
	{
		s_activePathtracer = this;
		s_activePathtracer->onSetAsActive();
	}
}

}
