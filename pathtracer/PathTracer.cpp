#include "PathTracer.h"

#include "scene/Scene.h"
#include "utils/Log.h"
#include "utils/ScopeTimer.h"

#include "optix_nvrtc_config.h"

#include <GL/glew.h>

#include <nvrtc.h>
#include <optix.h>
#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_math_namespace.h>

#include <chrono>
#include <random>

#include <cuda_runtime.h>

#ifdef _DEBUG
#define USE_DEBUG_SHADERS
#endif

#ifndef M_PI
#define M_PI 3.141592653589793238462643
#endif

#ifdef USE_DEBUG_SHADERS
#define SHADERS_TRACE_DEBUG
#endif

#define NVRTC_CHECK_ERROR( func )                      \
	do                                                 \
	{                                                  \
		nvrtcResult code = func;                       \
		if ( code != NVRTC_SUCCESS )                   \
			DebugError( nvrtcGetErrorString( code ) ); \
	} while ( 0 )

#define RT_CHECK_ERROR( func )                                                    \
	do                                                                            \
	{                                                                             \
		try                                                                       \
		{                                                                         \
			func;                                                                 \
		} catch ( optix::Exception e )                                            \
		{                                                                         \
			DebugError( optix_data.context->getErrorString( e.getErrorCode() ) ); \
		}                                                                         \
	} while ( 0 )



static inline optix::float4 tof( glm::vec4 v )
{
	return optix::make_float4( v.x, v.y, v.z, v.w );
}

static inline glm::vec4 tof( optix::float4 v )
{
	return glm::vec4( v.x, v.y, v.z, v.w );
}

static inline optix::float3 tof( glm::vec3 v )
{
	return optix::make_float3( v.x, v.y, v.z );
}

static inline glm::vec3 tof( optix::float3 v )
{
	return glm::vec3( v.x, v.y, v.z );
}

static inline optix::float2 tof( glm::vec2 v )
{
	return optix::make_float2( v.x, v.y );
}

static inline glm::vec2 tof( optix::float2 v )
{
	return glm::vec2( v.x, v.y );
}

using namespace std;
using namespace glm;


namespace pathtracer
{

static struct Light
{
	glm::vec3 position;
	float intensity = 0;
	glm::vec3 color;
};

static struct
{
	optix::Context context = 0;

	optix::Buffer generated_ray_count_buffer;
	uint64_t generated_ray_count_since_last_report = 0;
	uint64_t samples_since_last_report = 0;
	std::chrono::time_point<std::chrono::high_resolution_clock> last_reported_ray_count_time;

	optix::Program triangle_mesh_attribute_program;

	optix::Program closest_hit_program;
	optix::Program any_hit_program;

	optix::Buffer environment;
	optix::TextureSampler environmentSampler;

	optix::TextureSampler defaultTextureSampler;

	optix::GeometryInstance geometryInstance;
	std::vector<optix::Material> materials;

	uint32_t seed = 0;
	std::vector<Light> lights;
} optix_data;

struct PathtracedImage::Image
{
	uint16_t width = 0, height = 0;
	uint8_t num_channels = 4;
	glm::uvec3 render_channels = glm::uvec3(0, 1, 2);
	std::vector<float> data;
	std::vector<float> render_data;
	float* getPtr() { return data.data(); }
	GLuint gl_image_id;
	size_t frame_number = 0;
	uint32_t max_bounces = 4;
	size_t paths_per_pixel_per_pas = 1;
	size_t number_of_samples = 0;
	optix::Buffer output_buffer;
	float min_roughness = 0.f;
	float max_envmap_value = 0.f;
};

static scene::Scene* s_scene = nullptr;

static std::string loadPtxFromCudaFile( const char* filename, const std::vector<std::string>& define_flags );
static optix::Buffer createBuffer( RTbuffertype type, RTformat format, unsigned width, unsigned height );
static optix::Buffer createBuffer( RTbuffertype type, RTformat format, unsigned width, unsigned height, unsigned depth );

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

void prepareMaterials()
{
	optix_data.materials.clear();

	for ( const auto& model : s_scene->m_models )
	{
		for ( const auto& mat : model->m_materials )
		{
			optix::Material xmat = optix_data.context->createMaterial();
			if ( optix_data.closest_hit_program )
			{
				xmat->setClosestHitProgram( 0, optix_data.closest_hit_program );
			}
			if ( optix_data.any_hit_program )
			{
				xmat->setAnyHitProgram( 1, optix_data.any_hit_program );
			}

			xmat["material_base_color_factor"]->setFloat( tof( mat.second->m_base_color_factor ) );
			xmat["material_normal_scale"]->setFloat( mat.second->m_normal_scale );
			xmat["material_alpha_cutoff"]->setFloat( mat.second->m_alpha_cutoff );
			xmat["material_metallic_factor"]->setFloat( mat.second->m_metallic_factor );
			xmat["material_roughness_factor"]->setFloat( mat.second->m_roughness_factor );
			xmat["material_emissive_factor"]->setFloat( tof( mat.second->m_emissive_factor ) );

			auto setTexSampler = [&]( const char* var_name, scene::Texture* texture )
			{
				optix::Variable tex_var = xmat->declareVariable( var_name );
				if ( texture )
				{
					optix::TextureSampler tex;
					RT_CHECK_ERROR( tex = optix_data.context->createTextureSamplerFromGLImage(
											texture->m_GLID, RTgltarget::RT_TARGET_GL_TEXTURE_2D ) );
					RT_CHECK_ERROR( tex->setWrapMode( 0, RT_WRAP_REPEAT ) );
					RT_CHECK_ERROR( tex->setWrapMode( 1, RT_WRAP_REPEAT ) );
					RT_CHECK_ERROR( tex->setIndexingMode( RT_TEXTURE_INDEX_NORMALIZED_COORDINATES ) );
					RT_CHECK_ERROR( tex->setFilteringModes( RT_FILTER_LINEAR, RT_FILTER_LINEAR, RT_FILTER_NONE ) );
					RT_CHECK_ERROR( tex->setMaxAnisotropy( 1.f ) );
					tex_var->setTextureSampler( tex );
				}
				else
				{
					tex_var->setTextureSampler( optix_data.defaultTextureSampler );
				}
			};

			// Textures
			setTexSampler( "material_base_color_texture", mat.second->m_base_color_texture );
			setTexSampler( "material_normal_texture", mat.second->m_normal_texture );
			xmat["material_has_normal_texture"]->setInt( mat.second->m_normal_texture != nullptr ? 1 : 0 );
			setTexSampler( "material_emissive_texture", mat.second->m_emissive_texture );
			setTexSampler( "material_metal_roughness_texture", mat.second->m_metallic_roughness_texture );


			optix_data.materials.push_back( xmat );
		}
	}

	optix_data.geometryInstance->setMaterialCount( optix_data.materials.size() );

	for ( size_t i = 0; i < optix_data.materials.size(); ++i )
	{
		optix::Material& mat = optix_data.materials[i];
		optix_data.geometryInstance->setMaterial( i, mat );
	}
}

static void setEnvironmentMap( scene::Texture* environmentMap )
{
	const size_t n_horizontal_buckets = 256;
	const size_t n_vertical_buckets = 128;

	float rows_pdf[n_horizontal_buckets * n_vertical_buckets] = { 0.f };
	float marginal_pdf[n_vertical_buckets] = { 0.f };
	float rows_cdf[n_horizontal_buckets * n_vertical_buckets] = { 0.f };
	float marginal_cdf[n_vertical_buckets] = { 0.f };

	if ( environmentMap && environmentMap->m_format == scene::Texture::Format::F_RGBA )
	{
		optix::TextureSampler tex;
		RT_CHECK_ERROR( tex = optix_data.context->createTextureSamplerFromGLImage(
								environmentMap->m_GLID, RTgltarget::RT_TARGET_GL_TEXTURE_2D ) );
		RT_CHECK_ERROR( tex->setWrapMode( 0, RT_WRAP_CLAMP_TO_EDGE ) );
		RT_CHECK_ERROR( tex->setWrapMode( 1, RT_WRAP_CLAMP_TO_EDGE ) );
		RT_CHECK_ERROR( tex->setIndexingMode( RT_TEXTURE_INDEX_NORMALIZED_COORDINATES ) );
		RT_CHECK_ERROR( tex->setFilteringModes( RT_FILTER_LINEAR, RT_FILTER_LINEAR, RT_FILTER_NONE ) );
		RT_CHECK_ERROR( tex->setMaxAnisotropy( 1.f ) );
		optix_data.context["environment_map"]->setTextureSampler( tex );

		if ( environmentMap->m_width % n_horizontal_buckets != 0 || environmentMap->m_height % n_vertical_buckets != 0)
		{
			LOG_ERROR( "The environment map needs to be a size multiple of "
			           << n_horizontal_buckets << "x" << n_vertical_buckets << " for it to work properly, but it's "
			           << environmentMap->m_width << "x" << environmentMap->m_height );
		}

		const float horizontal_px_per_bucket = float(environmentMap->m_width) / n_horizontal_buckets;
		const float vertical_px_per_bucket = float(environmentMap->m_height) / n_vertical_buckets;
		float marginal_cdf_cur = 0;
		for ( int j = 0; j < n_vertical_buckets; ++j )
		{
			marginal_pdf[j] = 0;
			for ( size_t i = 0; i < n_horizontal_buckets; ++i )
			{
				float bucket_value = 0;
				for ( size_t y = vertical_px_per_bucket * j; y < vertical_px_per_bucket * (j + 1); ++y )
				{
					for ( size_t x = horizontal_px_per_bucket * i; x < horizontal_px_per_bucket * (i + 1); ++x )
					{
						bucket_value += glm::length( environmentMap->getRawValue( { x, y } ) );
					}
				}
				rows_pdf[i + j * n_horizontal_buckets] = bucket_value;
				marginal_pdf[j] += bucket_value;
				rows_cdf[i + j * n_horizontal_buckets] = marginal_pdf[j];
			}
			//float theta = (j + 0.5f) / float( n_vertical_buckets ) * M_PI;
			//marginal_pdf[j] *= sin( theta );

			marginal_cdf_cur += marginal_pdf[j];
			marginal_cdf[j] = marginal_cdf_cur;

			for ( size_t i = 0; i < n_horizontal_buckets; ++i )
			{
				rows_pdf[i + j * n_horizontal_buckets] *= n_horizontal_buckets / marginal_pdf[j];
				rows_cdf[i + j * n_horizontal_buckets] /= marginal_pdf[j];
			}
		}
		for ( size_t j = 0; j < n_vertical_buckets; ++j )
		{
			marginal_pdf[j] *= n_vertical_buckets / marginal_cdf_cur;
			marginal_cdf[j] /= marginal_cdf_cur;
		}
	}
	else
	{
		if ( environmentMap && environmentMap->m_format != scene::Texture::Format::F_RGBA )
		{
			LOG_ERROR( "Invalid environment map texture format." );
		}
		optix_data.context["environment_map"]->setTextureSampler( optix_data.defaultTextureSampler );

		float marginal_cdf_cur = 0;
		for ( size_t j = 0; j < n_vertical_buckets; ++j )
		{
			float theta = (j + 0.5f) / float( n_vertical_buckets ) * M_PI;
			marginal_pdf[j] = sin( theta );
			marginal_cdf_cur += marginal_pdf[j];
			marginal_cdf[j] = marginal_cdf_cur;

			for ( size_t i = 0; i < n_horizontal_buckets; ++i )
			{
				rows_pdf[i + j * n_horizontal_buckets] = 1.f;
				rows_cdf[i + j * n_horizontal_buckets] = (i + 1) / float( n_horizontal_buckets );
			}
		}
		for ( size_t j = 0; j < n_vertical_buckets; ++j )
		{
			marginal_pdf[j] *= n_vertical_buckets / marginal_cdf_cur;
			marginal_cdf[j] /= marginal_cdf_cur;
		}
	}

	optix::Buffer buffer_marginal_pdf = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT, n_vertical_buckets );
	optix::Buffer buffer_marginal_cdf = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT, n_vertical_buckets );
	optix::Buffer buffer_rows_pdf = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT, n_horizontal_buckets, n_vertical_buckets );
	optix::Buffer buffer_rows_cdf = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT, n_horizontal_buckets, n_vertical_buckets );

	std::memcpy( buffer_marginal_pdf->map(), marginal_pdf, n_vertical_buckets * sizeof( float ) );
	std::memcpy( buffer_marginal_cdf->map(), marginal_cdf, n_vertical_buckets * sizeof( float ) );
	std::memcpy( buffer_rows_pdf->map(), rows_pdf, n_vertical_buckets * n_horizontal_buckets * sizeof( float ) );
	std::memcpy( buffer_rows_cdf->map(), rows_cdf, n_vertical_buckets * n_horizontal_buckets * sizeof( float ) );

	buffer_marginal_pdf->unmap();
	buffer_marginal_cdf->unmap();
	buffer_rows_pdf->unmap();
	buffer_rows_cdf->unmap();


	optix_data.context["envmap_marginal_pdf"]->setBuffer( buffer_marginal_pdf );
	optix_data.context["envmap_marginal_cdf"]->setBuffer( buffer_marginal_cdf );
	optix_data.context["envmap_rows_pdf"]->setBuffer( buffer_rows_pdf );
	optix_data.context["envmap_rows_cdf"]->setBuffer( buffer_rows_cdf );
}

void setScene( scene::Scene* scene )
{
	s_scene = scene;

	// Setup environment map
	setEnvironmentMap( scene->m_environmentMap );

	optix_data.context["environment_color"]->setFloat( tof( scene->m_environment_color ) );
	optix_data.context["environment_multiplier"]->setFloat( scene->m_envmap_intensity_multiplier );

	////////////////////////////////
	// Setup geometries
	////////////////////////////////

	size_t vtxBufferCount = 0;
	size_t idxCount = 0;
	for ( const auto& model : scene->m_models )
	{
		vtxBufferCount += model->m_positions.m_host_vector.size();
		idxCount += model->m_indices.m_host_vector.empty() ? model->m_positions.m_host_vector.size() : model->m_indices.m_host_vector.size();
	}

	optix::Buffer verts_buffer = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT3, vtxBufferCount );
	optix::Buffer norms_buffer = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT3, vtxBufferCount );
	optix::Buffer tans_buffer = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT3, vtxBufferCount );
	optix::Buffer uvs_buffer = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT2, vtxBufferCount );
	optix::Buffer mats_buffer = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_UNSIGNED_INT, idxCount / 3 );

	optix::Buffer idxs_buffer = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_UNSIGNED_INT3, idxCount / 3 );

	optix::float3* verts = (optix::float3*)verts_buffer->map();
	optix::float3* norms = (optix::float3*)norms_buffer->map();
	optix::float3* tans = (optix::float3*)tans_buffer->map();
	optix::float2* uvs = (optix::float2*)uvs_buffer->map();
	unsigned int* mat_ids = (unsigned int*)mats_buffer->map();
	unsigned int* idxs = (unsigned int*)idxs_buffer->map();


	std::map<scene::Material*, size_t> matIds;

	size_t materialsCount = 0;
	for ( const auto& model : scene->m_models )
	{
		for ( const auto& mat : model->m_materials )
		{
			DebugAssert( matIds.find( mat.second ) == matIds.end() );
			matIds[mat.second] = materialsCount++;
		}
	}

	vtxBufferCount = 0;
	materialsCount = 0;
	idxCount = 0;

	for ( const auto& model : scene->m_models )
	{
		const glm::mat4& m = model->m_transform.get_MV_inv();
		glm::mat3 nm( m );

		for ( size_t i = 0; i < model->m_positions.m_host_vector.size(); ++i )
		{
			verts[vtxBufferCount + i] = tof( vec3( m * vec4( model->m_positions.m_host_vector[i], 1 ) ) );
			norms[vtxBufferCount + i] = tof( nm * model->m_normals.m_host_vector[i] );
			tans[vtxBufferCount + i] = tof( nm * model->m_tangents.m_host_vector[i] );
			uvs[vtxBufferCount + i] = tof( model->m_uvs[0].m_host_vector[i] );
		}


		size_t idxsAdded = 0;
		if ( model->m_indices.m_host_vector.empty() )
		{
			idxsAdded = model->m_positions.m_host_vector.size();
			for ( size_t i = 0; i < idxsAdded; ++i )
			{
				idxs[idxCount + i] = vtxBufferCount + i;
			}
		}
		else
		{
			idxsAdded = model->m_indices.m_host_vector.size();
			for ( size_t i = 0; i < idxsAdded; ++i )
			{
				idxs[idxCount + i] = vtxBufferCount + model->m_indices.m_host_vector[i];
			}
		}

		for ( size_t i = 0; i < idxsAdded / 3; ++i )
		{
			mat_ids[materialsCount + i] = matIds[model->m_triangleMaterial[i]];
		}

		vtxBufferCount += model->m_positions.m_host_vector.size();
		idxCount += idxsAdded;
		materialsCount += idxsAdded / 3;
	}


	idxs_buffer->unmap();
	mats_buffer->unmap();
	uvs_buffer->unmap();
	tans_buffer->unmap();
	norms_buffer->unmap();
	verts_buffer->unmap();


	optix::GeometryTriangles geom = optix_data.context->createGeometryTriangles();
	geom->setPrimitiveCount( materialsCount );
	geom->setMaterialIndices( mats_buffer, 0, sizeof(unsigned int), RT_FORMAT_UNSIGNED_INT );
	geom->setTriangleIndices( idxs_buffer, RT_FORMAT_UNSIGNED_INT3 );
	geom->setVertices( vtxBufferCount, verts_buffer, RT_FORMAT_FLOAT3 );
	geom->setPrimitiveIndexOffset( 0 );
	geom->setBuildFlags( RTgeometrybuildflags( 0 ) );
	geom->setAttributeProgram( optix_data.triangle_mesh_attribute_program );


	optix_data.geometryInstance = optix_data.context->createGeometryInstance();
	optix_data.geometryInstance->setGeometryTriangles( geom );

	prepareMaterials();
	geom->setMaterialCount( optix_data.materials.size() );
	DebugAssert( optix_data.materials.size() <= matIds.size() );

	optix::GeometryGroup gg = optix_data.context->createGeometryGroup();
	// Sbvh, Bvh, Trbvh
	gg->setAcceleration( optix_data.context->createAcceleration( "Trbvh" ) );
	gg->addChild( optix_data.geometryInstance );

	optix_data.context["index_buffer"]->setBuffer( idxs_buffer );
	optix_data.context["vertex_buffer"]->setBuffer( verts_buffer );
	optix_data.context["normal_buffer"]->setBuffer( norms_buffer );
	optix_data.context["tangent_buffer"]->setBuffer( tans_buffer );
	optix_data.context["texcoord_buffer"]->setBuffer( uvs_buffer );
	optix_data.context["top_scene_node"]->set( gg );
	optix_data.context["top_shadowing_node"]->set( gg );


	////////////////////////////////
	// Setup Lights
	////////////////////////////////
	updateLights();
}

void updateLights()
{
	optix_data.context["num_lights"]->setInt( s_scene->m_lights.size() );
	if ( s_scene->m_lights.size() == 0 )
		return;

	///////////////////////////////////////////////////////////////////////////
	// Check if number of lights has changed, and allocate memory as necessary
	///////////////////////////////////////////////////////////////////////////
	optix::Buffer l_pos_buffer, l_int_buffer, l_col_buffer;
	bool buffers_exist = optix_data.lights.size() > 0;
	if ( buffers_exist )
	{
		l_pos_buffer = optix_data.context["lights_position"]->getBuffer();
		l_int_buffer = optix_data.context["lights_intensity"]->getBuffer();
		l_col_buffer = optix_data.context["lights_color"]->getBuffer();
	}
	if ( optix_data.lights.size() != s_scene->m_lights.size() )
	{
		optix_data.lights.clear();
		if ( buffers_exist )
		{
			// Delete previous buffers
			l_pos_buffer->destroy();
			l_int_buffer->destroy();
			l_col_buffer->destroy();
		}
		// Create optix buffers
		l_pos_buffer = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT3, s_scene->m_lights.size() );
		l_int_buffer = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT, s_scene->m_lights.size() );
		l_col_buffer = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT3, s_scene->m_lights.size() );
		// Connect to program
		optix_data.context["lights_position"]->setBuffer( l_pos_buffer );
		optix_data.context["lights_intensity"]->setBuffer( l_int_buffer );
		optix_data.context["lights_color"]->setBuffer( l_col_buffer );
	}

	optix_data.lights.resize( s_scene->m_lights.size() );
	for ( size_t i = 0; i < s_scene->m_lights.size(); ++i )
	{
		optix_data.lights[i].position = s_scene->m_lights[i]->getView()->pos;
		optix_data.lights[i].intensity = s_scene->m_lights[i]->m_radiance;
		optix_data.lights[i].color = s_scene->m_lights[i]->m_color;
	}

	glm::vec3* poss = (glm::vec3*)l_pos_buffer->map();
	float* ints = (float*)l_int_buffer->map();
	glm::vec3* cols = (glm::vec3*)l_col_buffer->map();

	for ( size_t i = 0; i < optix_data.lights.size(); ++i )
	{
		poss[i] = optix_data.lights[i].position;
		ints[i] = optix_data.lights[i].intensity;
		cols[i] = optix_data.lights[i].color;
	}

	l_pos_buffer->unmap();
	l_int_buffer->unmap();
	l_col_buffer->unmap();
}


///////////////////////////////////////////////////////////////////////////////


void initialize()
{
	if ( optix_data.context != 0 )
	{
		return;
	}

	int RTX = 1;
	if ( rtGlobalSetAttribute( RT_GLOBAL_ATTRIBUTE_ENABLE_RTX, sizeof( RTX ), &RTX ) != RT_SUCCESS )
	{
		LOG_ERROR( "Error setting RTX mode." );
		RTX = 0;
		DebugAssert( rtGlobalSetAttribute( RT_GLOBAL_ATTRIBUTE_ENABLE_RTX, sizeof( RTX ), &RTX ) == RT_SUCCESS );
	}
	else
	{
		LOG_INFO( "OptiX RTX execution mode is " + std::string((RTX) ? "on" : "off") );
	}

	optix_data.context = optix::Context::create();
	optix_data.context->setRayTypeCount( 0 );
	optix_data.context->setEntryPointCount( 0 );
	optix_data.context->setStackSize( 1800 );

	// Super magenta to make sure it doesn't get averaged out in the progressive rendering.
	optix_data.context["bad_color"]->setFloat( 1000000.0f, 0.0f, 1000000.0f );

	optix_data.context["bg_color"]->setFloat( optix::make_float3( 0.0f ) );

	{
		std::random_device rand_dev;
		optix_data.seed = rand_dev();
	}

	try
	{
		std::string ptx = loadPtxFromCudaFile( "../pathtracer/triangle_mesh.cu", {} );
		optix_data.triangle_mesh_attribute_program = optix_data.context->createProgramFromPTXString( ptx, "triangle_mesh_attributes" );
	}
	catch ( optix::Exception& e )
	{
		DebugError( "Cuda compilation error " + e.getErrorString() );
	}

#ifdef SHADERS_TRACE_DEBUG
	optix_data.context->setPrintEnabled( true );
	optix_data.context->setPrintBufferSize( 1<<16 );
#endif

	// Create default textures variable
	{
		optix::Buffer default_tex = optix_data.context->createBuffer( RT_BUFFER_INPUT, RT_FORMAT_FLOAT4, 4, 4 );
		vec4* img = (vec4*)default_tex->map();
		for ( size_t j = 0; j < 4; ++j )
		{
			for ( size_t i = 0; i < 4; ++i )
			{
				img[( j * 4 ) + i] = vec4( 1, 1, 1, 1 );
			}
		}
		default_tex->unmap();

		RT_CHECK_ERROR( optix_data.defaultTextureSampler = optix_data.context->createTextureSampler() );
		RT_CHECK_ERROR( optix_data.defaultTextureSampler->setWrapMode( 0, RT_WRAP_CLAMP_TO_EDGE ) );
		RT_CHECK_ERROR( optix_data.defaultTextureSampler->setWrapMode( 1, RT_WRAP_CLAMP_TO_EDGE ) );
		RT_CHECK_ERROR( optix_data.defaultTextureSampler->setIndexingMode( RT_TEXTURE_INDEX_NORMALIZED_COORDINATES ) );
		RT_CHECK_ERROR( optix_data.defaultTextureSampler->setFilteringModes( RT_FILTER_LINEAR, RT_FILTER_LINEAR, RT_FILTER_NONE ) );
		RT_CHECK_ERROR( optix_data.defaultTextureSampler->setMaxAnisotropy( 1.f ) );
		RT_CHECK_ERROR( optix_data.defaultTextureSampler->setMipLevelCount( 1 ) );
		RT_CHECK_ERROR( optix_data.defaultTextureSampler->setArraySize( 1 ) );
		RT_CHECK_ERROR( optix_data.defaultTextureSampler->setBuffer( 0, 0, default_tex ) );
	}
}


///////////////////////////////////////////////////////////////////////////////


void finalize()
{
	if ( optix_data.context )
	{
		optix_data.materials.clear();
		optix_data.context->destroy();
		optix_data.context = 0;
	}
}



///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////



PathtracedImage::PathtracedImage()
{
	m_rendered_image = new Image;
}

PathtracedImage::~PathtracedImage()
{
	delete m_rendered_image;
}

void PathtracedImage::initialize( const char* cu_file,
                                  const char* ray_gen_pg,
                                  const char* closest_hit_pg,
                                  const char* any_hit_pg,
                                  const char* miss_pg,
                                  const char* exception_pg,
                                  const std::vector<std::string>& define_flags )
{
	pathtracer::initialize();


	// Setup programs
	optix_data.context->setEntryPointCount( 1 );

	optix_data.context->setRayTypeCount( 2 );

	std::string ptx = loadPtxFromCudaFile( cu_file, define_flags );

	try
	{
		optix_data.context->setRayGenerationProgram( 0, optix_data.context->createProgramFromPTXString( ptx, ray_gen_pg ) );
		optix_data.context->setExceptionProgram( 0, optix_data.context->createProgramFromPTXString( ptx, exception_pg ) );
		optix_data.context->setMissProgram( 0, optix_data.context->createProgramFromPTXString( ptx, miss_pg ) );

		optix_data.closest_hit_program = optix_data.context->createProgramFromPTXString( ptx, closest_hit_pg );
		optix_data.any_hit_program = optix_data.context->createProgramFromPTXString( ptx, any_hit_pg );
	}
	catch ( optix::Exception& e )
	{
		DebugError( "Cuda compilation error " + e.getErrorString() );
	}

	if ( s_scene && optix_data.geometryInstance )
	{
		prepareMaterials();
	}

	optix_data.generated_ray_count_since_last_report = 0;
	optix_data.samples_since_last_report = 0;
	optix_data.last_reported_ray_count_time = std::chrono::high_resolution_clock::now();
}

void PathtracedImage::resize( int w, int h )
{
	m_rendered_image->width = w;
	m_rendered_image->height = h;
	m_rendered_image->data.resize( m_rendered_image->width * m_rendered_image->height * m_rendered_image->num_channels );
	m_rendered_image->render_data.resize( m_rendered_image->width * m_rendered_image->height * 3 );

	// Delete old buffer
	if ( m_rendered_image->output_buffer )
	{
		m_rendered_image->output_buffer->destroy();
	}

	m_rendered_image->output_buffer = createBuffer( RT_BUFFER_OUTPUT, RT_FORMAT_FLOAT, m_rendered_image->width, m_rendered_image->height, m_rendered_image->num_channels );

	restart();
}

glm::ivec2 PathtracedImage::getSize() const
{
	return glm::ivec2( m_rendered_image->width, m_rendered_image->height );
}

const float * PathtracedImage::getData() const
{
	return m_rendered_image->data.data();
}

void PathtracedImage::setNumChannels( uint8_t n_channels )
{
	if ( n_channels != m_rendered_image->num_channels )
	{
		m_rendered_image->num_channels = n_channels;
		if ( m_rendered_image->output_buffer )
		{
			resize( m_rendered_image->width, m_rendered_image->height );
		}
	}
}

uint8_t PathtracedImage::getNumChannels() const
{
	return m_rendered_image->num_channels;
}

void PathtracedImage::setRenderChannels( glm::uvec3 channels )
{
	m_rendered_image->render_channels = channels;
}

void PathtracedImage::restart()
{
	// No need to clear image,
	m_rendered_image->number_of_samples = 0;
}

void PathtracedImage::tracePaths()
{
	// Set the ray types to use for the current program
	optix_data.context["closest_hit_ray_type_id"]->setUint( 0 );
	optix_data.context["any_hit_ray_type_id"]->setUint( 1 );

	// Update output buffer
	optix_data.context["output_buffer"]->set( m_rendered_image->output_buffer );

	{
		RTsize width = 0, height = 0;
		if ( optix_data.generated_ray_count_buffer )
		{
			optix_data.generated_ray_count_buffer->getSize( width, height );
			if ( width != m_rendered_image->width || height != m_rendered_image->height )
			{
				optix_data.generated_ray_count_buffer->destroy();
			}
		}
		if ( width != m_rendered_image->width || height != m_rendered_image->height )
		{
			optix_data.generated_ray_count_buffer = optix_data.context->createBuffer(
			        RT_BUFFER_OUTPUT, RT_FORMAT_UNSIGNED_INT, m_rendered_image->width, m_rendered_image->height );
		}
		optix_data.context["generated_ray_count"]->set( optix_data.generated_ray_count_buffer );
	}

	optix_data.context["rnd_seed"]->setUint( optix_data.seed );
	optix_data.context["frame_number"]->setUint( m_rendered_image->frame_number++ );
	optix_data.context["max_bounces"]->setUint( m_rendered_image->max_bounces );
	optix_data.context["max_paths"]->setUint( m_rendered_image->paths_per_pixel_per_pas );

	optix_data.context["min_roughness"]->setFloat( m_rendered_image->min_roughness );
	optix_data.context["max_envmap_value"]->setFloat( m_rendered_image->max_envmap_value );

	{
		//PROFILE_CUDA( "Pathtrace" );
		RT_CHECK_ERROR( optix_data.context->launch( 0, m_rendered_image->width, m_rendered_image->height ) );
	}

	// Copy buffer to texture
	if ( optix_data.context["output_buffer"]->getType() == RTobjecttype::RT_OBJECTTYPE_BUFFER )
	{
		optix::Buffer buffer = optix_data.context["output_buffer"]->getBuffer();

		void* mem = buffer->map();
		float* x_img = (float*)mem;

#pragma omp parallel for
		for ( int c = 0; c < m_rendered_image->num_channels; ++c )
		{
			for ( int j = 0; j < m_rendered_image->height; ++j )
			{
				for ( int i = 0; i < m_rendered_image->width; ++i )
				{
					double n = double( m_rendered_image->number_of_samples );
					double m = double( m_rendered_image->paths_per_pixel_per_pas );
					size_t rpos = c * m_rendered_image->width * m_rendered_image->height + j * m_rendered_image->width + i;
					size_t wpos = ( j * m_rendered_image->width + i ) * m_rendered_image->num_channels + c;
					float p = x_img[rpos];
					m_rendered_image->data[wpos] = (m_rendered_image->data[wpos] * n + p * m) / (n + m);
				}
			}
		}
		buffer->unmap();
#pragma omp parallel for
		for ( int cc = 0; cc < 3; ++cc )
		{
			int c = m_rendered_image->render_channels[cc];
			for ( int j = 0; j < m_rendered_image->height; ++j )
			{
				for ( int i = 0; i < m_rendered_image->width; ++i )
				{
					size_t rpos = ( j * m_rendered_image->width + i ) * m_rendered_image->num_channels + c;
					size_t wpos = ( j * m_rendered_image->width + i ) * 3 + cc;
					m_rendered_image->render_data[wpos] = m_rendered_image->data[rpos];
				}
			}
		}
	}

	{
		void* mem = optix_data.generated_ray_count_buffer->map();
		unsigned int *b = (unsigned int*)mem;
		for ( int j = 0; j < m_rendered_image->height; ++j )
		{
			for ( int i = 0; i < m_rendered_image->width; ++i )
			{
				unsigned int t = b[m_rendered_image->width * j + i];
				optix_data.generated_ray_count_since_last_report += t;
			}
		}
		optix_data.generated_ray_count_buffer->unmap();

		optix_data.samples_since_last_report += m_rendered_image->number_of_samples * m_rendered_image->width * m_rendered_image->height;

		auto now = std::chrono::high_resolution_clock::now();
		auto diff = now - optix_data.last_reported_ray_count_time;
		if ( diff >= 5s )
		{
			using namespace std::chrono;
			uint64_t rays_per_second =
			        ( optix_data.generated_ray_count_since_last_report * 1000 ) / duration_cast<milliseconds>( diff ).count();
			uint64_t samples_per_second = ( optix_data.samples_since_last_report * 1000 ) / duration_cast<milliseconds>( diff ).count();
			char t[256];
			std::sprintf( t,
			              "MRays/sec: %.2f; Samples Per second: %.2e; Samples per pixel done: %llu",
			              (float)rays_per_second / 1000000.0f,
			              (float)samples_per_second,
			              m_rendered_image->number_of_samples );
			LOG_INFO( t );
			optix_data.generated_ray_count_since_last_report = 0;
			optix_data.samples_since_last_report = 0;
			optix_data.last_reported_ray_count_time = now;
		}
	}

	m_rendered_image->number_of_samples += m_rendered_image->paths_per_pixel_per_pas;
}

void PathtracedImage::renderQuad()
{
	// TODO: Look into doing image blit in the same device
	// https://devtalk.nvidia.com/default/topic/735642/visualize-optix-result-by-opengl-es/?offset=3

	if ( m_rendered_image->width == 0 || m_rendered_image->height == 0 )
	{
		return;
	}

	///////////////////////////////////////////////////////////////////////
	// Upload to gpu texture
	///////////////////////////////////////////////////////////////////////
	if ( m_rendered_image->gl_image_id == 0 )
	{
		glGenTextures( 1, &m_rendered_image->gl_image_id );
	}

	glBindTexture( GL_TEXTURE_2D, m_rendered_image->gl_image_id );

	glTexImage2D( GL_TEXTURE_2D,
	              0,
	              GL_RGB32F,
	              m_rendered_image->width,
	              m_rendered_image->height,
	              0,
	              GL_RGB,
	              GL_FLOAT,
	              m_rendered_image->render_data.data() );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );

	///////////////////////////////////////////////////////////////////////
	// Render quad
	///////////////////////////////////////////////////////////////////////
	orthoview::drawFullScreenQuad();
}

size_t PathtracedImage::getCurrentNumberOfSamples() const
{
	return m_rendered_image->number_of_samples;
}

void PathtracedImage::setMaxBounces( uint32_t bounces )
{
	m_rendered_image->max_bounces = bounces;
}

uint32_t PathtracedImage::getMaxBounces() const
{
	return m_rendered_image->max_bounces;
}

void PathtracedImage::setNumPathsPerPixelPerPass( size_t n_samples )
{
	m_rendered_image->paths_per_pixel_per_pas = n_samples;
}

size_t PathtracedImage::getNumPathsPerPixelPerPass() const
{
	return m_rendered_image->paths_per_pixel_per_pas;
}

void PathtracedImage::setMinRoughness( float roughness )
{
	m_rendered_image->min_roughness = roughness;
}

float PathtracedImage::getMinRoughness() const
{
	return m_rendered_image->min_roughness;
}

void PathtracedImage::setMaxEnvmapValue( float envmap )
{
	m_rendered_image->max_envmap_value = envmap;
}

float PathtracedImage::getMaxEnvmapValue() const
{
	return m_rendered_image->max_envmap_value;
}

void PathtracedImage::unsetVariable( const std::string & name )
{
	optix::Variable var = optix_data.context->queryVariable( name );
	if ( var )
	{
		optix_data.context->removeVariable( var );
	}
}

void PathtracedImage::setVariable( const std::string & name, float v )
{
	optix_data.context[name]->setFloat( v );
}

void PathtracedImage::setVariable( const std::string & name, glm::vec3 v )
{
	optix_data.context[name]->setFloat( v.x, v.y, v.z );
}

void PathtracedImage::setVariable( const std::string & name, uint32_t v )
{
	optix_data.context[name]->setUint( v );
}

template<typename _T>
void setVariable_internal( optix::Context ctx, const std::string& name, const std::vector<_T>& vec, RTformat format )
{
	optix::Buffer buff = ctx->createBuffer( RT_BUFFER_INPUT, format, vec.size() );
	_T* mb = (_T*)buff->map();
	for ( size_t i = 0; i < vec.size(); ++i )
	{
		mb[i] = vec[i];
	}
	buff->unmap();
	ctx[name]->setBuffer( buff );
}

void PathtracedImage::setVariable( const std::string & name, const std::vector<glm::uvec2>& vec )
{
	setVariable_internal( optix_data.context, name, vec, RT_FORMAT_UNSIGNED_INT2 );
}

void PathtracedImage::setVariable( const std::string & name, const std::vector<glm::u16vec2>& vec )
{
	setVariable_internal( optix_data.context, name, vec, RT_FORMAT_UNSIGNED_SHORT2 );
}



void PathtracedImage::setTexture2DVariable( const std::string& name,
                                            uint32_t           texid,
                                            TextureFilter      filter,
                                            bool               normalizeCoords )
{
	if ( texid == 0 )
	{
		optix_data.context[name]->setTextureSampler( optix_data.defaultTextureSampler );
	}
	else
	{
		optix::TextureSampler sampler = optix_data.context->createTextureSamplerFromGLImage( texid, RT_TARGET_GL_TEXTURE_2D );
		switch ( filter )
		{
			case TextureFilter::Linear:
				sampler->setFilteringModes( RT_FILTER_LINEAR, RT_FILTER_LINEAR, RT_FILTER_NONE );
				break;
			case TextureFilter::Nearest:
				sampler->setFilteringModes( RT_FILTER_NEAREST, RT_FILTER_NEAREST, RT_FILTER_NONE );
				break;
		}
		if ( normalizeCoords )
		{
			sampler->setIndexingMode( RTtextureindexmode::RT_TEXTURE_INDEX_NORMALIZED_COORDINATES );
		}
		else
		{
			sampler->setIndexingMode( RTtextureindexmode::RT_TEXTURE_INDEX_ARRAY_INDEX );
		}
		sampler->setReadMode( RTtexturereadmode::RT_TEXTURE_READ_ELEMENT_TYPE );
		sampler->setWrapMode( 0, RTwrapmode::RT_WRAP_CLAMP_TO_EDGE );
		sampler->setWrapMode( 1, RTwrapmode::RT_WRAP_CLAMP_TO_EDGE );
		optix_data.context[name]->setTextureSampler( sampler );
	}
}



///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


static std::string loadPtxFromCudaFile( const char* filename, const std::vector<std::string>& define_flag_names )
{
	std::string ptx;

    std::ifstream ifs(filename);
    std::istreambuf_iterator<char> si(ifs);
	std::string cu( si, {} );

	// Create program
	nvrtcProgram prog = 0;
	nvrtcResult res = nvrtcCreateProgram( &prog, cu.c_str(), filename, 0, NULL, NULL );
	NVRTC_CHECK_ERROR( res );

	// Collect include dirs
	std::vector<std::string> include_dirs;
	const char* abs_dirs[] = { SHADERS_ABSOLUTE_INCLUDE_DIRS };

#ifdef BASE_DIR
	include_dirs.push_back( std::string( "-I" ) + BASE_DIR );
#endif

	const size_t n_abs_dirs = sizeof( abs_dirs ) / sizeof( abs_dirs[0] );
	for ( size_t i = 0; i < n_abs_dirs; i++ )
	{
		include_dirs.push_back( std::string( "-I" ) + abs_dirs[i] );
	}

	std::vector<const char*> options;
	for ( const auto& i : include_dirs )
	{
		options.push_back( i.c_str() );
	}

	std::vector<std::string> define_flags;
	for ( const auto& f : define_flag_names )
	{
		define_flags.push_back( "-D" + f );
		options.push_back( define_flags.back().c_str() );
	}

	const char* compiler_options[] = { CUDA_NVRTC_OPTIONS };
	const size_t n_compiler_options = sizeof( compiler_options ) / sizeof( compiler_options[0] );
	for ( size_t i = 0; i < n_compiler_options - 1; i++ )
	{
		options.push_back( compiler_options[i] );
	}

#ifdef USE_DEBUG_SHADERS
	options.push_back( "-D_DEBUG" );
#endif

	options.push_back("-DUSE_OPTIX");

	// JIT compile CU to PTX
	const nvrtcResult compileRes = nvrtcCompileProgram( prog, (int)options.size(), options.data() );

	// Retrieve log output
	size_t log_size = 0;
	NVRTC_CHECK_ERROR( nvrtcGetProgramLogSize( prog, &log_size ) );
	static std::string log;
	log.resize( log_size );
	if ( log_size > 1 )
	{
		NVRTC_CHECK_ERROR( nvrtcGetProgramLog( prog, &log[0] ) );
	}

	if ( compileRes != NVRTC_SUCCESS )
	{

		DebugError( "NVRTC Compilation failed.\n" + log );
	}
	else if ( strlen( log.c_str() ) > 0 )
	{
		LOG_INFO( log );
	}

	// Retrieve PTX code
	size_t ptx_size = 0;
	NVRTC_CHECK_ERROR( nvrtcGetPTXSize( prog, &ptx_size ) );
	ptx.resize( ptx_size );
	NVRTC_CHECK_ERROR( nvrtcGetPTX( prog, &ptx[0] ) );

	// Cleanup
	NVRTC_CHECK_ERROR( nvrtcDestroyProgram( &prog ) );

	return ptx;
}


///////////////////////////////////////////////////////////////////////////////


static optix::Buffer createBuffer( RTbuffertype type, RTformat format, unsigned width, unsigned height )
{
	optix::Buffer buffer;

	// First allocate the memory for the GL buffer, then attach it to OptiX.

	// Assume ubyte4 or float4 for now
	unsigned int elmt_size = format == RT_FORMAT_UNSIGNED_BYTE4 ? 4 : 16;

	GLuint vbo = 0;
	glGenBuffers( 1, &vbo );
	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	glBufferData( GL_ARRAY_BUFFER, elmt_size * width * height, 0, GL_STREAM_DRAW );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	buffer = optix_data.context->createBufferFromGLBO( type, vbo );
	buffer->setFormat( format );
	buffer->setSize( width, height );

	return buffer;
}


static optix::Buffer createBuffer( RTbuffertype type, RTformat format, unsigned width, unsigned height, unsigned depth )
{
	optix::Buffer buffer;

	// First allocate the memory for the GL buffer, then attach it to OptiX.

	// Assume ubyte4 or float4 for now
	unsigned int elmt_size = 16;
	switch ( format )
	{
		case RT_FORMAT_UNSIGNED_BYTE4:
			elmt_size = 4;
			break;
		case RT_FORMAT_FLOAT:
			elmt_size = 4;
			break;
		default:
			elmt_size = 16;
			break;
	}

	GLuint vbo = 0;
	glGenBuffers( 1, &vbo );
	glBindBuffer( GL_ARRAY_BUFFER, vbo );
	glBufferData( GL_ARRAY_BUFFER, elmt_size * width * height* depth, 0, GL_STREAM_DRAW );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	buffer = optix_data.context->createBufferFromGLBO( type, vbo );
	buffer->setFormat( format );
	buffer->setSize( width, height, depth );

	return buffer;
}

}   // namespace pathtracer
