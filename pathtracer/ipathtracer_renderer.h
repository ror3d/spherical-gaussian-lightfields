#pragma once

#include "renderer/irenderer.h"

namespace pathtracer
{
class PathtracedImage;


class IPathtracingRenderer : public chag::IRenderer
{
public:
	IPathtracingRenderer();
	virtual ~IPathtracingRenderer();

	virtual void init() override;

	virtual void resize( uint16_t width, uint16_t height ) override;

	virtual void destroy() override;

	virtual void render() override;

	void pathTrace();

	virtual void save( const std::string& filename );

	virtual void restart();

	virtual void setNumPathsPerPixelPerPass( size_t paths );

	virtual void setMaxBouncesPerPath( uint32_t bounces );

	virtual void setMaxSamplesPerPixel( size_t samples );

	virtual bool hasFinishedMaxSamplesPerPixel() const;

	virtual void setMinRoughness( float roughness );

	virtual void setMaxEnvmapValue( float envmap );

	inline bool isPathTracing() const { return m_is_pathtracing; }
	inline void startPathTracing() { m_is_pathtracing = true; }
	inline void stopPathTracing() { m_is_pathtracing = false; }

protected:

	virtual void pathTrace_impl() = 0;

	virtual void onSetAsActive() = 0;

	virtual void updateImageSettings();

	virtual void renderGUI();

public:
	void setAsActivePathtracer();
	static IPathtracingRenderer* getActivePathtracer() { return s_activePathtracer; }

protected:
	pathtracer::PathtracedImage* m_pImage = nullptr;

	GLSLProgramObject* m_copy_program = nullptr;

	size_t m_max_samples_per_pixel = 0;

	bool m_image_settings_changed = false;
	struct
	{
		size_t paths_per_pixel_per_pass;
		uint32_t max_bounces_per_path;
		float min_roughness;
		float max_envmap_value;
	} m_image_settings;

private:
	bool m_is_pathtracing = false;

protected:
	static IPathtracingRenderer* s_activePathtracer;

	static GLSLProgramManager* s_glslProgram;
	static size_t s_glslRefs;
	static GLSLProgramManager* s_addGlslRef();
	static void s_decGlslRef();
};

}
