/*
 * Copyright (c) 2018 NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <optix.h>
#include <optixu/optixu_math_namespace.h>

using namespace optix;

rtBuffer<uint3> index_buffer;
rtBuffer<float3> vertex_buffer;
rtBuffer<float3> normal_buffer;
rtBuffer<float3> tangent_buffer;
rtBuffer<float2> texcoord_buffer;


rtDeclareVariable( float3, geometric_normal, attribute geometric_normal, );
rtDeclareVariable( float3, shading_normal, attribute shading_normal, );
rtDeclareVariable( float3, shading_tangent, attribute shading_tangent, );
rtDeclareVariable( float2, texcoord, attribute texcoord, );

rtDeclareVariable( optix::Ray, ray, rtCurrentRay, );


RT_PROGRAM void triangle_mesh_attributes()
{
	const uint3 v_idx = index_buffer[rtGetPrimitiveIndex()];

	const float3 p0 = vertex_buffer[v_idx.x];
	const float3 p1 = vertex_buffer[v_idx.y];
	const float3 p2 = vertex_buffer[v_idx.z];

	const float3 Ng = optix::cross( p1 - p0, p2 - p0 );

	geometric_normal = optix::normalize( Ng );

	float2 bc = rtGetTriangleBarycentrics();

	if ( normal_buffer.size() == 0 )
	{
		shading_normal = geometric_normal;
	}
	else
	{
		float3 n0 = normal_buffer[v_idx.x];
		float3 n1 = normal_buffer[v_idx.y];
		float3 n2 = normal_buffer[v_idx.z];
		shading_normal = normalize( n1 * bc.x + n2 * bc.y + n0 * (1.0f - bc.x - bc.y) );
	}

	if ( tangent_buffer.size() == 0 )
	{
		optix::Onb onb( shading_normal );
		shading_tangent = onb.m_tangent;
	}
	else
	{
		float3 n0 = tangent_buffer[v_idx.x];
		float3 n1 = tangent_buffer[v_idx.y];
		float3 n2 = tangent_buffer[v_idx.z];
		shading_tangent = normalize( n1 * bc.x + n2 * bc.y + n0 * ( 1.0f - bc.x - bc.y ) );
	}

	if ( texcoord_buffer.size() == 0 )
	{
		texcoord = make_float2( bc.x, bc.y );
	}
	else
	{
		float2 t0 = texcoord_buffer[v_idx.x];
		float2 t1 = texcoord_buffer[v_idx.y];
		float2 t2 = texcoord_buffer[v_idx.z];
		float2 uv = t1 * bc.x + t2 * bc.y + t0 * ( 1.0f - bc.x - bc.y );
		texcoord = make_float2( uv.x, uv.y );
	}

}
