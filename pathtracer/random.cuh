#pragma once
#include <cuda_runtime_api.h>

#define USE_PCG 0


////////////////////////////////////////////////////////////////////////////////////////////////////
//
//     PCG
//
////////////////////////////////////////////////////////////////////////////////////////////////////

// *Really* minimal PCG32 code / (c) 2014 M.E. O'Neill / pcg-random.org
// Licensed under Apache License 2.0 (NO WARRANTY, etc. see website)

// clang-format off
#define PCG32_INITIALIZER { 0x853c49e6748fea9bULL, 0xda3e39cb94b95bdbULL }
// clang-format on

typedef struct
{
	unsigned long long state;
	unsigned long long inc;
} pcg32_random_t;

static __host__ __device__ __inline__ unsigned int pcg32_random_r( pcg32_random_t& rng )
{
	unsigned long long oldstate = rng.state;
	// Advance internal state
	rng.state = oldstate * 6364136223846793005ULL + ( rng.inc | 1 );
	// Calculate output function (XSH RR), uses old state for max ILP
	unsigned int xorshifted = ( ( oldstate >> 18u ) ^ oldstate ) >> 27u;
	unsigned int rot = oldstate >> 59u;
	return ( xorshifted >> rot ) | ( xorshifted << ( ( -rot ) & 31 ) );
}

static __host__ __device__ __inline__ float randf_pcg( pcg32_random_t& rng )
{
	return ( pcg32_random_r( rng ) & 0x00ffffff ) / float( 0x01000000 );
}

template<unsigned int N>
static __host__ __device__ __inline__ unsigned long long pcg_tea( unsigned int val0, unsigned int val1 )
{
	unsigned int v0 = val0;
	unsigned int v1 = val1;
	unsigned int s0 = 0;

	for ( unsigned int n = 0; n < N; n++ )
	{
		s0 += 0x9e3779b9;
		v0 += ( ( v1 << 4 ) + 0xa341316c ) ^ ( v1 + s0 ) ^ ( ( v1 >> 5 ) + 0xc8013ea4 );
		v1 += ( ( v0 << 4 ) + 0xad90777d ) ^ ( v0 + s0 ) ^ ( ( v0 >> 5 ) + 0x7e95761e );
	}

	return (((unsigned long long)v0) << 32) | ((unsigned long long)v1);
}



////////////////////////////////////////////////////////////////////////////////////////////////////
//
//     LCG
//
////////////////////////////////////////////////////////////////////////////////////////////////////

template<unsigned int N>
static __host__ __device__ __inline__ unsigned int tea( unsigned int val0, unsigned int val1 )
{
	unsigned int v0 = val0;
	unsigned int v1 = val1;
	unsigned int s0 = 0;

	for ( unsigned int n = 0; n < N; n++ )
	{
		s0 += 0x9e3779b9;
		v0 += ( ( v1 << 4 ) + 0xa341316c ) ^ ( v1 + s0 ) ^ ( ( v1 >> 5 ) + 0xc8013ea4 );
		v1 += ( ( v0 << 4 ) + 0xad90777d ) ^ ( v0 + s0 ) ^ ( ( v0 >> 5 ) + 0x7e95761e );
	}

	return v0;
}

// Generate random unsigned int in [0, 2^24)
static __host__ __device__ __inline__ unsigned int lcg( unsigned int& prev )
{
	const unsigned int LCG_A = 1664525u;
	const unsigned int LCG_C = 1013904223u;
	prev = ( LCG_A * prev + LCG_C );
	return prev & 0x00FFFFFF;
}

static __host__ __device__ __inline__ unsigned int lcg2( unsigned int& prev )
{
	prev = ( prev * 8121 + 28411 ) % 134456;
	return prev;
}

// Generate random float in [0, 1)
static __host__ __device__ __inline__ float randf_lcg( unsigned int& prev )
{
	return ( (float)lcg( prev ) / (float)0x01000000 );
}

static __host__ __device__ __inline__ unsigned int rot_seed( unsigned int seed, unsigned int frame )
{
	return seed ^ frame;
}


#if USE_PCG
typedef pcg32_random_t seed_t;
#define rnd randf_pcg
#else
typedef unsigned int seed_t;
#define rnd randf_lcg
#endif

static __host__ __device__ __inline__ float randf( seed_t& rng )
{
	return rnd( rng );
}


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

static __host__ __device__ __inline__ seed_t initialize_seed(uint2 launch_index, unsigned int frame_number, unsigned int seed)
{
#if USE_PCG
	pcg32_random_t s = PCG32_INITIALIZER;
	unsigned int v0 = tea<16>( launch_index.x, (frame_number + seed + 0x9E3779B9) * 65537 );
	unsigned int v1 = tea<16>( launch_index.y, (frame_number + seed + 0x9E3779B9) * 65537 );
	s.state = pcg_tea<16>( v0 * 31, v1 * 61 + (frame_number + seed + 101) * 97 );
	s.inc = pcg_tea<16>( v0 * 29, v1 * 71 + (frame_number + seed + 137) * 83 );
	return s;
#else
	unsigned int v0 = tea<16>( (launch_index.x + 101) * 97, (frame_number + seed + 0x9E3779B9) * 65537 );
	unsigned int v1 = tea<16>( (launch_index.y + 137) * 83, (frame_number + seed + 0x9E3779B9) * 65537 );
	seed_t s = tea<16>( v0, v1 );
	return s;
#endif
}
