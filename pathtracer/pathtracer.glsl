///////////////////////////////////////////////////////////////////////////////
// Copy texture to screen
///////////////////////////////////////////////////////////////////////////////
shader copy_vert
{
	#version 420 compatibility
	out vec2 texcoord; 
	void main()
	{
		texcoord = gl_Vertex.xy; 
		gl_Position = ftransform();
	}
}

shader copy_frag
{
	#version 420 compatibility
	in vec2 texcoord;
	layout(binding = 0)uniform sampler2D renderbuffer; 
	void main()
	{
		vec4 color = texture(renderbuffer, texcoord);
		gl_FragColor.xyz = color.xyz;
	}
}

///////////////////////////////////////////////////////////////////////////////
// Render position and normals in uv-coordinates
///////////////////////////////////////////////////////////////////////////////
shader uv_world_vert
{
#version 400 compatibility
	layout( location = 0 ) in vec3 position;
	layout( location = 1 ) in vec3 normalIn;
	layout( location = 2 ) in vec2 uvIn;
	layout( location = 3 ) in vec3 tangentIn;
	layout( location = 4 ) in vec2 uvWorldIn;
	out vec3 world_space_position;
	out vec3 world_space_normal;
	out vec3 world_space_tangent;
	out vec2 uvv;

	uniform mat4 modelMatrix;

	void main()
	{
		world_space_position = (modelMatrix * vec4( position, 1.0 )).xyz;
		world_space_normal = (modelMatrix * vec4( normalIn, 0.0 )).xyz;
		world_space_tangent = (modelMatrix * vec4(tangentIn, 0.0)).xyz;
		uvv = vec2( uvIn.x, uvIn.y );

		// For some reason, 3DSMAX outputs translated UVs. Quick fix. 
		vec2 pos = (mod(uvWorldIn, 1.0f) * 2.f) - vec2( 1, 1 );
		gl_Position = vec4(pos, 0, 1);
	}
}

shader uv_world_geom
{
#version 400 compatibility
	layout(triangles) in;
	layout( triangle_strip, max_vertices = 3 ) out;

	in vec3 world_space_position[];
	in vec3 world_space_normal[];
	in vec3 world_space_tangent[];
	in vec2 uvv[];

	out vec3 position;
	out vec3 normal;
	out vec3 tangent;
	out vec3 geom_normal;
	out vec2 uv;

	out vec4 a0;
	out vec4 a1;
	out vec4 a2;

	void main()
	{
		vec3 a = normalize( world_space_position[1] - world_space_position[0] );
		vec3 b = normalize( world_space_position[2] - world_space_position[0] );
		vec3 c = normalize( world_space_position[1] - world_space_position[2] );
		vec3 tnormal = cross( a, b );
		tnormal = normalize( tnormal );
		geom_normal = tnormal;
		a0.xyz = cross( tnormal, a );
		a0.w = -dot( a0.xyz, world_space_position[0] );
		a1.xyz = cross( b, tnormal );
		a1.w = -dot( a1.xyz, world_space_position[0] );
		a2.xyz = cross( c, tnormal );
		a2.w = -dot( a2.xyz, world_space_position[2] );

		gl_Position = gl_in[0].gl_Position;
		position = world_space_position[0];
		normal = world_space_normal[0];
		tangent = world_space_tangent[0];
		uv = uvv[0];
		EmitVertex();

		gl_Position = gl_in[1].gl_Position;
		position = world_space_position[1];
		normal = world_space_normal[1];
		tangent = world_space_tangent[1];
		uv = uvv[1];
		EmitVertex();

		gl_Position = gl_in[2].gl_Position;
		position = world_space_position[2];
		normal = world_space_normal[2];
		tangent = world_space_tangent[2];
		uv = uvv[2];
		EmitVertex();
		EndPrimitive();
	}
}

shader uv_world_frag
{
#version 400 compatibility
	///////////////////////////////////////////////////////////////////////////
	// Vertex shader inputs
	///////////////////////////////////////////////////////////////////////////
	in vec3 position;
	in vec3 normal;
	in vec3 tangent;
	in vec3 geom_normal;
	in vec2 uv;


	in vec4 a0;
	in vec4 a1;
	in vec4 a2;

	///////////////////////////////////////////////////////////////////////////
	// Constants
	///////////////////////////////////////////////////////////////////////////
	const float PI = 3.14159265359;
	///////////////////////////////////////////////////////////////////////////
	// Material uniforms
	///////////////////////////////////////////////////////////////////////////
	uniform float     normal_scale;
	uniform bool      has_normal_texture = false;
	uniform sampler2D normal_texture;

	void main()
	{
		vec4 inner_pos = vec4( position, 1 );
		float d = dot( a0, inner_pos );
		if ( d < 0 )
		{
			inner_pos.xyz -= (d * 1.001) * a0.xyz;
		}
		d = dot( a1, inner_pos );
		if ( d < 0 )
		{
			inner_pos.xyz -= (d * 1.001) * a1.xyz;
		}
		d = dot( a2, inner_pos );
		if ( d < 0 )
		{
			inner_pos.xyz -= (d * 1.001) * a2.xyz;
		}

		vec3 dp = inner_pos.xyz - position;
		vec3 v = dFdx( position );
		vec3 u = dFdy( position );
		vec3 w = cross( v, u );
		mat3 M = mat3( v, u, w );
		mat3 Mi = inverse( M );
		vec3 s = Mi * dp;
		vec3 inner_normal = normal + dFdx( normal ) * s.x + dFdy( normal ) * s.y;
		vec3 inner_tangent = tangent + dFdx( tangent ) * s.x + dFdy( tangent ) * s.y;
		vec2 inner_uv = uv + dFdx( uv ) * s.x + dFdy( uv ) * s.y;
		gl_FragData[0].xyzw = inner_pos;
		///////////////////////////////////////////////////////////////////////
		// Generate (possibly normal-mapped) normal and store in two 
		// components. 
		///////////////////////////////////////////////////////////////////////
		vec3 normal_out = normalize( inner_normal );
		if ( has_normal_texture )
		{
			vec3 normal_map_value = texture2D(normal_texture, inner_uv).xyz;
			normal_map_value = normal_map_value * 2.0 - vec3( 1.0 );
			vec3 bitangent = normalize( cross( inner_normal, inner_tangent ) );
			// The negation on the x axis is scientifically established by looking at 
			// the walls of crytek sponza after using the gimp plugin to generate 
			// normal maps
			normal_out = normalize(
				-normal_map_value.x * normalize( inner_tangent ) +
				normal_map_value.y * bitangent +
				normal_map_value.z * normalize( inner_normal ) );
		}
		// Have to avoid x = exactly n.x = 0.0 or atan2 freaks out. 
		if ( normal_out.x == 0.0 )
		{
			normal_out.x = 1e-5;
		}
		gl_FragData[1].xyz = normal_out.xyz;
		gl_FragData[2].xyz = geom_normal.xyz;
		gl_FragData[3].xyz = dFdx( position );
		gl_FragData[3].w = length( dFdy( position ) );
	}
}

shader merge_conservative_vert
{
#version 420 compatibility
	out vec2 texcoord; 
	void main()
	{
		texcoord = gl_Vertex.xy; 
		gl_Position = ftransform();
	}
}

shader merge_conservative_frag
{
#version 420 compatibility
	in vec2 texcoord;
	layout(binding = 0) uniform sampler2D correct_position;
	layout(binding = 1) uniform sampler2D correct_shading_normal;
	layout(binding = 2) uniform sampler2D correct_geom_normal;
	layout(binding = 3) uniform sampler2D correct_geom_tangent;
	layout(binding = 4) uniform sampler2D conservative_position;
	layout(binding = 5) uniform sampler2D conservative_shading_normal;
	layout(binding = 6) uniform sampler2D conservative_geom_normal;
	layout(binding = 7) uniform sampler2D conservative_geom_tangent;
	void main()
	{
		vec4 position = texture(correct_position, texcoord);

		vec4 c_position = texture(conservative_position, texcoord);

		if ( position.w == 0.f && c_position.w == 1.f )
		{
			float dx = dFdx( texcoord ).x;
			float dy = dFdy( texcoord ).y;

			// Check if the surrounding texels in the correct position are also empty
			bool empty = true;
			empty = empty && texture(correct_position, vec2( texcoord.x - dx, texcoord.y )).w == 0.f;
			empty = empty && texture(correct_position, vec2( texcoord.x + dx, texcoord.y )).w == 0.f;
			empty = empty && texture(correct_position, vec2( texcoord.x, texcoord.y - dy )).w == 0.f;
			empty = empty && texture(correct_position, vec2( texcoord.x, texcoord.y + dy )).w == 0.f;

			if ( empty )
			{
				gl_FragData[0] = c_position;
				gl_FragData[1] = texture(conservative_shading_normal, texcoord);
				gl_FragData[2] = texture(conservative_geom_normal, texcoord);
				gl_FragData[3] = texture(conservative_geom_tangent, texcoord);
				return;
			}
		}
		vec4 normal = texture(correct_shading_normal, texcoord);
		vec4 gnormal = texture(correct_geom_normal, texcoord);
		vec4 gtangent = texture(correct_geom_tangent, texcoord);

		gl_FragData[0] = position;
		gl_FragData[1] = normal;
		gl_FragData[2] = gnormal;
		gl_FragData[3] = gtangent;
	}
}
