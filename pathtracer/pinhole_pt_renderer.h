#pragma once

#include "ipathtracer_renderer.h"

namespace pathtracer
{

class PinholePathtracerRenderer : public IPathtracingRenderer
{
public:
	PinholePathtracerRenderer() : IPathtracingRenderer() {}

	virtual void updateScene( scene::Scene * ) override;

	virtual void updateView() override;

protected:
	virtual void pathTrace_impl() override;

	virtual void onSetAsActive() override;

private:
	void updateCamera();

private:
	struct
	{
		glm::vec3 pos;
		glm::vec3 lower_right_corner;
		glm::vec3 X;
		glm::vec3 Y;
	} camera;
};

}
