#include "pinhole_pt_renderer.h"
#include <CHAGApp/imgui/imgui.h>

#ifdef USE_PATHTRACER
#include "pathtracer/PathTracer.h"
#endif

namespace pathtracer
{

void PinholePathtracerRenderer::onSetAsActive()
{
#ifdef USE_PATHTRACER
	m_pImage->initialize( "../pathtracer/pinhole_pathtracer.cu", "pathtrace_pinhole_camera", "pinhole_radiance_closest_hit", "pinhole_shadow_any_hit", "pinhole_miss", "pinhole_exception" );

	// Declare variables
	m_pImage->setVariable( "camera_position", camera.pos );
	m_pImage->setVariable( "camera_LR", camera.lower_right_corner );
	m_pImage->setVariable( "camera_X", camera.X );
	m_pImage->setVariable( "camera_Y", camera.Y );
#endif
}

void PinholePathtracerRenderer::updateScene( scene::Scene * scene )
{
	IRenderer::updateScene( scene );
#ifdef USE_PATHTRACER
	// Setup Camera
	updateCamera();

	m_pImage->restart();
#endif
}

void PinholePathtracerRenderer::updateView()
{
#ifdef USE_PATHTRACER
	updateCamera();

	m_pImage->restart();
#endif
}

void PinholePathtracerRenderer::pathTrace_impl()
{
	if ( hasFinishedMaxSamplesPerPixel() )
	{
		return;
	}
#ifdef USE_PATHTRACER
	updateImageSettings();

	// Copy camera parameters to program
	m_pImage->setVariable( "camera_position", camera.pos );
	m_pImage->setVariable( "camera_LR", camera.lower_right_corner );
	m_pImage->setVariable( "camera_X", camera.X );
	m_pImage->setVariable( "camera_Y", camera.Y );

	// Trace
	m_pImage->tracePaths();
#endif
}

void PinholePathtracerRenderer::updateCamera()
{
	using namespace glm;
	// Update camera
	view& camTransform = *(m_scene->getCurrentRenderableItem()->getView());
	vec3  dir = -camTransform.R[2];
	vec3  up = camTransform.R[1];
	vec3  right = camTransform.R[0];
	float fov = camTransform.m_fov;
	float aspect = camTransform.m_aspect_ratio;

	glm::vec3 A = dir * cosf( fov / 2.0f * (glm::pi<float>() / 180.0f) );
	glm::vec3 B = up * sinf( fov / 2.0f * (glm::pi<float>() / 180.0f) );
	glm::vec3 C = right * sinf( fov / 2.0f * (glm::pi<float>() / 180.0f) ) * aspect;

	camera.pos = camTransform.pos;
	camera.lower_right_corner = A - C - B;
	camera.X = 2.0f * ((A - B) - camera.lower_right_corner);
	camera.Y = 2.0f * ((A - C) - camera.lower_right_corner);
}

}
