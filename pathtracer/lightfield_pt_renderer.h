#pragma once

#include "lightmap_pt_renderer.h"

#include <list>

namespace pathtracer
{

class LightfieldPathtracerRenderer : public LightmapPathtracerRenderer
{
public:
	LightfieldPathtracerRenderer() : LightmapPathtracerRenderer() {}

	virtual void destroy() override;

	// This does nothing, we will be resizing in the more appropiate functions
	virtual void resize( uint16_t width, uint16_t height ) override {}

	// This resizes the texel grid of the lightmap from which the lightfields are generated
	void setBaseLightmapSize( uint16_t width, uint16_t height );

	// This resizes the individual lightfield textures. Note that they are output in a packed grid.
	void setLightfieldSize( uint16_t size );

	// This sets the size of the grid for the output lightfield textures. Each cell of the grid will be an entire lightfield image
	void setGridSize( uint16_t size );

	void setFilePath( const std::string& path, const std::string& prefix );

	void continueFromPartialFile( const std::string& filepath );

	void setMaxJitterRoughness( float roughness );

	void setMaxJitterPosition( float mult );

	void setTreatEnvironmentAsAlpha( bool easa );

	void setContinueAfterFirstPass( bool cont );

#ifndef NO_SCENE
	virtual void updateScene( scene::Scene* scene ) override;
#endif

	virtual void render() override;

protected:
	virtual void pathTrace_impl() override;

	virtual void renderGUI() override;

	virtual void updateImageSettings() override;

	void startPage();
	uint32_t outputCurrentLightfield( const std::string& filename, size_t texel_count, uint16_t n_channels, uint32_t samples_added, bool continuing );
	void outputRawMetadata();
	void outputCorrectedMetadata();
	void initializeBatchesToProcess();
	void loadBatchesToProcess( const std::string& filename );

protected:
	virtual void onSetAsActive() override {}
	virtual void initializePathtracer_internal() override;

	bool m_need_update_scene_name = false;

	bool m_new_pathtrace_popup_open = false;
	bool m_continue_pathtrace_popup_open = false;

	std::vector<uint32_t> m_island_ids;
	std::vector<glm::u16vec2> m_valid_coords;

	struct LightfieldBatch
	{
		uint32_t page_idx;
		uint64_t first_texel_coords_idx;
		uint64_t texel_count;
		uint32_t island_id;
		char filename[64];
		bool continuing = false;
	};

	std::list<LightfieldBatch> m_batches_to_process;

	std::string m_path;
	std::string m_file_prefix;

	struct
	{
		bool baseLightmapSizeSet = false;
		bool lightfieldfieldSizeSet = false;
	} m_asserts;

	struct
	{
		glm::u16vec2 base_lightmap_size;
		uint16_t lightfield_size = 128;
		uint16_t grid_size = 16;
		float max_jitter_roughness = 0.f;
		float max_jitter_position = 0.f;
		bool environment_as_alpha = false;
		bool continue_after_first_pass = true;
		bool render_single_lightfield = false;
		glm::u16vec2 single_texel_coords;
		bool save_to_file = true;
	} m_lightfield_settings_current, m_lightfield_settings_next;
};

}
