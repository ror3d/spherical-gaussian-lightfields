#pragma once

#include <vector>
#include <string>
#include <cstring>
#include <glm/glm.hpp>

namespace scene
{
class Scene;
}

namespace pathtracer
{
///////////////////////////////////////////////////////////////////////////
// Initial setup of the pathtracer
///////////////////////////////////////////////////////////////////////////
void initialize();

///////////////////////////////////////////////////////////////////////////
// Release of the pathtracer's resources
///////////////////////////////////////////////////////////////////////////
void finalize();

///////////////////////////////////////////////////////////////////////////
// Set the scene object and reload its gpu representation
///////////////////////////////////////////////////////////////////////////
void setScene( scene::Scene* scene );

///////////////////////////////////////////////////////////////////////////
// Update the lights from the current scene object
///////////////////////////////////////////////////////////////////////////
void updateLights();


class PathtracedImage
{
public:
	PathtracedImage();
	~PathtracedImage();

	void initialize( const char* cu_file,
	                 const char* ray_gen_pg,
	                 const char* closest_hit_pg,
	                 const char* any_hit_pg,
	                 const char* miss_pg,
	                 const char* exception_pg,
					 const std::vector<std::string>& define_flags = {} );

	///////////////////////////////////////////////////////////////////////////
	// On window resize, window size is passed in, actual size of pathtraced
	// image may be smaller (if we're subsampling for speed)
	///////////////////////////////////////////////////////////////////////////
	void resize( int w, int h );
	glm::ivec2 getSize() const;


	void setNumChannels( uint8_t n_channels );
	uint8_t getNumChannels() const;

	void setRenderChannels( glm::uvec3 channels );

	// Get the pathtraced data. It's an array that for each the H rows contains W columns, and each of these columns
	// contains C channels
	const float * getData() const;

	///////////////////////////////////////////////////////////////////////////
	// Restart rendering of image
	///////////////////////////////////////////////////////////////////////////
	void restart();

	///////////////////////////////////////////////////////////////////////////
	// Trace the paths for each pixel
	///////////////////////////////////////////////////////////////////////////
	void tracePaths();

	///////////////////////////////////////////////////////////////////////////
	// Apply the generated texture to be rendered
	///////////////////////////////////////////////////////////////////////////
	void renderQuad();

	///////////////////////////////////////////////////////////////////////////
	// Set/Get algorithm properties and status
	///////////////////////////////////////////////////////////////////////////
	size_t getCurrentNumberOfSamples() const;
	void setMaxBounces( uint32_t bounces );
	uint32_t getMaxBounces() const;
	void setNumPathsPerPixelPerPass( size_t n_samples );

	size_t getNumPathsPerPixelPerPass() const;

	void setMinRoughness( float roughness );
	float getMinRoughness() const;

	void setMaxEnvmapValue( float envmap );
	float getMaxEnvmapValue() const;


	///////////////////////////////////////////////////////////////////////////
	// Set variable values in the gpu pathtracer program
	///////////////////////////////////////////////////////////////////////////
	void unsetVariable( const std::string& name );
	void setVariable( const std::string& name, float v );
	void setVariable( const std::string& name, glm::vec3 v );
	void setVariable( const std::string& name, uint32_t v );
	void setVariable( const std::string& name, const std::vector<glm::u16vec2>& vec);
	void setVariable( const std::string& name, const std::vector<glm::uvec2>& vec);

	enum class TextureFilter
	{
		Nearest,
		Linear
	};
	void setTexture2DVariable( const std::string& name,
							   uint32_t texid,
							   TextureFilter filter = TextureFilter::Linear,
							   bool normalizeCoords = true );

protected:
	struct Image;
	Image* m_rendered_image;
};

};   // namespace pathtracer
