#include "pathtracer_pbr.cuh"


rtTextureSampler<float4, 2> input_position_buffer;
rtTextureSampler<float4, 2> input_normal_buffer;

///////////////////////////////////////////////////////////////////////////////
//
//  Hemisphere program -- main ray tracing loop
//
///////////////////////////////////////////////////////////////////////////////

RT_PROGRAM void pathtrace_lightmap()
{
	PBR_PerRayData prd;
	prd.seed = initialize_seed( launch_index, frame_number, rnd_seed );

	float3 result = make_float3( 0.f );
	uint ray_count = 0;

	float4 pos_tex_sample = tex2D( input_position_buffer, launch_index.x + 0.5f, launch_index.y + 0.5f);
	if ( pos_tex_sample.w == 0.f )
	{
		return;
	}

	float3 texel_position = make_float3( pos_tex_sample );
	float3 texel_normal = make_float3( tex2D( input_normal_buffer, launch_index.x, launch_index.y ) );

	for ( uint path = 0; path < max_paths; ++path )
	{
		float  z1 = randf( prd.seed );
		float  z2 = randf( prd.seed );
		float3 ray_origin = texel_position + texel_normal * EPSILON;
		float3 ray_direction;
		cosine_sample_hemisphere( z1, z2, ray_direction );
		optix::Onb onb( texel_normal );
		onb.inverse_transform( ray_direction );
		ray_direction = normalize( ray_direction );

		pbrPathInit( prd );

		for ( uint bounce = 0; bounce < max_bounces; ++bounce )
		{
			bool is_finished = false;

			result += pbrTraceBounce( bounce, prd, ray_origin, ray_direction, ray_count, is_finished );

			if ( is_finished )
			{
				break;
			}
		}
	}

	generated_ray_count[launch_index] = ray_count;
	result /= float( max_paths );
	setOutputBufferValue( launch_index, result.x, result.y, result.z );
}


rtDeclareVariable( PBR_PerRayData, lightmap_radiance_prd, rtPayload, );

RT_PROGRAM void lightmap_radiance_closest_hit()
{
	onPbrHit( lightmap_radiance_prd );
}


rtDeclareVariable( PBR_Shadow_PerRayData, lightmap_shadow_prd, rtPayload, );

RT_PROGRAM void lightmap_shadow_any_hit()
{
	onPbrShadowHit( lightmap_shadow_prd );
	rtTerminateRay();
}


//-----------------------------------------------------------------------------
//  Miss program
//-----------------------------------------------------------------------------

RT_PROGRAM void lightmap_miss()
{
	onPbrMiss( lightmap_radiance_prd, lightmap_shadow_prd );
}


//-----------------------------------------------------------------------------
//  Exception program
//-----------------------------------------------------------------------------

RT_PROGRAM void lightmap_exception()
{
	setOutputBufferValue( launch_index, 1e12, 0.f, 1e12 );
}
