#include "pathtracer_pbr.cuh"
#include <optixu_matrix.h>

struct lightmap_preprocess_PerRayData
{
	float3 hit_position;
	float3 hit_normal;
	int   hit_backface;
};


rtTextureSampler<float4, 2> input_position_buffer;
rtTextureSampler<float4, 2> input_normal_buffer;
rtTextureSampler<float4, 2> input_tangent_buffer;


///////////////////////////////////////////////////////////////////////////////
//
//
///////////////////////////////////////////////////////////////////////////////


bool intersect_backface_and_move( float3 texel_position, float3 texel_normal, float3 direction, float len, float3& offset )
{
	float3 origin = texel_position + texel_normal * EPSILON;
	lightmap_preprocess_PerRayData prd;
	prd.hit_backface = false;
	Ray ray = make_Ray( origin, direction, closest_hit_ray_type_id, 0.f, len );
	rtTrace( top_scene_node, ray, prd );

	if ( !prd.hit_backface )
	{
		return false;
	}

	// The 3 planes defined by the following n1*xyz-c1=0, n2*xyz-c2=0, n3*xyz-c3=0 intersect on a point.
	// We will use that point as the new texel position.
	float3 n1 = texel_normal;
	float c1 = dot( n1, texel_position );
	float3 n2 = prd.hit_normal;
	float c2 = dot( n2, prd.hit_position );
	float3 n3 = cross( n1, n2 );
	float c3 = dot( n3, texel_position );

	// N * x = c -> x = N^-1 * c
	Matrix4x4 N;
	N.setRow( 0, make_float4( n1, 0 ) );
	N.setRow( 1, make_float4( n2, 0 ) );
	N.setRow( 2, make_float4( n3, 0 ) );
	N.setRow( 3, make_float4( 0, 0, 0, 1 ) );
	Matrix4x4 Ni = N.inverse();
	float3 xyz = make_float3( Ni * make_float4(c1, c2, c3, 0) );

	// Get the offset from the texel point to this new point, adjusting with epsilon to get past the plane
	float3 new_off = xyz - texel_position;
	float new_off_len = length( new_off );
	new_off *= (new_off_len + EPSILON) / new_off_len;
	new_off_len += EPSILON;

	float off_len = length( offset );

	if ( off_len == 0 || new_off_len < off_len )
	{
		offset = new_off;
	}

	return true;
}

RT_PROGRAM void preprocess_lightmap()
{
	generated_ray_count[launch_index] = 0;

	float4 pos_tex_sample = tex2D( input_position_buffer, launch_index.x, launch_index.y );
	if ( pos_tex_sample.w == 0.f )
	{
		setOutputBufferValue( launch_index, pos_tex_sample.x, pos_tex_sample.y, pos_tex_sample.z, pos_tex_sample.w );
		return;
	}
	float4 tan_tex_sample = tex2D( input_tangent_buffer, launch_index.x, launch_index.y );

	float3 texel_position = make_float3( pos_tex_sample );
	float3 texel_normal = make_float3( tex2D( input_normal_buffer, launch_index.x, launch_index.y ) );

	float3 texel_tangent = make_float3( tan_tex_sample );
	float2 texel_size = make_float2( length( texel_tangent ), tan_tex_sample.w );
	if ( texel_size.x == 0 || texel_size.y == 0 )
	{
		setOutputBufferValue( launch_index, pos_tex_sample.x, pos_tex_sample.y, pos_tex_sample.z, pos_tex_sample.w );
		return;
	}
	texel_tangent /= texel_size.x;

	float3 texel_bitangent = normalize( cross( texel_normal, texel_tangent ) );

	float3 texel_cotangent = texel_tangent * texel_size.x + texel_bitangent * texel_size.y;
	float3 texel_cobitangent = texel_tangent * texel_size.x - texel_bitangent * texel_size.y;
	float2 texel_cosize = make_float2( length( texel_cotangent ), length( texel_cobitangent ) );
	texel_cotangent /= texel_cosize.x;
	texel_cobitangent /= texel_cosize.y;

	float3 offset = make_float3( 0 );

	intersect_backface_and_move( texel_position, texel_normal, texel_tangent, texel_size.x, offset );
	intersect_backface_and_move( texel_position, texel_normal, texel_cotangent, texel_cosize.x, offset );
	intersect_backface_and_move( texel_position, texel_normal, texel_bitangent, texel_size.y, offset );
	intersect_backface_and_move( texel_position, texel_normal, texel_cobitangent, texel_cosize.y, offset );
	intersect_backface_and_move( texel_position, texel_normal, -texel_tangent, texel_size.x, offset );
	intersect_backface_and_move( texel_position, texel_normal, -texel_cotangent, texel_cosize.x, offset );
	intersect_backface_and_move( texel_position, texel_normal, -texel_bitangent, texel_size.y, offset );
	intersect_backface_and_move( texel_position, texel_normal, -texel_cobitangent, texel_cosize.y, offset );

	float3 position = texel_position + offset;
	setOutputBufferValue( launch_index, position.x, position.y, position.z, pos_tex_sample.w );
}


rtDeclareVariable( lightmap_preprocess_PerRayData, lightmap_preprocess_prd, rtPayload, );

RT_PROGRAM void lightmap_preprocess_closest_hit()
{
	float3 world_geometric_normal = normalize( rtTransformNormal( RT_OBJECT_TO_WORLD, geometric_normal ) );

	lightmap_preprocess_prd.hit_normal = world_geometric_normal;
	lightmap_preprocess_prd.hit_position = ray.origin + ray_intersect_distance * ray.direction;

	lightmap_preprocess_prd.hit_backface = dot( ray.direction, world_geometric_normal ) > 0;
}


RT_PROGRAM void lightmap_preprocess_any_hit()
{
	rtTerminateRay();
}


//-----------------------------------------------------------------------------
//  Miss program
//-----------------------------------------------------------------------------

RT_PROGRAM void lightmap_preprocess_miss()
{
}


//-----------------------------------------------------------------------------
//  Exception program
//-----------------------------------------------------------------------------

RT_PROGRAM void lightmap_preprocess_exception()
{
	float4 pos_tex_sample = tex2D( input_position_buffer, launch_index.x, launch_index.y );
	setOutputBufferValue( launch_index, pos_tex_sample.x, pos_tex_sample.y, pos_tex_sample.z, pos_tex_sample.w );
}
