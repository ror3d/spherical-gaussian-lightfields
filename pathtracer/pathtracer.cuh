#pragma once

#include <optix.h>
#include <optix_device.h>
#include <optixu/optixu_math_namespace.h>
#include <cuda_runtime_api.h>

#include "random.cuh"

#define USE_MIS_ENVMAP 1

#ifndef M_PI
#define M_PI 3.14159265358979323846f
#endif

#ifndef EPSILON
#define EPSILON 0.0001f
#endif

//#ifdef _DEBUG
#if 0
#define DebugAssert( x, ... )        \
	do                               \
	{                                \
		if ( !( x ) )            \
		{                            \
			rtPrintf( __VA_ARGS__ ); \
		}                            \
	} while ( 0 )
#define DebugNaN(x, ...) DebugAssert( !isnan(x), __VA_ARGS__ )
#else
#define DebugAssert( ... )
#define DebugNaN( ... )
#endif

using namespace optix;



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



rtDeclareVariable( uint2, launch_index, rtLaunchIndex, );

rtDeclareVariable( unsigned int, frame_number, , );
rtDeclareVariable( unsigned int, rnd_seed, , );

rtBuffer<float, 3> output_buffer;
rtBuffer<uint, 2> generated_ray_count;



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



__device__ __inline__  size_t2 getScreenSize()
{
	size_t3 size = output_buffer.size();
	return make_size_t2( size.x, size.y );
}

__device__ __inline__ float& outputBufferValue( uint2 position, uint channel )
{
	DebugAssert( output_buffer.size().z > channel,
	             "Trying to output on channel %d when there's only %d channels in the buffer!\n",
	             channel,
	             output_buffer.size().z );
	return output_buffer[make_uint3( position.x, position.y, channel )];
}

__device__ __inline__ void setOutputBufferValue( uint2 position, float v0, float v1=0, float v2=0, float v3=0, float v4=0, float v5=0, float v6=0, float v7=0, float v8=0, float v9=0 )
{
	switch ( output_buffer.size().z )
	{
		case 10:
			outputBufferValue( position, 9 ) = v9;
		case 9:
			outputBufferValue( position, 8 ) = v8;
		case 8:
			outputBufferValue( position, 7 ) = v7;
		case 7:
			outputBufferValue( position, 6 ) = v6;
		case 6:
			outputBufferValue( position, 5 ) = v5;
		case 5:
			outputBufferValue( position, 4 ) = v4;
		case 4:
			outputBufferValue( position, 3 ) = v3;
		case 3:
			outputBufferValue( position, 2 ) = v2;
		case 2:
			outputBufferValue( position, 1 ) = v1;
		case 1:
			outputBufferValue( position, 0 ) = v0;
			break;
		default:
			rtPrintf("Unsupported output buffer size.\n");
	}
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


float3 perpendicular(const float3& a) {
	float3 nv = make_float3(fabs(a.x), fabs(a.y), fabs(a.z));
	if (nv.x < nv.y)
		if (nv.x < nv.z)
			return make_float3(0.0f, -a.z, a.y);
		else
			return make_float3(-a.y, a.x, 0.0f);
	else if (nv.y < nv.z)
		return make_float3(-a.z, 0.0f, a.x);
	else
		return make_float3(-a.y, a.x, 0.0f);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




rtDeclareVariable( optix::Ray, ray, rtCurrentRay, );
rtDeclareVariable( float, ray_intersect_distance, rtIntersectionDistance, );



