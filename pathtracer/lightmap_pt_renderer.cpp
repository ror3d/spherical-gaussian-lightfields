#include "lightmap_pt_renderer.h"
#include "CHAGApp/PerformanceProfiler.h"
#include <utils/opengl_helpers.h>
#include "PathTracer.h"
#include "CHAGApp/ProgressListenerDialog.h"
#include <CHAGApp/imgui/imgui.h>

//#define ENABLE_CONSERVATIVE_RASTERISATION

namespace pathtracer
{
void LightmapPathtracerRenderer::init()
{
	m_gbuffer_program = m_program_manager->getProgramObject(
	        "uv_world_vert", "uv_world_geom", "uv_world_frag", GL_TRIANGLES, GL_TRIANGLE_STRIP, 3 );
#ifdef ENABLE_CONSERVATIVE_RASTERISATION
	m_merge_conservative_program = m_program_manager->getProgramObject( "merge_conservative_vert", "merge_conservative_frag" );
#endif
	IPathtracingRenderer::init();
}

void LightmapPathtracerRenderer::onSetAsActive()
{
	initializePathtracer_internal();
}

void LightmapPathtracerRenderer::renderLightmapBaseTextures()
{
#ifdef USE_PATHTRACER
	// Generate GBuffer
	glPushAttrib( GL_ALL_ATTRIB_BITS );

	glClampColor( GL_CLAMP_READ_COLOR, GL_FALSE );
	glClampColor( GL_CLAMP_VERTEX_COLOR, GL_FALSE );
	glClampColor( GL_CLAMP_FRAGMENT_COLOR, GL_FALSE );


	// Use conservative rasterization for triangles that are too small to be rendered otherwise, but avoid
	// using them when they would be extending an existing triangle, since we do that oursalves later by expanding
	// the pixels
#ifdef ENABLE_CONSERVATIVE_RASTERISATION
#ifdef GL_CONSERVATIVE_RASTERIZATION_NV
	uint32_t conser_position_texture = opengl_helpers::framebuffer::createTexture( m_base_width, m_base_height, GL_RGBA32F );
	uint32_t conser_shading_normal_texture = opengl_helpers::framebuffer::createTexture( m_base_width, m_base_height, GL_RGBA32F );
	uint32_t conser_geom_normal_texture = opengl_helpers::framebuffer::createTexture( m_base_width, m_base_height, GL_RGBA32F );
	uint32_t conser_geom_tangent_texture = opengl_helpers::framebuffer::createTexture( m_base_width, m_base_height, GL_RGBA32F );
	{
		uint32_t gbuffer = opengl_helpers::framebuffer::createFramebuffer(
		        m_base_width,
		        m_base_height,
		        0,
		        { conser_position_texture, conser_shading_normal_texture, conser_geom_normal_texture, conser_geom_tangent_texture } );

		opengl_helpers::framebuffer::pushFramebuffer( gbuffer );
		glDisable( GL_SCISSOR_TEST );
		glViewport( 0, 0, m_base_width, m_base_height );
		glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
		glClear( GL_COLOR_BUFFER_BIT );
		m_gbuffer_program->enable();

		{
			glEnable( GL_CONSERVATIVE_RASTERIZATION_NV );
			// glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
			// glLineWidth( 3 );
			m_scene->submitModels( m_gbuffer_program );

			glDisable( GL_CONSERVATIVE_RASTERIZATION_NV );
		}

		m_gbuffer_program->disable();
		opengl_helpers::framebuffer::popFramebuffer();


		opengl_helpers::framebuffer::deleteFramebuffer( gbuffer, false );
	}

	// Change the m_ textures for the temp ones
	uint32_t old_position_texture = m_position_texture;
	m_position_texture = opengl_helpers::framebuffer::createTexture( m_base_width, m_base_height, GL_RGBA32F );
	uint32_t old_shading_normal_texture = m_shading_normal_texture;
	m_shading_normal_texture = opengl_helpers::framebuffer::createTexture( m_base_width, m_base_height, GL_RGBA32F );
	uint32_t old_geom_normal_texture = m_geom_normal_texture;
	m_geom_normal_texture = opengl_helpers::framebuffer::createTexture( m_base_width, m_base_height, GL_RGBA32F );
	uint32_t old_geom_tangent_texture = m_geom_tangent_texture;
	m_geom_tangent_texture = opengl_helpers::framebuffer::createTexture( m_base_width, m_base_height, GL_RGBA32F );
#endif
	// glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
#endif

	m_gbuffer = opengl_helpers::framebuffer::createFramebuffer(
	        m_base_width,
	        m_base_height,
	        0,
	        { m_position_texture, m_shading_normal_texture, m_geom_normal_texture, m_geom_tangent_texture } );

	opengl_helpers::framebuffer::pushFramebuffer( m_gbuffer );
	glDisable( GL_SCISSOR_TEST );
	glViewport( 0, 0, m_base_width, m_base_height );
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	glClear( GL_COLOR_BUFFER_BIT );
	m_gbuffer_program->enable();

	m_scene->submitModels( m_gbuffer_program );

	m_gbuffer_program->disable();
	opengl_helpers::framebuffer::popFramebuffer();


	opengl_helpers::framebuffer::deleteFramebuffer( m_gbuffer, false );
	m_gbuffer = 0;

#ifdef ENABLE_CONSERVATIVE_RASTERISATION
	{
		uint32_t gbuffer = opengl_helpers::framebuffer::createFramebuffer(
				m_base_width,
				m_base_height,
				0,
				{ old_position_texture, old_shading_normal_texture, old_geom_normal_texture, old_geom_tangent_texture } );

		opengl_helpers::framebuffer::pushFramebuffer( gbuffer );
		glViewport( 0, 0, m_base_width, m_base_height );
		glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
		glClear( GL_COLOR_BUFFER_BIT );
		glDisable( GL_SCISSOR_TEST );

		m_merge_conservative_program->enable();

		m_merge_conservative_program->bindTexture( "correct_position", m_position_texture, GL_TEXTURE_2D, 0 );
		m_merge_conservative_program->bindTexture( "correct_shading_normal", m_shading_normal_texture, GL_TEXTURE_2D, 1 );
		m_merge_conservative_program->bindTexture( "correct_geom_normal", m_geom_normal_texture, GL_TEXTURE_2D, 2 );
		m_merge_conservative_program->bindTexture( "correct_geom_tangent", m_geom_tangent_texture, GL_TEXTURE_2D, 3 );
		m_merge_conservative_program->bindTexture( "conservative_position", conser_position_texture, GL_TEXTURE_2D, 4 );
		m_merge_conservative_program->bindTexture( "conservative_shading_normal", conser_shading_normal_texture, GL_TEXTURE_2D, 5 );
		m_merge_conservative_program->bindTexture( "conservative_geom_normal", conser_geom_normal_texture, GL_TEXTURE_2D, 6 );
		m_merge_conservative_program->bindTexture( "conservative_geom_tangent", conser_geom_tangent_texture, GL_TEXTURE_2D, 7 );
		orthoview::drawFullScreenQuad();

		m_merge_conservative_program->disable();
		
		opengl_helpers::framebuffer::popFramebuffer();

		opengl_helpers::texture::deleteTexture( m_position_texture );
		opengl_helpers::texture::deleteTexture( m_shading_normal_texture );
		opengl_helpers::texture::deleteTexture( m_geom_normal_texture );
		opengl_helpers::texture::deleteTexture( m_geom_tangent_texture );

		opengl_helpers::texture::deleteTexture( conser_position_texture );
		opengl_helpers::texture::deleteTexture( conser_shading_normal_texture );
		opengl_helpers::texture::deleteTexture( conser_geom_normal_texture );
		opengl_helpers::texture::deleteTexture( conser_geom_tangent_texture );

		m_position_texture = old_position_texture;
		m_shading_normal_texture = old_shading_normal_texture;
		m_geom_normal_texture = old_geom_normal_texture;
		m_geom_tangent_texture = old_geom_tangent_texture;	

		opengl_helpers::framebuffer::deleteFramebuffer( gbuffer, false );
	}
#endif

	glPopAttrib();

#endif
}

void LightmapPathtracerRenderer::resize( uint16_t width, uint16_t height )
{
	m_base_width = width;
	m_base_height = height;

	// Create the GBuffer textures
	{
		if ( m_position_texture != 0 )
		{
			opengl_helpers::texture::deleteTexture( m_position_texture );
		}
		if ( m_shading_normal_texture != 0 )
		{
			opengl_helpers::texture::deleteTexture( m_shading_normal_texture );
		}
		if ( m_geom_normal_texture != 0 )
		{
			opengl_helpers::texture::deleteTexture( m_geom_normal_texture );
		}
		if ( m_geom_tangent_texture != 0 )
		{
			opengl_helpers::texture::deleteTexture( m_geom_tangent_texture );
		}
		m_position_texture = opengl_helpers::framebuffer::createTexture( width, height, GL_RGBA32F );         // position ; is_valid
		m_shading_normal_texture = opengl_helpers::framebuffer::createTexture( width, height, GL_RGBA32F );   // normal
		m_geom_normal_texture = opengl_helpers::framebuffer::createTexture( width, height, GL_RGBA32F );      // normal
		m_geom_tangent_texture = opengl_helpers::framebuffer::createTexture( width, height, GL_RGBA32F );   // tangent, with length
		                                                                                                // equal to texel width;
		                                                                                                // bitangent length,
		                                                                                                // corresponding to
		                                                                                                // texel height
	}

	m_positionsDirty = true;
}

void LightmapPathtracerRenderer::updateScene( scene::Scene * scene )
{

	IRenderer::updateScene( scene );
}

void LightmapPathtracerRenderer::pathTrace_impl()
{
	if ( hasFinishedMaxSamplesPerPixel() )
	{
		return;
	}
#ifdef USE_PATHTRACER
	updateImageSettings();

	if ( m_positionsDirty )
	{
		g_progress->push_task( "Generating base textures for Lightmap" );
		{
			renderLightmapBaseTextures();
			updatePositionsTexture();
			initializePathtracer_internal();
			restart();
		}
		g_progress->pop_task();
		m_positionsDirty = false;
	}
	m_pImage->tracePaths();
#endif
}

void LightmapPathtracerRenderer::destroy()
{
	if ( m_gbuffer != 0 )
	{
		opengl_helpers::framebuffer::deleteFramebuffer( m_gbuffer );
		m_gbuffer = 0;
	}

	IPathtracingRenderer::destroy();
}

void LightmapPathtracerRenderer::render()
{
#if 0 // To debug the helper textures
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable( GL_DEPTH_TEST );

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	m_copy_program->enable();
	glBindTexture( GL_TEXTURE_2D, m_geom_normal_texture );

	orthoview::drawFullScreenQuad();
	m_copy_program->disable();

	glPopAttrib();

		renderGUI();

#else
	IPathtracingRenderer::render();
#endif
}

void LightmapPathtracerRenderer::renderGUI()
{
	ImGui::Begin( "Pathtrace Lightmap" );
	{
		if ( isPathTracing() )
		{
			if ( ImGui::Button( "Stop Baking" ) )
			{
				stopPathTracing();
			}
		}
		else
		{
			if ( ImGui::Button( "Bake Lightmap" ) )
			{
				startPathTracing();
			}
		}
		ImGui::SameLine();
		if ( ImGui::Button( "Restart Baking" ) )
		{
			m_pImage->restart();
		}

		if ( m_pImage->getCurrentNumberOfSamples() > 0 )
		{
			if ( ImGui::Button( "Save Lightmap As..." ) )
			{
				std::string default_name = m_scene->m_scene_name + "_lightmap";
				wxFileDialog openFileDialog( m_parentWindow, _( "Save Lightmap" ), "", default_name, "hdr files (*.hdr)|*.hdr|png files (*.png)|*.png", wxFD_SAVE | wxFD_OVERWRITE_PROMPT );
				if ( openFileDialog.ShowModal() == wxID_OK )
				{
					std::string filename = std::string( openFileDialog.GetPath().mb_str( wxConvUTF8 ) );
					LOG_VERBOSE( "Saving as " << filename << " ..." );
					save( filename );
				}
			}
		}
	}
	ImGui::End();
}

void LightmapPathtracerRenderer::updatePositionsTexture()
{
#ifdef USE_PATHTRACER
	m_pImage->resize( m_base_width, m_base_height );

	m_pImage->initialize( "../pathtracer/lightmap_preprocess_pathtracer.cu", "preprocess_lightmap", "lightmap_preprocess_closest_hit", "lightmap_preprocess_any_hit", "lightmap_preprocess_miss", "lightmap_preprocess_exception" );

	// Declare variables
	m_pImage->setTexture2DVariable( "input_position_buffer",
									m_position_texture,
									pathtracer::PathtracedImage::TextureFilter::Nearest,
									false );
	m_pImage->setTexture2DVariable( "input_normal_buffer",
									m_geom_normal_texture,
									pathtracer::PathtracedImage::TextureFilter::Nearest,
									false );
	m_pImage->setTexture2DVariable( "input_tangent_buffer",
									m_geom_tangent_texture,
									pathtracer::PathtracedImage::TextureFilter::Nearest,
									false );

	m_pImage->restart();

	m_pImage->tracePaths();


	auto sz = m_pImage->getSize();

	std::vector<glm::vec4> pos_tex( sz.x * sz.y );
	std::vector<glm::vec4> sh_norm_tex( sz.x * sz.y );
	std::vector<glm::vec4> geo_norm_tex( sz.x * sz.y );
	std::vector<glm::vec4> geo_tan_tex( sz.x * sz.y );

	glBindTexture( GL_TEXTURE_2D, m_position_texture );
	glGetTexImage( GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, (float*)pos_tex.data() );
	glBindTexture( GL_TEXTURE_2D, m_shading_normal_texture );
	glGetTexImage( GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, (float*)sh_norm_tex.data() );
	glBindTexture( GL_TEXTURE_2D, m_geom_normal_texture );
	glGetTexImage( GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, (float*)geo_norm_tex.data() );
	glBindTexture( GL_TEXTURE_2D, m_geom_tangent_texture );
	glGetTexImage( GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, (float*)geo_tan_tex.data() );


	const glm::vec4* data = (glm::vec4*)m_pImage->getData();
	for ( size_t i = 0; i < pos_tex.size(); ++i )
	{
		pos_tex[i] = glm::vec4( glm::vec3( data[i] ), pos_tex[i].w );
	}

	// Dilate the texels to avoid black lines
	{
		for ( size_t j = 0; j < sz.y; ++j )
		{
			for ( size_t i = 0; i < sz.x; ++i )
			{
				size_t a = i + sz.x * j;
				if ( pos_tex[a].w == 0 )
				{
					auto copy_from = [&]( int dx, int dy ) -> bool
					{
						if ( dx < 0 && i == 0 ) return false;
						if ( dy < 0 && j == 0 ) return false;
						if ( dx > 0 && i == sz.x - 1 ) return false;
						if ( dy > 0 && j == sz.y - 1 ) return false;
						size_t b = (i + dx) + sz.x * (j + dy);
						if ( pos_tex[b].w != 1 )
						{
							return false;
						}
						pos_tex[a] = pos_tex[b];
						pos_tex[a].w = 0.5f;
						sh_norm_tex[a] = sh_norm_tex[b];
						geo_norm_tex[a] = geo_norm_tex[b];
						geo_tan_tex[a] = geo_tan_tex[b];
						return true;
					};

					if ( copy_from( -1, 0 ) ) {}
					else if ( copy_from( 0, -1 ) ) {}
					else if ( copy_from( 1, 0 ) ) {}
					else if ( copy_from( 0, 1 ) ) {}
					else if ( copy_from( -1, -1 ) ) {}
					else if ( copy_from( 1, -1 ) ) {}
					else if ( copy_from( 1, 1 ) ) {}
					else if ( copy_from( -1, 1 ) ) {}
				}
			}
		}
		for ( size_t j = 0; j < sz.y; ++j )
		{
			for ( size_t i = 0; i < sz.x; ++i )
			{
				size_t a = i + sz.x * j;
				if ( pos_tex[a].w > 0 && pos_tex[a].w < 1 )
				{
					pos_tex[a].w = 1;
				}
			}
		}
		{
			opengl_helpers::texture::deleteTexture( m_shading_normal_texture );
			m_shading_normal_texture = opengl_helpers::framebuffer::createTexture( m_base_width, m_base_height, GL_RGBA32F );     // normal
			glBindTexture( GL_TEXTURE_2D, m_shading_normal_texture );
			glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA32F, sz.x, sz.y, 0, GL_RGBA, GL_FLOAT, (float*)sh_norm_tex.data() );
		}
		{
			opengl_helpers::texture::deleteTexture( m_geom_normal_texture );
			m_geom_normal_texture = opengl_helpers::framebuffer::createTexture( m_base_width, m_base_height, GL_RGBA32F );     // normal
			glBindTexture( GL_TEXTURE_2D, m_geom_normal_texture );
			glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA32F, sz.x, sz.y, 0, GL_RGBA, GL_FLOAT, (float*)geo_norm_tex.data() );
		}
		{
			opengl_helpers::texture::deleteTexture( m_geom_tangent_texture );
			m_geom_tangent_texture = opengl_helpers::framebuffer::createTexture( m_base_width, m_base_height, GL_RGBA32F );   // tangent, with length equal
			glBindTexture( GL_TEXTURE_2D, m_geom_tangent_texture );
			glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA32F, sz.x, sz.y, 0, GL_RGBA, GL_FLOAT, (float*)geo_tan_tex.data() );
		}
	}

	// Create corrected texture from the pixels passed through cuda
	if ( m_corrected_position_texture != 0 )
	{
		opengl_helpers::texture::deleteTexture( m_corrected_position_texture );
	}

	m_corrected_position_texture = opengl_helpers::framebuffer::createTexture( sz.x, sz.y, GL_RGBA32F );   // position ; is_valid
	glBindTexture( GL_TEXTURE_2D, m_corrected_position_texture );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, sz.x, sz.y, 0, GL_RGBA, GL_FLOAT, (float*)pos_tex.data());

	glBindTexture( GL_TEXTURE_2D, 0 );
#endif
}

void LightmapPathtracerRenderer::initializePathtracer_internal()
{
#ifdef USE_PATHTRACER
	m_pImage->initialize( "../pathtracer/lightmap_pathtracer.cu",
	                      "pathtrace_lightmap",
	                      "lightmap_radiance_closest_hit",
	                      "lightmap_shadow_any_hit",
	                      "lightmap_miss",
	                      "lightmap_exception" );

	// Declare variables
	m_pImage->setTexture2DVariable( "input_position_buffer",
									m_corrected_position_texture,
									pathtracer::PathtracedImage::TextureFilter::Nearest,
									false );
	m_pImage->setTexture2DVariable( "input_normal_buffer",
									m_geom_normal_texture,
									pathtracer::PathtracedImage::TextureFilter::Nearest,
									false );
#endif
}

}
