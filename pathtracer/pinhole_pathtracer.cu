#include "pathtracer_pbr.cuh"

rtDeclareVariable( float3, camera_position, , );
rtDeclareVariable( float3, camera_LR, , );
rtDeclareVariable( float3, camera_X, , );
rtDeclareVariable( float3, camera_Y, , );

///////////////////////////////////////////////////////////////////////////////
//
//  Camera program -- main ray tracing loop
//
///////////////////////////////////////////////////////////////////////////////

RT_PROGRAM void pathtrace_pinhole_camera()
{
	PBR_PerRayData prd;
	prd.seed = initialize_seed( launch_index, frame_number, rnd_seed );

	size_t2 screen = getScreenSize();

	float3 result = make_float3( 0.f );
	uint ray_count = 0;

	for ( uint path = 0; path < max_paths; ++path )
	{
		float2 jitter = make_float2( randf( prd.seed ), randf( prd.seed ) ) - make_float2( 0.5f, 0.5f );
		float2 d = ( make_float2( launch_index ) + jitter ) / make_float2( screen );

		float3 ray_origin = camera_position;
		float3 ray_direction = normalize( d.x * camera_X + d.y * camera_Y + camera_LR );

		pbrPathInit( prd );

		for ( uint bounce = 0; bounce < max_bounces; ++bounce )
		{
			bool is_finished = false;

			result += pbrTraceBounce( bounce, prd, ray_origin, ray_direction, ray_count, is_finished );

			if ( is_finished )
			{
				break;
			}
		}
	}

	generated_ray_count[launch_index] = ray_count;
	float4 output = make_float4( result / float( max_paths ), 1.f );
	setOutputBufferValue( launch_index, output.x, output.y, output.z, output.w );
}


//-----------------------------------------------------------------------------
//  Light Hit program
//-----------------------------------------------------------------------------

rtDeclareVariable( PBR_PerRayData, pinhole_radiance_prd, rtPayload, );

RT_PROGRAM void pinhole_radiance_closest_hit()
{
	onPbrHit( pinhole_radiance_prd );
}


//-----------------------------------------------------------------------------
//  Shadow Hit program
//-----------------------------------------------------------------------------

rtDeclareVariable( PBR_Shadow_PerRayData, pinhole_shadow_prd, rtPayload, );

RT_PROGRAM void pinhole_shadow_any_hit()
{
	onPbrShadowHit( pinhole_shadow_prd );
	rtTerminateRay();
}


//-----------------------------------------------------------------------------
//  Miss program
//-----------------------------------------------------------------------------

RT_PROGRAM void pinhole_miss()
{
	onPbrMiss( pinhole_radiance_prd, pinhole_shadow_prd );
}


//-----------------------------------------------------------------------------
//  Exception program
//-----------------------------------------------------------------------------

RT_PROGRAM void pinhole_exception()
{
	setOutputBufferValue( launch_index,  1.f, 0.f, 1.f, 1.f );
}
