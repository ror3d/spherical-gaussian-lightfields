#include "pathtracer_pbr.cuh"
#include <optixu_matrix_namespace.h>
#include "../include/spherical_projection_common.h"

rtDeclareVariable( unsigned int, lightfield_tex_size, , );
rtDeclareVariable( unsigned int, lightfield_grid_size, , );
rtDeclareVariable( unsigned int, lightfield_grid_page_start, , );

rtTextureSampler<float4, 2> input_position_buffer;
rtTextureSampler<float4, 2> input_normal_buffer;
rtTextureSampler<float4, 2> input_tangent_buffer;
rtBuffer<ushort2> valid_coords_buffer;

rtDeclareVariable( float, lightfield_max_jitter_roughness, , );
rtDeclareVariable( float, lightfield_max_jitter_position, , );

#ifndef __CUDACC__
#define RENDER_OUTGOING_ENVIRONMENT_WITH_VISIBILITY
//#define RENDER_ONLY_ENVIRONMENT_VISIBILITY
#endif

#if defined(RENDER_OUTGOING_ENVIRONMENT_WITH_VISIBILITY)
#define OUTPUT_BLANK() setOutputBufferValue( launch_index, 0, 0, 0, 0, 0, 0, 0, 0, 0 );
#define OUTPUT_RED() setOutputBufferValue( launch_index, 1, 0, 0, 0, 0, 0, 0, 0, 0 );
#define OUTPUT_BLUE() setOutputBufferValue( launch_index, 0, 0, 1, 0, 0, 0, 0, 0, 0 );
#else
#ifndef RENDER_ONLY_ENVIRONMENT_VISIBILITY
#define OUTPUT_BLANK() setOutputBufferValue( launch_index, 0, 0, 0, 1 );
#define OUTPUT_RED() setOutputBufferValue( launch_index, 1, 0, 0, 1 );
#define OUTPUT_BLUE() setOutputBufferValue( launch_index, 0, 0, 1, 1 );
#else
#define OUTPUT_BLANK() setOutputBufferValue( launch_index, 0, 0, 0, 0, 0, 0 );
#define OUTPUT_RED() setOutputBufferValue( launch_index, 1, 0, 0, 0, 0, 0 );
#define OUTPUT_BLUE() setOutputBufferValue( launch_index, 0, 0, 1, 0, 0, 0 );
#endif
#endif

///////////////////////////////////////////////////////////////////////////////
//
//  Hemisphere program -- main ray tracing loop
//
///////////////////////////////////////////////////////////////////////////////

RT_PROGRAM void pathtrace_lightfield()
{
	PBR_PerRayData prd;
	prd.seed = initialize_seed( launch_index, frame_number, rnd_seed );

	float3 result = make_float3( 0. );
#ifdef RENDER_OUTGOING_ENVIRONMENT_WITH_VISIBILITY
	float3 environment_occluded_result = make_float3( 0 );
	float3 environment_result = make_float3( 0 );
#endif
	uint ray_count = 0;

	// Get the position and direction for this thread
	uint2 grid_idx = make_uint2( launch_index.x / lightfield_tex_size, launch_index.y / lightfield_tex_size );
	uint2 lightfield_idx = launch_index - grid_idx * lightfield_tex_size;

	unsigned int valid_texel_idx = grid_idx.x + grid_idx.y * lightfield_grid_size + lightfield_grid_page_start;

	if ( valid_texel_idx >= valid_coords_buffer.size() )
	{
		OUTPUT_BLUE();
		return;
	}

	ushort2 valid_coords = valid_coords_buffer[valid_texel_idx];

	float4 pos_tex_sample = tex2D( input_position_buffer, valid_coords.x, valid_coords.y );

	if ( pos_tex_sample.w == 0.f )
	{
		OUTPUT_RED();
		return;
	}

	float3 texel_position = make_float3( 0 );
	float3 texel_normal = make_float3( 0, 0, 1 );

	texel_position = make_float3( pos_tex_sample );
	texel_normal = make_float3( tex2D( input_normal_buffer, valid_coords.x, valid_coords.y ) );


	// Get the tangent and bitangent to jitter the position
	float4 tan_tex_sample = tex2D( input_tangent_buffer, valid_coords.x, valid_coords.y );

	float3 texel_tangent = make_float3( tan_tex_sample );
	float2 texel_size = make_float2( length( texel_tangent ), tan_tex_sample.w );

	float3 texel_bitangent = normalize( cross( texel_normal, texel_tangent / texel_size.x ) );
	texel_bitangent *= texel_size.y;

	/*
	if ( frame_number == 10 && launch_index.x == 0 && launch_index.y == 0 )
	{
		rtPrintf( "coords: %u, %u\n", valid_coords.x, valid_coords.y );
		rtPrintf( "pos: %f, %f, %f\n", texel_position.x, texel_position.y, texel_position.z );
		rtPrintf( "nm: %f, %f, %f\n", texel_normal.x, texel_normal.y, texel_normal.z );
		rtPrintf( "tg: %f, %f, %f\n", texel_tangent.x, texel_tangent.y, texel_tangent.z );
		rtPrintf( "btg: %f, %f, %f\n", texel_bitangent.x, texel_bitangent.y, texel_bitangent.z );
	}
	*/

	float3 ray_base_origin = texel_position + texel_normal * EPSILON;

	float theta, phi;
	GetThetaPhiFromTexCoords( (lightfield_idx.x + 0.5f) / float(lightfield_tex_size),
						  (lightfield_idx.y + 0.5f) / float(lightfield_tex_size),
						  theta,
						  phi );
	float3 ray_base_direction = make_float3( sin( theta )*cos( phi ), sin( theta )*sin( phi ), cos( theta ) );


	if ( dot( ray_base_direction, texel_normal ) <= 0 )
	{
		OUTPUT_BLANK();
		return;
	}

	Onb ray_base_direction_base( ray_base_direction );

	uint valid_paths = 0;
	uint valid_pre_paths = 0;

#ifdef RENDER_OUTGOING_ENVIRONMENT_WITH_VISIBILITY
	// Move position so that the hit point from the first ray is at our current position
	float bias = EPSILON;// / dot( ray_base_direction, texel_normal );
	bias = fminf( bias, 100 * EPSILON );

	// Make direction incoming instead of outgoing
	float3 ray_base_pre_direction = -ray_base_direction;

	float3 ray_base_pre_origin = texel_position - ray_base_pre_direction * bias;

	float ray_pre_max_distance = length( texel_tangent + texel_bitangent );
#endif

	for ( uint path = 0; path < max_paths; ++path )
	{
		float3 position_jitter = ( texel_tangent * ( randf( prd.seed ) - 0.5f ) + texel_bitangent * ( randf( prd.seed ) - 0.5f ) )
								 * lightfield_max_jitter_position;

		// Check we are not going past a face
		{
			// We only jitter the position if doing so doesn't cross any face in the geometry. This is to avoid going past
			// walls that would get contributions from outside the geometry, for example.
			// If it does, we simply not jitter that ray's position.
			PBR_Shadow_PerRayData shadow_prd;
			shadow_prd.in_shadow = false;
			Ray ray = make_Ray( ray_base_origin, normalize( position_jitter ), any_hit_ray_type_id, 0.f, length( position_jitter ) );
			ray_count += 1;
			rtTrace( top_scene_node, ray, shadow_prd );
			
			if ( shadow_prd.in_shadow )
			{
				position_jitter = make_float3( 0 );
			}
		}

#ifdef RENDER_OUTGOING_ENVIRONMENT_WITH_VISIBILITY
		///////////////////////////////////////////////////////////////////////////
		// Irradiance from environment map
		{
			float3 ray_pre_origin = ray_base_origin;
			float3 ray_pre_direction = ray_base_direction;

			pbrPathInit( prd );
			prd.ray_type = 1; // Only GGX

			//bool is_finished = false;
			bool valid = false;

			ray_pre_origin = ray_base_pre_origin;
			ray_pre_direction = ray_base_pre_direction;

			// Jitter original position with texel tangent
			ray_pre_origin += position_jitter;

			{
				Ray ray = make_Ray( ray_pre_origin, ray_pre_direction, closest_hit_ray_type_id, 0.f, RT_DEFAULT_MAX );
				prd.ray_type = 100;
				ray_count += 1;
				rtTrace( top_scene_node, ray, prd );
				
				if ( prd.hit )
				{
					if ( length( prd.origin - ray_pre_origin ) < ray_pre_max_distance )
					{
						ray_pre_direction = -prd.direction;
						ray_pre_origin = prd.origin - EPSILON * ray_pre_direction;
						valid = true;
					}
				}
			}
			pbrPathInit( prd );
			prd.ray_type = 1; // Only GGX


			// Trace the ray that will hit the surface first
			{
				float3 old_atten = make_float3( 1.f );
				float old_pdf = 1.f;

				_pbrBounceInit( prd, old_atten, old_pdf );

				ray_count += 1;
				Ray ray = make_Ray( ray_pre_origin, ray_pre_direction, closest_hit_ray_type_id, 0.f, RT_DEFAULT_MAX );
				rtTrace( top_scene_node, ray, prd );
				if ( prd.hit )
				{
					ray_pre_origin = prd.origin;
					ray_pre_direction = prd.direction;
				}
				if ( prd.next_ray_pdf == 0.f )
				{
					valid = false;
				}
			}
			//pbrTraceBounce( 0, prd, ray_pre_origin, ray_pre_direction, ray_count, is_finished );

			float3 environment_sample = sampleEnvironment( environmentCoords( ray_pre_direction ) );


			if ( valid && prd.hit && dot( ray_pre_direction, texel_normal ) > 0 )
			{
				valid = true;
				valid_pre_paths += 1;

				//environment_sample *= prd.attenuation;
				environment_sample *= prd.attenuation / prd.next_ray_pdf;
				environment_result += environment_sample;
			}
			else
			{
				valid = false;
			}

			// Trace first ray, but we treat it as a shadow because we only want to add the result if it hit something
			{
				PBR_Shadow_PerRayData pbr_shadow_prd;
				pbr_shadow_prd.in_shadow = false;

				Ray shadow_ray = make_Ray( ray_pre_origin, ray_pre_direction, any_hit_ray_type_id, 0.f, RT_DEFAULT_MAX );
				ray_count += 1;
				rtTrace( top_shadowing_node, shadow_ray, pbr_shadow_prd );

				if ( valid && !pbr_shadow_prd.in_shadow )
				{
					environment_occluded_result += environment_sample;
				}
			}
		}
#endif

		float3 ray_origin = ray_base_origin;
		float3 ray_direction = ray_base_direction;

		///////////////////////////////////////////////////////////////////////////
		// Irradiance from reflections

		// Add jitter to position with texel tangent
		ray_origin += position_jitter;

		// Add jitter to direction by importance sampling a GGX BRDF
		if ( lightfield_max_jitter_roughness > 0.f )
		{
			float u1 = randf( prd.seed );
			float u2 = randf( prd.seed );
			float r = sqrtf( u1 / (1 - u1) );
			float _phi = 6.28318530718 * u2;
			float2 slope = r * make_float2( cos( _phi ), sin( _phi ) );
			slope *= lightfield_max_jitter_roughness * lightfield_max_jitter_roughness;
			float3 sdir = normalize( make_float3( -slope.x, -slope.y, 1.0f ) );

			float3 dir_tangent = normalize( perpendicular( ray_base_direction ) );
			float3 dir_bitangent = normalize( cross( ray_base_direction, dir_tangent ) );
			ray_direction = sdir.x * dir_tangent + sdir.y * dir_bitangent + sdir.z * ray_base_direction;
		}

		if ( dot( ray_direction, texel_normal ) < 0 )
		{
			continue;
		}

		valid_paths += 1;

#ifndef RENDER_ONLY_ENVIRONMENT_VISIBILITY
		pbrPathInit( prd );

		bool is_finished = false;
#ifdef RENDER_OUTGOING_ENVIRONMENT_WITH_VISIBILITY
		float3 temp_result = pbrTraceBounce( 0, prd, ray_origin, ray_direction, ray_count, is_finished );

		if ( prd.hit )
		{
			result += temp_result;

			for ( uint bounce = 1; (!is_finished) && (bounce < max_bounces); ++bounce )
			{
				result += pbrTraceBounce( bounce, prd, ray_origin, ray_direction, ray_count, is_finished );
			}
		}
#else
		for ( uint bounce = 0; !is_finished && bounce < max_bounces; ++bounce )
		{
			result += pbrTraceBounce( bounce, prd, ray_origin, ray_direction, ray_count, is_finished );
		}
#endif
#else
		{
			PBR_Shadow_PerRayData pbr_shadow_prd;
			pbr_shadow_prd.in_shadow = false;

			ray_count += 1;
			Ray shadow_ray = make_Ray( ray_origin, ray_direction, any_hit_ray_type_id, 0.f, RT_DEFAULT_MAX );
			rtTrace( top_shadowing_node, shadow_ray, pbr_shadow_prd );
			if ( !pbr_shadow_prd.in_shadow )
			{
				result += make_float3( 1, 1, 1 );
			}
		}
#endif
	}

	// Average only for all valid paths
	if ( valid_paths > 0 )
	{
		result /= float( valid_paths );
	}

#ifdef RENDER_OUTGOING_ENVIRONMENT_WITH_VISIBILITY
	if ( valid_pre_paths > 0 )
	{
		environment_result /= float( valid_pre_paths );
		environment_occluded_result /= float( valid_pre_paths );
	}
#endif

	generated_ray_count[launch_index] = ray_count;
#ifdef RENDER_OUTGOING_ENVIRONMENT_WITH_VISIBILITY
	setOutputBufferValue( launch_index,
	                      result.x,
	                      result.y,
	                      result.z,
	                      environment_occluded_result.x,
	                      environment_occluded_result.y,
	                      environment_occluded_result.z,
	                      environment_result.x,
	                      environment_result.y,
	                      environment_result.z );
#else
#ifndef RENDER_ONLY_ENVIRONMENT_VISIBILITY
	setOutputBufferValue( launch_index, result.x, result.y, result.z, 1.f );
#else
	setOutputBufferValue( launch_index, 0.f, 0.f, 0.f, result.x, result.y, result.z );
#endif
#endif
}


rtDeclareVariable( PBR_PerRayData, lightfield_radiance_prd, rtPayload, );

RT_PROGRAM void lightfield_radiance_closest_hit()
{
	if ( lightfield_radiance_prd.ray_type == 100 )
	{
		float3 world_shading_normal;
		float3 world_shading_tangent;
		float3 world_geometric_normal;
		float3 hit_point;
		float3 world_wo;
		getHitVectors( world_shading_normal, world_shading_tangent, world_geometric_normal, hit_point, world_wo );

		lightfield_radiance_prd.hit = true;
		lightfield_radiance_prd.origin = hit_point;
		lightfield_radiance_prd.direction = -reflect( world_wo, world_shading_normal );
		return;
	}
	onPbrHit( lightfield_radiance_prd );
}


rtDeclareVariable( PBR_Shadow_PerRayData, lightfield_shadow_prd, rtPayload, );

RT_PROGRAM void lightfield_shadow_any_hit()
{
	onPbrShadowHit( lightfield_shadow_prd );
	rtTerminateRay();
}


//-----------------------------------------------------------------------------
//  Miss program
//-----------------------------------------------------------------------------

RT_PROGRAM void lightfield_miss()
{
	onPbrMiss( lightfield_radiance_prd, lightfield_shadow_prd );
}


//-----------------------------------------------------------------------------
//  Exception program
//-----------------------------------------------------------------------------

RT_PROGRAM void lightfield_exception()
{
	setOutputBufferValue( launch_index, 1e12, 0.f, 1e12 );
}
