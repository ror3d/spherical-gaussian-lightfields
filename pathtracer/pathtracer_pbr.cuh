
#include "pathtracer.cuh"


struct PBR_PerRayData
{
	seed_t seed;
	float3 radiance;
	float3 light_radiance;
	float3 light_position;
#if USE_MIS_ENVMAP
	float3 env_radiance;
	float3 env_direction;
#endif
	float3 attenuation;
	float3 origin;
	float3 direction;
	float next_ray_pdf;
	bool hit;
	// Ray type: 0 = standard ray, 1 = GGX_ONLY
	int ray_type;
	bool done;
};

struct PBR_Shadow_PerRayData
{
	bool in_shadow;
};


void onPbrHit( PBR_PerRayData& prd );

void onPbrShadowHit( PBR_Shadow_PerRayData& shadow_prd );

void onPbrMiss( PBR_PerRayData& prd, PBR_Shadow_PerRayData& shadow_prd );

void pbrPathInit( PBR_PerRayData& prd );

float3 pbrTraceBounce( uint bounce, PBR_PerRayData& prd, float3& origin, float3& direction, uint& ray_count, bool& finished );


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Scene wide variables
rtDeclareVariable( rtObject, top_scene_node, , );
rtDeclareVariable( rtObject, top_shadowing_node, , );
rtDeclareVariable( unsigned int, max_bounces, , );
rtDeclareVariable( unsigned int, max_paths, , );

rtDeclareVariable( unsigned int, closest_hit_ray_type_id, , );
rtDeclareVariable( unsigned int, any_hit_ray_type_id, , );

rtDeclareVariable( float3, environment_color, , );
rtDeclareVariable( float, environment_multiplier, , );
rtTextureSampler<float4, 2> environment_map;

rtDeclareVariable( float, min_roughness, , );
rtDeclareVariable( float, max_envmap_value, , );


rtBuffer<float, 1> envmap_marginal_pdf;
rtBuffer<float, 1> envmap_marginal_cdf;
rtBuffer<float, 2> envmap_rows_pdf;
rtBuffer<float, 2> envmap_rows_cdf;

rtDeclareVariable( int, num_lights, , );
rtBuffer<float3, 1> lights_position;
rtBuffer<float, 1> lights_intensity;
rtBuffer<float3, 1> lights_color;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// direction to environment map coordinates
__device__ __inline__ float2 environmentCoords( const float3& dir )
{
	const float theta = acosf( fmaxf( -1.0f, fminf( 1.0f, dir.y ) ) );
	float phi = atan2f( dir.z, dir.x );
	if ( phi < 0.0f )
		phi = phi + 2.0f * M_PI;
	return make_float2( phi / ( 2.0 * M_PI ), theta / M_PI );
}

__device__ __inline__ float3 sampleEnvironment( const float2& uv )
{
	const float4 t = tex2D( environment_map, uv.x, uv.y );
	if ( max_envmap_value > 0 )
	{
		return fminf( make_float3( max_envmap_value ), environment_multiplier * make_float3( t.x, t.y, t.z ) * environment_color );
	}
	return environment_multiplier * make_float3( t.x, t.y, t.z ) * environment_color;
}

__device__ __inline__ float3 sphericalDirection( float sinTheta, float cosTheta, float phi )
{
	return make_float3( sinTheta * cos( phi ), sinTheta * sin( phi ), cosTheta );
}

__device__ __inline__ bool sameHemisphere( const float3& w, const float3& wp )
{
	return dot( w, wp ) > 0;
}

///////////////////////////////////////////////////////////////////////////////
// BSDF Inline Functions. Stolen from PBRT
///////////////////////////////////////////////////////////////////////////////
inline float cosTheta( const float3& w )
{
	return w.z;
}
inline float cos2Theta( const float3& w )
{
	return w.z * w.z;
}
inline float absCosTheta( const float3& w )
{
	return abs( w.z );
}
inline float sin2Theta( const float3& w )
{
	return fmaxf( 0, w.x * w.x + w.y * w.y );
}
inline float sinTheta( const float3& w )
{
	return sqrtf( sin2Theta( w ) );
}
inline float tanTheta( const float3& w )
{
	return sinTheta( w ) / cosTheta( w );
}
inline float tan2Theta( const float3& w )
{
	return sin2Theta( w ) / cos2Theta( w );
}
inline float cosPhi( const float3& w )
{
	float sinTh = sinTheta( w );
	return ( sinTh == 0 ) ? 1 : clamp( w.x / sinTh, -1.f, 1.f );
}
inline float sinPhi( const float3& w )
{
	float sinTh = sinTheta( w );
	return ( sinTh == 0 ) ? 0 : clamp( w.y / sinTh, -1.f, 1.f );
}
inline float cos2Phi( const float3& w )
{
	return cosPhi( w ) * cosPhi( w );
}
inline float sin2Phi( const float3& w )
{
	return sinPhi( w ) * sinPhi( w );
}
inline float cosDPhi( const float3& wa, const float3& wb )
{
	return clamp(
	        ( wa.x * wb.x + wa.y * wb.y ) / sqrtf( ( wa.x * wa.x + wa.y * wa.y ) * ( wb.x * wb.x + wb.y * wb.y ) ), -1.f, 1.f );
}

///////////////////////////////////////////////////////////////////////////////
// Tangent space trans. Stolen from PBRT
///////////////////////////////////////////////////////////////////////////////
inline float3 from_tangent_space( const float3& world_shading_normal, const float3& world_shading_tangent, const float3& v )
{
	float3 world_shading_bitangent = normalize( cross( world_shading_tangent, world_shading_normal ) );
	return make_float3( world_shading_tangent.x * v.x + world_shading_bitangent.x * v.y + world_shading_normal.x * v.z,
	                    world_shading_tangent.y * v.x + world_shading_bitangent.y * v.y + world_shading_normal.y * v.z,
	                    world_shading_tangent.z * v.x + world_shading_bitangent.z * v.y + world_shading_normal.z * v.z );
}

inline float3 to_tangent_space( const float3& world_shading_normal, const float3& world_shading_tangent, const float3& v )
{
	float3 world_shading_bitangent = normalize( cross( world_shading_tangent, world_shading_normal ) );
	return make_float3( world_shading_tangent.x * v.x + world_shading_tangent.y * v.y + world_shading_tangent.z * v.z,
	                    world_shading_bitangent.x * v.x + world_shading_bitangent.y * v.y + world_shading_bitangent.z * v.z,
	                    world_shading_normal.x * v.x + world_shading_normal.y * v.y + world_shading_normal.z * v.z );
}


rtDeclareVariable( float3, geometric_normal, attribute geometric_normal, );
rtDeclareVariable( float3, shading_normal, attribute shading_normal, );
rtDeclareVariable( float3, shading_tangent, attribute shading_tangent, );
rtDeclareVariable( float2, texcoord, attribute texcoord, );

rtDeclareVariable( float4, material_base_color_factor, , );
rtDeclareVariable( float, material_normal_scale, , );
rtDeclareVariable( float, material_alpha_cutoff, , );
rtDeclareVariable( float, material_metallic_factor, , );
rtDeclareVariable( float, material_roughness_factor, , );
rtDeclareVariable( float3, material_emissive_factor, , );

rtDeclareVariable( int, material_has_normal_texture, , );

rtTextureSampler<float4, 2> material_base_color_texture;
rtTextureSampler<float4, 2> material_normal_texture;
rtTextureSampler<float4, 2> material_emissive_texture;
rtTextureSampler<float4, 2> material_metal_roughness_texture;


void getHitVectors( float3& world_shading_normal,
                    float3& world_shading_tangent,
                    float3& world_geometric_normal,
                    float3& hit_point,
                    float3& world_wo )
{
	hit_point = ray.origin + ray_intersect_distance * ray.direction;
	world_wo = normalize( -ray.direction );
	world_shading_normal = normalize( rtTransformNormal( RT_OBJECT_TO_WORLD, shading_normal ) );
	world_geometric_normal = normalize( rtTransformNormal( RT_OBJECT_TO_WORLD, geometric_normal ) );
	world_shading_tangent = normalize( rtTransformNormal( RT_OBJECT_TO_WORLD, shading_tangent ) );
	///////////////////////////////////////////////////////////////////////////
	// Get the nmapped normal and make it face the right way
	///////////////////////////////////////////////////////////////////////////
	if ( material_has_normal_texture )
	{
		float3 nmap = make_float3( tex2D( material_normal_texture, texcoord.x, texcoord.y ) );
		nmap = nmap * 2.0 - make_float3( 1.0 );
		float3 bitg = normalize( cross( world_shading_normal, world_shading_tangent ) );
		world_shading_normal = normalize( -nmap.x * world_shading_tangent + nmap.y * bitg + nmap.z * world_shading_normal );
	}
	world_shading_normal = faceforward( world_shading_normal, -ray.direction, world_geometric_normal );
	///////////////////////////////////////////////////////////////////////////
	// clamp the shading normal to be slightly less than orthogonal to view ray
	///////////////////////////////////////////////////////////////////////////
	float dist = dot( world_wo, world_shading_normal );
	const float grazing = 0.001f;
	if ( dist < grazing )
	{
		world_shading_normal = normalize( world_shading_normal - dist * world_wo );
		world_shading_normal = normalize( world_shading_normal + grazing * world_wo );
	}
	///////////////////////////////////////////////////////////////////////////
	// Adjust shading tangent
	///////////////////////////////////////////////////////////////////////////
	float d = dot( world_shading_tangent, world_shading_normal );
	world_shading_tangent = normalize( world_shading_tangent - d * world_shading_normal );
}


float3 getFlatColor()
{
	return make_float3( material_base_color_factor ) * make_float3( tex2D( material_base_color_texture, texcoord.x, texcoord.y ) );
}

float3 getEmission()
{
	return material_emissive_factor * make_float3( tex2D( material_emissive_texture, texcoord.x, texcoord.y ) );
}

float getMetallicity()
{
	return material_metallic_factor * tex2D( material_metal_roughness_texture, texcoord.x, texcoord.y ).z;
}

float getRoughness()
{
	return max(
	        max( material_roughness_factor * tex2D( material_metal_roughness_texture, texcoord.x, texcoord.y ).y, min_roughness ),
	        1e-3f );
}


float2 getAlpha()
{
	float roughness = getRoughness();
	return make_float2( roughness * roughness );
}


float3 fresnelSchlick( float cosTheta, const float3& F0 )
{
	return F0 + ( make_float3( 1.0 ) - F0 ) * pow( fmaxf( 1.0 - cosTheta, 0 ), 5.f );
}

float3 getF0( const float3& flatColor, const float& metallicity )
{
	float3 F0 = make_float3( 0.04 );
	F0 = lerp( F0, flatColor, metallicity );
	return F0;
}

float3 getF( const float3& F0, const float& wo_wh )
{
	float3 F = fresnelSchlick( wo_wh, F0 );
	DebugNaN( F.x, "NaN value in F\n" );
	DebugNaN( F.y, "NaN value in F\n" );
	DebugNaN( F.z, "NaN value in F\n" );
	return F;
}


///////////////////////////////////////////////////////////////////////////////
// Plain diffuse BRDF
///////////////////////////////////////////////////////////////////////////////

float3 getDiffuseBrdf( const float3& wi, const float3& wo )
{
	float metallicity = getMetallicity();
	float3 flatColor = getFlatColor();
	if ( wi.z <= 0.f || ( signbit( wi.z ) != signbit( wo.z ) ) )
		return make_float3( 0.0f );
	return ( 1.f - metallicity ) * ( 1.0f / float( M_PI ) ) * flatColor;
}


float getGgxD( const float3& wh )
{
	float2 alpha = getAlpha();
	float tan2Th = tan2Theta( wh );
	if ( isinf( tan2Th ) )
		return 0.0f;
	const float cos4Theta = cos2Theta( wh ) * cos2Theta( wh );
	float e = ( cos2Phi( wh ) / ( alpha.x * alpha.x ) + sin2Phi( wh ) / ( alpha.y * alpha.y ) ) * tan2Th;
	return 1 / ( M_PI * alpha.x * alpha.y * cos4Theta * ( 1 + e ) * ( 1 + e ) );
}

float getGgxLambda( const float3& w )
{
	float2 alpha = getAlpha();
	float absTanTheta = abs( tanTheta( w ) );
	if ( isinf( absTanTheta ) )
		return 0.0f;
	// Compute _alpha_ for direction _w_
	float alpha_w = sqrt( cos2Phi( w ) * alpha.x * alpha.x + sin2Phi( w ) * alpha.y * alpha.y );
	float alpha2Tan2Theta = ( alpha_w * absTanTheta ) * ( alpha_w * absTanTheta );
	return ( -1 + sqrt( 1.f + alpha2Tan2Theta ) ) / 2;
}

float getGgxG( const float3& V, const float3& L )
{
	return 1 / ( 1 + getGgxLambda( V ) + getGgxLambda( L ) );
}

float3 getGgxBrdf( const float3& wi, const float3& wo )
{
	float3 wh = normalize( wi + wo );
	float n_wi = fmaxf( 0.f, wi.z );
	float n_wo = fmaxf( 0.f, wo.z );
	float wo_wh = fmaxf( 0.f, dot( wo, wh ) );
	float d = 4 * n_wo * n_wi;
	DebugNaN( d, "NaN value in d in GGX BRDF\n" );
	if ( d <= 0 || wo_wh == 0.f )
	{
		return make_float3( 0 );
	}

	float D = getGgxD( wh );
	float G = getGgxG( wi, wo );
	DebugNaN( D, "NaN value in D in GGX BRDF\n" );
	DebugNaN( G, "NaN value in G in GGX BRDF\n" );
	return make_float3( ( D * G ) / d );
}


float3 getBrdf( const float3& wi, const float3& wo, const int ray_type )
{
	float3 wh = normalize( wo + wi );
	float3 F = getF( getF0( getFlatColor(), getMetallicity() ), dot( wo, wh ) );
	float3 ret = F * getGgxBrdf( wi, wo );
	if ( ray_type != 1 )
		ret += ( make_float3( 1.0 ) - F ) * getDiffuseBrdf( wi, wo );
	return ret;
}


float3 sampleDiffuseWi( const float3& wo, seed_t& seed )
{
	float z1 = randf( seed );
	float z2 = randf( seed );
	float3 p;
	cosine_sample_hemisphere( z1, z2, p );
	p = normalize( p );
	DebugNaN( p.z, "NaN value in p in Diffuse_sample_wi\n" );
	return p;
}

///////////////////////////////////////////////////////////////////////////////
// TrowbridgeReitz BRDF with sampling only visible area.
// Almost entirely stolen from PBRT.
///////////////////////////////////////////////////////////////////////////////
float2 sampleTrowbridgeReitz11( const float cosTheta, seed_t& seed )
{
	float U1 = randf( seed );
	float U2 = randf( seed );
	// special case (normal incidence) - Have to check for > .999999 because at .9999 we get a very ugly circle
	if ( cosTheta > .999999 )
	{
		float r = sqrtf( U1 / ( 1 - U1 ) );
		float phi = 2 * M_PI * U2;
		return r * make_float2( cos( phi ), sin( phi ) );
	}

	float sinTheta = sqrtf( fmaxf( (float)0, (float)1 - cosTheta * cosTheta ) );
	float tanTheta = sinTheta / cosTheta;
	float a = 1 / tanTheta;
	DebugNaN( a, "NaN value in a in TrowbridgeRS11\n" );
	float G1 = 2 / ( 1 + sqrt( fmaxf( 0, 1.f + 1.f / ( a * a ) ) ) );
	DebugNaN( G1, "NaN value in G1 in TrowbridgeRS11\n" );
	// sample slope_x
	float A = 2 * U1 / G1 - 1;
	if ( fabsf( A ) == 1 )
	{
		A -= copysignf( EPSILON, A );
	}
	DebugNaN( A, "NaN value in A in TrowbridgeRS11\n" );
	float tmp = 1.f / ( A * A - 1.f );
	if ( tmp > 1e10 )
	{
		tmp = 1e10;
	}
	float B = tanTheta;
	float D = sqrt( fmaxf( B * B * tmp * tmp - ( A * A - B * B ) * tmp, 0.f ) );
	float slope_x_1 = B * tmp - D;
	float slope_x_2 = B * tmp + D;
	float2 slope;
	slope.x = ( A < 0 || slope_x_2 > 1.f / tanTheta ) ? slope_x_1 : slope_x_2;
	// sample slope_y
	float S;
	if ( U2 > 0.5f )
	{
		S = 1.f;
		U2 = 2.f * ( U2 - .5f );
	}
	else
	{
		S = -1.f;
		U2 = 2.f * ( .5f - U2 );
	}
	float z = ( U2 * ( U2 * ( U2 * 0.27385f - 0.73369f ) + 0.46341f ) )
	          / ( U2 * ( U2 * ( U2 * 0.093073f + 0.309420f ) - 1.000000f ) + 0.597999f );
	slope.y = S * z * sqrtf( 1.f + slope.x * slope.x );
	return slope;
}

float3 sampleTrowbridgeReitz( const float3& wi, seed_t& seed )
{
	// 1. stretch wi
	float2 alpha = getAlpha();
	float3 wiStretched = normalize( make_float3( alpha.x * wi.x, alpha.y * wi.y, wi.z ) );
	// 2. simulate P22_{wi}(x_slope, y_slope, 1, 1)
	float2 slope = sampleTrowbridgeReitz11( cosTheta( wiStretched ), seed );
	DebugNaN( slope.x, "NaN value in slope_x in TrowbridgeRS\n" );
	DebugNaN( slope.y, "NaN value in slope_y in TrowbridgeRS\n" );

	// 3. rotate
	float cph = cosPhi( wiStretched );
	float sph = sinPhi( wiStretched );
	slope = make_float2( cph * slope.x - sph * slope.y, sph * slope.x + cph * slope.y );
	// 4. unstretch
	slope.x = alpha.x * slope.x;
	slope.y = alpha.y * slope.y;

	// 5. compute normal
	return normalize( make_float3( -slope.x, -slope.y, 1. ) );
}

float3 sampleGgxWi( const float3& wo, seed_t& seed )
{
	bool flip = wo.z < 0;
	float3 wh = sampleTrowbridgeReitz( flip ? -wo : wo, seed );
	DebugNaN( wh.x, "NaN value in wh.x in ggx_sample_wi\n" );
	DebugNaN( wh.y, "NaN value in wh.y in ggx_sample_wi\n" );
	DebugNaN( wh.z, "NaN value in wh.z in ggx_sample_wi\n" );
	if ( flip )
		wh = -wh;
	float3 wi = -reflect( wo, wh );
	DebugNaN( wi.z, "NaN value in wi in ggx_sample_wi\n" );
	return wi;
}

float3 sampleWi( const float3& wo, seed_t& seed, const int ray_type )
{
	// Pick wi randomly from one distribution
	if ( randf( seed ) < 0.5f && ray_type != 1 )
	{
		return sampleDiffuseWi( wo, seed );
	}
	else
	{
		return sampleGgxWi( wo, seed );
	}
}

float getDiffuseSampledWiPdf( const float3& wi, const float3& wo )
{
	return fmaxf( 0.f, wi.z ) / M_PI;
}

float getGgxSampledWiPdf( const float3& wi, const float3& wo )
{
	float3 wh = normalize( wi + wo );
	DebugNaN( wh.z, "NaN value in wh in GGX PDF. wi = (%f, %f, %f), wo = (%f, %f, %f).\n", wi.x, wi.y, wi.z, wo.x, wo.y, wo.z );
	if ( cosTheta( wi ) == 0.f || ( dot( wi, wh ) * cosTheta( wi ) <= 0.f ) )
	{
		return 0.f;
	}
	if ( tanTheta( wi ) == 0.f )
	{
		return 1.f;
	}

	float G1 = 1.0f / ( 1.0f + getGgxLambda( wo ) );
	DebugNaN( G1, "NaN value in G1 in GGX PDF\n" );
	float D = getGgxD( wh );
	DebugNaN( D, "NaN value in D in GGX PDF\n" );
	float wh_pdf = D * G1 * abs( dot( wo, wh ) ) / absCosTheta( wo );
	DebugNaN( wh_pdf, "NaN value in wh_pdf in GGX PDF\n" );
	return wh_pdf / ( 4 * abs( dot( wo, wh ) ) );
}


float getSampledWiPdf( float3& wi, const float3& wo, const int ray_type )
{
	if ( ray_type == 1 )
		return getGgxSampledWiPdf( wi, wo );
	else
		return 0.5 * ( getDiffuseSampledWiPdf( wi, wo ) + getGgxSampledWiPdf( wi, wo ) );
}


// Returns if the ray should continue iterating
bool sampleNextRay( const float3& wo, seed_t& seed, float3& attenuation, float& pdf, float3& new_direction, const int ray_type )
{
	DebugNaN( wo.z, "NaN value in wo in sample_next_ray\n" );
	float3 wi = sampleWi( wo, seed, ray_type );
	DebugNaN( wi.z, "NaN value in wi in sample_next_ray\n" );
	float3 wh = wi + wo;
	if ( dot( wh, wh ) == 0 )
	{
		attenuation = make_float3( 0.f );
		pdf = 0.f;
		return false;
	}
	DebugNaN( wh.z, "NaN value in wh0 in sample_next_ray\n" );

	pdf = getSampledWiPdf( wi, wo, ray_type );
	DebugNaN( pdf, "NaN value in pdf in sample_next_ray\n" );

	if ( pdf <= 0.f )
	{
		attenuation = make_float3( 0.f );
		pdf = 0.f;
		return false;
	}


	float cosine = fabsf( wi.z );
	DebugNaN( cosine, "NaN value in cosine in sample_next_ray\n" );

	attenuation *= getBrdf( wi, wo, ray_type ) * cosine;
	DebugNaN( attenuation.x, "NaN value in attenuation.x in sample_next_ray\n" );
	DebugNaN( attenuation.y, "NaN value in attenuation.y in sample_next_ray\n" );
	DebugNaN( attenuation.z, "NaN value in attenuation.z in sample_next_ray\n" );

	new_direction = wi;

	return true;
}


size_t findSmallestHigherValue( float value, optix::buffer<float, 1>& list )
{
	size_t higher = list.size() - 1;
	size_t lower = 0;
	if ( value < list[lower] )
	{
		return lower;
	}
	while ( higher - lower > 1 )
	{
		size_t d = lower + ( ( higher - lower ) / 2 );
		float v = list[d];
		if ( v < value )
		{
			lower = d;
		}
		else if ( v > value )
		{
			higher = d;
		}
		else
		{
			higher = d;
			break;
		}
	}
	return higher;
}

size_t findSmallestHigherValue( float value, optix::buffer<float, 2>& list, size_t row )
{
	size_t higher = list.size().x - 1;
	size_t lower = 0;
	if ( value < list[make_size_t2( lower, row )] )
	{
		return lower;
	}
	while ( higher - lower > 1 )
	{
		size_t d = lower + ( ( higher - lower ) / 2 );
		float v = list[make_size_t2( d, row )];
		if ( v < value )
		{
			lower = d;
		}
		else if ( v > value )
		{
			higher = d;
		}
		else
		{
			higher = d;
			break;
		}
	}
	return higher;
}

#if USE_MIS_ENVMAP
void importanceSampleEnvironmentMap( const float3& world_shading_normal,
                                     const float3& world_shading_tangent,
                                     const float3& wo,
                                     float3& world_wi,
                                     float3& radiance,
                                     const int ray_type,
                                     seed_t& seed )
{
	float rx = randf( seed );
	float ry = randf( seed );
	size_t y = findSmallestHigherValue( ry, envmap_marginal_cdf );
	size_t x = findSmallestHigherValue( rx, envmap_rows_cdf, y );
	float pdf = envmap_marginal_pdf[y] * envmap_rows_pdf[make_size_t2( x, y )];

	size_t2 sz = envmap_rows_cdf.size();

	rx = randf( seed );
	ry = randf( seed );
	float sx = float( x ) / float( sz.x ) + rx / float( sz.x );
	float sy = float( y ) / float( sz.y ) + ry / float( sz.y );

	float phi = sx * 2 * M_PI;
	float theta = sy * M_PI;
	world_wi = make_float3( cos( phi ) * sin( theta ), cos( theta ), sin( phi ) * sin( theta ) );
	// Remap pdf from over u,v to over directions
	// PBRT 3rd ed. pp 850
	pdf *= ( 1.0f / ( 4.0f * M_PI * M_PI * sinf( theta ) ) );
	float3 wi = to_tangent_space( world_shading_normal, world_shading_tangent, world_wi );

	float3 wh = wi + wo;
	if ( dot( wh, wh ) == 0 )
	{
		pdf = 0;
		radiance = make_float3( 0 );
		return;
	}

	pdf += getSampledWiPdf( wi, wo, ray_type );

	float costh = clamp( cosTheta( wi ), 0.f, 1.f );
	DebugNaN( costh, "NaN value in cosine in sample_environment_map\n" );

	float3 irradiance = sampleEnvironment( make_float2( sx, sy ) );
	radiance = irradiance * getBrdf( wi, wo, ray_type ) * costh / pdf;
}
#endif


float3 lightIrradiance( const float3& light_color,
                        const float& light_intensity,
                        const float& light_distance,
                        const float3& wi,
                        const float3& wo,
                        const int ray_type )
{
	const float falloff_factor = 1.0f / ( light_distance * light_distance );
	float3 Li = falloff_factor * light_intensity * light_color;
	float3 wh = normalize( wi + wo );
	float3 brdf = getBrdf( wi, wo, ray_type );

	DebugNaN( brdf.x, "NaN value in brdf in light_irradiance\n" );
	DebugNaN( brdf.y, "NaN value in brdf in light_irradiance\n" );
	DebugNaN( brdf.z, "NaN value in brdf in light_irradiance\n" );

	return Li * brdf * fmaxf( 0.f, cosTheta( wi ) );
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void onPbrHit( PBR_PerRayData& prd )
{
	float3 world_shading_normal;
	float3 world_shading_tangent;
	float3 world_geometric_normal;
	float3 hit_point;
	float3 world_wo;
	getHitVectors( world_shading_normal, world_shading_tangent, world_geometric_normal, hit_point, world_wo );

	float3 wo = to_tangent_space( world_shading_normal, world_shading_tangent, world_wo );
	DebugAssert( !isnan( wo.x ) && !isnan( wo.y ) && !isnan( wo.z ), "NaN value in wo in onpbrhit\n" );
	DebugAssert( !isnan( wo.x ) && !isnan( wo.y ) && !isnan( wo.z ), "wwo=(%f, %f, %f)\n", world_wo.x, world_wo.y, world_wo.z );
	DebugAssert( !isnan( wo.x ) && !isnan( wo.y ) && !isnan( wo.z ),
	             "wsn=(%f, %f, %f)\n",
	             shading_normal.x,
	             shading_normal.y,
	             shading_normal.z );
	DebugAssert( !isnan( wo.x ) && !isnan( wo.y ) && !isnan( wo.z ),
	             "wst=(%f, %f, %f)\n",
	             shading_tangent.x,
	             shading_tangent.y,
	             shading_tangent.z );

	///////////////////////////////////////////////////////////////////////////
	// Irradiance from light(s)
	///////////////////////////////////////////////////////////////////////////
#if 1
	if (num_lights > 0)
	{
		float z1 = randf( prd.seed );
		size_t li = size_t( z1 * lights_position.size() ) % lights_position.size();
		float3 light_position = lights_position[li];
		float light_intensity = lights_intensity[li];
		float3 light_color = lights_color[li];

		const float Ldist = length( light_position - hit_point );
		float3 world_wi = (light_position - hit_point) / Ldist;
		float3      wi = to_tangent_space( world_shading_normal, world_shading_tangent, world_wi );
		// Irradiance from the light(s), if not shadowed
		prd.light_radiance = lightIrradiance( light_color, light_intensity, Ldist, wi, wo, prd.ray_type );
		prd.light_position = light_position;
	}
	else
#endif
	{
		prd.light_radiance = make_float3( 0.0f, 0.0f, 0.0f );
	}

	// Light emission from the material
	{
		prd.radiance += getEmission();
	}

#if USE_MIS_ENVMAP
	importanceSampleEnvironmentMap(
	        world_shading_normal, world_shading_tangent, wo, prd.env_direction, prd.env_radiance, prd.ray_type, prd.seed );
#endif

	float3 wi;
	if ( !sampleNextRay( wo, prd.seed, prd.attenuation, prd.next_ray_pdf, wi, prd.ray_type ) )
	{
		prd.done = true;
	}

	///////////////////////////////////////////////////////////////////////////
	// Bias origin of ray and transform direction to world space
	///////////////////////////////////////////////////////////////////////////
	float bias = dot( world_wo, world_geometric_normal ) < 0 ? -EPSILON : EPSILON;
	prd.origin = hit_point + bias * world_geometric_normal;
	prd.direction = from_tangent_space( world_shading_normal, world_shading_tangent, wi );
	prd.hit = true;
}

void onPbrShadowHit( PBR_Shadow_PerRayData& shadow_prd )
{
	shadow_prd.in_shadow = true;
}

void onPbrMiss( PBR_PerRayData& prd, PBR_Shadow_PerRayData& shadow_prd )
{
	if ( ray.ray_type == closest_hit_ray_type_id )
	{
		const float2 uv = environmentCoords( ray.direction );
		prd.radiance = sampleEnvironment( uv );
		prd.done = true;
		prd.hit = false;
	}
	else
	{
		shadow_prd.in_shadow = false;
	}
}

void pbrPathInit( PBR_PerRayData& prd )
{
	prd.attenuation = make_float3( 1.0f );
	prd.next_ray_pdf = 1.f;
	prd.ray_type = 0;
	prd.done = false;
	prd.hit = false;
}

void _pbrBounceInit( PBR_PerRayData& prd, float3& out_old_attenuation, float& out_old_pdf )
{
	prd.radiance = make_float3( 0.f );
	prd.light_radiance = make_float3( 0.f );
	out_old_attenuation = prd.attenuation;
	out_old_pdf = prd.next_ray_pdf;
	prd.done = false;
	prd.hit = false;
}

// Returns the current ray's radiance contribution
float3 _pbrGetRayRadiance( PBR_PerRayData& prd, float3 old_attenuation, float old_pdf, uint& ray_count )
{
	bool was_done = prd.done;
	float3 attenuation = old_attenuation;
	prd.attenuation /= prd.next_ray_pdf;

	float3 radiance = prd.radiance;

#if USE_MIS_ENVMAP
	if ( !prd.hit )
	{
		// Get the pdf of sampling this direction using to the environment map
		// to do multiple importance sampling
		float2 env_uv = environmentCoords( prd.direction );
		size_t2 env_xy = make_size_t2( env_uv.x * envmap_rows_pdf.size().x, env_uv.y * envmap_rows_pdf.size().y );
		float env_pdf = envmap_marginal_pdf[env_xy.y] * envmap_rows_pdf[env_xy];

		// Remap pdf from over u,v to over directions
		// PBRT 3rd ed. pp 850
		float theta = env_uv.y * M_PI;
		env_pdf *= ( 1.0f / ( 4.0f * M_PI * M_PI * sinf( theta ) ) );


		// Radiance contains environment map information, weight it with MIS
		// We multiply by old_pdf to cancel out the one in old_attenuation
		radiance = prd.radiance * old_pdf / ( old_pdf + env_pdf );
	}

	// Check if we are in shadow of the environment ray
	{
		PBR_Shadow_PerRayData pbr_shadow_prd;
		pbr_shadow_prd.in_shadow = false;

		Ray shadow_ray = make_Ray( prd.origin, prd.env_direction, any_hit_ray_type_id, 0.f, RT_DEFAULT_MAX );

		ray_count += 1;
		rtTrace( top_shadowing_node, shadow_ray, pbr_shadow_prd );
		prd.done = was_done;

		if ( !pbr_shadow_prd.in_shadow && prd.hit )
		{
			// env_radiance is already weighted with the sum of the pdfs for MIS so don't need to divide now
			radiance += prd.env_radiance;
		}
	}
#endif

	// Check if we are in shadow of the light(s)
	{
		PBR_Shadow_PerRayData pbr_shadow_prd;
		pbr_shadow_prd.in_shadow = false;
		const float Ldist = length( prd.light_position - prd.origin );
		float3 wi = ( prd.light_position - prd.origin ) / Ldist;

		Ray shadow_ray = make_Ray( prd.origin, wi, any_hit_ray_type_id, 0.f, Ldist );
		ray_count += 1;
		rtTrace( top_shadowing_node, shadow_ray, pbr_shadow_prd );
		prd.done = was_done;

		if ( !pbr_shadow_prd.in_shadow && prd.hit )
		{
			radiance += prd.light_radiance;
		}
	}

	return radiance * attenuation;
}


bool _pbrIsRayFinished( const PBR_PerRayData& prd )
{
	if ( prd.done )
	{
		return true;
	}
	if ( prd.next_ray_pdf == 0.f )
	{
		return true;
	}
	if ( fmaxf( prd.attenuation ) <= 0.0001f )
	{
		return true;
	}
	return false;
}

void _pbrGetNextRay( const PBR_PerRayData& prd, float3& origin, float3& direction )
{
	origin = prd.origin;
	direction = prd.direction;
}

float3 pbrTraceBounce( uint bounce, PBR_PerRayData& prd, float3& origin, float3& direction, uint& ray_count, bool& finished )
{
	float3 result = make_float3( 0.f );
	float3 old_atten = make_float3( 1.f );
	float old_pdf = 1.f;

	_pbrBounceInit( prd, old_atten, old_pdf );

	ray_count += 1;
	Ray ray = make_Ray( origin, direction, closest_hit_ray_type_id, 0.f, RT_DEFAULT_MAX );
	rtTrace( top_scene_node, ray, prd );

	result = _pbrGetRayRadiance( prd, old_atten, old_pdf, ray_count );

	if ( bounce == 0 && !prd.hit )
	{
		// Just get the environment map radiance without any weighting with the pdf
		// We do it like this so that the rtTrace inside GetRayRadiance are run anyways
		result = prd.radiance;
	}

	_pbrGetNextRay( prd, origin, direction );

	finished = _pbrIsRayFinished( prd );

	{
		// Avoid NaNs
		// This will bias the result a bit but it will avoid NaNs which would break it anyway...
		if ( isnan( result.x ) || isnan( result.y ) || isnan( result.z ) )
		{
#ifndef _DEBUG   // We will only do this in release so that we still can find NaNs in debug
			result = make_float3( 0 );
			finished = true;
			rtPrintf( "NaN found!\n" );
#endif
		}
	}

	return result;
}
