#pragma once

#include "ipathtracer_renderer.h"

namespace pathtracer
{

class LightmapPathtracerRenderer : public IPathtracingRenderer
{
public:
	LightmapPathtracerRenderer() : IPathtracingRenderer() {}

	virtual void init() override;

	virtual void updateScene( scene::Scene * ) override;

	virtual void resize( uint16_t width, uint16_t height ) override;

	virtual bool resizeOnContextResized() const override { return false; }

	virtual void destroy() override;

	virtual void render() override;

	virtual void renderGUI() override;

protected:
	virtual void pathTrace_impl() override;

	virtual void onSetAsActive() override;
	void renderLightmapBaseTextures();
	void updatePositionsTexture();
	virtual void initializePathtracer_internal();

protected:
	GLSLProgramObject* m_gbuffer_program = nullptr;
	GLSLProgramObject* m_merge_conservative_program = nullptr;
	uint16_t m_base_width = 128, m_base_height = 128;
	GLuint m_position_texture = 0;
	GLuint m_shading_normal_texture = 0;
	GLuint m_geom_normal_texture = 0;
	GLuint m_geom_tangent_texture = 0;
	GLuint m_gbuffer = 0;
	GLuint m_corrected_position_texture = 0;
	bool m_positionsDirty = true;
};

}
