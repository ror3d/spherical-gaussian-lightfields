#include "ProgressListener.h"
#include <iostream>
#include <assert.h>

ProgressListener default_progress_listener;
TextProgressListener text_progress_listener;
ProgressListener * g_progress = & default_progress_listener;

void ProgressListener::activate()
{
	g_progress = &default_progress_listener;
}

void TextProgressListener::activate()
{
	g_progress = &text_progress_listener;
}

void TextProgressListener::setRange(int value)
{
	assert(m_progress_counters.size() > 0);
	m_progress_counters.back().range = value;
}
void TextProgressListener::update(int progress)
{
	assert(m_progress_counters.size() > 0);
	m_progress_counters.back().progress = progress;
	if (m_progress_counters.back().range == 0) {
		std::cout << "\r[" << m_progress_counters.back().name << "]";
	}
	else {
		if (progress >= m_progress_counters.back().range) {
			std::cout << "\r[" << m_progress_counters.back().name << "] Done!\n";
		}
		else {
			std::cout << "\r[" << m_progress_counters.back().name << "] ";
			printf("%.1f", 100.0f * float(m_progress_counters.back().progress) / float(m_progress_counters.back().range));
			std::cout << " (" << m_progress_counters.back().progress << "/"
				<< m_progress_counters.back().range << ")";
		}
	}
	std::cout << std::flush;
}

void TextProgressListener::push_task(const std::string & name, bool /*indeterminate*/)
{
	progress_counter pc;
	pc.name = name;
	m_progress_counters.push_back(pc);
	std::cout << "Starting " << name << std::endl; ;
	setRange(0);
	update(0);
}

void TextProgressListener::pop_task()
{
	assert(m_progress_counters.size() > 0);
	if (m_progress_counters.back().progress < m_progress_counters.back().range) {
		std::cout << "\r[" << m_progress_counters.back().name << "] Finished!\n";
	}
	m_progress_counters.pop_back();
}
