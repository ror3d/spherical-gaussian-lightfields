#include "orientation.h"
#include <iostream>
#include "utils/glm_extensions.h"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
using namespace std;

namespace chag
{

Orientation::Orientation(vec3 pos, vec3 dir, vec3 up)
{
  lookAt(pos, dir, up);
  scale = vec3(1.0f);
}

void Orientation::lookAt(vec3 eye, vec3 center, vec3 up)
{
  pos = eye;
  vec3 dir = normalize(eye - center);
  vec3 right = normalize(cross(up, normalize(dir)));
  vec3 newup = normalize(cross(dir, right));
  R = mat3(right, newup, dir);
}

void Orientation::lookAt(vec3 eye, vec3 center)
{
  this->pos = eye;
  vec3 dir = normalize(eye - center);
  vec3 right = normalize(perp(dir));
  vec3 newup = normalize(cross(dir,right));
  this->R = mat3(right, newup, dir);
}

void Orientation::set_as_modelview()
{
#ifndef NO_OPENGL_OR_CUDA
	mat4 MV = get_MV();
	glMatrixLoadfEXT(GL_MODELVIEW, glm::value_ptr(MV));
#endif
}

Orientation::mat4 Orientation::get_MV() const
{
	mat4 invrot = mat4(transpose(R));
	return glm::scale(mat4(1.0f),vec3(1.0f/scale.x, 1.0f/scale.y, 1.0f/scale.z)) * invrot * glm::translate(mat4(1.0f),-pos);
}

Orientation::mat4 Orientation::get_MV_inv() const
{
	return glm::translate(mat4(1.0f), pos) * mat4(R) * glm::scale(mat4(1.0f), scale);
}

void Orientation::roll(float angle)
{	
	R = R * mat3(glm::rotate(mat4(1.0f), angle, vec3(0.0f, 0.0f, -1.0f)));
}
void Orientation::yaw(float angle)
{
	R = R * mat3(glm::rotate(mat4(1.0f), angle, vec3(0.0f, 1.0f, 0.0f)));
}
void Orientation::pitch(float angle)
{
	R = R * mat3(glm::rotate(mat4(1.0f), angle, vec3(1.0f, 0.0f, 0.0f)));
}

ostream & operator << ( ostream &o, const orientation &myView)
{
	o << myView.R << " " << myView.pos << " " << myView.scale;
	return o;
}

istream & operator >> ( istream &i, orientation& myView)
{
	i >> myView.R >> myView.pos >> myView.scale;
	return i;
}


} // namespace chag
