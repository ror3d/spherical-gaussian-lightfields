#include "ScopeTimer.h"
#include <assert.h>
#include <GL/glew.h>
#include <utils/Log.h>
#include <map>


///////////////////////////////////////////////////////////////////////////////
// Global list of traces (e.g. "Frame", "Build BVH") and a stack of currently
// active traces
///////////////////////////////////////////////////////////////////////////////
std::map<std::string, Trace> g_traces;
std::vector<std::string> g_trace_stack;

// Sync and start a new trace
void push_trace(std::string name)
{
	assert(g_trace_stack.size() < 10); // Sanity check while building this stuff.
	g_trace_stack.push_back(name);
	// If a trace with this name already exists, wipe it and start fresh
	if (g_traces.count(name) > 0) {
		// Make sure we are not pushing an active trace
		assert(g_traces[name].active == false);
		g_traces[name].trace.clear();
	}
	g_traces[name].active = true; // Also creates the trace if it did not exist.
	PROFILE_SYNC("trace_begin");
}

// Sync and finalize trace
void pop_trace()
{
	// Sync
	PROFILE_SYNC("trace_end");
	// Finalize trace
	assert(g_trace_stack.size() > 0);
	Trace & trace = g_traces[g_trace_stack.back()];

	// Calculate CPU and OpenGL times at start of trace
	TraceEntry start_entry = trace.trace[0];
	uint64_t start_gl_time;
	glGetQueryObjectui64v(start_entry.m_gl_query, GL_QUERY_RESULT, &start_gl_time);

	for (auto & e : trace.trace)
	{
		// Calculate CPU time
		e.m_cpu_time = std::chrono::duration_cast<Milliseconds>(e.m_cpu_timestamp - start_entry.m_cpu_timestamp).count();
		// Calculate OpenGL time
		if (e.m_type == TraceEntry::ID_GL || e.m_type == TraceEntry::ID_SYNC) {
			uint64_t gl_time;
			glGetQueryObjectui64v(e.m_gl_query, GL_QUERY_RESULT, &gl_time);
			std::chrono::nanoseconds delta(gl_time - start_gl_time);
			e.m_opengl_time = std::chrono::duration_cast<Milliseconds>(delta).count();
		}
		else e.m_opengl_time = -1.0;
#ifdef CHAG_ENABLE_CUDA
		// Calculate CUDA time
		if (e.m_type == TraceEntry::ID_CUDA || e.m_type == TraceEntry::ID_SYNC) {
			float cuda_time;
			cudaEventElapsedTime(&cuda_time, start_entry.m_cuda_event, e.m_cuda_event);
			e.m_cuda_time = cuda_time;
		}
		else e.m_cuda_time = -1.0;
#endif
	}

	// Clean up GL and CUDA events
	for (auto & e : trace.trace)
	{
		if (e.m_type == TraceEntry::ID_GL || e.m_type == TraceEntry::ID_SYNC) {
			GLScopeTimer::return_query_id(e.m_gl_query);
		}
#ifdef CHAG_ENABLE_CUDA
		if (e.m_type == TraceEntry::ID_CUDA || e.m_type == TraceEntry::ID_SYNC) {
			cudaEventDestroy(e.m_cuda_event);
		}
#endif
	}

	// Remove from stack
	trace.active = false;
	g_trace_stack.pop_back();
}

// Get current trace
Trace & CURRENT_TRACE()
{
	assert(g_trace_stack.size() > 0);
	std::string current_trace_name = g_trace_stack.back();
	assert(g_traces.count(current_trace_name) > 0);
	return g_traces[current_trace_name];
}

std::map<std::string, Trace> & GET_TRACES()
{
	return g_traces;
}

///////////////////////////////////////////////////////////////////////////////
// Trace Sync (sync and record all times)
///////////////////////////////////////////////////////////////////////////////
TraceSync::TraceSync(const std::string &name)
{
#ifdef CHAG_ENABLE_CUDA
	// Sync CUDA
	cudaEvent_t sync_event;
	cudaEventCreate(&sync_event);
	cudaEventRecord(sync_event, 0);
	cudaEventSynchronize(sync_event);
	cudaEventDestroy(sync_event);
#endif
	// Sync GL
	GLsync sync_object = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
	GLenum sync_result = glClientWaitSync(sync_object, GL_SYNC_FLUSH_COMMANDS_BIT, 1000000000);
	if ((sync_result != GL_ALREADY_SIGNALED) && (sync_result != GL_CONDITION_SATISFIED)) {
		LOG_ERROR("Something went very wrong when waiting for GL Sync");
	}
	// Create trace entry
	Trace & current_trace = CURRENT_TRACE();
	if (current_trace.active == false) return;
	TraceEntry entry;
	entry.m_type = TraceEntry::ID_SYNC;
	entry.m_is_scope_start = true;
	entry.m_contains_scope = false;
	entry.m_name = name;
	// Record CPU time
	entry.m_cpu_timestamp = CPUScopeTimer::getTimestamp();
	// Record OpenGL event
	entry.m_gl_query = GLScopeTimer::get_query_id();
	glQueryCounter(entry.m_gl_query, GL_TIMESTAMP);
#ifdef CHAG_ENABLE_CUDA
	// Record CUDA event
	cudaEventCreate(&entry.m_cuda_event);
	cudaEventRecord(entry.m_cuda_event, 0);
#endif
	// Flush GL and Cuda client side queues
	glFlush();
#ifdef CHAG_ENABLE_CUDA
	cudaStreamQuery(0); // NOTE: Flushes queue as a side effect!
#endif
	// Push entry
	current_trace.trace.push_back(entry);
}

TraceSync::~TraceSync()
{
}

///////////////////////////////////////////////////////////////////////////////
// Plain CPU scope timer
///////////////////////////////////////////////////////////////////////////////
CPUScopeTimer::CPUScopeTimer(const std::string &name)
{
	Trace & current_trace = CURRENT_TRACE();
	if (current_trace.active == false) return;
	TraceEntry entry;
	entry.m_type = TraceEntry::ID_CPU;
	entry.m_is_scope_start = true;
	entry.m_contains_scope = false;
	entry.m_name = name;
	// Record CPU time
	entry.m_cpu_timestamp = getTimestamp();
	current_trace.trace.push_back(entry);
};

CPUScopeTimer::~CPUScopeTimer()
{
	Trace & current_trace = CURRENT_TRACE();
	if (current_trace.active == false) return;
	TraceEntry entry;
	entry.m_type = TraceEntry::ID_CPU;
	entry.m_is_scope_start = false;
	entry.m_contains_scope = false;
	// Record CPU time
	entry.m_cpu_timestamp = getTimestamp();
	current_trace.trace.push_back(entry);
}

Timestamp CPUScopeTimer::getTimestamp()
{
	return Clock::now();
}
///////////////////////////////////////////////////////////////////////////////
// OpenGL (and CPU) scope timer
///////////////////////////////////////////////////////////////////////////////

// Get gl query objects en masse to avoid scary gl calls when doing timing.
std::vector<uint32_t> GLScopeTimer::m_query_ids;
uint32_t GLScopeTimer::get_query_id()
{
	if (m_query_ids.size() == 0) {
		// Out of query id's. If some are taken, they will be pushed back later
		// (increasing total size), so just add to the list.
		LOG_WARNING("Out of GL timer query IDs. Replenishing.");
		const int nof_new_queries = 1000;
		m_query_ids.resize(nof_new_queries);
		glGenQueries(nof_new_queries, &m_query_ids[0]);
	}
	uint32_t q = m_query_ids.back();
	m_query_ids.pop_back();
	return q;
}

void GLScopeTimer::return_query_id(uint32_t id)
{
	m_query_ids.push_back(id);
}

GLScopeTimer::GLScopeTimer(const std::string &name)
{
	Trace & current_trace = CURRENT_TRACE();
	if (current_trace.active == false) return;
	TraceEntry entry;
	entry.m_type = TraceEntry::ID_GL;
	entry.m_contains_scope = false;
	entry.m_is_scope_start = true;
	entry.m_name = name;
	// Record CPU time
	entry.m_cpu_timestamp = CPUScopeTimer::getTimestamp();
	// Record OpenGL start event
	entry.m_gl_query = get_query_id();
	glQueryCounter(entry.m_gl_query, GL_TIMESTAMP);
	current_trace.trace.push_back(entry);
	glFlush();
};

GLScopeTimer::~GLScopeTimer()
{
	Trace & current_trace = CURRENT_TRACE();
	if (current_trace.active == false) return;
	TraceEntry entry;
	entry.m_type = TraceEntry::ID_GL;
	entry.m_is_scope_start = false;
	entry.m_contains_scope = false;
	// Record CPU time
	entry.m_cpu_timestamp = CPUScopeTimer::getTimestamp();
	// Record OpenGL start event
	entry.m_gl_query = get_query_id();
	glQueryCounter(entry.m_gl_query, GL_TIMESTAMP);
	current_trace.trace.push_back(entry);
	glFlush();
};

#ifdef CHAG_ENABLE_CUDA
///////////////////////////////////////////////////////////////////////////////
// CUDA (and CPU) scope timer
///////////////////////////////////////////////////////////////////////////////
CUDAScopeTimer::CUDAScopeTimer(const std::string &name)
{
	Trace & current_trace = CURRENT_TRACE();
	if (current_trace.active == false) return;
	TraceEntry entry;
	entry.m_type = TraceEntry::ID_CUDA;
	entry.m_is_scope_start = true;
	entry.m_contains_scope = false;
	entry.m_name = name;
	// Record CPU time
	entry.m_cpu_timestamp = CPUScopeTimer::getTimestamp();
	// Record CUDA start event
	cudaEventCreate(&entry.m_cuda_event);
	cudaEventRecord(entry.m_cuda_event, 0);
	cudaStreamQuery(0); // NOTE: Flushes queue as a side effect!
	current_trace.trace.push_back(entry);
};

CUDAScopeTimer::~CUDAScopeTimer()
{
	Trace & current_trace = CURRENT_TRACE();
	if (current_trace.active == false) return;
	TraceEntry entry;
	entry.m_type = TraceEntry::ID_CUDA;
	entry.m_is_scope_start = false;
	entry.m_contains_scope = false;
	// Record CPU time
	entry.m_cpu_timestamp = CPUScopeTimer::getTimestamp();
	// Record CUDA stop event
	cudaEventCreate(&entry.m_cuda_event);
	cudaEventRecord(entry.m_cuda_event, 0);
	cudaStreamQuery(0); // NOTE: Flushes queue as a side effect!
	current_trace.trace.push_back(entry);
}
#endif


