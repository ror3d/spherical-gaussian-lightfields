#include <cstdlib>			// exit()
#include <utils/Log.h>
#include "GLSLProgramManager.h"
#include <assert.h>
#define NV_REPORT_COMPILE_ERRORS
//#include "./nvShaderUtils.h"

using namespace std;

GLSLProgramManager::GLSLProgramManager(string filename)
{
	setFile(filename);
}

void GLSLProgramManager::setFile(string filename)
{
	m_fullSource.clear();
	LOG_VERBOSE("Loading shader collection: " << filename);
	// Load the file as a list of strings
	m_filename = filename;
	ifstream f;
	f.open(filename.c_str());
	if(!f.is_open()){
		LOG_ERROR("Loading shader collection: " << filename << "FAILED.");
	}
	string lineread;
	while(std::getline(f, lineread))
	{
#ifndef WINDOWS
		if (lineread.size() > 0 && lineread.end()[-1] == '\r')
			lineread.erase(lineread.end()-1);		// strip DOS line ending
#endif
		m_fullSource.push_back(lineread);
	}
}

GLSLProgramManager::~GLSLProgramManager(void)
{
	for (auto& program : m_loadedPrograms)
		delete program.programObject;
	m_loadedPrograms.clear();
}

string GLSLProgramManager::extractShader(string shaderName, const std::string& prepend)
{
	// First find what common clauses to use, if any
	vector<string> is_using;
	for(unsigned int i=0; i<m_fullSource.size(); i++){
		string &line = m_fullSource[i];
		if(line == string("shader ") + shaderName) {
			string line2 = m_fullSource[i+1];
			if(line2.substr(0, 5) == "using"){
				size_t current = 6;
				size_t next = std::numeric_limits<size_t>::max();
				do
				{
					next = line2.find(" \0", current+1);
					if (next == std::numeric_limits<size_t>::max()) next = line2.size();
					string common = line2.substr(current, next - current);
					current = next + 1;
					is_using.push_back(common);
				}
				while(current < line2.size());
			}
		}
	}

	// Create the source files without losing line information
	string shader;
	// Add the defines
	if (prepend != "")
		shader += prepend;
	for(map<string, string>::iterator i = m_defines.begin(); i!=m_defines.end(); i++){
		shader += "#define ";
		shader += (*i).first;
		shader += " ";
		shader += (*i).second;
		shader += "\n";
	}

	bool shaderFound = false;
	bool commonFound = false;
	string version_line = "";
	for(unsigned int i=0; i<m_fullSource.size(); i++){
		string line = m_fullSource[i];
		if(line == string("shader ") + shaderName) {
			shaderFound = true;
			shader += "#line ";
			shader += std::to_string(i + 1);
			shader += "\n";
		}
		else if(line.substr(0,6) == "common"){
			string block = line.substr(7, line.size());
			for(unsigned int j=0; j<is_using.size(); j++){
				if(block == is_using[j]) {
					commonFound = true;
					shader += "#line ";
					shader += std::to_string(i + 1);
					shader += "\n";
				}
			}
		}
		else if(line.substr(0,1) == "{"){
			if(commonFound || shaderFound) {
				shader += "\n";
			}
		}
		else if(line.substr(0,1) == "}"){
			if(shaderFound) break;
			if(commonFound) {
				shader += "\n";
				commonFound = false;
			}
		}
		else if(commonFound) shader += line + "\n";
		else if(shaderFound) {
			///////////////////////////////////////////////////////////////////////////
			// As of driver 347.x, it seems the #version statement must preceed ANY
			// other statement, including e.g. #line, so:
			///////////////////////////////////////////////////////////////////////////
			if(line.find("#version") != string::npos) version_line = line;
			else {
				if(line.substr(0, 5) == "using") line = string("//") + line;
				else shader += line + "\n";
			}
		}
	}
	if(version_line != "") shader = version_line + "\n" + shader;

	if(!shaderFound) return "";
	return shader;
}

void GLSLProgramManager::compileProgram(const string &vertex_shader,
                                        const string &fragment_shader,
                                        GLSLProgramObject *program,
                                        const string &vertexPrepend,
                                        const string &fragmentPrepend)
{
	LOG_VERBOSE("Creating GLSL program: (" << vertex_shader << ", " << fragment_shader << ") from " << m_filename);
	string vertexShaderSource = extractShader(vertex_shader, vertexPrepend);
	if(vertexShaderSource == "") {
		LOG_ERROR("Failed to extract shader \"" << vertex_shader);
		assert(false);
	}
	string fragmentShaderSource = extractShader(fragment_shader, fragmentPrepend);
	if(fragmentShaderSource == "") {
		LOG_ERROR("Failed to extract shader \"" << fragment_shader);
		assert(false);
	}
	program->compileProgram(vertexShaderSource.c_str(), NULL, fragmentShaderSource.c_str());
}

void GLSLProgramManager::compileProgram(const string &compute_shader,
                                        GLSLProgramObject *program)
{
	LOG_VERBOSE("Creating GLSL program: (" << compute_shader << ") from " << m_filename);
	string computeShaderSource = extractShader(compute_shader);
	if(computeShaderSource == "") {
		LOG_ERROR("Failed to extract shader \"" << compute_shader);
		assert(false);
	}
	program->compileProgram(computeShaderSource.c_str());
}

void GLSLProgramManager::compileProgram(const string &vertex_shader,
                                        const string &geometry_shader,
                                        const string &fragment_shader,
                                        GLenum gsInput,
                                        GLenum gsOutput,
                                        unsigned int verticesOut,
                                        GLSLProgramObject *program)
{
	LOG_VERBOSE("Creating GLSL program: (" << vertex_shader << ", " << geometry_shader << ", " << fragment_shader << ") from " << m_filename);
	string vertexShaderSource = extractShader(vertex_shader);
	if(vertexShaderSource == "") {
		LOG_ERROR("Failed to extract shader \"" << vertex_shader);
		assert(false);
	}
	string geometryShaderSource = extractShader(geometry_shader);
	if(geometryShaderSource == "") {
		LOG_ERROR("Failed to extract shader \"" << geometry_shader);
		assert(false);
	}
	string fragmentShaderSource = extractShader(fragment_shader);
	if(fragmentShaderSource == "") {
		LOG_ERROR("Failed to extract shader \"" << fragment_shader);
		assert(false);
	}
	program->compileProgram(vertexShaderSource.c_str(), geometryShaderSource.c_str(), fragmentShaderSource.c_str(),
	                        gsInput, gsOutput, verticesOut);
}

GLSLProgramObject * GLSLProgramManager::getProgramObject(const string &compute_shader)
{
	GLSLProgramObject *program = new GLSLProgramObject;
	compileProgram(compute_shader, program);
	LoadedProgram thisOne = { "", "", "", compute_shader, 0, 0, 0, program, "", "", "", ""};
	m_loadedPrograms.push_back(thisOne);
	return program;
}

GLSLProgramObject * GLSLProgramManager::getProgramObject(const string &vertex_shader,
                                                         const string &fragment_shader,
                                                         const string &vertex_prepend,
                                                         const string &fragment_prepend)
{
	GLSLProgramObject *program = new GLSLProgramObject;
	compileProgram(vertex_shader, fragment_shader, program, vertex_prepend, fragment_prepend);
	LoadedProgram thisOne = {vertex_shader, "", fragment_shader, "", 0, 0, 0, program, vertex_prepend, "", fragment_prepend, ""};
	m_loadedPrograms.push_back(thisOne);
	return program;
}

GLSLProgramObject * GLSLProgramManager::getProgramObject(const string &vertex_shader,
                                                         const string &fragment_shader)
{
	GLSLProgramObject *program = new GLSLProgramObject;
	compileProgram(vertex_shader, fragment_shader, program);
	LoadedProgram thisOne = {vertex_shader, "", fragment_shader, "", 0, 0, 0, program, "", "", "", ""};
	m_loadedPrograms.push_back(thisOne);
	return program;
}

GLSLProgramObject * GLSLProgramManager::getProgramObject(const string &vertex_shader,
                                                         const string &geometry_shader,
                                                         const string &fragment_shader,
                                                         GLenum gsInput,
                                                         GLenum gsOutput,
                                                         unsigned int verticesOut)
{
	GLSLProgramObject *program = new GLSLProgramObject;
	compileProgram(vertex_shader, geometry_shader, fragment_shader, gsInput, gsOutput, verticesOut, program);
	LoadedProgram thisOne = {vertex_shader, geometry_shader, fragment_shader, "", gsInput, gsOutput, verticesOut, program, "", "", "", ""};
	m_loadedPrograms.push_back(thisOne);
	return program;
}


// Re-read, re-extract and re-compile shaders.
// Useful if m_defines have been changed, or if file has been edited.
void GLSLProgramManager::reload()
{
	setFile(m_filename);
	for(unsigned int i=0; i<m_loadedPrograms.size(); i++)
	{
		m_loadedPrograms[i].programObject->destroy();
		if(m_loadedPrograms[i].computeShader != "") {
			compileProgram(m_loadedPrograms[i].computeShader, m_loadedPrograms[i].programObject);
		}
		else if(m_loadedPrograms[i].geometryShader == ""){
			compileProgram(m_loadedPrograms[i].vertexShader,
			               m_loadedPrograms[i].fragmentShader,
			               m_loadedPrograms[i].programObject,
			               m_loadedPrograms[i].vertexPrepend,
			               m_loadedPrograms[i].fragmentPrepend);
		}
		else {
			compileProgram(m_loadedPrograms[i].vertexShader,
			               m_loadedPrograms[i].geometryShader,
			               m_loadedPrograms[i].fragmentShader,
			               m_loadedPrograms[i].geomIn,
			               m_loadedPrograms[i].geomOut,
			               m_loadedPrograms[i].verticesIn,
			               m_loadedPrograms[i].programObject);
		}
	}
}

void GLSLProgramManager::setPreprocDef(const char *name, int value)
{
	char buf[11];
	sprintf(buf, "%d", value);
	m_defines[name] = buf;
}



void GLSLProgramManager::setPreprocDef(const char *name, bool value)
{
	m_defines[name] = value ? "1" : "0";
}
