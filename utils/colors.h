#pragma once
#include "glm/glm.hpp"

namespace chag
{
using color4_t = glm::vec4;
using color3_t = glm::vec3;

inline color3_t hsv_to_rgb( float h, float s, float v )
{
	int h_i = (h * 6);
	float f = h * 6 - h_i;
	float p = v * (1 - s);
	float q = v * (1 - f * s);
	float t = v * (1 - (1 - f) * s);
	switch ( h_i )
	{
		case 0:
			return { v, t, p };
			break;
		case 1:
			return { q, v, p };
			break;
		case 2:
			return { p, v, t };
			break;
		case 3:
			return { p, q, v };
			break;
		case 4:
			return { t, p, v };
			break;
		case 5:
			return { v, p, q };
			break;
		default:
			return { 0, 0, 0 };
			break;
	}
}

}
