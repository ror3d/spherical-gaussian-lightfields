#pragma once
#include <glm/glm.hpp>
#include <cstring>
#include <stb_image.h>
#include <utils/Log.h>
#include <utils/opengl_helpers.h>

enum class ImageInterpolationType
{
    NEAREST,
    LINEAR
};

template<typename T>
struct Image
{
	public: 
	int width = 0, height = 0; 
	std::vector<T> data; 
	uint32_t gl_id = 0; 
    bool depth_buffer = false;
	uint32_t GLInternalFormat();
	
	void Resize(int _width, int _height) {
		width = _width; 
		height = _height; 
		data.resize(width * height);
		if (gl_id == 0) {
			gl_id = opengl_helpers::texture::createTexture(width, height, GLInternalFormat());
		}
		else {
            opengl_helpers::texture::resizeTexture(gl_id, width, height);
		}
		glTextureParameteri(gl_id, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTextureParameteri(gl_id, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}
	void LoadPNG(const std::string & filename);
	void Download()
	{
		uint32_t format = opengl_helpers::texture::getDefaultFormat(GLInternalFormat());
		uint32_t type = opengl_helpers::texture::getDefaultType(GLInternalFormat());
		glGetTextureImage(gl_id, 0, format, type, width * height * sizeof(T), data.data());
	}
	void Upload()
	{
		uint32_t format = opengl_helpers::texture::getDefaultFormat(GLInternalFormat());
		uint32_t type = opengl_helpers::texture::getDefaultType(GLInternalFormat());
		glTextureSubImage2D(gl_id, 0, 0, 0, width, height, format, type, data.data());
	}
};

/* This assumes that your image type is float or vecX */
template<typename T> void Image<T>::LoadPNG(const std::string & filename) {
	stbi_set_flip_vertically_on_load(true);
	int dummy; 
	T * stb_data = (T *)stbi_loadf(filename.c_str(), &width, &height, &dummy, sizeof(T)/sizeof(float));
	if (stb_data == nullptr) {
		LOG_ERROR("Failed to load" << filename); 
	}
	Resize(width, height);
	memcpy(data.data(), stb_data, width * height * sizeof(T));
	stbi_image_free(stb_data);
}
template<> inline uint32_t Image<glm::vec4>::GLInternalFormat() { return GL_RGBA32F; }
template<> inline uint32_t Image<glm::vec3>::GLInternalFormat() { return GL_RGB32F; }
template<> inline uint32_t Image<float>::GLInternalFormat()
{ 
    if (depth_buffer)
        return GL_DEPTH_COMPONENT32;
    else
        return GL_R32F; 
}
template<> inline uint32_t Image<uint32_t>::GLInternalFormat() { return GL_R32UI; }

template<typename T>
T Lookup(const Image<T> & image, glm::vec2 texcoord, ImageInterpolationType iit = ImageInterpolationType::NEAREST)
{
    T result; 

    switch (iit)
    {
        case ImageInterpolationType::NEAREST:
            {
                glm::ivec2 coord = glm::ivec2(texcoord.x * image.width, texcoord.y * image.height);
                // TODO: Currently returning uninitialized value when out of bounds. Better
                //		 rules might be a good idea. 
                if (coord.x < 0 || coord.x >= image.width || coord.y < 0 || coord.y >= image.height) return result;
                else return image.data[coord.y * image.width + coord.x];
            }
        case ImageInterpolationType::LINEAR:
            {
                glm::vec2 px_coord = glm::vec2(texcoord.x * image.width, texcoord.y * image.height);

                // Compute which integer coords to sample from. Counter-clockwise from bottom left.
                glm::ivec2 sample_coords[4];
                sample_coords[0] = glm::round(px_coord - glm::vec2(1,1));
                sample_coords[1] = glm::round(px_coord - glm::vec2(0,1));
                sample_coords[2] = glm::round(px_coord - glm::vec2(0,0));
                sample_coords[3] = glm::round(px_coord - glm::vec2(1,0));

                // Check for out of bounds.
                for (const auto & s : sample_coords) if (s.x < 0 || s.x >= image.width || s.y < 0 || s.y >= image.height) return result;

                // Collect weighted sample (see wikipedia).
                float x = px_coord.x;
                float y = px_coord.y;
                float x1 = sample_coords[0].x + 0.5f;
                float y1 = sample_coords[0].y + 0.5f;
                float x2 = sample_coords[1].x + 0.5f;
                float y2 = sample_coords[2].y + 0.5f;

                T weighted_sample = 
                    (x2 - x) * (y2 - y) * image.data[sample_coords[0].y * image.width + sample_coords[0].x] +
                    (x - x1) * (y2 - y) * image.data[sample_coords[1].y * image.width + sample_coords[1].x] + 
                    (x - x1) * (y - y1) * image.data[sample_coords[2].y * image.width + sample_coords[2].x] +
                    (x2 - x) * (y - y1) * image.data[sample_coords[3].y * image.width + sample_coords[3].x];

                return weighted_sample;
            }

    }
    return result;
}
