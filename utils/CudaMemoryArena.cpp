#include "CudaMemoryArena.h"
#include <cuda_runtime.h>
#include "Log.h"
#include <list>
#include <assert.h>
#include <chrono>
#include <thread>
#include <algorithm>

int uint64_log2(uint64_t n)
{
#define S(k) if (n >= (UINT64_C(1) << k)) { i += k; n >>= k; }
	int i = -(n == 0); S(32); S(16); S(8); S(4); S(2); S(1); return i;
#undef S
}

static inline int
is_pow_of_2(uint64_t x) {
	return !(x & (x - 1ull));
}

static inline uint64_t
next_pow_of_2(uint64_t x) {
	if (is_pow_of_2(x))	return x;
	x |= x >> 1;
	x |= x >> 2;
	x |= x >> 4;
	x |= x >> 8;
	x |= x >> 16;
	return x + 1;
}

CudaMemoryArena::CudaMemoryArena(uint64_t size)
{
	// Always allocate a power of two sized chunk
	uint64_t allocated_size = next_pow_of_2(size);
	uint64_t allocated_blocks = allocated_size / CMA_BLOCK_SIZE;
	// Create a buddy allocator of this size
	m_buddy_allocator = buddy_new(uint64_log2(allocated_blocks));
	// Allocate a corresponding slab of GPU memory
	cudaError_t error = cudaMalloc(&d_ptr, allocated_size);
	if (error != cudaSuccess) {
		LOG_ERROR("CudaMemoryArena: Failed to allocate " << allocated_size << " bytes of CUDA memory."); 
	}
	else {
		LOG_VERBOSE("CudaMemoryArena: Allocated " << allocated_size << " bytes for arena."); 
	}
}


CudaMemoryArena::~CudaMemoryArena()
{
	// Delete the allocator and the GPU memory
	buddy_delete(m_buddy_allocator);
	m_buddy_allocator = nullptr; 
	cudaError_t error = cudaFree(d_ptr);
	if (error != cudaSuccess) {
		LOG_ERROR("CudaMemoryArena: Failed to free() CUDA memory.");
	}
}

void * CudaMemoryArena::allocate(uint64_t size)
{
	uint64_t allocated_blocks = (size - 1) / CMA_BLOCK_SIZE + 1;
	int block = buddy_alloc(m_buddy_allocator, uint32_t(allocated_blocks)); 
	if (block == -1) {
		LOG_ERROR("CudaMemoryArena: Failed to allocate " << size << " bytes of memory from arena."); 
		return nullptr; 
	}
	else {
		uint64_t offset = block * CMA_BLOCK_SIZE; 
		return (void *)(((uint8_t *)d_ptr) + offset);
	}
}

void CudaMemoryArena::free(void * ptr)
{
	uint32_t block = uint32_t((uint64_t(ptr) - uint64_t(d_ptr)) / CMA_BLOCK_SIZE); 
	buddy_free(m_buddy_allocator, block);
}

uint64_t CudaMemoryArena::available()
{
	return uint64_t(buddy_available(m_buddy_allocator)) * uint64_t(CMA_BLOCK_SIZE);
}


///////////////////////////////////////////////////////////////////////////////
// Simple unit test. The performance meassurements may or may not work when 
// we've moved to VS2015. In VS2013, the high_resolution_clock is broken. 
///////////////////////////////////////////////////////////////////////////////
void CudaMemoryArena_UnitTest()
{
	using namespace std::chrono;

	const uint64_t total_size = 0x40000000ull; 
	const int iterations = 1000;

	CudaMemoryArena arena(total_size); 

	struct Allocation {
		uint64_t requested_size; 
		uint64_t actual_size; 
		void * ptr; 
	};

	std::list<Allocation> allocations; 

	auto requestedSize = [&allocations]() {
		uint64_t s = 0; 
		for(auto a : allocations) s += a.requested_size;
		return s; 
	};

	auto actualSize = [&allocations]() {
		uint64_t s = 0;
		for (auto a : allocations) s += a.actual_size;
		return s;
	};

	double nof_allocations = 0.0; 
	double allocation_time_sum = 0.0;
	double allocation_time_min = DBL_MAX;
	double allocation_time_max = 0.0;

	double nof_frees = 0.0; 
	double free_time_sum = 0.0;
	double free_time_min = DBL_MAX;
	double free_time_max = 0.0;

	for (int i = 0; i < iterations; i++)
	{
		LOG_INFO("Available: " << arena.available() << ", Expected Available: " << total_size - actualSize() << ", Requested: " << requestedSize()); 

		if (arena.available() != total_size - actualSize()) {
			LOG_ERROR("TEST_FAILED! Actually available memory is not what was expected."); 
			exit(0);
		}

		uint64_t currently_available = arena.available(); 
		double prob_of_allocation = double(currently_available) / double(total_size); 
		double xi = rand() / double(RAND_MAX);
		if (xi < prob_of_allocation) 
		{
			// Try a new allocation of random size

			Allocation a; 
			a.requested_size = uint64_t((rand() / double(RAND_MAX)) * total_size);
			while (true) {
				assert(a.requested_size > 0);
				a.actual_size = next_pow_of_2(a.requested_size);

				high_resolution_clock::time_point t0 = high_resolution_clock::now();
				nof_allocations += 1.0; 
				a.ptr = arena.allocate(a.requested_size);
				high_resolution_clock::time_point t1 = high_resolution_clock::now();
				double time = duration_cast<duration<double>>(t1 - t0).count();

				allocation_time_sum += time; 
				allocation_time_min = std::min(allocation_time_min, time);
				allocation_time_max = std::max(allocation_time_max, time);

				if (a.ptr != nullptr) {
					allocations.push_back(a);
					break;
				}
				else {
					a.requested_size /= 2; 
				}
			}
			LOG_INFO("Allocated " << a.requested_size << " [" << a.actual_size << "] bytes.");
		}
		else
		{
			// Free one random allocation
			int j = rand() % allocations.size(); 
			auto iter = std::next(allocations.begin(), j); 
			
			high_resolution_clock::time_point t0 = high_resolution_clock::now();
			nof_frees += 1.0;
			arena.free((*iter).ptr);
			high_resolution_clock::time_point t1 = high_resolution_clock::now();
			double time = duration_cast<duration<double>>(t1 - t0).count();
			free_time_sum += time;
			free_time_min = std::min(free_time_min, time);
			free_time_max = std::max(free_time_max, time);

			LOG_INFO("Freed " << (*iter).requested_size << " [" << (*iter).actual_size << "] bytes from: " << (*iter).ptr);
			allocations.erase(std::next(allocations.begin(), j));
		}
	}

	// Free all remaining allocations and make sure all space is back
	for (auto & a : allocations) {
		arena.free(a.ptr);
	}

	if (arena.available() != total_size) {
		LOG_ERROR("TEST FAILED! All memory was not freed!");
		exit(0);
	}

	LOG_INFO("TEST SUCCEEDED!");
	double average_allocation_time = allocation_time_sum / nof_allocations;
	LOG_INFO("Allocation performance: Avg = " << average_allocation_time << ", min = " << allocation_time_min << ", max = " << allocation_time_max);
	double average_free_time = free_time_sum / nof_frees;
	LOG_INFO("Free performance: Avg = " << average_free_time << ", min = " << free_time_min << ", max = " << free_time_max);
}