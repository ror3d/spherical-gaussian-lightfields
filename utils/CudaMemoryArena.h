#pragma once
#include "buddy_allocator.h"
#include <inttypes.h>

#define CMA_BLOCK_SIZE 256
///////////////////////////////////////////////////////////////////////////////
// A memory arena for CUDA that allows us to include memory allocations and 
// deallocations in timed scopes (cudaMalloc is unreasonably slow for larger
// chunks). Currently built on a buddy allocator stolen from: 
// https://github.com/cloudwu/buddy
// 
// NOTES: 
// * Performance is very good, but any allocation will be of a power of 2, so 
//   lots of memory (up to 50%) can be wasted if you're unlucky. 
// * The smallest allocatable block is CMA_BLOCK_SIZE. But, if a situation 
//   arises where we need many small allocations, we should probably 
//   incorporate special methods for that, using a slab allocator or something 
//   instead. 
// * The allocator _should_ be fine up to sizes of 2G * CMA_BLOCK_SIZE. The 
//	 buddy allocator sometimes uses plain ints to describe the block offset. 
///////////////////////////////////////////////////////////////////////////////
class CudaMemoryArena
{
private: 
	// The actual allocator 
	buddy * m_buddy_allocator = nullptr; 
	// Pointer to CUDA memory
	void * d_ptr = nullptr; 
public:
	CudaMemoryArena(uint64_t size);
	~CudaMemoryArena();

	void * allocate(uint64_t size_in_bytes); 
	template <class T> T * allocate(uint64_t nof_elements) {
		return (T *) allocate(nof_elements * sizeof(T));
	}
	void free(void * ptr);
	uint64_t available();
};

void CudaMemoryArena_UnitTest();