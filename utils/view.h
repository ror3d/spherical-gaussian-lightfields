#pragma once
#include <iostream>
#include "orientation.h"
#undef near
#undef far

/**
 * A view is a composite of its base class, the orientation
 * and the variables defining its frustrum.
 */

class view :
	public chag::orientation
{
public:
	view(void);
	~view(void);
	virtual view *clone();
	// Perspective view variables
	float m_fov; 
	float m_aspect_ratio;
	float m_near; 
	float m_far; 

	virtual void set_as_projection();
	void push();
	void pop();
	glm::mat4 get_MVP() const;
	glm::mat4 get_MVP_inv() const;
	virtual glm::mat4 get_P() const;
	virtual glm::mat4 get_P_inv() const;

	static void drawFullScreenQuad();
	void draw(); 

	friend std::ostream & operator << ( std::ostream &o, const view &myView); 
	friend std::istream & operator >> ( std::istream &i, view& myView);
};

/** 
 * Less than optimally, an orthoview inherits the "view" which is actually
 * a perspective view. Some day I should let perspectiveview inherit view as well
 * leaving view as an interface. 
 */
class orthoview : public view
{
public: 
	orthoview(void); 
	~orthoview(void) {}; 
	virtual view *clone();
	// Ortho view variables
	float m_right, m_left, m_top, m_bottom;
	virtual glm::mat4 get_P() const;
	virtual glm::mat4 get_P_inv() const;
	friend std::ostream & operator << (std::ostream &o, const orthoview &myView);
	friend std::istream & operator >> ( std::istream &i, orthoview& myView);
};
