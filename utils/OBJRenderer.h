#pragma once
#include "DebugRenderer.h"

namespace chag {

class OBJRenderer :
	public DebugRenderer
{
public:
	static OBJRenderer& instance(); 
	OBJRenderer(void);
	~OBJRenderer(void);
	virtual void flushLayer(const std::string &layerId); 
};

}