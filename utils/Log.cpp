#include <utils/Log.h>

using namespace std::string_literals;

Log::Level Log::level_active = Log::Verbose; 
Log::LogOStream Log::log_stream;

void _chag_error( const char* file, unsigned int line, const std::string& reason )
{
	Log::InsertTimestamp();
	Log::GetStream() << " ERROR in " << file << "(" << line << ") : " << reason << "\n";

	std::string errorText = file + "("s + std::to_string( line ) + ")\n"s + reason;

	// NOTE: Not using wxWidgets here because that would require linking with that even for projects that don't use GUI
#ifdef _WIN32
	std::string msg = "ERROR! The application will now close!\n\n"s + errorText + "\n\nDo you want to copy this message to your clipboard?"s;
	int b = MessageBoxA( nullptr, msg.c_str(), nullptr, MB_ICONERROR | MB_TASKMODAL | MB_YESNO | MB_DEFBUTTON1 );
	if ( b == IDYES )
	{
		if ( OpenClipboard( nullptr ) )
		{
			EmptyClipboard();
			HGLOBAL cpytxt = GlobalAlloc( GMEM_MOVEABLE, errorText.length() + 1 );
			char* strcpy = (char*)GlobalLock( cpytxt );
			memcpy( strcpy, errorText.c_str(), errorText.length() + 1 );
			GlobalUnlock( cpytxt );
			SetClipboardData( CF_TEXT, cpytxt );
			CloseClipboard();
		}
	}
#else
#pragma message("HEY! YOU MIGHT WANT TO IMPLEMENT THIS FOR YOUR PLATFORM!")
#endif
	exit( -1 );
}
