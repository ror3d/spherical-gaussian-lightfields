#include "opengl_helpers.h"

#include <glm/gtx/rotate_vector.hpp>

namespace opengl_helpers
{
	
	void drawFullScreenQuad()
	{
		GLboolean previous_depth_state;
		glGetBooleanv(GL_DEPTH_TEST, &previous_depth_state);
		glDisable(GL_DEPTH_TEST);
		static GLuint vertexArrayObject = 0;
		static int nofVertices = 6;
		// do this initialization first time the function is called... 
		if (vertexArrayObject == 0)
		{
			glGenVertexArrays(1, &vertexArrayObject);
			static const float positions[] = {
				-1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f,
				-1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f 
			};
			GLuint buffer = 0;
			glGenBuffers(1, &buffer);
			glBindBuffer(GL_ARRAY_BUFFER, buffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(positions), positions, GL_STATIC_DRAW);
			glBindVertexArray(vertexArrayObject);
			glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);
			glEnableVertexAttribArray(0);
		}
		glBindVertexArray(vertexArrayObject);
		glDrawArrays(GL_TRIANGLES, 0, nofVertices);
		if (previous_depth_state) glEnable(GL_DEPTH_TEST);
	}

	namespace texture
	{
		/////////////////////////////////////////////////////////////////////////////
		// Get a suitable format and type for an internal format.
		// NOTE: I would have expected that I could use:
		//       glGetInternalformativ(... GL_TEXTURE_IMAGE_FORMAT/TYPE ...)
		//       here, but while that works, it returns types that for some reason
		//       makes the texture unusable as a rendertarget (framebuffer attachment
		//       incomplete). Thus, we go with this annoying homemade mapping
		//		 instead.
		/////////////////////////////////////////////////////////////////////////////
		GLenum getDefaultType(GLenum internal_format)
		{
			switch (internal_format)
			{
			case GL_RGBA16F:
			case GL_RGB16F:
				return GL_HALF_FLOAT;
			case GL_DEPTH_COMPONENT32:
			case GL_DEPTH_COMPONENT32F:
			case GL_DEPTH_COMPONENT16:
			case GL_RGBA32F:
			case GL_RGB32F:
			case GL_RG32F:
			case GL_R32F:
			case GL_RED:
				return GL_FLOAT;
			case GL_RGBA8:
				return GL_UNSIGNED_BYTE;
			case GL_R32UI: 
			case GL_RG32UI: 
			case GL_RGBA32UI: 
				return GL_UNSIGNED_INT; 
			case GL_RG16UI:
				return GL_UNSIGNED_SHORT; 
			default:
				LOG_ERROR("Default type not implemented for internalformat: " << internal_format);
				return GL_NONE;
				break;
			}
		};
		GLenum getDefaultFormat(GLenum internal_format)
		{
			switch (internal_format)
			{
			case GL_DEPTH_COMPONENT32:
			case GL_DEPTH_COMPONENT32F:
			case GL_DEPTH_COMPONENT16:
				return GL_DEPTH_COMPONENT;
			case GL_RGBA32F:
			case GL_RGBA16F:
			case GL_RGBA8:
				return GL_RGBA;
			case GL_RGB32F:
			case GL_RGB16F:
			case GL_RGB8:
				return GL_RGB;
			case GL_RG32F:
				return GL_RG;
			case GL_RGBA32UI: 
				return GL_RGBA_INTEGER;
			case GL_RG16UI:
			case GL_RG32UI: 
				return GL_RG_INTEGER;
			case GL_RED:
			case GL_R32F:
				return GL_RED;
			case GL_R32UI: 
				return GL_RED_INTEGER; 
			default:
				LOG_ERROR("Default format not implemented for internalformat: " << internal_format);
				return GL_NONE;
				break;
			}
		};
		/////////////////////////////////////////////////////////////////////////////
		// Create an empty new 1D GL texture.
		/////////////////////////////////////////////////////////////////////////////
		uint32_t createTexture(int width, GLenum internal_format, void * data)
		{
			int temp;
			glGetIntegerv(GL_TEXTURE_BINDING_1D, &temp);
			uint32_t texture_id;
			glGenTextures(1, &texture_id);
			glBindTexture(GL_TEXTURE_1D, texture_id);
			int type = getDefaultType(internal_format);
			int format = getDefaultFormat(internal_format);
			glTexImage1D( GL_TEXTURE_1D, 0, internal_format, width, 0, format, type, data );
			glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
			glBindTexture(GL_TEXTURE_1D, temp);
			return texture_id;
		}
		/////////////////////////////////////////////////////////////////////////////
		// Create an empty new GL texture.
		// NOTE: I would have wanted to use glTextureStorage (Direct State Access)
		//       instead here, but doing so means the texture is IMMUTABLE, so I
		//       cannot resize it without deleting and recreating. This brings a
		//       number of problems, worst of which is that we would need to
		//       query and copy all sampler settings, so we'll do it the old-
		//       fashioned way.
		/////////////////////////////////////////////////////////////////////////////
		uint32_t createTexture(int width, int height, GLenum internal_format, void * data)
		{
			int temp;
			glGetIntegerv(GL_TEXTURE_BINDING_2D, &temp);
			uint32_t texture_id;
			glGenTextures(1, &texture_id);
			glBindTexture(GL_TEXTURE_2D, texture_id);
			int type = getDefaultType(internal_format);
			int format = getDefaultFormat(internal_format);
			glTexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, type, data);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
			glBindTexture(GL_TEXTURE_2D, temp);
			return texture_id;
		}
		/////////////////////////////////////////////////////////////////////////////
		// Delete a texture
		/////////////////////////////////////////////////////////////////////////////
		void deleteTexture(uint32_t id)
		{
			glDeleteTextures(1, &id);
		}
		/////////////////////////////////////////////////////////////////////////////
		// Resize the texture
		/////////////////////////////////////////////////////////////////////////////
		uint32_t resizeTexture(uint32_t id, int width, int height)
		{
			int temp;
			glGetIntegerv(GL_TEXTURE_BINDING_2D, &temp);
			glBindTexture(GL_TEXTURE_2D, id);
			int internal_format;
			glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT, &internal_format);
			int type = getDefaultType(internal_format);
			int format = getDefaultFormat(internal_format);
			glTexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, type, NULL);
			glBindTexture(GL_TEXTURE_2D, temp);
			return id;
		}
		///////////////////////////////////////////////////////////////////////
		// Lazy initialize and return a global dummy shadow map.
		// NOTE: Recent drivers will report an error if we have a
		//		 shadowMapSampler but do not bind a shadow map. This is
		//		 extremely annoying when the shadow map shall not be used, as
		//		 decided by a uniform. (e.g., a light with no shadow map). So,
		//		 we bind a dummy shadowmap.
		///////////////////////////////////////////////////////////////////////
		uint32_t g_dummy_shadowmap = 0;
		uint32_t getDummyShadowmap()
		{
			if (g_dummy_shadowmap == 0) {
				g_dummy_shadowmap = createTexture(1, 1, GL_DEPTH_COMPONENT32F);
				glTextureParameteri(g_dummy_shadowmap, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTextureParameteri(g_dummy_shadowmap, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTextureParameteri(g_dummy_shadowmap, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
				glTextureParameteri(g_dummy_shadowmap, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
			}
			return g_dummy_shadowmap;
		}

		/////////////////////////////////////////////////////////////////////////////
		// Helpers to get size of texture
		/////////////////////////////////////////////////////////////////////////////
		int getWidth(uint32_t id, int mip_level)
		{
			int w; 
			glGetTextureLevelParameteriv(id, mip_level, GL_TEXTURE_WIDTH, &w);
			return w; 
		}
		int getHeight(uint32_t id, int mip_level)
		{
			int h;
			glGetTextureLevelParameteriv(id, mip_level, GL_TEXTURE_HEIGHT, &h);
			return h;
		}


		/////////////////////////////////////////////////////////////////////////////
		// Generate mipmap with custom shader
		/////////////////////////////////////////////////////////////////////////////
		void generateMipmap(uint32_t texture_id, uint32_t program_object)
		{
			glPushAttrib(GL_VIEWPORT_BIT);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, texture_id);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			int write_width = getWidth(texture_id, 0) / 2;
			int write_height = getHeight(texture_id, 0) / 2; 
			int write_level = 1; 

			int internal_format;
			glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT, &internal_format);
			int type = getDefaultType(internal_format);
			int format = getDefaultFormat(internal_format);

			uint32_t fbo; 
			glCreateFramebuffers(1, &fbo);
			framebuffer::pushFramebuffer(fbo);
			GLenum drawbuffers[] = { GL_COLOR_ATTACHMENT0 };
			glNamedFramebufferDrawBuffers(fbo, 1, drawbuffers);
			
			glUseProgram(program_object);
			while (write_width >= 1 && write_height >= 1)
			{
				glUniform1i(0, write_level);
				glTexImage2D(GL_TEXTURE_2D, write_level, internal_format, write_width, write_height, 0, format, type, NULL);
				glNamedFramebufferTexture(fbo, GL_COLOR_ATTACHMENT0, texture_id, write_level);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, write_level - 1);
				framebuffer::checkFramebufferCompleteness(fbo);

				glViewport(0, 0, write_width, write_height);
				drawFullScreenQuad(); 

				write_level += 1; 
				write_width /= 2; 
				write_height /= 2; 
			}
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, write_level);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			framebuffer::popFramebuffer();
			glDeleteFramebuffers(1, &fbo);
			glUseProgram(0);
			glPopAttrib(); 
		}
	};

	namespace framebuffer
	{
		///////////////////////////////////////////////////////////////////////
		// Check framebuffer completeness
		///////////////////////////////////////////////////////////////////////
		bool checkFramebufferCompleteness(uint32_t id)
		{
			switch (glCheckNamedFramebufferStatus(id, GL_FRAMEBUFFER))
			{
			case GL_FRAMEBUFFER_COMPLETE: return true;
			case GL_FRAMEBUFFER_UNDEFINED: LOG_WARNING("Framebuffer incomplete: GL_FRAMEBUFFER_UNDEFINED."); return false;
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: LOG_WARNING("Framebuffer incomplete: GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT."); return false;
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: LOG_WARNING("Framebuffer incomplete: GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT."); return false;
			case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER: LOG_WARNING("Framebuffer incomplete: GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER."); return false;
			case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER: LOG_WARNING("Framebuffer incomplete: GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER."); return false;
			case GL_FRAMEBUFFER_UNSUPPORTED: LOG_WARNING("Framebuffer incomplete: GL_FRAMEBUFFER_UNSUPPORTED."); return false;
			case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE: LOG_WARNING("Framebuffer incomplete: GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE."); return false;
			case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS: LOG_WARNING("Framebuffer incomplete: GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS."); return false;
			default: LOG_WARNING("Framebuffer incomplete: <unknown>"); return false;
			}
		}
		///////////////////////////////////////////////////////////////////////
		// Framebuffer stack
		// It is very common that we want to switch to a new framebuffer,
		// render something, and then get back to the old framebuffer.
		// NOTE: If the stack is empty, we will assume that the previous
		//		 framebuffer is the default (0) framebuffer.
		// NOTE: I check both that the stack is always intact and framebuffer
		//		 completeness everytime we push/pop. IF this ever becomes
		//		 a performance issue, we can safely define both methods
		//		 NULL.
		///////////////////////////////////////////////////////////////////////
		std::vector<uint32_t> stack;
		void assertFramebufferStackIntact()
		{
			int draw_binding, read_binding;
			glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &draw_binding);
			glGetIntegerv(GL_READ_FRAMEBUFFER_BINDING, &read_binding);
			assert(draw_binding == read_binding);
			if (stack.size() == 0) assert(draw_binding == 0);
			else assert(static_cast<unsigned int>(draw_binding) == stack.back());
		}
		void pushFramebuffer(uint32_t id)
		{
			assertFramebufferStackIntact();
			stack.push_back(id);
			glBindFramebuffer(GL_FRAMEBUFFER, id);
		}
		uint32_t createTexture( int width, int height, GLenum internal_format )
		{
			int temp;
			glGetIntegerv( GL_TEXTURE_BINDING_2D, &temp );
			uint32_t texture_id;
			glGenTextures( 1, &texture_id );
			glBindTexture( GL_TEXTURE_2D, texture_id );
			int type = texture::getDefaultType( internal_format );
			int format = texture::getDefaultFormat( internal_format );
			glTexImage2D( GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, type, nullptr );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );
			glBindTexture( GL_TEXTURE_2D, temp );
			return texture_id;
		}
		uint32_t popFramebuffer()
		{
			assertFramebufferStackIntact();
			stack.pop_back();
			int popped_framebuffer = 0;
			if (stack.size() > 0) popped_framebuffer = stack.back();
			if (popped_framebuffer != 0) checkFramebufferCompleteness(popped_framebuffer);
			glBindFramebuffer(GL_FRAMEBUFFER, popped_framebuffer);
			return popped_framebuffer;
		}
		uint32_t currentFramebuffer()
		{
			assertFramebufferStackIntact();
			if (stack.size() == 0) return 0;
			return stack.back();
		}
		///////////////////////////////////////////////////////////////////////
		// Create a new framebuffer, including render targets. If there shall
		// be a depth buffer, it is first in the list.
		// NOTE: This function does not allow for stencil buffers, I have not
		//		 tested it for framebuffers without depth and it will not
		//		 work for framebuffers without rendertargets. So... It's
		//		 WIP.
		///////////////////////////////////////////////////////////////////////
		uint32_t createFramebuffer(int width, int height, uint32_t depth_id,
			std::vector<uint32_t> color_id)
		{
			uint32_t id;
			glCreateFramebuffers(1, &id);
			glNamedFramebufferParameteri(id, GL_FRAMEBUFFER_DEFAULT_WIDTH, width);
			glNamedFramebufferParameteri(id, GL_FRAMEBUFFER_DEFAULT_HEIGHT, height);
			bool has_depth_buffer = depth_id != 0;
			if (has_depth_buffer) { glNamedFramebufferTexture(id, GL_DEPTH_ATTACHMENT, depth_id, 0); }
			for (int i = 0; i < static_cast<int>(color_id.size()); i++) {
				glNamedFramebufferTexture(id, GL_COLOR_ATTACHMENT0 + i, color_id[i], 0);
			}
			GLenum drawbuffers[] = {
				GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3,
				GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5, GL_COLOR_ATTACHMENT6, GL_COLOR_ATTACHMENT7,
				GL_COLOR_ATTACHMENT8, GL_COLOR_ATTACHMENT9, GL_COLOR_ATTACHMENT10, GL_COLOR_ATTACHMENT11,
				GL_COLOR_ATTACHMENT12, GL_COLOR_ATTACHMENT13, GL_COLOR_ATTACHMENT14, GL_COLOR_ATTACHMENT15 };
			glNamedFramebufferDrawBuffers(id, uint32_t(color_id.size()), drawbuffers);
			checkFramebufferCompleteness(id);
			return id;
		}
		///////////////////////////////////////////////////////////////////////
		// Get a vector containing all USED framebuffer attachments
		// NOTE: We don't handle stencil attachments (and certainly not
		//		 combined depth and stencil attachments)
		///////////////////////////////////////////////////////////////////////
		std::vector<attached_texture> getAttachedTextures(uint32_t framebuffer)
		{
			std::vector<attached_texture> ret;
			int type, name;
			glGetNamedFramebufferAttachmentParameteriv(framebuffer, GL_DEPTH_ATTACHMENT, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &type);
			if (type != GL_NONE) {
				glGetNamedFramebufferAttachmentParameteriv(framebuffer, GL_DEPTH_ATTACHMENT, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, &name);
				attached_texture at = { GL_DEPTH_ATTACHMENT, static_cast<uint32_t>(name) };
				ret.push_back(at);
			}
			uint32_t max_color_attachments;
			glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, (GLint *)&max_color_attachments);
			for (uint32_t i = GL_COLOR_ATTACHMENT0; i < GL_COLOR_ATTACHMENT0 + max_color_attachments; i++)
			{
				glGetNamedFramebufferAttachmentParameteriv(framebuffer, i, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &type);
				if (type != GL_NONE) {
					glGetNamedFramebufferAttachmentParameteriv(framebuffer, i, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, &name);
					attached_texture at = { static_cast<uint32_t>(i), static_cast<uint32_t>(name) };
					ret.push_back(at);
				}
			}
			return ret;
		}
		///////////////////////////////////////////////////////////////////////
		// Delete a framebuffer, including render targets
		///////////////////////////////////////////////////////////////////////
		void deleteFramebuffer(uint32_t framebuffer, bool delete_rendertargets)
		{
			if (delete_rendertargets) {
				for (auto at : getAttachedTextures(framebuffer)) texture::deleteTexture(at.texture_id);
			}
			glDeleteFramebuffers(1, &framebuffer);
		}
		///////////////////////////////////////////////////////////////////////
		// Change the size of all render targets. Deletes and creates new
		// textures for all rendertargets.
		///////////////////////////////////////////////////////////////////////
		void resizeFramebuffer(uint32_t framebuffer, int width, int height)
		{
			glNamedFramebufferParameteri(framebuffer, GL_FRAMEBUFFER_DEFAULT_WIDTH, width);
			glNamedFramebufferParameteri(framebuffer, GL_FRAMEBUFFER_DEFAULT_HEIGHT, height);
			for (auto at : getAttachedTextures(framebuffer)) {
				uint32_t new_texture = texture::resizeTexture(at.texture_id, width, height);
				glNamedFramebufferTexture(framebuffer, at.attachment, new_texture, 0);
			}
		}
		///////////////////////////////////////////////////////////////////////
		// Get the width and height of the framebuffer rendertargets
		// NOTE: Assuming that the DEFAULT_WIDTH/HEIGHT value has been set
		//	     explicitly.
		///////////////////////////////////////////////////////////////////////
		int getFramebufferDefaultWidth(const uint32_t framebuffer)
		{
			int width;
			glGetNamedFramebufferParameteriv(framebuffer, GL_FRAMEBUFFER_DEFAULT_WIDTH, &width);
			return width;
		}
		int getFramebufferDefaultHeight(const uint32_t framebuffer)
		{
			int height;
			glGetNamedFramebufferParameteriv(framebuffer, GL_FRAMEBUFFER_DEFAULT_HEIGHT, &height);
			return height;
		}
		///////////////////////////////////////////////////////////////////////
		// Bind the frambuffers depth attachment as an input texture
		///////////////////////////////////////////////////////////////////////
		void bindFramebufferDepthBuffer(uint32_t framebuffer, uint32_t base_texture_unit)
		{
			int type, name;
			glGetNamedFramebufferAttachmentParameteriv(framebuffer, GL_DEPTH_ATTACHMENT, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &type);
			if (type != GL_NONE) {
				glGetNamedFramebufferAttachmentParameteriv(framebuffer, GL_DEPTH_ATTACHMENT, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, &name);
				glBindTextures(base_texture_unit, 1, (uint32_t *)&name);
			}
		}
		///////////////////////////////////////////////////////////////////////
		// Bind ALL the frambuffers color attachments as input textures
		// NOTE: It seems the base texture unit given to glBindTextures is
		//       NOT GL_TEXTURE0 + ... but starts from 0.
		///////////////////////////////////////////////////////////////////////
		size_t bindFramebufferColorBuffers(uint32_t framebuffer, uint32_t base_texture_unit)
		{
			std::vector<uint32_t> textures;
			int max_color_attachments;
			glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &max_color_attachments);
			for (int i = GL_COLOR_ATTACHMENT0; i < GL_COLOR_ATTACHMENT0 + max_color_attachments; i++)
			{
				int type, name;
				glGetNamedFramebufferAttachmentParameteriv(framebuffer, i, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &type);
				if (type != GL_NONE) {
					glGetNamedFramebufferAttachmentParameteriv(framebuffer, i, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME, &name);
					textures.push_back(name);
				}
			}
			glBindTextures(base_texture_unit, uint32_t(textures.size()), &textures[0]);
			return textures.size();
		};
	};

namespace geometry
{
	const int DISK_SEGMENTS = 16;
	using namespace glm;

	static vec3 perpendicular( vec3 v )
	{
		return v.z < v.x ? vec3( v.y, -v.x, 0 ) : vec3( 0, v.z, v.y );
	}

	void line( vec3 start, vec3 end, vec3 color )
	{
		glBegin( GL_LINES );
		glColor3fv( &color.r );
		glVertex3fv( &start.x );
		glVertex3fv( &end.x );
		glEnd();
	}

	void disk( vec3 position, float radius, vec3 normal, vec3 color, bool fill, vec3 origin, float arc )
	{
		normal = normalize( normal );
		if ( origin == vec3{} )
		{
			origin = perpendicular( normal );
		}

		origin = normalize( cross( normal, cross( origin, normal ) ) );

		origin = glm::rotate( origin, -arc / 2, normal );

		if ( fill )
		{
			glBegin( GL_TRIANGLE_FAN );
			glColor3fv( &color.r );
			glVertex3fv( &position.x );
		}
		else
		{
			glBegin( GL_LINE_STRIP );
			glColor3fv( &color.r );
		}

		vec3 p = position + origin * radius;
		glVertex3fv( &p.x );

		vec3 current = origin;
		for(int i = 0; i < DISK_SEGMENTS; i++)
		{
			current = rotate( current, arc / DISK_SEGMENTS, normal );
			p = position + current * radius;
			glVertex3fv(&p.x);
		}
		glEnd();
	}

	void arrow( vec3 start, vec3 end, vec3 color )
	{
		vec3 dir = end - start;
		vec3 perp = normalize( perpendicular( dir ) );
		float l = length( dir );
		dir = normalize( dir );
		vec3 pointy = (perp * 0.075f - dir * 0.15f) * l;

		glBegin( GL_LINES );
		glColor3fv( &color.r );
		glVertex3fv( &start.x );
		glVertex3fv( &end.x );

		for ( size_t i = 0; i < 4; ++i )
		{
			vec3 p = end + pointy;
			glVertex3fv( &end.x );
			glVertex3fv( &p.x );
			pointy = rotate( pointy, pi<float>() / 2, dir );
		}
		glEnd();
	}

	void sphere( vec3 position, float radius, vec3 color )
	{
		// create 12 vertices of a icosahedron
		const float t = (1.0 + sqrtf( 5.0 )) / 2.0;
		
		vec3 vertices[12] =
		{
			{ -1, t, 0 },
			{ 1, t, 0 },
			{ -1, -t, 0 },
			{ 1, -t, 0 },

			{ 0, -1, t },
			{ 0, 1, t },
			{ 0, -1, -t },
			{ 0, 1, -t },

			{ t, 0, -1 },
			{ t, 0, 1 },
			{ -t, 0, -1 },
			{ -t, 0, 1 }
		};

		size_t indices[60] =
		{
			// 5 faces around point 0
			0, 11, 5,
			0, 5, 1,
			0, 1, 7,
			0, 7, 10,
			0, 10, 11,

			// 5 adjacent faces	
			1, 5, 9,
			5, 11, 4,
			11, 10, 2,
			10, 7, 6,
			7, 1, 8,

			// 5 faces around point 3	
			3, 9, 4,
			3, 4, 2,
			3, 2, 6,
			3, 6, 8,
			3, 8, 9,

			// 5 adjacent faces	
			4, 9, 5,
			2, 4, 11,
			6, 2, 10,
			8, 6, 7,
			9, 8, 1
		};

		glBegin( GL_TRIANGLES );
		glColor3fv( &color.x );
		
		for ( size_t i = 0; i < 60; ++i )
		{
			vec3 v = vertices[indices[i]];
			v *= radius;
			v += position;
			glVertex3fv( &v.x );
		}

		glEnd();
	}



}
}
