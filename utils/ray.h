#pragma once
#include "glm/vec3.hpp"
#include <float.h>

namespace chag {
class ray {
 public:
	using vec3 = glm::vec3;
	ray(void) : mint(0.0f), maxt(FLT_MAX){};
	ray(const vec3 &origin, const vec3 direction, float start = 0.0f, float end = FLT_MAX)
	    : o(origin), d(direction), mint(start), maxt(end){};
	~ray(void);
	vec3 o, d;
	mutable float mint, maxt;
	vec3 operator()(float t) const { return o + d * t; }
	// In pbrt we have:
	// float time;	// For motion blur
	// int depth;	// Number of bounces
};
}
