#include <GL/glew.h>
#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <utility>
#include "ScopeTimer.h"
#include <GL/freeglut.h>
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm_extensions.h"

using namespace std;

///////////////////////////////////////////////////////////////////////////////
// A single range entry in the plot
///////////////////////////////////////////////////////////////////////////////
struct Range
{
	float cpu_start, cpu_end;
	float opengl_start, opengl_end;
#ifdef CHAG_ENABLE_CUDA
	float cuda_start, cuda_end;
#endif
	Range() : cpu_start(-1.0f), cpu_end(-1.0f), opengl_start(-1.0f), opengl_end(-1.0f)
#ifdef CHAG_ENABLE_CUDA
	        , cuda_start(-1.0f), cuda_end(-1.0f)
#endif
	{}
};

///////////////////////////////////////////////////////////////////////////////
// A node in the Trace Tree
///////////////////////////////////////////////////////////////////////////////
struct TraceTreeNode
{
	std::string m_name;
	std::vector<Range> m_ranges;
	TraceTreeNode *m_parent;
	std::map<std::string, int> m_children_ids;
	std::vector<TraceTreeNode *> m_children;
	TraceTreeNode(std::string name, TraceTreeNode * parent) : m_name(name), m_parent(parent) {};
	void getDimensions(int &chars_high, int &chars_wide) {
		chars_high += 1;
		chars_wide = std::max(chars_wide, int(m_name.length()));
		for (auto e : m_children) e->getDimensions(chars_high, chars_wide);
	}
	~TraceTreeNode()
	{
		for (auto c : m_children) {
			delete c;
		}
	}
};

///////////////////////////////////////////////////////////////////////////////
// The trace is parsed into a tree structure for easy access
///////////////////////////////////////////////////////////////////////////////
struct TraceTree
{
	TraceTreeNode *m_current;
	std::string m_name;

	TraceTree(std::string name) : m_name(name) {
		m_current = new TraceTreeNode(name, NULL);
	};
	~TraceTree() {
		delete m_current;
	}

	void enterScope(const std::string &name) {
		// Check if the suggested node exists, otherwise create it
		if (m_current->m_children_ids.count(name) == 0) {
			m_current->m_children_ids[name] = int(m_current->m_children.size());
			m_current->m_children.push_back(new TraceTreeNode(name, m_current));
		}
		m_current = m_current->m_children[m_current->m_children_ids[name]];
	}

	void exitScope() {
		m_current = m_current->m_parent;
	}

	void getDimensions(int &chars_high, int &chars_wide) {
		m_current->getDimensions(chars_high, chars_wide);
	}
};


///////////////////////////////////////////////////////////////////////
// Visualize profiler trace
///////////////////////////////////////////////////////////////////////
void profilerView(int width, int height)
{
	///////////////////////////////////////////////////////////////////
	// Set up GL stuff
	///////////////////////////////////////////////////////////////////
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_TEXTURE_2D);


	///////////////////////////////////////////////////////////////////
	// Helper lambda. Immediate mode OpenGL rules!
	///////////////////////////////////////////////////////////////////
	auto drawBox = [](float x0, float y0, float x1, float y1) {
		glBegin(GL_QUADS);
		glVertex2f(x0, y0);
		glVertex2f(x1, y0);
		glVertex2f(x1, y1);
		glVertex2f(x0, y1);
		glEnd();
	};

	///////////////////////////////////////////////////////////////////
	// Run through the ScopeTimer traces and build trees out of 'em
	///////////////////////////////////////////////////////////////////
	std::map<std::string, Trace> &traces = GET_TRACES();
	std::vector<TraceTree *> trace_trees;
	for (auto entry : traces)
	{
		std::string trace_name = entry.first;
		std::vector<TraceEntry> &trace = entry.second.trace;

		// Trace will start and end with a sync
		double cpu_start_time = trace[0].m_cpu_time;
		double cpu_end_time = trace.back().m_cpu_time;
		double opengl_start_time = trace[0].m_opengl_time;
		double opengl_end_time = trace.back().m_opengl_time;
#ifdef CHAG_ENABLE_CUDA
		double cuda_start_time = trace[0].m_cuda_time;
		double cuda_end_time = trace.back().m_cuda_time;
#endif

		std::vector<double> start_time_stack_cpu;
		std::vector<double> start_time_stack_opengl;
#ifdef CHAG_ENABLE_CUDA
		std::vector<double> start_time_stack_cuda;
#endif

		TraceTree *trace_tree = new TraceTree(trace_name);
		for (auto entry : trace)
		{
			// SYNC entries do not appear in list 
			if (entry.m_type != TraceEntry::ID_SYNC)
			{
				if (entry.m_is_scope_start) {
					trace_tree->enterScope(entry.m_name);
					start_time_stack_cpu.push_back(entry.m_cpu_time);
					if (entry.m_type == TraceEntry::ID_GL) {
						start_time_stack_opengl.push_back(entry.m_opengl_time);
					}
#ifdef CHAG_ENABLE_CUDA
					if (entry.m_type == TraceEntry::ID_CUDA) {
						start_time_stack_cuda.push_back(entry.m_cuda_time);
					}
#endif
				}
				else {
					Range range;
					range.cpu_start = float(start_time_stack_cpu.back() / (cpu_end_time - cpu_start_time));
					range.cpu_end = float(entry.m_cpu_time / (cpu_end_time - cpu_start_time));
					start_time_stack_cpu.pop_back();
					if (entry.m_type == TraceEntry::ID_GL) {
						range.opengl_start = float(start_time_stack_opengl.back() / (opengl_end_time - opengl_start_time));
						range.opengl_end = float(entry.m_opengl_time / (opengl_end_time - opengl_start_time));
						start_time_stack_opengl.pop_back();
					}
#ifdef CHAG_ENABLE_CUDA
					if (entry.m_type == TraceEntry::ID_CUDA) {
						range.cuda_start = float(start_time_stack_cuda.back() / (cuda_end_time - cuda_start_time));
						range.cuda_end = float(entry.m_cuda_time / (cuda_end_time - cuda_start_time));
						start_time_stack_cuda.pop_back();
					}
#endif
					trace_tree->m_current->m_ranges.push_back(range);
					trace_tree->exitScope();
				}
			}
		}
		trace_trees.push_back(trace_tree);
	}

	///////////////////////////////////////////////////////////////////////
	// Render Node names
	///////////////////////////////////////////////////////////////////////
	const auto ortho = glm::make_ortho2d(0.0f, float(width), 0.0f, float(height));
	glMatrixPushEXT(GL_PROJECTION);
	glMatrixLoadIdentityEXT(GL_PROJECTION);
	glMatrixLoadfEXT(GL_PROJECTION, glm::value_ptr(ortho));
	glMatrixPushEXT(GL_MODELVIEW);
	glMatrixLoadIdentityEXT(GL_MODELVIEW);

	const int line_size = 19;
	const int char_width = 9;
	int chars_high = 0, chars_wide = 0;
	for (auto tree : trace_trees) {
		tree->getDimensions(chars_high, chars_wide);
	}
	chars_wide += 6; // Arbitrary. Should be nof levels + x

	{ // Traverse tree and render node names
		int current_y = (chars_high - 1) * line_size;
		for (auto trace_tree : trace_trees) {
			// Create a flat list, but keep the level with each name
			std::vector<std::pair<TraceTreeNode *, int>> stack;
			stack.push_back(make_pair(trace_tree->m_current, 0));
			int counter = 0;
			while (stack.size() > 0) {
				pair<TraceTreeNode *, int> current = stack.back();
				stack.pop_back();

				glm::vec3 text_color, bg_color;
				if (current.second == 0) {
					text_color = glm::vec3(0.9f, 0.9f, 0.9f);
					bg_color = glm::vec3(0.1f, 0.1f, 0.1f);
				}
				else {
					text_color = glm::vec3(0.1f, 0.1f, 0.1f);
					if (current.second % 2 == 0) bg_color = glm::vec3(0.9f, 0.9f, 0.9f);
					else bg_color = glm::vec3(0.7f, 0.7f, 0.7f);
					if (counter % 2 == 0) bg_color = 0.95f * bg_color;
				}
				counter += 1;

				glColor3f(bg_color.x, bg_color.y, bg_color.z);
				drawBox(0.0f, float(current_y), float(chars_wide * char_width), float(current_y + line_size));

				glColor3f(text_color.x, text_color.y, text_color.z);
				glRasterPos2i(current.second * char_width, current_y + 2);
				current_y -= line_size;
				glutBitmapString(GLUT_BITMAP_9_BY_15, (unsigned char *)(current.first->m_name.c_str()));

				for (int i = int(current.first->m_children.size()) - 1; i >= 0; i--) {
					stack.push_back(make_pair(current.first->m_children[i], current.second + 1));
				}
			}
		}
	}

	int num_counters = 2u;
#ifdef CHAG_ENABLE_CUDA
	++num_counters;
#endif

	///////////////////////////////////////////////////////////////////////
	// Render Profile
	///////////////////////////////////////////////////////////////////////
	{ // Traverse tree and render profile
		int current_y = (chars_high - 1) * line_size;
		int start_x = chars_wide * char_width;
		int profile_width = width - start_x;
		for (auto trace_tree : trace_trees) {
			// Create a flat list, but keep the level with each name
			std::vector<std::pair<TraceTreeNode *, int>> stack;
			stack.push_back(make_pair(trace_tree->m_current, 0));
			while (stack.size() > 0) {
				pair<TraceTreeNode *, int> current = stack.back();
				TraceTreeNode & node = *current.first;
				stack.pop_back();

				glm::vec3 bg_color;
				if (current.second == 0) { bg_color = glm::vec3(0.1f, 0.1f, 0.1f); }
				else {
					if (current.second % 2 == 0) bg_color = glm::vec3(0.4f, 0.4f, 0.4f);
					else bg_color = glm::vec3(0.3f, 0.3f, 0.3f);
				}
				glColor3f(bg_color.x * 0.5f, bg_color.y * 0.5f, bg_color.z * 0.5f);
				drawBox(float(start_x), float(current_y), float(start_x + profile_width), float(current_y + line_size));

				for (auto range : node.m_ranges)
				{
					// Show active region (may overlap with other ranges)
					glColor3f(bg_color.x, bg_color.y, bg_color.z);
					float min_start = range.cpu_start;
					float max_end = range.cpu_end;
					if (range.opengl_start != -1.0f) min_start = min(min_start, range.opengl_start);
#ifdef CHAG_ENABLE_CUDA
					if (range.cuda_start != -1.0f) min_start = min(min_start, range.cuda_start);
#endif
					if (range.opengl_end != -1.0f) max_end = max(max_end, range.opengl_end);
#ifdef CHAG_ENABLE_CUDA
					if (range.cuda_end != -1.0f) max_end = max(max_end, range.cuda_end);
#endif
					drawBox(float(start_x + min_start * profile_width), float(current_y),
							float(start_x + max_end * profile_width), float(current_y + line_size));
					// CPU
					glColor3f(0.8f, 0.4f, 0.4f);
					drawBox(float(start_x + range.cpu_start * profile_width), float(current_y + (num_counters - 1) * line_size / num_counters),
							float(start_x + range.cpu_end * profile_width), float(current_y + line_size));
					// OpenGL
					glColor3f(0.4f, 0.8f, 0.4f);
					drawBox(float(start_x + range.opengl_start * profile_width), float(current_y + (num_counters - 2) * line_size / num_counters),
							float(start_x + range.opengl_end * profile_width), float(current_y + (num_counters - 1) * line_size / num_counters));
#ifdef CHAG_ENABLE_CUDA
					// CUDA
					glColor3f(0.4f, 0.4f, 0.8f);
					drawBox(float(start_x + range.cuda_start * profile_width), float(current_y),
							float(start_x + range.cuda_end * profile_width), float(current_y + (num_counters - 2) * line_size / num_counters));
#endif
				}
				current_y -= line_size;
				for (int i = int(current.first->m_children.size()) - 1; i >= 0; i--) {
					stack.push_back(make_pair(current.first->m_children[i], current.second + 1));
				}
			}
		}
	}

	///////////////////////////////////////////////////////////////////////
	// Delete trace trees
	///////////////////////////////////////////////////////////////////////
	for (auto e : trace_trees) delete e;

	///////////////////////////////////////////////////////////////////////
	// OpenGL cleanup
	///////////////////////////////////////////////////////////////////////
	glMatrixPopEXT(GL_MODELVIEW);
	glMatrixPopEXT(GL_PROJECTION);
	glPopAttrib();
}
