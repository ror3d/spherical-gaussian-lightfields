// Simple class to contain GLSL shaders/programs

#ifndef GLSL_PROGRAM_H
#define GLSL_PROGRAM_H

#include <GL/glew.h>
#include <stdio.h>
#include <string>
#include "glm/glm.hpp"

class GLSLProgramObject
{
public:
	using vec2 = glm::vec2;
	using vec3 = glm::vec3;
	using vec4 = glm::vec4;
	using ivec2 = glm::ivec2;
	using ivec3 = glm::ivec3;
	using ivec4 = glm::ivec4;
	using mat2 = glm::mat2;
	using mat3 = glm::mat3;
	using mat4 = glm::mat4;
	// construct program from strings
	GLSLProgramObject() {};
	GLSLProgramObject(const char *vsource, const char *fsource);
	GLSLProgramObject(const char *vsource, const char *gsource, const char *fsource,
	                  GLenum gsInput = GL_POINTS, GLenum gsOutput = GL_TRIANGLE_STRIP);
	~GLSLProgramObject();

	void destroy();

	void enable();
	void disable();

	/*
	void setUniform1f(const GLchar *name, GLfloat x);
	void setUniform1i(const GLchar *name, GLint x);
	void setUniform2f(const GLchar *name, GLfloat x, GLfloat y);
	void setUniform3f(const char *name, float x, float y, float z);
	void setUniform4f(const char *name, float x, float y, float z, float w);
	*/
	//void setUniformfv(const GLchar *name, GLfloat *v, int elementSize, int count=1);
	//void setUniformMatrix4fv(const GLchar *name, GLfloat *m, bool transpose);

	int getLocation(const char *name);
	// Set one uniform
	void setUniform(const char *name, const float &value);
	void setUniform(const char *name, const vec2 &value);
	void setUniform(const char *name, const vec3 &value);
	void setUniform(const char *name, const vec4 &value);
	void setUniform(const char *name, const int &value);
	void setUniform(const char *name, const ivec2 &value);
	void setUniform(const char *name, const ivec3 &value);
	void setUniform(const char *name, const ivec4 &value);
	void setUniform(const char *name, const unsigned int &value);
	void setUniform(const char *name, const mat4 &value);
	void setUniform(const char *name, const mat3 &value);
	void setUniform(const char *name, const mat2 &value);
	// Set uniform array
	void setUniform(const char *name, const float *value, int count);
	void setUniform(const char *name, const vec2 *value, int count);
	void setUniform(const char *name, const vec3 *value, int count);
	void setUniform(const char *name, const vec4 *value, int count);
	void setUniform(const char *name, const int *value, int count);
	void setUniform(const char *name, const ivec2 *value, int count);
	void setUniform(const char *name, const ivec3 *value, int count);
	void setUniform(const char *name, const ivec4 *value, int count);
	void setUniform(const char *name, const unsigned int *value, int count);
	void setUniform(const char *name, const mat4 *value, int count);

	void bindTexture(const char *name, GLuint tex, GLenum target, GLint unit, GLuint sampler = 0u);

	inline GLuint getProgId() { return mProg; }
	GLuint compileShader(const char *source, GLenum shaderType);
	GLuint compileProgram(const char *csource);
	GLuint compileProgram(const char *vsource, const char *gsource, const char *fsource,
	                      GLenum gsInput = GL_POINTS, GLenum gsOutput = GL_TRIANGLE_STRIP,
	                      unsigned int verticesOut = 3);
private:
	GLuint mProg;

	static std::string getCompilerLogs(GLuint shader);
	static std::string getLinkLogs(GLuint program);
};

#endif
