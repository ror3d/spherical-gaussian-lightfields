#ifndef _chag_Orientation_h
#define _chag_Orientation_h

#include "glm/glm.hpp"
#ifndef NO_OPENGL_OR_CUDA
#include <GL/glew.h>
#endif
#include <fstream>



namespace chag
{
class Orientation
{
public:
	using mat3 = glm::mat3;
	using mat4 = glm::mat4;
	using vec3 = glm::vec3;
	mat3 R;
	vec3   pos;
	vec3   scale;

	Orientation() { R = mat3(1.0f); pos = vec3(0.0f); scale = vec3(1.0f); };
	Orientation(vec3 pos, vec3 dir, vec3 up);
	void lookAt(vec3 pos, vec3 center, vec3 up);
	void lookAt(vec3 pos, vec3 center);

	mat4 get_MV() const;
	mat4 get_MV_inv() const;
	void set_as_modelview();

	void roll(float angle);
	void yaw(float angle);
	void pitch(float angle);
};
typedef Orientation orientation;

std::ostream& operator << (std::ostream& os, const chag::Orientation& o);
std::istream& operator >> (std::istream& is, chag::Orientation& o);

} // namespace chag

#endif // _chag_Orientation_h
