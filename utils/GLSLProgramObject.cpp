#include <stdlib.h>
#include <iostream>
using namespace std;
#include "GLSLProgramObject.h"
#include "Log.h"
#include <assert.h>
#include "glm/gtc/type_ptr.hpp"

GLSLProgramObject::GLSLProgramObject(const char *vsource, const char *fsource)
{
	mProg = compileProgram(vsource, 0, fsource);
}

GLSLProgramObject::GLSLProgramObject(const char *vsource, const char *gsource, const char *fsource,
                                     GLenum gsInput, GLenum gsOutput)
{
	mProg = compileProgram(vsource, gsource, fsource, gsInput, gsOutput);
}

GLSLProgramObject::~GLSLProgramObject()
{
	destroy();
}

void GLSLProgramObject::destroy()
{
	if (mProg) {
		glDeleteProgram(mProg);
		mProg = 0u;
	}
}

void
GLSLProgramObject::enable()
{
	glUseProgram(mProg);
}

void
GLSLProgramObject::disable()
{
	glUseProgram(0);
}

int GLSLProgramObject::getLocation(const char *name){
#if _DEBUG
	if(glGetUniformLocation(mProg, name) < 0){
		//LOG_WARNING("Error setting parameter " << name);
	}
#endif
	return glGetUniformLocation(mProg, name);
}

///////////////////////////////////////////////////////////////////////////////
// Set one uniform
///////////////////////////////////////////////////////////////////////////////
void GLSLProgramObject::setUniform(const char *name, const float &value){
	glUniform1f(getLocation(name), value);
}
void GLSLProgramObject::setUniform(const char *name, const glm::vec2 &value){
	glUniform2f(getLocation(name), value.x, value.y);
}
void GLSLProgramObject::setUniform(const char *name, const glm::vec3 &value){
	glUniform3f(getLocation(name), value.x, value.y, value.z);
}
void GLSLProgramObject::setUniform(const char *name, const glm::vec4 &value){
	glUniform4f(getLocation(name), value.x, value.y, value.z, value.w);
}
void GLSLProgramObject::setUniform(const char *name, const int &value){
	glUniform1i(getLocation(name), value);
}
void GLSLProgramObject::setUniform(const char *name, const glm::ivec2 &value){
	glUniform2i(getLocation(name), value.x, value.y);
}
void GLSLProgramObject::setUniform(const char *name, const glm::ivec3 &value){
	glUniform3i(getLocation(name), value.x, value.y, value.z);
}
void GLSLProgramObject::setUniform(const char *name, const glm::ivec4 &value){
	glUniform4i(getLocation(name), value.x, value.y, value.z, value.w);
}
void GLSLProgramObject::setUniform(const char *name, const unsigned int &value){
	glUniform1ui(getLocation(name), value);
}
void GLSLProgramObject::setUniform(const char *name, const glm::mat4 &value){
	glUniformMatrix4fv(getLocation(name), 1, false, glm::value_ptr(value));
}
void GLSLProgramObject::setUniform(const char *name, const glm::mat3 &value){
	glUniformMatrix3fv(getLocation(name), 1, false, glm::value_ptr(value));
}
void GLSLProgramObject::setUniform(const char *name, const glm::mat2 &value){
	glUniformMatrix2fv(getLocation(name), 1, false, glm::value_ptr(value));
}

///////////////////////////////////////////////////////////////////////////////
// Set uniform array
///////////////////////////////////////////////////////////////////////////////
void GLSLProgramObject::setUniform(const char *name, const float *value, int count){
	glUniform1fv(getLocation(name), count, value);
}
void GLSLProgramObject::setUniform(const char *name, const glm::vec2 *value, int count){
	glUniform2fv(getLocation(name), count, &(value->x));
}
void GLSLProgramObject::setUniform(const char *name, const glm::vec3 *value, int count){
	glUniform3fv(getLocation(name), count, &(value->x));
}
void GLSLProgramObject::setUniform(const char *name, const glm::vec4 *value, int count){
	glUniform4fv(getLocation(name), count, &(value->x));
}
void GLSLProgramObject::setUniform(const char *name, const int *value, int count){
	glUniform1iv(getLocation(name), count, value);
}
void GLSLProgramObject::setUniform(const char *name, const glm::ivec2 *value, int count){
	glUniform2iv(getLocation(name), count, &(value->x));
}
void GLSLProgramObject::setUniform(const char *name, const glm::ivec3 *value, int count){
	glUniform3iv(getLocation(name), count, &(value->x));
}
void GLSLProgramObject::setUniform(const char *name, const glm::ivec4 *value, int count){
	glUniform4iv(getLocation(name), count, &(value->x));
}
void GLSLProgramObject::setUniform(const char *name, const glm::mat4 *value, int count){
	glUniformMatrix4fv(getLocation(name), count, false, glm::value_ptr(*value));
}

void GLSLProgramObject::bindTexture(const char *name, GLuint tex, GLenum target, GLint unit, GLuint sampler)
{
	GLint loc = getLocation(name);
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(target, tex);
	glUseProgram(mProg);
	glUniform1i(loc, unit);
	if (sampler != 0u)
		glBindSampler(unit, sampler);
	glActiveTexture(GL_TEXTURE0);
}


GLuint GLSLProgramObject::compileShader(const char *source, GLenum shaderType)
{
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &source, 0);
	glCompileShader(shader);
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if(status == GL_FALSE){
		std::string log = getCompilerLogs(shader);
		std::string shaderTypeName = std::to_string(shaderType) + " [unsupported]";
		if(shaderType == GL_VERTEX_SHADER) shaderTypeName = "vertex";
		if(shaderType == GL_FRAGMENT_SHADER) shaderTypeName = "fragment";
		if(shaderType == GL_GEOMETRY_SHADER) shaderTypeName = "geometry";
		if(shaderType == GL_COMPUTE_SHADER) shaderTypeName = "compute";

		LOG_WARNING(shaderTypeName << " shader failed to compile:" << std::endl << log);
	}
	return shader;
}

GLuint GLSLProgramObject::compileProgram(const char *vsource, const char *gsource, const char *fsource,
                                         GLenum gsInput, GLenum gsOutput, unsigned int verticesOut)
{
	GLuint program = glCreateProgram();
	GLuint vshader = compileShader(vsource, GL_VERTEX_SHADER);
	glAttachShader(program, vshader);
	glDeleteShader(vshader);
	GLuint fshader = compileShader(fsource, GL_FRAGMENT_SHADER);
	glAttachShader(program, fshader);
	glDeleteShader(fshader);
	if (gsource) {
		GLuint geomShader = compileShader(gsource, GL_GEOMETRY_SHADER_ARB);
		glAttachShader(program, geomShader);
		glDeleteShader(geomShader);

		glProgramParameteriEXT(program, GL_GEOMETRY_INPUT_TYPE_EXT, gsInput);
		glProgramParameteriEXT(program, GL_GEOMETRY_OUTPUT_TYPE_EXT, gsOutput);
		glProgramParameteriEXT(program, GL_GEOMETRY_VERTICES_OUT_EXT, verticesOut);
	}
	glLinkProgram(program);

	// check if program linked
	GLint success = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		LOG_ERROR("Failed to link program:" << std::endl << getLinkLogs(program));
		glDeleteProgram(program);
		program = 0;
		assert(false);
	}

	mProg = program;
	return program;
}

GLuint GLSLProgramObject::compileProgram(const char *csource)
{
	GLuint program = glCreateProgram();
	GLuint cshader = compileShader(csource, GL_COMPUTE_SHADER);
	glAttachShader(program, cshader);
	glDeleteShader(cshader);
	glLinkProgram(program);

	// check if program linked
	GLint success = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		LOG_ERROR("Failed to link program:" << std::endl << getLinkLogs(program));
		glDeleteProgram(program);
		program = 0;
		assert(false);
	}

	mProg = program;
	return program;
}

std::string GLSLProgramObject::getCompilerLogs(GLuint shader)
{
	// TODO with C++17, use `char* std::string::data()` instead of C-array
	int loglength;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &loglength);
	char *log = new char[loglength];
	glGetShaderInfoLog(shader, loglength, nullptr, log);
	std::string logs(log);
	delete[] log;

	return logs;
}

std::string GLSLProgramObject::getLinkLogs(GLuint program)
{
	// TODO with C++17, use `char* std::string::data()` instead of C-array
	int loglength;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &loglength);
	char *log = new char[loglength];
	glGetProgramInfoLog(program, loglength, nullptr, log);
	std::string logs(log);
	delete[] log;

	return logs;
}
