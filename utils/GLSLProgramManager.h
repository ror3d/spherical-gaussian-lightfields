#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include "GLSLProgramObject.h"

class GLSLProgramManager
{
private:
	std::vector<std::string> m_fullSource;
	std::string extractShader(std::string shaderName, const std::string& prepend = std::string());
	std::string m_filename;

	class LoadedProgram {
	public:
		std::string vertexShader;
		std::string geometryShader;
		std::string fragmentShader;
		std::string computeShader;
		GLenum geomIn;
		GLenum geomOut;
		unsigned int verticesIn;
		GLSLProgramObject *programObject;
		std::string vertexPrepend;
		std::string geometryPrepend;
		std::string fragmentPrepend;
		std::string computePrepend;
	};
	std::vector<LoadedProgram> m_loadedPrograms;

public:
	GLSLProgramManager(std::string filename);
	void setFile(std::string filename);
	~GLSLProgramManager(void);
	std::map<std::string, std::string> m_defines;
	void compileProgram(const std::string &vertex_shader,
	                    const std::string &fragment_shader,
	                    GLSLProgramObject *program,
	                    const std::string &vertex_prepend = std::string(),
	                    const std::string &fragment_prepend = std::string());
	void compileProgram(const std::string &compute_shader,
	                    GLSLProgramObject *program);
	void compileProgram(const std::string &vertex_shader,
	                    const std::string &geometry_shader,
	                    const std::string &fragment_shader,
	                    GLenum gsInput,
	                    GLenum gsOutput,
	                    unsigned int verticesOut,
	                    GLSLProgramObject *program);
	void reload();
	GLSLProgramObject * getProgramObject(const std::string &compute_shader);
	GLSLProgramObject * getProgramObject(const std::string &vertex_shader,
	                                     const std::string &fragment_shader);
	GLSLProgramObject * getProgramObject(const std::string &vertex_shader,
	                                     const std::string &fragment_shader,
	                                     const std::string &vertex_prepend,
	                                     const std::string &fragment_prepend);
	GLSLProgramObject * getProgramObject(const std::string &vertex_shader,
	                                     const std::string &geometry_shader,
	                                     const std::string &fragment_shader,
	                                     GLenum gsInput,
	                                     GLenum gsOutput,
	                                     unsigned int verticesOut);

  void setPreprocDef(const char *name, int value);
  void setPreprocDef(const char *name, bool value);
};
