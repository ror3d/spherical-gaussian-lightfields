#pragma once

#ifdef CHAG_ENABLE_CUDA
#include <cuda_runtime_api.h>
#endif
#include <string>
#include <stdint.h>
#include <vector>
#include <string>
#include <map>
#include <chrono>

#if !defined(WIN32)
using Clock = std::chrono::steady_clock;
#else
// See https://connect.microsoft.com/VisualStudio/feedback/details/858357/steady-clock-now-returning-the-wrong-type
// Using std::chrono::steady_clock should work with Visual Studio 2015
using Clock = std::chrono::system_clock;
#endif
using Timestamp = std::chrono::time_point<Clock>;
using Milliseconds = std::chrono::duration<double, std::milli>;

struct TraceEntry
{
	enum Type {
		ID_CPU,
		ID_GL,
#ifdef CHAG_ENABLE_CUDA
		ID_CUDA,
#endif
		ID_SYNC
	} m_type;
	bool		m_is_scope_start;
	bool m_contains_scope;					// the GTK backend on Linux needs to know in advance whether an item can be a container
	std::string m_name;
	// Events
	Timestamp	m_cpu_timestamp;
	uint32_t	m_gl_query;
#ifdef CHAG_ENABLE_CUDA
	cudaEvent_t m_cuda_event;
#endif
	// Timestamps
	double m_cpu_time;					// CPU time in milliseconds
	double m_opengl_time;				// OpenGL time in milliseconds;
#ifdef CHAG_ENABLE_CUDA
	double m_cuda_time;					// CUDA time in milliseconds;
#endif
};

struct Trace
{
	std::vector<TraceEntry> trace;
	// Is the trace actively being built? (or are we waiting for times to be available?)
	bool active;
};

void push_trace(std::string name);
void pop_trace();
#if !defined(DISABLE_PROFILER)
#define PUSH_TRACE(name) do { push_trace(name); } while (false)
#define POP_TRACE() do { pop_trace(); } while (false)
#else
#define PUSH_TRACE(name)
#define POP_TRACE()
#endif
std::map<std::string, Trace> & GET_TRACES();

class CPUScopeTimer {
public:
	CPUScopeTimer(const std::string &name);
	~CPUScopeTimer();
	static Timestamp getTimestamp();
};

class GLScopeTimer {
public:
	static std::vector<uint32_t> m_query_ids;
	static uint32_t get_query_id();
	static void return_query_id(uint32_t id);
	GLScopeTimer(const std::string &name);
	~GLScopeTimer();
};

#ifdef CHAG_ENABLE_CUDA
class CUDAScopeTimer {
public:
	CUDAScopeTimer(const std::string &name);
	~CUDAScopeTimer();
};
#endif

class TraceSync {
public:
	TraceSync(const std::string &name);
	~TraceSync();
};

#if !defined(DISABLE_PROFILER)
#define PROFILE_SCOPE(n)
#define CAT_ID2(_id1_, _id2_) _id1_##_id2_
#define CAT_ID(_id1_, _id2_) CAT_ID2(_id1_, _id2_)
#define PROFILE_SYNC(_string_id_) \
	TraceSync CAT_ID(_scope_timer_, __LINE__)(_string_id_);
#define PROFILE_CPU(_string_id_) \
	CPUScopeTimer CAT_ID(_scope_timer_, __LINE__)(_string_id_);
#define PROFILE_GL(_string_id_) \
	GLScopeTimer CAT_ID(_scope_timer_, __LINE__)(_string_id_);
#ifdef CHAG_ENABLE_CUDA
#define PROFILE_CUDA(_string_id_) \
	CUDAScopeTimer CAT_ID(_scope_timer_, __LINE__)(_string_id_);
#endif
#else
#define PROFILE_SCOPE(n)
#define CAT_ID2(_id1_, _id2_)
#define CAT_ID(_id1_, _id2_)
#define PROFILE_SYNC(_string_id_)
#define PROFILE_CPU(_string_id_)
#define PROFILE_GL(_string_id_)
#ifdef CHAG_ENABLE_CUDA
#define PROFILE_CUDA(_string_id_)
#endif
#endif
