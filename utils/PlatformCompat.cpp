#include "PlatformCompat.h"

// linux
#if defined(__linux__)
char* _getcwd( char* buf, size_t size )
{
	return getcwd( buf, size );
}
int _stricmp( const char* a, const char* b )
{
	return strcasecmp( a, b );
}
#endif // __linux__

// windows
#if defined(_WIN32)
int strcasecmp( const char* a, const char* b )
{
	return _stricmp( a, b );
}
#endif // _WIN32

