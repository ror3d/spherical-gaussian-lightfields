#ifndef _DebugRenderer_h_
#define _DebugRenderer_h_

#ifdef _WIN32
    #include "IntTypes.h"
#endif
#include <map>
#include <vector>
#include <string>

#include "Aabb.h"

#include "GLSLProgramObject.h"
#include "view.h"
#include <mutex>

namespace chag
{

///////////////////////////////////////////////////////////////////////////
// An actual debugrenderer
///////////////////////////////////////////////////////////////////////////
class DebugRenderer
{
public:
	using vec4 = glm::vec4;
	using vec3 = glm::vec3;
	using ivec4 = glm::ivec4;
	using ivec3 = glm::ivec3;
	using mat4 = glm::mat4;

	static const vec4 red;
	static const vec4 blue;
	static const vec4 green;
	static const vec4 yellow;
	static const vec4 purple;
	static const vec4 cyan;
	static const vec4 orange;
	static const vec4 brown;
	static const vec4 pink;
	static const vec4 olive;

  static DebugRenderer& instance();
  
	virtual void addCone(const vec3 &pos, const vec3 &dir, float height, float angle, const vec4 &color, const std::string &layer = std::string());
	virtual void addAabb(const chag::Aabb &aabb, const vec4 &color, const std::string &layer = std::string());
	virtual void addSphere(const vec3 &pos, float rad, const vec4 &color, const std::string &layer = std::string());
	virtual void addPoint(const vec3 &pos, const vec4 &color, const std::string &layer = std::string());
	virtual void addLine(const vec3 &pos0, const vec3 &pos1, const vec4 &color, const std::string &layer = std::string());
	virtual void addTriangle(const vec3 &pos0, const vec3 &pos1, const vec3 &pos2, const vec4 &color, const std::string &layer = std::string());
	virtual void clear(const std::string &layer = std::string(), const mat4 &layerTransform = mat4(1.0f), bool hidden = false);
	virtual void flush(const std::string &layer);
	virtual void hide(const std::string &layer, bool value);
	virtual void flushAll();
	virtual void displayEnabledLayers(GLSLProgramObject* program, view& camera);

	static vec4 randomColor(uint32_t seed, float alpha = 1.0f);
protected:
	virtual void flushLayer(const std::string &layer);

	struct Item
	{
		Item() {}
		enum Kind
		{
			K_Cone,
			K_Aabb,
			K_Sphere,
			K_Point,
			K_Line,
			K_Triangle,
			K_Max,
		};
		vec4 color;
		struct Cone
		{
			vec3 position;
			float height;
			vec3 direction;
			float angle;
		};
		struct Sphere
		{
			vec3 position;
			float radius;
		};
		struct Point
		{
			vec3 position;
		};
		struct Line
		{
			vec3 pos0; 
			vec3 pos1; 
		};
		struct Triangle
		{
			vec3 pos0; 
			vec3 pos1; 
			vec3 pos2; 
		};
		union
		{
			Cone cone;
			Aabb aabb;
			Sphere sphere;
			Point point;
			Line line; 
			Triangle triangle; 
		};
		Kind kind;
	};

	struct Layer
	{
		mat4 transform;
		std::vector<Item> items;
		bool hidden; 
	};

	// map of layers, and their items.
	std::mutex m_items_guard; 
	std::map<std::string, Layer> m_items;

protected: 
  DebugRenderer();
};


///////////////////////////////////////////////////////////////////////////
// A dummy class for simply turning off the debug renders
///////////////////////////////////////////////////////////////////////////
class EmptyDebugRenderer
{
public:
	using vec4 = glm::vec4;
	using vec3 = glm::vec3;
	using mat4 = glm::mat4;

	static EmptyDebugRenderer& instance();
	virtual void addCone(const vec3 &/*pos*/, const vec3 &/*dir*/, float /*height*/, float /*angle*/, const vec4 &/*color*/, const std::string &/*layer = std::string()*/) {};
	virtual void addAabb(const chag::Aabb &/*aabb*/, const vec4 &/*color*/, const std::string &/*layer = std::string()*/) {};
	virtual void addSphere(const vec3 &/*pos*/, float /*rad*/, const vec4 &/*color*/, const std::string &/*layer = std::string()*/) {};
	virtual void addPoint(const vec3 &/*pos*/, const vec4 &/*color*/, const std::string &/*layer = std::string()*/) {};
	virtual void addLine(const vec3 &/*pos0*/, const vec3 &/*pos1*/, const vec4 &/*color*/, const std::string &/*layer = std::string()*/) {};
	virtual void addTriangle(const vec3 &/*pos0*/, const vec3 &/*pos1*/, const vec3 &/*pos2*/, const vec4 &/*color*/, const std::string &/*layer = std::string()*/) {};
	virtual void clear(const std::string &/*layer*/ = std::string(), const mat4 &/*layerTransform*/ = mat4(1.0f), bool /*hidden*/ = false) {};
	virtual void flush(const std::string &/*layer*/) {};
	virtual void hide(const std::string &/*layer*/, bool /*value*/) {};
	virtual void flushLayer(const std::string &/*layerId*/) {};
	virtual void flushAll() {};
	virtual void displayEnabledLayers(GLSLProgramObject* /*program*/, view& /*camera*/) {};
};

}; // namespace chag


#endif // _DebugRenderer_h_
