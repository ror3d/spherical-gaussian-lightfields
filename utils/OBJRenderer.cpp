#include "OBJRenderer.h"
#include <iostream>
#include <fstream>
#include "Log.h"
#include <sstream>

using namespace std; 

namespace chag 
{

	OBJRenderer::OBJRenderer(void)
	{
	}

	OBJRenderer::~OBJRenderer(void)
	{
	}

	OBJRenderer& OBJRenderer::instance()
	{
		static OBJRenderer inst;
		return inst;
	}

	void OBJRenderer::flushLayer(const std::string &layerId)
	{
		stringstream ss; 
		ss << "OBJRenderer_" << layerId << ".obj"; 
		ofstream os(ss.str().c_str());
		if(os.is_open()) {
			vector<vec4> vertices; 
			vector<ivec4> faces;
			vector<ivec3> triangles; 

			const Layer &layer = m_items[layerId];
			const std::vector<Item> &items = layer.items;

			mat4 Transform = layer.transform; 

			for (std::vector<Item>::const_iterator it = items.begin(); it != items.end(); ++it)
			{
				const Item &item = (*it);
				//glColor(item.color);
				switch(item.kind)
				{
				case Item::K_Cone:
				case Item::K_Sphere:
				case Item::K_Line: 
				case Item::K_Triangle: {
					int base = int(vertices.size());
					vertices.push_back(Transform * vec4(item.triangle.pos0.x, item.triangle.pos0.y, item.triangle.pos0.z, 1.0f));
					vertices.push_back(Transform * vec4(item.triangle.pos1.x, item.triangle.pos1.y, item.triangle.pos1.z, 1.0f));
					vertices.push_back(Transform * vec4(item.triangle.pos2.x, item.triangle.pos2.y, item.triangle.pos2.z, 1.0f));
					triangles.push_back(ivec3(base + 0, base + 1, base + 2));
				}
					break;
				case Item::K_Aabb:
					{
						const Aabb &aabb = item.aabb;
						glm::vec3 mi = aabb.min;
						glm::vec3 ma = aabb.max;
						int base = int(vertices.size()); 
						vertices.push_back(Transform * vec4(mi.x, mi.y, mi.z, 1.0f));
						vertices.push_back(Transform * vec4(ma.x, mi.y, mi.z, 1.0f));
						vertices.push_back(Transform * vec4(ma.x, ma.y, mi.z, 1.0f));
						vertices.push_back(Transform * vec4(mi.x, ma.y, mi.z, 1.0f));
						vertices.push_back(Transform * vec4(mi.x, mi.y, ma.z, 1.0f));
						vertices.push_back(Transform * vec4(ma.x, mi.y, ma.z, 1.0f));
						vertices.push_back(Transform * vec4(ma.x, ma.y, ma.z, 1.0f));
						vertices.push_back(Transform * vec4(mi.x, ma.y, ma.z, 1.0f));
						faces.push_back(ivec4(base + 0, base + 1, base + 2, base + 3));
						faces.push_back(ivec4(base + 1, base + 5, base + 6, base + 2));
						faces.push_back(ivec4(base + 5, base + 4, base + 7, base + 6));
						faces.push_back(ivec4(base + 4, base + 0, base + 3, base + 7));
						faces.push_back(ivec4(base + 2, base + 6, base + 7, base + 3));
						faces.push_back(ivec4(base + 0, base + 4, base + 5, base + 1));
					};
					break;
				default:
					LOG_VERBOSE("Skipping item of type " << item.kind << ".");
					break;
				};
			}

			os << "mtllib " << "OBJRenderer_" << layerId << ".mtl" << endl;
			os << "g " << layerId << "\n";
			os << "s off\n";
			os << "usemtl Default" << endl; 
			for(unsigned int i=0; i<vertices.size(); i++) {
				vertices[i] = (1.0f/vertices[i].w) * vertices[i]; 
				os << "v " << vertices[i].x << " " << vertices[i].y << " " << vertices[i].z << "\n";
			}
			for(unsigned int i=0; i<faces.size(); i++) {
				os << "f " << faces[i].x+1 << " " << faces[i].y+1 << " " << faces[i].z+1 << " " << faces[i].w+1 << "\n";
			}
			for(unsigned int i = 0; i<triangles.size(); i++) {
				os << "f " << triangles[i].x + 1 << " " << triangles[i].y + 1 << " " << triangles[i].z + 1 << "\n";
			}

			{ // Save a default material library
				stringstream ss; 
				ss << "OBJRenderer_" << layerId << ".mtl";
				ofstream os(ss.str().c_str());
				if(os.is_open()) {
					os << "newmtl Default\n";
					os << "\tKd 0.7 0.7 0.7\n";
					os.close(); 
				}
				else LOG_WARNING("Failed to save " << ss.str());
			}
			os.close(); 
		}
		else {
			LOG_ERROR("Failed to open OBJRenderer.obj"); 
		}
	}

}
