#ifndef BUDDY_MEMORY_ALLOCATION_H
#define BUDDY_MEMORY_ALLOCATION_H
#include <inttypes.h>

///////////////////////////////////////////////////////////////////////////////
// This code is taken from: 
// https://github.com/cloudwu/buddy
// with very few modifications (at the time of writing, only the 
// _available() and buddy_available() functions are added)
///////////////////////////////////////////////////////////////////////////////

struct buddy;

struct buddy * buddy_new(int level);
void buddy_delete(struct buddy *);
int buddy_alloc(struct buddy *, int size);
void buddy_free(struct buddy *, int offset);
int buddy_size(struct buddy *, int offset);
void buddy_dump(struct buddy *);
uint32_t buddy_available(struct buddy * self);

#endif