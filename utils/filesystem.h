#pragma once

#ifndef _chag_Filesystem_h
#define _chag_Filesystem_h

#include <string>
#include <filesystem>

namespace chag
{

inline std::string getFilePath( const std::string& filename )
{
	std::string basePath = filename;
	{
		for ( size_t i = 0; i < basePath.length(); ++i )
		{
			if ( basePath[i] == '\\' )
			{
				basePath[i] = '/';
			}
		}
		size_t slash = basePath.find_last_of( '/' );
		if ( slash != std::string::npos )
		{
			basePath = basePath.substr( 0, slash + 1 );
		}
		else
		{
			basePath = "";
		}
	}
	return basePath;
}

} // namespace chag

#endif
