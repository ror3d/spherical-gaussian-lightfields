#ifndef MORTON_H
#define MORTON_H

#include <stdint.h>
#include <limits.h>
#include "glm/vec3.hpp"
#include "glm/exponential.hpp"

// Interleaving 32-bits integers into a 64-bit morton code. Largest resolution in 63 bits is 4194303^3 voxels. 21 bits per x,y,z.
// From http://www.forceflow.be/2013/10/07/morton-encodingdecoding-through-bit-interleaving-implementations/
static inline uint64_t splitBy3(unsigned int a)
{ // method to separate bits from a given integer 3 positions apart
	uint64_t x = a & 0x1fffff; // we only look at the first 21 bits
	x = (x | x << 32) & 0x1f00000000ffff;  // shift left 32 bits, OR with self, and 00011111000000000000000000000000000000001111111111111111
	x = (x | x << 16) & 0x1f0000ff0000ff;  // shift left 32 bits, OR with self, and 00011111000000000000000011111111000000000000000011111111
	x = (x | x << 8) & 0x100f00f00f00f00f; // shift left 32 bits, OR with self, and 0001000000001111000000001111000000001111000000001111000000000000
	x = (x | x << 4) & 0x10c30c30c30c30c3; // shift left 32 bits, OR with self, and 0001000011000011000011000011000011000011000011000011000100000000
	x = (x | x << 2) & 0x1249249249249249;
	return x;
}

static inline uint64_t mortonEncode_magicbits(unsigned int x, unsigned int y, unsigned int z)
{
	uint64_t answer = 0;
	answer |= splitBy3(x) | splitBy3(y) << 1 | splitBy3(z) << 2;
	return answer;
}

static inline unsigned int compactBy3(uint64_t x) 
{
	x &= 0x1249249249249249;					// x = ...f--e--d--c--b--a--9--8--7-6--5--4--3--2--1--0
	x = (x ^ (x >> 2)) & 0x10c30c30c30c30c3;	// x = --fe --dc --ba --98 --76 --54 --32 --10
	x = (x ^ (x >> 4)) & 0x100f00f00f00f00f;	// x = ---- fedc ---- ba98 ---- 7654 ---- 3210
	x = (x ^ (x >> 8)) & 0x1f0000ff0000ff;		// x = ---- ---- fedc ba98 ---- ---- 7654 3210
	x = (x ^ (x >> 16)) & 0x1f00000000ffff;	    // x = ---- ---- ---- ---- fedc ba98 7654 3210
	x = (x ^ (x >> 32)) & 0x1fffff;			    // x = ---- ---- ---- ---- fedc ba98 7654 3210
	return (unsigned int)x;
}

static inline glm::ivec3 mortonDecode_magicbits(uint64_t p)
{
	glm::ivec3 answer;
	answer.x = compactBy3(p);
	answer.y = compactBy3(p >> 1);
	answer.z = compactBy3(p >> 2);
	return answer;
}

static inline int roof(float a)
{
	int b = (int)a;
	if (b < a) b++;
	return b;
}


// from: http://and-what-happened.blogspot.se/2011/08/fast-2d-and-3d-hilbert-curves-and.html
static inline unsigned int Morton_2D_Encode_16bit(unsigned int index1, unsigned int index2)
{ // pack 2 16-bit indices into a 32-bit Morton code
	index1 &= 0x0000ffff;
	index2 &= 0x0000ffff;
	index1 |= (index1 << 8);
	index2 |= (index2 << 8);
	index1 &= 0x00ff00ff;
	index2 &= 0x00ff00ff;
	index1 |= (index1 << 4);
	index2 |= (index2 << 4);
	index1 &= 0x0f0f0f0f;
	index2 &= 0x0f0f0f0f;
	index1 |= (index1 << 2);
	index2 |= (index2 << 2);
	index1 &= 0x33333333;
	index2 &= 0x33333333;
	index1 |= (index1 << 1);
	index2 |= (index2 << 1);
	index1 &= 0x55555555;
	index2 &= 0x55555555;
	return(index1 | (index2 << 1));
}
static inline void Morton_2D_Decode_16bit(const unsigned int morton, unsigned int& index1, unsigned int& index2)
{ // unpack 2 16-bit indices from a 32-bit Morton code
	unsigned int value1 = morton;
	unsigned int value2 = (value1 >> 1);
	value1 &= 0x55555555;
	value2 &= 0x55555555;
	value1 |= (value1 >> 1);
	value2 |= (value2 >> 1);
	value1 &= 0x33333333;
	value2 &= 0x33333333;
	value1 |= (value1 >> 2);
	value2 |= (value2 >> 2);
	value1 &= 0x0f0f0f0f;
	value2 &= 0x0f0f0f0f;
	value1 |= (value1 >> 4);
	value2 |= (value2 >> 4);
	value1 &= 0x00ff00ff;
	value2 &= 0x00ff00ff;
	value1 |= (value1 >> 8);
	value2 |= (value2 >> 8);
	value1 &= 0x0000ffff;
	value2 &= 0x0000ffff;
	index1 = value1;
	index2 = value2;
}
#endif // MORTON_H