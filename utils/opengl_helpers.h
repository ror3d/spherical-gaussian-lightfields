#pragma once
#include <stdint.h>
#include <vector>
#include <GL/glew.h>
#include <cstdarg>
#include <utils/Log.h>
#include <assert.h>
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

namespace opengl_helpers
{
	void drawFullScreenQuad();

	namespace texture
	{
		uint32_t createTexture(int width, GLenum internal_format, void * data = nullptr);
		uint32_t createTexture(int width, int height, GLenum internal_format, void * data = nullptr);
		void deleteTexture(uint32_t id);
		uint32_t resizeTexture(uint32_t old_id, int width, int height);
		uint32_t getDummyShadowmap();
		GLenum getDefaultType(GLenum internal_format);
		GLenum getDefaultFormat(GLenum internal_format);
		int getWidth(uint32_t id, int mip_level = 0);
		int getHeight(uint32_t id, int mip_level = 0);
		void generateMipmap(uint32_t texture_id, uint32_t program_object);
	}

	namespace framebuffer
	{
		bool checkFramebufferCompleteness(uint32_t id);
		void assertFramebufferStackIntact();
		void pushFramebuffer(uint32_t id);
		uint32_t createTexture(int width, int height, GLenum internal_format);
		uint32_t popFramebuffer();
		uint32_t currentFramebuffer();
		uint32_t createFramebuffer(int width, int height, uint32_t depth_id, std::vector<uint32_t> color_id); 
		inline uint32_t createFramebuffer(int width, int height, std::vector<uint32_t> color_id) {
			return createFramebuffer(width, height, 0u, color_id);
		}
		struct attached_texture { uint32_t attachment; uint32_t texture_id; };
		std::vector<attached_texture> getAttachedTextures(uint32_t framebuffer);
		void deleteFramebuffer(uint32_t framebuffer, bool delete_rendertargets = true);
		void resizeFramebuffer(uint32_t framebuffer, int width, int height);
		int getFramebufferDefaultWidth(const uint32_t framebuffer);
		int getFramebufferDefaultHeight(const uint32_t framebuffer);
		void bindFramebufferDepthBuffer(uint32_t framebuffer, uint32_t base_texture_unit);
		// Returns number of color buffers bound
		size_t bindFramebufferColorBuffers(uint32_t framebuffer, uint32_t base_texture_unit);
	}

	namespace geometry
	{
		void line( glm::vec3 start, glm::vec3 end, glm::vec3 color );
		void disk( glm::vec3 position, float radius, glm::vec3 normal, glm::vec3 color, bool fill = true, glm::vec3 origin_direction = {}, float arc = glm::two_pi<float>() );
		void sphere( glm::vec3 position, float radius, glm::vec3 color );
		void arrow( glm::vec3 start, glm::vec3 end, glm::vec3 color );
	}
};

