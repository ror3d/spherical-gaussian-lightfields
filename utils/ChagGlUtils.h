#ifndef _ChagGlUtils_h_
#define _ChagGlUtils_h_

#include <GL/glew.h>
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"

namespace chag
{

inline void glColor(const glm::vec4 &c)
{
	glColor4fv(glm::value_ptr(c));
}
inline void glColor(const glm::vec3 &c)
{
	glColor3fv(glm::value_ptr(c));
}
inline void glVertexF3(const glm::vec3 &v)
{
	glVertex3fv(glm::value_ptr(v));
}
inline void glVertex(const glm::vec3 &v)
{
	glVertex3fv(glm::value_ptr(v));
}
/*
inline void glVertex(const chag::uint2 &v)
{
	glVertex2iv(reinterpret_cast<const GLint*>(&v.x));
}*/
inline void glLoadMatrix(const glm::mat4 &m)
{
	glLoadMatrixf(glm::value_ptr(m));
}
inline void glMultMatrix(const glm::mat4 &m)
{
	glMultMatrixf(glm::value_ptr(m));
}
inline void glTranslate(const glm::vec3 &v)
{
	glTranslatef(v.x, v.y, v.z);
}


}; // namespace chag


#endif // _ChagGlUtils_h_
