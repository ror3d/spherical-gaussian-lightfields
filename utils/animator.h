#pragma once
#include <vector>

namespace chag
{

template<typename _T>
class Animator
{
public:
	Animator()
		: m_value( nullptr )
	{
	}

	Animator( _T* value )
		: m_value( value )
	{
	}

	Animator( _T* value, _T start_value, _T end_value, float time )
		: m_value( value )
	{
		m_frames.push_back( KeyFrame{ start_value, 0.f } );
		m_frames.push_back( KeyFrame{ end_value, time } );
	}

	void setValue( _T* value )
	{
		m_value = value;
	}

	_T* getValue()
	{
		return m_value;
	}

	void update( float dt )
	{
		if ( !m_playing )
		{
			return;
		}
		if ( m_current_frame + 1 >= m_frames.size() )
		{
			return;
		}
		m_time += dt;
		updateInternal();
	}

	void play()
	{
		m_playing = true;
	}

	void pause()
	{
		m_playing = false;
	}

	void reset()
	{
		m_time = 0;
		m_current_frame = 0;
	}

	void setCurrentTime( float time )
	{
		if ( time >= 0 )
		{
			m_current_frame = 0;
			m_time = time;
			updateInternal();
		}
	}

	bool isPlaying() const
	{
		return m_playing;
	}

	bool isFinished() const
	{
		return m_current_frame + 1 == m_frames.size();
	}

	void clearKeyFrames()
	{
		m_frames.clear();
		reset();
	}

	void addKeyFrame( float time, _T value )
	{
		bool added = false;
		for ( auto it = m_frames.begin(); it != m_frames.end(); ++it )
		{
			if ( time == it->timestamp )
			{
				it->value = value;
				added = true;
				break;
			}
			else if ( time < it->timestamp )
			{
				m_frames.insert( it, KeyFrame{ value, time } );
				added = true;
				break;
			}
		}
		if ( !added )
		{
			m_frames.push_back( KeyFrame{ value, time } );
		}
	}

	void setInitial( _T value )
	{
		if ( m_frames.size() == 0 )
		{
			m_frames.push_back( KeyFrame{ value, 0 } );
		}
		else
		{
			m_frames[0].value = value;
		}
	}

	void addNextFrame( float time_increment, _T value )
	{
		if ( m_frames.size() == 0 )
		{
			m_frames.push_back( KeyFrame{ value, 0 } );
		}
		m_frames.push_back( KeyFrame{ value, m_frames.back().timestamp + time_increment } );
	}

	size_t getNumKeyFrames() const
	{
		return m_frames.size();
	}

	float getFrameTime( size_t frameIdx ) const
	{
		if ( frameIdx < m_frames.size() )
		{
			return m_frames[frameIdx].timestamp;
		}
		return 0.f;
	}


	_T getFrameValue( size_t frameIdx ) const
	{
		if ( frameIdx < m_frames.size() )
		{
			return m_frames[frameIdx].value;
		}
		return {};
	}

	void removeFrame( size_t frameIdx )
	{
		if ( frameIdx < m_frames.size() )
		{
			m_frames.erase( m_frames.begin() + frameIdx );
		}
	}

private:
	void updateInternal()
	{
		while ( m_current_frame + 1 < m_frames.size() && m_frames[m_current_frame + 1].timestamp <= m_time )
		{
			m_current_frame += 1;
		}
		if ( m_current_frame + 1 >= m_frames.size() )
		{
			*m_value = m_frames[m_current_frame].value;
			return;
		}
		float t = (m_time - m_frames[m_current_frame].timestamp)
			/ (m_frames[m_current_frame + 1].timestamp - m_frames[m_current_frame].timestamp);
		*m_value = (1 - t)*m_frames[m_current_frame].value + t * m_frames[m_current_frame + 1].value;
	}


private:
	struct KeyFrame
	{
		_T value;
		float timestamp;
	};


private:
	_T* m_value;
	float m_time = 0;
	size_t m_current_frame = 0;
	bool m_playing = false;
	std::vector<KeyFrame> m_frames;
};

}
