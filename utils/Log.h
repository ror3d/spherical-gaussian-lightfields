#pragma once

#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

#ifdef _WIN32
#include <Windows.h>
#else
#include <csignal>
#endif

///////////////////////////////////////////////////////////////////////////////
// Simple logging
///////////////////////////////////////////////////////////////////////////////
class Log
{
public:
	struct LogOStream : std::ostream
	{
		struct LogStream : std::basic_streambuf<char>
		{
			LogStream() {}
			~LogStream()
			{
				sync();
			}
			inline std::streamsize xsputn( const std::char_traits<char>::char_type* s, std::streamsize n ) override
			{
#ifdef _WIN32
				OutputDebugStringA( s );
#endif
				std::cout.write( s, n );
				return n;
			}
			std::char_traits<char>::int_type overflow ( std::char_traits<char>::int_type c )
			{
#ifdef _WIN32
				char str[2]; str[0] = c; str[1] = 0;
				OutputDebugStringA( str );
#endif
				std::cout.put( c );
				return std::char_traits<char>::int_type( c );
			}
			inline int sync() override
			{
				std::cout.flush();
				return 0;
			}
		};
		LogOStream() : std::ostream( &stream ) {}
	private:
		LogStream stream;
	};
	static void InsertTimestamp()
	{
		std::time_t t = std::time( nullptr );
		std::tm*    timeinfo = std::localtime( &t );
		Log::GetStream() << std::put_time( timeinfo, "[%X]" );   // writes localized time representation (locale dependent)
	}
	enum Level { Error, Warning, Info, Verbose };
	static Level         level_active;
	static LogOStream     log_stream;
	static std::ostream& GetStream() { return log_stream; }
	static void          SetLevelActive( Level l ) { level_active = l; }
	static bool          IsLevelActive( Level l ) { return l <= level_active; }
	static std::locale   SetLocale( const std::locale& loc ) { return Log::GetStream().imbue( loc ); }
};

#ifndef NO_LOG
#ifdef _WIN32
#define LOG_FATAL( M )                                     \
	do                                                     \
	{                                                      \
		Log::InsertTimestamp();							   \
		Log::GetStream() << " FATAL  : " << M << " at " __FILE__ "(" STRINGIFY( __LINE__ ) ")\n" << std::flush; \
		DebugBreak();									   \
		exit(-1);										   \
	} while ( false )
#else
#define LOG_FATAL( M )                                     \
	do                                                     \
	{                                                      \
		Log::InsertTimestamp();							   \
		Log::GetStream() << " FATAL  : " << M << " at " __FILE__ "(" STRINGIFY( __LINE__ ) ")\n"; \
		raise(SIGTRAP);    								   \
		exit(-1);										   \
	} while ( false )
#endif

#define LOG_ERROR( M )                                     \
	do                                                     \
	{                                                      \
		if ( Log::IsLevelActive( Log::Error ) )            \
		{                                                  \
			Log::InsertTimestamp();                        \
			Log::GetStream() << " ERROR  : " << M << "\n"; \
		}                                                  \
	} while ( false )
#define LOG_INFO( M )                                      \
	do                                                     \
	{                                                      \
		if ( Log::IsLevelActive( Log::Info ) )             \
		{                                                  \
			Log::InsertTimestamp();                        \
			Log::GetStream() << " INFO   : " << M << "\n"; \
		}                                                  \
	} while ( false )
#define LOG_WARNING( M )                                   \
	do                                                     \
	{                                                      \
		if ( Log::IsLevelActive( Log::Warning ) )          \
		{                                                  \
			Log::InsertTimestamp();                        \
			Log::GetStream() << " WARNING: " << M << "\n"; \
		}                                                  \
	} while ( false )
#define LOG_VERBOSE( M )                                   \
	do                                                     \
	{                                                      \
		if ( Log::IsLevelActive( Log::Verbose ) )          \
		{                                                  \
			Log::InsertTimestamp();                        \
			Log::GetStream() << " VERBOSE: " << M << "\n"; \
		}                                                  \
	} while ( false )
#else
#define LOG_FATAL( M )
#define LOG_ERROR( M )
#define LOG_INFO( M )
#define LOG_WARNING( M )
#endif


///////////////////////////////////////////////////////////////////////////////
// Breaking asserts and errors
///////////////////////////////////////////////////////////////////////////////
#define STRINGIFY( x ) STRINGIFY2( x )
#define STRINGIFY2( x ) #x
#ifdef _WIN32
#define DebugAssert( x )                                                                          \
	do                                                                                            \
	{                                                                                             \
		if ( !( x ) )                                                                             \
		{                                                                                         \
			LOG_ERROR( "Assertion failed :" #x " at " __FILE__ "(" STRINGIFY( __LINE__ ) ")\n" ); \
			if ( IsDebuggerPresent() )                                                            \
				DebugBreak();                                                                     \
		}                                                                                         \
	} while ( 0 )
#define DebugError( x )                                                                 \
	do                                                                                  \
	{                                                                                   \
		LOG_ERROR( "Error at " __FILE__ "(" STRINGIFY( __LINE__ ) "): " << x << "\n" ); \
		if ( IsDebuggerPresent() )                                                      \
			DebugBreak();                                                               \
	} while ( 0 )
#else
#define DebugAssert( x )                                                                          \
	do                                                                                            \
	{                                                                                             \
		if ( !( x ) )                                                                             \
		{                                                                                         \
			LOG_ERROR( "Assertion failed :" #x " at " __FILE__ "(" STRINGIFY( __LINE__ ) ")\n" ); \
            raise(SIGTRAP);                                                                       \
		}                                                                                         \
	} while ( 0 )
#define DebugError( x )                                                                 \
	do                                                                                  \
	{                                                                                   \
		LOG_ERROR( "Error at " __FILE__ "(" STRINGIFY( __LINE__ ) "): " << x << "\n" ); \
        raise(SIGTRAP);                                                                 \
	} while ( 0 )
#endif



void _chag_error( const char* file, unsigned int line, const std::string& reason );

// Exit with an error message popup
#define CHAG_EXIT_ERROR( message ) do { _chag_error( __FILE__, __LINE__, message ); } while(0) 


