#include "DebugRenderer.h"
#include <GL/glew.h>
#include <GL/freeglut.h>
#ifdef _WIN32
    #include "Assert.h"
#endif
#include "ChagGlUtils.h"
#include "glm_extensions.h"
#include "glm/glm.hpp"
#include "Log.h"

static void drawCone(const glm::vec3 &axis, const float angle, const glm::vec3 &pos, bool drawAxis = true)
{
	if (drawAxis)
	{
		glLineWidth(3.0f);
		glBegin(GL_LINES);
			chag::glVertex(pos);
			chag::glVertex(pos + axis * 1.3f);
		glEnd();
		glLineWidth(1.0f);
	}
	glPushMatrix();
	chag::glMultMatrix(make_matrix_from_zAxis(pos + axis, -axis, perp(axis)));
	float height = length(axis);
	glutSolidCone(tanf(angle) * height, height, 48, 2);
	glPopMatrix();
}


static void drawAabb(const chag::Aabb &aabb)
{
  using namespace chag;
  using vec3 = glm::vec3;
  vec3 mi = aabb.min;
  vec3 ma = aabb.max;

  // face 0 min X
  glVertex(mi);
  glVertex(vec3(mi.x, ma.y, mi.z));
  glVertex(vec3(ma.x, ma.y, mi.z));
  glVertex(vec3(ma.x, mi.y, mi.z));
  // face 1, max X
  glVertex(vec3(mi.x, mi.y, ma.z));
  glVertex(vec3(ma.x, mi.y, ma.z));
  glVertex(ma);
  glVertex(vec3(mi.x, ma.y, ma.z));

  // face 0 min Y
  glVertex(mi);
  glVertex(vec3(ma.x, mi.y, mi.z));
  glVertex(vec3(ma.x, mi.y, ma.z));
  glVertex(vec3(mi.x, mi.y, ma.z));
  // face 1, max Y
  glVertex(vec3(mi.x, ma.y, mi.z));
  glVertex(vec3(mi.x, ma.y, ma.z));
  glVertex(ma);
  glVertex(vec3(ma.x, ma.y, mi.z));

  // face 0 min Z
  glVertex(mi);
  glVertex(vec3(mi.x, mi.y, ma.z));
  glVertex(vec3(mi.x, ma.y, ma.z));
  glVertex(vec3(mi.x, ma.y, mi.z));
  // face 1, max Z      
  glVertex(vec3(ma.x, mi.y, mi.z));
  glVertex(vec3(ma.x, ma.y, mi.z));
  glVertex(ma);
  glVertex(vec3(ma.x, mi.y, ma.z));
}



namespace chag
{

const glm::vec4 DebugRenderer::red    = {1.0f, 0.0f, 0.0f, 1.0f};
const glm::vec4 DebugRenderer::blue   = {0.0f, 0.0f, 1.0f, 1.0f};
const glm::vec4 DebugRenderer::green  = {0.0f, 1.0f, 0.0f, 1.0f};
const glm::vec4 DebugRenderer::yellow = {1.0f, 1.0f, 0.1f, 1.0f};
const glm::vec4 DebugRenderer::purple = {0.4f, 0.1f, 0.4f, 1.0f};
const glm::vec4 DebugRenderer::cyan   = {0.0f, 1.0f, 1.0f, 1.0f};
const glm::vec4 DebugRenderer::orange = {1.0f, 0.5f, 0.0f, 1.0f};
const glm::vec4 DebugRenderer::brown  = {0.55f, 0.27f, 0.07f, 1.0f};
const glm::vec4 DebugRenderer::pink   = {0.78f, 0.44f, 0.44f, 1.0f};
const glm::vec4 DebugRenderer::olive  = {0.56f, 0.56f, 0.22f, 1.0f};

DebugRenderer& DebugRenderer::instance()
{
  static DebugRenderer inst;
  return inst;
}

DebugRenderer::vec4 DebugRenderer::randomColor(uint32_t i, float alpha)
{
	static const vec4 colors[] =
	{
		red,
		green,
		blue,
		purple,
		yellow,
		cyan,
		orange,
		brown, 
		pink,
		olive,
	};
	vec4 c = colors[i % (sizeof(colors) / sizeof(colors[0]))];
	c.w = alpha;
	return c;
}

void DebugRenderer::addCone(const vec3 &pos, const vec3  &dir, float height, float angle, const vec4 &color, const std::string &layer)
{
	Item::Cone cone = { pos, height, dir, angle };
	Item item;
	item.kind = Item::K_Cone;
	item.color = color;
	item.cone = cone;
	std::lock_guard<std::mutex> guard(m_items_guard);
	m_items[layer].items.push_back(item);
}



void DebugRenderer::addAabb(const chag::Aabb &aabb, const vec4 &color, const std::string &layer)
{
	Item item;
	item.kind = Item::K_Aabb;
	item.color = color;
	item.aabb = aabb;
	std::lock_guard<std::mutex> guard(m_items_guard);
	m_items[layer].items.push_back(item);
}



void DebugRenderer::addSphere(const vec3 &pos, float rad, const vec4 &color, const std::string &layer)
{
	Item::Sphere sphere = { pos, rad };
	Item item;
	item.kind = Item::K_Sphere;
	item.color = color;
	item.sphere = sphere;
	std::lock_guard<std::mutex> guard(m_items_guard);
	m_items[layer].items.push_back(item);
}

void DebugRenderer::addPoint(const vec3 &pos, const vec4 &color, const std::string &layer)
{
	Item::Point point = { pos };
	Item item;
	item.kind = Item::K_Point;
	item.color = color;
	item.point = point;
	std::lock_guard<std::mutex> guard(m_items_guard);
	m_items[layer].items.push_back(item);
}

void DebugRenderer::addLine(const vec3 &pos0, const vec3 &pos1, const vec4 &color, const std::string &layer)
{
	Item::Line line = {pos0, pos1};
	Item item; 
	item.kind = Item::K_Line;
	item.color = color; 
	item.line = line; 
	std::lock_guard<std::mutex> guard(m_items_guard);
	m_items[layer].items.push_back(item);
}

void DebugRenderer::addTriangle(const vec3 &pos0, const vec3 &pos1, const vec3 &pos2, const vec4 &color, const std::string &layer)
{
	Item::Triangle triangle = {pos0, pos1, pos2};
	Item item; 
	item.kind = Item::K_Triangle;
	item.color = color; 
	item.triangle = triangle; 
	std::lock_guard<std::mutex> guard(m_items_guard);
	m_items[layer].items.push_back(item);
}

void DebugRenderer::clear(const std::string &layer, const mat4 &layerTransform, bool hidden)
{	
	if (layer == std::string())
	{
		m_items.clear();
	}
	else
	{
		Layer &l = m_items[layer];
		l.hidden = hidden; 
		l.items.clear();
		l.transform = layerTransform;
	}
}





void DebugRenderer::flush(const std::string &layer)
{
	if (layer == std::string())
	{
		//flushLayer();
	}
	else
	{
		flushLayer(layer);
	}
}

void DebugRenderer::flushAll()
{
	for(auto layer : m_items) {
		if(!layer.second.hidden) {
			flush(layer.first);
		}
	}
}

void DebugRenderer::hide(const std::string &layer, bool value) {
	m_items[layer].hidden = value; 
}

void DebugRenderer::flushLayer(const std::string &layerId)
{
	const Layer &layer = m_items[layerId];
	const std::vector<Item> &items = layer.items;

	glPushMatrix();
	glMultMatrix(layer.transform);

	for (std::vector<Item>::const_iterator it = items.begin(); it != items.end(); ++it)
	{
		const Item &item = (*it);
		glColor(item.color);
		switch(item.kind)
		{
		case Item::K_Cone:
			{
				const Item::Cone &cone = item.cone;
				drawCone(cone.direction * cone.height, cone.angle, cone.position, false);
			};
			break;
		case Item::K_Sphere:
			{
				const Item::Sphere &sphere = item.sphere;
				glPushMatrix();
				glTranslate(sphere.position);
				glutSolidSphere(sphere.radius, 6, 6);
				glPopMatrix();				
			};
			break;
		case Item::K_Aabb:
			{
				const Aabb &aabb = item.aabb;
				glBegin(GL_QUADS);
				drawAabb(aabb);
				glEnd();
			};
			break;
		case Item::K_Point:
			{
				glPointSize(4.0f);
				glBegin(GL_POINTS);
				glVertex3fv(&item.point.position.x);
				glEnd();
			}
			break;
		case Item::K_Line: 
			{
				glBegin(GL_LINES); 
				glVertex3fv(&item.line.pos0.x); 
				glVertex3fv(&item.line.pos1.x); 
				glEnd(); 
			}
			break;
		case Item::K_Triangle: 
			{
				glBegin(GL_TRIANGLES); 
				glVertex(item.triangle.pos0); 
				glVertex(item.triangle.pos1); 
				glVertex(item.triangle.pos2); 
				glEnd(); 
			}
			break;
		default:
			LOG_VERBOSE("Skipping item of type " << item.kind << ".");
			break;
		};
	}

	glPopMatrix();
}

void DebugRenderer::displayEnabledLayers(GLSLProgramObject* program, view& camera)
{
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	camera.push();
	program->enable();
	flushAll();
	program->disable();
	camera.pop();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
}


DebugRenderer::DebugRenderer()
{
}


EmptyDebugRenderer& EmptyDebugRenderer::instance()
{
	static EmptyDebugRenderer inst;
	return inst;
}

}; // namespace chag

