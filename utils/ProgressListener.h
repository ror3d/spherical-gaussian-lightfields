#pragma once
#include <string>
#include <vector>

///////////////////////////////////////////////////////////////////////////////
// The interface for a progress listener
// There will only be one (global) progress listener, or it will be NULL,
// but there may be different implementations. For example, the standard
// progress listener will be this empty class, but there will also be the
// option of attaching a text output, or a GUI progress listener.
///////////////////////////////////////////////////////////////////////////////

class ProgressListener
{
public:
	static void activate();
	virtual void setRange(int /*value*/) {}
	virtual void update(int /*progress*/) {}
	int tasks_in_progress = 0;
	virtual void push_task(const std::string &/*new_task_name*/, bool /*indeterminate*/ = false) { tasks_in_progress += 1; }
	virtual void pop_task() { tasks_in_progress -= 1; }
	virtual bool isBusy() { return tasks_in_progress > 0; }
};

extern ProgressListener * g_progress;

class TextProgressListener : public ProgressListener
{
private:
	struct progress_counter {
		int range, progress;
		std::string name;
	};
	std::vector<progress_counter> m_progress_counters;
public:
	static void activate();
	virtual void setRange(int value);
	virtual void update(int progress);
	virtual void push_task(const std::string & new_task_name, bool indeterminate = false);
	virtual void pop_task();
	virtual bool isBusy() { return m_progress_counters.size() > 0; }
};
