#include "preprocessing.h"
#include "../include/spherical_projection_common.h"
#include <glm/glm.hpp>
#include "error_handling.h"
#include "../pathtracer/random.cuh"

using namespace glm; 
using namespace std; 

namespace preprocessing
{
	__inline__ __device__ vec3 perp(const vec3& a) {
		vec3 nv = vec3(std::abs(a.x), std::abs(a.y), std::abs(a.z));
		if (nv.x < nv.y)
			if (nv.x < nv.z)
				return vec3(0.0f, -a.z, a.y);
			else
				return vec3(-a.y, a.x, 0.0f);
		else if (nv.y < nv.z)
			return vec3(-a.z, 0.0f, a.x);
		else
			return vec3(-a.y, a.x, 0.0f);
	}

	__global__ void convolve_kernel(float *d_input, float *d_output)
	{
		const float roughness = 0.2f;
		int pixel_x = threadIdx.x;
		int pixel_y = blockIdx.x;
		int channel = blockIdx.y;
		float theta, phi;
		GetThetaPhiFromTexCoords(pixel_x / float(ENVMAP_SIZE), pixel_y / float(ENVMAP_SIZE), theta, phi);
		glm::vec3 n = { sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta) };
		glm::vec3 t = normalize(perp(n));
		glm::vec3 b = cross(n, t);

		seed_t seed = initialize_seed(make_uint2(pixel_x, pixel_y), 0, 0);

		float sum = 0.0f;
		const int NUM_SAMPLES = 256; 

		for (int s = 0; s < NUM_SAMPLES; s++) {
			float u1 = ::randf(seed);
			float u2 = ::randf(seed);
			float r = sqrtf(u1 / (1 - u1));
			float _phi = 6.28318530718 * u2;
			glm::vec2 slope = r * glm::vec2(cos(_phi), sin(_phi));
			slope *= roughness * roughness;
			glm::vec3 sdir = normalize(glm::vec3(-slope.x, -slope.y, 1.0f));
			// Out of tangent space
			sdir = sdir.x * t + sdir.y * b + sdir.z * n;
			float Theta = acos(sdir.z);
			float Phi = atan2(sdir.y, sdir.x);
			if (Phi < 0.0f) Phi += 2.0f * M_PI;
			float fx, fy;
			GetTexCoordsFromThetaPhi(fx, fy, Theta, Phi);
			int X = fx * ENVMAP_SIZE, Y = fy * ENVMAP_SIZE;
			float input = d_input[channel * (ENVMAP_SIZE * ENVMAP_SIZE) + Y * ENVMAP_SIZE + X];
			if (isnan(input) || isinf(input)) continue; 
			input = max(0.0f, min(1.0f, input));
			sum += d_input[channel * (ENVMAP_SIZE * ENVMAP_SIZE) + Y * ENVMAP_SIZE + X];
		}
		d_output[channel * (ENVMAP_SIZE * ENVMAP_SIZE) + pixel_y * ENVMAP_SIZE + pixel_x] = (1.0f / float(NUM_SAMPLES)) * sum;
		//d_output[channel * (ENVMAP_SIZE * ENVMAP_SIZE) + pixel_y * ENVMAP_SIZE + pixel_x] = 
		//	d_input[channel * (ENVMAP_SIZE * ENVMAP_SIZE) + pixel_y * ENVMAP_SIZE + pixel_x] + sum;
	}

	void ConvolveImage(const vector<float> & input, vector<float> & output, const uint16_t channels)
	{
		static float * d_input = nullptr, *d_output = nullptr;
		if (d_input == nullptr) checkCudaErr(cudaMalloc(&d_input, input.size() * sizeof(float)));
		if (d_output == nullptr) checkCudaErr(cudaMalloc(&d_output, output.size() * sizeof(float)));
		checkCudaErr(cudaMemcpy(d_input, input.data(), input.size() * sizeof(float), cudaMemcpyHostToDevice));
		dim3 block = { ENVMAP_SIZE, 1, 1 }; // One line per block, so x = threadIdx.x
		dim3 grid = { ENVMAP_SIZE, channels, 1 }; // y = blockIdx.x, c = blockIdx.y
		convolve_kernel << <grid, block >> > (d_input, d_output);
		checkCudaErr(cudaMemcpy(output.data(), d_output, output.size() * sizeof(float), cudaMemcpyDeviceToHost));
	}
}