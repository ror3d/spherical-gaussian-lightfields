#pragma once
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <list>
#include <vector>
#include <atomic>
#include <map>
#include <array>
#include <glm/glm.hpp>

namespace chag
{

class TrainingDataScheduler
{
public:
	class data_pointer
	{
	public:
		data_pointer() {}

		data_pointer( data_pointer&& ptr )
		{
			std::swap( data, ptr.data );
			std::swap( fileIdx, ptr.fileIdx );
			std::swap( scheduler, ptr.scheduler );
			std::swap( normal, ptr.normal );
			std::swap( coord, ptr.coord );
		}

		~data_pointer()
		{
			if ( scheduler && data )
			{
				scheduler->freeTrainingData( *this );
			}
		}

		data_pointer& operator=( data_pointer&& ptr )
		{
			std::swap( data, ptr.data );
			std::swap( fileIdx, ptr.fileIdx );
			std::swap( scheduler, ptr.scheduler );
			std::swap( normal, ptr.normal );
			std::swap( coord, ptr.coord );
			return *this;
		}

		float& operator*() { return *data; }
		const float& operator*() const { return *data; }

		float* get() { return data; }
		const float* get() const { return data; }

		glm::vec3 getNormal() const { return normal; }
		glm::u16vec2 getCoords() const { return coord; }

	private:
		float* data = nullptr;
		size_t fileIdx = ~size_t(0);
		TrainingDataScheduler* scheduler = nullptr;
		glm::vec3 normal;
		glm::u16vec2 coord;
	private:
		data_pointer( float* _data, glm::u16vec2 _coord, glm::vec3 _normal, size_t _file, TrainingDataScheduler* sch )
		    : data( _data ), fileIdx( _file ), coord( _coord ), normal( _normal ), scheduler( sch )
		{
		}
		friend class TrainingDataScheduler;
	};

public:
	TrainingDataScheduler( const std::string& data_source_file_path, const std::array<uint16_t, 3>& channel_remap );
	TrainingDataScheduler();
	~TrainingDataScheduler();

	void initialize( const std::string& data_source_file_path, const std::array<uint16_t, 3>& channel_remap );
	size_t getTrainingSetSize() const;

	size_t getEpochDataSetsRemaining() const;

	void prepareNewEpoch();
	void prepareNewInterpolationEpoch();
	void prepareStraightforwardEpoch();
	void endEpoch();
	glm::uvec2 getLightmapSize() const { return m_lightmap_size; }
	uint16_t getLightfieldSize() const { return m_lightfield_side_size; }

	data_pointer getNextTrainingData();

	std::string getBaseFilePath() const { return m_trainingDataBasePath; }

	inline operator bool() const { return m_loaderThread.get_id() != std::thread::id(); };
private:
	struct TrainData
	{
		size_t dataFileIdx;
		size_t chunkOffsetInFile;
		glm::u16vec2 coord;
		glm::vec3 texelNormal;
	};

	void prepareBasicRandomEpoch();
	void loaderThreadFn();
	void loadTrainingMetadata( const std::string& data_source_file_path );
	void loadTrainingData( const TrainData& src );
	void freeTrainingData( const data_pointer& td );
	bool clearSomeLoadedData();
	friend class data_pointer;

private:
	std::thread m_loaderThread;
	std::mutex m_metadataMtx;
	std::recursive_mutex m_loadedDataMtx;
	std::condition_variable m_onMetadataAvailable;
	std::condition_variable_any m_onDataAvailable;
	std::condition_variable_any m_onFreeMemory;
	std::atomic<size_t> m_currentEpochId = 0;

	std::array<uint16_t, 3> m_channel_remap;

	glm::uvec2 m_lightmap_size; 
	uint16_t m_lightfield_side_size = 0;

	size_t m_dataChunkSize = 0;
	size_t m_maxDataFileSize_bytes = 0;

	std::string m_trainingDataBasePath;
	std::vector<std::vector<size_t>> m_batchTexelIndices;
	std::vector<std::string> m_trainingDataFiles;
	std::vector<TrainData> m_trainDataChunks;

	struct ChunkAdjacency
	{
		bool valid;
		std::array<size_t, 4> adjacent;
	};
	std::vector<ChunkAdjacency> m_adjacentChunks_cornerAdjacencyIdxs;
	std::vector<ChunkAdjacency> m_adjacentChunks_edgeAdjacencyIdxs;

	std::vector<size_t> m_randomizedDataChunkIdxs;
	size_t m_epochSetsRemaining = 0;
	std::list<data_pointer> m_loadedTrainingData;
	bool m_firstEpochPrepared = false;

	std::map<size_t, std::vector<float>> m_loadedTrainingBatches;
	std::map<size_t, size_t> m_loadedTrainingBatches_refcount;
	std::list<size_t> m_loadedTrainingBatches_loadOrder;

	// Not used as a HARD maximum, it might be overflown, but not by more than 1 file's worth of data
#ifdef _DEBUG
	const size_t MAX_LOADED_MEMORY = (1ull << 28); // 256 MB
#else
	const size_t MAX_LOADED_MEMORY = 4ull * (1ull << 30); // 4 GB
#endif
	size_t m_currentLoadedMemory = 0;

	bool m_finish = false;
};

}  // namespace chag
