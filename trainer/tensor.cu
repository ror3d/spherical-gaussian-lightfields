#include "error_handling.h"
#include "tensor.h"
#include <cuda_runtime.h>
#include <cudnn.h>
#include <random>
using namespace std;

namespace dnn {
std::default_random_engine generator;

void tensor4D<cudnnTensorDescriptor_t>::Resize(int n, int c, int h, int w, bool _has_gradient) {
	has_gradient = _has_gradient;
	dim          = {n, c, h, w};
	checkCudnnErr(cudnnCreateTensorDescriptor(&descriptor));
	checkCudnnErr(cudnnSetTensor4dDescriptor(descriptor, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, n, c, h, w));
	checkCudaErr(cudaMalloc(&d_data, SizeInBytes()));
	if(has_gradient) {
		checkCudaErr(cudaMalloc(&d_grad, SizeInBytes()));
		checkCudaErr(cudaMemset(d_grad, 0, SizeInBytes()));
	}
}
void tensor4D<cudnnFilterDescriptor_t>::Resize(int n, int c, int h, int w, bool _has_gradient) {
	has_gradient = _has_gradient;
	dim = { n, c, h, w };
	checkCudnnErr(cudnnCreateFilterDescriptor(&descriptor));
	checkCudnnErr(cudnnSetFilter4dDescriptor(descriptor, CUDNN_DATA_FLOAT, CUDNN_TENSOR_NCHW, n, c, h, w));
	checkCudaErr(cudaMalloc(&d_data, SizeInBytes()));
	if(has_gradient) {
		checkCudaErr(cudaMalloc(&d_grad, SizeInBytes()));
		checkCudaErr(cudaMemset(d_grad, 0, SizeInBytes()));
	}
}

template <typename DESCRIPTOR_TYPE>
void tensor4D<DESCRIPTOR_TYPE>::ResetRandom(float variance) {
	// Assuming no initialization for gradient.
	float stddev = sqrt(variance);
	float mean   = 0.0f;
	std::normal_distribution<float> distribution(mean, stddev);
	vector<float> values(NumValues());
	for(int i = 0; i < NumValues(); i++) values[i] = distribution(generator);
	checkCudaErr(cudaMemcpy(d_data, values.data(), SizeInBytes(), cudaMemcpyHostToDevice));
}

template <typename DESCRIPTOR_TYPE>
void tensor4D<DESCRIPTOR_TYPE>::ResetZero() {
	checkCudaErr(cudaMemset(d_data, 0, SizeInBytes()));
	if(has_gradient) checkCudaErr(cudaMemset(d_grad, 0, SizeInBytes()));
}

template <typename DESCRIPTOR_TYPE>
vector<float> tensor4D<DESCRIPTOR_TYPE>::DownloadFlatData() const {
	vector<float> values(NumValues());
	checkCudaErr(cudaMemcpy(values.data(), d_data, SizeInBytes(), cudaMemcpyDeviceToHost));
	return values;
}

template <typename DESCRIPTOR_TYPE>
vector<float> tensor4D<DESCRIPTOR_TYPE>::DownloadFlatGrad() const {
	vector<float> values(NumValues());
	checkCudaErr(cudaMemcpy(values.data(), d_grad, SizeInBytes(), cudaMemcpyDeviceToHost));
	return values;
}

template <typename DESCRIPTOR_TYPE>
void tensor4D<DESCRIPTOR_TYPE>::CheckForBadFloat() {
	vector<float> values(NumValues());
	checkCudaErr(cudaMemcpy(values.data(), d_data, SizeInBytes(), cudaMemcpyDeviceToHost));
	::CheckForBadFloat(values);
	if (has_gradient) {
		checkCudaErr(cudaMemcpy(values.data(), d_grad, SizeInBytes(), cudaMemcpyDeviceToHost));
		::CheckForBadFloat(values);
	}
}

template <typename DESCRIPTOR_TYPE>
void tensor4D<DESCRIPTOR_TYPE>::SetData(float* host_data) {
	checkCudaErr(cudaMemcpy(d_data, host_data, SizeInBytes(), cudaMemcpyHostToDevice));
}

template <typename DESCRIPTOR_TYPE>
void tensor4D<DESCRIPTOR_TYPE>::SetGrad(float* host_data) {
	checkCudaErr(cudaMemcpy(d_grad, host_data, SizeInBytes(), cudaMemcpyHostToDevice));
}

template<typename DESCRIPTOR_TYPE>
tensor4D<DESCRIPTOR_TYPE>::tensor4D( tensor4D && t )
{
	*this = std::move( t );
}

template<typename DESCRIPTOR_TYPE>
tensor4D<DESCRIPTOR_TYPE> & tensor4D<DESCRIPTOR_TYPE>::operator=( tensor4D && t )
{
	std::swap( d_data, t.d_data );
	std::swap( has_gradient, t.has_gradient );
	std::swap( d_grad, t.d_grad );
	std::swap( dim, t.dim );
	std::swap( descriptor, t.descriptor );
	return *this;
}

template <typename DESCRIPTOR_TYPE>
tensor4D<DESCRIPTOR_TYPE>::~tensor() {
	if ( d_data )
	{
		checkCudaErr( cudaFree( d_data ) );
	}
	if ( has_gradient && d_grad )
	{
		checkCudaErr( cudaFree( d_grad ) );
	}
}

// Explicit instantiation of both types of tensors
template struct tensor4D<cudnnTensorDescriptor_t>;
template struct tensor4D<cudnnFilterDescriptor_t>;

};  // namespace dnn