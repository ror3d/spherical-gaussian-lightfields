#include <iostream>
#include "opengl_context_init.h"
#include "dnn_trainer.h"
#include <GL/glew.h>
#include <utils/opengl_helpers.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>
#include <filesystem>
#include "preprocessing.h"
#include <string>

#include <cuda.h>


using namespace opengl_helpers; 
using std::cout;
using namespace std::string_literals;

void ExitWithUsageInfo(int argc, char *argv[])
{
	cout << "Usage:\n";
	cout << std::filesystem::path(argv[0]).filename() << "[_OPTIONS_] <input.json|directory>\n";
	cout << "\t\t-j\tJitter (default: disabled)\n";
	cout << "\t\t-d\tDropout (default: disabled)\n";
	cout << "\t\t-f\tFixed Directions (default: disabled)\n";
	cout << "\t\t-h\tConstrain to upper hemisphere (default: disabled)\n";
	cout << "\t\t-s\tTrain islands separately (default: disabled)\n";
	cout << "\t\t-e N\tLimit to N epochs (default: 0 (no limit))\n";
	cout << "\t\t-g N\tUse N gaussians per texel (default: 16, max: 256)\n";
	cout << "\t\t-p s\tOutput name prefix (default: gaussian_tex)\n";
	cout << "\t\t-o s\tOutput path, implies -ps (relative to the input json) (default: output)\n";
	cout << "\t\t-i\tSpecify a file from which to load the network structure and data\n";
	cout << "\t\t-ps\tAdd settings suffix to the filename instead of the path\n";
	cout << "\t\t\t-Ps\tAdd settings suffix to the path instead of the filename\n";
	cout << "\t\t-l\tRamp up (and down) learning rate\n";
	cout << "\t\t-C r,g,b\tSelect input channels to train (default: 0,1,2)\n";
	cout << "\t\t-C3\tShortcut for -C 3,4,5\n";
	cout << "\t\t-n\tDirect Optimization (no network) (default: disabled)\n";
	cout << "\t\t-D ID\tChoose CUDA Device by ID\n";
	cout << std::filesystem::path(argv[0]).filename() << "--convolve <input.json|directory>\n";
	cout << std::filesystem::path(argv[0]).filename() << "--scramble <input.json|directory>\n";
	cout << std::filesystem::path(argv[0]).filename() << "--unscramble <input.json>\n";
	cout << std::filesystem::path(argv[0]).filename() << "--list-devices\n";
	exit(-1);
}

std::string getFileInPath( std::string path, const std::string& extension = ".json" )
{
	std::filesystem::path fp = path;
	if ( std::filesystem::is_directory( fp ) )
	{
		path = "";
		for ( auto dentry : std::filesystem::directory_iterator( fp ) )
		{
			if ( dentry.path().extension() == extension )
			{
				path = dentry.path().string();
				break;
			}
		}
		if ( path.empty() )
		{
			cout << "No " << extension << " file found in path " + fp.string() + ".\n";
		}
	}
	else if ( fp.extension() != extension )
	{
		cout << "File " << path << " doesn't have the " << extension << " extension.\n";
		path = "";
	}
	return path;
}

int main(int argc, char * argv[])
{
    ///////////////////////////////////////////////////////////////////////////
    // Set up OpenGL context and initialize GLEW
    ///////////////////////////////////////////////////////////////////////////
    SetUpDummyWindow(); 
    glewInit(); 
    cout << glGetString(GL_VERSION) << "\n";

    ///////////////////////////////////////////////////////////////////////////
    // Parse options
    ///////////////////////////////////////////////////////////////////////////
	if (argc <= 1) ExitWithUsageInfo(argc, argv);
	if (std::string(argv[1]) == "--convolve") {
		if(argc != 3) ExitWithUsageInfo(argc, argv);
		std::string file = getFileInPath( argv[2] );
		if ( file.empty() )
		{
			ExitWithUsageInfo( argc, argv );
		}
		preprocessing::Convolve(file);
		exit(0);
	}

	if (std::string(argv[1]) == "--scramble") {
		if (argc != 3) ExitWithUsageInfo(argc, argv);
		std::string file = getFileInPath( argv[2] );
		if ( file.empty() )
		{
			ExitWithUsageInfo( argc, argv );
		}
		preprocessing::Scramble(file);
		exit(0);
	}

	if (std::string(argv[1]) == "--list-devices") {
		int count = 0;
		cudaError_t err = cudaGetDeviceCount( &count );
		for ( int i = 0; i < count; ++i )
		{
			cudaDeviceProp props;
			err = cudaGetDeviceProperties( &props, i );
			cout << i << "\t-\t" << props.name << "\t" << props.major << "." << props.minor << "\n";
		}
		exit(0);
	}
	std::string filepath;
	std::string filename_prefix = "gaussian_tex";
	std::string output_path = "output";
	std::string input_filename = "";
	uint16_t num_gaussians = 16;
	uint32_t max_epochs = 0;
	bool drop_enabled = false;
	bool jitter_enabled = false;
	bool islands = false;
	bool suffix_in_filename = false;
	std::array<uint16_t, 3> channel_remap = { 0, 1, 2 };
	bool channels_remapped = false;
	bool fixed_directions = false; 
	bool constrain_upper_hemisphere = false; 
	bool ramp_learning_rate = false; 
	bool no_network = false; 
	int device_id = 0;

	for ( size_t i = 1; i < argc; ++i )
	{
		if ( argv[i][0] == '-' )
		{
			if ( argv[i] == "-j"s )
			{
				jitter_enabled = true;
			}
			else if ( argv[i] == "-d"s )
			{
				drop_enabled = true;
			}
			else if (argv[i] == "-f"s)
			{
				fixed_directions = true; 
			}
			else if (argv[i] == "-h"s)
			{
				constrain_upper_hemisphere = true;
			}
			else if ( argv[i] == "-s"s )
			{
				islands = true;
			}
			else if ( argv[i] == "-e"s )
			{
				if ( i + 1 == argc )
				{
					cout << "Count parameter not provided for -e flag!\n";
					ExitWithUsageInfo( argc, argv );
				}
				++i;
				max_epochs = std::stoul( argv[i] );
			}
			else if ( argv[i] == "-g"s )
			{
				if ( i + 1 == argc )
				{
					cout << "Count parameter not provided for -g flag!\n";
					ExitWithUsageInfo( argc, argv );
				}
				++i;
				num_gaussians = uint16_t(std::stoul( argv[i] ));
				if ( num_gaussians > 256 )
				{
					cout << "Maybe you have too many gaussians there?\n";
					ExitWithUsageInfo( argc, argv );
				}
			}
			else if ( argv[i] == "-p"s )
			{
				if ( i + 1 == argc )
				{
					cout << "Prefix parameter not provided to -p\n";
					ExitWithUsageInfo( argc, argv );
				}
				++i;
				filename_prefix = argv[i];
			}
			else if ( argv[i] == "-o"s )
			{
				if ( i + 1 == argc )
				{
					cout << "Path parameter not provided to -o\n";
					ExitWithUsageInfo( argc, argv );
				}
				++i;
				output_path = argv[i];
				suffix_in_filename = true;
			}
			else if ( argv[i] == "-i"s )
			{
				if ( i + 1 == argc )
				{
					cout << "Path parameter not provided to -i\n";
					ExitWithUsageInfo( argc, argv );
				}
				++i;
				input_filename = argv[i];
			}
			else if ( argv[i] == "-ps"s )
			{
				suffix_in_filename = true;
			}
			else if ( argv[i] == "-Ps"s )
			{
				suffix_in_filename = false;
			}
			else if (argv[i] == "-l"s)
			{
				ramp_learning_rate = true;
			}
			else if ( argv[i] == "-C"s )
			{
				if ( i + 1 == argc )
				{
					cout << "Path parameter not provided to -o\n";
					ExitWithUsageInfo( argc, argv );
				}
				++i;
				uint32_t c0, c1, c2;
				sscanf_s( argv[i], "%u,%u,%u", &c0, &c1, &c2 );
				channel_remap = { uint16_t( c0 ), uint16_t( c1 ), uint16_t( c2 ) };
				channels_remapped = true;
			}
			else if ( argv[i] == "-C3"s )
			{
				channel_remap = { 3, 4, 5 };
				channels_remapped = true;
			}
			else if (argv[i] == "-n"s)
			{
				no_network = true;
			}
			else if ( argv[i] == "-D"s )
			{
				if ( i + 1 == argc )
				{
					cout << "Device ID parameter not provided for -D\n";
					ExitWithUsageInfo( argc, argv );
				}
				++i;
				device_id = std::stoi( argv[i] );
				int device_count = 0;
				cudaGetDeviceCount( &device_count );
				if ( device_id < 0 || device_id >= device_count )
				{
					cout << "Invalid Device ID parameter provided for -D.\nRun with --device-list to get a list of devices available.\n";
					ExitWithUsageInfo( argc, argv );
				}
			}
		}
		else
		{
			filepath = argv[i];
			if ( argc > i + 1 )
			{
				cout << "WARNING: Some parameters were not used, they must come before the input json file:\n";
				for ( size_t j = i+1; j < argc; ++j )
				{
					cout << "\t" << argv[j] << "\n";
				}
				cout << std::endl;
			}
			break;
		}
	}

	if ( filepath.empty() )
	{
		cout << "Must provide a filename.\n";
		ExitWithUsageInfo( argc, argv );
	}
	else if ( !std::filesystem::exists( filepath ) )
	{
		cout << "File " << filepath << " doesn't exist.\n";
		ExitWithUsageInfo( argc, argv );
	}
	else
	{
		std::filesystem::path fp = filepath;
		if ( std::filesystem::is_directory( fp ) )
		{
			filepath = "";
			for ( auto dentry : std::filesystem::directory_iterator( fp ) )
			{
				if ( dentry.path().extension() == ".json" )
				{
					filepath = dentry.path().string();
					break;
				}
			}
			if ( filepath.empty() )
			{
				cout << "No json file found in path " + fp.string() + ".\n";
				ExitWithUsageInfo( argc, argv );
			}
		}
		else if ( fp.extension() != ".json" )
		{
			cout << "File " << filepath << " doesn't look like a json file.\n";
			ExitWithUsageInfo( argc, argv );
		}
	}

	// Fix output path and filename prefix
	{
		if ( output_path.back() == '\\' || output_path.back() == '/' )
		{
			output_path = output_path.substr( 0, output_path.length() - 1 );
		}

		std::string settings_suffix = ""s
			+ (jitter_enabled ? "_jitter" : "")
			+ (drop_enabled ? "_drop" : "")
			+ (fixed_directions ? "_fixed" : "")
			+ (constrain_upper_hemisphere ? "_upper" : "")
			+ (ramp_learning_rate ? "_ramp" : "")
			+ (islands ? "_islands" : "");
		if ( channels_remapped )
		{
			settings_suffix += "_channels_"s + std::to_string( channel_remap[0] ) + "_" + std::to_string( channel_remap[1] ) + "_"
				+ std::to_string( channel_remap[2] );
		}

		settings_suffix += "_g" + std::to_string(num_gaussians);

		if ( suffix_in_filename )
		{
			filename_prefix += settings_suffix;
		}
		else
		{
			output_path += settings_suffix;
		}
		if ( !output_path.empty() )
		{
			output_path += "/";
		}
	}

	cudaDeviceProp device_props;
	cudaGetDeviceProperties( &device_props, device_id );

	cout << "Starting training of " << filepath << " with settings:"
		<< "\n\tInitial parameters file = " << (input_filename.empty() ? "<none>" : input_filename)
		<< "\n\tFilename prefix = " << filename_prefix
		<< "\n\tOutput path: \"" << output_path << "\""
		<< "\n\tMax epochs = " << max_epochs
		<< "\n\tNum gaussians = " << num_gaussians
		<< "\n\tJitter " << (jitter_enabled ? "enabled" : "disabled")
		<< "\n\tDropout " << (drop_enabled ? "enabled" : "disabled")
		<< "\n\tChannels: r=" << channel_remap[0] << ", g=" << channel_remap[1] << ", b=" << channel_remap[2]
		<< "\n\tFixed Directions " << (fixed_directions ? "enabled" : "disabled")
		<< "\n\tConstrain Upper " << (constrain_upper_hemisphere ? "enabled" : "disabled")
		<< "\n\tRamping learning rate " << (ramp_learning_rate ? "enabled" : "disabled")
		<< "\n\tDirect optimization " << (no_network ? "enabled" : "disabled")
		<< "\n\tCUDA Device " << device_id << " " << device_props.name
		<< "\n--------------\n";


	dnn::TrainerSettings settings;

	settings.max_epochs = max_epochs;
	settings.num_gaussians = num_gaussians;
	settings.separate_islands = islands;
	settings.jitter_directions = jitter_enabled;
    settings.fixed_directions = fixed_directions;
	settings.constrain_upper_hemisphere = constrain_upper_hemisphere;
	settings.drop_random_gaussians = drop_enabled;
	settings.ramp_learning_rate = ramp_learning_rate;
	settings.channel_remap = channel_remap;
	settings.output_path = output_path;
	settings.filename_prefix = filename_prefix;
	settings.initial_network_params = input_filename;
	settings.cuda_device = device_id;

	///////////////////////////////////////////////////////////////////////////
	// Initialize dnn 
	///////////////////////////////////////////////////////////////////////////
	dnn::Init( settings );
	if (!no_network) {
		dnn::TrainWithoutInterpolation(filepath);
	}
	else {
		dnn::DirectOptimization(filepath);
	}

	return 0;
}


///////////////////////////////////////////////////////////////////////////
// To allow creating directory from CUDA file
///////////////////////////////////////////////////////////////////////////
namespace filesystem
{
	void create_directory(const std::string& d)
	{
		std::filesystem::create_directory(d);
	}
}
