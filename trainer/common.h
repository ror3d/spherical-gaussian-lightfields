#pragma once
/////////////////////////////////////////////////////////////////////////////
// Currently hardcoded input information
/////////////////////////////////////////////////////////////////////////////
#include "../include/spherical_projection_common.h"
const int BATCH_SIZE = 32;
const int IMAGE_CHANNELS = 3;
const int EVAL_BATCH_SIZE = ( ENVMAP_SIZE / 2 ) * ( ENVMAP_SIZE / 2 );

#include <string>

namespace filesystem
{
void create_directory( const std::string& d );
}

#define ADAM_T_VAR s_epoch
