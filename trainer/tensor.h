#pragma once

#include <vector>
#include <cuda_runtime.h>
#include <cudnn.h>

namespace dnn {
/////////////////////////////////////////////////////////////////////////////
// A 4d tensor
/////////////////////////////////////////////////////////////////////////////
struct tensor_dimension {
	int n = 0, c = 0, h = 0, w = 0;
	bool operator==(const tensor_dimension& b) const { return (n == b.n) && (c == b.c) && (h == b.h) && (w == b.w); }
	bool operator!=(const tensor_dimension& b) const { return !(*this == b); }
};

// Templated to be either a cudnnTensorDescriptor_t or a cudnnFilterDescriptor_t
template<typename DESCRIPTOR_TYPE>
struct tensor4D {
	DESCRIPTOR_TYPE descriptor;
	tensor_dimension dim;
	float* d_data = nullptr;
	float* d_grad = nullptr;
	bool has_gradient;

public:
	int NumValues() const { return dim.n * dim.c * dim.h * dim.w; };
	uint64_t SizeInBytes() const { return NumValues() * sizeof(float); }
	void Resize(int n, int c, int h, int w, bool _has_gradient = true);
	void ResetRandom(float variance);
	void ResetZero();
	std::vector<float> DownloadFlatData() const;
	std::vector<float> DownloadFlatGrad() const;
	void CheckForBadFloat();
	void SetData(float* host_data);
	void SetGrad(float* host_data);
	tensor4D() = default;
	tensor4D(tensor4D&&);
	tensor4D(const tensor4D&) = default;
	tensor4D& operator=(tensor4D&&);
	tensor4D& operator=(const tensor4D&) = default;
	tensor4D(int n, int c, int h, int w, bool has_gradient = true) { Resize(n, c, h, w, has_gradient); }
	~tensor4D();
};

typedef tensor4D<cudnnTensorDescriptor_t> tensor;
typedef tensor4D<cudnnFilterDescriptor_t> filter;

}  // namespace dnn