#include <cuda_runtime.h>
#include <assert.h>
#include <cudnn.h>
#include <vector>
#include <string>
#include <array>

namespace dnn
{
	struct TrainerSettings
	{
		uint32_t num_gaussians = 16;
		bool drop_random_gaussians = false;
		bool jitter_directions = false;
		bool fixed_directions = false;
		bool constrain_upper_hemisphere = false; 
		bool separate_islands = false;
		bool ramp_learning_rate = false;
		uint32_t max_epochs = 0;
		std::array<uint16_t, 3> channel_remap = { 0, 1, 2 };
		std::string filename_prefix;
		std::string output_path;
		std::string initial_network_params;
		uint32_t cuda_device = 0;
	};

	void Init( TrainerSettings settings );
	void TrainWithoutInterpolation(const std::string & filepath);
	void DirectOptimization(const std::string & filepath);
}