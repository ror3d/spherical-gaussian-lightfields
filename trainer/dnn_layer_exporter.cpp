#include "dnn_layer_exporter.h"

#include <fstream>
#include <string>
#include <iostream>
#include <vector>

#include <utils/json.hpp>
#include <utils/json_extras.hpp>
#include <utils/base64.h>
#include <utils/Log.h>

#include "layers.h"
#include "error_handling.h"

using std::vector;
using std::string;
using std::to_string;
using std::cout;
using std::flush;
using std::ofstream;
using std::ifstream;
using std::endl;
using namespace dnn;
using namespace nlohmann;


namespace dnn
{
extern cudnnHandle_t cudnn_handle;

extern uint64_t s_epoch;
extern uint64_t s_batch;
}

namespace chag
{

void exportDnnLayers( ofstream& file, const vector<layer*>& layers, const vector<tensor*>& layer_outputs, const tensor& final_output )
{
	json js;

	auto getBase64data = []( void* data, size_t num_bytes ) {
		return b64encode( data, num_bytes );
	};

	vector<float> data;
	auto getBase64floatData = [getBase64data, &data]( const float* filter_data, size_t num_filter_values ) {
		data.resize( num_filter_values );
		checkCudaErr( cudaMemcpy( data.data(), filter_data, num_filter_values * sizeof( float ), cudaMemcpyDeviceToHost ) );
		return getBase64data( data.data(), num_filter_values * sizeof( float ) );
	};

	json js_layers = json::array();

	for ( size_t i = 0; i < layers.size(); ++i )
	{
		json jsfilter = json::object();
		const layer* l = layers[i];

		// Get layer type and parameters, write them out to file
		if ( const convolution_layer* cl = dynamic_cast<const convolution_layer*>(l); cl != nullptr )
		{
			jsfilter["type"] = "convolution";
			jsfilter["dimensions"] = glm::ivec4( cl->f.dim.n, cl->f.dim.c, cl->f.dim.h, cl->f.dim.w );

			// TODO: Descriptor info
			//cudnnGetConvolutionNdDescriptor()
			//jsfilter["descriptor"] = getBase64data(cl->convolution_descriptor)

			jsfilter["f"] = getBase64floatData( cl->f.d_data, cl->f.NumValues() );
			jsfilter["bias"] = getBase64floatData( cl->bias.d_data, cl->bias.NumValues() );

			jsfilter["adam_s"] = getBase64floatData( cl->d_adam_s, cl->f.NumValues() + cl->bias.NumValues() );
			jsfilter["adam_r"] = getBase64floatData( cl->d_adam_r, cl->f.NumValues() + cl->bias.NumValues() );
			jsfilter["alpha"] = cl->alpha;
			jsfilter["beta"] = cl->beta;
		}
		else if ( const pool_layer* pl = dynamic_cast<const pool_layer*>(l); pl != nullptr )
		{
			jsfilter["type"] = "pool";

			jsfilter["alpha"] = pl->alpha;
			jsfilter["beta"] = pl->beta;
			// TODO: Descriptor info
		}
		else if ( const activation_layer* al = dynamic_cast<const activation_layer*>(l); al != nullptr )
		{
			jsfilter["type"] = "activation";

			// TODO: Descriptor info

			cudnnActivationMode_t mode;
			cudnnNanPropagation_t reluNanOpt;
			double coef;

			cudnnGetActivationDescriptor( al->activation_descriptor, &mode, &reluNanOpt, &coef );
			jsfilter["mode"] = (int)mode;

			jsfilter["alpha"] = al->alpha;
			jsfilter["beta"] = al->beta;
		}
		else if ( const fc_layer* fl = dynamic_cast<const fc_layer*>(l); fl != nullptr )
		{
			jsfilter["type"] = "fully-connected";

			jsfilter["input_nodes"] = fl->num_input_nodes;
			jsfilter["output_nodes"] = fl->num_output_nodes;

			jsfilter["weights"] = getBase64floatData( fl->weights.d_data, fl->weights.NumValues() );
			jsfilter["bias"] = getBase64floatData( fl->bias.d_data, fl->bias.NumValues() );

			jsfilter["adam_s"] = getBase64floatData( fl->d_adam_s, fl->weights.NumValues() + fl->bias.NumValues() );
			jsfilter["adam_r"] = getBase64floatData( fl->d_adam_r, fl->weights.NumValues() + fl->bias.NumValues() );
			jsfilter["alpha"] = fl->alpha;
			jsfilter["beta"] = fl->beta;
		}
		else if ( const softmax_layer* sl = dynamic_cast<const softmax_layer*>(l); sl != nullptr )
		{
			jsfilter["type"] = "softmax";
		}
		else if ( const dropout_layer* dl = dynamic_cast<const dropout_layer*>(l); dl != nullptr )
		{
			jsfilter["type"] = "dropout";

			// TODO: Descriptor info
			// TODO: States info?
			float dropout;
			void* states;
			unsigned long long seed;
			cudnnGetDropoutDescriptor( dl->descriptor, cudnn_handle, &dropout, &states, &seed );
			jsfilter["p"] = dropout;
		}
		else if ( const upsample_layer* ul = dynamic_cast<const upsample_layer*>(l); ul != nullptr )
		{
			jsfilter["type"] = "upsample";
		}
		else
		{
			DebugError( "Unknown layer type for layer number " << i );
			return;
		}

		js_layers.push_back( jsfilter );
	}

	js["layers"] = js_layers;

	js["epoch"] = s_epoch;
	js["batch"] = s_batch;

	json js_tensors = json::array();
	for ( size_t i = 0; i < layer_outputs.size(); ++i )
	{
		const tensor* t = layer_outputs[i];

		json jst;
		jst["dim"] = glm::ivec4( t->dim.n, t->dim.c, t->dim.h, t->dim.w );
		jst["gradient"] = t->has_gradient;

		js_tensors.push_back( jst );
	}

	js["intermediate_output_tensors"] = js_tensors;


	json jst;
	jst["dim"] = glm::ivec4( final_output.dim.n, final_output.dim.c, final_output.dim.h, final_output.dim.w );
	jst["gradient"] = final_output.has_gradient;
	
	js["final_output_tensor"] = jst;

	file << js.dump() << "\n";
}

bool importDnnLayers( ifstream& file, vector<layer*>& layers, vector<tensor*>& layer_outputs, tensor& final_output )
{
	json js;
	try
	{
		if ( file )
		{
			js = json::parse( file );
		}
		else
		{
			LOG_ERROR( "Failed to load dnn file." );
			return false;
		}
	}
	catch ( std::exception& e )
	{
		LOG_ERROR( "Error when loading dnn file." );
		DebugError( e.what() );
		return false;
	}

	auto getFloatDataFromBase64 = []( float* filter_data, size_t expected_size, const std::string& b64 ) {
		auto data = b64decode( b64.data(), b64.size() );
		if ( data.size() != expected_size )
		{
			LOG_ERROR("Input data size doesn't match target tensor size in the device!");
			return;
		}
		checkCudaErr( cudaMemcpy( filter_data, data.data(), data.size(), cudaMemcpyHostToDevice ) );
	};

	// TODO: Check it exists
	json jslayers = js["layers"];

	if ( !jslayers.is_array() )
	{
		LOG_ERROR( "Dnn file with wrong format. Expected 'layers' to be an array." );
		return false;
	}

	// TODO: Check it exists
	json jstensors = js["intermediate_output_tensors"];

	if ( !jstensors.is_array() )
	{
		LOG_ERROR( "Dnn file with wrong format. Expected 'intermediate_output_tensors' to be an array." );
		return false;
	}

	if ( jstensors.size() != jslayers.size() )
	{
		LOG_ERROR( "Number of intermediate tensors doesn't correspond to number of layers! " << jstensors.size() << "!=" << jslayers.size() );
		return false;
	}

	// Load intermediate tensors
	for ( size_t i = 0; i < jstensors.size(); ++i )
	{
		json jt = jstensors[i];
		glm::ivec4 dim = jt["dim"];
		bool has_gradient = jt["gradient"];

		layer_outputs.push_back( new tensor( dim.x, dim.y, dim.z, dim.w, has_gradient ) );
	}

	// Load layers
	for ( size_t i = 0; i < jslayers.size(); ++i )
	{
		json jl = jslayers[i];

		std::string type = jl.value( "type", "" );

		layer* l = nullptr;

		if ( type == "convolution" )
		{
			glm::ivec4 dim = jl.value( "dimensions", glm::ivec4{} );

			// dimensions is stored from n, c, h, w; n represents output channels, c represents input, so they are swapped here
			convolution_layer* cl = new convolution_layer( dim.y, dim.x, dim.z );

			std::string f = jl.value( "f", "" );
			std::string bias = jl.value( "bias", "" );

			getFloatDataFromBase64( cl->f.d_data, cl->f.SizeInBytes(), f );
			getFloatDataFromBase64( cl->bias.d_data, cl->bias.SizeInBytes(), bias );

			std::string adam_r = jl.value( "adam_r", "" );
			std::string adam_s = jl.value( "adam_s", "" );
			if ( !adam_r.empty() )
			{
				getFloatDataFromBase64( cl->d_adam_r, cl->f.SizeInBytes() + cl->bias.SizeInBytes(), adam_r );
				getFloatDataFromBase64( cl->d_adam_s, cl->f.SizeInBytes() + cl->bias.SizeInBytes(), adam_s );
			}

			cl->alpha = jl.value( "alpha", 1.0f );
			cl->beta = jl.value( "beta", 0.0f );

			if ( i == 0 )
			{
				tensor tmp_input_tensor( layer_outputs[i]->dim.n, dim.y, layer_outputs[i]->dim.h, layer_outputs[i]->dim.w );
				cl->FindAlgorithmsAndWorkspace( tmp_input_tensor, *layer_outputs[i] );
			}
			else
			{
				cl->FindAlgorithmsAndWorkspace( *layer_outputs[i-1], *layer_outputs[i] );
			}


			l = cl;
		}
		else if ( type == "pool" )
		{
			pool_layer* pl = new pool_layer();

			pl->alpha = jl.value( "alpha", 1.0f );
			pl->beta = jl.value( "beta", 0.0f );

			l = pl;
		}
		else if ( type == "activation" )
		{
			cudnnActivationMode_t mode = (cudnnActivationMode_t)jl.value( "mode", 1 );
			activation_layer* al = new activation_layer( mode );

			al->alpha = jl.value( "alpha", 1.0f );
			al->beta = jl.value( "beta", 0.0f );

			l = al;
		}
		else if ( type == "fully-connected" )
		{
			int input = jl.value( "input_nodes", 0 );
			int output = jl.value( "output_nodes", 0 );

			fc_layer* fc = new fc_layer( input, output );

			std::string weights = jl.value( "weights", "" );
			std::string bias = jl.value( "bias", "" );

			getFloatDataFromBase64( fc->weights.d_data, fc->weights.SizeInBytes(), weights );
			getFloatDataFromBase64( fc->bias.d_data, fc->bias.SizeInBytes(), bias );

			std::string adam_r = jl.value( "adam_r", "" );
			std::string adam_s = jl.value( "adam_s", "" );
			if ( !adam_r.empty() )
			{
				getFloatDataFromBase64( fc->d_adam_r, fc->weights.SizeInBytes() + fc->bias.SizeInBytes(), adam_r );
				getFloatDataFromBase64( fc->d_adam_s, fc->weights.SizeInBytes() + fc->bias.SizeInBytes(), adam_s );
			}
			fc->alpha = jl.value( "alpha", 1.0f );
			fc->beta = jl.value( "beta", 0.0f );


			l = fc;
		}
		else if ( type == "softmax" )
		{
			softmax_layer* sm = new softmax_layer();
			l = sm;
		}
		else if ( type == "dropout" )
		{
			float p = jl.value( "p", 0.f );
			dropout_layer* dl = new dropout_layer(p);

			l = dl;
		}
		else if ( type == "upsample" )
		{
			upsample_layer* ul = new upsample_layer();
			l = ul;
		}
		else
		{
			LOG_ERROR( "Unrecognized layer type " << type );
			return false;
		}

		layers.push_back( l );
	}

	{
		json jsft = js["final_output_tensor"];
		glm::ivec4 dim = jsft["dim"];
		bool has_gradient = jsft["gradient"];
		final_output = std::move( tensor( dim.x, dim.y, dim.z, dim.w, has_gradient ) );
	}

	s_epoch = js.value( "epoch", 0 );
	s_batch = js.value( "batch", 0 );
}

}
