#include "training_data_scheduler.h"
#include <utils/json.hpp>
#include <utils/json_extras.hpp>
#include <utils/half.hpp>
#include <utils/Log.h>
#include <fstream>
#include <utils/pcg/pcg_random.hpp>
#include <algorithm>
#include <random>
#include "../include/spherical_projection_common.h"

#ifdef _DEBUG
#define DebugOutput(x) OutputDebugStringA((std::string(x) + "\n").c_str())
#else
#define DebugOutput(x)
#endif

namespace chag
{

TrainingDataScheduler::TrainingDataScheduler( const std::string& data_source_file_path, const std::array<uint16_t, 3>& channel_remap )
{
	initialize( data_source_file_path, channel_remap );
}

TrainingDataScheduler::TrainingDataScheduler()
{
}

TrainingDataScheduler::~TrainingDataScheduler()
{
	{
		std::unique_lock<std::recursive_mutex> lock( m_loadedDataMtx );
		m_loadedTrainingData.clear();
		m_finish = true;
	}
	m_onMetadataAvailable.notify_all();
	if ( m_loaderThread.joinable() )
	{
		m_loaderThread.join();
	}
}

void TrainingDataScheduler::initialize( const std::string & data_source_file_path, const std::array<uint16_t, 3>& channel_remap )
{
	if ( m_loaderThread.get_id() != std::thread::id() )
	{
		return;
	}
	m_channel_remap = channel_remap;
	loadTrainingMetadata( data_source_file_path );
	m_loaderThread = std::thread( [this]() { this->loaderThreadFn(); } );
}

size_t TrainingDataScheduler::getTrainingSetSize() const
{
	return m_trainDataChunks.size();
}

size_t TrainingDataScheduler::getEpochDataSetsRemaining() const
{
	return m_epochSetsRemaining;
}

void TrainingDataScheduler::prepareBasicRandomEpoch()
{
	std::unique_lock<std::mutex> lock( m_metadataMtx );
	DebugAssert( m_randomizedDataChunkIdxs.empty() );
	m_randomizedDataChunkIdxs.clear();

	std::vector<size_t> randomizedDataChunkIdxs;
	randomizedDataChunkIdxs.reserve( m_trainDataChunks.size() );


	// Randomize
	{
		static int seed = 0; 
		pcg64 rng( seed++ );

		// Randomize batch order:
		std::vector<size_t> randomizedBatchIdxs;
		randomizedBatchIdxs.reserve( m_batchTexelIndices.size() );
		for ( size_t i = 0; i < m_batchTexelIndices.size(); ++i )
		{
			randomizedBatchIdxs.push_back( i );
		}
		std::shuffle( randomizedBatchIdxs.begin(), randomizedBatchIdxs.end(), rng );

		// Fill the texel list with the indices of the texels for each randomized batch
		for ( size_t i = 0; i < randomizedBatchIdxs.size(); ++i )
		{
			auto& texelIndices = m_batchTexelIndices[randomizedBatchIdxs[i]];
			for ( size_t j = 0; j < texelIndices.size(); ++j )
			{
				randomizedDataChunkIdxs.push_back( texelIndices[j] );
			}
		}

		// Divide the texel indices in blocks of max size MAX_LOADED_MEMORY/2
		size_t flt_sz = sizeof( decltype(m_loadedTrainingBatches)::value_type::second_type::value_type );
		size_t subrandomBatches = 2 * std::max(1ull, (randomizedDataChunkIdxs.size() * m_dataChunkSize * flt_sz) / MAX_LOADED_MEMORY);
		size_t subrandomBatchSize = randomizedDataChunkIdxs.size() / subrandomBatches;
		if ( subrandomBatches * subrandomBatchSize < randomizedDataChunkIdxs.size() )
		{
			subrandomBatches += 1;
		}

		// Randomize the order of the texels inside each of these blocks
		for ( size_t b = 0; b < subrandomBatches; ++b )
		{
			size_t begin = b * subrandomBatchSize;
			size_t end = std::min( begin + subrandomBatchSize + 1, randomizedDataChunkIdxs.size() );
			std::shuffle( randomizedDataChunkIdxs.data() + begin, randomizedDataChunkIdxs.data() + end, rng );
		}
	}

	// Set the randomized data
	m_randomizedDataChunkIdxs.swap( randomizedDataChunkIdxs );

	m_epochSetsRemaining = m_randomizedDataChunkIdxs.size();
}

void TrainingDataScheduler::prepareNewEpoch()
{
	DebugOutput( "new Epoch" );

	prepareBasicRandomEpoch();

	m_firstEpochPrepared = true;
	m_onMetadataAvailable.notify_all();
}

void TrainingDataScheduler::prepareNewInterpolationEpoch()
{
	DebugOutput( "new Interpolation Epoch" );

	prepareBasicRandomEpoch();


	// Add each of the indices followed by the indices of the adjacent textures so we can take them together
	int num_sets = 0; 
	int valid_corners = 0; 
	int valid_edges = 0; 
	{
		std::vector<size_t> randomizedDataChunkIdxs;
		randomizedDataChunkIdxs.reserve( 2 * 5 * m_randomizedDataChunkIdxs.size() );

		for ( size_t i = 0; i < m_randomizedDataChunkIdxs.size(); ++i )
		{
			size_t idx = m_randomizedDataChunkIdxs[i];
			/*
			if ( m_adjacentChunks_cornerAdjacencyIdxs[idx].valid )
			{
				randomizedDataChunkIdxs.push_back( idx );
				for ( size_t j = 0; j < 4; ++j )
				{
					randomizedDataChunkIdxs.push_back( m_adjacentChunks_cornerAdjacencyIdxs[idx].adjacent[j] );
				}
				num_sets += 1; 
				valid_corners += 1; 
			}*/
			if ( m_adjacentChunks_edgeAdjacencyIdxs[idx].valid )
			{
				for ( size_t j = 0; j < 4; ++j )
				{
					randomizedDataChunkIdxs.push_back( m_adjacentChunks_edgeAdjacencyIdxs[idx].adjacent[j] );
				}
				randomizedDataChunkIdxs.push_back(idx);
				num_sets += 1;
				valid_edges += 1; 
			}
		}

		m_randomizedDataChunkIdxs.swap( randomizedDataChunkIdxs );
	}

	m_epochSetsRemaining = m_randomizedDataChunkIdxs.size();

	m_firstEpochPrepared = true;
	m_onMetadataAvailable.notify_all();
}

void TrainingDataScheduler::prepareStraightforwardEpoch()
{
	DebugOutput( "new Epoch" );
	std::unique_lock<std::mutex> lock( m_metadataMtx );
	DebugAssert( m_randomizedDataChunkIdxs.empty() );
	m_randomizedDataChunkIdxs.clear();

	m_randomizedDataChunkIdxs.reserve( m_trainDataChunks.size() );
	for ( size_t i = 0; i < m_trainDataChunks.size(); ++i )
	{
		m_randomizedDataChunkIdxs.push_back( i );
	}
	m_epochSetsRemaining = m_randomizedDataChunkIdxs.size();

	m_firstEpochPrepared = true;
	m_onMetadataAvailable.notify_all();
}

void TrainingDataScheduler::endEpoch()
{
	{
		std::unique_lock<std::mutex> lock( m_metadataMtx );
		m_randomizedDataChunkIdxs.clear();
		m_epochSetsRemaining = 0;
		m_currentEpochId++;
	}
	{
		std::unique_lock<std::recursive_mutex> lock( m_loadedDataMtx );
		m_loadedTrainingData.clear();
	}
}

TrainingDataScheduler::data_pointer TrainingDataScheduler::getNextTrainingData()
{
	DebugAssert( m_firstEpochPrepared );
	data_pointer data;
	{
		std::unique_lock<std::recursive_mutex> lock( m_loadedDataMtx );
		while ( m_loadedTrainingData.empty() )
		{
			m_onDataAvailable.wait( lock );
		}
		data = std::move( m_loadedTrainingData.front() );
		m_loadedTrainingData.pop_front();
		--m_epochSetsRemaining;
		DebugOutput( "getting image from " + m_trainingDataFiles[data.fileIdx] );
	}
	return std::move( data );
}

void TrainingDataScheduler::freeTrainingData( const data_pointer& td )
{
	std::unique_lock<std::recursive_mutex> lock( m_loadedDataMtx );
	m_loadedTrainingBatches_refcount[td.fileIdx]--;
	if ( m_loadedTrainingBatches_refcount[td.fileIdx] == 0 )
	{
		m_onFreeMemory.notify_all();
	}
}

bool TrainingDataScheduler::clearSomeLoadedData()
{
	for( auto bidxIt = m_loadedTrainingBatches_loadOrder.begin(); bidxIt != m_loadedTrainingBatches_loadOrder.end(); )
	{
		auto bidx = *bidxIt;
		auto refc = m_loadedTrainingBatches_refcount[bidx];
		if ( refc == 0 )
		{
			DebugOutput( "freeing data for file " + m_trainingDataFiles[bidx] );
			size_t freedMemory = m_loadedTrainingBatches[bidx].size() * sizeof( decltype(m_loadedTrainingBatches)::value_type::second_type::value_type );
			DebugAssert( m_currentLoadedMemory >= freedMemory );
			m_currentLoadedMemory -= freedMemory;
			m_loadedTrainingBatches.erase( bidx );
			m_loadedTrainingBatches_refcount.erase( bidx );
			bidxIt = m_loadedTrainingBatches_loadOrder.erase( bidxIt );
			if ( m_currentLoadedMemory < MAX_LOADED_MEMORY )
			{
				return true;
			}
		}
		else
		{
			++bidxIt;
		}
	}
	return false;
}

void TrainingDataScheduler::loaderThreadFn()
{
	while ( !m_finish )
	{
		size_t epochId;
		size_t chunkIdx;
		{
			std::unique_lock<std::mutex> lock( m_metadataMtx );
			while ( m_randomizedDataChunkIdxs.empty() && !m_finish )
			{
				m_onMetadataAvailable.wait( lock );
			}
			if ( m_finish )
			{
				break;
			}
			chunkIdx = m_randomizedDataChunkIdxs.back();
			DebugAssert( chunkIdx < m_trainDataChunks.size() );
			m_randomizedDataChunkIdxs.pop_back();
			epochId = m_currentEpochId;
		}

		TrainData& td = m_trainDataChunks[chunkIdx];

		{
			auto trainingBatchIt = m_loadedTrainingBatches.find( td.dataFileIdx );
			if (trainingBatchIt == m_loadedTrainingBatches.end())
			{
				{
					std::unique_lock<std::recursive_mutex> lock(m_loadedDataMtx);
					while (m_currentLoadedMemory >= MAX_LOADED_MEMORY)
					{
						if (!clearSomeLoadedData())
						{
							m_onFreeMemory.wait(lock);
						}
					}
				}
				DebugOutput( "Loading data from file " + m_trainingDataFiles[td.dataFileIdx] );
				loadTrainingData( td );
				trainingBatchIt = m_loadedTrainingBatches.find( td.dataFileIdx );
			}

			// Add the reference to the chunk
			if ( epochId == m_currentEpochId )
			{
				DebugOutput( "Queueing data from " + m_trainingDataFiles[td.dataFileIdx] );
				std::unique_lock<std::recursive_mutex> lock(m_loadedDataMtx);
				data_pointer ptr(&(trainingBatchIt->second[td.chunkOffsetInFile * m_dataChunkSize]), td.coord, td.texelNormal, td.dataFileIdx, this);
				m_loadedTrainingData.push_back(std::move(ptr));
				m_loadedTrainingBatches_refcount[td.dataFileIdx]++;
				m_onDataAvailable.notify_all();
			}
		}
	}

}

void TrainingDataScheduler::loadTrainingMetadata( const std::string& filename )
{
	using json = nlohmann::json;
	json meta;
	try
	{
		std::ifstream fstream( filename );
		if ( fstream )
		{
			meta = json::parse( fstream );
		}
		else
		{
			CHAG_EXIT_ERROR( "Failed to load " + filename + ". File not found!" );
		}
	}
	catch ( std::exception& e )
	{
		LOG_ERROR( "Failed to load " << filename );
		DebugError( e.what() );
		CHAG_EXIT_ERROR( "Failed to load " + filename + "\n" + e.what() );
	}

	{
		m_trainingDataBasePath = filename;
		for ( size_t i = 0; i < m_trainingDataBasePath.length(); ++i )
		{
			if ( m_trainingDataBasePath[i] == '\\' )
			{
				m_trainingDataBasePath[i] = '/';
			}
		}
		size_t slash = m_trainingDataBasePath.find_last_of( '/' );
		if ( slash != std::string::npos )
		{
			m_trainingDataBasePath = m_trainingDataBasePath.substr( 0, slash + 1 );
		}
		else
		{
			m_trainingDataBasePath = "";
		}
	}

	std::vector<glm::vec3> geo_norm_tex;
	std::vector<char> validity;
	uint16_t lm_width;
	uint16_t lm_height;
	{
		std::ifstream cif( m_trainingDataBasePath + meta["texelInformation"].get<std::string>() );

		float magic;
		uint16_t version;
		cif.read( reinterpret_cast<char*>(&magic), sizeof( magic ) );
		DebugAssert( magic == 1.f / 5.f );
		cif.read( reinterpret_cast<char*>(&version), sizeof( version ) );
		DebugAssert( version == 1 );
		cif.read( reinterpret_cast<char*>(&lm_width), sizeof( lm_width ) );
		cif.read( reinterpret_cast<char*>(&lm_height), sizeof( lm_height ) );

		size_t lm_length = lm_width * lm_height;
		m_lightmap_size = {lm_width, lm_height};

		// Read validity array
		validity.resize( lm_length );
		cif.read( validity.data(), lm_length );

		// Skip positions array
		cif.seekg( lm_length * sizeof(glm::vec3), std::ios_base::cur );
		// Skip shading normals array
		cif.seekg( lm_length * sizeof(glm::vec3), std::ios_base::cur );

		geo_norm_tex.resize( lm_length );
		cif.read( reinterpret_cast<char*>(geo_norm_tex.data()), sizeof( glm::vec3 ) * lm_length);
	}

	size_t texel_idx = 0;
	size_t batch_files_count = meta["batches"].size();
	std::vector<uint32_t> cellIdxs( lm_width * lm_height, ~uint32_t( 0 ) );
	m_trainingDataFiles.resize( batch_files_count );
	for ( auto b : meta["batches"] )
	{
		TrainData d;
		d.dataFileIdx = b["pageIndex"];
		m_trainingDataFiles[d.dataFileIdx] = b.value("uri", "");

		size_t texel_count = b["texelCount"];

		m_batchTexelIndices.push_back( {} );

		for ( size_t i = 0; i < texel_count; ++i )
		{
			d.chunkOffsetInFile = i;
			d.coord = b["texels"][i];

			d.texelNormal = geo_norm_tex[d.coord.y * lm_width + d.coord.x];

			m_trainDataChunks.push_back( d );

			cellIdxs[d.coord.y * lm_width + d.coord.x] = texel_idx;

			m_batchTexelIndices.back().push_back( texel_idx );
			++texel_idx;
		}
	}

	// Fill adjacency arrays
	{
		auto checkAdjacency = [&validity, lm_width, lm_height]( int x, int y )
		{
			return x >= 1 && x < lm_width - 1 && y >= 1 && y < lm_height - 1
				&& validity[(y - 1) * lm_width + x] == 1
				&& validity[(y + 1) * lm_width + x] == 1
				&& validity[y * lm_width + x - 1] == 1
				&& validity[y * lm_width + x + 1] == 1
				&& validity[(y - 1) * lm_width + x - 1] == 1
				&& validity[(y - 1) * lm_width + x + 1] == 1
				&& validity[(y + 1) * lm_width + x - 1] == 1
				&& validity[(y + 1) * lm_width + x + 1] == 1;
		};

		m_adjacentChunks_cornerAdjacencyIdxs.reserve( m_trainDataChunks.size() );
		m_adjacentChunks_edgeAdjacencyIdxs.reserve( m_trainDataChunks.size() );

		for ( size_t i = 0; i < m_trainDataChunks.size(); ++i )
		{
			auto c = m_trainDataChunks[i].coord;
			if ( checkAdjacency( c.x, c.y ) )
			{
				m_adjacentChunks_edgeAdjacencyIdxs.push_back( { true,
				                                                  { cellIdxs[( c.y - 1 ) * lm_width + c.x],
				                                                    cellIdxs[( c.y + 1 ) * lm_width + c.x],
				                                                    cellIdxs[c.y * lm_width + c.x - 1],
				                                                    cellIdxs[c.y * lm_width + c.x + 1] } } );
				m_adjacentChunks_cornerAdjacencyIdxs.push_back( { true,
				                                                { cellIdxs[( c.y - 1 ) * lm_width + c.x - 1],
				                                                  cellIdxs[( c.y - 1 ) * lm_width + c.x + 1],
				                                                  cellIdxs[( c.y + 1 ) * lm_width + c.x - 1],
				                                                  cellIdxs[( c.y + 1 ) * lm_width + c.x + 1] } } );
			}
			else
			{
				m_adjacentChunks_cornerAdjacencyIdxs.push_back( { false, {} } );
				m_adjacentChunks_edgeAdjacencyIdxs.push_back( { false, {} } );
			}
		}
	}

	size_t lightfieldSize = meta["lightfieldSideSize"];
	m_lightfield_side_size = lightfieldSize;
	m_dataChunkSize = lightfieldSize * lightfieldSize * 3;

	size_t lightfieldsPerBatch = meta["lighfieldsPerBatchFile"];
}

void TrainingDataScheduler::loadTrainingData( const TrainData & src )
{
	using namespace half_float;

	std::string file = m_trainingDataBasePath + m_trainingDataFiles[src.dataFileIdx];
	std::ifstream f( file, std::ifstream::binary );

	// 2 bytes: magic number = half_float 1./3. (this might be used to check correct endianness)
	// 2 bytes: version
	// 2 bytes: lightfield tile side length. Lightfield images are always a square.
	// 2 bytes: number of tiles in this texture
	half magic;
	uint16_t version;
	uint16_t lf_size;
	uint16_t lf_count;
	uint16_t lf_channels;
	uint32_t lf_num_samples; 
	f.read( reinterpret_cast<char*>(&magic), sizeof( half ) );
	DebugAssert( magic == half( 1.f / 3.f ) );
	f.read( reinterpret_cast<char*>(&version), sizeof( uint16_t ) );
	//DebugAssert( version == 3 );
	f.read( reinterpret_cast<char*>(&lf_size), sizeof( uint16_t ) );
	DebugAssert( lf_size == m_lightfield_side_size );
	f.read( reinterpret_cast<char*>(&lf_count), sizeof( uint16_t ) );
	DebugAssert( lf_count > src.chunkOffsetInFile );
	f.read( reinterpret_cast<char*>( &lf_channels ), sizeof( uint16_t ) );
	if(version > 3)
		f.read(reinterpret_cast<char*>(&lf_num_samples), sizeof(uint32_t));

	size_t use_chunk_size = lf_size * lf_size * 3;
	size_t read_chunk_size = lf_size * lf_size * lf_channels;
	size_t read_chunk_size_bytes = read_chunk_size * sizeof( half );
	size_t start = f.tellg();

	f.seekg( 0, std::ios::end );
	DebugAssert( f.tellg() == start + read_chunk_size_bytes * lf_count );
	f.seekg( start );

	std::vector<float> ret_buffer( use_chunk_size * lf_count );
	std::vector<half> buffer( read_chunk_size * lf_count );

	f.read( reinterpret_cast<char*>(buffer.data()), read_chunk_size_bytes * lf_count );

	for (int image = 0; image < lf_count; image++)
	{
		for (int channel = 0; channel < 3; channel++)
		{
			const size_t read_channel = m_channel_remap[channel];
			for (int i = 0; i < lf_size * lf_size; i++)
			{
				ret_buffer[image * (3 * lf_size*lf_size) + channel * (lf_size*lf_size) + i] =
					buffer[image * (lf_channels * lf_size*lf_size) + read_channel * (lf_size*lf_size) + i];
				if ( isnan( ret_buffer[image * (3 * lf_size*lf_size) + channel * (lf_size*lf_size) + i] ) )
				{
					ret_buffer[image * (3 * lf_size*lf_size) + channel * (lf_size*lf_size) + i] = 0.f;
				}
			}
		}
	}

	{
		std::unique_lock<std::recursive_mutex> lock(m_loadedDataMtx);
		m_loadedTrainingBatches.insert_or_assign(src.dataFileIdx, std::move(ret_buffer));
		m_loadedTrainingBatches_loadOrder.push_back( src.dataFileIdx );
		m_currentLoadedMemory += use_chunk_size * lf_count * sizeof(decltype(m_loadedTrainingBatches)::value_type::second_type::value_type);
		m_loadedTrainingBatches_refcount[src.dataFileIdx] = 0;
	}
}

}
