#include "preprocessing.h"
#include <filesystem>
#include <iostream>
#include <fstream>
#include <utils/Log.h>
#include <utils/half.hpp>
#include <utils/json.hpp>
#include <utils/json_extras.hpp>
#include "../include/spherical_projection_common.h"
#include <algorithm>
#include <random>

using namespace std;
using namespace std::filesystem;
using namespace half_float; 

namespace preprocessing
{
	void ConvolveLFB(const string & filename, const string & outputpath)
	{
		LOG_INFO("Processing " << path(filename).filename());
		path output_filename = path(outputpath) / path(filename).filename();
		std::ofstream os(output_filename.string(), std::ofstream::binary);
		std::ifstream is(filename, std::ifstream::binary);
		if (!is.is_open()) {
			CHAG_EXIT_ERROR("Failed to open file " + filename);
		}
		half magic;
		uint16_t version;
		uint16_t lf_size;
		uint16_t lf_count;
		uint16_t lf_channels;
		uint32_t lf_num_samples; 
		is.read(reinterpret_cast<char*>(&magic), sizeof(half));
		os.write(reinterpret_cast<char*>(&magic), sizeof(half));
		DebugAssert(magic == half(1.f / 3.f));
		is.read(reinterpret_cast<char*>(&version), sizeof(uint16_t));
		os.write(reinterpret_cast<char*>(&version), sizeof(uint16_t));
		DebugAssert(version > 3);
		is.read(reinterpret_cast<char*>(&lf_size), sizeof(uint16_t));
		os.write(reinterpret_cast<char*>(&lf_size), sizeof(uint16_t));
		is.read(reinterpret_cast<char*>(&lf_count), sizeof(uint16_t));
		os.write(reinterpret_cast<char*>(&lf_count), sizeof(uint16_t));
		is.read(reinterpret_cast<char*>(&lf_channels), sizeof(uint16_t));
		os.write(reinterpret_cast<char*>(&lf_channels), sizeof(uint16_t));
		is.read(reinterpret_cast<char*>(&lf_num_samples), sizeof(uint32_t));
		os.write(reinterpret_cast<char*>(&lf_num_samples), sizeof(uint32_t));

		for (int tile = 0; tile < lf_count; tile++)
		{
			//cout << to_string(tile) << "/" << to_string(lf_count) << "                 \r" << flush; 
			vector<half> half_input(lf_size * lf_size * lf_channels);
			vector<float> input(lf_size * lf_size * lf_channels);
			vector<half> half_output(lf_size * lf_size * lf_channels);
			vector<float> output(lf_size * lf_size * lf_channels);
			is.read(reinterpret_cast<char*>(half_input.data()), half_input.size() * sizeof(half));
			for (int i = 0; i < input.size(); i++) input[i] = half_input[i];
			ConvolveImage(input, output, lf_channels);
			for (int i = 0; i < output.size(); i++) half_output[i] = output[i]; 
			os.write(reinterpret_cast<char*>(half_output.data()), half_output.size() * sizeof(half));
		}
	}

	void Convolve(const std::string & filename)
	{
		///////////////////////////////////////////////////////////////////////////
		// Loop through all .lfb files and blur the input images
		///////////////////////////////////////////////////////////////////////////
		// Read original json file and copy 
		using json = nlohmann::json; 
		json original; 
		{
			std::ifstream fstream( filename );
			if ( fstream )
			{
				original = json::parse( fstream );
			}
			else
			{
				CHAG_EXIT_ERROR( "Failed to load " + filename + ". File not found!" );
			}
		}
		json copy = original;
		copy["convolved"] = true;

		std::string old_dir_name = "original";
		if ( original.value( "scrambled", false ) )
		{
			old_dir_name = "scrambled";
		}

		if ( original.value( "convolved", false ) )
		{
			CHAG_EXIT_ERROR( "These files are already convolved!" );
		}

		path base_dir = path(filename).parent_path();
		path backup_dir = path(filename).parent_path() / old_dir_name;

		create_directory(backup_dir);

		for ( auto dentry : directory_iterator( base_dir ) )
		{
			if ( dentry.path().extension() == ".lfb" )
			{
				LOG_INFO("Moving " << dentry.path().filename());
				std::filesystem::rename( dentry.path(), backup_dir / dentry.path().filename() );
			}
			else if (dentry.is_directory()) {
				LOG_WARNING("Input path contains directory: " << dentry.path().filename() << "... Skipping."); 
			}
			else
			{
				path p = dentry.path();
				LOG_INFO("Moving " << p.filename());
				std::filesystem::rename( p, backup_dir / p.filename() );
				LOG_INFO("Copying " << p.filename());
				std::filesystem::copy(backup_dir / p.filename(), p, copy_options::overwrite_existing);
			}
		}

		for (auto dentry : directory_iterator(backup_dir))
		{
			if (dentry.path().extension() == ".lfb") {
				ConvolveLFB(dentry.path().string(), base_dir.string());
			}
		}

		// Save new json file
		std::ofstream o(base_dir / path(filename).filename());
		o << std::setw(4) << copy << std::endl;
	}

	void Scramble(const std::string & filename)
	{
		// Read original json file and copy 
		using json = nlohmann::json; 
		json original; 
		{
			std::ifstream fstream( filename );
			if ( fstream )
			{
				original = json::parse( fstream );
			}
			else
			{
				CHAG_EXIT_ERROR( "Failed to load " + filename + ". File not found!" );
			}
		}
		json copy = original;
		copy["scrambled"] = true;

		std::string old_dir_name = "original";
		if ( original.value( "convolved", false ) )
		{
			old_dir_name = "convolved";
		}

		if ( original.value( "scrambled", false ) )
		{
			old_dir_name = "prev_scramble";
		}

		// Create output directory and copy original files
		path base_dir = path(filename).parent_path();
		path backup_dir = path(filename).parent_path() / old_dir_name;
		create_directory(backup_dir);
		for (auto dentry : directory_iterator(base_dir))
		{
			if (dentry.is_directory()) {
				LOG_WARNING("Input path contains directory: " << dentry.path().filename() << "... Skipping.");
			}
			else {
				path p = dentry.path();
				LOG_INFO("Moving " << p.filename());
				std::filesystem::rename( p, backup_dir / p.filename() );
				LOG_INFO("Copying " << p.filename());
				std::filesystem::copy(backup_dir / p.filename(), p, copy_options::overwrite_existing);
			}
		}

		// Scramble original order
		std::vector<std::tuple<string /* lfb file */, int /*lfb_index*/, int /* texel_index */>> original_order; 
		int num_batches = original["batches"].size();
		for (int i = 0; i < num_batches; i++)
		{
			int num_texels = original["batches"][i]["texelCount"];
			string uri = original["batches"][i]["uri"];
			for (int j = 0; j < num_texels; j++) {
				original_order.push_back({uri, i, j});
			}
		}
		std::vector<std::tuple<string /* lfb file */, int /*lfb_index*/, int /* texel_index */>> shuffled_order = original_order;
		std::mt19937 g;
		std::shuffle(shuffled_order.begin(), shuffled_order.end(), g);

		// Copy scrambled images
		int shuffle_ctr = 0; 
		for (uint64_t i = 0; i < original_order.size(); i++)
		{
			cout << "Copying image " << i << "/" << original_order.size() << "\r";
			path input_filename = backup_dir / path(std::get<0>(original_order[i]));
			int input_batch = std::get<1>(original_order[i]);
			int input_index = std::get<2>(original_order[i]);
			ifstream is(input_filename, ios::binary);
			if (!is.is_open()) {
				CHAG_EXIT_ERROR("Could not find: " + input_filename.string());
			}
			path output_filename = base_dir / path(std::get<0>(shuffled_order[i]));
			int output_batch = std::get<1>(shuffled_order[i]);
			int output_index = std::get<2>(shuffled_order[i]);
			std::fstream os(output_filename, ios::binary | ios_base::in | ios_base::out);
			if (!os.is_open()) {
				CHAG_EXIT_ERROR("Could not find: " + output_filename.string());
			}

			// Read header from original
			half magic;
			uint16_t version;
			uint16_t lf_size;
			uint16_t lf_count;
			uint16_t lf_channels;
			uint32_t lf_num_samples;
			is.read(reinterpret_cast<char*>(&magic), sizeof(half));
			DebugAssert(magic == half(1.f / 3.f));
			is.read(reinterpret_cast<char*>(&version), sizeof(uint16_t));
			DebugAssert(version == 4);
			is.read(reinterpret_cast<char*>(&lf_size), sizeof(uint16_t));
			is.read(reinterpret_cast<char*>(&lf_count), sizeof(uint16_t));
			is.read(reinterpret_cast<char*>(&lf_channels), sizeof(uint16_t));
			is.read(reinterpret_cast<char*>(&lf_num_samples), sizeof(uint32_t));
			// Skip header on output
			os.seekp(5 * sizeof(uint16_t) + sizeof(uint32_t), std::ios_base::beg);
			// Skip previous images on both
			const int image_size = lf_size * lf_size * lf_channels * sizeof(half);
			is.seekg(input_index * image_size, std::ios_base::cur);
			os.seekp(output_index * image_size, std::ios_base::cur);
			// Copy images
			if (lf_channels != 9) { // Just color maybe alpha data
				vector<char> data(image_size);
				is.read(data.data(), image_size);
				os.write(data.data(), image_size);
			}
			else { // 3xreflection, 3xFLV, 3xFL
				const int rgb_image_size = lf_size * lf_size * 3 * sizeof(half);
				vector<char> data(rgb_image_size);
				is.read(data.data(), rgb_image_size);
				os.write(data.data(), rgb_image_size);

				vector<half> fLV(lf_size * lf_size * 3);
				is.read((char *)fLV.data(), rgb_image_size);

				vector<half> fL(lf_size * lf_size * 3);
				is.read((char *)fL.data(), rgb_image_size);

				vector<half> ratio(lf_size * lf_size * 3);
				vector<half> invratio(lf_size * lf_size * 3);
				for (int i = 0; i < lf_size * lf_size * 3; i++) {
					ratio[i] = fLV[i] / fL[i]; 
					if ( isnan( ratio[i] ) )
					{
						ratio[i] = 0.f;
					}
					invratio[i] = fL[i] / fLV[i];
					if ( isnan( invratio[i] ) )
					{
						invratio[i] = 0.f;
					}
				}

				os.write((char *)ratio.data(), rgb_image_size);
				os.write((char *)invratio.data(), rgb_image_size);
			}

			is.close(); 
			os.close(); 
			// Update json
			auto & src = original["batches"][input_batch]["texels"][input_index];
			auto & dst = copy["batches"][output_batch]["texels"][output_index];
			dst = src; 
		}
		cout << "\nDone.";

		// Save new json file
		std::ofstream o(base_dir / path(filename).filename());
		o << std::setw(4) << copy << std::endl;
	}
}

