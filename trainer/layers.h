#pragma once

#include "tensor.h"
#include <cuda_runtime.h>
#include <cudnn.h>

namespace dnn {

	const float adam_beta_1 = 0.9f;
	const float adam_beta_2 = 0.999f;
	__global__ void adam_update_parameters_kernel(int num_values, float *parameters, float *s, float *r, const float *g,
		const float beta_1, const float beta_2, const int t,
		const float step_size);


/////////////////////////////////////////////////////////////////////////////
// Layer Interface
/////////////////////////////////////////////////////////////////////////////
struct layer {
	virtual void Forward(tensor &input_tensor, tensor &output_tensor)  = 0;
	virtual void Backward(tensor &input_tensor, tensor &output_tensor) = 0;
	virtual void UpdateWeights(float learning_rate)                    = 0;
	virtual void CheckForBadFloat(){};
};

/////////////////////////////////////////////////////////////////////////////
// A convolution layer with square, odd-sized filter, no stride or dilation.
/////////////////////////////////////////////////////////////////////////////
struct convolution_layer : public layer {
	float alpha = 1.0f, beta = 0.0f;
	filter f;
	tensor bias;
	cudnnConvolutionDescriptor_t convolution_descriptor;
	cudnnConvolutionFwdAlgo_t forward_algorithm;
	cudnnConvolutionBwdFilterAlgo_t backward_filter_algorithm;
	cudnnConvolutionBwdDataAlgo_t backward_data_algorithm;
	void *workspace         = nullptr;
	uint64_t workspace_size = 0;
	// Adam optimizer moments
	float *d_adam_s, *d_adam_r;
	void init(int num_input_channels, int num_output_channels, int filter_size);
	convolution_layer(int num_input_channels, int num_output_channels, int filter_size) {
		init(num_input_channels, num_output_channels, filter_size);
	}
	void FindAlgorithmsAndWorkspace(tensor &input_tensor, tensor &output_tensor);
	void Forward(tensor &input_tensor, tensor &output_tensor) override;
	void Backward(tensor &input_tensor, tensor &output_tensor) override;
	void UpdateWeights(float learning_rate) override;
	void CheckForBadFloat() override {
		f.CheckForBadFloat();
		bias.CheckForBadFloat();
	}
};

/////////////////////////////////////////////////////////////////////////////
// Pooling layer
/////////////////////////////////////////////////////////////////////////////
struct pool_layer : public layer {
	cudnnPoolingDescriptor_t pooling_descriptor;
	float alpha = 1.0f, beta = 0.0f;
	void init();
	pool_layer() { init(); }
	void Forward(tensor &input_tensor, tensor &output_tensor) override;
	void Backward(tensor &input_tensor, tensor &output_tensor) override;
	void UpdateWeights(float learning_rate) override { /* Nothing */
	}
};

/////////////////////////////////////////////////////////////////////////////
// Activation layer
/////////////////////////////////////////////////////////////////////////////
struct activation_layer : public layer {
	float alpha = 1.0f, beta = 0.0f;
	cudnnActivationDescriptor_t activation_descriptor;
	void init(cudnnActivationMode_t mode);
	activation_layer(cudnnActivationMode_t mode) { init(mode); }
	void Forward(tensor &input_tensor, tensor &output_tensor) override;
	void Backward(tensor &input_tensor, tensor &output_tensor) override;
	void UpdateWeights(float learning_rate) override { /* Nothing */
	}
};

/////////////////////////////////////////////////////////////////////////////
// Fully Connected Layer
/////////////////////////////////////////////////////////////////////////////
struct fc_layer : public layer {
	float alpha = 1.0f, beta = 0.0f;
	int num_input_nodes, num_output_nodes;
	tensor weights;
	tensor bias;
	// Adam Optimizer Moments
	float *d_adam_s, *d_adam_r;
	void init(int _num_input_nodes, int _num_output_nodes);
	fc_layer(int _num_input_nodes, int _num_output_nodes) { init(_num_input_nodes, _num_output_nodes); }
	void Forward(tensor &input_tensor, tensor &output_tensor) override;
	void Backward(tensor &input_tensor, tensor &output_tensor) override;
	void UpdateWeights(float learning_rate) override;
	void CheckForBadFloat() override {
		weights.CheckForBadFloat();
		bias.CheckForBadFloat();
	}
};

/////////////////////////////////////////////////////////////////////////////
// Softmax Layer
/////////////////////////////////////////////////////////////////////////////
struct softmax_layer : public layer {
	const float alpha = 1.0f, beta = 0.0f;
	void Forward(tensor &input_tensor, tensor &output_tensor) override;
	void Backward(tensor &input_tensor, tensor &output_tensor) override;
	void UpdateWeights(float learning_rate) override { /* Nothing */
	}
};

/////////////////////////////////////////////////////////////////////////////
// Dropout Layer
/////////////////////////////////////////////////////////////////////////////
struct dropout_layer : public layer {
	cudnnDropoutDescriptor_t descriptor;
	void *workspace;
	uint64_t workspace_size;
	void *states;
	uint64_t states_size;
	dropout_layer(float amount);
	void FindWorkspace(tensor &input_tensor);
	void Forward(tensor &input_tensor, tensor &output_tensor) override;
	void Backward(tensor &input_tensor, tensor &output_tensor) override;
	void UpdateWeights(float learning_rate) override { /* Nothing */
	}
};

/////////////////////////////////////////////////////////////////////////////
// Upsample Layer (simple nearest neighbour 2x2 upsampling)
/////////////////////////////////////////////////////////////////////////////
struct upsample_layer : public layer {
	void Forward(tensor &input_tensor, tensor &output_tensor) override;
	void Backward(tensor &input_tensor, tensor &output_tensor) override;
	void UpdateWeights(float learning_rate) override { /* Nothing */
	}
};

}  // namespace dnn