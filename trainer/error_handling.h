#include <cudnn.h>
#include <stdio.h>
#include <sstream>
#include <iostream>
#include <vector>

inline void FatalError(const std::string & s, const char * file, int line)
{
    std::stringstream _where, _message;
    _where << file << ':' << line;
    _message << std::string(s) + "\n" << file << ':' << line;
    std::cerr << _message.str() << "\nAborting...\n";
    //__debugbreak();
    cudaDeviceReset();
    exit(1);
}

#define CUDA_FATAL_ERROR(err_str) do {                                 \
    FatalError( std::string(err_str), __FILE__, __LINE__ );            \
} while (0)

#define checkCudnnErr(status) do {                                     \
    std::stringstream _error;                                          \
    if (status != CUDNN_STATUS_SUCCESS) {                              \
      _error << "CUDNN failure: " << cudnnGetErrorString(status);      \
      FatalError(_error.str(), __FILE__, __LINE__);                    \
    }                                                                  \
} while(0)

#define checkCublasErr(status) do {                                    \
    std::stringstream _error;                                          \
    if (status != 0) {                                                 \
      _error << "CUBLAS failure: " << status;                          \
      FatalError(_error.str(), __FILE__, __LINE__);                    \
    }                                                                  \
} while(0)


#define checkCudaErr(status) do {                                      \
    if (status != 0) {                                                 \
      std::stringstream _error;                                        \
      _error << "Cuda failure: " << cudaGetErrorString(status);        \
      FatalError(_error.str(), __FILE__, __LINE__);                    \
    }                                                                  \
} while(0)

inline void CheckForBadFloat(const std::vector<float> & values)
{
	for (uint64_t i = 0; i < values.size(); i++) {
		if (isnan(values[i])) {
			std::cout << "nan at index: " << i << std::endl;
		}
		if (isinf(values[i])) {
			std::cout << "inf at index: " << i << std::endl;
		}
	}
}
