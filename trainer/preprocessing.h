#pragma once
#include <string>
#include <vector>

namespace preprocessing
{
	void Convolve(const std::string & filename);
	void ConvolveImage(const std::vector<float> & input, std::vector<float> & output, const uint16_t channels);
	void Scramble(const std::string & filename);
}