#include "../include/spherical_projection_common.h"
#include "common.h"
#include "dnn_trainer.h"
#include "error_handling.h"
#include "layers.h"
#include "tensor.h"
#include "training_data_scheduler.h"
#include "dnn_layer_exporter.h"
#include <algorithm>
#include <array>
#include <chrono>
#include <cublas_v2.h>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <iomanip>
#include <iostream>
#include <random>
#include <stb_image.h>
#include <stb_image_write.h>
#include <tuple>
#include <utils/glm_extensions.h>


#include "../pathtracer/random.cuh"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

using std::array;
using std::vector;
using std::min;
using std::max;
using std::string;
using std::to_string;
using std::cout;
using std::flush;
using std::ofstream;
using std::endl;
using std::ios;

// How many parameters has each gaussian
#define NUM_PARAMS 7

// ---------------------------------------

#define NO_CONSTRAINTS 0

// ---------------------------------------

// Train so that the direction vector is close to unit length
#define CONSTRAIN_TO_UNIT_LENGTH 1

// ---------------------------------------

// Train so that A and S should always be positive
#define CONSTRAIN_A_AND_S_POSITIVE 1

// ---------------------------------------

#define EXTRA_FC_LAYER 0

// ---------------------------------------

// Maximum expected input radiance to normalize input to the neural network
#define CLAMP_INPUT_RADIANCE 0
#define SCALE_INPUT_RADIANCE 1.0f

// ---------------------------------------

// Use log(x+1)-log(t+1) instead of (x-t)^2
#define LOG_LOSS 1

// ---------------------------------------

	// The gradient is calculated in xyz space
#define DIRECTION_GRADIENT_MODE_DIRECT 0
	// The gradient is calculated in xyz space and then it's made tangential to the sphere's surface
#define DIRECTION_GRADIENT_MODE_TANGENT 1
	// The gradient is calculated in spherical coords space
#define DIRECTION_GRADIENT_MODE_SPHERICAL 2
// Determines how the gradient is calculated
#define DIRECTION_GRADIENT_MODE DIRECTION_GRADIENT_MODE_DIRECT

// ---------------------------------------

// Use the area of each pixel in the lightfield to weigh up/down how much it contributes to the gradient
#define NORMALIZE_LIGHTFIELD_PIXEL_AREA 1

// ---------------------------------------

// If the direction points below the normal, don't affect the gradient.
#define IGNORE_BELOW_NORMAL 1

// Apply a smoothstep on the input image below the normal plane
#define SMOOTHSTEP_BELOW_NORMAL 0
	// How big is the smoothstep applied to the input image
#define SMOOTHSTEP_STEP_ANGLE (10.f * M_PI / 180.f)

#define DIRECT_C 0

namespace dnn
{

namespace
{

struct TrainerKernelSettings
{
	uint32_t num_gaussians = 16;
	bool drop_random_gaussians = false;
	bool jitter_directions = false;
	bool fixed_directions = false; 
	bool constrain_upper_hemisphere = false;
	bool separate_islands = false;
	uint16_t lightfield_size = 128;
	uint16_t base_lightmap_size = 128;
};

TrainerSettings s_settings;
TrainerKernelSettings s_kernel_settings;

}

const float START_SHARPNESS = 1.0f; 
const float START_COLOR = 0.5f; 
constexpr float MIN_AMPLITUDE = 0.01f; 

cublasHandle_t cublas_handle;
cudnnHandle_t cudnn_handle;

using Clock = std::chrono::system_clock;
using Timestamp = std::chrono::time_point<Clock>;
using Milliseconds = std::chrono::duration<double, std::milli>;

uint64_t s_epoch = 0;
uint64_t s_batch = 0;

void DumpTensorAsImage( const tensor& t, const std::string& filename, float scale = 1.0f, bool gradient = false )
{
	if ( t.dim.c != 3 )
	{
		/*cout << "ERROR: DumpTensorAsImage only implemented for 3-channel tensors so far.\n";
		exit(0);*/
		// Nah, just dump three first channels, might wanna add an offset argument
	}
	int image_channels = 3;
	vector<uint8_t> batch_data( t.dim.w * 4 * t.dim.h * 4 * image_channels );
	vector<float> tdata;
	if ( !gradient )
		tdata = t.DownloadFlatData();
	else
		tdata = t.DownloadFlatGrad();
	for ( int Y = 0; Y < 4; Y++ )
	{
		for ( int X = 0; X < 4; X++ )
		{
			vector<uint8_t> data( t.dim.w * t.dim.h * image_channels );
			int batch_id = Y * 4 + X;
			for ( int y = 0; y < t.dim.h; y++ )
			{
				for ( int x = 0; x < t.dim.w; x++ )
				{
					glm::vec3 frgb =
					        glm::vec3( tdata[batch_id * t.dim.c * t.dim.h * t.dim.w + 0 * t.dim.h * t.dim.w + y * t.dim.w + x],
					                   tdata[batch_id * t.dim.c * t.dim.h * t.dim.w + 1 * t.dim.h * t.dim.w + y * t.dim.w + x],
					                   tdata[batch_id * t.dim.c * t.dim.h * t.dim.w + 2 * t.dim.h * t.dim.w + y * t.dim.w + x] )
					        * scale;
					glm::ivec3 irgb;
					irgb = glm::clamp( glm::ivec3( abs( frgb * 255.0f ) ), glm::ivec3( 0 ), glm::ivec3( 255 ) );
					if ( isnan( frgb.x ) || isnan( frgb.y ) || isnan( frgb.z ) )
						irgb = glm::vec3( 255, 0, 255 );
					else if ( isinf( frgb.x ) || isinf( frgb.y ) || isinf( frgb.z ) )
						irgb = glm::vec3( 0, 255, 0 );
					// else if (abs(frgb.x) > 100.0f || abs(frgb.y) > 100.0f || abs(frgb.z) > 100.0f) irgb = glm::vec3(255, 255,
					// 0); else if (frgb.x < 0.0f || frgb.y < 0.0f || frgb.z < 0.0f) { irgb.r = 255; } else if (frgb.x > 1.0f ||
					// frgb.y > 1.0f || frgb.z > 1.0f) { irgb.b = 255; }
					data[y * t.dim.w * image_channels + x * image_channels + 0] = irgb.x;
					data[y * t.dim.w * image_channels + x * image_channels + 1] = irgb.y;
					data[y * t.dim.w * image_channels + x * image_channels + 2] = irgb.z;
				}
			}
			for ( int y = 0; y < t.dim.h; y++ )
			{
				for ( int x = 0; x < t.dim.w; x++ )
				{
					batch_data[( ( Y * t.dim.h + y ) * ( t.dim.w * 4 ) + X * t.dim.w + x ) * 3 + 0] =
					        data[y * t.dim.w * image_channels + x * image_channels + 0];
					batch_data[( ( Y * t.dim.h + y ) * ( t.dim.w * 4 ) + X * t.dim.w + x ) * 3 + 1] =
					        data[y * t.dim.w * image_channels + x * image_channels + 1];
					batch_data[( ( Y * t.dim.h + y ) * ( t.dim.w * 4 ) + X * t.dim.w + x ) * 3 + 2] =
					        data[y * t.dim.w * image_channels + x * image_channels + 2];
				}
			}
		}
	}
	stbi_flip_vertically_on_write( true );
	stbi_write_png( filename.c_str(), t.dim.w * 4, t.dim.h * 4, image_channels, batch_data.data(), 0 );
}

// Batch sized one vector. Used here and there, initialized in init.
tensor ones_tensor;

__inline__ __device__ float warpReduceSum( float val )
{
	const int warpSize = 32;
	for ( int offset = warpSize / 2; offset > 0; offset /= 2 )
	{
		val += __shfl_down_sync( 0xffffffff, val, offset, 32 );
	}
	return val;
}

float randf()
{
	return rand() / float( RAND_MAX );
}


// Calculate the output image and the error gradient at once
__global__ void create_image_kernel( TrainerKernelSettings settings,
									 const float* d_gaussian_parameters,
                                     const float* d_truth,
                                     float* d_data,
                                     float* d_grad,
                                     float* d_normals,
                                     glm::u16vec2* d_batch_coords,
                                     float* d_mse,
                                     float* d_mse_image,
									 float* d_pixel_areas,
                                     const int ctr )
{
	int tx = blockIdx.x * blockDim.x + threadIdx.x;
	int ty = blockIdx.y * blockDim.y + threadIdx.y;
	int tI = blockIdx.z * blockDim.z + threadIdx.z;
	int channel = tI % IMAGE_CHANNELS;
	int id_in_batch = tI / IMAGE_CHANNELS;
	int thread_index = tI * ( settings.lightfield_size * settings.lightfield_size ) + ty * settings.lightfield_size + tx;


	///////////////////////////////////////////////////////////////////////
	// Calculate the direction of the current pixel
	///////////////////////////////////////////////////////////////////////
	glm::vec3 direction;
	{
		float theta, phi;
		float fx, fy;
		if ( settings.jitter_directions )
		{
			seed_t seed = initialize_seed( make_uint2( tx, ty ), 0, ctr );
			fx = tx + ::randf( seed );
			fy = ty + ::randf( seed );
		}
		else
		{
			fx = tx + 0.5f;
			fy = ty + 0.5f;
		}
		GetThetaPhiFromTexCoords( fx / float(settings.lightfield_size), fy / float(settings.lightfield_size), theta, phi );
		direction = normalize( GetXYZFromThetaPhi( theta, phi ) );
	}

	///////////////////////////////////////////////////////////////////////
	// Evaluate current pixel value and pixel gradient
	///////////////////////////////////////////////////////////////////////
	float sum = 0.0f;
	const int num_gaussian_parameters = settings.num_gaussians * NUM_PARAMS;
	for ( int j = 0; j < settings.num_gaussians; j++ )
	{
		glm::vec3 xyz = glm::vec3( d_gaussian_parameters[id_in_batch * num_gaussian_parameters + j * NUM_PARAMS + 0],
		                           d_gaussian_parameters[id_in_batch * num_gaussian_parameters + j * NUM_PARAMS + 1],
		                           d_gaussian_parameters[id_in_batch * num_gaussian_parameters + j * NUM_PARAMS + 2] );
		// Check if gaussian is marked as "dropped" in this run. 
		if (xyz == glm::vec3(0.0f)) continue; 

		glm::vec3 Direction = normalize(xyz);
		float s = d_gaussian_parameters[id_in_batch * num_gaussian_parameters + j * NUM_PARAMS + 3];
		float A = d_gaussian_parameters[id_in_batch * num_gaussian_parameters + j * NUM_PARAMS + 4 + channel];
		sum += A * SG( direction, Direction, s );
	}

	//sum = max(-0.05f, sum);

	float truth = d_truth[thread_index];

#if SMOOTHSTEP_BELOW_NORMAL
	float step = 0.f;
	{
		glm::vec3 normal( d_normals[id_in_batch * 3 + 0], d_normals[id_in_batch * 3 + 1], d_normals[id_in_batch * 3 + 2] );
		step = glm::smootherstep( M_PI / 2, M_PI / 2 - SMOOTHSTEP_STEP_ANGLE, acosf( dot( direction, normal ) ) );
		truth *= step;
	}
#endif

	//d_data[thread_index] = truth;
	//d_data[thread_index] = d_pixel_areas[thread_index % (128*128)];

	// Use a log of the radiance so errors in darker regions do not disappear
	// in favor of strong highlights
	
#if 0 // L1
	float gradient = 0.0f; 
	if ((truth - sum) == 0) gradient = 0.0f;
	else if ((truth - sum) < 0) gradient = 1.0f; 
	else gradient = -1.0f; 
#else
#if !LOG_LOSS
	float gradient = 2.0f * ( sum - truth );
#else
	sum = max(-0.5f, sum * SCALE_INPUT_RADIANCE);
	truth = truth * SCALE_INPUT_RADIANCE; 
	float gradient = -1.0f * /*20.0f */ (log(1 + truth) - log(1 + sum)) / (1 + sum);
#endif
#endif

#if NORMALIZE_LIGHTFIELD_PIXEL_AREA
	d_grad[thread_index] = gradient * d_pixel_areas[thread_index % (settings.lightfield_size * settings.lightfield_size)];
#else
	d_grad[thread_index] = gradient;
#endif

	d_data[thread_index] = d_grad[thread_index];


	///////////////////////////////////////////////////////////////////////
	// If the direction points below the normal, don't affect the gradient.
	///////////////////////////////////////////////////////////////////////
#if IGNORE_BELOW_NORMAL
	{
		glm::vec3 normal(d_normals[id_in_batch * 3 + 0], d_normals[id_in_batch * 3 + 1], d_normals[id_in_batch * 3 + 2]);
#if SQRT_FALLOFF
		d_grad[thread_index] = sqrt( max( 0.0f, dot( normal, direction ) ) ) * d_grad[thread_index];
		d_data[thread_index] = sqrt( max( 0.0f, dot( normal, direction ) ) ) * d_data[thread_index];
		// For MSE
		sum = d_data[thread_index];
		truth = sqrt(max(0.0f, dot(normal, direction))) * truth;
#else
		if (dot(normal, direction) < 0) {
			gradient = 0.0f; 
			d_grad[thread_index] = gradient;
			// For MSE only
			sum = truth; 
		}
#endif


	}
#endif

	if ( d_mse != nullptr )
	{
		// For stats
		float warp_square_error = warpReduceSum( ( truth - sum ) * ( truth - sum ) );
		if ( threadIdx.x % 32 == 0 )
			atomicAdd( d_mse,
			           ( 1.0f / float( BATCH_SIZE ) ) * ( 1.0f / float( settings.lightfield_size * settings.lightfield_size * 3 ) ) * warp_square_error );
	}
	if ( d_mse_image != nullptr )
	{
		float mse = (truth - sum) / SCALE_INPUT_RADIANCE;
		glm::ivec2 coord = d_batch_coords[id_in_batch];
		atomicAdd( &d_mse_image[coord.y * settings.base_lightmap_size + coord.x],
		           ( 1.0f / (settings.lightfield_size * settings.lightfield_size * 3 ) ) * mse * mse );
	}
}


// Update gradients for all gaussian paramaters
__global__ void update_parameters_kernel( TrainerKernelSettings settings,
										  const float* d_grad,
										  float* d_gaussian_parameters,
										  float* d_parameter_grad,
										  const int ctr )
{
	const int pixel_x = threadIdx.x;
	const int pixel_y = ( blockIdx.x % settings.lightfield_size );
	const int channel = ( blockIdx.x / settings.lightfield_size );
	const int num_gaussian_parameters = settings.num_gaussians * NUM_PARAMS;
	const int id_in_batch = blockIdx.y / settings.num_gaussians;
	const int gaussian = ( blockIdx.y % settings.num_gaussians );

	// Get current parameters
	glm::vec3 xyz = glm::vec3( d_gaussian_parameters[id_in_batch * num_gaussian_parameters + gaussian * NUM_PARAMS + 0],
	                           d_gaussian_parameters[id_in_batch * num_gaussian_parameters + gaussian * NUM_PARAMS + 1],
	                           d_gaussian_parameters[id_in_batch * num_gaussian_parameters + gaussian * NUM_PARAMS + 2] );
	// Check if gaussian is marked as "dropped" 
	if (xyz == glm::vec3(0.0f)) return; 
	glm::vec3 Direction = normalize(xyz);
	float s = d_gaussian_parameters[id_in_batch * num_gaussian_parameters + gaussian * NUM_PARAMS + 3];
	float A = d_gaussian_parameters[id_in_batch * num_gaussian_parameters + gaussian * NUM_PARAMS + 4 + channel];

	// Get pixel gradient
	float pixel_gradient =
	        d_grad[id_in_batch * ( settings.lightfield_size * settings.lightfield_size * 3 )
	               + channel * ( settings.lightfield_size * settings.lightfield_size ) + pixel_y * settings.lightfield_size + pixel_x];

	// Calculate gradients
	float theta, phi;
	float fx, fy;
	if ( settings.jitter_directions )
	{
		seed_t seed = initialize_seed( make_uint2( pixel_x, pixel_y ), 0, ctr );
		fx = pixel_x + ::randf( seed );
		fy = pixel_y + ::randf( seed );
	}
	else
	{
		fx = pixel_x + 0.5f;
		fy = pixel_y + 0.5f;
	}
	GetThetaPhiFromTexCoords( fx / float(settings.lightfield_size),  fy / float(settings.lightfield_size), theta, phi );
	glm::vec3 direction = normalize( GetXYZFromThetaPhi( theta, phi ) );

#if DIRECTION_GRADIENT_MODE == DIRECTION_GRADIENT_MODE_DIRECT || DIRECTION_GRADIENT_MODE == DIRECTION_GRADIENT_MODE_TANGENT
	float a = dot( direction, Direction ) - 1;
	float b = exp( s * a );

	float Xgradient = A * b * s * direction.x;
	float Ygradient = A * b * s * direction.y;
	float Zgradient = A * b * s * direction.z;
	float sgradient = A * b * a;
	float Agradient = b;

#if DIRECTION_GRADIENT_MODE == DIRECTION_GRADIENT_MODE_TANGENT
	{
		glm::vec3 Vgrad( Xgradient, Ygradient, Zgradient );
		float VgradL = glm::length( Vgrad );
		Vgrad = glm::cross( Vgrad, Direction );
		Vgrad = glm::cross( Direction, Vgrad );

		float l = glm::length( Vgrad );
		if ( fabsf( l ) < 1e-6 )
		{
			Vgrad = glm::normalize( glm::perpendicular( Direction ) );
		}
		else
		{
			Vgrad /= l;
		}
		Vgrad = Vgrad * VgradL;

		Xgradient = Vgrad.x;
		Ygradient = Vgrad.y;
		Zgradient = Vgrad.z;
	}
#endif

#elif DIRECTION_GRADIENT_MODE == DIRECTION_GRADIENT_MODE_SPHERICAL
	float Xgradient = 0.f;
	float Ygradient = 0.f;
	float Zgradient = 0.f;
	float sgradient = 0.f;
	float Agradient = 0.f;
	{
		float Theta = 0.f, Phi = 0.f;
		GetThetaPhiFromXYZ( Direction, Theta, Phi );

		float a = -1 +
			cos( theta ) * cos( Theta ) +
			cos( phi ) * cos( Phi ) * sin( theta ) * sin( Theta ) +
			sin( phi ) * sin( Phi ) * sin( theta ) * sin( Theta );
		float b = exp( s * a );


		float ThetaGradient = 0.0f, PhiGradient = 0.0f;
		ThetaGradient = b * A * s * (cos( phi )*cos( Phi )*cos( Theta )*sin( theta ) + cos( Theta )*sin( phi )*sin( Phi )*sin( theta ) - cos( theta )*sin( Theta ));
		PhiGradient = b * A * s * sin( phi - Phi ) * sin( theta ) * sin( Theta );
		sgradient = b * A * (-1 + cos( theta )*cos( Theta ) + cos( phi )*cos( Phi )*sin( theta )*sin( Theta ) + sin( phi )*sin( Phi )*sin( theta )*sin( Theta ));
		Agradient = b;

		glm::vec3 XYZgrad = (GetXYZFromThetaPhi( Theta + ThetaGradient, Phi + PhiGradient ) - direction);
		Xgradient = XYZgrad.x;
		Ygradient = XYZgrad.y;
		Zgradient = XYZgrad.z;
	}
#else

#error "Not Implemented"

#endif

	// Atomically add up this blocks results
	// HACK boosting moveability
	Xgradient = 1.0f * ( 1.0f / 32.0f ) * warpReduceSum( Xgradient * pixel_gradient );
	Ygradient = 1.0f * ( 1.0f / 32.0f ) * warpReduceSum( Ygradient * pixel_gradient );
	Zgradient = 1.0f * ( 1.0f / 32.0f ) * warpReduceSum( Zgradient * pixel_gradient );
	sgradient = 10.0f * ( 1.0f / 32.0f ) * warpReduceSum( sgradient * pixel_gradient );
	Agradient = 1.0f * ( 1.0f / 32.0f ) * warpReduceSum( Agradient * pixel_gradient );

	// Finally atomic add to global parameters
	if ( threadIdx.x % 32 == 0 )
	{
		atomicAdd( &d_parameter_grad[id_in_batch * num_gaussian_parameters + gaussian * NUM_PARAMS + 0],
		           ( 32.0f / float( gridDim.x * blockDim.x ) ) * Xgradient );
		atomicAdd( &d_parameter_grad[id_in_batch * num_gaussian_parameters + gaussian * NUM_PARAMS + 1],
		           ( 32.0f / float( gridDim.x * blockDim.x ) ) * Ygradient );
		atomicAdd( &d_parameter_grad[id_in_batch * num_gaussian_parameters + gaussian * NUM_PARAMS + 2],
		           ( 32.0f / float( gridDim.x * blockDim.x ) ) * Zgradient );
		atomicAdd( &d_parameter_grad[id_in_batch * num_gaussian_parameters + gaussian * NUM_PARAMS + 3],
		           ( 32.0f / float( gridDim.x * blockDim.x ) ) * sgradient );
		atomicAdd( &d_parameter_grad[id_in_batch * num_gaussian_parameters + gaussian * NUM_PARAMS + 4 + channel],
		           ( 32.0f / float( gridDim.x * blockDim.x ) ) * Agradient );
	}
}


__global__ void constrain_forward_kernel( TrainerKernelSettings settings,
										  const float* d_input_data,
										  float* d_output_data,
										  const int ctr,
										  int texture_size = 0,
										  glm::u16vec2 * d_coords = nullptr,
										  float * d_valid = nullptr )
{
	const int tid = blockIdx.x * blockDim.x + threadIdx.x;

	if (tid < settings.num_gaussians * NUM_PARAMS * BATCH_SIZE)
	{



		if (texture_size != 0) {
			// Also, set valid coords
			int batch_id = tid / settings.num_gaussians * NUM_PARAMS;
			d_valid[d_coords[batch_id].y*texture_size + d_coords[batch_id].x] = 1.0f;
		}

		if ( settings.drop_random_gaussians )
		{
			// A whole gaussian might be marked as "dropped" 
			int batch_id = tid / settings.num_gaussians * NUM_PARAMS;
			int gaussian_id = (tid % settings.num_gaussians * NUM_PARAMS) / NUM_PARAMS;
			seed_t seed = initialize_seed( make_uint2( gaussian_id, batch_id ), ctr, 1234 );
			float xi = ::randf( seed );
			if (xi < 0.00001f)
			{
				d_output_data[tid] = 0.0f;
				return; 
			}
		}

#if NO_CONSTRAINTS
		d_output_data[tid] = d_input_data[tid];
#else
		if ( tid % NUM_PARAMS <= 2 )
		{   // X, Y and Z
#if CONSTRAIN_TO_UNIT_LENGTH && false   // Doing this causes nan's and should not be necessary?
			float x = d_input_data[( tid / NUM_PARAMS ) * NUM_PARAMS + 0];
			float y = d_input_data[( tid / NUM_PARAMS ) * NUM_PARAMS + 1];
			float z = d_input_data[( tid / NUM_PARAMS ) * NUM_PARAMS + 2];
			float d = x * x + y * y + z * z;
			if ( d < 0.001f )
			{
				d_output_data[tid] = 0.001f;
			}
			else
			{
				d_output_data[tid] = d_output_data[tid] / sqrt( d );
			}
#else
			d_output_data[tid] = d_input_data[tid];
#endif
		}
		else
		{   // s and A
#if CONSTRAIN_A_AND_S_POSITIVE
			d_output_data[tid] = max( MIN_AMPLITUDE, d_input_data[tid] );
#else
			d_output_data[tid] = d_input_data[tid];
#endif
		}
#endif
	}
}
__global__ void constrain_backward_kernel( TrainerKernelSettings settings,
                                           const float* d_input_data,
                                           float* d_input_grad,
                                           const float* d_output_grad,
                                           glm::vec3* d_normals,
                                           const int ctr,
                                           glm::u16vec2* d_batch_coords = nullptr,
                                           float* d_parameter_texture = nullptr,
										   int * d_valid_texture = nullptr)
{
	const int tid = blockIdx.x * blockDim.x + threadIdx.x;
	const int batch_id = tid / (settings.num_gaussians * NUM_PARAMS);

	if (tid < settings.num_gaussians * NUM_PARAMS * BATCH_SIZE)
	{
#if NO_CONSTRAINTS
		d_input_grad[tid] = d_output_grad[tid];
#else

		//if ( settings.drop_random_gaussians )
		//{
		//	int batch_id = tid / settings.num_gaussians * NUM_PARAMS;
		//	int gaussian_id = (tid % settings.num_gaussians * NUM_PARAMS) / NUM_PARAMS;
		//	seed_t seed = initialize_seed(make_uint2(gaussian_id, batch_id), ctr, 1234);
		//	float xi = ::randf( seed );
		//	if (xi < 0.01f)
		//	{
		//		d_input_grad[tid] = 0.0f;
		//		return; 
		//	}
		//}

		if (tid % NUM_PARAMS <= 2)
		{   // X, Y and Z

			// If we are using fixed directions, simply nevery update directions. 
			if (settings.fixed_directions) {
				d_input_grad[tid] = 0.0f; 
				return; 
			}

#if CONSTRAIN_TO_UNIT_LENGTH
			float x = d_input_data[(tid / NUM_PARAMS) * NUM_PARAMS + 0];
			float y = d_input_data[(tid / NUM_PARAMS) * NUM_PARAMS + 1];
			float z = d_input_data[(tid / NUM_PARAMS) * NUM_PARAMS + 2];
			float X = d_input_data[tid];   // Is x, y, or z

			float l = sqrt(x * x + y * y + z * z);

			float dMSEdX = (2.0f * X * (-1.0f + l)) / l;
			d_input_grad[tid] = d_output_grad[tid] + (1.0f / float(BATCH_SIZE)) * dMSEdX;
#else
			d_input_grad[tid] = d_output_grad[tid];
#endif

			if (settings.constrain_upper_hemisphere)
			{
				/* If E = -dot(n,v)
				glm::vec3 n = d_normals[batch_id];
				glm::vec3 v = { x, y, z };
				if (dot(n, v) < 0) {
					float dMSEdX = -d_normals[batch_id][(tid % NUM_PARAMS)];
					d_input_grad[tid] += (1.0f / float(BATCH_SIZE)) * dMSEdX;
				}
				*/
				// if E = dot(n,v)^2
				glm::vec3 n = d_normals[batch_id];
				glm::vec3 v = { x, y, z };
				if (dot(n, v) < 0) {
					float nX = n[tid%NUM_PARAMS];
					float dMSEdX = 2.0f * nX * dot(n, v);
					d_input_grad[tid] += (1.0f / float(BATCH_SIZE)) * dMSEdX;
				}
			}
		}
		else
		{   // s and A
#if CONSTRAIN_A_AND_S_POSITIVE
			if (tid % NUM_PARAMS >= 3) {
				if (d_input_data[tid] > MIN_AMPLITUDE) {
					d_input_grad[tid] = d_output_grad[tid];
				}
				else {
					d_input_grad[tid] = d_output_grad[tid] + (1.0f / float(BATCH_SIZE)) * 2.0f * (d_input_data[tid] - MIN_AMPLITUDE);
				}
			}
			//	float r = d_input_data[(tid / NUM_PARAMS) * NUM_PARAMS + 4];
			//	float g = d_input_data[(tid / NUM_PARAMS) * NUM_PARAMS + 5];
			//	float b = d_input_data[(tid / NUM_PARAMS) * NUM_PARAMS + 6];
			//	float C = d_input_data[tid];   // Is r, g, or b
			//	d_input_grad[tid] = (1.0f / float(BATCH_SIZE)) * (2.0f / 3.0f) * ((1.0f / 3.0f) * (r + g + b) - bias);
			//}
			//else {
			//	if (d_input_data[tid] > 0.0f)
			//	{
			//		d_input_grad[tid] = d_output_grad[tid];
			//	}
			//	else
			//	{
			//		d_input_grad[tid] = (1.0f / float(BATCH_SIZE)) * 2.0f * d_input_data[tid];
			//	}
			//}
#else
			d_input_grad[tid] = d_output_grad[tid];
#endif
		}
#endif
		///////////////////////////////////////////////////////////////////////
		// Optionally change the gradient to also aim for being close to the 
		// average of neighbours
		///////////////////////////////////////////////////////////////////////
#if 1
		if (d_batch_coords != nullptr)
		{
			int x = d_batch_coords[batch_id].x;
			int y = d_batch_coords[batch_id].y;
			const int num_gaussians = settings.num_gaussians;
			glm::vec3 avg_dir = glm::vec3(0.0f);
			float avg_sharpness = 0.0f;
			glm::vec3 avg_amplitude = glm::vec3(0.0f);
			float w_sum = 0.0f;
			for (int Y = -1; Y <= 1; Y++)
			{
				for (int X = -1; X <= 1; X++)
				{
					// Don't include my last value in average
					if (Y == 0 && X == 0) continue;
					// Avoid neighbours outside texture
					if ((x + X == -1) || (x + X == settings.base_lightmap_size)) continue;
					if ((y + Y == -1) || (y + Y == settings.base_lightmap_size)) continue;
					// Avoid non-valid neighbours
					if (d_valid_texture[(y + Y) * settings.base_lightmap_size + (x + X)] == 0) continue;

					int gaussian_id = (tid / NUM_PARAMS) % settings.num_gaussians;

					glm::vec3 dir = glm::vec3(
						d_parameter_texture[((y + Y) * settings.base_lightmap_size + (x + X)) * num_gaussians * NUM_PARAMS + gaussian_id * NUM_PARAMS + 0],
						d_parameter_texture[((y + Y) * settings.base_lightmap_size + (x + X)) * num_gaussians * NUM_PARAMS + gaussian_id * NUM_PARAMS + 1],
						d_parameter_texture[((y + Y) * settings.base_lightmap_size + (x + X)) * num_gaussians * NUM_PARAMS + gaussian_id * NUM_PARAMS + 2]);
					float sharpness = d_parameter_texture[((y + Y) * settings.base_lightmap_size + (x + X)) * num_gaussians * NUM_PARAMS + gaussian_id * NUM_PARAMS + 3];
					glm::vec3 amplitude = glm::vec3(
						d_parameter_texture[((y + Y) * settings.base_lightmap_size + (x + X)) * num_gaussians * NUM_PARAMS + gaussian_id * NUM_PARAMS + 4],
						d_parameter_texture[((y + Y) * settings.base_lightmap_size + (x + X)) * num_gaussians * NUM_PARAMS + gaussian_id * NUM_PARAMS + 5],
						d_parameter_texture[((y + Y) * settings.base_lightmap_size + (x + X)) * num_gaussians * NUM_PARAMS + gaussian_id * NUM_PARAMS + 6]);
					float w = (1.0f / 3.0f) * (amplitude.x + amplitude.y * amplitude.z);
					w_sum += w;
					avg_dir += w * dir;
					avg_sharpness += w * sharpness;
					avg_amplitude += w * amplitude;
				}
			}
			if (w_sum == 0.0f) {
				d_input_grad[tid] = 0.0f;
				return;
			}
			avg_dir = normalize(avg_dir);
			//avg_dir = (1.0f / w_sum) * avg_dir;
			avg_amplitude = (1.0f / w_sum) * avg_amplitude;
			avg_sharpness = (1.0f / w_sum) * avg_sharpness;

			float grad = 0.0f; 
			if (tid % NUM_PARAMS == 0) grad = 2.0f * (d_input_data[tid] - avg_dir.x);
			if (tid % NUM_PARAMS == 1) grad = 2.0f * (d_input_data[tid] - avg_dir.y);
			if (tid % NUM_PARAMS == 2) grad = 2.0f * (d_input_data[tid] - avg_dir.z);
			if (tid % NUM_PARAMS == 3) grad = 2.0f * (d_input_data[tid] - avg_sharpness);
			if (tid % NUM_PARAMS == 4) grad = 2.0f * (d_input_data[tid] - avg_amplitude.x);
			if (tid % NUM_PARAMS == 5) grad = 2.0f * (d_input_data[tid] - avg_amplitude.y);
			if (tid % NUM_PARAMS == 6) grad = 2.0f * (d_input_data[tid] - avg_amplitude.z);
			
			grad *= (1.0f / float(BATCH_SIZE));

			d_input_grad[tid] = DIRECT_C * grad + (1.0f - DIRECT_C)  * d_input_grad[tid];
		}
#endif

	}
}


__global__ void calc_distance_gradient( TrainerKernelSettings settings,
										int LIGHTMAP_WIDTH,
										int LIGHTMAP_HEIGHT,
										const float* d_valid,
										const float* d_data,
										float* d_grad)
{
	const int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if (tid < settings.num_gaussians * NUM_PARAMS * LIGHTMAP_WIDTH * LIGHTMAP_HEIGHT)
	{
		int texel = tid / (settings.num_gaussians * NUM_PARAMS);
		int param = tid % (settings.num_gaussians * NUM_PARAMS);
		int coord_x = texel / LIGHTMAP_WIDTH;
		int coord_y = texel % LIGHTMAP_WIDTH; 
		if (d_valid[coord_y * LIGHTMAP_WIDTH + coord_x]) {
			float my_param = d_data[coord_y * LIGHTMAP_WIDTH * settings.num_gaussians * NUM_PARAMS + coord_x * settings.num_gaussians * NUM_PARAMS + param];
			float avg_param = 0.0f; 
			int valid_neighbours = 0; 
			for (int y = -1; y <= 1; y++) {
				for (int x = -1; x <= 1; x++) {
					//if (y == 0 && x == 0) continue; 
					int n_coord_x = coord_x + x; 
					int n_coord_y = coord_y + y; 
					if (n_coord_x < 1 || n_coord_x > LIGHTMAP_WIDTH - 2 || n_coord_y < 1 || n_coord_y > LIGHTMAP_HEIGHT - 2) continue; 
					if (d_valid[n_coord_y * LIGHTMAP_WIDTH + n_coord_x] == 0.0f) continue; 
					float n_param = d_data[n_coord_y * LIGHTMAP_WIDTH * settings.num_gaussians * NUM_PARAMS + n_coord_x * settings.num_gaussians * NUM_PARAMS + param];
					valid_neighbours += 1;
					avg_param += n_param;
				}
			}
			if (valid_neighbours > 0) avg_param /= float(valid_neighbours);
			float gradient = 2.0f * (my_param - avg_param);
			d_grad[coord_y * LIGHTMAP_WIDTH * settings.num_gaussians * NUM_PARAMS + coord_x * settings.num_gaussians * NUM_PARAMS + param] = 
				(1.0f / float(BATCH_SIZE)) * gradient;
		}
	}
}


__global__ void distance_mse_kernel( TrainerKernelSettings settings,
									 const float* d_middle_params,
									 const float* d_neighbour_params,
									 float* d_neighbour_grad )
{
	const int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if ( tid < settings.num_gaussians * NUM_PARAMS * BATCH_SIZE )
	{
		float e = d_middle_params[tid] - d_neighbour_params[tid];
		d_neighbour_grad[tid] = (1.0f / float(BATCH_SIZE)) * 2.0f * e;
	}
}

void prepareTrainingBatch( chag::TrainingDataScheduler& scheduler, tensor& input_batch, tensor& input_batch_normals, void* coords = nullptr )
{
	static vector<float> batch_data( BATCH_SIZE * s_kernel_settings.lightfield_size * s_kernel_settings.lightfield_size * IMAGE_CHANNELS );
	static vector<glm::vec3> batch_normals( BATCH_SIZE );
	const size_t image_size = s_kernel_settings.lightfield_size * s_kernel_settings.lightfield_size * IMAGE_CHANNELS;
	for ( int i = 0; i < BATCH_SIZE; i++ )
	{
		if ( scheduler.getEpochDataSetsRemaining() <= 0 )
		{
			// Doing nothing... train for crap data
		}
		else
		{
			auto img = scheduler.getNextTrainingData();
			batch_normals[i] = img.getNormal();
			float* data = img.get();
			if ( data == nullptr )
			{
				cout << "ERROR: Data broken.\n" << flush;
			}
			if ( coords != nullptr )
			{
				((glm::u16vec2*)coords)[i] = img.getCoords();
			}
			for ( size_t j = 0; j < image_size; ++j )
			{
				// For not entirely certain reasons (maybe parameter initialization ?) training 
				// mostly produces NaNs if input is > 1.0. Therefore, we normalize the input 
				// here. 
				// TODO: Normalization should really depend on the true maximum incoming radiance. 
				float r = max(0.0f, data[j] / SCALE_INPUT_RADIANCE);
#if CLAMP_INPUT_RADIANCE
				r = min( 1.0f, r);
#endif
				batch_data[image_size * i + j] = r;
			}
		}
	} 
	input_batch.SetData( batch_data.data() );
	input_batch_normals.SetData( reinterpret_cast<float*>( batch_normals.data() ) );
}

void saveSGData(const std::string& output_path, glm::uvec2 lm_size, const float* dev_data )
{
	const string filename = output_path + s_settings.filename_prefix + "_epoch_" + to_string( s_epoch ) + ".sgt";
	cout << "Writing (" << lm_size.x << "x" << lm_size.y << ") texture to " << filename << "..." << flush;
	uint64_t size = lm_size.x * lm_size.y * s_kernel_settings.num_gaussians * NUM_PARAMS;
	vector<float> data(size);
	checkCudaErr(cudaMemcpy(data.data(), dev_data, size * sizeof(float), cudaMemcpyDeviceToHost));

	///////////////////////////////////////////////////////////////////////////
	// Amplitudes are scaled since input data was normalized. Rescale here. 
	///////////////////////////////////////////////////////////////////////////
	for (uint64_t i = 0; i < size; i++) if ((i%NUM_PARAMS) > 3) data[i] *= SCALE_INPUT_RADIANCE; 

	// SGT (Spherical Gaussian Texture) file format, version 2:
	// 4 bytes: magic number
	// 2 bytes: version
	// 2 bytes: base lightmap width
	// 2 bytes: base lightmap height
	// 2 bytes: number of gaussians
	// 1 byte: gaussian parameters format: 0 > float, 1 > half-float
	// 1 byte: number of parameters per gaussian
	// csv\0: for each parameter, its semantic association, separated by commas. Null-terminated string.
	// 4 byte floats: data
	// CHANGELOG
	// v2: Add semantic association, it wasn't working previously so it was removed
	{
		float magic = 54168231936.f;
		uint16_t version = 2;
		uint16_t lm_width = lm_size.x;
		uint16_t lm_height = lm_size.y;
		uint16_t gp_num_gaussians = s_kernel_settings.num_gaussians;
		uint8_t gp_format = 0;
		uint8_t gp_num_parameters = NUM_PARAMS;
		const char* gp_params_semantic = "x,y,z,sharpness,r,g,b";

		ofstream os( filename, ios::binary );
		os.write( reinterpret_cast<const char*>(&magic), sizeof( magic ) );
		os.write( reinterpret_cast<const char*>(&version), sizeof( version ) );
		os.write( reinterpret_cast<const char*>(&lm_width), sizeof( lm_width ) );
		os.write( reinterpret_cast<const char*>(&lm_height), sizeof( lm_height ) );
		os.write( reinterpret_cast<const char*>(&gp_num_gaussians), sizeof( gp_num_gaussians ) );
		os.write( reinterpret_cast<const char*>(&gp_format), sizeof( gp_format ) );
		os.write( reinterpret_cast<const char*>(&gp_num_parameters), sizeof( gp_num_parameters ) );
		os.write( gp_params_semantic, std::strlen( gp_params_semantic ) + 1 );
		os.write( reinterpret_cast<const char*>(data.data()), data.size() * sizeof( float ) );
	}

}

void saveDnnParams( const std::string& output_path,
                    vector<layer*>& layers,
                    vector<tensor*>& intermediate_outputs,
                    tensor& final_output )
{
	const string filename = output_path + s_settings.filename_prefix + "_epoch_" + to_string( s_epoch ) + ".nnp";
	cout << "Writing Dnn parameters to " << filename << "..." << flush;
	ofstream of( filename );
	chag::exportDnnLayers( of, layers, intermediate_outputs, final_output );
}

vector<glm::vec3> GetRandomDirs()
{
	const int sqrt_num_gaussians = sqrt(s_settings.num_gaussians);
	vector<glm::vec3> random_dir;
	int W = sqrt_num_gaussians;
	int H = sqrt_num_gaussians;
	if (sqrt_num_gaussians * sqrt_num_gaussians != s_settings.num_gaussians) {
		if (s_settings.num_gaussians == 32) {
			W = 8;
			H = 4;
		}
		else if (s_settings.num_gaussians == 8) {
			W = 4;
			H = 2;
		}
		else if (s_settings.num_gaussians == 2) {
			W = 1;
			H = 2;
		}
		else {
			cout << "WARNING: Use sqrtable num gaussians.\n";
		}
	}
	for (int y = 0; y < H; y++) {
		for (int x = 0; x < W; x++) {
			float xi0 = (y + 0.5f - 0.25f + 0.5f * randf()) / float(H);
			float xi1 = (x + 0.5f - 0.25f + 0.5f * randf()) / float(W);
			glm::vec3 dir = { 2.0f * cos(2.0f * M_PI * xi0) * sqrt(xi1 * (1.0f - xi1)),
								2.0f * sin(2.0f * M_PI * xi0) * sqrt(xi1 * (1.0f - xi1)),
								1.0f - 2.0f * xi1 };
			random_dir.push_back(dir);
		}
	}
	return random_dir; 
}

float * GetNormalizedPixelAreas()
{
	vector<float> pixel_areas( s_kernel_settings.lightfield_size * s_kernel_settings.lightfield_size );

	{
		// Get the four corners of each pixel
		const size_t lfs1 = (s_kernel_settings.lightfield_size + 1);
		vector<glm::vec3> corner_positions( lfs1 * lfs1 );
		for ( size_t nv = 0; nv < lfs1; ++nv )
		{
			float v = float( nv ) / s_kernel_settings.lightfield_size;
			for ( size_t nu = 0; nu < lfs1; ++nu )
			{
				float u = float( nu ) / s_kernel_settings.lightfield_size;
				float theta = 0.f, phi = 0.f;
				GetThetaPhiFromTexCoords( u, v, theta, phi );
				corner_positions[nv * lfs1 + nu] = GetXYZFromThetaPhi( theta, phi );
			}
		}

		// Calculate the area of each pixel from its four corners
		const size_t lfs = s_kernel_settings.lightfield_size;
		float avgA = 0;
		for ( size_t y = 0; y < lfs; ++y )
		{
			for ( size_t x = 0; x < lfs; ++x )
			{
				// According to https://en.wikipedia.org/wiki/Quadrilateral#Vector_formulas
				// The area of a quadrilateral can be obtained from its diagonal vectors AC and BD with
				// Area = 1/2 * | AC x BD |
				glm::vec3 ac = corner_positions[(y + 1) * lfs1 + (x + 1)] - corner_positions[y * lfs1 + x];
				glm::vec3 bd = corner_positions[y * lfs1 + (x + 1)] - corner_positions[(y + 1) * lfs1 + x];
				float A = 0.5f * glm::length( glm::cross( ac, bd ) );
				pixel_areas[y * lfs + x] = A;
				avgA += A;
			}
		}
		avgA /= lfs * lfs;

		// Let's normalize the areas
		for ( size_t y = 0; y < lfs; ++y )
		{
			for ( size_t x = 0; x < lfs; ++x )
			{
				pixel_areas[y * lfs + x] /= avgA;
			}
		}
	}
	float * d_pixel_areas; 
	cudaMalloc( &d_pixel_areas, pixel_areas.size() * sizeof( float ) );
	cudaMemcpy( d_pixel_areas, pixel_areas.data(), pixel_areas.size() * sizeof( float ), cudaMemcpyHostToDevice );
	return d_pixel_areas; 
}

void SetupTrainingNetwork( vector<layer*>& layers, vector<tensor*>& intermediate_output_tensors, tensor& final_output_tensor )
{
	auto AddEncodeStep = [&layers, &intermediate_output_tensors]( int input_channels, int output_channels, int input_width, int input_height ) {
		{   // Convolution
			convolution_layer* conv = new convolution_layer( input_channels, output_channels, 3 );
			tensor* output = new tensor( BATCH_SIZE, output_channels, input_width, input_height );
			tensor temp_input_tensor( BATCH_SIZE, input_channels, input_width, input_height );
			conv->FindAlgorithmsAndWorkspace( temp_input_tensor, *output );
			layers.push_back( conv );
			intermediate_output_tensors.push_back( output );
		}
		{   // Relu
			// TODO: Should be leaky!
			activation_layer* relu = new activation_layer( CUDNN_ACTIVATION_RELU );
			tensor* output = new tensor( BATCH_SIZE, output_channels, input_width, input_height );
			layers.push_back( relu );
			intermediate_output_tensors.push_back( output );
		}
		{   // Max Pooling
			pool_layer* pool = new pool_layer();
			tensor* output = new tensor( BATCH_SIZE, output_channels, input_width / 2, input_height / 2 );
			layers.push_back( pool );
			intermediate_output_tensors.push_back( output );
		}
	};
	vector<int> channels = { 3, 32, 64, 128, 256 };
	AddEncodeStep( channels[0], channels[1], s_kernel_settings.lightfield_size, s_kernel_settings.lightfield_size );
	AddEncodeStep( channels[1], channels[2], s_kernel_settings.lightfield_size / 2, s_kernel_settings.lightfield_size / 2 );
	AddEncodeStep( channels[2], channels[3], s_kernel_settings.lightfield_size / 4, s_kernel_settings.lightfield_size / 4 );
	AddEncodeStep( channels[3], channels[4], s_kernel_settings.lightfield_size / 8, s_kernel_settings.lightfield_size / 8 );

	///////////////////////////////////////////////////////////////////////
	// End with an fc layer that creates the gaussian parameters
	// (X, Y, Z, s, Ar, Ag, Ab)
	///////////////////////////////////////////////////////////////////////
	const int num_gaussian_parameters = s_kernel_settings.num_gaussians * NUM_PARAMS;

#if EXTRA_FC_LAYER
	const int fc1_channels = num_gaussian_parameters;
	fc_layer* fc1 = new fc_layer(channels[4] * (s_kernel_settings.lightfield_size / 16) * (s_kernel_settings.lightfield_size / 16), fc1_channels);
	layers.push_back(fc1);
	tensor fc1_output(BATCH_SIZE, 1, 1, fc1_channels);
	intermediate_output_tensors.push_back(&fc1_output);

	activation_layer* relu1 = new activation_layer(CUDNN_ACTIVATION_RELU);
	tensor* relu1_output = new tensor(BATCH_SIZE, 1, 1, fc1_channels);
	layers.push_back(relu1);
	intermediate_output_tensors.push_back(relu1_output);

	fc_layer* fc0 = new fc_layer(fc1_channels, num_gaussian_parameters);
#else
	fc_layer* fc0 = new fc_layer(channels[4] * (s_kernel_settings.lightfield_size / 16) * (s_kernel_settings.lightfield_size / 16), num_gaussian_parameters);
#endif

	///////////////////////////////////////////////////////////////////////////
	// Create a jittered set of directions on the hemisphere. Used both as 
	// starting position for training and as the fixed directions when not 
	// training directions. 
	///////////////////////////////////////////////////////////////////////////
	vector<glm::vec3> random_dir = GetRandomDirs();

	// Prime the bias of the fc0 network to get reasonable starting gaussians
	vector<float> bias( num_gaussian_parameters );
	for ( int i = 0; i < s_kernel_settings.num_gaussians; i++ )
	{
		//glm::vec3 dir = normalize( glm::vec3( randf() * 2.0f - 1.0f, randf() * 2.0f - 1.0f, randf() * 2.0f - 1.0f ) );
		glm::vec3 dir = random_dir[i];
		bias[i * NUM_PARAMS + 0] = dir.x;
		bias[i * NUM_PARAMS + 1] = dir.y;
		bias[i * NUM_PARAMS + 2] = dir.z;
		bias[i * NUM_PARAMS + 3] = START_SHARPNESS;
		bias[i * NUM_PARAMS + 4] = START_COLOR;
		bias[i * NUM_PARAMS + 5] = START_COLOR;
		bias[i * NUM_PARAMS + 6] = START_COLOR;
	}
	fc0->bias.SetData( bias.data() );

	layers.push_back( fc0 );
	tensor* fc0_output = new tensor( BATCH_SIZE, 1, 1, num_gaussian_parameters );
	intermediate_output_tensors.push_back( fc0_output );

	final_output_tensor = tensor( BATCH_SIZE, 1, 1, num_gaussian_parameters );
}

void TrainWithoutInterpolation( const std::string& filepath )
{
	///////////////////////////////////////////////////////////////////////
	// Initialize training scheduler
	///////////////////////////////////////////////////////////////////////
	chag::TrainingDataScheduler dataScheduler( filepath, s_settings.channel_remap );

	s_kernel_settings.base_lightmap_size = dataScheduler.getLightmapSize().x;
	s_kernel_settings.lightfield_size = dataScheduler.getLightfieldSize();

	std::string output_path = dataScheduler.getBaseFilePath() + s_settings.output_path;
	filesystem::create_directory( output_path );

	size_t num_batches = dataScheduler.getTrainingSetSize() / BATCH_SIZE;

	///////////////////////////////////////////////////////////////////////
	// Set up tensors
	///////////////////////////////////////////////////////////////////////
	tensor batch_images = tensor( BATCH_SIZE, IMAGE_CHANNELS, s_kernel_settings.lightfield_size, s_kernel_settings.lightfield_size, false );
	tensor batch_normals = tensor( BATCH_SIZE, 1, 1, 3, false );
	// Used for the single batch used for debug output
	tensor test_image = tensor( BATCH_SIZE, IMAGE_CHANNELS, s_kernel_settings.lightfield_size, s_kernel_settings.lightfield_size, false );
	tensor test_normals = tensor( BATCH_SIZE, 1, 1, 3, false );
	tensor test_predicted_image( BATCH_SIZE, IMAGE_CHANNELS, s_kernel_settings.lightfield_size, s_kernel_settings.lightfield_size );
	dataScheduler.prepareNewEpoch();
	prepareTrainingBatch( dataScheduler, test_image, test_normals );
	dataScheduler.endEpoch();


	///////////////////////////////////////////////////////////////////////
	// Set up pixel areas
	///////////////////////////////////////////////////////////////////////
	float* d_pixel_areas = GetNormalizedPixelAreas(); 

	///////////////////////////////////////////////////////////////////////
	// Set up training network
	///////////////////////////////////////////////////////////////////////
	vector<layer*> layers;
	vector<tensor*> layer_outputs;

	tensor gaussian_parameters;

	if ( s_settings.initial_network_params.empty() )
	{
		SetupTrainingNetwork( layers, layer_outputs, gaussian_parameters );
	}
	else
	{
		std::ifstream f( s_settings.initial_network_params );
		if ( !f )
		{
			cout << "ERROR while trying to read file " << s_settings.initial_network_params << endl;
			return;
		}
		if ( !chag::importDnnLayers( f, layers, layer_outputs, gaussian_parameters ) )
		{
			return;
		}
	}

	// Tensor containing final predicted image and gradients
	tensor predicted_image( BATCH_SIZE, IMAGE_CHANNELS, s_kernel_settings.lightfield_size, s_kernel_settings.lightfield_size );

	// Create the texture containing all current parameter values
	// Invalidate all by setting X to -FLT_MAX
	const int LIGHTMAP_WIDTH = dataScheduler.getLightmapSize().x;
	const int LIGHTMAP_HEIGHT = dataScheduler.getLightmapSize().y;
	tensor gaussian_parameter_texture(LIGHTMAP_HEIGHT, LIGHTMAP_WIDTH, s_kernel_settings.num_gaussians, NUM_PARAMS);
	vector<float> temp = gaussian_parameter_texture.DownloadFlatData();
	for (auto & e : temp) e = -FLT_MAX; 
	gaussian_parameter_texture.SetData(temp.data());

	// Counter for randomness
	int ctr = 0;

	// Keeping track of error
	tensor mse_image = tensor(1, 1, s_kernel_settings.base_lightmap_size, s_kernel_settings.base_lightmap_size, false);
	float * d_mse = nullptr;
	checkCudaErr(cudaMalloc(&d_mse, sizeof(float)));


	///////////////////////////////////////////////////////////////////////
	// Forward Propagation
	///////////////////////////////////////////////////////////////////////
	auto Forward = [&layers, &layer_outputs, &ctr, &settings=s_kernel_settings](tensor & input_images, tensor & output_parameters) 
	{
		layers[0]->Forward(input_images, *layer_outputs[0] );
		for (int j = 1; j < layers.size(); j++) {
			layers[j]->Forward(*layer_outputs[j - 1], *layer_outputs[j]);
		}
		// Constrain parameters
		dim3 block = { 128, 1, 1 };
		dim3 grid = { ( settings.num_gaussians * NUM_PARAMS * BATCH_SIZE - 1 ) / block.x + 1 };
		auto & fc0_output = *layer_outputs.back();
		constrain_forward_kernel<<<grid, block>>>( settings, fc0_output.d_data, output_parameters.d_data, ctr );
	};

	///////////////////////////////////////////////////////////////////////
	// Back Propagation
	///////////////////////////////////////////////////////////////////////
	auto Backprop = [&layers, &layer_outputs, &ctr, &settings=s_kernel_settings](tensor & input_images, tensor & output_parameters, tensor & batch_normals)
	{
		auto & fc0_output = *layer_outputs.back();
		// Constraints
		dim3 block = { 128, 1, 1 };
		dim3 grid = { (settings.num_gaussians * NUM_PARAMS * BATCH_SIZE - 1) / block.x + 1 };
		constrain_backward_kernel << <grid, block >> > ( settings, fc0_output.d_data, fc0_output.d_grad, output_parameters.d_grad, (glm::vec3 *)batch_normals.d_data, ctr);
		// Back propagation
		for (int64_t j = layers.size() - 1; j > 0; j--)
		{
			layers[j]->Backward(*layer_outputs[j - 1], *layer_outputs[j]);
		}
		layers[0]->Backward(input_images, *layer_outputs[0]);
	};

	//////////////////|   ||/////////////////////////////////////////////////////
	// Loss function  ||  |_
	///////////////////////////////////////////////////////////////////////
	auto Loss = [&d_mse, &mse_image, &ctr, &d_pixel_areas, &settings=s_kernel_settings]( 
					tensor& input_parameters,
	                tensor& validation_image,
	                tensor& batch_normals,
					glm::u16vec2 * d_batch_coords,
	                tensor& output_image, 
					bool update_mse) 
	{
		// We now have the gaussian parameters. Calculate predicted image and gaussian parameter gradients.
		checkCudaErr(cudaMemset(input_parameters.d_grad, 0, input_parameters.SizeInBytes()));
		dim3 block = { 16, 16, 1 };
		dim3 grid = { settings.lightfield_size / 16ul, settings.lightfield_size / 16ul, IMAGE_CHANNELS * BATCH_SIZE };
		create_image_kernel << <grid, block >> > (
			settings,
			input_parameters.d_data,
			validation_image.d_data,
			output_image.d_data,
			output_image.d_grad,
			batch_normals.d_data,
			d_batch_coords,
			update_mse ? d_mse : nullptr,
			update_mse ? mse_image.d_data : nullptr,
			d_pixel_areas,
			ctr);

		// Update parameter gradients
		// One block does one line of one color channel
		// Start one block per line, color channel and gaussian
		block = { 128, 1, 1 };
		grid = { settings.lightfield_size * uint32_t(IMAGE_CHANNELS), settings.num_gaussians * BATCH_SIZE };
		update_parameters_kernel << <grid, block >> > ( settings,
			output_image.d_grad, input_parameters.d_data, input_parameters.d_grad, ctr);
	};

	///////////////////////////////////////////////////////////////////////
	// Train one epoch at a time
	///////////////////////////////////////////////////////////////////////
	vector<float> mse_vector; 
	for ( uint64_t &epoch = s_epoch; s_settings.max_epochs == 0 || epoch < s_settings.max_epochs; epoch++ )
	{
		bool outputEpochData = false
			|| ( epoch == 0 )
			|| ( epoch == 1 )
			|| (((epoch != 0) && (epoch < 20)) && (epoch % 4 == 0))
			|| ((epoch >= 20 && epoch < 50) && (epoch % 8 == 0))
			|| ((epoch >= 50) && (epoch % 16 == 0));

		{   // Minimize predicted image MSE
			Timestamp epoch_start = Clock::now();
			dataScheduler.prepareNewEpoch();
			checkCudaErr( cudaMemset( d_mse, 0, sizeof( float ) ) );
			mse_image.ResetZero();
			size_t b = 0;
			while ( BATCH_SIZE <= dataScheduler.getEpochDataSetsRemaining() )
			{
				///////////////////////////////////////////////////////////////
				// Grab image, normals and coords for current training batch
				///////////////////////////////////////////////////////////////
				static vector<glm::u16vec2> h_batch_coords( BATCH_SIZE );
				static glm::u16vec2* d_batch_coords = nullptr;
				if ( d_batch_coords == nullptr )
					checkCudaErr( cudaMalloc( &d_batch_coords, BATCH_SIZE * sizeof( glm::u16vec2 ) ) );
				prepareTrainingBatch( dataScheduler, batch_images, batch_normals, h_batch_coords.data() );
				checkCudaErr( cudaMemcpy(
				        d_batch_coords, h_batch_coords.data(), BATCH_SIZE * sizeof( glm::u16vec2 ), cudaMemcpyHostToDevice ) );

				///////////////////////////////////////////////////////////////
				// Forward Propagation
				///////////////////////////////////////////////////////////////
				Forward(batch_images, gaussian_parameters);

				///////////////////////////////////////////////////////////////
				// Evaluate loss function
				///////////////////////////////////////////////////////////////
				Loss(gaussian_parameters, batch_images, batch_normals, d_batch_coords, predicted_image, true); 

				///////////////////////////////////////////////////////////////
				// Output images
				///////////////////////////////////////////////////////////////
				if ( epoch == 0 && b == 0 )
				{
					DumpTensorAsImage(
							test_image,
							output_path + "input_colors_epoch_" + to_string( epoch ) + ".png",
							1.0f,
							false );
				}
				if ( b == 0 && outputEpochData )
				{
					Forward(test_image, gaussian_parameters);
					Loss(gaussian_parameters, test_image, test_normals, nullptr, test_predicted_image, false);
					DumpTensorAsImage( test_predicted_image,
					                   output_path + "output_epoch_" + to_string( epoch ) + ".png",
					                   1.0f,
					                   false );
				}

				///////////////////////////////////////////////////////////////
				// Back propagation
				///////////////////////////////////////////////////////////////
				Backprop(batch_images, gaussian_parameters, batch_normals);

				///////////////////////////////////////////////////////////////
				// Update weights
				// Note: Optionally ramp learning rate up during the first ten 
				//       epochs, then decay slowly
				///////////////////////////////////////////////////////////////
				//const float learning_rate = 0.00005f;
				const float learning_rate = 0.00001f;
				
				float ramped_learning_rate = learning_rate; 
				if (s_settings.ramp_learning_rate) {
					if(epoch < 10) {
						ramped_learning_rate = learning_rate * pow(1.25893f, epoch);
					}
					else {
						ramped_learning_rate = (1.0f / sqrt(float(epoch))) * 10.0f * learning_rate; 
					}
				}

				for ( uint64_t j = 0; j < layers.size(); j++ )
				{
					layers[j]->UpdateWeights( ramped_learning_rate );
				}

				///////////////////////////////////////////////////////////////
				// Output batch statistics
				///////////////////////////////////////////////////////////////
				Timestamp batch_end = Clock::now();
				double batch_time = std::chrono::duration_cast<Milliseconds>( batch_end - epoch_start ).count();
				double images_per_second = double( b * BATCH_SIZE ) / ( batch_time / 1000.0 );
				cout << "Epoch " << epoch << ": Batch " << b << "/" << num_batches << ": (" << images_per_second
				     << " images/sec)" << " LR = " << ramped_learning_rate << ( ( b == num_batches - 1 ) ? "        " : "\r" ) << flush;
				++b;
				ctr++;

				s_batch += 1;
			}

			///////////////////////////////////////////////////////////////////
			// Write mse image for this epoch
			///////////////////////////////////////////////////////////////////
			if ( outputEpochData )
			{
				vector<float> data = mse_image.DownloadFlatData();
				vector<uint8_t> u8data( data.size() );
				for ( int i = 0; i < int( data.size() ); i++ )
				{
					u8data[i] = uint8_t( max( 0.0f, min( 255.0f, data[i] * 300.0f * 256.0f ) ) );
				}
				const string& filename = output_path + "mse_image_" + to_string( epoch ) + ".png";
				stbi_write_png(
				        filename.c_str(), dataScheduler.getLightmapSize().x, dataScheduler.getLightmapSize().y, 1, u8data.data(), 0 );
			}

			dataScheduler.endEpoch();

			Timestamp epoch_end = Clock::now();
			double epoch_time = std::chrono::duration_cast<Milliseconds>( epoch_end - epoch_start ).count();
			float MSE;
			checkCudaErr( cudaMemcpy( &MSE, d_mse, sizeof( float ), cudaMemcpyDeviceToHost ) );
			mse_vector.push_back(MSE / float(num_batches));
			cout << ". Done. " << epoch_time / 1000 << "sec. MSE: " << MSE / float( num_batches ) << "\n";
		}
		///////////////////////////////////////////////////////////////////////
		// Dump MSE plot
		///////////////////////////////////////////////////////////////////////
		{
			ofstream os(output_path + "mseplot.py");
			os << "import matplotlib.pyplot as plt\n";
			os << "import numpy as np\n";
			os << "x = np.linspace(0, " << mse_vector.size() << ", " << mse_vector.size() << ")\n";
			os << "y = ["; 
			for (int i = 0; i < mse_vector.size() - 1; i++) os << mse_vector[i] << ", ";
			os << mse_vector.back() << "]\n";
			os << "plt.semilogx(x, y, label='mse')\n";
			os << "plt.show()\n";
		}

		///////////////////////////////////////////////////////////////////////
		// Generate gaussian parameter textures
		///////////////////////////////////////////////////////////////////////
		if ( outputEpochData )
		{
			///////////////////////////////////////////////////////////////////
			// Run whole epoch forward once to get current texture
			///////////////////////////////////////////////////////////////////
			dataScheduler.prepareStraightforwardEpoch();
			int num_images = dataScheduler.getEpochDataSetsRemaining();
			int remaining_images = dataScheduler.getEpochDataSetsRemaining();
			while (remaining_images > 0)
			{
				///////////////////////////////////////////////////////////////
				// Grab image, normals and coords for current training batch
				///////////////////////////////////////////////////////////////
				static vector<glm::u16vec2> h_batch_coords(BATCH_SIZE);
				static glm::u16vec2* d_batch_coords = nullptr;
				if (d_batch_coords == nullptr)
					checkCudaErr(cudaMalloc(&d_batch_coords, BATCH_SIZE * sizeof(glm::u16vec2)));
				prepareTrainingBatch(dataScheduler, batch_images, batch_normals, h_batch_coords.data());
				checkCudaErr(cudaMemcpy(
					d_batch_coords, h_batch_coords.data(), BATCH_SIZE * sizeof(glm::u16vec2), cudaMemcpyHostToDevice));

				///////////////////////////////////////////////////////////////
				// Forward Propagation
				///////////////////////////////////////////////////////////////
				Forward(batch_images, gaussian_parameters);

				///////////////////////////////////////////////////////////////
				// Copy generated parameters to texture
				///////////////////////////////////////////////////////////////
				for (int i = 0; i < BATCH_SIZE; i++) {
					glm::u16vec2& coord = h_batch_coords[i];
					checkCudaErr(cudaMemcpy(gaussian_parameter_texture.d_data
						+ coord.y * dataScheduler.getLightmapSize().x * s_kernel_settings.num_gaussians * NUM_PARAMS
						+ coord.x * s_kernel_settings.num_gaussians * NUM_PARAMS,
						gaussian_parameters.d_data + i * s_kernel_settings.num_gaussians * NUM_PARAMS,
						s_kernel_settings.num_gaussians * NUM_PARAMS * sizeof(float),
						cudaMemcpyDeviceToDevice));
				}

				remaining_images = dataScheduler.getEpochDataSetsRemaining();
				cout << "Generating texture (" << remaining_images << "/" << num_images << ")              \r";
			}
			cout << endl; 
			dataScheduler.endEpoch();

			saveSGData( output_path, dataScheduler.getLightmapSize(), gaussian_parameter_texture.d_data );

			cout << "Outputting NN parameters" << endl;

			saveDnnParams( output_path, layers, layer_outputs, gaussian_parameters );

			cout << "done." << endl;
		}
	}
}


void DirectOptimization(const std::string& filepath)
{
	///////////////////////////////////////////////////////////////////////
	// Initialize training scheduler
	///////////////////////////////////////////////////////////////////////
	chag::TrainingDataScheduler dataScheduler(filepath, s_settings.channel_remap);

	s_kernel_settings.base_lightmap_size = dataScheduler.getLightmapSize().x;
	s_kernel_settings.lightfield_size = dataScheduler.getLightfieldSize();

	std::string output_path = dataScheduler.getBaseFilePath() + "nonet_" + s_settings.output_path;
	filesystem::create_directory(output_path);
	size_t num_batches = dataScheduler.getTrainingSetSize() / BATCH_SIZE;

	///////////////////////////////////////////////////////////////////////
	// Set up tensors
	///////////////////////////////////////////////////////////////////////
	tensor batch_images = tensor(BATCH_SIZE, IMAGE_CHANNELS, s_kernel_settings.lightfield_size, s_kernel_settings.lightfield_size, false);
	tensor batch_normals = tensor(BATCH_SIZE, 1, 1, 3, false);
	tensor batch_parameters = tensor(BATCH_SIZE, 1, 1, s_kernel_settings.num_gaussians * NUM_PARAMS);
	tensor batch_adam_r = tensor(BATCH_SIZE, 1, 1, s_kernel_settings.num_gaussians * NUM_PARAMS, false);
	tensor batch_adam_s = tensor(BATCH_SIZE, 1, 1, s_kernel_settings.num_gaussians * NUM_PARAMS, false);
	tensor batch_parameters_constrained = tensor(BATCH_SIZE, 1, 1, s_kernel_settings.num_gaussians * NUM_PARAMS);

	///////////////////////////////////////////////////////////////////////////
	// Create the lightfield texture, initialized with fixed random directions
	///////////////////////////////////////////////////////////////////////////
	vector<glm::vec3> random_dir = GetRandomDirs();
	const int LIGHTMAP_WIDTH = dataScheduler.getLightmapSize().x;
	const int LIGHTMAP_HEIGHT = dataScheduler.getLightmapSize().y;
	tensor gaussian_parameter_texture(LIGHTMAP_HEIGHT, LIGHTMAP_WIDTH, s_kernel_settings.num_gaussians, NUM_PARAMS);
	tensor adam_r_texture(LIGHTMAP_HEIGHT, LIGHTMAP_WIDTH, s_kernel_settings.num_gaussians, NUM_PARAMS, false);
	tensor adam_s_texture(LIGHTMAP_HEIGHT, LIGHTMAP_WIDTH, s_kernel_settings.num_gaussians, NUM_PARAMS, false);
	cudaMemset(adam_r_texture.d_data, 0, adam_r_texture.NumValues() * sizeof(float));
	cudaMemset(adam_s_texture.d_data, 0, adam_s_texture.NumValues() * sizeof(float));

	vector<float> temp = gaussian_parameter_texture.DownloadFlatData();
	for (int i = 0; i < LIGHTMAP_WIDTH * LIGHTMAP_HEIGHT; i++) {
		for (int j = 0; j < s_kernel_settings.num_gaussians; j++) {
			temp[i * NUM_PARAMS * s_kernel_settings.num_gaussians + j * NUM_PARAMS + 0] = random_dir[j].x;
			temp[i * NUM_PARAMS * s_kernel_settings.num_gaussians + j * NUM_PARAMS + 1] = random_dir[j].y;
			temp[i * NUM_PARAMS * s_kernel_settings.num_gaussians + j * NUM_PARAMS + 2] = random_dir[j].z;
			temp[i * NUM_PARAMS * s_kernel_settings.num_gaussians + j * NUM_PARAMS + 3] = START_SHARPNESS;
			temp[i * NUM_PARAMS * s_kernel_settings.num_gaussians + j * NUM_PARAMS + 4] = START_COLOR;
			temp[i * NUM_PARAMS * s_kernel_settings.num_gaussians + j * NUM_PARAMS + 5] = START_COLOR;
			temp[i * NUM_PARAMS * s_kernel_settings.num_gaussians + j * NUM_PARAMS + 6] = START_COLOR;
		}
	}
	gaussian_parameter_texture.SetData(temp.data());

	///////////////////////////////////////////////////////////////////////
	// Keep track of valid texels
	///////////////////////////////////////////////////////////////////////
	vector<int> valid_texture(LIGHTMAP_WIDTH * LIGHTMAP_HEIGHT, 0);
	int * d_valid_texture;
	cudaMalloc(&d_valid_texture, LIGHTMAP_WIDTH * LIGHTMAP_HEIGHT * sizeof(int));
	cudaMemset(d_valid_texture, 0, LIGHTMAP_WIDTH * LIGHTMAP_HEIGHT * sizeof(int));

	///////////////////////////////////////////////////////////////////////
	// Set up pixel areas
	///////////////////////////////////////////////////////////////////////
	float* d_pixel_areas = GetNormalizedPixelAreas();

	///////////////////////////////////////////////////////////////////////////
	// TODO: Mark all non-used texels by rolling through one epoch of data
	///////////////////////////////////////////////////////////////////////////


	// Tensor containing final predicted image and gradients
	tensor predicted_image(BATCH_SIZE, IMAGE_CHANNELS, s_kernel_settings.lightfield_size, s_kernel_settings.lightfield_size);

	// Counter for randomness
	int ctr = 0;

	// Keeping track of error
	float * d_mse = nullptr;
	checkCudaErr(cudaMalloc(&d_mse, sizeof(float)));

	///////////////////////////////////////////////////////////////////////
	// Loss function
	///////////////////////////////////////////////////////////////////////
	auto Loss = [&d_mse, &ctr, &settings = s_kernel_settings, &d_pixel_areas](
		tensor& input_parameters,
		tensor& validation_image,
		tensor& batch_normals,
		glm::u16vec2 * d_batch_coords,
		tensor& output_image,
		bool update_mse)
	{
		// We now have the gaussian parameters. Calculate predicted image and gaussian parameter gradients.
		checkCudaErr(cudaMemset(input_parameters.d_grad, 0, input_parameters.SizeInBytes()));
		dim3 block = { 16, 16, 1 };
		dim3 grid = { settings.lightfield_size / 16ul, settings.lightfield_size / 16ul, IMAGE_CHANNELS * BATCH_SIZE };
		create_image_kernel << <grid, block >> > (
			settings,
			input_parameters.d_data,
			validation_image.d_data,
			output_image.d_data,
			output_image.d_grad,
			batch_normals.d_data,
			d_batch_coords,
			update_mse ? d_mse : nullptr,
			nullptr,
			d_pixel_areas,
			ctr);

		// Update parameter gradients
		// One block does one line of one color channel
		// Start one block per line, color channel and gaussian
		block = { 128, 1, 1 };
		grid = { settings.lightfield_size * uint32_t(IMAGE_CHANNELS), settings.num_gaussians * BATCH_SIZE };
		update_parameters_kernel << <grid, block >> > (settings,
			output_image.d_grad, input_parameters.d_data, input_parameters.d_grad, ctr);
	};


	///////////////////////////////////////////////////////////////////////
	// Train one epoch at a time
	///////////////////////////////////////////////////////////////////////
	vector<float> mse_vector;
	for (int epoch = 0; s_settings.max_epochs == 0 || epoch < s_settings.max_epochs; epoch++)
	{
		{   // Minimize predicted image MSE
			Timestamp epoch_start = Clock::now();
			dataScheduler.prepareNewEpoch();
			checkCudaErr(cudaMemset(d_mse, 0, sizeof(float)));
			size_t b = 0;
			while (BATCH_SIZE <= dataScheduler.getEpochDataSetsRemaining())
			{
				///////////////////////////////////////////////////////////////
				// Grab image, normals and coords for current training batch
				///////////////////////////////////////////////////////////////
				static vector<glm::u16vec2> h_batch_coords(BATCH_SIZE);
				static glm::u16vec2* d_batch_coords = nullptr;
				if (d_batch_coords == nullptr)
					checkCudaErr(cudaMalloc(&d_batch_coords, BATCH_SIZE * sizeof(glm::u16vec2)));
				prepareTrainingBatch(dataScheduler, batch_images, batch_normals, h_batch_coords.data());
				checkCudaErr(cudaMemcpy(
					d_batch_coords, h_batch_coords.data(), BATCH_SIZE * sizeof(glm::u16vec2), cudaMemcpyHostToDevice));

				// Mark used coords as valid textures
				for (auto & c : h_batch_coords) {
					valid_texture[c.y * LIGHTMAP_WIDTH + c.x] = 1;
				}

				///////////////////////////////////////////////////////////////
				// Copy current gaussians into batch
				// Note: If first epoch, provide start values
				///////////////////////////////////////////////////////////////
				for (int i = 0; i < BATCH_SIZE; i++) {
					int params_in_texel = s_kernel_settings.num_gaussians * NUM_PARAMS;
					float* d_src = gaussian_parameter_texture.d_data + (h_batch_coords[i].y * LIGHTMAP_WIDTH + h_batch_coords[i].x) * params_in_texel;
					cudaMemcpy( batch_parameters.d_data + i * params_in_texel,
					            d_src,
					            params_in_texel * sizeof( float ),
					            cudaMemcpyDeviceToDevice );
					// Adam 
					d_src = adam_r_texture.d_data + (h_batch_coords[i].y * LIGHTMAP_WIDTH + h_batch_coords[i].x) * params_in_texel;
					cudaMemcpy(batch_adam_r.d_data + i * params_in_texel, d_src, params_in_texel * sizeof(float), cudaMemcpyDeviceToDevice);
					d_src = adam_s_texture.d_data + (h_batch_coords[i].y * LIGHTMAP_WIDTH + h_batch_coords[i].x) * params_in_texel;
					cudaMemcpy(batch_adam_s.d_data + i * params_in_texel, d_src, params_in_texel * sizeof(float), cudaMemcpyDeviceToDevice);
				}

				///////////////////////////////////////////////////////////////
				// Apply constraints forward
				///////////////////////////////////////////////////////////////
				{
					dim3 block = { 128, 1, 1 };
					dim3 grid = { (s_kernel_settings.num_gaussians * NUM_PARAMS * BATCH_SIZE - 1) / block.x + 1 };
					constrain_forward_kernel << <grid, block >> > (s_kernel_settings, batch_parameters.d_data, batch_parameters_constrained.d_data, ctr);
				}

				///////////////////////////////////////////////////////////////
				// Evaluate loss function
				///////////////////////////////////////////////////////////////
				Loss(batch_parameters_constrained, batch_images, batch_normals, d_batch_coords, predicted_image, true);
				//Loss(batch_parameters, batch_images, batch_normals, d_batch_coords, predicted_image, true);

				///////////////////////////////////////////////////////////////
				// Apply constraints backward
				///////////////////////////////////////////////////////////////
				{
					dim3 block = { 128, 1, 1 };
					dim3 grid = { (s_kernel_settings.num_gaussians * NUM_PARAMS * BATCH_SIZE - 1) / block.x + 1 };
					constrain_backward_kernel << <grid, block >> > (s_kernel_settings, batch_parameters.d_data, batch_parameters.d_grad, batch_parameters_constrained.d_grad, (glm::vec3 *)batch_normals.d_data, ctr, d_batch_coords, gaussian_parameter_texture.d_data, d_valid_texture);
				}


				///////////////////////////////////////////////////////////////
				// Move in gradient direction
				// Note: Optionally ramp learning rate up during the first ten 
				//       epochs, then decay slowly
				///////////////////////////////////////////////////////////////
				//const float learning_rate = 0.00005f;
				//const float learning_rate = 0.00001f;
				//const float learning_rate = 0.01f;
				const float learning_rate = 0.01f;

				float ramped_learning_rate = learning_rate;
				if (s_settings.ramp_learning_rate) {
					if (epoch < 10) {
						ramped_learning_rate = learning_rate * pow(1.25893f, epoch);
					}
					else {
						ramped_learning_rate = (1.0f / sqrt(float(epoch))) * 10.0f * learning_rate;
					}
				}


				///////////////////////////////////////////////////////////////
				// Update parameters with ADAM optimizer
				///////////////////////////////////////////////////////////////
				const int num_parameters = batch_parameters.NumValues();
				{  
					int threads = 256;
					int blocks = (num_parameters - 1) / threads + 1;
					adam_update_parameters_kernel << <blocks, threads >> > (num_parameters, batch_parameters.d_data, batch_adam_s.d_data, batch_adam_r.d_data,
						batch_parameters.d_grad, adam_beta_1, adam_beta_2, ADAM_T_VAR, learning_rate);
				}

				///////////////////////////////////////////////////////////////
				// Copy batch back to texture
				///////////////////////////////////////////////////////////////

				for (int i = 0; i < h_batch_coords.size(); i++) {
					int params_in_texel = s_kernel_settings.num_gaussians * NUM_PARAMS;
					float* d_dst = gaussian_parameter_texture.d_data
						+ (h_batch_coords[i].y * LIGHTMAP_WIDTH + h_batch_coords[i].x) * params_in_texel;
					cudaMemcpy(d_dst,
						batch_parameters.d_data + i * params_in_texel,						
						params_in_texel * sizeof(float),
						cudaMemcpyDeviceToDevice);
					// Adam 
					d_dst = adam_r_texture.d_data + (h_batch_coords[i].y * LIGHTMAP_WIDTH + h_batch_coords[i].x) * params_in_texel;
					cudaMemcpy(d_dst, batch_adam_r.d_data + i * params_in_texel, params_in_texel * sizeof(float), cudaMemcpyDeviceToDevice);
					d_dst = adam_s_texture.d_data + (h_batch_coords[i].y * LIGHTMAP_WIDTH + h_batch_coords[i].x) * params_in_texel;
					cudaMemcpy(d_dst, batch_adam_s.d_data + i * params_in_texel, params_in_texel * sizeof(float), cudaMemcpyDeviceToDevice);
				}

				///////////////////////////////////////////////////////////////
				// Output batch statistics
				///////////////////////////////////////////////////////////////
				Timestamp batch_end = Clock::now();
				double batch_time = std::chrono::duration_cast<Milliseconds>(batch_end - epoch_start).count();
				double images_per_second = double(b * BATCH_SIZE) / (batch_time / 1000.0);
				cout << "Epoch " << epoch << ": Batch " << b << "/" << num_batches << ": (" << images_per_second
					<< " images/sec)" << " LR = " << ramped_learning_rate << ((b == num_batches - 1) ? "        " : "\r") << flush;
				++b;
				ctr++;

				s_batch += 1;
			}
			dataScheduler.endEpoch();

			Timestamp epoch_end = Clock::now();
			double epoch_time = std::chrono::duration_cast<Milliseconds>(epoch_end - epoch_start).count();
			float MSE;
			checkCudaErr(cudaMemcpy(&MSE, d_mse, sizeof(float), cudaMemcpyDeviceToHost));
			mse_vector.push_back(MSE / float(num_batches));
			cout << ". Done. " << epoch_time / 1000 << "sec. MSE: " << MSE / float(num_batches) << "\n";
		}
//		cout << "max deviation from normal: " << max_deviation << endl;

		///////////////////////////////////////////////////////////////////////
		// Dump MSE plot
		///////////////////////////////////////////////////////////////////////
		{
			ofstream os(output_path + "mseplot.py");
			os << "import matplotlib.pyplot as plt\n";
			os << "import numpy as np\n";
			os << "x = np.linspace(0, " << mse_vector.size() << ", " << mse_vector.size() << ")\n";
			os << "y = [";
			for (int i = 0; i < mse_vector.size() - 1; i++) os << mse_vector[i] << ", ";
			os << mse_vector.back() << "]\n";
			os << "plt.semilogx(x, y, label='mse')\n";
			os << "plt.show()\n";
		}

		///////////////////////////////////////////////////////////////////////
		// Generate gaussian parameter textures
		///////////////////////////////////////////////////////////////////////
		//if ((epoch > 0) && ((epoch == 3 || ((epoch < 1000) && (epoch % 8 == 7))) || (epoch % 100) == 0))
		if(epoch % 10 == 0)
		{
			cout << "Generating texture\r";
			saveSGData(output_path, dataScheduler.getLightmapSize(), gaussian_parameter_texture.d_data);
			cout << "done.\n";
		}


		///////////////////////////////////////////////////////////////////
		// Update valid texture (somewhat hacky)
		///////////////////////////////////////////////////////////////////		
		cudaMemcpy(d_valid_texture, valid_texture.data(), LIGHTMAP_WIDTH * LIGHTMAP_HEIGHT * sizeof(int), cudaMemcpyHostToDevice);
		cudaMemcpy(valid_texture.data(), d_valid_texture, LIGHTMAP_WIDTH * LIGHTMAP_HEIGHT * sizeof(int), cudaMemcpyDeviceToHost);
		vector<uint8_t> data(valid_texture.size());
		for (int i = 0; i < LIGHTMAP_WIDTH*LIGHTMAP_HEIGHT; i++) { data[i] = valid_texture[i] == 1 ? 255 : 0; };
		stbi_write_png("test.png", LIGHTMAP_WIDTH, LIGHTMAP_HEIGHT, 1, data.data(), 0);


#if 0
		///////////////////////////////////////////////////////////////////
		// Attempt to make gaussians coherent. 
		///////////////////////////////////////////////////////////////////		
		vector<float> parameter_texture = gaussian_parameter_texture.DownloadFlatData(); 
		vector<float> out_parameter_texture = gaussian_parameter_texture.DownloadFlatData();

		for (int y = 1; y < LIGHTMAP_HEIGHT - 1; y++)
		{
			for (int x = 1; x < LIGHTMAP_WIDTH - 1; x++)
			{
				if (!valid_texture[y * LIGHTMAP_WIDTH + x]) continue;
				const int num_gaussians = s_settings.num_gaussians;
				glm::vec3 my_dir = {
					parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 0],
					parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 1],
					parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 2]
				};
				glm::vec3 avg_dir = glm::vec3(0.0f);
				float avg_sharpness = 0.0f;
				glm::vec3 avg_amplitude = glm::vec3(0.0f);
				float w_sum = 0.0f; 
				for (int Y = -1; Y <= 1; Y++)
				{
					for (int X = -1; X <= 1; X++)
					{
						if (Y == 0 && X == 0) continue; 
						glm::vec3 dir= glm::vec3(
							parameter_texture[((y + Y) * LIGHTMAP_WIDTH + (x + X)) * num_gaussians * NUM_PARAMS + 0],
							parameter_texture[((y + Y) * LIGHTMAP_WIDTH + (x + X)) * num_gaussians * NUM_PARAMS + 1],
							parameter_texture[((y + Y) * LIGHTMAP_WIDTH + (x + X)) * num_gaussians * NUM_PARAMS + 2]);
						if (!valid_texture[(y + Y) * LIGHTMAP_WIDTH + (x + X)]) continue;

						float sharpness = parameter_texture[((y + Y) * LIGHTMAP_WIDTH + (x + X)) * num_gaussians * NUM_PARAMS + 3];
						glm::vec3 amplitude = glm::vec3(
						        parameter_texture[( ( y + Y ) * LIGHTMAP_WIDTH + ( x + X ) ) * num_gaussians * NUM_PARAMS + 4],
						        parameter_texture[( ( y + Y ) * LIGHTMAP_WIDTH + ( x + X ) ) * num_gaussians * NUM_PARAMS + 5],
						        parameter_texture[( ( y + Y ) * LIGHTMAP_WIDTH + ( x + X ) ) * num_gaussians * NUM_PARAMS + 6] );
						float w = (1.0f / 3.0f) * (amplitude.x + amplitude.y * amplitude.z);
						w_sum += w; 
						avg_dir += w * dir; 
						avg_sharpness += w * sharpness; 
						avg_amplitude += w * amplitude; 
					}
				}
				if (w_sum == 0.0f) continue; 
				const float C = 0.75f;

				my_dir = normalize(my_dir + C * avg_dir);

				float sharpness = parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 3];
				sharpness = (1.0f - C) * sharpness + C * (avg_sharpness * (1.0f / w_sum));
				glm::vec3 amplitude = {
					parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 4],
					parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 5],
					parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 6]
				};
				amplitude = (1.0f - C) * amplitude + C * (avg_amplitude * (1.0f / w_sum));
				
				out_parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 0] = my_dir.x;
				out_parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 1] = my_dir.y;
				out_parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 2] = my_dir.z;
				out_parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 3] = sharpness;
				out_parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 4] = amplitude.x;
				out_parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 5] = amplitude.y;
				out_parameter_texture[(y * LIGHTMAP_WIDTH + x) * num_gaussians * NUM_PARAMS + 6] = amplitude.z;				
			}
		}
		gaussian_parameter_texture.SetData(out_parameter_texture.data());
#endif

	}
}



void Init( TrainerSettings settings )
{
	s_settings = settings;
	s_kernel_settings.num_gaussians = s_settings.num_gaussians;
	s_kernel_settings.drop_random_gaussians = s_settings.drop_random_gaussians;
	s_kernel_settings.jitter_directions = s_settings.jitter_directions;
	s_kernel_settings.separate_islands = s_settings.separate_islands;
    s_kernel_settings.fixed_directions = s_settings.fixed_directions;
	s_kernel_settings.constrain_upper_hemisphere = s_settings.constrain_upper_hemisphere; 

	///////////////////////////////////////////////////////////////////////
	// Setup the device
	///////////////////////////////////////////////////////////////////////
	checkCudaErr( cudaSetDevice( s_settings.cuda_device ) );
	int device;
	struct cudaDeviceProp device_properties;
	checkCudaErr( cudaGetDevice( &device ) );
	checkCudaErr( cudaGetDeviceProperties( &device_properties, device ) );
	cout << device << ": "<< device_properties.name << ":" << endl;
	cout << "=========================================\n";
	cout << "Compute capability: " << device_properties.major << "." << device_properties.minor << endl;
	cout << "Concurrent copy and kernel execution: " << ( device_properties.deviceOverlap ? "Yes" : "No" ) << " with "
	     << device_properties.asyncEngineCount << " copy engine(s)\n";
	cout << "=========================================\n";
	///////////////////////////////////////////////////////////////////////
	// Init cudnn and cublas
	///////////////////////////////////////////////////////////////////////
	checkCudnnErr( cudnnCreate( &cudnn_handle ) );
	checkCublasErr( cublasCreate( &cublas_handle ) );
	///////////////////////////////////////////////////////////////////////
	// Create vector of ones
	///////////////////////////////////////////////////////////////////////
	ones_tensor.Resize( BATCH_SIZE, 1, 1, 1 );
	vector<float> ones( BATCH_SIZE, 1.0f );
	ones_tensor.SetData( ones.data() );
}
}   // namespace dnn