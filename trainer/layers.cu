#include "common.h"
#include "error_handling.h"
#include "layers.h"
#include <cublas_v2.h>

using namespace std;


namespace dnn {
	extern cublasHandle_t cublas_handle;
	extern cudnnHandle_t cudnn_handle;

/////////////////////////////////////////////////////////////////////////////
// Adam optimizer kernel
/////////////////////////////////////////////////////////////////////////////

extern uint64_t s_epoch;
extern uint64_t s_batch;

extern tensor ones_tensor;

__global__ void adam_update_parameters_kernel(int num_values, float *parameters, float *s, float *r, const float *g,
                                              const float beta_1, const float beta_2, const int t,
                                              const float step_size) {
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(tid >= num_values) return;
	// Correct bias in first moment
	s[tid]              = beta_1 * s[tid] + (1.0f - beta_1) * g[tid];
	r[tid]              = beta_2 * r[tid] + (1.0f - beta_2) * g[tid] * g[tid];
	float shat          = s[tid] / (1.0f - pow(beta_1, t + 1));
	float rhat          = r[tid] / (1.0f - pow(beta_2, t + 1));
	const float epsilon = 1e-8f;
	float delta         = -step_size * (shat / (sqrt(rhat) + epsilon));
	//if (isnan(delta)) delta = 0.0f; 
	parameters[tid]     = parameters[tid] + delta;
}

/////////////////////////////////////////////////////////////////////////////
// A convolution layer with square, odd-sized filter, no stride or dilation.
/////////////////////////////////////////////////////////////////////////////

void convolution_layer::init(int num_input_channels, int num_output_channels, int filter_size) {
	// Create filter and bias with xavier random weights
	f.Resize(num_output_channels, num_input_channels, filter_size, filter_size);
	// float dist_size = sqrt(3.0f / float(f.dim.w * f.dim.h* f.dim.c));
	// cout << dist_size << "\n\n\n\n";
	float variance = 1.0f / float(f.dim.w * f.dim.h * f.dim.c);
	f.ResetRandom(variance);
	bias.Resize(1, num_output_channels, 1, 1);
	// Bias can safely be initialized to zero
	bias.ResetZero();

	// Adam optimizer init
	checkCudaErr(cudaMalloc(&d_adam_s, (f.NumValues() + bias.NumValues()) * sizeof(float)));
	checkCudaErr(cudaMalloc(&d_adam_r, (f.NumValues() + bias.NumValues()) * sizeof(float)));
	checkCudaErr(cudaMemset(d_adam_s, 0, (f.NumValues() + bias.NumValues()) * sizeof(float)));
	checkCudaErr(cudaMemset(d_adam_r, 0, (f.NumValues() + bias.NumValues()) * sizeof(float)));

	// Create convolution descriptor
	int padding[]  = {(f.dim.h - 1) / 2, (f.dim.w - 1) / 2};
	int stride[]   = {1, 1};
	int dilation[] = {1, 1};
	checkCudnnErr(cudnnCreateConvolutionDescriptor(&convolution_descriptor));
	checkCudnnErr(cudnnSetConvolutionNdDescriptor(convolution_descriptor, 2, padding, stride, dilation,
	                                              CUDNN_CROSS_CORRELATION, CUDNN_DATA_FLOAT));
}

void convolution_layer::FindAlgorithmsAndWorkspace(tensor &input_tensor, tensor &output_tensor) {
	if(workspace != nullptr) {
		cudaFree(workspace);
		workspace_size = 0;
	}
	uint64_t required_workspace_size;
#if CUDNN_MAJOR == 7
	checkCudnnErr(cudnnGetConvolutionForwardAlgorithm(
	    cudnn_handle, input_tensor.descriptor, f.descriptor, convolution_descriptor, output_tensor.descriptor,
	    CUDNN_CONVOLUTION_FWD_PREFER_FASTEST, 0xFFFFFFFF, &forward_algorithm));
	checkCudnnErr(cudnnGetConvolutionForwardWorkspaceSize(cudnn_handle, input_tensor.descriptor, f.descriptor,
	                                                      convolution_descriptor, output_tensor.descriptor,
	                                                      forward_algorithm, &required_workspace_size));
	workspace_size = max(workspace_size, required_workspace_size);
	checkCudnnErr(cudnnGetConvolutionBackwardFilterAlgorithm(
	    cudnn_handle, input_tensor.descriptor, output_tensor.descriptor, convolution_descriptor, f.descriptor,
	    CUDNN_CONVOLUTION_BWD_FILTER_PREFER_FASTEST, 0, &backward_filter_algorithm));
	checkCudnnErr(cudnnGetConvolutionBackwardFilterWorkspaceSize(
	    cudnn_handle, input_tensor.descriptor, output_tensor.descriptor, convolution_descriptor, f.descriptor,
	    backward_filter_algorithm, &required_workspace_size));
	workspace_size = max(workspace_size, required_workspace_size);
	checkCudnnErr(cudnnGetConvolutionBackwardDataAlgorithm(
	    cudnn_handle, f.descriptor, output_tensor.descriptor, convolution_descriptor, input_tensor.descriptor,
	    CUDNN_CONVOLUTION_BWD_DATA_PREFER_FASTEST, 0, &backward_data_algorithm));
	checkCudnnErr(cudnnGetConvolutionBackwardDataWorkspaceSize(cudnn_handle, f.descriptor, output_tensor.descriptor,
	                                                           convolution_descriptor, input_tensor.descriptor,
	                                                           backward_data_algorithm, &required_workspace_size));
#else
	{
		cudnnConvolutionFwdAlgoPerf_t perfResults;
		int nPerfResults = 0;
		checkCudnnErr( cudnnGetConvolutionForwardAlgorithm_v7( cudnn_handle,
															   input_tensor.descriptor,
															   f.descriptor,
															   convolution_descriptor,
															   output_tensor.descriptor,
															   1,
															   &nPerfResults,
															   &perfResults ) );
		if ( nPerfResults != 1 )
		{
			CUDA_FATAL_ERROR( "Error! unexpected value for nPerfResults" );
		}
		forward_algorithm = perfResults.algo;
	}
	checkCudnnErr( cudnnGetConvolutionForwardWorkspaceSize( cudnn_handle,
															input_tensor.descriptor,
															f.descriptor,
															convolution_descriptor,
															output_tensor.descriptor,
															forward_algorithm,
															&required_workspace_size ) );
	uint64_t new_workspace_size = required_workspace_size;

	{
		cudnnConvolutionBwdFilterAlgoPerf_t perfResults;
		int nPerfResults = 0;
		checkCudnnErr( cudnnGetConvolutionBackwardFilterAlgorithm_v7( cudnn_handle,
																	  input_tensor.descriptor,
																	  output_tensor.descriptor,
																	  convolution_descriptor,
																	  f.descriptor,
																	  1,
																	  &nPerfResults,
																	  &perfResults ) );
		if ( nPerfResults != 1 )
		{
			CUDA_FATAL_ERROR( "Error! unexpected value for nPerfResults" );
		}
		backward_filter_algorithm = perfResults.algo;
	}

	checkCudnnErr( cudnnGetConvolutionBackwardFilterWorkspaceSize( cudnn_handle,
																   input_tensor.descriptor,
																   output_tensor.descriptor,
																   convolution_descriptor,
																   f.descriptor,
																   backward_filter_algorithm,
																   &required_workspace_size ) );
	new_workspace_size = max( new_workspace_size, required_workspace_size );

	{
		cudnnConvolutionBwdDataAlgoPerf_t perfResults;
		int nPerfResults = 0;
		checkCudnnErr( cudnnGetConvolutionBackwardDataAlgorithm_v7( cudnn_handle,
																	f.descriptor,
																    output_tensor.descriptor,
																	convolution_descriptor,
																    input_tensor.descriptor,
																	1,
																	&nPerfResults,
																	&perfResults ) );
		if ( nPerfResults != 1 )
		{
			CUDA_FATAL_ERROR( "Error! unexpected value for nPerfResults" );
		}
		backward_data_algorithm = perfResults.algo;
	}

	checkCudnnErr( cudnnGetConvolutionBackwardDataWorkspaceSize( cudnn_handle,
																 f.descriptor,
																 output_tensor.descriptor,
																 convolution_descriptor,
																 input_tensor.descriptor,
																 backward_data_algorithm,
																 &required_workspace_size ) );
	required_workspace_size = max( new_workspace_size, required_workspace_size );
#endif
	workspace_size = max(workspace_size, required_workspace_size);
	if(workspace_size > 0) checkCudaErr(cudaMalloc(&workspace, workspace_size));
}

void convolution_layer::Forward(tensor &input_tensor, tensor &output_tensor) {
	checkCudnnErr(cudnnConvolutionForward(cudnn_handle, &alpha, input_tensor.descriptor, input_tensor.d_data,
	                                      f.descriptor, f.d_data, convolution_descriptor, forward_algorithm, workspace,
	                                      workspace_size, &beta, output_tensor.descriptor, output_tensor.d_data));
	// Bias
	checkCudnnErr(cudnnAddTensor(cudnn_handle, &alpha, bias.descriptor, bias.d_data, &alpha, output_tensor.descriptor,
	                             output_tensor.d_data));
}

void convolution_layer::Backward(tensor &input_tensor, tensor &output_tensor) {
	checkCudnnErr(cudnnConvolutionBackwardBias(cudnn_handle, &alpha, output_tensor.descriptor, output_tensor.d_grad,
	                                           &beta, bias.descriptor, bias.d_grad));
	checkCudnnErr(cudnnConvolutionBackwardFilter(cudnn_handle, &alpha, input_tensor.descriptor, input_tensor.d_data,
	                                             output_tensor.descriptor, output_tensor.d_grad, convolution_descriptor,
	                                             backward_filter_algorithm, workspace, workspace_size, &beta,
	                                             f.descriptor, f.d_grad));
	if(input_tensor.has_gradient) {
		checkCudnnErr(cudnnConvolutionBackwardData(
		    cudnn_handle, &alpha, f.descriptor, f.d_data, output_tensor.descriptor, output_tensor.d_grad,
		    convolution_descriptor, backward_data_algorithm, workspace, workspace_size, &beta, input_tensor.descriptor,
		    input_tensor.d_grad));
	}
}
void convolution_layer::UpdateWeights(float learning_rate) {
	int num_weights = f.NumValues();
	int num_biases  = bias.NumValues();
	{  // Update weights
		int threads = 256;
		int blocks  = (num_weights - 1) / threads + 1;
		adam_update_parameters_kernel<<<blocks, threads>>>(num_weights, f.d_data, d_adam_s, d_adam_r, f.d_grad,
		                                                   adam_beta_1, adam_beta_2, ADAM_T_VAR, learning_rate);
	}
	{  // Update biases
		int threads = 256;
		int blocks  = (num_biases - 1) / threads + 1;
		adam_update_parameters_kernel<<<blocks, threads>>>(num_biases, bias.d_data, d_adam_s + num_weights,
		                                                   d_adam_r + num_weights, bias.d_grad, adam_beta_1,
		                                                   adam_beta_2, ADAM_T_VAR, learning_rate);
	}
}

/////////////////////////////////////////////////////////////////////////////
// Pooling layer
/////////////////////////////////////////////////////////////////////////////
void pool_layer::init() {
	checkCudnnErr(cudnnCreatePoolingDescriptor(&pooling_descriptor));
	checkCudnnErr(
	    cudnnSetPooling2dDescriptor(pooling_descriptor, CUDNN_POOLING_MAX, CUDNN_PROPAGATE_NAN, 2, 2, 0, 0, 2, 2));
}
void pool_layer::Forward(tensor &input_tensor, tensor &output_tensor) {
	checkCudnnErr(cudnnPoolingForward(cudnn_handle, pooling_descriptor, &alpha, input_tensor.descriptor,
	                                  input_tensor.d_data, &beta, output_tensor.descriptor, output_tensor.d_data));
}
void pool_layer::Backward(tensor &input_tensor, tensor &output_tensor) {
	if(input_tensor.has_gradient) {
		checkCudnnErr(cudnnPoolingBackward(cudnn_handle, pooling_descriptor, &alpha, output_tensor.descriptor,
		                                   output_tensor.d_data, output_tensor.descriptor, output_tensor.d_grad,
		                                   input_tensor.descriptor, input_tensor.d_data, &beta, input_tensor.descriptor,
		                                   input_tensor.d_grad));
	}
}

/////////////////////////////////////////////////////////////////////////////
// Activation layer
/////////////////////////////////////////////////////////////////////////////

void activation_layer::init(cudnnActivationMode_t mode) {
	checkCudnnErr(cudnnCreateActivationDescriptor(&activation_descriptor));
	checkCudnnErr(cudnnSetActivationDescriptor(activation_descriptor, mode, CUDNN_PROPAGATE_NAN, 1.0));
}
void activation_layer::Forward(tensor &input_tensor, tensor &output_tensor) {
	checkCudnnErr(cudnnActivationForward(cudnn_handle, activation_descriptor, &alpha, input_tensor.descriptor,
	                                     input_tensor.d_data, &beta, output_tensor.descriptor, output_tensor.d_data));
}
void activation_layer::Backward(tensor &input_tensor, tensor &output_tensor) {
	if(input_tensor.has_gradient) {
		checkCudnnErr(cudnnActivationBackward(cudnn_handle, activation_descriptor, &alpha, output_tensor.descriptor,
		                                      output_tensor.d_data, output_tensor.descriptor, output_tensor.d_grad,
		                                      input_tensor.descriptor, input_tensor.d_data, &beta,
		                                      input_tensor.descriptor, input_tensor.d_grad));
	}
}

/////////////////////////////////////////////////////////////////////////////
// Fully Connected Layer
/////////////////////////////////////////////////////////////////////////////
void fc_layer::init(int _num_input_nodes, int _num_output_nodes) {
	num_input_nodes  = _num_input_nodes;
	num_output_nodes = _num_output_nodes;
	weights.Resize(1, 1, num_input_nodes, num_output_nodes);
	// float dist_size = sqrt(3.0f / float(num_input_nodes * num_output_nodes));
	float variance = 1.0f / float(num_input_nodes);
	weights.ResetRandom(variance);
	bias.Resize(1, 1, 1, num_output_nodes);
	// Bias can safely be initialized to zero
	bias.ResetZero();
	// Adam optimizer init
	int num_weights = num_input_nodes * num_output_nodes;
	int num_biases  = num_output_nodes;
	checkCudaErr(cudaMalloc(&d_adam_s, (num_weights + num_biases) * sizeof(float)));
	checkCudaErr(cudaMalloc(&d_adam_r, (num_weights + num_biases) * sizeof(float)));
	checkCudaErr(cudaMemset(d_adam_s, 0, (num_weights + num_biases) * sizeof(float)));
	checkCudaErr(cudaMemset(d_adam_r, 0, (num_weights + num_biases) * sizeof(float)));
}
void fc_layer::Forward(tensor &input_tensor, tensor &output_tensor) {
	checkCublasErr(cublasSgemm(cublas_handle, CUBLAS_OP_T, CUBLAS_OP_N,
	                           /*M*/ num_output_nodes, /*N*/ BATCH_SIZE, /*K*/ num_input_nodes, &alpha, weights.d_data,
	                           num_input_nodes, input_tensor.d_data, num_input_nodes, &beta, output_tensor.d_data,
	                           num_output_nodes));

	vector<float> w = weights.DownloadFlatData();
	vector<float> b = bias.DownloadFlatData();

	checkCublasErr(cublasSgemm(cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N, num_output_nodes, BATCH_SIZE, 1, &alpha,
	                           bias.d_data, num_output_nodes, ones_tensor.d_data, 1, &alpha, output_tensor.d_data,
	                           num_output_nodes));
}
void fc_layer::Backward(tensor &input_tensor, tensor &output_tensor) {
	// Compute derivative of error with respect to weights
	// We have dMSE/d0 and we want dMSE/dw = (dO/dw)(dMSE/dO)
	// dO/dw = i, so:
	checkCublasErr(cublasSgemm(cublas_handle, CUBLAS_OP_N, CUBLAS_OP_T,
	                           /*M*/ num_input_nodes, /*N*/ num_output_nodes, /*K*/ BATCH_SIZE, &alpha,
	                           input_tensor.d_data, num_input_nodes, output_tensor.d_grad, num_output_nodes, &beta,
	                           weights.d_grad, num_input_nodes));
	// Compute derivative of error with respect to bias
	// dMSE/dB = (dO/dB)(dMSE/dO)
	// dO/dB = 1, so:
	checkCublasErr(cublasSgemv(cublas_handle, CUBLAS_OP_N,
	                           /*M*/ num_output_nodes, /*N*/ BATCH_SIZE, &alpha, output_tensor.d_grad, num_output_nodes,
	                           ones_tensor.d_data, 1, &beta, bias.d_grad, 1));

	if(input_tensor.has_gradient) {
		// Compute derivative of error with respect to input
		// dMSE/di = (dO/di)(dMSE/dO)
		// dO/di = w, so:
		checkCublasErr(cublasSgemm(cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N,
		                           /*M*/ num_input_nodes, /*N*/ BATCH_SIZE, /*K*/ num_output_nodes, &alpha,
		                           weights.d_data, num_input_nodes, output_tensor.d_grad, num_output_nodes, &beta,
		                           input_tensor.d_grad, num_input_nodes));
	}
}
void fc_layer::UpdateWeights(float learning_rate) {
	int num_weights = num_input_nodes * num_output_nodes;
	int num_biases  = num_output_nodes;
	{  // Update weights
		int threads = 256;
		int blocks  = (num_weights - 1) / threads + 1;
		adam_update_parameters_kernel<<<blocks, threads>>>(num_weights, weights.d_data, d_adam_s, d_adam_r,
		                                                   weights.d_grad, adam_beta_1, adam_beta_2, ADAM_T_VAR,
		                                                   learning_rate);
	}
	{  // Update biases
		int threads = 256;
		int blocks  = (num_biases - 1) / threads + 1;
		adam_update_parameters_kernel<<<blocks, threads>>>(num_biases, bias.d_data, d_adam_s + num_weights,
		                                                   d_adam_r + num_weights, bias.d_grad, adam_beta_1,
		                                                   adam_beta_2, ADAM_T_VAR, learning_rate);
	}
}

/////////////////////////////////////////////////////////////////////////////
// Softmax Layer
/////////////////////////////////////////////////////////////////////////////
void softmax_layer::Forward(tensor &input_tensor, tensor &output_tensor) {
	checkCudnnErr(cudnnSoftmaxForward(cudnn_handle, CUDNN_SOFTMAX_ACCURATE, CUDNN_SOFTMAX_MODE_INSTANCE, &alpha,
	                                  input_tensor.descriptor, input_tensor.d_data, &beta, output_tensor.descriptor,
	                                  output_tensor.d_data));
}
void softmax_layer::Backward(tensor &input_tensor, tensor &output_tensor) {
	if(input_tensor.has_gradient) {
		checkCudnnErr(cudnnSoftmaxBackward(cudnn_handle, CUDNN_SOFTMAX_ACCURATE, CUDNN_SOFTMAX_MODE_INSTANCE, &alpha,
		                                   output_tensor.descriptor, output_tensor.d_data, output_tensor.descriptor,
		                                   output_tensor.d_grad, &beta, input_tensor.descriptor, input_tensor.d_grad));
	}
}

/////////////////////////////////////////////////////////////////////////////
// Dropout Layer
/////////////////////////////////////////////////////////////////////////////
dropout_layer::dropout_layer(float amount) {
	checkCudnnErr(cudnnCreateDropoutDescriptor(&descriptor));
	checkCudnnErr(cudnnDropoutGetStatesSize(cudnn_handle, &states_size));
	checkCudaErr(cudaMalloc(&states, states_size));
	checkCudnnErr(cudnnSetDropoutDescriptor(descriptor, cudnn_handle, amount, states, states_size, 0));
}
void dropout_layer::FindWorkspace(tensor &input_tensor) {
	checkCudnnErr(cudnnDropoutGetReserveSpaceSize(input_tensor.descriptor, &workspace_size));
	checkCudaErr(cudaMalloc(&workspace, workspace_size));
}
void dropout_layer::Forward(tensor &input_tensor, tensor &output_tensor) {
	checkCudnnErr(cudnnDropoutForward(cudnn_handle, descriptor, input_tensor.descriptor, input_tensor.d_data,
	                                  output_tensor.descriptor, output_tensor.d_data, workspace, workspace_size));
}
void dropout_layer::Backward(tensor &input_tensor, tensor &output_tensor) {
	if(input_tensor.has_gradient) {
		checkCudnnErr(cudnnDropoutBackward(cudnn_handle, descriptor, output_tensor.descriptor, output_tensor.d_grad,
		                                   input_tensor.descriptor, input_tensor.d_grad, workspace, workspace_size));
	}
}

/////////////////////////////////////////////////////////////////////////////
// Upsample Layer (simple nearest neighbour 2x2 upsampling)
/////////////////////////////////////////////////////////////////////////////
__global__ void upsample_kernel(const int num_output_values, const float *d_input, float *d_output, int n, int c, int h,
                                int w) {
	int thread_index = blockIdx.x * blockDim.x + threadIdx.x;
	if(thread_index >= num_output_values) return;
	int batch   = thread_index / (c * h * w);
	int channel = (thread_index % (c * h * w)) / (h * w);
	int o_y     = ((thread_index % (c * h * w)) % (h * w)) / w;
	int o_x     = ((thread_index % (c * h * w)) % (h * w)) % w;
	int i_y     = o_y / 2;
	int i_x     = o_x / 2;
	int i_w     = w / 2;
	int i_h     = h / 2;
	// Simple nearest neighbour
	d_output[batch * (c * h * w) + channel * (h * w) + o_y * w + o_x] =
	    d_input[batch * (c * i_h * i_w) + channel * (i_h * i_w) + i_y * i_w + i_x];
}
// Simple average 2x2 downsampling
__global__ void downsample_kernel(const int num_output_values, const float *d_input, float *d_output, int n, int c,
                                  int h, int w) {
	int thread_index = blockIdx.x * blockDim.x + threadIdx.x;
	if(thread_index >= num_output_values) return;
	int batch   = thread_index / (c * h * w);
	int channel = (thread_index % (c * h * w)) / (h * w);
	int o_y     = ((thread_index % (c * h * w)) % (h * w)) / w;
	int o_x     = ((thread_index % (c * h * w)) % (h * w)) % w;
	float avg   = 0.0f;
	int i_w     = w * 2;
	int i_h     = h * 2;
	for(int Y = 0; Y < 2; Y++) {
		for(int X = 0; X < 2; X++) {
			int i_y = o_y * 2 + Y;
			int i_x = o_x * 2 + X;
			avg += (1.0f / 4.0f) * d_input[batch * (c * i_h * i_w) + channel * (i_h * i_w) + i_y * i_w + i_x];
		}
	}
	d_output[batch * (c * h * w) + channel * (h * w) + o_y * w + o_x] = avg;
}

void upsample_layer::Forward(tensor &input_tensor, tensor &output_tensor) {
	int threads = 256;
	int blocks  = (output_tensor.NumValues() - 1) / threads + 1;
	upsample_kernel<<<blocks, threads>>>(output_tensor.NumValues(), input_tensor.d_data, output_tensor.d_data,
	                                     output_tensor.dim.n, output_tensor.dim.c, output_tensor.dim.h,
	                                     output_tensor.dim.w);
}
void upsample_layer::Backward(tensor &input_tensor, tensor &output_tensor) {
	if(input_tensor.has_gradient) {
		int threads = 256;
		int blocks  = (input_tensor.NumValues() - 1) / threads + 1;
		downsample_kernel<<<blocks, threads>>>(input_tensor.NumValues(), output_tensor.d_grad, input_tensor.d_grad,
		                                       input_tensor.dim.n, input_tensor.dim.c, input_tensor.dim.h,
		                                       input_tensor.dim.w);
	}
}

}  // namespace dnn