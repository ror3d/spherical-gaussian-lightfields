#pragma once

#include <fstream>
#include <vector>

#include "layers.h"

namespace chag
{

void exportDnnLayers( std::ofstream& file,
                      const std::vector<dnn::layer*>& layers,
                      const std::vector<dnn::tensor*>& layer_outputs,
                      const dnn::tensor& final_output );

bool importDnnLayers( std::ifstream& file,
                      std::vector<dnn::layer*>& layers,
                      std::vector<dnn::tensor*>& layer_outputs,
                      dnn::tensor& final_output );
}
