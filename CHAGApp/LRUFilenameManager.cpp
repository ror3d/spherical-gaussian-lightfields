#include "LRUFilenameManager.h"
#include <fstream>
#include <algorithm>
#include <utils/Log.h>

using namespace std; 

LRUFilenameManager::LRUFilenameManager(std::string database)
{
	m_databaseFilename = database; 
	readFromFile();
}


LRUFilenameManager::~LRUFilenameManager(void)
{
}

void LRUFilenameManager::addFilename(string filename)
{
	bool filenameExists = false;
	for(auto &entry : m_filenames) {
		entry.prio += 1;
		if(entry.filename == filename) {
			entry.prio = 0;
			filenameExists = true;
		}
	}
	if(!filenameExists) {
		FilenameEntry e; 
		e.filename = filename; 
		e.prio = 0; 
		m_filenames.push_back(e); 
	}
	std::sort(m_filenames.begin(), m_filenames.end()); 
}

void LRUFilenameManager::readFromFile()
{
	m_filenames.clear(); 
	ifstream is(m_databaseFilename.c_str());
	int prioCounter = 0; 
	while(is.is_open() && !is.eof()) {
		char buffer[4096]; 
		is.getline(buffer, 4096); 
		FilenameEntry e; 
		e.prio = prioCounter; 
		e.filename = string(buffer); 
		m_filenames.push_back(e); 
		prioCounter += 1; 
	}
	is.close(); 
}

void LRUFilenameManager::writeToFile()
{
	ofstream os(m_databaseFilename.c_str(), ios::trunc); 
	for (size_t i = 0u; i < m_filenames.size(); ++i) {
		os << m_filenames[i].filename;
		if(i<m_filenames.size() - 1) os << std::endl; 
	}
	os.close(); 
}

