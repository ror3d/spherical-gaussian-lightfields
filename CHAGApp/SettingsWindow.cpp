#include "SettingsWindow.h"
#include <wx/artprov.h>
#include "glm/glm.hpp"


SettingsWindow::SettingsWindow(wxFrame * parent_frame) : wxPanel(parent_frame)
{
	m_vert_sizer = new wxBoxSizer(wxVERTICAL);
	m_top_sizer = new wxBoxSizer(wxHORIZONTAL);
	m_settings_db_combobox = new wxComboBox(this, -1);
	// TODO: Get proper bitmaps and sizes for buttons!
	wxSize buttonSize = wxSize(int(0.7f * m_settings_db_combobox->GetSize().y), int(0.7f * m_settings_db_combobox->GetSize().y));
	m_save_button = new wxBitmapButton(this, -1, wxArtProvider::GetBitmap(wxART_FILE_SAVE, wxART_OTHER, buttonSize));
	m_save_button->SetToolTip("Save the currently selected database");
	m_save_button->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &SettingsWindow::OnSaveClicked, this, wxID_ANY);
	m_set_active_button = new wxBitmapButton(this, -1, wxArtProvider::GetBitmap(wxART_REDO, wxART_OTHER, buttonSize));
	m_set_active_button->SetToolTip("Make current selection the active database");
	m_set_active_button->Bind(wxEVT_COMMAND_BUTTON_CLICKED, &SettingsWindow::OnActivateClicked, this, wxID_ANY);
	m_top_sizer->Add(m_settings_db_combobox);
	m_top_sizer->Add(m_save_button);
	m_top_sizer->Add(m_set_active_button);
	m_vert_sizer->Add(m_top_sizer);
	m_prop_grid = new wxPropertyGrid(this);
	m_prop_grid->Bind(wxEVT_PG_CHANGED, &SettingsWindow::OnSettingChanged, this);
	m_vert_sizer->Add(m_prop_grid, 1, wxEXPAND);
	SetSizerAndFit(m_vert_sizer);
	Bind(wxEVT_COMMAND_COMBOBOX_SELECTED, &SettingsWindow::OnSettingsDBSelected, this, wxID_ANY);
	populateSettingsDBCombobox();
	Settings::instance().addListener(this);
};

void SettingsWindow::activeSettingChanged(const std::string &/*category_name*/, const std::string &/*setting_name*/)
{
	/* WIP: Need to do stuff here to make outside changes show up in UI
	std::string & active_guid = Settings::instance().GetCurrentActiveDatabaseGUID();
	std::string & selected_guid = GetSelectedDatabaseGUID();
	if (active_guid == selected_guid) {
		// This is a bit cumersome
		const auto & current_preset = Settings::instance().m_settingsDB.find(active_guid)->second;
		const auto cat_idx = current_preset.category_map.at(category_name);
		const auto &cat = current_preset.ordered_categories[cat_idx];
		const auto set_idx = cat.setting_map.at(setting_name);
		const auto setting = cat.ordered_settings[set_idx];
		m_prop_grid->GetProperty(setting.name)->SetValue(wxVariant(setting.val));
		m_prop_grid->Refresh();
	}
	*/
}

void SettingsWindow::populateSettingsDBCombobox()
{
	m_settings_db_combobox->Clear();
	///////////////////////////////////////////////////////////////////////
	// Loop over all loaded settings files
	///////////////////////////////////////////////////////////////////////
	for (auto & settings_file : Settings::instance().m_settingsDB)
	{
		wxString uid = settings_file.first;
		wxString ui_name = settings_file.second.ui_name;
		m_settings_db_combobox->Append(ui_name, &settings_file.second);
	}
	m_settings_db_combobox->Select(0);
}

void SettingsWindow::settingsHaveChanged()
{
	m_settings_db_combobox->SendSelectionChangedEvent(wxEVT_COMBOBOX);
}

void SettingsWindow::OnSettingsDBSelected(wxCommandEvent &/*event*/) {
	std::string selected_guid = GetSelectedDatabaseGUID();
	// Clear the grid
	m_prop_grid->Clear();
    m_sliders.clear();
    m_slider_mappings.clear();
    m_slider_inverse_mappings.clear();

	// If the selected database is not the active one, activate the "set active" button
	if (Settings::instance().GetCurrentActiveDatabaseGUID() == selected_guid) 
		m_set_active_button->Disable(); 
	else  m_set_active_button->Enable(); 

	// If the selected database has changed since last save, enable save button
	if (m_has_database_changed.count(selected_guid) && m_has_database_changed[selected_guid] == true) {
		m_save_button->Enable(); 
	}
	else m_save_button->Disable();

	// Populate grid
	auto categories = Settings::instance().categories_in_order();
	for (auto category : categories)
	{
		m_prop_grid->Append(new wxPropertyCategory(category.ui_name, category.name));
		for (auto setting : category.ordered_settings)
		{
			if (setting.val.CheckType<bool>()) {
				const bool value = Settings::instance().get<bool>(category.name, setting.name, selected_guid);
				m_prop_grid->Append(new wxBoolProperty(setting.ui_name, setting.name, value));
			}
			else if (setting.val.CheckType<int>()) {
				const int value = Settings::instance().get<int>(category.name, setting.name, selected_guid);
				m_prop_grid->Append(new wxIntProperty(setting.ui_name, setting.name, value));
			}
			else if (setting.val.CheckType<float>()) {
				const float value = Settings::instance().get<float>(category.name, setting.name, selected_guid);
				m_prop_grid->Append(new wxFloatProperty(setting.ui_name, setting.name, value));
			}
			else if (setting.val.CheckType<glm::vec3>()) {
				const glm::vec3 &value = Settings::instance().get<glm::vec3>(category.name, setting.name, selected_guid);
				auto *prop = m_prop_grid->Append(new wxPGProperty(setting.ui_name, setting.name));
				prop->AppendChild(new wxFloatProperty("X", "x", value[0]));
				prop->AppendChild(new wxFloatProperty("Y", "y", value[1]));
				prop->AppendChild(new wxFloatProperty("Z", "z", value[2]));
				prop->SetValue(prop->GenerateComposedValue());
				prop->SetExpanded(false);
			}
            else if (setting.val.CheckType<IntSlider>()) {
				const IntSlider & slider= Settings::instance().get<IntSlider>(category.name, setting.name, selected_guid);
				auto *prop = m_prop_grid->Append(new wxPGProperty(setting.ui_name, setting.name));
				prop->AppendChild(new wxIntProperty("value", "value", slider.value));
				prop->AppendChild(new wxIntProperty("min", "min", slider.min));
				prop->AppendChild(new wxIntProperty("max", "max", slider.max));
				prop->SetValue(prop->GenerateComposedValue());
				prop->SetExpanded(false);
                int x,y;
                m_prop_grid->GetSize(&x,&y);
                m_sliders.push_back(std::unique_ptr<wxSlider>(new wxSlider(this, m_sliders.size(), slider.value, slider.min, slider.max, wxPoint(0,0), wxSize(x, 50))));
	            m_sliders.back()->Bind(wxEVT_SCROLL_THUMBTRACK, &SettingsWindow::OnSliderChanged, this);
	            m_vert_sizer->Add(m_sliders.back().get());
                const std::string map_name = category.name + "/" + setting.name;
                m_slider_mappings[map_name] = m_sliders.back()->GetId();
                m_slider_inverse_mappings[m_sliders.back()->GetId()] = map_name;
            }
            else if (setting.val.CheckType<FloatSlider>()) {
				const FloatSlider & slider= Settings::instance().get<FloatSlider>(category.name, setting.name, selected_guid);
				auto *prop = m_prop_grid->Append(new wxPGProperty(setting.ui_name, setting.name));
				prop->AppendChild(new wxFloatProperty("value", "value", slider.value));
				prop->AppendChild(new wxFloatProperty("min", "min", slider.min));
				prop->AppendChild(new wxFloatProperty("max", "max", slider.max));
				prop->SetValue(prop->GenerateComposedValue());
				prop->SetExpanded(false);
                int x,y;
                m_prop_grid->GetSize(&x,&y);
                int slider_val = ((slider.value - slider.min) / (slider.max - slider.min)) * FLOAT_SLIDER_MAX;
                m_sliders.push_back(std::unique_ptr<wxSlider>(new wxSlider(this, m_sliders.size(), slider_val, 0, FLOAT_SLIDER_MAX, wxPoint(0,0), wxSize(x, 50))));
	            m_sliders.back()->Bind(wxEVT_SCROLL_THUMBTRACK, &SettingsWindow::OnSliderChanged, this);
	            m_vert_sizer->Add(m_sliders.back().get());
                const std::string map_name = category.name + "/" + setting.name;
                m_slider_mappings[map_name] = m_sliders.back()->GetId();
                m_slider_inverse_mappings[m_sliders.back()->GetId()] = map_name;
            }
			else if (setting.val.CheckType<wxColour>()) {
				const wxColour &value = Settings::instance().get<wxColour>(category.name, setting.name, selected_guid);
				m_prop_grid->Append(new wxColourProperty(setting.ui_name, setting.name, value));
			}
			else if (setting.val.CheckType<std::string>()) {
				const std::string &value = Settings::instance().get<std::string>(category.name, setting.name, selected_guid);

				// enums have the same types as strings, so first check whether the setting (which is of type
				// std::string)
				// is acually an enum and handle the different cases respectively.
				if (Settings::instance().is_enum(setting.name)) {
					wxArrayString enums;
					int index = -1;
					const auto enum_vec_ptr = Settings::instance().enum_values(setting.name);
					for (size_t i = 0; enum_vec_ptr != nullptr && i < enum_vec_ptr->size(); ++i) {
						const auto item = (*enum_vec_ptr)[i];
						enums.push_back(item.c_str());
						if (item == value) { index = i; }
					}
					if (index == -1) {
						LOG_ERROR("Line: " << __LINE__ << ", in File: " << __FILE__
							": Enum not found!.. Check your spelling!");
						index = 0;
					}
					m_prop_grid->Append(
						new wxEnumProperty(setting.ui_name, setting.name, enums, wxArrayInt(), index));
				}
				else {
					m_prop_grid->Append(new wxStringProperty(setting.ui_name, setting.name, value));
				}
			}
			else {
				LOG_ERROR("Line: " << __LINE__ << ", in File: " << __FILE__ ": Unexpected settings type!");
			}
		}
	}
	m_prop_grid->SetPropertyAttributeAll(wxPG_BOOL_USE_CHECKBOX, true);
	m_prop_grid->FitColumns(); 
	m_vert_sizer->RecalcSizes();
}

void SettingsWindow::selectAndActivateSettingsDB(const std::string & name)
{
	int item = m_settings_db_combobox->FindString(name); 
	if (item == wxNOT_FOUND) {
		LOG_ERROR("Attempted to select " << name << "but settings database was not found.");
	}
	m_settings_db_combobox->Select(item);
	// Emulate a click of the "set active" button
	wxCommandEvent button_event(wxEVT_COMMAND_BUTTON_CLICKED, m_set_active_button->GetId());
	wxPostEvent(m_set_active_button, button_event);
}

void SettingsWindow::OnSettingChanged(wxPropertyGridEvent & event) {
	const std::string &category_name = event.m_pg->GetPropertyCategory(event.GetProperty())->GetName().ToStdString();
	const std::string &setting_name = event.GetMainParent()->GetName().ToStdString();
	std::string selected_ui_name = (std::string) m_settings_db_combobox->GetStringSelection();
	std::string selected_guid = GetSelectedDatabaseGUID();
	auto main_parent = event.GetMainParent();
	m_has_database_changed[selected_guid] = true;
	m_save_button->Enable();
    if (Settings::instance().get_type(category_name, setting_name) == "float3")
    {
        float x = main_parent->GetPropertyByName("x")->GetValue().GetAny().As<float>();
        float y = main_parent->GetPropertyByName("z")->GetValue().GetAny().As<float>();
        float z = main_parent->GetPropertyByName("y")->GetValue().GetAny().As<float>();
        Settings::instance().set_setting(category_name, setting_name, wxAny(glm::vec3(x, y, z)), selected_guid);
        return;
    }
    if (Settings::instance().get_type(category_name, setting_name) == "int_slider")
    {
        int value = main_parent->GetPropertyByName("value")->GetValue().GetAny().As<int>();
        int min = main_parent->GetPropertyByName("min")->GetValue().GetAny().As<int>();
        int max = main_parent->GetPropertyByName("max")->GetValue().GetAny().As<int>();
        Settings::instance().set_setting(category_name, setting_name, wxAny(IntSlider{value, min, max}), selected_guid);
        const std::string map_name = category_name + "/" + setting_name;
        m_sliders[m_slider_mappings[map_name]]->SetValue(value);
        m_sliders[m_slider_mappings[map_name]]->SetMin(min);
        m_sliders[m_slider_mappings[map_name]]->SetMax(max);
        return;
    }
    if (Settings::instance().get_type(category_name, setting_name) == "float_slider")
    {
        float value = main_parent->GetPropertyByName("value")->GetValue().GetAny().As<float>();
        float min = main_parent->GetPropertyByName("min")->GetValue().GetAny().As<float>();
        float max = main_parent->GetPropertyByName("max")->GetValue().GetAny().As<float>();
        Settings::instance().set_setting(category_name, setting_name, wxAny(FloatSlider{value, min, max}), selected_guid);
        const std::string map_name = category_name + "/" + setting_name;
        int slider_val = ((value - min) / (max - min)) * FLOAT_SLIDER_MAX;
        m_sliders[m_slider_mappings[map_name]]->SetValue(slider_val);
        return;
    }
    // enum
	if (Settings::instance().is_enum(setting_name)) {
		auto idx = event.GetMainParent()->GetValue().GetAny().As<int>();
		auto enum_vec_ptr = Settings::instance().enum_values(setting_name);
		std::string name = enum_vec_ptr->at(idx);
		Settings::instance().set_setting(category_name, setting_name, wxAny(name), selected_guid);
		return;
	}
	Settings::instance().set_setting(category_name, setting_name, event.GetMainParent()->GetValue().GetAny(), selected_guid);
}

void SettingsWindow::OnSliderChanged(wxScrollEvent & event)
{
    const std::string map_name = m_slider_inverse_mappings[event.GetId()];
    int delim = map_name.find("/");
    const std::string category_name = map_name.substr(0,delim);
    const std::string setting_name = map_name.substr(delim+1,map_name.size());
    int slider_value = event.GetPosition();
	std::string selected_guid = GetSelectedDatabaseGUID();
    if (Settings::instance().get_type(category_name, setting_name) == "int_slider")
    {
        const IntSlider &slider = Settings::instance().get<IntSlider>(category_name, setting_name);
        Settings::instance().set_setting(category_name, setting_name, wxAny(IntSlider{slider_value, slider.min, slider.max}),
                                         selected_guid);
        m_prop_grid->GetPropertyByName(category_name)->GetPropertyByName(setting_name)->GetPropertyByName("value")->SetValue(slider_value);
    }
    if (Settings::instance().get_type(category_name, setting_name) == "float_slider")
    {

        const FloatSlider &slider = Settings::instance().get<FloatSlider>(category_name, setting_name);
        float value = (slider.max - slider.min) * slider_value / FLOAT_SLIDER_MAX + slider.min;
        Settings::instance().set_setting(category_name, setting_name, wxAny(FloatSlider{value, slider.min, slider.max}),
                                         selected_guid);
        m_prop_grid->GetPropertyByName(category_name)->GetPropertyByName(setting_name)->GetPropertyByName("value")->SetValue(value);
    }
    m_prop_grid->GetPropertyByName(category_name)->GetPropertyByName(setting_name)->RefreshEditor();
}

std::string SettingsWindow::GetSelectedDatabaseGUID()
{
	int i = m_settings_db_combobox->GetSelection();
	assert(i != -1);
	Settings::preset * database = static_cast<Settings::preset *>(m_settings_db_combobox->GetClientData(i));
	return database->name; 
}

void SettingsWindow::OnSaveClicked(wxCommandEvent &/*event*/)
{
	LOG_INFO("Save button clicked");
	Settings::instance().save_settings(GetSelectedDatabaseGUID());
	m_has_database_changed[GetSelectedDatabaseGUID()] = false; 
	m_save_button->Disable();
}

void SettingsWindow::OnActivateClicked(wxCommandEvent &/*event*/)
{
	LOG_INFO("Activate button clicked");
	Settings::instance().set_active_preset(GetSelectedDatabaseGUID());
}
