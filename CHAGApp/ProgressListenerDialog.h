#pragma once
#include <utils/ProgressListener.h>
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/gauge.h>
#include "CHAGApp.h"
#include <chrono>

class ProgressListenerDialog :
	public ProgressListener
{
private:
	static CHAGApp * m_app; 
	struct progress_counter {
		std::string task_name; 
		wxStaticText * label; 
		wxGauge * gauge; 
		std::chrono::time_point<std::chrono::system_clock> time_when_started; 
		std::chrono::time_point<std::chrono::system_clock> last_updated;
	};
	std::vector<progress_counter> m_progress_counters;
	wxDialog * m_dialog = nullptr; 
	wxBoxSizer * m_sizer = nullptr; 
	wxBoxSizer * m_outer_sizer = nullptr; 
	wxButton * m_exit_button = nullptr; 
public:
	static void activate(CHAGApp * app);
	virtual void setRange(int value);
	virtual void update(int progress);
	virtual void push_task(const std::string & new_task_name, bool indeterminate = false);
	virtual void pop_task();
	virtual bool isBusy() { return m_progress_counters.size() > 0; };
	void OnExitButton(wxCommandEvent & event);
};

