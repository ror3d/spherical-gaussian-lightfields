#pragma once
#include <iostream>
#include <string>
#include <vector>

class LRUFilenameManager
{
private: 
	struct FilenameEntry 
	{
		int prio; 
		std::string filename; 	
		bool operator< (const FilenameEntry &e1) const {
			return prio < e1.prio; 
		}
	};
	std::vector<FilenameEntry> m_filenames; 
	std::string m_databaseFilename; 
	std::string m_selectedFilename; 

 
	void readFromFile(); 
public:
	std::vector<std::string> getFilenames() {
		std::vector<std::string> filenames; 
		for(auto e : m_filenames) {
			filenames.push_back(e.filename); 
		}
		return filenames; 
	}
	void addFilename(std::string filename); 
	void writeToFile();
	LRUFilenameManager(std::string databaseFilename);
	~LRUFilenameManager(void);
};

