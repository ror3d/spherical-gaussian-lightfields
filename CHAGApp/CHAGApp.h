#pragma once
#include <wx/wxprec.h>
#include "wxGLPane.h"
#include <iostream>
#include <wx/propgrid/propgrid.h>
#include <wx/notebook.h>
#include <wx/treectrl.h>
#include <wx/splitter.h>
#include <scene/Scene.h>
#include <map>
#include "LRUFilenameManager.h"
#include <wx/propgrid/advprops.h>
#include <wx/dataview.h>
#include "Settings.h"
#include <wx/aui/aui.h>
#include <wx/config.h>
#include <wx/fileconf.h>
#include "SettingsWindow.h"
#include <wx/cmdline.h>
#include <chrono>

class CHAGApp;
class wxTabbedPanel;

inline wxColour toWxColour(const glm::vec4 &color) { return wxColour(color.x * 255, color.y * 255, color.z * 255, color.a * 255); }
inline glm::vec4 toFloat4Color(const wxColour &color) {
	return glm::vec4(color.Red() / 255.0f, color.Green() / 255.0f, color.Blue() / 255.0f, color.Alpha() / 255.f );
}
inline wxColour toWxColour(const glm::vec3 &color) { return wxColour(color.x * 255, color.y * 255, color.z * 255); }
inline glm::vec3 toFloat3Color(const wxColour &color)
{
	return glm::vec3(color.Red() / 255.0f, color.Green() / 255.0f, color.Blue() / 255.0f);
}

class MyFrame : public wxFrame
{
 public:
	MyFrame(const wxString &title, const wxPoint &pos, const wxSize &size, CHAGApp *chagApp);
	~MyFrame();

	void setupWindowMenu(); 

 private:
	CHAGApp *m_CHAGApp;
	void OnImportOBJ(wxCommandEvent &event);
	void OnTakeScreenshot(wxCommandEvent &event);
	void OnReloadShaders(wxCommandEvent &event);
	void OnExit(wxCommandEvent &event);
	void OnAbout(wxCommandEvent &event);
	void OnSave(wxCommandEvent &event);
	void OnSaveAs(wxCommandEvent &event);
	void OnOpen(wxCommandEvent &event);
	void OnMenuSelection(wxCommandEvent &event);
	void OnLoadPerspective(wxCommandEvent &/*event*/) { loadWindowLayout(); };
	void OnSavePerspective(wxCommandEvent &/*event*/) { saveWindowLayout(); };
	void OnShowPane(wxCommandEvent &event);
	void OnShowProfilerView(wxCommandEvent &event);

 public:
	void loadWindowLayout();
	void saveWindowLayout();

 public:
	wxAuiManager m_mgr;

	wxDECLARE_EVENT_TABLE();
};

static const wxCmdLineEntryDesc g_cmdLineDesc[] = {
	{ wxCMD_LINE_PARAM, NULL, NULL, "scene file", wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL },
    {wxCMD_LINE_SWITCH, "h", "help", "displays help on the command line parameters", wxCMD_LINE_VAL_NONE,
     wxCMD_LINE_OPTION_HELP},
    {wxCMD_LINE_OPTION, "b", "batchmode", "give the batch mode as a number (0 is not batch mode)",
     wxCMD_LINE_VAL_NUMBER, 0},
    {wxCMD_LINE_OPTION, "s", "select_setting", "select witch settings file is used when program starts",
     wxCMD_LINE_VAL_STRING, 0},
    {wxCMD_LINE_OPTION, "m", "modify_settings",
     "give a string on the form \"name = val; name = val; ...\" to modify settings", wxCMD_LINE_VAL_STRING, 0},
    {wxCMD_LINE_NONE, nullptr, nullptr, nullptr, wxCMD_LINE_VAL_NONE, 0}};

class CHAGApp : public wxApp
{
public: 
	MyFrame *m_frame;

protected:
	wxGLPane *m_glPane;
	wxTabbedPanel* m_renderPanel;

	wxTreeCtrl *m_sceneTreeCtrl;
	std::map<wxTreeItemId, scene::Item *> m_sceneItems;
	std::map<wxTreeItemId, scene::Material *> m_sceneMaterials;

	wxTreeItemId m_lightRootID, m_cameraRootID, m_modelRootID, m_shadowGeneratorRootID;

	std::chrono::time_point<std::chrono::high_resolution_clock> m_lastFrameTime;
	std::chrono::duration<float> m_frameDt;
	//////////////////////////////////////////////////////////////////////////////
	// Batch mode is ignored by the CHAGApp itself, other than that it can be set
	// from the command line, but the application can choose to interpret it as it
	// pleases
	//////////////////////////////////////////////////////////////////////////////
	long m_batch_mode = 0; 

	//////////////////////////////////////////////////////////////////////////////
	// Preselected settings database from command line
	//////////////////////////////////////////////////////////////////////////////
	std::string m_command_line_selected_settings_database = "default"; 

	virtual void initializeSceneTreeCtrl();
	virtual void OnSceneTreeCtrlSelectionChanged(wxTreeEvent &event);
	virtual void OnSceneTreeCtrlMenu(wxTreeEvent &event);
	virtual void OnScenePropertyGridChanged(wxPropertyGridEvent &event);

 public:
	virtual void OnInitCmdLine(wxCmdLineParser & parser) override;
	virtual bool OnCmdLineParsed(wxCmdLineParser & parser) override;

	virtual void onApplicationCreateMenuBar(wxMenuBar * /*menuBar*/){}
	virtual void onUnhandledMenuCommand(wxCommandEvent & /*event*/){}

	wxPropertyGrid *m_sceneItemPropertyGrid;

	SettingsWindow * m_settings_window; 

	glm::ivec2 getGlSize() const;

 public:
	CHAGApp() : m_settings_listener(this) {}
	virtual ~CHAGApp(void);

	void syncConsoleWithStdout();

	std::string m_initialFilename;
	LRUFilenameManager *m_LRUFilenameManager;
	scene::Scene *m_scene;

	void save(std::string filename);
	void load(std::string filename);
	void takeScreenshot(std::string filename);

	virtual bool OnInit() override;

	void startFrame();

    class SettingsListener : public Settings::Listener
    {
		CHAGApp * m_chagApp; 

	public: 
		SettingsListener(CHAGApp * chagapp) { m_chagApp = chagapp; }
		virtual void activeSettingChanged(const std::string &categoryName, const std::string &settingName) override
		{
			m_chagApp->settingChanged(categoryName, settingName);
		}
	} m_settings_listener;

	scene::Item * CloneItem(scene::Item * item, const std::string & new_name);

	void firstFrame();
	void settingChanged(const std::string &categoryName, const std::string &settingName);
	virtual void onGlContextCreated(){}
	virtual void onGlContextDestroy() {}
	virtual void onFirstFrame(){}
	virtual void onDisplay() = 0;
	virtual void onReshape(int /*w*/, int /*h*/){}
	virtual void onSettingChanged(const std::string &/*categoryName*/, const std::string &/*settingName*/) {}
	virtual void onSceneChanged(){}
	virtual void onViewChanged(){}
	virtual void onReloadShaders(){}
	virtual void onMouse(int /*x*/, int /*y*/, bool /*leftIsDown*/, bool /*middleIsDown*/, bool /*rightIsDown*/){}
    virtual void leftMouseDown(int /*x*/, int /*y*/) {}
    virtual void leftMouseUp(int /*x*/, int /*y*/) {}
    virtual void middleMouseDown(int /*x*/, int /*y*/) {}
    virtual void middleMouseUp(int /*x*/, int /*y*/) {}
    virtual void rightMouseDown(int /*x*/, int /*y*/) {}
    virtual void rightMouseUp(int /*x*/, int /*y*/) {}
    virtual void mouseWheel(int /*x*/, int /*y*/, float wheel_rotation) {}
	virtual bool onKeyPressed( char /*key*/, bool /*shift_down*/, bool /*ctrl_down*/, bool /*alt_down*/ ) { return false; }
	virtual bool applicationLoadsScene() { return true; }
	virtual std::string applicationWindowText() { return std::string("CHAG Application"); }

	virtual std::vector<std::string> getRendererTabNames() const { return {}; }
	virtual void onRenderTabChanged( const std::string& new_tab ) {}
	std::string getCurrentRenderTab() const;
	void setCurrentRenderTab(const std::string&);

	inline float getFrameDt() const { return m_frameDt.count(); }

	std::ifstream m_stdout_in;
	wxTextCtrl *m_console;

	bool m_show_profiler_view; 

	wxDataViewCtrl *m_performance_view_ctrl;
	wxDataViewTextRenderer *m_text_renderer_0;
	wxDataViewTextRenderer *m_text_renderer_1;
	wxDataViewTextRenderer *m_text_renderer_2;
#ifdef CHAG_ENABLE_CUDA
	wxDataViewTextRenderer *m_text_renderer_3;
#endif
	wxDataViewToggleRenderer *m_toggle_renderer;

public: 
	wxConfig *m_config;
	void OnClearWxConfig(wxCommandEvent &/*event*/) { m_config->DeleteAll(); }
};

enum
{
	ID_ImportOBJ          = 1,
	ID_ReloadShaders      = 2,
	ID_ClearWxConfig      = 4,
	ID_AnimationPlay      = 5,
	ID_AnimationRewind    = 6,
	ID_AnimationBenchmark = 7,
	ID_AnimationRecord    = 8,
	ID_LoadWindowLayout    = 9,
	ID_SaveWindowLayout    = 10,
	ID_ShowPane0          = 11,
	ID_ShowPane20         = 31,
	ID_ShowProfilerView   = 32,
	ID_TakeScreenshot     = 33
};
