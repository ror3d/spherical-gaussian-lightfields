#include "wxTabbedPanel.h"
#include "CHAGApp.h"
#include "utils/Log.h"

using namespace std; 
using namespace scene; 

wxTabbedPanel::wxTabbedPanel( wxFrame * parent, const std::vector<std::string>& tabNames )
	: wxPanel( parent )
	, m_tabNames( tabNames )
{
	init();
}

wxTabbedPanel::~wxTabbedPanel()
{
	if ( m_content )
	{
		delete m_content;
		m_content = nullptr;
	}
}

void wxTabbedPanel::SetContent( wxWindow * content )
{
	if ( m_content != nullptr )
	{
		GetSizer()->Remove( 0 );
	}

	m_content = content;
	m_content->Reparent( this );

	init();
}

void wxTabbedPanel::SetTabChangedHandler( std::function<void( const std::string& )> handler )
{
	m_tabChangedHandler = handler;
}

std::string wxTabbedPanel::GetCurrentTab() const
{
	if ( m_tabNames.empty() )
	{
		return "";
	}

	return m_tabNames[m_currentTab];
}

void wxTabbedPanel::SetCurrentTab( const std::string & tabName )
{
	for ( size_t i = 0; i < m_tabNames.size(); ++i )
	{
		if ( tabName == m_tabNames[i] )
		{
			SetCurrentTab( i );
			return;
		}
	}
	LOG_WARNING( "Tab name '" + tabName + "' not recognized." );
}

void wxTabbedPanel::SetCurrentTab( size_t idx )
{
	DebugAssert( idx >= 0 && idx < m_tabNames.size() );

	if ( GetSizer()->GetItemCount() > 1 )
	{
		GetSizer()->Remove( 1 );
	}

	m_currentTab = idx;

	auto tabs = initButtons();
	if ( tabs )
	{
		GetSizer()->Add( tabs );
	}
	GetSizer()->Layout();

	if ( m_tabChangedHandler )
	{
		m_tabChangedHandler( GetCurrentTab() );
	}
}

void wxTabbedPanel::init()
{
	auto sz = new wxBoxSizer( wxVERTICAL );
	if ( m_content )
	{
		sz->Add( m_content, wxSizerFlags( 1 ).Expand() );
	}
	auto tabs = initButtons();
	if ( tabs )
	{
		sz->Add( tabs );
	}

	SetSizerAndFit( sz );
}

wxBoxSizer* wxTabbedPanel::initButtons()
{
	if ( m_tabNames.empty() )
	{
		return nullptr;
	}

	auto tabs = new wxBoxSizer( wxHORIZONTAL );

	for ( size_t i = 0; i < m_tabButtons.size(); ++i )
	{
		m_tabButtons[i]->Destroy();
	}
	m_tabButtons.clear();

	size_t i = 0;
	for ( const auto& n : m_tabNames )
	{
		auto btn = new wxButton( this, i, n );
		btn->SetBackgroundColour( wxColour(200, 200, 200) );
		btn->Bind( wxEVT_BUTTON, &wxTabbedPanel::onTabChanged, this, i );
		tabs->Add( btn );
		m_tabButtons.push_back( btn );
		btn->SetBackgroundColour( wxNullColour );
		++i;
	}

	m_tabButtons[m_currentTab]->SetBackgroundColour( wxColour(200, 200, 250, 255) );
	return tabs;
}

void wxTabbedPanel::onTabChanged( wxCommandEvent & e )
{
	SetCurrentTab( e.GetId() );
}
