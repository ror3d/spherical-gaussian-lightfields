#pragma once

#include <string>
#include <functional>
#include <GL/glew.h>
#include "wx/wx.h"
#include "wx/glcanvas.h"
#include <utils/opengl_helpers.h>
#include <glm/glm.hpp>

#define CHAGAPP_DISPLAY_TIMER_MS 100

class CHAGApp; 

class wxGLPane : public wxGLCanvas
{
    wxGLContext*	m_context;
	CHAGApp *m_chagApp; 
	bool m_firstFrame; 

public:
	uint32_t m_default_framebuffer;

	wxGLPane (CHAGApp *chagApp, wxFrame* parent);
	virtual ~wxGLPane();
	void onIdle(wxIdleEvent &/*event*/) { Refresh(); }
	void reshape(wxSizeEvent &evt);
	double m_time_of_last_frame; 
	void display(wxPaintEvent &evt); 
	void takeScreenshot(const std::string &filename);
	int m_oldMouseX; 
	int m_oldMouseY; 
	float m_mouseSensitivity; 
	float m_forwardVelocity; 
	float m_strafeVelocity; 
	float m_ascentVelocity; 
	bool m_shiftDown; 
	glm::ivec2 windowToFramebufferCoords(const glm::ivec2 & window_coords);
	void leftMouseDown(wxMouseEvent &event); 
	void leftMouseUp(wxMouseEvent &event); 
    void middleMouseDown(wxMouseEvent &event);
    void middleMouseUp(wxMouseEvent &event);
    void rightMouseDown(wxMouseEvent &event);
    void rightMouseUp(wxMouseEvent &event);
    void mouseWheel(wxMouseEvent &event);
	void mouseMoved(wxMouseEvent &event);
	void keyPressed(wxKeyEvent &event); 
	void keyReleased(wxKeyEvent &event); 
	void charInput(wxKeyEvent &event); 

	DECLARE_EVENT_TABLE()
};
