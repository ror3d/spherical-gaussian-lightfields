#include "wxGLPane.h"
#include "CHAGApp.h"
#include <iostream>
#include <sstream>
#include <stdio.h>
#include "utils/Log.h"
#include "utils/glm_extensions.h"
#include "PerformanceProfiler.h"
#include "utils/ProfilerView.h"
#include "utils/ProgressListener.h"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#if defined(CHAG_USE_IMGUI)
#include "imgui/imgui.h"
#include "imgui/imgui_impl_opengl3.h"
#include "imgui/imgui_impl_wx.h"
#endif

using namespace std; 
using namespace scene; 

///////////////////////////////////////////////////////////////////////////////
// The callback for OpenGL errors. 
///////////////////////////////////////////////////////////////////////////////
#ifndef APIENTRY
#define APIENTRY
#endif
void APIENTRY debug_callback(GLenum source, GLenum type, GLuint /*id*/,
	GLenum severity, GLsizei /*length*/, const GLchar * message, const void * /*userParam*/)
{
	// (We should have a user setting for what one wants to see)
	if (severity == GL_DEBUG_SEVERITY_NOTIFICATION) return; 
	if (type == GL_DEBUG_TYPE_PERFORMANCE) return; 

	string source_str;
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:				source_str = "API";				break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:		source_str = "Window System";	break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:	source_str = "Shader Compiler"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:		source_str = "Third Party";		break;
	case GL_DEBUG_SOURCE_APPLICATION:		source_str = "Application";		break;
	case GL_DEBUG_SOURCE_OTHER:				source_str = "Other";			break;
	default: source_str = "(ERROR: Unknown Source)";
	};
	string severity_str; 
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:		 severity_str = "High";			break;
	case GL_DEBUG_SEVERITY_MEDIUM:		 severity_str = "Medium";		break;
	case GL_DEBUG_SEVERITY_LOW:			 severity_str = "Low";			break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: severity_str = "Notification"; break;
	default: source_str = "(ERROR: Unknown severity)";
	};
	string type_str;
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:				LOG_ERROR("(ERROR) Source: " << source_str << ", Severity: " << severity_str << ":"); break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:	LOG_WARNING("(DEPRECATED BEHAVIOUR) Source: " << source_str << ", Severity: " << severity_str << ":"); break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:	LOG_WARNING("(UNDEFINED BEHAVIOUR) Source: " << source_str << ", Severity: " << severity_str << ":"); break;
	case GL_DEBUG_TYPE_PORTABILITY:			LOG_WARNING("(PORTABILITY) Source: " << source_str << ", Severity: " << severity_str << ":"); break;
	case GL_DEBUG_TYPE_PERFORMANCE:			LOG_WARNING("(PERFORMANCE) Source: " << source_str << ", Severity: " << severity_str << ":"); break;
	case GL_DEBUG_TYPE_OTHER:				LOG_WARNING("(OTHER) Source: " << source_str << ", Severity: " << severity_str << ":"); break;
	default:								LOG_WARNING("(Unknown Type) Source: " << source_str << ", Severity: " << severity_str << ":"); break;
	}
	if ( type == GL_DEBUG_TYPE_ERROR )
	{
		LOG_ERROR( message );
#ifdef _WIN32
		static bool break_on_error = true;
		if ( IsDebuggerPresent() && break_on_error )
		{
			DebugBreak();
		}
#endif
	}
	else
	{
		LOG_INFO( message );
	}
}

wxGLPane::wxGLPane(CHAGApp *chagApp, wxFrame* parent)
	: wxGLCanvas(parent, wxID_ANY, NULL, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE | wxWANTS_CHARS)
	, m_chagApp(chagApp), m_default_framebuffer(0)
{
	wxGLContextAttrs attribs; 
	attribs.PlatformDefaults().OGLVersion(4, 5).CompatibilityProfile().DebugCtx().EndList();
	m_context = new wxGLContext(this, NULL, &attribs);
	if (!m_context->IsOK()) {
		LOG_ERROR("Failed to create OpenGL 4.5 debug compatibility profile.");
		delete m_context;
		m_context = nullptr;
		std::exit(-1);
	}
	m_firstFrame = true; 
	m_mouseSensitivity = 1.0f; 
	m_forwardVelocity = 0.0f; 
	m_strafeVelocity = 0.0f; 
	m_ascentVelocity = 0.0f; 
	// To avoid flashing on MSW
	SetBackgroundStyle(wxBG_STYLE_CUSTOM);
}

wxGLPane::~wxGLPane()
{
#ifdef CHAG_USE_IMGUI
	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplWxWidgets_Shutdown();
	ImGui::DestroyContext();
#endif

	m_chagApp->onGlContextDestroy();
	delete m_context;
}

BEGIN_EVENT_TABLE(wxGLPane, wxGLCanvas)
	EVT_MOTION(wxGLPane::mouseMoved)
	EVT_LEFT_DOWN(wxGLPane::leftMouseDown)
	EVT_LEFT_UP(wxGLPane::leftMouseUp)
    EVT_MIDDLE_DOWN(wxGLPane::middleMouseDown)
    EVT_MIDDLE_UP(wxGLPane::middleMouseUp)
    EVT_RIGHT_DOWN(wxGLPane::rightMouseDown)
    EVT_RIGHT_UP(wxGLPane::rightMouseUp)
    EVT_MOUSEWHEEL(wxGLPane::mouseWheel)
    EVT_KEY_DOWN(wxGLPane::keyPressed)
	EVT_KEY_UP(wxGLPane::keyReleased)
	EVT_IDLE(wxGLPane::onIdle)
	EVT_SIZE(wxGLPane::reshape)
	EVT_PAINT(wxGLPane::display)
	EVT_CHAR(wxGLPane::charInput)
END_EVENT_TABLE()

void wxGLPane::reshape(wxSizeEvent& evt)
{
	Refresh();
	// Ensure that the Application knows the initial size of the window at firstFrame, 
	// but don't run the actual reshape function until firstFrame() has run.
	// Also, only run when the size actually changed. 
	if(m_firstFrame) return; 
	// Resize the default framebuffer unless locked
	if (!Settings::instance().get<bool>("framebuffer", "locked_resolution")) {
		opengl_helpers::framebuffer::resizeFramebuffer(m_default_framebuffer, evt.GetSize().x, evt.GetSize().y);
		m_chagApp->onReshape(evt.GetSize().x, evt.GetSize().y);
	}
}

void wxGLPane::display(wxPaintEvent &/*evt*/)
{	
	///////////////////////////////////////////////////////////////////////////
	// This is necessary to make sure no rendering is attempted while we are
	// busy building something. So... be careful with your pushing and popping
	// of progressbars. 
	///////////////////////////////////////////////////////////////////////////
	if (g_progress->isBusy()) return;

	// Sanity check that at least makes sure display() is not called while display() is going. 
	static bool s_display_started = false; 
	if (s_display_started) {
		LOG_ERROR("DISPLAY LOOP STARTED BEFORE IT FINISHED! FREAK OUT!");
		return; 
	}
	s_display_started = true; 

	if(!IsShown()) return;
	wxGLCanvas::SetCurrent(*m_context);
	wxPaintDC(this); // only to be used in paint events. use wxClientDC 
					 // to paint outside the paint event

	m_chagApp->syncConsoleWithStdout();

	///////////////////////////////////////////////////////////////////////////
	// Set up stuff I tend to assume to be GL default but isn't 
	///////////////////////////////////////////////////////////////////////////
	glEnable(GL_DEPTH_TEST); 

	///////////////////////////////////////////////////////////////////////////
	// For now, assume we get our full expected fps for motion. Will meassuare
	// later. 
	///////////////////////////////////////////////////////////////////////////
	if(m_chagApp->m_scene != NULL) {
		float dt = CHAGAPP_DISPLAY_TIMER_MS; 
		float boost = m_shiftDown ? 0.4f : 0.1f; 
		float sceneUnitMultiplier = m_chagApp->m_scene->m_sceneUnitMultiplier * 0.01; 
		chag::orientation *camera = &m_chagApp->m_scene->getCurrentControllableItem()->m_transform; 
		camera->pos += boost * sceneUnitMultiplier * dt * m_forwardVelocity * camera->R[2];
		camera->pos += boost * sceneUnitMultiplier * dt * m_strafeVelocity  * camera->R[0];
		camera->pos += boost * sceneUnitMultiplier * dt * m_ascentVelocity  * camera->R[1];
		if(m_forwardVelocity != 0.0f || m_strafeVelocity != 0.0f || m_ascentVelocity != 0.0f){
			m_chagApp->onViewChanged(); 
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// Call initialization stuff to be done on very first frame 
	///////////////////////////////////////////////////////////////////////////

	if (m_firstFrame)
	{
#if defined(CHAG_USE_IMGUI)
		// Setup Dear ImGui context
		IMGUI_CHECKVERSION();
		ImGui::CreateContext();

		// Setup Dear ImGui style
		ImGui::StyleColorsDark();
		//ImGui::StyleColorsClassic();

		// Setup Platform/Renderer bindings
		ImGui_ImplWxWidgets_InitForOpenGL(this, m_context);
		const char* glsl_version = "#version 130";
		ImGui_ImplOpenGL3_Init( glsl_version );
#endif

		m_chagApp->firstFrame();
		glDebugMessageCallback(debug_callback, NULL);
#ifdef _DEBUG
		// This ensures that the debug callback is called immediately from the 
		// offending function. VERY useful when debugging, but turning off in 
		// release for performance reasons. 
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif
		m_firstFrame = false; 
	}

	// Start the Dear ImGui frame

#ifdef CHAG_USE_IMGUI
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplWxWidgets_NewFrame( this );
	ImGui::NewFrame();
#endif

	m_chagApp->startFrame();

	///////////////////////////////////////////////////////////////////////////
	// Get current windows and default framebuffer width and height (may or 
	// may not be the same). 
	///////////////////////////////////////////////////////////////////////////
	int window_width = GetSize().x;
	int window_height = GetSize().y;
	int buffer_width = opengl_helpers::framebuffer::getFramebufferDefaultWidth(m_default_framebuffer);
	int buffer_height = opengl_helpers::framebuffer::getFramebufferDefaultHeight(m_default_framebuffer);

	///////////////////////////////////////////////////////////////////////////
	// Render frame to off-screen framebuffer, then copy to window (with 
	// scaling).
	///////////////////////////////////////////////////////////////////////////
	if (m_chagApp->m_scene != NULL || !m_chagApp->applicationLoadsScene()) {
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		{ // Application renders to default framebuffer object
			glViewport(0, 0, buffer_width, buffer_height);
			opengl_helpers::framebuffer::pushFramebuffer(m_default_framebuffer);
			m_chagApp->onDisplay();
			opengl_helpers::framebuffer::popFramebuffer();
		}
		{   ///////////////////////////////////////////////////////////////////
			// Scale default framebuffer to current window size. If aspect 
			// ratios differ, render with black border
			///////////////////////////////////////////////////////////////////
			glPushAttrib(GL_ALL_ATTRIB_BITS);
			glViewport(0, 0, window_width, window_height);
			glColor3f(1.0f, 1.0f, 1.0f);
			glDisable(GL_LIGHTING);
			glEnable(GL_TEXTURE_2D);
			glDisable(GL_DEPTH_TEST);
			opengl_helpers::framebuffer::bindFramebufferColorBuffers(m_default_framebuffer, 0);
			float winAR = window_width / float(window_height);
			float bufAR = buffer_width / float(buffer_height);
			float extraHeight = (bufAR > winAR) ? (1.0f - (winAR / bufAR)) / 2.0f : 0.0f;
			float extraWidth = (bufAR > winAR) ? 0.0f : (1.0f - (bufAR / winAR)) / 2.0f;
			const auto ortho = glm::make_ortho2d(0.0f, 1.0f, 0.0f, 1.0f);
			glMatrixLoadfEXT(GL_PROJECTION, glm::value_ptr(ortho));
			glMatrixLoadIdentityEXT(GL_MODELVIEW);
			glBegin(GL_QUADS);
			glTexCoord2f(0, 0); glVertex2f(0 + extraWidth, 0 + extraHeight);
			glTexCoord2f(1, 0); glVertex2f(1 - extraWidth, 0 + extraHeight);
			glTexCoord2f(1, 1); glVertex2f(1 - extraWidth, 1 - extraHeight);
			glTexCoord2f(0, 1); glVertex2f(0 + extraWidth, 1 - extraHeight);
			glEnd();
			glPopAttrib();

			///////////////////////////////////////////////////////////////////
			// Optionally render profiler view on top
			///////////////////////////////////////////////////////////////////
			if (m_chagApp->m_show_profiler_view) {
				profilerView(window_width, window_height);
			}
		}

		PerformanceProfiler::instance().finishFrame(); 
	}

#if defined(CHAG_USE_IMGUI)
	if ( ImGui::IsRenderEnabled() )
	{
		ImGui::Render();
		ImGuiIO& io = ImGui::GetIO();
		glViewport( 0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y );
		ImGui_ImplOpenGL3_RenderDrawData( ImGui::GetDrawData() );
	}
	else
	{
		ImGui::EndFrame();
	}
#endif


	SwapBuffers();
	s_display_started = false;
}

void wxGLPane::keyPressed(wxKeyEvent &event)
{
#if defined(CHAG_USE_IMGUI)
	ImGui_ImplWxWidgets_ProcessEvent(&event);
	if ( ImGui::GetIO().WantCaptureKeyboard )
	{
		event.Skip();
		return;
	}
#endif

	if ( m_firstFrame )
	{
		event.Skip();
		return;
	}

	m_shiftDown = event.ShiftDown();
	bool used = false;
	used = m_chagApp->onKeyPressed( event.GetKeyCode(), event.ShiftDown(), event.ControlDown(), event.AltDown() );

	if ( !used )
	{
		switch ( event.GetKeyCode() )
		{
			case 'S': m_forwardVelocity = 1.0f; used = true; break;
			case 'W': m_forwardVelocity = -1.0f; used = true; break;
			case 'D': m_strafeVelocity = 1.0f; used = true; break;
			case 'A': m_strafeVelocity = -1.0f; used = true; break;
			case 'E': m_ascentVelocity = 1.0f; used = true; break;
			case 'Q': m_ascentVelocity = -1.0f; used = true; break;
		}
	}

	if ( !used )
	{
		event.Skip();
	}
}

void wxGLPane::keyReleased(wxKeyEvent &event)
{
#if defined(CHAG_USE_IMGUI)
	ImGui_ImplWxWidgets_ProcessEvent(&event);
	if ( ImGui::GetIO().WantCaptureKeyboard )
	{
		event.Skip();
		return;
	}
#endif

	m_shiftDown = event.ShiftDown();
	bool used = false;
	switch(event.GetKeyCode()) {
		case 'S': m_forwardVelocity = 0.0f; used = true; break;
		case 'W': m_forwardVelocity = 0.0f; used = true; break;
		case 'D': m_strafeVelocity	= 0.0f; used = true; break;
		case 'A': m_strafeVelocity	= 0.0f; used = true; break;
		case 'E': m_ascentVelocity	= 0.0f; used = true; break;
		case 'Q': m_ascentVelocity	= 0.0f; used = true; break;
	}

	if ( !used )
	{
		event.Skip();
	}
}

void wxGLPane::charInput( wxKeyEvent & event )
{
	ImGui_ImplWxWidgets_ProcessEvent(&event);
	if ( !ImGui::GetIO().WantCaptureMouse )
	{
		event.Skip();
	}
}


void wxGLPane::mouseMoved(wxMouseEvent &event)
{
#if defined(CHAG_USE_IMGUI)
	ImGui_ImplWxWidgets_ProcessEvent(&event);
	if ( ImGui::GetIO().WantCaptureMouse ) return;
#endif

	if(m_firstFrame) return; 

	glm::ivec2 fb_coords = windowToFramebufferCoords(glm::ivec2(event.GetX(), event.GetY()));
	m_chagApp->onMouse(fb_coords.x, fb_coords.y, event.LeftIsDown(), event.MiddleIsDown(), event.RightIsDown()); 

	if(m_chagApp->m_scene == NULL) return; 
	float delta_x = m_mouseSensitivity * 0.1f * (m_oldMouseX - event.GetX());
	float delta_y = m_mouseSensitivity * 0.1f * (m_oldMouseY - event.GetY());		

	chag::orientation &transform = m_chagApp->m_scene->getCurrentControllableItem()->m_transform;

	if(event.LeftIsDown() && event.RightIsDown()) { // Zoom
		transform.pos -= delta_y * transform.R[2]; 
		m_chagApp->onViewChanged(); 
	}
	else if(event.LeftIsDown()) {
		transform.pitch(delta_y * 0.05f);
		transform.yaw(delta_x * 0.05f);
		m_chagApp->onViewChanged(); 
	}
	else if(event.RightIsDown()) {
		transform.roll(delta_x * 0.05f);
		m_chagApp->onViewChanged(); 
	}
	else if(event.MiddleIsDown()){
		float sceneUnitMultiplier = m_chagApp->m_scene->m_sceneUnitMultiplier * 0.01; 
		transform.pos -= (delta_y * sceneUnitMultiplier) * transform.R[1];
		transform.pos += (delta_x * sceneUnitMultiplier) * transform.R[0];
		m_chagApp->onViewChanged(); 
	}
	m_oldMouseX = event.GetX();
	m_oldMouseY = event.GetY(); 
}

void wxGLPane::leftMouseDown(wxMouseEvent &event)
{
#if defined(CHAG_USE_IMGUI)
	ImGui_ImplWxWidgets_ProcessEvent(&event);
	if ( ImGui::GetIO().WantCaptureMouse ) return;
#endif

	SetFocus(); 
	m_oldMouseX = event.GetX(); 
	m_oldMouseY = event.GetY(); 
	glm::ivec2 fb_coords = windowToFramebufferCoords(glm::ivec2(event.GetX(), event.GetY()));
	m_chagApp->onMouse(fb_coords.x, fb_coords.y, event.LeftIsDown(), event.MiddleIsDown(), event.RightIsDown());
	m_chagApp->leftMouseDown(fb_coords.x, fb_coords.y);
}

void wxGLPane::leftMouseUp(wxMouseEvent &event)
{
#if defined(CHAG_USE_IMGUI)
	ImGui_ImplWxWidgets_ProcessEvent(&event);
	if ( ImGui::GetIO().WantCaptureMouse ) return;
#endif

	SetFocus(); 
	m_oldMouseX = event.GetX(); 
	m_oldMouseY = event.GetY(); 
	glm::ivec2 fb_coords = windowToFramebufferCoords(glm::ivec2(event.GetX(), event.GetY()));
	m_chagApp->onMouse(fb_coords.x, fb_coords.y, event.LeftIsDown(), event.MiddleIsDown(), event.RightIsDown());
	m_chagApp->leftMouseUp(fb_coords.x, fb_coords.y);
}

void wxGLPane::middleMouseDown(wxMouseEvent &event)
{
#if defined(CHAG_USE_IMGUI)
	ImGui_ImplWxWidgets_ProcessEvent(&event);
	if ( ImGui::GetIO().WantCaptureMouse ) return;
#endif

    SetFocus();
    m_oldMouseX = event.GetX();
    m_oldMouseY = event.GetY();
    glm::ivec2 fb_coords = windowToFramebufferCoords(glm::ivec2(event.GetX(), event.GetY()));
    m_chagApp->onMouse(fb_coords.x, fb_coords.y, event.LeftIsDown(), event.MiddleIsDown(), event.RightIsDown());
    m_chagApp->middleMouseDown(fb_coords.x, fb_coords.y);
}

void wxGLPane::middleMouseUp(wxMouseEvent &event)
{
#if defined(CHAG_USE_IMGUI)
	ImGui_ImplWxWidgets_ProcessEvent(&event);
	if ( ImGui::GetIO().WantCaptureMouse ) return;
#endif

    SetFocus();
    m_oldMouseX = event.GetX();
    m_oldMouseY = event.GetY();
    glm::ivec2 fb_coords = windowToFramebufferCoords(glm::ivec2(event.GetX(), event.GetY()));
    m_chagApp->onMouse(fb_coords.x, fb_coords.y, event.LeftIsDown(), event.MiddleIsDown(), event.RightIsDown());
    m_chagApp->middleMouseUp(fb_coords.x, fb_coords.y);
}

void wxGLPane::rightMouseDown(wxMouseEvent &event)
{
#if defined(CHAG_USE_IMGUI)
	ImGui_ImplWxWidgets_ProcessEvent(&event);
	if ( ImGui::GetIO().WantCaptureMouse ) return;
#endif

    SetFocus();
    m_oldMouseX = event.GetX();
    m_oldMouseY = event.GetY();
    glm::ivec2 fb_coords = windowToFramebufferCoords(glm::ivec2(event.GetX(), event.GetY()));
    m_chagApp->onMouse(fb_coords.x, fb_coords.y, event.LeftIsDown(), event.MiddleIsDown(), event.RightIsDown());
    m_chagApp->rightMouseDown(fb_coords.x, fb_coords.y);
}

void wxGLPane::rightMouseUp(wxMouseEvent &event)
{
#if defined(CHAG_USE_IMGUI)
	ImGui_ImplWxWidgets_ProcessEvent(&event);
	if ( ImGui::GetIO().WantCaptureMouse ) return;
#endif

    SetFocus();
    m_oldMouseX = event.GetX();
    m_oldMouseY = event.GetY();
    glm::ivec2 fb_coords = windowToFramebufferCoords(glm::ivec2(event.GetX(), event.GetY()));
    m_chagApp->onMouse(fb_coords.x, fb_coords.y, event.LeftIsDown(), event.MiddleIsDown(), event.RightIsDown());
    m_chagApp->rightMouseUp(fb_coords.x, fb_coords.y);
}

void wxGLPane::mouseWheel(wxMouseEvent &event)
{
#if defined(CHAG_USE_IMGUI)
	ImGui_ImplWxWidgets_ProcessEvent(&event);
	if ( ImGui::GetIO().WantCaptureMouse ) return;
#endif

    SetFocus();
    m_oldMouseX = event.GetX();
    m_oldMouseY = event.GetY();
    glm::ivec2 fb_coords = windowToFramebufferCoords(glm::ivec2(event.GetX(), event.GetY()));
    m_chagApp->onMouse(fb_coords.x, fb_coords.y, event.LeftIsDown(), event.MiddleIsDown(), event.RightIsDown());
	m_chagApp->mouseWheel(fb_coords.x, fb_coords.y, event.GetWheelRotation());
}

glm::ivec2 wxGLPane::windowToFramebufferCoords(const glm::ivec2 & window_coords)
{
	int window_width = GetSize().x;
	int window_height = GetSize().y;
	int buffer_width = opengl_helpers::framebuffer::getFramebufferDefaultWidth(m_default_framebuffer);
	int buffer_height = opengl_helpers::framebuffer::getFramebufferDefaultHeight(m_default_framebuffer);
	float winAR = window_width / float(window_height);
	float bufAR = buffer_width / float(buffer_height);
	float borderHeight = ((bufAR > winAR) ? (1.0f - (winAR / bufAR)) / 2.0f : 0.0f) * window_height;
	float borderWidth = ((bufAR > winAR) ? 0.0f : (1.0f - (bufAR / winAR)) / 2.0f) * window_width;
	glm::vec2 scaled_fb_size = glm::vec2(window_width - 2.0f * borderWidth, window_height - 2.0f * borderHeight);
	glm::vec2 fb_scale = glm::vec2(buffer_width, buffer_height) / scaled_fb_size;
	glm::vec2 offset_window_coords = glm::vec2(window_coords.x - borderWidth, (window_height - window_coords.y - 1) - borderHeight);
	glm::vec2 fb_coords = offset_window_coords * fb_scale;
	return glm::ivec2(fb_coords);
}

void wxGLPane::takeScreenshot(const std::string &screenshot)
{
	opengl_helpers::framebuffer::pushFramebuffer(m_default_framebuffer);
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	int bufWidth = opengl_helpers::framebuffer::getFramebufferDefaultWidth(m_default_framebuffer);
	int bufHeight = opengl_helpers::framebuffer::getFramebufferDefaultHeight(m_default_framebuffer);
	wxImage image; 
	image.Create(bufWidth, bufHeight, false);
	glReadPixels(0, 0, bufWidth, bufHeight, GL_RGB, GL_UNSIGNED_BYTE, image.GetData());
	image.Mirror(false).SaveFile(screenshot);
	opengl_helpers::framebuffer::popFramebuffer();
}

