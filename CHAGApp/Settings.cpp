#include "Settings.h"

#include <algorithm>
#include <iostream>
#include <sstream>

#include "wx/dir.h"
#include "glm/glm.hpp"

/*
// Need manual conversion to/from wxColour to glm::vec3.
// TODO(dolonius): This could probably be moved to the get<T>()-function above..
template <>
glm::vec3 Settings::get<glm::vec3>(const std::string &category_name, const std::string &setting_name) {
	const auto the_setting = find_setting(category_name, setting_name);
	if (the_setting == nullptr) {
		LOG_ERROR("Line: " << __LINE__ << ", in File: " << __FILE__ << ": No such setting exist!");
		return glm::vec3();
	}
	if (the_setting->CheckType<wxColour>()) {
		LOG_WARNING("Line: " << __LINE__ << ", in File: " << __FILE__ << ": The type requested differs from type stored! (glm::vec3 -> wxColour)");
		const auto &val = the_setting->As<wxColour>();
		return chag::make_vector(static_cast<float>(val.Red()) / 255.f, static_cast<float>(val.Green()) / 255.f, static_cast<float>(val.Blue()) / 255.f);
	}
	return the_setting->As<glm::vec3>();
}

template <>
wxColour Settings::get<wxColour>(const std::string &category_name, const std::string &setting_name) {
	const auto the_setting = find_setting(category_name, setting_name);
	if (the_setting == nullptr) {
		LOG_ERROR("Line: " << __LINE__ << ", in File: " << __FILE__ << ": No such setting exist!");
		return wxColour();
	}
	if (the_setting->CheckType<glm::vec3>()) {
		LOG_WARNING("Line: " << __LINE__ << ", in File: " << __FILE__ << ": The type requested differs from type stored! (wxColour -> glm::vec3)");
		const auto &val = the_setting->As<glm::vec3>();
		return wxColour(static_cast<unsigned char>(val[0] * 255u), static_cast<unsigned char>(val[1] * 255u), static_cast<unsigned char>(val[2] * 255u));
	}
	return the_setting->As<wxColour>();
}
*/


Settings::Settings() : m_current_active_preset(nullptr) {
  wxDir dir(wxGetCwd());
  if (dir.Open(dir.GetNameWithSep() + "Settings")) {
    wxArrayString files;
    dir.GetAllFiles(dir.GetName(), &files, "*.xml");

	bool first_preset = true; 

    for (const auto &file : files) {
      // Read the settings file.
      tinyxml2::XMLDocument doc;
      doc.LoadFile(file.ToStdString().c_str());

      // Find different settings tabs.
      auto root = doc.FirstChildElement("root");
      const std::string root_guid(root->Attribute("guid"));
      const std::string root_ui_name(root->Attribute("ui_name"));
      add_preset(root_guid, preset(root_ui_name, root_guid));
	  /////////////////////////////////////////////////////////////////////////
	  // Set first read file as active preset. This should be saved in registry 
	  // later on 
	  /////////////////////////////////////////////////////////////////////////
	  if (first_preset) {
		  set_active_preset(root_guid, false);
		  first_preset = false;
	  }
      for (auto tab = root->FirstChildElement(); tab != nullptr; tab = tab->NextSiblingElement()) {
        const std::string category_name(tab->Name());
        auto current_category = category(tab->Attribute("ui_name"), category_name);
        add_category(category_name, current_category, root_guid);
        for (auto setting_node = tab->FirstChildElement(); setting_node != nullptr; setting_node = setting_node->NextSiblingElement()) {
          const std::string setting_name(setting_node->Name());
          const std::string ui_name(setting_node->Attribute("ui_name"));
          const std::string setting_type(setting_node->Attribute("type"));
          if (setting_type == "bool") {
            bool val;
            setting_node->QueryBoolText(&val);
            Settings::add_setting(category_name, setting_name, setting(wxAny(val), ui_name, setting_name), root_guid);
          } else if (setting_type == "int") {
            int val;
            setting_node->QueryIntText(&val);
            Settings::add_setting(category_name, setting_name, setting(wxAny(val), ui_name, setting_name), root_guid);
          } else if (setting_type == "float") {
            float val;
            setting_node->QueryFloatText(&val);
            Settings::add_setting(category_name, setting_name, setting(wxAny(val), ui_name, setting_name), root_guid);
          } else if (setting_type == "float3") {
            glm::vec3 val;
            setting_node->FirstChildElement("x")->QueryFloatText(&val[0]);
            setting_node->FirstChildElement("y")->QueryFloatText(&val[1]);
            setting_node->FirstChildElement("z")->QueryFloatText(&val[2]);
            Settings::add_setting(category_name, setting_name, setting(wxAny(val), ui_name, setting_name), root_guid);
          } else if (setting_type == "int_slider") {
            IntSlider slider;
            setting_node->FirstChildElement("value")->QueryIntText(&slider.value);
            setting_node->FirstChildElement("min")->QueryIntText(&slider.min);
            setting_node->FirstChildElement("max")->QueryIntText(&slider.max);
            Settings::add_setting(category_name, setting_name, setting(wxAny(slider), ui_name, setting_name), root_guid);
          } else if (setting_type == "float_slider") {
            FloatSlider slider;
            setting_node->FirstChildElement("value")->QueryFloatText(&slider.value);
            setting_node->FirstChildElement("min")->QueryFloatText(&slider.min);
            setting_node->FirstChildElement("max")->QueryFloatText(&slider.max);
            Settings::add_setting(category_name, setting_name, setting(wxAny(slider), ui_name, setting_name), root_guid);
          } else if (setting_type == "color") {
            Settings::add_setting(category_name, setting_name, setting(wxAny(wxColour(wxString(setting_node->GetText()))), ui_name, setting_name), root_guid);
          } else if (setting_type == "string") {
            Settings::add_setting(category_name, setting_name, setting(wxAny(std::string(setting_node->GetText())), ui_name, setting_name), root_guid);
          } else if (setting_type == "enum") {
            Settings::add_setting(category_name, setting_name, setting(wxAny(std::string(setting_node->GetText())), ui_name, setting_name), root_guid);
          } else {
            LOG_WARNING("Line: " << __LINE__ << ", in File: " << __FILE__ << ": Undefined variable type "
                                 << "\" " << setting_type << "\"! Skipping...");
            continue;
          }
        }
		/////////////////////////////////////////////////////////////////////////////
		// The outputted errors are not very helpful. The TinyXML documentation 
		// talks about a GetErrorDesc() function, but we don't seem to have that...? 
		/////////////////////////////////////////////////////////////////////////////
		if(doc.Error()) { 
			if(doc.GetErrorStr1()) LOG_ERROR(doc.GetErrorStr1());
			if(doc.GetErrorStr2()) LOG_ERROR(doc.GetErrorStr2());
		}
      }

      // And add enums to database as well.
      auto additional = doc.FirstChildElement("additional");
      for (auto item = additional->FirstChildElement(); item != nullptr; item = item->NextSiblingElement()) {
        const auto key  = std::string(item->Name());
        auto enum_items = m_enums.find(key);
        if (enum_items == m_enums.end()) {
          auto &new_enums = m_enums[key];
          for (auto value = item->FirstChildElement(); value != nullptr; value = value->NextSiblingElement()) {
            new_enums.push_back(std::string(value->GetText()));
          }
        }
      }
    }
  }
}

void Settings::addListener(Listener * listener)
{
	m_listeners.emplace(listener);
}

void Settings::save_settings(const std::string &preset_guid) {
  wxDir dir(wxGetCwd());
  if (!dir.Open(dir.GetNameWithSep() + "Settings")) {
    dir.Make(dir.GetNameWithSep() + "Settings");
    dir.Open(dir.GetNameWithSep() + "Settings");
  }

  if (m_settingsDB.count(preset_guid) == 0) {
	  LOG_ERROR("Attempting to save non-existing database");
  }
  preset & current_preset = m_settingsDB[preset_guid];

  tinyxml2::XMLDocument doc;
  doc.InsertFirstChild(doc.NewDeclaration());
  auto root_elem  = doc.NewElement("root");
  root_elem->SetAttribute("ui_name", current_preset.ui_name.c_str());
  // Set the guid to the filename to make sure of uniqueness.
  root_elem->SetAttribute("guid", preset_guid.c_str());
  auto root       = doc.InsertEndChild(root_elem);
  auto additional = doc.InsertEndChild(doc.NewElement("additional"));

  

  for (auto &category_item : current_preset.ordered_categories) {
    auto category_elem = doc.NewElement(category_item.name.c_str());
    category_elem->SetAttribute("ui_name", category_item.ui_name.c_str());
    root->InsertEndChild(category_elem);

    // Loop over all settings and add them to the property grid.
    for (auto &setting_item : category_item.ordered_settings) {
      auto setting_elem = doc.NewElement(setting_item.name.c_str());
      if (setting_item.val.CheckType<bool>()) {
        setting_elem->SetAttribute("type", "bool");
        setting_elem->SetText(setting_item.val.As<bool>());
      } else if (setting_item.val.CheckType<int>()) {
        setting_elem->SetAttribute("type", "int");
        setting_elem->SetText(setting_item.val.As<int>());
      } else if (setting_item.val.CheckType<float>()) {
        setting_elem->SetAttribute("type", "float");
        setting_elem->SetText(setting_item.val.As<float>());
      } else if (setting_item.val.CheckType<wxColour>()) {
        setting_elem->SetAttribute("type", "color");
        setting_elem->SetText(setting_item.val.As<wxColour>().GetAsString(wxC2S_HTML_SYNTAX).ToUTF8());
      }
      else if (setting_item.val.CheckType<glm::vec3>())
      {
          setting_elem->SetAttribute("type", "float3");
          auto val = setting_item.val.As<glm::vec3>();
          auto x = doc.NewElement("x");
          auto y = doc.NewElement("y");
          auto z = doc.NewElement("z");

          x->SetText(val[0]);
          y->SetText(val[1]);
          z->SetText(val[2]);
          setting_elem->InsertEndChild(x);
          setting_elem->InsertEndChild(y);
          setting_elem->InsertEndChild(z);
      }
      else if (setting_item.val.CheckType<IntSlider>())
      {
          setting_elem->SetAttribute("type", "int_slider");
          auto slider = setting_item.val.As<IntSlider>();
          auto value = doc.NewElement("value");
          auto min = doc.NewElement("min");
          auto max = doc.NewElement("max");
          value->SetText(slider.value);
          min->SetText(slider.min);
          max->SetText(slider.max);
          setting_elem->InsertEndChild(value);
          setting_elem->InsertEndChild(min);
          setting_elem->InsertEndChild(max);
      }
      else if (setting_item.val.CheckType<FloatSlider>())
      {
          setting_elem->SetAttribute("type", "float_slider");
          auto slider = setting_item.val.As<FloatSlider>();
          auto value = doc.NewElement("value");
          auto min = doc.NewElement("min");
          auto max = doc.NewElement("max");
          value->SetText(slider.value);
          min->SetText(slider.min);
          max->SetText(slider.max);
          setting_elem->InsertEndChild(value);
          setting_elem->InsertEndChild(min);
          setting_elem->InsertEndChild(max);
      }
      else if (setting_item.val.CheckType<std::string>())
      {
          if (is_enum(setting_item.name))
          {
              setting_elem->SetAttribute("type", "enum");
              auto enum_node = doc.NewElement(setting_item.name.c_str());
              for (auto &enum_it : m_enums[setting_item.name])
              {
                  auto enum_elem = doc.NewElement("value");
                  enum_elem->SetText(enum_it.c_str());
                  enum_node->InsertEndChild(enum_elem);
              }
              additional->InsertEndChild(enum_node);
          } else {
          setting_elem->SetAttribute("type", "string");
        }
        setting_elem->SetText(setting_item.val.As<std::string>().c_str());
      }
      else
      {
          LOG_ERROR("Line: " << __LINE__ << ", in File: " << __FILE__ << ": Unknown setting type!");
          continue;
      }
      setting_elem->SetAttribute("ui_name", setting_item.ui_name.c_str());
      category_elem->InsertEndChild(setting_elem);
    }
  }
  doc.SaveFile((dir.GetNameWithSep().ToStdString() + preset_guid + ".xml").c_str());
}

void Settings::add_preset(const std::string &preset_name, const preset &new_preset) {
  if (m_settingsDB.emplace(preset_name, new_preset).second == false) {
    LOG_WARNING("Line: " << __LINE__ << ", in File: " << __FILE__ << ": Preset already added!");
  }
}

void Settings::add_category(const std::string &category_name, const category &new_category, 
							const std::string &preset_guid) 
{
	assert(m_settingsDB.count(preset_guid) == 1); 
	preset & current_preset = m_settingsDB[preset_guid];
    index idx = static_cast<index>(current_preset.ordered_categories.size());
	assert(current_preset.category_map.count(category_name) == 0);
	current_preset.category_map[category_name] = idx;
    current_preset.ordered_categories.emplace_back(new_category);
}

void Settings::add_setting(	const std::string &category_name, const std::string &setting_name, 
							const setting &new_setting, const std::string & preset_guid) 
{
	assert(m_settingsDB.count(preset_guid) == 1);
	preset & current_preset = m_settingsDB[preset_guid];
	assert(current_preset.category_map.count(category_name) == 1);
	const int & category_item = current_preset.category_map[category_name];
	category & settings = current_preset.ordered_categories[category_item];
    index idx = static_cast<index>(settings.ordered_settings.size());
	assert(settings.setting_map.count(setting_name) == 0);
	settings.setting_map[setting_name] = idx; 
	settings.ordered_settings.emplace_back(new_setting);
}

void Settings::set_setting(	const std::string &category_name, const std::string &setting_name, 
							const wxAny &val, const std::string & preset_guid) 
{
	assert(m_settingsDB.count(preset_guid) == 1);
	preset & current_preset = m_settingsDB[preset_guid];
	assert(current_preset.category_map.count(category_name) == 1);
	const int & category_item = current_preset.category_map[category_name];
	category & category = current_preset.ordered_categories[category_item];
	assert(category.setting_map.count(setting_name) == 1);
	const auto &setting_item = category.setting_map.find(setting_name);
	auto &prev_setting = category.ordered_settings[setting_item->second];
	if (prev_setting.val.HasSameType(val)) {
		prev_setting.val = val;
		settingChanged(category_name, setting_name, preset_guid);
		return;
	}
	LOG_ERROR("Line: " << __LINE__ << ", in File: " << __FILE__ << ": Setting types does not match!");
	return;
}

std::string Settings::get_type(const std::string & category_name, const std::string & setting_name)
{
	std::string preset_guid = GetCurrentActiveDatabaseGUID(); 
	wxAny current_value = * find_setting(category_name, setting_name, preset_guid); 
	if (current_value.CheckType<bool>()) return "bool";
	else if (current_value.CheckType<std::string>()) return "string";
	else if (current_value.CheckType<int>()) return "int";
	else if (current_value.CheckType<float>()) return "float";
	else if (current_value.CheckType<glm::vec3>()) return "float3";
	else if (current_value.CheckType<IntSlider>()) return "int_slider";
	else if (current_value.CheckType<FloatSlider>()) return "float_slider";
	else if (current_value.CheckType<wxColour>()) return "color";
	else {
		LOG_ERROR("Setting has unknown type. Freaky.");
		return "unknown";
	}
}

void Settings::set_setting(const std::string &category_name, const std::string &setting_name, const wxAny &val)
{
	set_setting(category_name, setting_name, val, GetCurrentActiveDatabaseGUID());
}

void Settings::settingChanged(const std::string &category_name, const std::string &setting_name, const std::string & preset_guid)
{
	assert(m_settingsDB.count(preset_guid) == 1);
	for (auto listener : m_listeners) {
		if (m_current_active_preset == &m_settingsDB[preset_guid])
			listener->activeSettingChanged(category_name, setting_name);
		else 
			listener->inactiveSettingChanged(category_name, setting_name, preset_guid);
	}
}

const wxAny *Settings::find_setting(const std::string &category_name, const std::string &setting_name, const std::string & preset_guid) {
	try {
		assert(m_settingsDB.count(preset_guid) == 1);
		const preset & current_preset = m_settingsDB.find(preset_guid)->second;
		const auto cat_idx = current_preset.category_map.at(category_name);
		const auto &cat = current_preset.ordered_categories[cat_idx];
		const auto set_idx = cat.setting_map.at(setting_name);
		return &cat.ordered_settings[set_idx].val;
	}
	catch (const std::out_of_range &oor) {
		LOG_ERROR("Line: " << __LINE__ << ", in File: " << __FILE__ << ": " << oor.what());
	}
	return nullptr;
}

void Settings::set_active_preset(settingPreset key, bool inform_listeners) 
{
	///////////////////////////////////////////////////////////////////////////
	// Collect all values in the previous database, so we can know which 
	// ones actually change. 
	// Note: There _should_ be a prettier way of doing this.
	///////////////////////////////////////////////////////////////////////////
	struct Setting { std::string category_name; std::string setting_name; wxVariant value; };
	std::vector<Setting> old_settings; 
	if (inform_listeners) { 
		auto categories = categories_in_order();
		for (auto & category : categories) {
			for (auto & setting : category.ordered_settings) {
				Setting old;
				old.category_name = category.name;
				old.setting_name = setting.name;
				find_setting(category.name, setting.name, m_current_active_preset->name)->GetAs(&old.value);
				old_settings.push_back(old);
			}
		}
	}

	// Change current database 
	m_current_active_preset = &m_settingsDB.at(key);

	// Inform listeners of all settings that have (actually) changed 
	if (inform_listeners) {
		for (auto & setting : old_settings)
		{
			wxVariant new_value;
			const wxAny & new_wxAny_value = *find_setting(setting.category_name, setting.setting_name, key);
			new_wxAny_value.GetAs(&new_value);
			if (new_value != setting.value) {
				set_setting(setting.category_name, setting.setting_name, new_wxAny_value, key);
			}
		}
	}
}

const std::vector<Settings::category> & Settings::categories_in_order() const {
	assert(m_current_active_preset != nullptr);
    return m_current_active_preset->ordered_categories;
}

const Settings::enumValues *Settings::enum_values(const std::string &key) const {
  try {
    return &m_enums.at(key);
  } catch (const std::out_of_range &oor) {
    LOG_ERROR("Line: " << __LINE__ << ", in File: " << __FILE__ << ": " << oor.what());
  }
  return nullptr;
}
