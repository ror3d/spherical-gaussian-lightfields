// dear imgui: Platform Binding for WxWidgets
// This needs to be used along with a Renderer (e.g. DirectX11, OpenGL3, Vulkan..)

#include "imgui.h"
#include "imgui_impl_wx.h"

#include <wx/wx.h>
#include <wx/clipbrd.h>


// Data
static wxWindow*      g_Window = NULL;
static wxLongLong     g_Time = 0;
static bool           g_MousePressed[3] = { false, false, false };
static wxStockCursor  g_MouseCursors[ImGuiMouseCursor_COUNT] = { wxStockCursor::wxCURSOR_ARROW };
static std::string    g_ClipboardTextData;

static const char* ImGui_ImplWxWidgets_GetClipboardText(void*)
{
	g_ClipboardTextData.clear();

	if ( wxTheClipboard->Open() )
	{
		if ( wxTheClipboard->IsSupported( wxDF_TEXT ) )
		{
			wxTextDataObject data;
			wxTheClipboard->GetData( data );
			g_ClipboardTextData = data.GetText();
		}
		wxTheClipboard->Close();
	}
    return g_ClipboardTextData.c_str();
}

static void ImGui_ImplWxWidgets_SetClipboardText(void*, const char* text)
{
	if ( wxTheClipboard->Open() )
	{
		wxTheClipboard->SetData( new wxTextDataObject( "Some text" ) );
		wxTheClipboard->Close();
	}
}

// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
bool ImGui_ImplWxWidgets_ProcessEvent(const wxEvent* evt)
{
    ImGuiIO& io = ImGui::GetIO();
	if ( const wxMouseEvent* mevt = dynamic_cast<const wxMouseEvent*>(evt) )
	{
		if ( mevt->GetEventType() == wxEVT_MOUSEWHEEL )
		{
			if ( mevt->GetWheelAxis() == wxMOUSE_WHEEL_HORIZONTAL )
			{
				if ( mevt->GetWheelRotation() > 0 )
				{
					io.MouseWheelH += 1;
				}
				else if ( mevt->GetWheelRotation() < 0 )
				{
					io.MouseWheelH -= 1;
				}
			}
			else if ( mevt->GetWheelAxis() == wxMOUSE_WHEEL_VERTICAL )
			{
				if ( mevt->GetWheelRotation() > 0 )
				{
					io.MouseWheel += 1;
				}
				else if ( mevt->GetWheelRotation() < 0 )
				{
					io.MouseWheel -= 1;
				}
			}
		}
		if ( mevt->GetEventType() == wxEVT_BUTTON )
		{
			if ( mevt->LeftDown() )
			{
				g_MousePressed[0] = true;
			}
			if ( mevt->RightDown() )
			{
				g_MousePressed[1] = true;
			}
			if ( mevt->MiddleDown() )
			{
				g_MousePressed[2] = true;
			}
		}
	}
	else if ( const wxKeyEvent* kevt = dynamic_cast<const wxKeyEvent*>(evt) )
	{
		if ( kevt->GetEventType() == wxEVT_CHAR )
		{
			wxChar chrs[2] = { 0 };
			chrs[0] = kevt->GetUnicodeKey();
			wxString str( chrs );
			io.AddInputCharactersUTF8( str.ToStdString().c_str() );
		}
		else if ( kevt->GetEventType() == wxEVT_KEY_DOWN || kevt->GetEventType() == wxEVT_KEY_UP )
		{
			int key = kevt->GetKeyCode();
			IM_ASSERT( key >= 0 && key < IM_ARRAYSIZE( io.KeysDown ) );
			io.KeysDown[key] = kevt->GetEventType() == wxEVT_KEY_DOWN;
			io.KeyAlt = kevt->AltDown();
			io.KeyCtrl = kevt->ControlDown();
			io.KeyShift = kevt->ShiftDown();
			io.KeySuper = kevt->MetaDown();
		}
	}
    return false;
}

static bool ImGui_ImplWxWidgets_Init( wxWindow* window)
{
    g_Window = window;

    // Setup back-end capabilities flags
    ImGuiIO& io = ImGui::GetIO();
    io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;       // We can honor GetMouseCursor() values (optional)
    io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;        // We can honor io.WantSetMousePos requests (optional, rarely used)
    io.BackendPlatformName = "imgui_impl_wx";

    // Keyboard mapping. ImGui will use those indices to peek into the io.KeysDown[] array.
    io.KeyMap[ImGuiKey_Tab] = WXK_TAB;
	io.KeyMap[ImGuiKey_LeftArrow] = WXK_LEFT;
    io.KeyMap[ImGuiKey_RightArrow] = WXK_RIGHT;
    io.KeyMap[ImGuiKey_UpArrow] = WXK_UP;
    io.KeyMap[ImGuiKey_DownArrow] = WXK_DOWN;
    io.KeyMap[ImGuiKey_PageUp] = WXK_PAGEUP;
    io.KeyMap[ImGuiKey_PageDown] = WXK_PAGEDOWN;
    io.KeyMap[ImGuiKey_Home] = WXK_HOME;
    io.KeyMap[ImGuiKey_End] = WXK_END;
    io.KeyMap[ImGuiKey_Insert] = WXK_INSERT;
    io.KeyMap[ImGuiKey_Delete] = WXK_DELETE;
    io.KeyMap[ImGuiKey_Backspace] = WXK_BACK;
    io.KeyMap[ImGuiKey_Space] = WXK_SPACE;
    io.KeyMap[ImGuiKey_Enter] = WXK_RETURN;
    io.KeyMap[ImGuiKey_Escape] = WXK_ESCAPE;
    io.KeyMap[ImGuiKey_A] = 'A';
    io.KeyMap[ImGuiKey_C] = 'C';
    io.KeyMap[ImGuiKey_V] = 'V';
    io.KeyMap[ImGuiKey_X] = 'X';
    io.KeyMap[ImGuiKey_Y] = 'Y';
    io.KeyMap[ImGuiKey_Z] = 'Z';

    io.SetClipboardTextFn = ImGui_ImplWxWidgets_SetClipboardText;
    io.GetClipboardTextFn = ImGui_ImplWxWidgets_GetClipboardText;
    io.ClipboardUserData = NULL;

	g_MouseCursors[ImGuiMouseCursor_Arrow] = wxStockCursor::wxCURSOR_ARROW;
    g_MouseCursors[ImGuiMouseCursor_TextInput] = wxStockCursor::wxCURSOR_IBEAM;
    g_MouseCursors[ImGuiMouseCursor_ResizeAll] = wxStockCursor::wxCURSOR_SIZING;
    g_MouseCursors[ImGuiMouseCursor_ResizeNS] = wxStockCursor::wxCURSOR_SIZENS;
    g_MouseCursors[ImGuiMouseCursor_ResizeEW] = wxStockCursor::wxCURSOR_SIZEWE;
    g_MouseCursors[ImGuiMouseCursor_ResizeNESW] = wxStockCursor::wxCURSOR_SIZENESW;
    g_MouseCursors[ImGuiMouseCursor_ResizeNWSE] = wxStockCursor::wxCURSOR_SIZENWSE;
    g_MouseCursors[ImGuiMouseCursor_Hand] = wxStockCursor::wxCURSOR_HAND;

#ifdef _WIN32
	io.ImeWindowHandle = window->GetHWND();
#else
    (void)window;
#endif

    return true;
}

bool ImGui_ImplWxWidgets_InitForOpenGL( wxWindow* window, wxGLContext* sdl_gl_context)
{
    (void)sdl_gl_context; // Viewport branch will need this.
    return ImGui_ImplWxWidgets_Init(window);
}

void ImGui_ImplWxWidgets_Shutdown()
{
    g_Window = NULL;
}

static void ImGui_ImplWxWidgets_UpdateMousePosAndButtons()
{
    ImGuiIO& io = ImGui::GetIO();

    // Set OS mouse position if requested (rarely used, only when ImGuiConfigFlags_NavEnableSetMousePos is enabled by user)
    //if (io.WantSetMousePos)
    //    SDL_WarpMouseInWindow(g_Window, (int)io.MousePos.x, (int)io.MousePos.y);
    //else
	io.MousePos = ImVec2(-FLT_MAX, -FLT_MAX);

    int mx, my;
    int sx, sy;
	wxMouseState state = wxGetMouseState();
	state.GetPosition( &mx, &my );
	g_Window->GetScreenPosition( &sx, &sy );

	if ( g_Window->HasFocus() )
	{
		io.MouseDown[0] = g_MousePressed[0] || state.LeftIsDown();
		io.MouseDown[1] = g_MousePressed[1] || state.RightIsDown();
		io.MouseDown[2] = g_MousePressed[2] || state.MiddleIsDown();
	
		io.MousePos = ImVec2( (float)mx - sx, (float)my - sy );
	}
	g_MousePressed[0] = g_MousePressed[1] = g_MousePressed[2] = false;
}

static void ImGui_ImplWxWidgets_UpdateMouseCursor()
{
    ImGuiIO& io = ImGui::GetIO();
    if (io.ConfigFlags & ImGuiConfigFlags_NoMouseCursorChange)
        return;

    ImGuiMouseCursor imgui_cursor = ImGui::GetMouseCursor();
    if (io.MouseDrawCursor || imgui_cursor == ImGuiMouseCursor_None)
    {
        // Hide OS mouse cursor if imgui is drawing it or if it wants no cursor
        wxSetCursor( wxStockCursor::wxCURSOR_NONE );
    }
    else
    {
        // Show OS mouse cursor
        wxSetCursor(g_MouseCursors[imgui_cursor] ? g_MouseCursors[imgui_cursor] : g_MouseCursors[ImGuiMouseCursor_Arrow]);
    }
}

void ImGui_ImplWxWidgets_NewFrame( wxWindow* window )
{
    ImGuiIO& io = ImGui::GetIO();
    IM_ASSERT(io.Fonts->IsBuilt() && "Font atlas not built! It is generally built by the renderer back-end. Missing call to renderer _NewFrame() function? e.g. ImGui_ImplOpenGL3_NewFrame().");

	/*
    // Setup display size (every frame to accommodate for window resizing)
    int display_w, display_h;
    SDL_GetWindowSize(window, &w, &h);
    SDL_GL_GetDrawableSize(window, &display_w, &display_h);
    if (w > 0 && h > 0)
        io.DisplayFramebufferScale = ImVec2((float)display_w / w, (float)display_h / h);
	*/
    int w, h;
	window->GetSize( &w, &h );
    io.DisplaySize = ImVec2((float)w, (float)h);

    // Setup time step (we don't use SDL_GetTicks() because it is using millisecond resolution)
	wxLongLong current_time = wxGetUTCTimeUSec();
    io.DeltaTime = g_Time > 0 ? (float)((double)(current_time.GetValue() - g_Time.GetValue()) / 1000000) : (float)(1.0f / 60.0f);
    g_Time = current_time;

    ImGui_ImplWxWidgets_UpdateMousePosAndButtons();
    ImGui_ImplWxWidgets_UpdateMouseCursor();
}
