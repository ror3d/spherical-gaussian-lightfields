#include "imconfig.h"


namespace ImGui
{
static bool s_renderEnabled = true;

bool IsRenderEnabled()
{
	return s_renderEnabled;
}

void EnableRender()
{
	s_renderEnabled = true;
}

void DisableRender()
{
	s_renderEnabled = false;
}
}
