#pragma once

#ifdef CHAG_ENABLE_CUDA
#include <cuda_runtime_api.h>
#endif
#include <string>
#include <stdint.h>
#include <map>
#include <vector>
#include <deque>
#include <utils/Log.h>
#include <utils/ScopeTimer.h>
#include <wx/dataview.h>
#include <ostream>

class PerformanceProfilerNode
{
public: // Getters and setters are for wuzzies
	bool m_active;						// Was this number changed since the last frame? 
	bool m_contains_scope;					// the GTK backend on Linux needs to know in advance whether an item can be a container
	std::string m_name;					// Name of scope
	double m_cpu_time;					// CPU time in milliseconds
	std::string m_cpu_time_str; 
	double m_opengl_time;				// OpenGL time in milliseconds;
	std::string m_opengl_time_str;
#ifdef CHAG_ENABLE_CUDA
	double m_cuda_time;					// CUDA time in milliseconds; 
	std::string m_cuda_time_str;
#endif
	PerformanceProfilerNode * m_parent;
	std::map<std::string, int> m_children_ids;
	std::vector<PerformanceProfilerNode*> m_children;
	PerformanceProfilerNode() {
		//m_meassured = true;
		m_active = false; 
		m_contains_scope = false;
		m_name = "";
		m_cpu_time = 0.0;
		m_opengl_time = 0.0;
#ifdef CHAG_ENABLE_CUDA
		m_cuda_time = 0.0;
#endif
		m_parent = NULL; 
	}
	PerformanceProfilerNode(const std::string &name, PerformanceProfilerNode *parent, bool contains_scope) : PerformanceProfilerNode() {
		m_contains_scope = contains_scope;
		m_name = name; 
		m_parent = parent; 
	}
	~PerformanceProfilerNode();
	void recursivelyFormatTimingStrings();
	void recursivelyResetTimers(); 
	double recursivelyGetScopeTime(TraceEntry::Type, const std::string & name);
	void recursivelyDumpJSON(std::ostream & os, int level = 0);
};

class PerformanceProfiler : public wxDataViewModel
{
private: 
	static PerformanceProfiler m_instance; 
	///////////////////////////////////////////////////////////////////////////
	// Interface to code
	///////////////////////////////////////////////////////////////////////////
public: // Getters and setters are for wuzzies
	PerformanceProfilerNode m_root; 
	PerformanceProfilerNode *m_current; 
	void enterScope(const std::string &name, bool contains_scope);
//	void exitScope(const ScopeTimer &timer); 
	
	void updateTraces(); 
	double getScopeTime(TraceEntry::Type type, const std::string & name) { 
		return m_root.recursivelyGetScopeTime(type, name);
	}

	PerformanceProfiler();
	~PerformanceProfiler();
	static PerformanceProfiler & instance();
	void dumpJSON(std::ostream & os); // Dump tree to JSON
	void finishFrame(); 
	///////////////////////////////////////////////////////////////////////////
	// I need the model to know about the control to see which items are 
	// selected. There is almost certainly a better way to handle multiple 
	// changes at once. 
	///////////////////////////////////////////////////////////////////////////
	wxDataViewCtrl * m_owning_control;
	///////////////////////////////////////////////////////////////////////////
	// Inherit wxDataViewModel
	///////////////////////////////////////////////////////////////////////////
	virtual unsigned int GetColumnCount() const override;
	virtual wxString GetColumnType(unsigned int col) const override;
	virtual void GetValue(wxVariant &variant, const wxDataViewItem &item, unsigned int col) const override;
	virtual bool SetValue(const wxVariant &variant, const wxDataViewItem &item, unsigned int col) override;
	virtual bool IsEnabled(const wxDataViewItem &item, unsigned int col) const override;
	virtual wxDataViewItem GetParent(const wxDataViewItem &item) const override;
	virtual bool IsContainer(const wxDataViewItem &item) const override;
	virtual bool HasContainerColumns(const wxDataViewItem &item) const override;
	virtual unsigned int GetChildren(const wxDataViewItem &parent, wxDataViewItemArray &array) const override;
};
