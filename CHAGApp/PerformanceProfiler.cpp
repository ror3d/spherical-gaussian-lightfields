#include <GL/glew.h>
#include "PerformanceProfiler.h"
#include <utils/Log.h>
#ifndef _WIN32
    #include <cfloat>
#endif

PerformanceProfiler PerformanceProfiler::m_instance; 

PerformanceProfiler::PerformanceProfiler()
{
	m_root.m_name = "Root";
	m_root.m_contains_scope = true;
	m_current = &m_root; 	
	m_owning_control = NULL; 
	ItemAdded(wxDataViewItem(0), wxDataViewItem(m_current));
}

PerformanceProfiler::~PerformanceProfiler()
{
}

PerformanceProfiler & PerformanceProfiler::instance()
{
	return m_instance;
}

///////////////////////////////////////////////////////////////////////////////
// Process all entries in all traces and build Tree to update UI
// NOTE: We are intentionally not removing items that are not in the trace. 
//       The idea is that if we, for example, ran a different algorithm before
//       we tried the current, we still want to see the results of that. 
//       Soo... we might want some kind of "clear" button. Or rethink this. 
///////////////////////////////////////////////////////////////////////////////
void PerformanceProfiler::updateTraces()
{
	std::map<std::string, Trace> &traces = GET_TRACES(); 

	for (auto trace : traces)
	{
		TraceEntry *previous_entry = nullptr;
		for (auto& entry : trace.second.trace)
		{
			// SYNC entries do not appear in list
			if (entry.m_type != TraceEntry::ID_SYNC)
			{
				if (entry.m_is_scope_start) {
					if (previous_entry != nullptr)
						previous_entry->m_contains_scope = true;
					previous_entry = &entry;
				}
				else {
					previous_entry = nullptr;
				}
			}
		}

		m_current->m_active = true; 
		enterScope(trace.first, !trace.second.trace.empty());
		// Trace will start and end with a sync
		m_current->m_cpu_time = trace.second.trace.back().m_cpu_time - trace.second.trace[0].m_cpu_time;
		m_current->m_opengl_time = trace.second.trace.back().m_opengl_time - trace.second.trace[0].m_opengl_time;
#ifdef CHAG_ENABLE_CUDA
		m_current->m_cuda_time = trace.second.trace.back().m_cuda_time - trace.second.trace[0].m_cuda_time;
#endif

		std::vector<double> start_time_stack_cpu;
		std::vector<double> start_time_stack_opengl;
#ifdef CHAG_ENABLE_CUDA
		std::vector<double> start_time_stack_cuda;
#endif
		for (auto entry : trace.second.trace)
		{
			// SYNC entries do not appear in list 
			if (entry.m_type != TraceEntry::ID_SYNC)
			{
				if (entry.m_is_scope_start) {
					enterScope(entry.m_name, entry.m_contains_scope);
					start_time_stack_cpu.push_back(entry.m_cpu_time);
					if (entry.m_type == TraceEntry::ID_GL) {
						start_time_stack_opengl.push_back(entry.m_opengl_time);
					}
#ifdef CHAG_ENABLE_CUDA
					if (entry.m_type == TraceEntry::ID_CUDA) {
						start_time_stack_cuda.push_back(entry.m_cuda_time);
					}
#endif
				}
				else {
					m_current->m_cpu_time += entry.m_cpu_time - start_time_stack_cpu.back(); 
				
					start_time_stack_cpu.pop_back();
					if (entry.m_type == TraceEntry::ID_GL) {
						m_current->m_opengl_time += entry.m_opengl_time - start_time_stack_opengl.back();
						start_time_stack_opengl.pop_back();
					}
					else {
						m_current->m_opengl_time = -FLT_MAX; 
					}
#ifdef CHAG_ENABLE_CUDA
					if (entry.m_type == TraceEntry::ID_CUDA) {
						m_current->m_cuda_time += entry.m_cuda_time - start_time_stack_cuda.back();
						start_time_stack_cuda.pop_back();
					}
					else {
						m_current->m_cuda_time = -FLT_MAX; 
					}
#endif
					m_current->m_active = true;
					ItemChanged(wxDataViewItem(m_current)); // The GTK backend on Linux won’t refresh all the children of a node, only the current element
					if (m_current->m_parent == NULL) {
						LOG_ERROR("PerformanceProfiler: exitScope but parent == NULL");
					}
					else { m_current = m_current->m_parent; }
				}
			}
		}
		ItemChanged(wxDataViewItem(m_current));
		m_current = m_current->m_parent; 
	}
}

void PerformanceProfiler::enterScope(const std::string &name, bool contains_scope)
{
	///////////////////////////////////////////////////////////////////////////
	// Check if the suggested node exists, otherwise create it
	///////////////////////////////////////////////////////////////////////////
	if(m_current->m_children_ids.count(name) == 0) {
		m_current->m_children_ids[name] = m_current->m_children.size(); 
		m_current->m_children.push_back(new PerformanceProfilerNode(name, m_current, contains_scope));
		ItemAdded(wxDataViewItem(m_current), wxDataViewItem(m_current->m_children[m_current->m_children.size() - 1]));
		///////////////////////////////////////////////////////////////////////
		// The first time a scope is entered, expand the tree to show it
		///////////////////////////////////////////////////////////////////////
		if (m_owning_control != NULL) {
			m_owning_control->ExpandAncestors(wxDataViewItem(m_current));
			m_owning_control->Expand(wxDataViewItem(m_current));
		}
	}
	m_current = m_current->m_children[m_current->m_children_ids[name]];
}

void PerformanceProfiler::dumpJSON(std::ostream & os)
{
	os << "{\n"; 
	m_root.recursivelyDumpJSON(os, 1); 
	os << "\n}\n";
}


PerformanceProfilerNode::~PerformanceProfilerNode()
{
	for ( auto child : m_children )
	{
		delete child;
	}
}

void PerformanceProfilerNode::recursivelyDumpJSON(std::ostream & os, int level)
{
	auto tab = [level](){
		std::string s; 
		for (int i = 0; i < level * 2; i++) s += " ";
		return s; 
	};
	os << tab() << "{\n";
	os << tab() << "\"name\":\"" << m_name << "\",\n";
	if (m_cpu_time != -FLT_MAX) {
		os << tab() << "\"cpu\":" << m_cpu_time << ",\n";
	}
	else { os << tab() << "\"cpu\":null,\n"; }
	if (m_opengl_time != -FLT_MAX) {
		os << tab() << "\"gl\":" << m_opengl_time << ",\n";
	}
	else { os << tab() << "\"gl\":null,\n"; }
#ifdef CHAG_ENABLE_CUDA
	if (m_cuda_time != -FLT_MAX) {
		os << tab() << "\"cuda\":" << m_cuda_time;
	}
	else { os << tab() << "\"cuda\":null"; }
#endif
	if (m_children.size() > 0) {
		os << ",\n";
		os << tab() << "\"children\": [\n";
		for (size_t i = 0u; i < m_children.size(); ++i) {
			auto c = m_children[i]; 
			c->recursivelyDumpJSON(os, level + 1); 
			if (i != m_children.size() - 1) os << ",\n";
			else os << "\n";
		}
		os << tab() << "]\n";
	}
	else {
		os << "\n";
	}
	os << tab() << "}";
}

void PerformanceProfilerNode::recursivelyFormatTimingStrings()
{
	{
		const size_t buffer_size = 16u;
		char buffer[buffer_size];
		std::string format = "%4.4f"; 
		std::snprintf(buffer, buffer_size, format.c_str(), m_cpu_time);
		m_cpu_time_str = buffer;
		if (m_opengl_time == -FLT_MAX) m_opengl_time_str = "-";
		else {
			std::snprintf(buffer, buffer_size, format.c_str(), m_opengl_time);
			m_opengl_time_str = buffer;
		}
#ifdef CHAG_ENABLE_CUDA
		if (m_cuda_time == -FLT_MAX) m_cuda_time_str = "-";
		else {
			std::snprintf(buffer, buffer_size, format.c_str(), m_cuda_time);
			m_cuda_time_str = buffer;
		}
#endif
	}
	for (auto c : m_children) c->recursivelyFormatTimingStrings(); 
}

void PerformanceProfilerNode::recursivelyResetTimers()
{
	m_cpu_time = 0.0f; 
	m_opengl_time = 0.0f; 
#ifdef CHAG_ENABLE_CUDA
	m_cuda_time = 0.0f; 
#endif
	for (auto c : m_children) c->recursivelyResetTimers(); 
	m_active = false; 
}

double PerformanceProfilerNode::recursivelyGetScopeTime(TraceEntry::Type type, const std::string & name)
{
	if (m_name == name) {
		switch (type) {
		case TraceEntry::ID_CPU: return m_cpu_time;
#ifdef CHAG_ENABLE_CUDA
		case TraceEntry::ID_CUDA: return m_cuda_time;
#endif
		case TraceEntry::ID_GL: return m_opengl_time;
		default: {
			LOG_ERROR("Attempting to return time of sync event"); 
			return -1.0f;
		}
		}
	}
	else {
		for (auto c : m_children) {
			double time = c->recursivelyGetScopeTime(type, name); 
			if (time >= 0.0f) return time; 
		}
	}
	return -1.0f; 
}

void PerformanceProfiler::finishFrame()
{
	///////////////////////////////////////////////////////////////////////////
	// Format text output and reset all timers
	///////////////////////////////////////////////////////////////////////////
	updateTraces(); 
	m_root.recursivelyFormatTimingStrings();
	m_root.recursivelyResetTimers(); 
}

///////////////////////////////////////////////////////////////////////////
// Inherit wxDataViewModel
///////////////////////////////////////////////////////////////////////////
unsigned int PerformanceProfiler::GetColumnCount() const { return 4; }
wxString PerformanceProfiler::GetColumnType(unsigned int /*col*/) const {
	// All columns are strings now...
	return wxT("string");
}
void PerformanceProfiler::GetValue(wxVariant &variant, const wxDataViewItem &item, unsigned int col) const
{
	wxASSERT(item.IsOk());
	PerformanceProfilerNode * node = (PerformanceProfilerNode *)item.GetID(); 
	switch(col) {
		case 0: variant = node->m_name; break;
		case 1: variant = node->m_opengl_time_str; break;
#ifdef CHAG_ENABLE_CUDA
		case 2: variant = node->m_cuda_time_str; break;
		case 3: variant = node->m_cpu_time_str; break;
#else
		case 2: variant = node->m_cpu_time_str; break;
#endif
		default: LOG_ERROR("PerformanceProfiler: GetValue: wrong column " << col);
	}
}
bool PerformanceProfiler::SetValue(const wxVariant &/*variant*/, const wxDataViewItem &/*item*/, unsigned int /*col*/)
{	
	LOG_ERROR("PerformanceProfiler::SetValue: called. Which is not expected anymore.");
	return true; 
}
bool PerformanceProfiler::IsEnabled(const wxDataViewItem &item, unsigned int /*col*/) const
{
	wxASSERT(item.IsOk());
	return true; 
}
wxDataViewItem PerformanceProfiler::GetParent(const wxDataViewItem &item) const
{
	// the invisible root node has no parent
	if(!item.IsOk()) return wxDataViewItem(0);
	PerformanceProfilerNode * node = (PerformanceProfilerNode *)item.GetID();
	if(node->m_parent == NULL) return wxDataViewItem(0);
	return wxDataViewItem((void*)(node->m_parent));
}
bool PerformanceProfiler::IsContainer(const wxDataViewItem &item) const
{
	// the invisble root node can have children 
	if(!item.IsOk()) return true;
	PerformanceProfilerNode * node = (PerformanceProfilerNode *)item.GetID();
	return node->m_contains_scope;
}

bool PerformanceProfiler::HasContainerColumns(const wxDataViewItem &item) const
{
	wxASSERT(item.IsOk());
	PerformanceProfilerNode * node = (PerformanceProfilerNode *)item.GetID();
	if (node->m_parent == NULL) return false; // For now. Maybe "Frame" should show a sum...
	if(node->m_children.size() > 0) return true; 
	return false;
}

unsigned int PerformanceProfiler::GetChildren(const wxDataViewItem &parent, wxDataViewItemArray &array) const
{
	if(!parent.IsOk()) {
		array.Add(wxDataViewItem((void*)&m_root));
		return 1;
	}
	PerformanceProfilerNode * node = (PerformanceProfilerNode *)parent.GetID();
	if(node->m_children.size() == 0) return 0; 

	unsigned int count = node->m_children.size(); 
	for(auto child : node->m_children) {
		array.Add(wxDataViewItem((void *)child));
	}
	return count; 
}

