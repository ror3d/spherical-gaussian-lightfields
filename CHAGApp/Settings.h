#ifndef CHAGAPP_SETTINGS_H_
#define CHAGAPP_SETTINGS_H_

#include <assert.h>

#include <map>
#include <string>
#include <unordered_set>

#include "tinyxml2.h"
#include "wx/variant.h"
#include "wx/any.h"
#include "wx/colour.h"

#include "utils/Log.h"

///////////////////////////////////////////////////////////////////////////
// New type of settings
///////////////////////////////////////////////////////////////////////////
struct IntSlider
{
    int value;
    int min;
    int max;
};
constexpr int FLOAT_SLIDER_MAX = INT_MAX / 2;
struct FloatSlider
{
    float value;
    float min;
    float max;
};

class Settings {
public:
	// Anyone should be able to be a listener to when some setting changes
	// NOTE: Most listeners (i.e. your project) won't care about inactive settings
	//       changing, as they won't be in effect until the active database is changed.
	//       But you _can_ still opt in to listen to all changes. 
	class Listener {
	public: 
		virtual void activeSettingChanged(const std::string &categoryName, const std::string &settingName) = 0;
		virtual void inactiveSettingChanged(const std::string &/*categoryName*/, const std::string &/*settingName*/, const std::string &/*preset_guid*/) 
		{ /* Normally, do nothing */}
	};
	void addListener(Listener * listener);
private:
	std::unordered_set<Listener *> m_listeners;
	void settingChanged(const std::string &categoryName, const std::string &settingName, const std::string & preset_guid);

  // Convenience.
  struct setting {
    wxAny val;
    std::string ui_name;
    std::string name;
    setting() {}
    setting(const wxAny &_val, const std::string _ui_name, const std::string _name) : val(_val), ui_name(_ui_name), name(_name) {}
  };

  using index       = unsigned int;
  using settingName = std::string;
  using SettingMap  = std::map<settingName, index>;
  struct category {
    std::string ui_name;
    std::string name;
    SettingMap setting_map;
    std::vector<setting> ordered_settings;
    category() {}
    category(const std::string _ui_name, const std::string _name) : ui_name(_ui_name), name(_name) {}
  };

  using settingCategory = std::string;
  using CategoryMap     = std::map<settingCategory, index>;
public:
	struct preset {
    std::string ui_name;
    std::string name;
    CategoryMap category_map;
    std::vector<category> ordered_categories;
    preset() {}
    preset(const std::string _ui_name, const std::string _name) : ui_name(_ui_name), name(_name) {}
  };

	using settingPreset = std::string;
  using SettingDatabase = std::map<settingPreset, preset>;
  using enumValues      = std::vector<std::string>;
  using EnumDatabase    = std::map<settingName, enumValues>;

  // Neo.
  static Settings &instance() {
    static Settings instance;
    return instance;
  }

  // Methods.
  void save_settings(const std::string &filename);
  void add_preset(const std::string &preset_name, const preset &new_preset);
  void add_category(const std::string &category_name, const category &new_category, const std::string & preset_guid);
  void add_setting(const std::string &category_name, const std::string &setting_name, const setting &new_setting, const std::string & preset_guid);
  void set_setting(const std::string &category_name, const std::string &setting_name, const wxAny &val, const std::string & preset_guid);
  void set_setting(const std::string &category_name, const std::string &setting_name, const wxAny &val);
  void set_active_preset(settingPreset key, bool inform_listeners = true);
  const std::vector<category> & categories_in_order() const;
  const enumValues *enum_values(const std::string &key) const;
  bool is_enum(const std::string &key) { return (m_enums.find(key) != m_enums.end()); }
  SettingDatabase::const_iterator settings_cbegin() const { return m_settingsDB.cbegin(); }
  SettingDatabase::const_iterator settings_cend() const { return m_settingsDB.cend(); }

  // Get setting with explicit preset named. Used by UI. 
  template <typename T>
  T get(const std::string &category_name, const std::string &setting_name, const std::string &preset_guid);
  // Get setting from currently active preset. Used by most anyone else
  template <typename T>
  T get(const std::string &category_name, const std::string &setting_name)
  {
	  assert(m_current_active_preset != nullptr);
	  return get<T>(category_name, setting_name, m_current_active_preset->name);
  }

 private:
  // Decalare and disable some ctor/dtors.
  Settings();
  Settings(Settings const &) = delete;
  void operator=(Settings const &) = delete;

  // Methods
  const wxAny *find_setting(const std::string &category_name, const std::string &setting_name, const std::string & preset_guid);

  // Variables
  preset * m_current_active_preset;

public: 
  // The databases.
  SettingDatabase m_settingsDB;
  std::string GetCurrentActiveDatabaseGUID() {
	  assert(m_current_active_preset != nullptr);
	  return m_current_active_preset->name;
  };

  std::string get_type(const std::string & category_name, const std::string & setting_name); 

private: 
  // NOTE(dolonius): For now we assume only _one_ set of enums per global setting name.
  EnumDatabase m_enums;
};

template <typename T>
T Settings::get(const std::string &category_name, const std::string &setting_name, const std::string &preset_guid) {
	const auto the_setting = find_setting(category_name, setting_name, preset_guid);
	assert(the_setting != nullptr);
	assert(the_setting->CheckType<T>());
	T retval;
	the_setting->GetAs(&retval);
	return retval;
}
/*
template <>
glm::vec3 Settings::get<glm::vec3>(const std::string &category_name, const std::string &setting_name, const std::string &preset_guid);

template <>
wxColour Settings::get<wxColour>(const std::string &category_name, const std::string &setting_name, const std::string &preset_guid);
*/

#endif  // CHAGAPP_SETTINGS_H_
