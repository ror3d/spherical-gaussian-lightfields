#pragma once

#include <string>
#include <functional>
#include "wx/wx.h"


class wxTabbedPanel : public wxPanel
{
public:
	wxTabbedPanel (wxFrame* parent, const std::vector<std::string>& tabNames);
	~wxTabbedPanel();

	void SetContent( wxWindow* content );

	void SetTabChangedHandler( std::function<void( const std::string& )> handler );

	std::string GetCurrentTab() const;

	void SetCurrentTab( const std::string& tabName );
	void SetCurrentTab( size_t idx );

private:
	wxWindow* m_content = nullptr;
	size_t m_currentTab = 0;
	std::vector<std::string> m_tabNames;
	std::vector<wxButton*> m_tabButtons;
	std::function<void( const std::string& )> m_tabChangedHandler;
	wxColour m_bgcolour;

	void init();
	wxBoxSizer* initButtons();

	void onTabChanged( wxCommandEvent & e );
};
