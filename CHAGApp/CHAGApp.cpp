#include "CHAGApp.h"
#include <fstream>
#include "PerformanceProfiler.h"
#include "ProgressListenerDialog.h"
#include <GL/freeglut.h>
#include <assert.h>
#include <scene/SceneLoader.h>
#include <sstream>
#include <stb_image.h>
#include <utils/Log.h>
#include <wx/event.h>
#include "wxTabbedPanel.h"

using namespace scene;

wxBEGIN_EVENT_TABLE( MyFrame, wxFrame )
	EVT_MENU( ID_ImportOBJ, MyFrame::OnImportOBJ )
	EVT_MENU( ID_TakeScreenshot, MyFrame::OnTakeScreenshot )
	EVT_MENU( ID_ReloadShaders, MyFrame::OnReloadShaders )
	EVT_MENU( wxID_EXIT, MyFrame::OnExit )
	EVT_MENU( wxID_ABOUT, MyFrame::OnAbout )
	EVT_MENU( wxID_SAVE, MyFrame::OnSave )
	EVT_MENU( wxID_SAVEAS, MyFrame::OnSaveAs )
	EVT_MENU( wxID_OPEN, MyFrame::OnOpen )
wxEND_EVENT_TABLE()


CHAGApp::~CHAGApp( void )
{
	delete m_LRUFilenameManager;
	delete m_scene;
	delete m_config;   // Saves current configuration
}

bool CHAGApp::OnInit()
{
	///////////////////////////////////////////////////////////////////////////
	// Call default behaviour (mandatory)
	// NOTE: Also kicks in command line parsing (below)
	// NOTE: Initialize the settings first, if that's not already done.
	///////////////////////////////////////////////////////////////////////////
	auto& settings = Settings::instance();
	if ( !wxApp::OnInit() )
		return false;

	m_scene = NULL;
	///////////////////////////////////////////////////////////////////////////
	// Set up wxConfig
	///////////////////////////////////////////////////////////////////////////
	m_config = new wxConfig( applicationWindowText() );

	///////////////////////////////////////////////////////////////////////////
	// Profiler View
	///////////////////////////////////////////////////////////////////////////
	m_show_profiler_view = false;

	///////////////////////////////////////////////////////////////////////////
	// Set up window
	///////////////////////////////////////////////////////////////////////////
	wxSize windowSize( 1280, 720 );
	m_frame = new MyFrame( applicationWindowText(), wxPoint( 50, 50 ), windowSize, this );
	m_frame->m_mgr.SetManagedWindow( m_frame );
	wxInitAllImageHandlers();

	///////////////////////////////////////////////////////////////////////////
	// Set up Settings' callback
	///////////////////////////////////////////////////////////////////////////
	settings.addListener( &m_settings_listener );

	///////////////////////////////////////////////////////////////////////////
	// The OpenGL Pane
	// NOTE: We go with the default window attributes here, since it is all we
	// 	     need and the new (wx 3.1) wxGLAttributes thing bugs out on linux.
	///////////////////////////////////////////////////////////////////////////
	m_glPane = new wxGLPane( this, (wxFrame*)m_frame );

	m_renderPanel = new wxTabbedPanel( m_frame, getRendererTabNames() );
	m_renderPanel->SetContent(m_glPane);
	m_renderPanel->SetTabChangedHandler( [this]( const std::string& t ) { onRenderTabChanged( t ); } );

	m_frame->m_mgr.AddPane( m_renderPanel, wxAuiPaneInfo().Name( "opengl_pane" ).Center().Caption( "Render" ).CaptionVisible( true ).MaximizeButton( true ) );

	///////////////////////////////////////////////////////////////////////////
	// Default settings for extra panes
	///////////////////////////////////////////////////////////////////////////
	wxAuiPaneInfo defaultAuiPaneInfo;
	defaultAuiPaneInfo.MaximizeButton( true ).BestSize( wxSize( 300, 300 ) );
	defaultAuiPaneInfo.DestroyOnClose( false );

	///////////////////////////////////////////////////////////////////////////
	// A text control for warnings and information
	///////////////////////////////////////////////////////////////////////////
	m_console = new wxTextCtrl( m_frame, -1, "", wxDefaultPosition, wxSize( 0, 300 ), wxTE_MULTILINE | wxTE_RICH );
	wxFont     f( 10, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, "consolas" );
	wxTextAttr a;
	a.SetFont( f );
	m_console->SetBackgroundColour( wxColor( 0x222222 ) );
	m_console->SetForegroundColour( wxColor( 0xDDDDDD ) );
	m_console->SetDefaultStyle( a );
	m_frame->m_mgr.AddPane( m_console, defaultAuiPaneInfo.Name( "console_pane" ).Bottom().Caption( "Console" ) );

	///////////////////////////////////////////////////////////////////////////
	// A DataViewControl for performance numbers
	///////////////////////////////////////////////////////////////////////////
	const int ID_PERFORMANCE_CTRL = 0x0;
	m_performance_view_ctrl =
	        new wxDataViewCtrl( m_frame, ID_PERFORMANCE_CTRL, wxDefaultPosition, wxDefaultSize, wxDV_MULTIPLE | wxDV_ROW_LINES );
	PerformanceProfiler* performance_model = &PerformanceProfiler::instance();
	performance_model->m_owning_control = m_performance_view_ctrl;
	m_performance_view_ctrl->AssociateModel( performance_model );
	m_text_renderer_0 = new wxDataViewTextRenderer( "string", wxDATAVIEW_CELL_INERT );
	m_text_renderer_1 = new wxDataViewTextRenderer( "string", wxDATAVIEW_CELL_INERT );
	m_text_renderer_2 = new wxDataViewTextRenderer( "string", wxDATAVIEW_CELL_INERT );
#ifdef CHAG_ENABLE_CUDA
	m_text_renderer_3 = new wxDataViewTextRenderer( "string", wxDATAVIEW_CELL_INERT );
#endif
	wxDataViewColumn* column1 = new wxDataViewColumn( "Scope Name", m_text_renderer_0, 0, 200, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE );
	m_performance_view_ctrl->AppendColumn( column1 );
	wxDataViewColumn* column2 = new wxDataViewColumn( "GL", m_text_renderer_1, 1, 100, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE );
	m_performance_view_ctrl->AppendColumn( column2 );
#ifdef CHAG_ENABLE_CUDA
	wxDataViewColumn* column3 = new wxDataViewColumn( "CUDA", m_text_renderer_2, 2, 100, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE );
	m_performance_view_ctrl->AppendColumn( column3 );
	wxDataViewColumn* column4 = new wxDataViewColumn( "CPU", m_text_renderer_3, 3, 100, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE );
	m_performance_view_ctrl->AppendColumn( column4 );
#else
	wxDataViewColumn* column3 = new wxDataViewColumn( "CPU", m_text_renderer_2, 2, 100, wxALIGN_LEFT, wxDATAVIEW_COL_RESIZABLE );
	m_performance_view_ctrl->AppendColumn( column3 );
#endif
	m_performance_view_ctrl->SetExpanderColumn( m_performance_view_ctrl->GetColumn( 0 ) );
	m_frame->m_mgr.AddPane( m_performance_view_ctrl, defaultAuiPaneInfo.Name( "profiler_pane" ).Bottom().Caption( "Profiler" ) );

	if ( applicationLoadsScene() )
	{
		///////////////////////////////////////////////////////////////////////////
		// Tree for showing scene
		///////////////////////////////////////////////////////////////////////////
		m_sceneTreeCtrl = new wxTreeCtrl( m_frame );
		m_sceneTreeCtrl->Bind( wxEVT_TREE_SEL_CHANGED, &CHAGApp::OnSceneTreeCtrlSelectionChanged, this );
		m_sceneTreeCtrl->Bind( wxEVT_TREE_ITEM_MENU, &CHAGApp::OnSceneTreeCtrlMenu, this );
		// m_frame->m_mgr.AddPane(m_sceneTreeCtrl, wxLEFT, "Scene Tree");
		m_frame->m_mgr.AddPane( m_sceneTreeCtrl, defaultAuiPaneInfo.Name( "scene_pane" ).Left().Caption( "Scene" ) );

		///////////////////////////////////////////////////////////////////////////
		// Property grid for viewing/modifying values of objects
		///////////////////////////////////////////////////////////////////////////
		m_sceneItemPropertyGrid =
		        new wxPropertyGrid( m_frame, 3, wxDefaultPosition, wxSize( 100, 220 ), wxPG_SPLITTER_AUTO_CENTER | wxPG_DEFAULT_STYLE );
		m_sceneItemPropertyGrid->SetMinClientSize( wxSize( 300, 0 ) );
		m_sceneItemPropertyGrid->Bind( wxEVT_PG_CHANGED, &CHAGApp::OnScenePropertyGridChanged, this );
		// m_frame->m_mgr.AddPane(m_sceneItemPropertyGrid, wxLEFT, "Properties");
		m_frame->m_mgr.AddPane( m_sceneItemPropertyGrid, defaultAuiPaneInfo.Name( "properties_pane" ).Left().Caption( "Properties" ) );
	}

	///////////////////////////////////////////////////////////////////////////
	// Create the settings window
	///////////////////////////////////////////////////////////////////////////
	m_settings_window = new SettingsWindow( m_frame );
	// Select a default settings database (possibly from command line)
	if ( m_command_line_selected_settings_database != "" )
	{
		m_settings_window->selectAndActivateSettingsDB( m_command_line_selected_settings_database );
	}
	// Add to window
	m_frame->m_mgr.AddPane( m_settings_window, defaultAuiPaneInfo.Name( "settings_window" ).Left().Caption( "Settings" ) );

	///////////////////////////////////////////////////////////////////////////
	// Open the file stdout.txt for reading (rerouted first thing in main).
	///////////////////////////////////////////////////////////////////////////
    m_stdout_in.open("stdout.txt");
    if (!m_stdout_in.good())
    {
        LOG_ERROR("Could not open stdout.txt");
        exit(1);
    }

	///////////////////////////////////////////////////////////////////////////
	// Commit to wxAUI and load configuration if there is one
	///////////////////////////////////////////////////////////////////////////
	m_frame->loadWindowLayout();
	m_frame->m_mgr.Update();
	SetTopWindow( m_frame );
	m_frame->Show();
	m_frame->setupWindowMenu();

	///////////////////////////////////////////////////////////////////////////
	// Forcing an update here so that the first frame stuff (including GLEW
	// initialization) happens BEFORE we try to initialize a scene.
	///////////////////////////////////////////////////////////////////////////
	m_frame->Refresh();
	m_frame->Update();
	m_settings_window->settingsHaveChanged();

	///////////////////////////////////////////////////////////////////////////
	// Set up a progress listener
	///////////////////////////////////////////////////////////////////////////
	// TextProgressListener::activate();
	ProgressListenerDialog::activate( this );

	///////////////////////////////////////////////////////////////////////////
	// Get file to open from LRU dialog
	///////////////////////////////////////////////////////////////////////////
	if ( applicationLoadsScene() )
	{
		m_LRUFilenameManager = new LRUFilenameManager( "LRUFilenames.txt" );
		if ( m_initialFilename == "" )
		{
			std::vector<std::string> filenames = m_LRUFilenameManager->getFilenames();
			if ( filenames.size() > 0 )
			{
				wxArrayString choices;
				for ( const auto& filename : filenames )
					choices.Add( filename );
				wxSingleChoiceDialog* dlg = new wxSingleChoiceDialog( m_frame, "Choose a recently used file", "Choose File", choices );
				if ( dlg->ShowModal() == wxID_OK )
				{
					LOG_VERBOSE( "LRU File chosen: " << filenames[dlg->GetSelection()] );
					m_initialFilename = filenames[dlg->GetSelection()];
				}
				delete dlg;
			}
		}
		if ( m_initialFilename == "" )
		{
			wxFileDialog openFileDialog(
			        m_frame, _( "Open SCENE file" ), "", "", "glTF files (*.gltf)|*.gltf", wxFD_OPEN | wxFD_FILE_MUST_EXIST );
			if ( openFileDialog.ShowModal() == wxID_OK )
				m_initialFilename = openFileDialog.GetPath();
		}
		m_scene = new scene::Scene;
		if ( m_initialFilename == "" )
		{
			LOG_VERBOSE( "No initial file chosen." );
		}
		else
		{
			load( m_initialFilename );
			m_LRUFilenameManager->addFilename( m_initialFilename );
			m_LRUFilenameManager->writeToFile();
		}
		initializeSceneTreeCtrl();
	}
	return true;
}

void CHAGApp::startFrame()
{
	std::chrono::time_point<std::chrono::high_resolution_clock> thisFrame = std::chrono::high_resolution_clock::now();
	m_frameDt = thisFrame - m_lastFrameTime;
	m_lastFrameTime = thisFrame;
}

void CHAGApp::OnInitCmdLine( wxCmdLineParser& parser )
{
	parser.SetDesc( g_cmdLineDesc );
	// must refuse '/' as parameter starter or cannot use "/path" style paths
	parser.SetSwitchChars( wxT( "-" ) );
}

bool CHAGApp::OnCmdLineParsed( wxCmdLineParser& parser )
{
	///////////////////////////////////////////////////////////////////////////
	// Find out if a starting .scene file was chosen
	///////////////////////////////////////////////////////////////////////////
	if ( parser.GetParamCount() > 0 )
	{
		m_initialFilename = parser.GetParam( 0 );
	}

	///////////////////////////////////////////////////////////////////////////
	// See if we should set a batch mode
	///////////////////////////////////////////////////////////////////////////
	if ( parser.Found( "b", &m_batch_mode ) )
	{
		LOG_VERBOSE( "Running in batch_mode: " << m_batch_mode );
	}

	///////////////////////////////////////////////////////////////////////////
	// Find out if a specific settings database should be chosen
	///////////////////////////////////////////////////////////////////////////

	auto     remove_whitespace = []( std::string& str ) { str.erase( std::remove( str.begin(), str.end(), ' ' ), str.end() ); };
	wxString database;
	if ( parser.Found( "s", &database ) )
	{
		m_command_line_selected_settings_database = database.ToStdString();
		remove_whitespace( m_command_line_selected_settings_database );
		LOG_VERBOSE( "Preselecting " << m_command_line_selected_settings_database << " settings database." );
	}

	///////////////////////////////////////////////////////////////////////////
	// Find out if modifications should be done to the selected settings
	///////////////////////////////////////////////////////////////////////////
	wxString modifications;
	if ( parser.Found( "m", &modifications ) )
	{
		std::stringstream        ss( modifications.ToStdString() );
		std::string              item;
		std::vector<std::string> statements;
		while ( std::getline( ss, item, ';' ) )
		{
			statements.push_back( item );
		}
		for ( auto s : statements )
		{
			remove_whitespace( s );
			std::stringstream is( s );
			std::string       category, name, valuestr;
			getline( is, category, ':' );
			getline( is, name, '=' );
			getline( is, valuestr );
			LOG_VERBOSE( "Settings modifier on command line: " << category << "[:]" << name << "[=]" << valuestr );

			wxAny             val;
			std::stringstream ss( valuestr );
			std::string       setting_type = Settings::instance().get_type( category, name );
			if ( setting_type == "string" )
			{
				std::string v;
				ss >> v;
				val = wxAny( v );
			}
			else if ( setting_type == "int" )
			{
				int v;
				ss >> v;
				val = wxAny( v );
			}
			else if ( setting_type == "bool" )
			{
				bool v;
				ss >> std::boolalpha >> v;
				val = wxAny( v );
			}
			else if ( setting_type == "float" )
			{
				float v;
				ss >> v;
				val = wxAny( v );
			}
			else
			{
				LOG_ERROR( "Oops, was not expecting this type on command line. Easy fix but it's yours. Sorry." );
			}
			Settings::instance().set_setting( category, name, val, m_command_line_selected_settings_database );
		}
	}
	return true;
}


void CHAGApp::initializeSceneTreeCtrl()
{
	m_sceneTreeCtrl->DeleteAllItems();
	m_sceneItems.clear();
	m_sceneMaterials.clear();
	wxTreeItemId rootID = m_sceneTreeCtrl->AddRoot( "Scene" );
	m_lightRootID = m_sceneTreeCtrl->AppendItem( rootID, "Lights" );
	for ( auto l : m_scene->m_lights )
	{
		wxTreeItemId lightID = m_sceneTreeCtrl->AppendItem( m_lightRootID, l->m_name );
		m_sceneItems[lightID] = l;
	}
	m_shadowGeneratorRootID = m_sceneTreeCtrl->AppendItem( rootID, "Shadow Generators" );
	for ( auto s : m_scene->m_shadowGenerators )
	{
		wxTreeItemId shadowCasterID = m_sceneTreeCtrl->AppendItem( m_shadowGeneratorRootID, s->m_name );
		m_sceneItems[shadowCasterID] = s;
	}
	m_cameraRootID = m_sceneTreeCtrl->AppendItem( rootID, "Cameras" );
	for ( auto c : m_scene->m_cameras )
	{
		wxTreeItemId cameraID = m_sceneTreeCtrl->AppendItem( m_cameraRootID, c->m_name );
		m_sceneItems[cameraID] = c;
	}
	m_modelRootID = m_sceneTreeCtrl->AppendItem( rootID, "Models" );
	for ( auto m : m_scene->m_models )
	{
		wxTreeItemId modelID = m_sceneTreeCtrl->AppendItem( m_modelRootID, m->m_name );
		m_sceneItems[modelID] = m;

		for ( auto mat : m->m_materials )
		{
			wxTreeItemId materialID = m_sceneTreeCtrl->AppendItem( modelID, mat.first );
			m_sceneMaterials[materialID] = mat.second;
		}
	}
	m_sceneTreeCtrl->ExpandAll();
}

void CHAGApp::OnSceneTreeCtrlSelectionChanged( wxTreeEvent& /*event*/ )
{
	wxTreeItemId selected = m_sceneTreeCtrl->GetSelection();
	scene::Item* item = m_sceneItems.count( selected ) > 0 ? m_sceneItems[selected] : NULL;
	Material*    material = m_sceneMaterials.count( selected ) > 0 ? m_sceneMaterials[selected] : NULL;
	if ( item )
	{
		scene::Light*           light = dynamic_cast<scene::Light*>( item );
		scene::Camera*          camera = dynamic_cast<scene::Camera*>( item );
		scene::Model*           model = dynamic_cast<scene::Model*>( item );
		scene::ShadowGenerator* shadowGenerator = dynamic_cast<scene::ShadowGenerator*>( item );

		m_sceneItemPropertyGrid->Clear();

		if ( !shadowGenerator )
		{
			m_sceneItemPropertyGrid->Append( new wxPropertyCategory( "Transform" ) );
			m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Position X", wxPG_LABEL, item->m_transform.pos.x ) );
			m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Position Y", wxPG_LABEL, item->m_transform.pos.y ) );
			m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Position Z", wxPG_LABEL, item->m_transform.pos.z ) );
			m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Scale X", wxPG_LABEL, item->m_transform.scale.x ) );
			m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Scale Y", wxPG_LABEL, item->m_transform.scale.y ) );
			m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Scale Z", wxPG_LABEL, item->m_transform.scale.z ) );
		}
		else
		{
			m_sceneItemPropertyGrid->Append( new wxPropertyCategory( "Transform" ) );
			m_sceneItemPropertyGrid->Append(
			        new wxFloatProperty( "Position X", wxPG_LABEL, shadowGenerator->m_light->m_transform.pos.x ) );
			m_sceneItemPropertyGrid->Append(
			        new wxFloatProperty( "Position Y", wxPG_LABEL, shadowGenerator->m_light->m_transform.pos.y ) );
			m_sceneItemPropertyGrid->Append(
			        new wxFloatProperty( "Position Z", wxPG_LABEL, shadowGenerator->m_light->m_transform.pos.z ) );
			m_sceneItemPropertyGrid->Append(
			        new wxFloatProperty( "Scale X", wxPG_LABEL, shadowGenerator->m_light->m_transform.scale.x ) );
			m_sceneItemPropertyGrid->Append(
			        new wxFloatProperty( "Scale Y", wxPG_LABEL, shadowGenerator->m_light->m_transform.scale.y ) );
			m_sceneItemPropertyGrid->Append(
			        new wxFloatProperty( "Scale Z", wxPG_LABEL, shadowGenerator->m_light->m_transform.scale.z ) );
		}

		if ( light )
		{
			m_sceneItemPropertyGrid->Append( new wxPropertyCategory( "Emittance" ) );
			m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Radiance", wxPG_LABEL, light->m_radiance ) );
			m_sceneItemPropertyGrid->Append( new wxColourProperty( "Color", wxPG_LABEL, toWxColour( light->m_color ) ) );
			switch ( light->m_shape.type )
			{
				case scene::Light::Shape::Disc:
				{
					m_sceneItemPropertyGrid->Append( new wxPropertyCategory( "Disc Diffuse Light" ) );
					m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Radius", wxPG_LABEL, light->m_shape.Disc.m_radius ) );
					break;
				}
				case scene::Light::Shape::Rectangle:
				{
					m_sceneItemPropertyGrid->Append( new wxPropertyCategory( "Rectangle Diffuse Light" ) );
					m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Width", wxPG_LABEL, light->m_shape.Rectangle.m_width ) );
					m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Height", wxPG_LABEL, light->m_shape.Rectangle.m_height ) );
					break;
				}
				case scene::Light::Shape::Point:
				default:
					break;
			}
			switch ( light->m_distribution.type )
			{
				case scene::Light::Distribution::Spot:
				{
					m_sceneItemPropertyGrid->Append( new wxPropertyCategory( "Spot Light" ) );
					m_sceneItemPropertyGrid->Append( new wxFloatProperty(
					        "Attenuation Start", wxPG_LABEL, light->m_distribution.Spot.m_attenuation_start ) );
					m_sceneItemPropertyGrid->Append(
					        new wxFloatProperty( "Attenuation End", wxPG_LABEL, light->m_distribution.Spot.m_attenuation_end ) );
					m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Beam FOV", wxPG_LABEL, light->m_distribution.Spot.m_beam_fov ) );
					m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Field FOV", wxPG_LABEL, light->m_distribution.Spot.m_field_fov ) );
					break;
				}
				case scene::Light::Distribution::Diffuse:
				default:
					break;
			}
		}
		if ( camera )
		{
			m_sceneItemPropertyGrid->Append( new wxPropertyCategory( "Projection" ) );
			m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Near plane", wxPG_LABEL, camera->m_near ) );
			m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Far plane", wxPG_LABEL, camera->m_far ) );

			scene::PerspectiveCamera* perspective_camera = dynamic_cast<scene::PerspectiveCamera*>( camera );
			if ( perspective_camera )
			{
				m_sceneItemPropertyGrid->Append( new wxFloatProperty( "FOV", wxPG_LABEL, perspective_camera->m_fov ) );
			}
			else
			{
			}
		}
		if ( model )
		{
		}
		if ( shadowGenerator )
		{
			m_sceneItemPropertyGrid->Clear();
			ShadowMapShadowGenerator* shadow_map_shadow_generator = dynamic_cast<ShadowMapShadowGenerator*>( shadowGenerator );
			PerspectiveShadowMapShadowGenerator* perspective_shadow_map_shadow_generator =
			        dynamic_cast<PerspectiveShadowMapShadowGenerator*>( shadowGenerator );
			OrthoShadowMapShadowGenerator* ortho_shadow_map_shadow_generator =
			        dynamic_cast<OrthoShadowMapShadowGenerator*>( shadowGenerator );
			if ( shadow_map_shadow_generator )
			{
				m_sceneItemPropertyGrid->Append( new wxPropertyCategory( "Shadow Map" ) );
				m_sceneItemPropertyGrid->Append(
				        new wxIntProperty( "Width", wxPG_LABEL, shadow_map_shadow_generator->m_resolution.x ) );
				m_sceneItemPropertyGrid->Append(
				        new wxIntProperty( "Height", wxPG_LABEL, shadow_map_shadow_generator->m_resolution.y ) );
				m_sceneItemPropertyGrid->Append( new wxFloatProperty(
				        "Polygon Offset Factor", wxPG_LABEL, shadow_map_shadow_generator->m_polygon_offset_factor ) );
				m_sceneItemPropertyGrid->Append( new wxFloatProperty(
				        "Polygon Offset Units", wxPG_LABEL, shadow_map_shadow_generator->m_polygon_offset_units ) );
			}
			if ( perspective_shadow_map_shadow_generator )
			{
				m_sceneItemPropertyGrid->Append(
				        new wxFloatProperty( "Near plane", wxPG_LABEL, perspective_shadow_map_shadow_generator->m_near ) );
				m_sceneItemPropertyGrid->Append(
				        new wxFloatProperty( "Far plane", wxPG_LABEL, perspective_shadow_map_shadow_generator->m_far ) );
				m_sceneItemPropertyGrid->Append( new wxFloatProperty(
				        "Aspect Ratio", wxPG_LABEL, perspective_shadow_map_shadow_generator->m_aspect_ratio ) );
				m_sceneItemPropertyGrid->Append( new wxFloatProperty( "FOV", wxPG_LABEL, perspective_shadow_map_shadow_generator->m_fov ) );
			}
			if ( ortho_shadow_map_shadow_generator )
			{
				m_sceneItemPropertyGrid->Append(
				        new wxFloatProperty( "Near plane", wxPG_LABEL, ortho_shadow_map_shadow_generator->m_near ) );
				m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Far plane", wxPG_LABEL, ortho_shadow_map_shadow_generator->m_far ) );
				m_sceneItemPropertyGrid->Append(
				        new wxFloatProperty( "Left plane", wxPG_LABEL, ortho_shadow_map_shadow_generator->m_left ) );
				m_sceneItemPropertyGrid->Append(
				        new wxFloatProperty( "Right plane", wxPG_LABEL, ortho_shadow_map_shadow_generator->m_right ) );
				m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Top plane", wxPG_LABEL, ortho_shadow_map_shadow_generator->m_top ) );
				m_sceneItemPropertyGrid->Append(
				        new wxFloatProperty( "Bottom plane", wxPG_LABEL, ortho_shadow_map_shadow_generator->m_bottom ) );
			}
		}
	}
	else if ( material )
	{
		m_sceneItemPropertyGrid->Clear();
		m_sceneItemPropertyGrid->Append( new wxPropertyCategory( "Material" ) );
		m_sceneItemPropertyGrid->Append( new wxColourProperty( "Base Color", wxPG_LABEL, toWxColour( material->m_base_color_factor ) ) );
		m_sceneItemPropertyGrid->Append( new wxFileProperty( "Base Color Texture", wxPG_LABEL, material->m_base_color_texture_path ) );
		m_sceneItemPropertyGrid->SetPropertyAttribute( "Base Color Texture", wxPG_FILE_WILDCARD, "Images (*.jpg,*.png,*.tga,*.bmp,*.hdr,...)|*.jpeg;*.jpg;*.png;*.gif;*.tga;*.bmp;*.psd;*.hdr" );
		m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Alpha Cutoff", wxPG_LABEL, material->m_alpha_cutoff ) );
		m_sceneItemPropertyGrid->Append( new wxColourProperty( "Emission Factor", wxPG_LABEL, toWxColour( material->m_emissive_factor ) ) );
		m_sceneItemPropertyGrid->Append( new wxFileProperty( "Emission Texture", wxPG_LABEL, material->m_emissive_texture_path ) );
		m_sceneItemPropertyGrid->SetPropertyAttribute( "Emission Texture", wxPG_FILE_WILDCARD, "Images (*.jpg,*.png,*.tga,*.bmp,*.hdr,...)|*.jpeg;*.jpg;*.png;*.gif;*.tga;*.bmp;*.psd;*.hdr" );
		m_sceneItemPropertyGrid->Append( new wxFileProperty( "Normal Texture", wxPG_LABEL, material->m_normal_texture_path ) );
		m_sceneItemPropertyGrid->SetPropertyAttribute( "Normal Texture", wxPG_FILE_WILDCARD, "Images (*.jpg,*.png,*.tga,*.bmp,*.hdr,...)|*.jpeg;*.jpg;*.png;*.gif;*.tga;*.bmp;*.psd;*.hdr" );
		m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Normal Map Scale Factor", wxPG_LABEL, material->m_normal_scale ) );
		m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Metallicity Factor", wxPG_LABEL, material->m_metallic_factor ) );
		m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Roughness Factor", wxPG_LABEL, material->m_roughness_factor ) );
		m_sceneItemPropertyGrid->Append( new wxFileProperty( "Metallicity/Roughness Texture", wxPG_LABEL, material->m_metallic_roughness_texture_path ) );
		m_sceneItemPropertyGrid->SetPropertyAttribute( "Metallicity/Roughness Texture", wxPG_FILE_WILDCARD, "Images (*.jpg,*.png,*.tga,*.bmp,*.hdr,...)|*.jpeg;*.jpg;*.png;*.gif;*.tga;*.bmp;*.psd;*.hdr" );
	}
	else if ( selected == m_sceneTreeCtrl->GetRootItem() )
	{
		m_sceneItemPropertyGrid->Clear();
		m_sceneItemPropertyGrid->Append( new wxPropertyCategory( "Scene" ) );
		m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Unit Multiplier", wxPG_LABEL, m_scene->m_sceneUnitMultiplier ) );
		m_sceneItemPropertyGrid->Append( new wxFloatProperty( "Debug Geometry Size", wxPG_LABEL, m_scene->m_sceneDebugGeomSizeMultiplier ) );
		m_sceneItemPropertyGrid->Append( new wxFileProperty( "Environment Map", wxPG_LABEL, m_scene->m_environmentMap_path ) );
		m_sceneItemPropertyGrid->SetPropertyAttribute( "Environment Map", wxPG_FILE_WILDCARD, "Images (*.jpg,*.png,*.tga,*.bmp,*.hdr,...)|*.jpeg;*.jpg;*.png;*.gif;*.tga;*.bmp;*.psd;*.hdr" );
		m_sceneItemPropertyGrid->Append( new wxFloatProperty(
		        "Environment Map Intensity Multplier", wxPG_LABEL, m_scene->m_envmap_intensity_multiplier ) );
		m_sceneItemPropertyGrid->Append( new wxColourProperty( "Environment Color", wxPG_LABEL, toWxColour( m_scene->m_environment_color ) ) );

		std::string path;
		if ( m_scene->m_lightmap )
		{
			path = m_scene->m_lightmap->m_filename;
		}
		m_sceneItemPropertyGrid->Append( new wxFileProperty( "Lightmap Texture", wxPG_LABEL, path ) );
		m_sceneItemPropertyGrid->SetPropertyAttribute( "Lightmap Texture", wxPG_FILE_WILDCARD, "Images (*.jpg,*.png,*.tga,*.bmp,*.hdr,...)|*.jpeg;*.jpg;*.png;*.gif;*.tga;*.bmp;*.psd;*.hdr" );
	}
	else
	{
		m_sceneItemPropertyGrid->Clear();
		LOG_INFO( "Selected item was not found" );
	}
}

void CHAGApp::OnScenePropertyGridChanged( wxPropertyGridEvent& event )
{
	wxTreeItemId selected = m_sceneTreeCtrl->GetSelection();
	scene::Item* item = m_sceneItems[selected];
	Material*    material = m_sceneMaterials[selected];
	wxAny        value = event.GetValue();
	
	auto setTexture = [&]( const std::string& path, scene::Texture*& tex )
	{
		if ( path.empty() )
		{
			if ( tex != nullptr )
			{
				delete tex;
				tex = nullptr;
			}
		}
		else
		{
			if ( tex != nullptr )
			{
				delete tex;
			}
			tex = scene::Texture::loadFromAnyFileType(path);
		}
	};

	if ( item )
	{
		Light*           light = dynamic_cast<Light*>( item );
		Camera*          camera = dynamic_cast<Camera*>( item );
		Model*           model = dynamic_cast<Model*>( item );
		ShadowGenerator* shadowGenerator = dynamic_cast<ShadowGenerator*>( item );

		if ( !shadowGenerator )
		{
			if ( event.GetPropertyName() == "Position X" )
				item->m_transform.pos.x = value.As<float>();
			if ( event.GetPropertyName() == "Position Y" )
				item->m_transform.pos.y = value.As<float>();
			if ( event.GetPropertyName() == "Position Z" )
				item->m_transform.pos.z = value.As<float>();
			if ( event.GetPropertyName() == "Scale X" )
				item->m_transform.scale.x = value.As<float>();
			if ( event.GetPropertyName() == "Scale Y" )
				item->m_transform.scale.y = value.As<float>();
			if ( event.GetPropertyName() == "Scale Z" )
				item->m_transform.scale.z = value.As<float>();
		}
		else
		{
			if ( event.GetPropertyName() == "Position X" )
				shadowGenerator->m_light->m_transform.pos.x = value.As<float>();
			if ( event.GetPropertyName() == "Position Y" )
				shadowGenerator->m_light->m_transform.pos.y = value.As<float>();
			if ( event.GetPropertyName() == "Position Z" )
				shadowGenerator->m_light->m_transform.pos.z = value.As<float>();
			if ( event.GetPropertyName() == "Scale X" )
				shadowGenerator->m_light->m_transform.scale.x = value.As<float>();
			if ( event.GetPropertyName() == "Scale Y" )
				shadowGenerator->m_light->m_transform.scale.y = value.As<float>();
			if ( event.GetPropertyName() == "Scale Z" )
				shadowGenerator->m_light->m_transform.scale.z = value.As<float>();
		}

		if ( light )
		{
			if ( event.GetPropertyName() == "Radiance" )
				light->m_radiance = value.As<float>();
			if ( event.GetPropertyName() == "Color" )
				light->m_color = toFloat3Color( value.As<wxColour>() );
			switch ( light->m_shape.type )
			{
				case scene::Light::Shape::Disc:
				{
					if ( event.GetPropertyName() == "Radius" )
						light->m_shape.Disc.m_radius = value.As<float>();
					break;
				}
				case scene::Light::Shape::Rectangle:
				{
					if ( event.GetPropertyName() == "Width" )
						light->m_shape.Rectangle.m_width = value.As<float>();
					if ( event.GetPropertyName() == "Height" )
						light->m_shape.Rectangle.m_height = value.As<float>();
					break;
				}
				case scene::Light::Shape::Point:
				default:
					break;
			}
			switch ( light->m_distribution.type )
			{
				case scene::Light::Distribution::Spot:
				{
					if ( event.GetPropertyName() == "Attenuation Start" )
						light->m_distribution.Spot.m_attenuation_start = value.As<float>();
					if ( event.GetPropertyName() == "Attenuation End" )
						light->m_distribution.Spot.m_attenuation_end = value.As<float>();
					if ( event.GetPropertyName() == "Beam FOV" )
						light->m_distribution.Spot.m_beam_fov = value.As<float>();
					if ( event.GetPropertyName() == "Field FOV" )
						light->m_distribution.Spot.m_field_fov = value.As<float>();
					break;
				}
				case ::scene::Light::Distribution::Diffuse:
				default:
					break;
			}
		}
		if ( camera )
		{
			if ( event.GetPropertyName() == "Near plane" )
				camera->m_near = value.As<float>();
			if ( event.GetPropertyName() == "Far plane" )
				camera->m_far = value.As<float>();
			scene::PerspectiveCamera* perspective_camera = dynamic_cast<scene::PerspectiveCamera*>( camera );
			if ( perspective_camera )
			{
				if ( event.GetPropertyName() == "FOV" )
					perspective_camera->m_fov = value.As<float>();
			}
		}
		if ( model )
		{
		}
		if ( shadowGenerator )
		{
			ShadowMapShadowGenerator* shadow_map_shadow_generator = dynamic_cast<ShadowMapShadowGenerator*>( shadowGenerator );
			PerspectiveShadowMapShadowGenerator* perspective_shadow_map_shadow_generator =
			        dynamic_cast<PerspectiveShadowMapShadowGenerator*>( shadowGenerator );
			OrthoShadowMapShadowGenerator* ortho_shadow_map_shadow_generator =
			        dynamic_cast<OrthoShadowMapShadowGenerator*>( shadowGenerator );
			if ( shadow_map_shadow_generator )
			{
				if ( event.GetPropertyName() == "Width" )
				{
					shadow_map_shadow_generator->m_resolution.x = value.As<int>();
					shadow_map_shadow_generator->resize( shadow_map_shadow_generator->m_resolution.x,
					                                     shadow_map_shadow_generator->m_resolution.y );
				}
				if ( event.GetPropertyName() == "Height" )
				{
					shadow_map_shadow_generator->m_resolution.y = value.As<int>();
					shadow_map_shadow_generator->resize( shadow_map_shadow_generator->m_resolution.x,
					                                     shadow_map_shadow_generator->m_resolution.y );
				}
				if ( event.GetPropertyName() == "Polygon Offset Factor" )
					shadow_map_shadow_generator->m_polygon_offset_factor = value.As<float>();
				if ( event.GetPropertyName() == "Polygon Offset Units" )
					shadow_map_shadow_generator->m_polygon_offset_units = value.As<float>();
			}
			if ( perspective_shadow_map_shadow_generator )
			{
				if ( event.GetPropertyName() == "Near plane" )
					perspective_shadow_map_shadow_generator->m_near = value.As<float>();
				if ( event.GetPropertyName() == "Far plane" )
					perspective_shadow_map_shadow_generator->m_far = value.As<float>();
				if ( event.GetPropertyName() == "FOV" )
					perspective_shadow_map_shadow_generator->m_fov = value.As<float>();
				if ( event.GetPropertyName() == "Aspect Ratio" )
					perspective_shadow_map_shadow_generator->m_aspect_ratio = value.As<float>();
			}
			if ( ortho_shadow_map_shadow_generator )
			{
				if ( event.GetPropertyName() == "Near plane" )
					ortho_shadow_map_shadow_generator->m_near = value.As<float>();
				if ( event.GetPropertyName() == "Far plane" )
					ortho_shadow_map_shadow_generator->m_far = value.As<float>();
				if ( event.GetPropertyName() == "Left plane" )
					ortho_shadow_map_shadow_generator->m_left = value.As<float>();
				if ( event.GetPropertyName() == "Right plane" )
					ortho_shadow_map_shadow_generator->m_right = value.As<float>();
				if ( event.GetPropertyName() == "Top plane" )
					ortho_shadow_map_shadow_generator->m_top = value.As<float>();
				if ( event.GetPropertyName() == "Bottom plane" )
					ortho_shadow_map_shadow_generator->m_bottom = value.As<float>();
			}
		}
	}
	else if ( material )
	{
		wxTreeItemId parentModelId = m_sceneTreeCtrl->GetItemParent( selected );
		Model*       model = m_scene->getModel( std::string( m_sceneTreeCtrl->GetItemText( parentModelId ).mb_str() ) );
		std::string  materialName = std::string( m_sceneTreeCtrl->GetItemText( selected ).mb_str() );
		Material*    material = model->m_materials[materialName];


		if ( event.GetPropertyName() == "Base Color" )
		{
			material->m_base_color_factor = toFloat4Color( value.As<wxColour>() );
		}
		else if ( event.GetPropertyName() == "Base Color Texture" )
		{
			material->m_base_color_texture_path = value.As<wxString>().ToStdString();
			setTexture( material->m_base_color_texture_path, material->m_base_color_texture );
		}
		else if ( event.GetPropertyName() == "Alpha Cutoff" )
		{
			material->m_alpha_cutoff = value.As<float>();
		}
		if ( event.GetPropertyName() == "Emission Factor" )
		{
			material->m_emissive_factor = toFloat3Color( value.As<wxColour>() );
		}
		else if ( event.GetPropertyName() == "Emission Texture" )
		{
			material->m_emissive_texture_path = value.As<wxString>().ToStdString();
			setTexture( material->m_emissive_texture_path, material->m_emissive_texture );
		}
		else if ( event.GetPropertyName() == "Normal Texture" )
		{
			material->m_normal_texture_path = value.As<wxString>().ToStdString();
			setTexture( material->m_normal_texture_path, material->m_normal_texture );
		}
		else if ( event.GetPropertyName() == "Normal Map Scale Factor" )
		{
			material->m_normal_scale = value.As<float>();
		}
		else if ( event.GetPropertyName() == "Metallicity Factor" )
		{
			material->m_metallic_factor = value.As<float>();
		}
		else if ( event.GetPropertyName() == "Roughness Factor" )
		{
			material->m_roughness_factor = value.As<float>();
		}
		else if ( event.GetPropertyName() == "Metallicity/Roughness Texture" )
		{
			material->m_metallic_roughness_texture_path = value.As<wxString>().ToStdString();
			setTexture( material->m_metallic_roughness_texture_path, material->m_metallic_roughness_texture );
		}
	}
	else if ( selected == m_sceneTreeCtrl->GetRootItem() )
	{
		if ( event.GetPropertyName() == "Unit Multiplier" )
		{
			m_scene->m_sceneUnitMultiplier = value.As<float>();
		}
		if ( event.GetPropertyName() == "Debug Geometry Size" )
		{
			m_scene->m_sceneDebugGeomSizeMultiplier = value.As<float>();
		}
		if ( event.GetPropertyName() == "Environment Map Intensity Multplier" )
		{
			m_scene->m_envmap_intensity_multiplier = value.As<float>();
		}
		if ( event.GetPropertyName() == "Environment Map" )
		{
			m_scene->m_environmentMap_path = value.As<wxString>().ToStdString();
			setTexture( m_scene->m_environmentMap_path, m_scene->m_environmentMap );
		}
		if ( event.GetPropertyName() == "Environment Color" )
		{
			m_scene->m_environment_color = toFloat3Color( value.As<wxColour>() );
		}
		if ( event.GetPropertyName() == "Lightmap Texture" )
		{
			std::string path = value.As<wxString>().ToStdString();
			setTexture( path, m_scene->m_lightmap );
		}
	}
	// Trigger onSceneChanged() as things have happened to it..
	onSceneChanged();
}

Item* CHAGApp::CloneItem( Item* item, const std::string& new_name )
{
	scene::Item* new_item = item->clone();
	new_item->m_name = new_name;
	if ( dynamic_cast<Camera*>( item ) )
		m_scene->m_cameras.push_back( (scene::Camera*)new_item );
	if ( dynamic_cast<Light*>( item ) )
		m_scene->m_lights.push_back( (Light*)new_item );
	if ( dynamic_cast<Model*>( item ) )
		m_scene->m_models.push_back( (Model*)new_item );
	initializeSceneTreeCtrl();
	return new_item;
}

void CHAGApp::OnSceneTreeCtrlMenu( wxTreeEvent& event )
{
	wxTreeItemId       selected = event.GetItem();
	Light*             light = dynamic_cast<Light*>( m_sceneItems[selected] );
	PerspectiveCamera* camera = dynamic_cast<PerspectiveCamera*>( m_sceneItems[selected] );
	Model*             model = dynamic_cast<Model*>( m_sceneItems[selected] );
	ShadowGenerator*   shadowGenerator = dynamic_cast<ShadowGenerator*>( m_sceneItems[selected] );
	Item*              item = dynamic_cast<Item*>( m_sceneItems[selected] );
	if ( item )
	{
		wxMenu menu;
		enum { RENAME, CLONE, REMOVE, CONTROL, VIEW, VIEW_AND_CONTROL, CREATE_SHADOWMAP, CREATE_ORTHO_SHADOWMAP, TOGGLE_HIDE };
		menu.Append( VIEW, "View" );
		if ( !shadowGenerator )
		{
			menu.Append( CONTROL, "Control" );
			menu.Append( VIEW_AND_CONTROL, "View and Control" );
		}

		if ( light )
		{
			menu.AppendSeparator();

			menu.Append( CREATE_SHADOWMAP, "Create Shadowmap..." );
			menu.Append( CREATE_ORTHO_SHADOWMAP, "Create Ortho Shadowmap..." );
		}

		menu.AppendSeparator();

		menu.Append( RENAME, wxT( "Rename" ) );
		if ( !shadowGenerator )
		{
			menu.Append( CLONE, wxT( "Clone" ) );
		}

		menu.AppendSeparator();

		if ( model )
		{
			menu.Append( TOGGLE_HIDE, model->m_hidden ? "Show" : "Hide" );
		}
		menu.Append( REMOVE, wxT( "Remove" ) );

		switch ( m_sceneTreeCtrl->GetPopupMenuSelectionFromUser( menu ) )
		{
			case RENAME:
			{
				wxTextEntryDialog dlg( m_frame, "Enter new name", "Rename", item->m_name );
				if ( dlg.ShowModal() == wxID_OK )
				{
					item->rename( std::string( dlg.GetValue().mb_str() ) );
					m_sceneTreeCtrl->SetItemText( selected, item->m_name );
				}
			}
			break;
			case CLONE:
			{
				std::string name_of_new = item->m_name;
				while ( name_of_new == item->m_name )
				{
					wxTextEntryDialog dlg( m_frame, "Enter name for new item", "Clone", item->m_name );
					if ( dlg.ShowModal() == wxID_OK )
					{
						name_of_new = dlg.GetValue();
						m_sceneTreeCtrl->SetItemText( selected, name_of_new );
					}
				}
				CloneItem( item, name_of_new );
			}
			break;
			case REMOVE:
			{
				if ( camera )
				{
					std::vector<scene::Camera*>::iterator position =
					        std::find( m_scene->m_cameras.begin(), m_scene->m_cameras.end(), camera );
					m_scene->m_cameras.erase( position );
					delete camera;
				}
				if ( light )
				{
					std::vector<Light*>::iterator position = std::find( m_scene->m_lights.begin(), m_scene->m_lights.end(), light );
					m_scene->m_lights.erase( position );
					delete light;
				}
				if ( model )
				{
					std::vector<Model*>::iterator position = std::find( m_scene->m_models.begin(), m_scene->m_models.end(), model );
					m_scene->m_models.erase( position );
					delete model;
				}
				if ( shadowGenerator )
				{
					std::vector<ShadowGenerator*>::iterator position =
					        std::find( m_scene->m_shadowGenerators.begin(), m_scene->m_shadowGenerators.end(), shadowGenerator );
					m_scene->m_shadowGenerators.erase( position );
					delete shadowGenerator;
				}
				initializeSceneTreeCtrl();
			}
			break;
			case VIEW:
				m_scene->setCurrentRenderableItem( item->m_name );
				break;
			case CONTROL:
				m_scene->setCurrentControllableItem( item->m_name );
				break;
			case VIEW_AND_CONTROL:
				m_scene->setCurrentControllableItem( item->m_name );
				m_scene->setCurrentRenderableItem( item->m_name );
				break;
			case CREATE_SHADOWMAP:
			{
				PerspectiveShadowMapShadowGenerator* new_item = new PerspectiveShadowMapShadowGenerator;
				new_item->m_light = light;
				new_item->m_aspect_ratio = 1.0f;
				if ( light->m_distribution.type == scene::Light::Distribution::Spot )
				{
					new_item->m_fov = light->m_distribution.Spot.m_field_fov;
					new_item->m_far = light->m_distribution.Spot.m_attenuation_end;
				}
				else
				{
					new_item->m_fov = 60.0f;
					new_item->m_far = 1000.0f;
				}
				new_item->m_near = 0.1f;
				new_item->resize( 512, 512 );
				new_item->m_polygon_offset_factor = 1.0f;
				new_item->m_polygon_offset_units = 1.0f;
				std::stringstream ss;
				ss << "ShadowMap:" << light->m_name;
				new_item->m_name = ss.str();
				m_scene->m_shadowGenerators.push_back( (ShadowGenerator*)new_item );
				initializeSceneTreeCtrl();
				break;
			}
			case CREATE_ORTHO_SHADOWMAP:
			{
				OrthoShadowMapShadowGenerator* new_item = new OrthoShadowMapShadowGenerator;
				new_item->m_light = light;
				new_item->m_near = 1.0;
				new_item->m_far = 1000.0;
				new_item->m_left = -1.0;
				new_item->m_right = 1.0;
				new_item->m_bottom = -1.0;
				new_item->m_top = 1.0;
				new_item->resize( 512, 512 );
				new_item->m_polygon_offset_factor = 1.0f;
				new_item->m_polygon_offset_units = 1.0f;
				std::stringstream ss;
				ss << "OrthoShadowMap:" << light->m_name;
				new_item->m_name = ss.str();
				m_scene->m_shadowGenerators.push_back( (ShadowGenerator*)new_item );
				initializeSceneTreeCtrl();
				break;
			}
			case TOGGLE_HIDE:
			{
				model->m_hidden = !model->m_hidden;
				onSceneChanged();
				break;
			}
		}
	}
	if ( model )
	{
	}
	Material* material = m_sceneMaterials[selected];
	if ( material )
	{
	}
}

glm::ivec2 CHAGApp::getGlSize() const
{
	int width, height;
	if ( Settings::instance().get<bool>( "framebuffer", "locked_resolution" ) )
	{
		width = Settings::instance().get<int>( "framebuffer", "locked_resolution_width" );
		height = Settings::instance().get<int>( "framebuffer", "locked_resolution_height" );
	}
	else
	{
		width = m_glPane->GetSize().x;
		height = m_glPane->GetSize().y;
	}
	return { width, height };
}

void CHAGApp::firstFrame()
{
	m_lastFrameTime = std::chrono::high_resolution_clock::now();
	///////////////////////////////////////////////////////////////////////////
	// Initialize GLEW. Must be done after GL context is created and before
	// anything glewish is done.
	///////////////////////////////////////////////////////////////////////////
	glewInit();

	///////////////////////////////////////////////////////////////////////////
	// Initialize GLUT. To my knowledge we only need this for glutBitmapString
	// which should be replaced, but hey, maybe someone wants to draw a
	// glutSolidTeapot.
	///////////////////////////////////////////////////////////////////////////
	int zero = 0;
	glutInit( &zero, NULL );

	///////////////////////////////////////////////////////////////////////////
	// Context craeted and GLEW is initialized. Create the default
	// framebuffer
	///////////////////////////////////////////////////////////////////////////
	glm::ivec2 sz = getGlSize();
	int        width = sz.x, height = sz.y;

	m_glPane->m_default_framebuffer = opengl_helpers::framebuffer::createFramebuffer(
	        width,
	        height,
	        opengl_helpers::framebuffer::createTexture( width, height, GL_DEPTH_COMPONENT32 ),
	        { opengl_helpers::framebuffer::createTexture( width, height, GL_RGBA8 ) } );

	///////////////////////////////////////////////////////////////////////////
	// Initialization of renderers and corresponding shaders
	///////////////////////////////////////////////////////////////////////////
	onGlContextCreated();

	///////////////////////////////////////////////////////////////////////////
	// If framebuffer size is locked, reshape() will not call onReshape(),
	// so issue a first onReshape() here.
	// NOTE: Also, On Linux it seems that onReshape might not be guaranteed to
	//		 run before display, so force one here.
	///////////////////////////////////////////////////////////////////////////
	onReshape( width, height );

	///////////////////////////////////////////////////////////////////////////
	// Application dependent post GL initialization
	///////////////////////////////////////////////////////////////////////////
	onFirstFrame();
}

void CHAGApp::settingChanged( const std::string& categoryName, const std::string& settingName )
{
	LOG_VERBOSE( "Setting " << categoryName << ":" << settingName << " changed." );
	if ( categoryName == "log" && settingName == "active_level" )
	{
		///////////////////////////////////////////////////////////////////////////
		// Tell Log the currently wanted level of verbosity.
		///////////////////////////////////////////////////////////////////////////
		std::string active_level = Settings::instance().get<std::string>( categoryName, settingName );
		if ( active_level == "Error" )
			Log::SetLevelActive( Log::Error );
		if ( active_level == "Warning" )
			Log::SetLevelActive( Log::Warning );
		if ( active_level == "Info" )
			Log::SetLevelActive( Log::Info );
		if ( active_level == "Verbose" )
			Log::SetLevelActive( Log::Verbose );
	}

	///////////////////////////////////////////////////////////////////////////
	// Resize the default framebuffer, if resolution is locked.
	///////////////////////////////////////////////////////////////////////////
	assert( m_glPane->m_default_framebuffer != 0 );
	if ( categoryName == "framebuffer"
	     && ( settingName == "locked_resolution" || settingName == "locked_resolution_width" || settingName == "locked_resolution_height" ) )
	{
		if ( Settings::instance().get<bool>( "framebuffer", "locked_resolution" ) )
		{
			int w = Settings::instance().get<int>( "framebuffer", "locked_resolution_width" );
			int h = Settings::instance().get<int>( "framebuffer", "locked_resolution_height" );
			opengl_helpers::framebuffer::resizeFramebuffer( m_glPane->m_default_framebuffer, w, h );
			onReshape( w, h );
		}
		else
		{
			m_glPane->SendSizeEvent();
		}
	}

	onSettingChanged( categoryName, settingName );
}

std::string CHAGApp::getCurrentRenderTab() const
{
	return m_renderPanel->GetCurrentTab();
}

void CHAGApp::setCurrentRenderTab( const std::string & tab )
{
	m_renderPanel->SetCurrentTab( tab );
}

void CHAGApp::save( std::string filename )
{
	if ( m_scene != NULL )
	{   // Well, why wouldn't it?
		if ( filename.empty() )
		{
			filename = m_scene->m_filename;
		}
		if ( !saveScene( m_scene, filename ) )
		{
			wxMessageBox( "Save failed", "ERROR" );
		};
		if ( filename != "" )
		{
			m_LRUFilenameManager->addFilename( filename );
			m_LRUFilenameManager->writeToFile();
		}
	}
}

void CHAGApp::load( std::string filename )
{
	if ( !scene::loadScene( m_scene, filename ) )
	{
		wxMessageBox( "Load failed", "ERROR" );
	};
	m_LRUFilenameManager->addFilename( filename );
	m_LRUFilenameManager->writeToFile();
	initializeSceneTreeCtrl();
	onSceneChanged();
}

void CHAGApp::takeScreenshot( std::string filename )
{
	m_glPane->takeScreenshot( filename );
}

MyFrame::MyFrame( const wxString& title, const wxPoint& pos, const wxSize& size, CHAGApp* chagApp )
	: wxFrame( NULL, wxID_ANY, title, pos, size )
	, m_CHAGApp( chagApp )
{
	wxMenuBar* menuBar = new wxMenuBar;
	SetMenuBar( menuBar );

	wxMenu* menuFile = new wxMenu;
	if ( m_CHAGApp->applicationLoadsScene() )
	{
		menuFile->Append( wxID_OPEN, "&Open File...\tCtrl-O" );
		menuFile->Append( wxID_SAVE, "&Save\tCtrl-S" );
		menuFile->Append( wxID_SAVEAS, "&Save as...\tCtrl-Shift-S" );
		menuFile->Append( ID_ImportOBJ, "&Import OBJ...\tCtrl-H" );
		menuFile->AppendSeparator();
	}
	menuFile->Append( ID_TakeScreenshot, "&Take Screenshot\tF12" );
	menuFile->Append( ID_ReloadShaders, "&Reload Shaders\tShift-R" );
	menuFile->Append( ID_ClearWxConfig, "&Clear wxConfig" );
	menuFile->Append( wxID_EXIT, "&Exit...\tCtrl+C" );

	menuBar->Append( menuFile, "&File" );

	Bind( wxEVT_MENU, &MyFrame::OnMenuSelection, this );
	Bind( wxEVT_MENU, &MyFrame::OnExit, this, wxID_EXIT );
	Bind( wxEVT_MENU, &MyFrame::OnSave, this, wxID_SAVE );
	Bind( wxEVT_MENU, &MyFrame::OnSaveAs, this, wxID_SAVEAS );
	Bind( wxEVT_MENU, &MyFrame::OnOpen, this, wxID_OPEN );
	Bind( wxEVT_MENU, &MyFrame::OnImportOBJ, this, ID_ImportOBJ );
	Bind( wxEVT_MENU, &MyFrame::OnTakeScreenshot, this, ID_TakeScreenshot );
	Bind( wxEVT_MENU, &MyFrame::OnReloadShaders, this, ID_ReloadShaders );
	Bind( wxEVT_MENU, &CHAGApp::OnClearWxConfig, m_CHAGApp, ID_ClearWxConfig );

	/*
	wxMenu* animMenu = new wxMenu;
	animMenu->Append( ID_AnimationPlay, "&Toggle Play\tF5", "Play animation" );
	animMenu->Append( ID_AnimationRewind, "&Rewind\tF6", "Rewind animation" );
	animMenu->Append( ID_AnimationBenchmark, "&Benchmark\tCtrl+F5", "Benchmark animation" );
	animMenu->Append( ID_AnimationRecord, "&Record\tCtrl+Shift+F5", "Record animation" );
	menuBar->Append( animMenu, "Animation" );
	*/

	m_CHAGApp->onApplicationCreateMenuBar( menuBar );

	CreateStatusBar();
}

MyFrame::~MyFrame()
{
	m_mgr.UnInit();
}

void MyFrame::setupWindowMenu()
{
	wxMenu* menuWindows = new wxMenu;
	menuWindows->Append( ID_LoadWindowLayout, "&Load Window Layout\tHome" );
	menuWindows->Append( ID_SaveWindowLayout, "&Save Window Layout\tInsert" );
	menuWindows->AppendSeparator();
	wxAuiPaneInfoArray allPanes = m_mgr.GetAllPanes();
	for ( int i = 0; i < static_cast<int>( allPanes.size() ); i++ )
	{
		menuWindows->Append( ID_ShowPane0 + i, wxString( "Show " ) + allPanes[i].caption );
		Bind( wxEVT_MENU, &MyFrame::OnShowPane, this, ID_ShowPane0 + i );
	}
	Bind( wxEVT_MENU, &MyFrame::OnLoadPerspective, this, ID_LoadWindowLayout );
	Bind( wxEVT_MENU, &MyFrame::OnSavePerspective, this, ID_SaveWindowLayout );

	menuWindows->Append( ID_ShowProfilerView, "&Show Profile View\tF3" );
	Bind( wxEVT_MENU, &MyFrame::OnShowProfilerView, this, ID_ShowProfilerView );
	GetMenuBar()->Append( menuWindows, "&Window" );
}

void MyFrame::OnShowPane( wxCommandEvent& event )
{
	wxAuiPaneInfo& pane = m_mgr.GetAllPanes()[event.GetId() - ID_ShowPane0];
	if ( !pane.IsShown() )
	{
		pane.Show( true );
		m_mgr.Update();
	}
}

void MyFrame::OnShowProfilerView( wxCommandEvent& /*event*/ )
{
	m_CHAGApp->m_show_profiler_view = !m_CHAGApp->m_show_profiler_view;
};


void MyFrame::loadWindowLayout()
{
	///////////////////////////////////////////////////////////////////////////
	// Reset window position to stored
	///////////////////////////////////////////////////////////////////////////
	wxPoint pos;
	if ( !m_CHAGApp->m_config->Read( "last_window_posx", &pos.x ) || !m_CHAGApp->m_config->Read( "last_window_posy", &pos.y ) )
	{
		LOG_WARNING( "No previous window position" );
	}
	else
	{
		SetPosition( pos );
	}
	///////////////////////////////////////////////////////////////////////////
	// Reset window size to stored
	///////////////////////////////////////////////////////////////////////////
	wxSize size;
	if ( !m_CHAGApp->m_config->Read( "last_window_width", &size.x ) || !m_CHAGApp->m_config->Read( "last_window_height", &size.y ) )
	{
		LOG_WARNING( "No previous window size" );
	}
	else
	{
		SetSize( size );
	}
	///////////////////////////////////////////////////////////////////////////
	// Load wxAUI perspective
	///////////////////////////////////////////////////////////////////////////
	wxString loaded_config;
	if ( !m_CHAGApp->m_config->Read( "wxAUI_configuration", &loaded_config ) )
	{
		LOG_WARNING( "No perspective stored\n" );
	}
	else
	{
		m_mgr.LoadPerspective( loaded_config );
	}
	///////////////////////////////////////////////////////////////////////////
	// Load Renderer tab
	///////////////////////////////////////////////////////////////////////////
	wxString renderer_tab;
	if ( !m_CHAGApp->m_config->Read( "active_renderer_tab", &renderer_tab ) )
	{
		LOG_WARNING( "No Renderer Tab stored\n" );
	}
	else
	{
		m_CHAGApp->setCurrentRenderTab( renderer_tab.ToStdString() );
	}
}

void MyFrame::saveWindowLayout()
{
	///////////////////////////////////////////////////////////////////////////
	// Store window position
	///////////////////////////////////////////////////////////////////////////
	wxPoint windowPos = GetPosition();
	m_CHAGApp->m_config->Write( "last_window_posx", windowPos.x );
	m_CHAGApp->m_config->Write( "last_window_posy", windowPos.y );
	///////////////////////////////////////////////////////////////////////////
	// Store window size
	///////////////////////////////////////////////////////////////////////////
	wxSize windowSize = GetSize();
	m_CHAGApp->m_config->Write( "last_window_width", windowSize.x );
	m_CHAGApp->m_config->Write( "last_window_height", windowSize.y );
	///////////////////////////////////////////////////////////////////////////
	// Store wxAUI perspective
	///////////////////////////////////////////////////////////////////////////
	std::string current_config = m_mgr.SavePerspective().ToStdString();
	m_CHAGApp->m_config->Write( "wxAUI_configuration", wxString( current_config ) );
	///////////////////////////////////////////////////////////////////////////
	// Store Renderer tab
	///////////////////////////////////////////////////////////////////////////
	wxString renderer_tab;
	m_CHAGApp->m_config->Write( "active_renderer_tab", wxString( m_CHAGApp->getCurrentRenderTab() ) );

	m_CHAGApp->m_config->Flush();
}

void MyFrame::OnMenuSelection( wxCommandEvent& event )
{
	if ( event.GetId() == ID_AnimationPlay )
	{
	}
	else if ( event.GetId() == ID_AnimationRewind )
	{
	}
	else if ( event.GetId() == ID_AnimationBenchmark )
	{
	}
	else if ( event.GetId() == ID_AnimationRecord )
	{
	}
	else
	{
		LOG_VERBOSE( "Unhandled Menu Command. Calling application." );
		m_CHAGApp->onUnhandledMenuCommand( event );
	}
}

void MyFrame::OnSaveAs( wxCommandEvent& /*event*/ )
{
	wxFileDialog saveFileDialog(
	        this, _( "Save SCENE file" ), "", "", "glTF files (*.gltf)|*.gltf", wxFD_SAVE | wxFD_OVERWRITE_PROMPT );
	if ( saveFileDialog.ShowModal() == wxID_CANCEL )
		return;
	std::string filename = std::string( saveFileDialog.GetPath().mb_str( wxConvUTF8 ) );
	LOG_VERBOSE( "Save as..." << filename );
	m_CHAGApp->save( filename );
}

void MyFrame::OnSave( wxCommandEvent& /*event*/ )
{
	LOG_VERBOSE( "Save" );
	m_CHAGApp->save( "" );
}

void MyFrame::OnOpen( wxCommandEvent& /*event*/ )
{
	wxFileDialog openFileDialog(
	        this, _( "Open SCENE file" ), "", "", "glTF files (*.gltf)|*.gltf", wxFD_OPEN | wxFD_FILE_MUST_EXIST );
	if ( openFileDialog.ShowModal() == wxID_CANCEL )
		return;
	std::string filename = std::string( openFileDialog.GetPath().mb_str( wxConvUTF8 ) );
	LOG_VERBOSE( "Load " << filename );
	m_CHAGApp->load( filename );
}

void MyFrame::OnExit( wxCommandEvent& /*event*/ )
{
	saveWindowLayout();
    m_CHAGApp->m_stdout_in.close();
	while ( !Close( true ) )
		;
}

void MyFrame::OnAbout( wxCommandEvent& /*event*/ )
{
	wxMessageBox( "This is a wxWidgets' Hello world sample", "About Hello World", wxOK | wxICON_INFORMATION );
}
void MyFrame::OnImportOBJ( wxCommandEvent& /*event*/ )
{
	wxMessageBox( "Importing models is disabled", "Import Error", wxOK | wxICON_INFORMATION );
	/*
	wxFileDialog openFileDialog( this, _( "Open OBJ file" ), "", "", "OBJ files (*.obj)|*.obj", wxFD_OPEN | wxFD_FILE_MUST_EXIST );
	if ( openFileDialog.ShowModal() == wxID_CANCEL )
		return;
	std::string filename = std::string( openFileDialog.GetPath().mb_str( wxConvUTF8 ) );
	LOG_VERBOSE( "Import OBJ " << filename );
	m_CHAGApp->importOBJ( filename );
	*/
}

void MyFrame::OnTakeScreenshot( wxCommandEvent& /*event*/ )
{
	wxFileDialog saveFileDialog( this, _( "Save PNG file" ), "", "", "PNG files (*.png)|*.png", wxFD_SAVE );
	if ( saveFileDialog.ShowModal() == wxID_CANCEL )
		return;
	std::string filename = std::string( saveFileDialog.GetPath().mb_str( wxConvUTF8 ) );
	LOG_VERBOSE( "Saving screenshot to " << filename );
	m_CHAGApp->takeScreenshot( filename );
}

void MyFrame::OnReloadShaders( wxCommandEvent& /*event*/ )
{
	wxString wxaui_configuration;
	m_CHAGApp->onReloadShaders();
}

void CHAGApp::syncConsoleWithStdout()
{
	///////////////////////////////////////////////////////////////////////////
	// Scan stdout and choose font depending on if this is info, warning
	// or error.
	///////////////////////////////////////////////////////////////////////////
	fflush( stdout );
	std::string line;
	while(std::getline(m_stdout_in, line)) {
		if (line.find("] VERBOSE") != std::string::npos) {
			wxFont     f( 10, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, "consolas" );
			wxTextAttr a;
			a.SetFont( f );
			a.SetTextColour( wxColor( 0xDDDDDD ) );
			m_console->SetDefaultStyle( a );
		}
		if ( line.find( "] INFO" ) != std::string::npos )
		{
			wxFont     f( 10, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, "consolas" );
			wxTextAttr a;
			a.SetFont( f );
			a.SetTextColour( wxColor( 0x11AA11 ) );
			m_console->SetDefaultStyle( a );
		}
		if ( line.find( "] WARNING" ) != std::string::npos )
		{
			wxFont     f( 10, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, "consolas" );
			wxTextAttr a;
			a.SetFont( f );
			a.SetTextColour( wxColor( 0x00a5ff ) );
			m_console->SetDefaultStyle( a );
		}
		if ( line.find( "] ERROR" ) != std::string::npos )
		{
			wxFont     f( 10, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, "consolas" );
			wxTextAttr a;
			a.SetFont( f );
			a.SetTextColour( wxColor( 0x2222EE ) );
			m_console->SetDefaultStyle( a );
		}
		(*m_console) << line << "\n";
	}
    m_stdout_in.clear();
}
