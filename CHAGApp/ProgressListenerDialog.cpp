#include "ProgressListenerDialog.h"
#include <assert.h>
#include <iostream>
#include <wx/app.h>

ProgressListenerDialog progress_listener_dialog; 

CHAGApp * ProgressListenerDialog::m_app; 

void ProgressListenerDialog::activate(CHAGApp * app)
{
	progress_listener_dialog.m_app = app; 
	g_progress = &progress_listener_dialog;	
}

void ProgressListenerDialog::setRange(int value)
{
	assert(m_progress_counters.size() > 0);
	m_progress_counters.back().gauge->SetRange(value);
}

void ProgressListenerDialog::update(int progress)
{
	assert(m_progress_counters.size() > 0);
	progress_counter & pc = m_progress_counters.back();
	auto current_time = std::chrono::system_clock::now();
	auto time_since_last_update = current_time - pc.last_updated; 
	// Only allow updates at ~10hz or so to avoid spending all time here. 
	if (std::chrono::duration_cast<std::chrono::milliseconds>(time_since_last_update).count() < 100) return; 
	pc.last_updated = current_time; 
	pc.gauge->SetValue(progress); 
	auto time_spent = current_time - pc.time_when_started; 
	//auto time_spent_in_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time_spent).count();
	auto estimated_time = (float(pc.gauge->GetRange()) / float(progress)) * time_spent; 
	auto estimated_time_left = estimated_time - time_spent; 
	auto seconds = std::chrono::duration_cast<std::chrono::seconds>(estimated_time_left).count();
	auto minutes = seconds / 60; 
	auto hours = minutes / 60; 
	minutes = minutes - hours * 60; 
	seconds = seconds - hours * 3600 - minutes * 60; 
	std::string label = pc.task_name
		+ ", ETA: [" + std::to_string(hours) + "h:" +
		std::to_string(minutes) + "m:" + std::to_string(seconds) + "s]";
	pc.label->SetLabel(label);
	m_dialog->Refresh();
	wxTheApp->Yield(); 
}

void ProgressListenerDialog::OnExitButton(wxCommandEvent &/*event*/)
{
	// Called when the user presses exit. Just kill the program, no recovery
	// is possible. 
	LOG_ERROR("User aborted! Freaking out and exiting."); 
	exit(0);
}

void ProgressListenerDialog::push_task(const std::string & name, bool indeterminate)
{
	// IF this is the top progressbar, create the dialog
	if (m_progress_counters.size() == 0) {
		m_dialog = new wxDialog(NULL, -1, "COMPUTING");
		m_sizer = new wxBoxSizer(wxVERTICAL);
		m_outer_sizer = new wxBoxSizer(wxVERTICAL);
		m_exit_button = new wxButton(m_dialog, wxID_ANY, "Exit");
		m_exit_button->Bind(wxEVT_BUTTON, &ProgressListenerDialog::OnExitButton, this);
		m_app->m_frame->Enable(false);
		m_dialog->SetSizer(m_outer_sizer);
		m_outer_sizer->Add(m_sizer);
		m_outer_sizer->Add(m_exit_button);
	}
	// Create a new progressbar in the dialog
	progress_counter pc;
	pc.task_name = name; 
	pc.label = new wxStaticText(m_dialog, wxID_ANY, pc.task_name);
	pc.gauge = new wxGauge(m_dialog, wxID_ANY, 0);
	pc.gauge->SetMinSize(wxSize(400, pc.gauge->GetSize().y));
	if (indeterminate) pc.gauge->Pulse(); 
	pc.time_when_started = std::chrono::system_clock::now(); 
	m_sizer->Add(pc.label);
	m_sizer->Add(pc.gauge);
	m_dialog->Fit();
	m_dialog->Show();
	m_progress_counters.push_back(pc);
	m_dialog->Update();
	wxTheApp->Yield();
}

void ProgressListenerDialog::pop_task()
{
	assert(m_progress_counters.size() > 0);
	// Remove the current progressbar
	m_sizer->Remove(m_sizer->GetItemCount() - 1);
	m_sizer->Remove(m_sizer->GetItemCount() - 1);
	progress_counter &pc = m_progress_counters.back(); 
	pc.label->Show(false);
	pc.gauge->Show(false);
	m_dialog->Fit();
	m_dialog->Show();
	m_progress_counters.pop_back();
	m_dialog->Update();
	wxTheApp->Yield();
	// IF this is the last progressbar, close and delete the dialog
	if (m_progress_counters.size() == 0) {
		m_app->m_frame->Enable(true);
		m_app->m_frame->Show();
		m_dialog->Close();
		delete m_dialog; 
		m_dialog = nullptr; 
	}
}
